package com.sportsstars;

/**
 * Created by gautambisht on 10/11/16.
 */

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.connection.Connection;
import com.appster.chatlib.service.ConnectionService;
import com.appster.chatlib.utils.LogUtils;
import com.aspsine.multithreaddownload.DownloadConfiguration;
import com.aspsine.multithreaddownload.DownloadManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.orhanobut.hawk.Hawk;
import com.sportsstars.MVVM.di.AuthComponent;
import com.sportsstars.MVVM.di.DaggerAuthComponent;
import com.sportsstars.chat.database.RealmManager;
import com.sportsstars.util.NetworkMonitor;
import com.sportsstars.util.NutraBaseImageDecoder;

import io.realm.Realm;


public class SportsStarsApp extends MultiDexApplication {
    private static SportsStarsApp mAppContext;
//    AuthComponent authComponent;
    public static boolean isChatScreenActive = false;
    //private MixpanelAPI mixpanelAPI;


    public static Context getAppContext() {
        return mAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)           // Enables Crashlytics debugger
//                .build();
//        Fabric.with(fabric);

//        Fabric.with(this, new Crashlytics());
       // Fabric.with(this, new Crashlytics());
//        authComponent = DaggerAuthComponent.builder().build();
        mAppContext = this;
       // mixpanelAPI = MixpanelAPI.getInstance(this, getString(R.string.mixpanel_token));

        /*
          Initialize DB Helper class.
         */
        Hawk.init(mAppContext).build();
        NetworkMonitor.initialize();

        initRealm();
        ChatController.init(this);

        initDownloader();

       // Stetho.initializeWithDefaults(this);

        if (BuildConfig.DEBUG) {
            // LeakCanary.install(this);
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .detectActivityLeaks()
                    .penaltyLog()
                    //.penaltyDeath()
                    .build());
          /*  Stetho.initialize(
                    Stetho.newInitializerBuilder(this)
                            .enableDumpapp(
                                    Stetho.defaultDumperPluginsProvider(this))
                            .enableWebKitInspector(
                                    Stetho.defaultInspectorModulesProvider(this))
                            .build());*/
        }


        // Push ReadNotificationRequest initialize
        FirebaseApp.initializeApp(mAppContext);

        initImageLoader(mAppContext);

    }

    // Initializing Image Loader
    private void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .memoryCache(new WeakMemoryCache())
                .imageDecoder(new NutraBaseImageDecoder(true))
                .build();

        ImageLoader.getInstance().init(config);
    }

    public static void destroyAllStaticVariables() {
        ChatController.destroyAllStaticVariables();
        isChatScreenActive = false;
        LogUtils.LOGD("STATIC", "Destroy all static variables.");

    }

    private void initRealm() {
        try {
            Realm.init(this);
            RealmManager.initializeRealmConfig();

        } catch (Exception ex) {
            LogUtils.LOGE("AppController", ex.toString());
        }
    }

    public void closeConnection() {
        Connection.disableAutomaticReconnect();
        Intent mIntent = new Intent(getApplicationContext(), ConnectionService.class);
        mIntent.setAction(ConnectionService.STOP_CONNECTION_SERVICE);
        startService(mIntent);


    }

    private void initDownloader() {
        DownloadConfiguration configuration = new DownloadConfiguration();
        configuration.setMaxThreadNum(10);
        configuration.setThreadNum(3);
        DownloadManager.getInstance().init(getApplicationContext(), configuration);
    }


//    public AuthComponent getAppComponent() {
//        return authComponent;
//    }
    public static SportsStarsApp getInstance(){
        return mAppContext;
    }

    //public MixpanelAPI getMixpanelAPI(){
    //    return mixpanelAPI;
    //}

}
