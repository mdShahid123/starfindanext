/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.interfaces;

public interface RecyclerPageChangeListener {

    void onPageChanged(int position);
}
