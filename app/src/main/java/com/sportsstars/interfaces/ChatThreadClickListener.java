package com.sportsstars.interfaces;

import android.view.View;

import com.sportsstars.chat.chatobjects.ChatThreads;


public interface ChatThreadClickListener {

    void onChatThreadClickListener(int position, View parent, ChatThreads chatThread);

    void onChatThreadLongClickListener(int position, View parent, ChatThreads chatThread);

}
