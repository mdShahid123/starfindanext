package com.sportsstars.interfaces;

import com.sportsstars.databinding.ItemCoachResultBinding;
import com.sportsstars.databinding.ItemSpeakerResultBinding;
import com.sportsstars.model.Coach;
import com.sportsstars.model.Speaker;

public interface SpeakerBySportItemClick {

     void onItemClick(ItemSpeakerResultBinding itemSpeakerResultBinding, Speaker speaker, int pos);
}
