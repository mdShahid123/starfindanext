package com.sportsstars.interfaces;

public interface SportsItemSelectedListener {

    void onSportsItemSelected(int position, boolean isSelected);
}
