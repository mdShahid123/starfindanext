/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.interfaces;

public interface TravelInfoListener {

    void onInterStateTravelClickListener(int interStateTravel);

    void onTravelTypeClickListener(int travelType);

    void onTravelTypeInfoClickListener(int travelType);

    void onStateSwitchChangedListener(int position, boolean state);

    void onStateFeeClickListener(int parentPosition, int childPosition);

    void onTravelSuggestionClickListener();

}
