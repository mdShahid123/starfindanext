/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.interfaces;

import com.sportsstars.network.response.coachsession.CoachSession;

public interface CoachSessionItemClickListener {

    void onCoachSessionItemClick(CoachSession coachSession, int position);
}
