package com.sportsstars.interfaces;

import com.sportsstars.databinding.ItemSessionBinding;
import com.sportsstars.model.Session;

/**
 * Created by abhaykant on 21/01/18.
 */

public interface SessionItemClick {

     void onItemClick(ItemSessionBinding itemSessionBinding, Session session, int pos);
}
