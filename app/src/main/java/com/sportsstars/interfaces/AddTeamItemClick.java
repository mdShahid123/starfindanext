package com.sportsstars.interfaces;

import com.sportsstars.model.Team;

/**
 * Created by rohantaneja on 17/04/18.
 */

public interface AddTeamItemClick {
    void onTeamSelected(Team team);
}
