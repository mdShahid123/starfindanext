/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.interfaces;

public interface EventInfoListener {

    void onSpecialitiesEntered(String specialitiesText);

    void onEventSwitchChangedListener(int position, boolean state);

    void onEventFeeClickListener(int parentPosition, int childPosition);

    void onEventCategoriesClickListener(int position);

}
