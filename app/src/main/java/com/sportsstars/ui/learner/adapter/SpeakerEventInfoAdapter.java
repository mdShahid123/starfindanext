package com.sportsstars.ui.learner.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemSpeakerEventInfoLayoutBinding;
import com.sportsstars.databinding.SpeakerEventListItemBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.interfaces.SpeakerSelectedListener;
import com.sportsstars.network.response.LearnerEvent;
import com.sportsstars.network.response.speaker.GuestSpeakerSessionRequest;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SpeakerEventInfoAdapter extends RecyclerView.Adapter<SpeakerEventInfoAdapter.MyViewHolder> {

    private ArrayList<GuestSpeakerSessionRequest> mGuestSpeakerSessionList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ListItemClickListener mListItemClickListener;
    private SpeakerSelectedListener mSpeakerSelectedListener;
    private int mEventStatus = -1;

    public SpeakerEventInfoAdapter(Activity mActivity,
                                    ArrayList<GuestSpeakerSessionRequest> mGuestSpeakerSessionList,
                                    ListItemClickListener mListItemClickListener,
                                    SpeakerSelectedListener mSpeakerSelectedListener, int eventStatus) {
        this.mActivity = mActivity;
        this.mGuestSpeakerSessionList = mGuestSpeakerSessionList;
        this.mListItemClickListener = mListItemClickListener;
        this.mSpeakerSelectedListener = mSpeakerSelectedListener;
        this.mEventStatus = eventStatus;
        mInflator = LayoutInflater.from(mActivity);
    }

    public void setEventStatus(int eventStatus) {
        this.mEventStatus = eventStatus;
    }

    @Override
    public SpeakerEventInfoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSpeakerEventInfoLayoutBinding mBinding = DataBindingUtil.inflate(mInflator, R.layout.item_speaker_event_info_layout, parent, false);
        return new SpeakerEventInfoAdapter.MyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(SpeakerEventInfoAdapter.MyViewHolder holder, int position) {
        if (mGuestSpeakerSessionList != null && mGuestSpeakerSessionList.size() > 0) {
            holder.bindData(mGuestSpeakerSessionList.get(position), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mGuestSpeakerSessionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ItemSpeakerEventInfoLayoutBinding speakerEventListItemBinding;

        public MyViewHolder(ItemSpeakerEventInfoLayoutBinding speakerEventListItemBinding) {
            super(speakerEventListItemBinding.getRoot());
            this.speakerEventListItemBinding = speakerEventListItemBinding;
        }

        public ItemSpeakerEventInfoLayoutBinding getBinding() {
            return speakerEventListItemBinding;
        }

        public void bindData(GuestSpeakerSessionRequest guestSpeakerSessionRequest, int position) {
            speakerEventListItemBinding.setGuestSpeakerSession(guestSpeakerSessionRequest);
            if(guestSpeakerSessionRequest != null) {

                if(guestSpeakerSessionRequest.getEventUserInfo() != null
                        && !TextUtils.isEmpty(guestSpeakerSessionRequest.getEventUserInfo().getProfileImage())) {
                    Picasso.with(mActivity)
                            .load(guestSpeakerSessionRequest.getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(speakerEventListItemBinding.ivImageUser);
                }

                if(guestSpeakerSessionRequest.getEventUserInfo() != null
                        && !TextUtils.isEmpty(guestSpeakerSessionRequest.getEventUserInfo().getName())) {
                    speakerEventListItemBinding.tvSpeakerName.setText(guestSpeakerSessionRequest.getEventUserInfo().getName());
                }

                int athletePos = position+1;
                speakerEventListItemBinding.tvAthlete.setText(mActivity.getResources()
                        .getString(R.string.athlete_with_count, athletePos));


                String upcomingText = Utils.getSpeakerAvailabilityStatusText(guestSpeakerSessionRequest.getStatus());
                speakerEventListItemBinding.tvUpcoming.setText(upcomingText);
                speakerEventListItemBinding.tvUpcoming.setBackground(Utils.getSpeakerAvailabilityStatusBackground(mActivity, guestSpeakerSessionRequest.getStatus()));

                if(guestSpeakerSessionRequest.getEventUserInfo() != null
                        && guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser() != null
                        && guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports() != null
                        && guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports().getName() != null
                        && !TextUtils.isEmpty(guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports().getName())) {
                    speakerEventListItemBinding.tvSportName.setText(guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports().getName());
                }

                if(guestSpeakerSessionRequest.getEventUserInfo() != null
                        && guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser() != null
                        && guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports() != null
                        && guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports().getIcon() != null
                        && !TextUtils.isEmpty(guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports().getIcon())) {
                    Picasso.with(mActivity)
                            .load(guestSpeakerSessionRequest.getEventUserInfo().getCoachDetailsUser().getSports().getIcon())
                            .placeholder(R.drawable.ic_cricket_green)
                            .error(R.drawable.ic_cricket_green)
                            .into(speakerEventListItemBinding.ivSportIcon);
                }

                if(mEventStatus == Utils.STATUS_BOOKING.UPCOMING) {
                    speakerEventListItemBinding.ivCheckbox.setImageResource(R.drawable.ic_chat_green);
                } else {
                    if(guestSpeakerSessionRequest.isSelected()) {
                        speakerEventListItemBinding.ivCheckbox.setImageResource(R.drawable.ic_checked);
                    } else {
                        speakerEventListItemBinding.ivCheckbox.setImageResource(R.drawable.ic_unchecked_circle);
                    }
                }

                if(guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED
                        || guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING
                        || guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.NO_RESPONSE) {
                    speakerEventListItemBinding.ivCheckbox.setVisibility(View.INVISIBLE);
                } else {
                    if(mEventStatus == Utils.STATUS_BOOKING.UPCOMING) {
                        if(guestSpeakerSessionRequest.getIsPaid() == 1) {
                            speakerEventListItemBinding.ivCheckbox.setVisibility(View.VISIBLE);
                        } else {
                            speakerEventListItemBinding.ivCheckbox.setVisibility(View.INVISIBLE);
                        }
                    } else if(mEventStatus == Utils.STATUS_BOOKING.CANCELLED){
                        speakerEventListItemBinding.ivCheckbox.setVisibility(View.INVISIBLE);
                    } else if(mEventStatus == Utils.STATUS_BOOKING.COMPLETED){
                        speakerEventListItemBinding.ivCheckbox.setVisibility(View.INVISIBLE);
                    } else if(mEventStatus == Utils.STATUS_BOOKING.ONGOING){
                        speakerEventListItemBinding.ivCheckbox.setVisibility(View.INVISIBLE);
                    } else {
                        speakerEventListItemBinding.ivCheckbox.setVisibility(View.VISIBLE);
                    }
                }

                speakerEventListItemBinding.ivCheckbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mSpeakerSelectedListener != null) {
//                            if(guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING
//                                    || guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
//                                return;
//                            }
                            if(mEventStatus == Utils.STATUS_BOOKING.UPCOMING) {
                                if(guestSpeakerSessionRequest.getIsPaid() == 1) {
                                    mSpeakerSelectedListener.onSpeakerSelectedForChat(position);
                                }
                            } else {
                                if(guestSpeakerSessionRequest.isSelected()) {
                                    mSpeakerSelectedListener.onSpeakerSelected(position, false);
                                } else {
                                    mSpeakerSelectedListener.onSpeakerSelected(position, true);
                                }
                            }
                        }
                    }
                });

                speakerEventListItemBinding.tvTotalPriceValue.setText(
                        mActivity.getResources().getString(R.string.min_budget_with_dollar,
                        (int) guestSpeakerSessionRequest.getSessionPrice()));

                speakerEventListItemBinding.relUserInfo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListItemClickListener.onListItemClicked(position);
                    }
                });

            }
        }

    }

}
