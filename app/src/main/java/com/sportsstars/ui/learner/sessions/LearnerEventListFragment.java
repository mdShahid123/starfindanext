package com.sportsstars.ui.learner.sessions;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentLearnerEventListBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.UserRoleRequest;
import com.sportsstars.network.response.LearnerEvent;
import com.sportsstars.network.response.LearnerEventListResponse;
import com.sportsstars.network.response.auth.UserRoleResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.learner.adapter.LearnerEventAdapter;
import com.sportsstars.ui.learner.events.LearnerEventDetailsActivity;
import com.sportsstars.ui.search.SearchCoachActivity;
import com.sportsstars.ui.speaker.search.SelectEventTypeActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

public class LearnerEventListFragment extends BaseFragment implements ListItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private static final int PER_PAGE_COUNT = 10;
    private FragmentLearnerEventListBinding mBinding;
    private LearnerEventAdapter mLearnerEventAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<LearnerEvent> eventList = new ArrayList<>();
    private int pageNo = 1;
    private int perPageRecords;
    private int listCount;
    private boolean isListLoading;
    private WeakReference<LearnerHomeActivity> mActivity;

    public static LearnerEventListFragment newInstance() {
        return new LearnerEventListFragment();
    }

    @Override
    public String getFragmentName() {
        return LearnerEventListFragment.class.getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((LearnerHomeActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_learner_event_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (LearnerHomeActivity.selectedTabPos == LearnerHomeActivity.MY_BOOKING) {
            if(eventList != null && eventList.size() > 0) {
                eventList.clear();
            }
            refreshList();
        }
    }

    private void initViews() {
        try {
            mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            mLearnerEventAdapter = new LearnerEventAdapter(getActivity(), eventList, this);
            mBinding.rvSessions.setAdapter(mLearnerEventAdapter);
            mBinding.rvSessions.setLayoutManager(mLayoutManager);
            mBinding.swipeRefreshLayout.setOnRefreshListener(this);
            mBinding.rvSessions.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (!Utils.isNetworkAvailable()) {
                        showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        return;
                    }
                    checkIfItsLastPage();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkIfItsLastPage() {
        try {
            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
            if (!isListLoading) {
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    if (shouldCallApi()) {
                        ++pageNo;
                        getSessionsList(pageNo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean shouldCallApi() {
        return this.pageNo * perPageRecords <= listCount;
    }

    private void getSessionsList(int pageNo) {
        try {
            if (!mBinding.swipeRefreshLayout.isRefreshing()
                    || mActivity.get().isFromNotificationPanel) {
                processToShowDialog();
            }
            isListLoading = true;
            AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
            webServices.getLearnerEventList(pageNo, PER_PAGE_COUNT)
                    .enqueue(new BaseCallback<LearnerEventListResponse>(getBaseActivity()) {

                        @Override
                        public void onSuccess(LearnerEventListResponse response) {
                            try {
                                if (response != null && response.getStatus() == 1) {
                                    if(response.getResult() != null && response.getResult().getData() != null) {
                                        ArrayList<LearnerEvent> localSessionList = response.getResult().getData();
                                        if (localSessionList != null && !localSessionList.isEmpty()) {
                                            showEmptyListError(false, false);
                                            if (mBinding.swipeRefreshLayout.isRefreshing()) {
                                                if (eventList != null && !eventList.isEmpty()) {
                                                    eventList.clear();
                                                }
                                            }

                                            listCount = response.getResult().getTotal();
                                            perPageRecords = response.getResult().getPerPage();
                                            if(eventList != null) {
                                                eventList.addAll(localSessionList);
                                            }
                                            mLearnerEventAdapter.notifyDataSetChanged();
                                        } else {
                                            if (eventList != null && !eventList.isEmpty()) {
                                                return;
                                            }
                                            showEmptyListError(true, false);
                                        }
                                    } else {
                                        if (eventList != null && !eventList.isEmpty()) {
                                            return;
                                        }
                                        showEmptyListError(true, false);
                                    }
                                }
                                mBinding.swipeRefreshLayout.setRefreshing(false);
                                isListLoading = false;

//                            if(mActivity != null) {
//                                if(mActivity.get().notificationTypeId
//                                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED
//                                        && mActivity.get().notificationPayloadSessionId == 0) {
//                                    if(Utils.isNetworkAvailable() && mActivity != null
//                                            && mActivity.get().isFromNotificationPanel) {
//                                        userRoleApi();
//                                    }
//                                } else if(mActivity.get().notificationTypeId
//                                        != Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED
//                                        && mActivity.get().notificationPayloadSessionId != 0) {
//                                    launchSessionDetailsFromNotificationPanel();
//                                }
//                            }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(Call<LearnerEventListResponse> call, BaseResponse baseResponse) {
                            mBinding.swipeRefreshLayout.setRefreshing(false);
                            isListLoading = false;
                            if (eventList != null && eventList.isEmpty()) {
                                showEmptyListError(true, false);
                                try {
                                    if (baseResponse != null && baseResponse.getMessage() != null
                                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                        showSnackbarFromTop(baseResponse.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                getBaseActivity().hideProgressBar();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmptyListError(boolean showError, boolean showInternetError) {
        try {
            if (showError) {
                mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                mBinding.tvNoSessions.setVisibility(View.GONE);
                mBinding.rvSessions.setVisibility(View.GONE);
                if (showInternetError) {
                    mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setVisibility(View.VISIBLE);
                    mBinding.tvNoSessions.setText(getResources().getString(R.string.no_internet));
                } else {
                    mBinding.tvNoSessions.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setText(getResources().getString(R.string.no_sessions_msg));
                    mBinding.rlNoSessionsParent.setVisibility(View.VISIBLE);
                    mBinding.btnSearchCoach.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundleCoach = new Bundle();
                            bundleCoach.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, true);
                            Navigator.getInstance().navigateToActivityWithData(getActivity(), SearchCoachActivity.class, bundleCoach);
                        }
                    });
                    mBinding.btnSearchSpeaker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            showSnackbarFromTop("Coming Soon");

                            Bundle bundleSpeaker = new Bundle();
                            bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                            bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
                            bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
                            //Navigator.getInstance().navigateToActivityWithData(getActivity(), SearchSpeakerTypeActivity.class, bundleSpeaker);
                            Navigator.getInstance().navigateToActivityWithData(getActivity(), SelectEventTypeActivity.class, bundleSpeaker);
                            //Navigator.getInstance().navigateToActivityWithData(getActivity(), SelectSpeakerSportsActivity.class, bundleSpeaker);
                        }
                    });
                }
            } else {
                mBinding.tvNoSessions.setVisibility(View.GONE);
                mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                mBinding.rvSessions.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshList() {
        try {
            if (!Utils.isNetworkAvailable()) {
                if (mBinding.swipeRefreshLayout.isRefreshing()) {
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                }
                showSnackbarFromTop(getResources().getString(R.string.no_internet));
                showEmptyListError(true, true);
                return;
            }
            pageNo = 1;
            isListLoading = false;
            listCount = 0;
            perPageRecords = 0;
//        if(sessionList != null && !sessionList.isEmpty()) {
//            sessionList.clear();
//        }
            if (Utils.isNetworkAvailable()) {
                getSessionsList(pageNo);
            } else {
                showEmptyListError(true, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userRoleApi() {

        int mUserRole = Constants.USER_ROLE.COACH;
        PreferenceUtil.setCurrentUserRole(mUserRole);

        UserRoleRequest userRoleRequest = new UserRoleRequest();
        userRoleRequest.setUserLoggedInType(mUserRole);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.userRole(userRoleRequest).enqueue(new BaseCallback<UserRoleResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(UserRoleResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        Log.d("mylog", "User role updated on server.");
                    } else {
                        Log.d("mylog", "User role not updated on server.");
                    }
                }
                if(mActivity != null && mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED) {
                    mActivity.get().isFromNotificationPanel = false;
                }
            }

            @Override
            public void onFail(Call<UserRoleResponse> call, BaseResponse baseResponse) {
                if(mActivity != null && mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED) {
                    mActivity.get().isFromNotificationPanel = false;
                }
                try {
                    if(mActivity != null) {
                        mActivity.get().hideProgressBar();
                    }
                    if (baseResponse != null
                            && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        Log.d("mylog", "User role not updated to coach. Error - " + baseResponse.getMessage());
                        //showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onListItemClicked(int position) {
//        showSnackbarFromTop("Coming Soon");

        if(Utils.isNetworkAvailable()) {
            if (eventList != null && !eventList.isEmpty()) {
                LearnerEvent learnerEvent = eventList.get(position);
                Intent i = new Intent(getActivity(), LearnerEventDetailsActivity.class);
                i.putExtra(Constants.BundleKey.SESSION_ID_KEY, learnerEvent.getSessionId());
                startActivity(i);
            }
        } else {
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

}
