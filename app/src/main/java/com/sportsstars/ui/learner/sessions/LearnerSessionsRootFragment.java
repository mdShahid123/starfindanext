package com.sportsstars.ui.learner.sessions;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentLearnerSessionsRootBinding;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.util.Constants;

import java.lang.ref.WeakReference;

public class LearnerSessionsRootFragment extends BaseFragment {

    private FragmentLearnerSessionsRootBinding mBinding;
    private FragmentManager mFragmentManagerLearner;
    private WeakReference<LearnerHomeActivity> mActivity;
    private int mFragmentPosition;

    public static LearnerSessionsRootFragment newInstance() {
        return new LearnerSessionsRootFragment();
    }

    @Override
    public String getFragmentName() {
        return LearnerSessionsRootFragment.class.getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((LearnerHomeActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_learner_sessions_root, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {

        Bundle bundle = getArguments();
        if(bundle != null) {
            mFragmentPosition = bundle.getInt(Constants.BundleKey.LOAD_FRAGMENT_POSITION, 0);
        }

        mFragmentManagerLearner = getActivity().getSupportFragmentManager();

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                getResources().getString(R.string.font_bebas_neue));
        mBinding.radioBtn1.setTypeface(font);
        mBinding.radioBtn2.setTypeface(font);

        if(mFragmentPosition == 0) {
            mBinding.radioBtn1.setChecked(true);
        } else {
            mBinding.radioBtn2.setChecked(true);
        }
        loadFragment(mFragmentPosition, true);
        mBinding.radioGroupHeader.setOnCheckedChangeListener(radioListener);
    }

    private RadioGroup.OnCheckedChangeListener radioListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                switch (checkedId) {
                    case R.id.radio_btn_1:
                        loadFragment(0, true);
                        break;
                    case R.id.radio_btn_2:
                        loadFragment(1, true);
                        break;
                }
            }
        }
    };

    private void loadFragment(int position, boolean isReload) {
        switch (position) {
            case 0:
                LearnerSessionListFragment learnerSessionListFragment = LearnerSessionListFragment.newInstance();
                if (mFragmentManagerLearner != null) {
                    mFragmentManagerLearner.beginTransaction().replace(R.id.frame_layout, learnerSessionListFragment).commit();
                }
                if (mActivity != null) {
                    mActivity.get().setUpHeader(mActivity.get().mBinder.headerId,
                            getResources().getString(R.string.my_bookings), "");
                }
                break;
            case 1:
                LearnerEventListFragment learnerEventListFragment = LearnerEventListFragment.newInstance();
                if (mFragmentManagerLearner != null) {
                    mFragmentManagerLearner.beginTransaction().replace(R.id.frame_layout, learnerEventListFragment).commit();
                }
                if (mActivity != null) {
                    mActivity.get().setUpHeader(mActivity.get().mBinder.headerId,
                            getResources().getString(R.string.my_bookings), "");
                }
                break;
        }
    }

}
