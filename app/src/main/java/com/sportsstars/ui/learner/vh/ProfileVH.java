package com.sportsstars.ui.learner.vh;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemLearnerProfileBinding;
import com.sportsstars.network.request.auth.learner.ProfileRequest;
import com.sportsstars.ui.auth.VerifyPhoneActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.learner.CreateProfileActivity;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by atul on 30/04/18.
 * To inject activity reference.
 */

public class ProfileVH extends RecyclerView.ViewHolder {

    private static final String TAG = "Profile";

    private String mDOB;
    private Calendar mCalendar;
    ItemLearnerProfileBinding mBinding;
    private Activity mActivity;

    public ProfileVH(ItemLearnerProfileBinding binding, Activity activity) {
        super(binding.getRoot());
        mBinding = binding;
        mCalendar = Calendar.getInstance();
        mActivity = activity;
    }

    public void bindData(final ProfileRequest profileRequest, Bitmap bitmap) {

        mBinding.setProfile(profileRequest);

        if(CreateProfileActivity.mIsEdit) {
            mBinding.tvMobileNo.setVisibility(View.VISIBLE);
            mBinding.etMobileNo.setVisibility(View.VISIBLE);
            mBinding.lineMobileNo.setVisibility(View.VISIBLE);
            mBinding.etMobileNo.setFocusable(false);
            mBinding.etMobileNo.setFocusableInTouchMode(false);

//            mBinding.spinnerAccountType.setSelection(-1);
//            mBinding.spinnerAccountType.setOnItemSelectedListener(null);
//            mBinding.etCompanyName.setFocusable(false);
//            mBinding.etCompanyName.setFocusableInTouchMode(false);
//            mBinding.etAbnNumber.setFocusable(false);
//            mBinding.etAbnNumber.setFocusableInTouchMode(false);

            //mBinding.ivSpinner.setVisibility(View.INVISIBLE);
            mBinding.tvTitleAccountType.setText(mActivity.getResources().getString(R.string.select_account_type));

            //set tips type spinner
            ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<String>(mActivity, R.layout.item_spinner_transparent,
                    mActivity.getResources().getStringArray(R.array.account_type_array)) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        tv.setTextColor(ContextCompat.getColor(mActivity, R.color.hint_text_color));
                    } else {
                        tv.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_clr_green));
                    }
                    return view;
                }
            };
            mBinding.spinnerAccountType.setAdapter(accountTypeAdapter);
            accountTypeAdapter.setDropDownViewResource(R.layout.item_booking_spinner);

            mBinding.spinnerAccountType.setSelection(-1);
            mBinding.spinnerAccountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        return;
                    }
                    mBinding.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
                    mBinding.tvSpinnerLabel.setText(String.valueOf(parent.getItemAtPosition(position)));
                    mBinding.tvSpinnerLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.selected_item_text_color));

                    profileRequest.setAccountType((position == 1 ? Constants.ACCOUNT_TYPE.PERSONAL : Constants.ACCOUNT_TYPE.CORPORATE));

                    mBinding.spinnerAccountType.setSelection(-1);

                    ((TextView) view).setText(null);

                    if(position == 1) {
                        mBinding.llCorporateAccountDetails.setVisibility(View.GONE);
                    } else {
                        mBinding.llCorporateAccountDetails.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            mBinding.etCompanyName.setFocusable(true);
            mBinding.etCompanyName.setFocusableInTouchMode(true);
            mBinding.etAbnNumber.setFocusable(true);
            mBinding.etAbnNumber.setFocusableInTouchMode(true);

        } else {
            mBinding.tvMobileNo.setVisibility(View.GONE);
            mBinding.etMobileNo.setVisibility(View.GONE);
            mBinding.lineMobileNo.setVisibility(View.GONE);
            mBinding.etMobileNo.setFocusable(false);
            mBinding.etMobileNo.setFocusableInTouchMode(false);
            mBinding.ivSpinner.setVisibility(View.VISIBLE);
            mBinding.tvTitleAccountType.setText(mActivity.getResources().getString(R.string.select_account_type));

            //set tips type spinner
            ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<String>(mActivity, R.layout.item_spinner_transparent,
                    mActivity.getResources().getStringArray(R.array.account_type_array)) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        tv.setTextColor(ContextCompat.getColor(mActivity, R.color.hint_text_color));
                    } else {
                        tv.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_clr_green));
                    }
                    return view;
                }
            };
            mBinding.spinnerAccountType.setAdapter(accountTypeAdapter);
            accountTypeAdapter.setDropDownViewResource(R.layout.item_booking_spinner);

            mBinding.spinnerAccountType.setSelection(-1);
            mBinding.spinnerAccountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        return;
                    }
                    mBinding.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
                    mBinding.tvSpinnerLabel.setText(String.valueOf(parent.getItemAtPosition(position)));
                    mBinding.tvSpinnerLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.selected_item_text_color));

                    profileRequest.setAccountType((position == 1 ? Constants.ACCOUNT_TYPE.PERSONAL : Constants.ACCOUNT_TYPE.CORPORATE));

                    mBinding.spinnerAccountType.setSelection(-1);

                    ((TextView) view).setText(null);

                    if(position == 1) {
                        mBinding.llCorporateAccountDetails.setVisibility(View.GONE);
                    } else {
                        mBinding.llCorporateAccountDetails.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            mBinding.etCompanyName.setFocusable(true);
            mBinding.etCompanyName.setFocusableInTouchMode(true);
            mBinding.etAbnNumber.setFocusable(true);
            mBinding.etAbnNumber.setFocusableInTouchMode(true);
        }

        if(!TextUtils.isEmpty(profileRequest.getDob())) {
            mBinding.etAge.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dropdown_selected,0);
        } else {
            mBinding.etAge.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dropdown,0);
        }

        if(profileRequest.getAccountType() == Constants.ACCOUNT_TYPE.NONE) {
            profileRequest.setAccountType(Constants.ACCOUNT_TYPE.PERSONAL);
            mBinding.tvSpinnerLabel.setText(mActivity.getResources().getString(R.string.personal_account));
            mBinding.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
        }

        if(profileRequest.getAccountType() == Constants.ACCOUNT_TYPE.CORPORATE) {
            mBinding.llCorporateAccountDetails.setVisibility(View.VISIBLE);
        } else {
            mBinding.llCorporateAccountDetails.setVisibility(View.GONE);
        }

        if(profileRequest.getAbnNumber() == 0) {
            mBinding.etAbnNumber.setText("");
        } else {
            mBinding.etAbnNumber.setText(String.valueOf(profileRequest.getAbnNumber()));
        }
        if(!TextUtils.isEmpty(profileRequest.getCompanyName())) {
            mBinding.etCompanyName.setText(profileRequest.getCompanyName());
        } else {
            mBinding.etCompanyName.setText("");
        }

        profileRequest.setAccountType(profileRequest.getAccountType() == 0 ? Constants.ACCOUNT_TYPE.NONE : profileRequest.getAccountType());
        mBinding.tvSpinnerLabel.setText(Utils.getAccountTypeString(profileRequest.getAccountType()));
        if(profileRequest.getAccountType() == Constants.ACCOUNT_TYPE.NONE) {
            mBinding.ivSpinner.setImageResource(R.drawable.ic_dropdown);
            mBinding.tvSpinnerLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.black_25));
        } else {
            mBinding.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
            mBinding.tvSpinnerLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.selected_item_text_color));
        }
        profileRequest.setGender(profileRequest.getGender() == 0 ? Constants.GENDER_TYPE.MALE : profileRequest.getGender());
        setGender(profileRequest.getGender());
        if (bitmap != null) {
            profileRequest.setProfileImage(null);
            mBinding.imageUser.setImageBitmap(bitmap);
        }
        setProfilePic(profileRequest.getProfileImage());
        mBinding.etAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(profileRequest);
            }
        });
        mBinding.etPostCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.EXTRA_TITLE, mActivity.getResources().getString(R.string.select_location));
                Navigator.getInstance().navigateToActivityForResultWithData(mActivity,
                        SearchCoachLocationActivity.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_LOCATION);
            }
        });
        mBinding.rlMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileRequest.setGender(Constants.GENDER_TYPE.MALE);
                setGender(Constants.GENDER_TYPE.MALE);
            }
        });
        mBinding.rlFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileRequest.setGender(Constants.GENDER_TYPE.FEMALE);
                setGender(Constants.GENDER_TYPE.FEMALE);
            }
        });
        mBinding.imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Permission is granted");
                        ((Activity) mBinding.getRoot().getContext()).startActivityForResult(Utils.getPickImageChooserIntent(view.getContext()), CreateProfileActivity.REQUEST_CODE);
                    } else {
                        Log.d(TAG, "Permission is revoked");
                        ActivityCompat.requestPermissions((Activity) mBinding.getRoot().getContext(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                CreateProfileActivity.MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Log.d(TAG, "Permission is granted");
                    ((Activity) mBinding.getRoot().getContext()).startActivityForResult(Utils.getPickImageChooserIntent(view.getContext()), CreateProfileActivity.REQUEST_CODE);
                }
//                if (ContextCompat.checkSelfPermission(view.getContext(),
//                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(view.getContext(),
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions((Activity) mBinding.getRoot().getContext(),
//                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            CreateProfileActivity.MY_PERMISSIONS_REQUEST_CAMERA);
//                } else {
//                    ((Activity) mBinding.getRoot().getContext()).startActivityForResult(Utils.getPickImageChooserIntent(view.getContext()), CreateProfileActivity.REQUEST_CODE);
//                }
            }
        });

        mBinding.etMobileNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go to phone number screen
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, true);
                Navigator.getInstance().navigateToActivityWithData(mActivity, VerifyPhoneActivity.class, bundle);
            }
        });

        mBinding.etAbnNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if(!TextUtils.isEmpty(s.toString())) {
                        profileRequest.setAbnNumber(Long.parseLong(s.toString()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void setGender(int gender) {
        switch (gender) {
            case Constants.GENDER_TYPE.MALE:
                setGenderType(mBinding.rlMale, mBinding.tvMale);
                break;
            case Constants.GENDER_TYPE.FEMALE:
                setGenderType(mBinding.rlFemale, mBinding.tvFemale);
                break;
        }
    }

    private void setGenderType(RelativeLayout relSelected, TextView tvSelected) {
        Context context = mBinding.getRoot().getContext();
        mBinding.rlFemale.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_grey));
        mBinding.tvFemale.setTextColor(ContextCompat.getColor(context, R.color.black_25));

        mBinding.rlMale.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_grey));
        mBinding.tvMale.setTextColor(ContextCompat.getColor(context, R.color.black_25));

        relSelected.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_border_green_bg_grey));
        tvSelected.setTextColor(ContextCompat.getColor(context, R.color.green_gender));

    }

    private void showDatePicker(final ProfileRequest profileRequest) {
        final BaseActivity activity = (BaseActivity) mBinding.getRoot().getContext();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(mBinding.getRoot().getContext(), new
                DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormated = yyyymmddFormat.format(mCalendar.getTime());

                        try {
                            if (date != null && isValidDate(date)) {
                                mDOB = date;
                                mBinding.etAge.setText(mDOB);
                                mBinding.etAge.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dropdown_selected,0);
                                profileRequest.setDob(dateFormated);
                            } else {
                                mDOB = "";
                                mBinding.etAge.setText(mDOB);
                                activity.showSnackbarFromTop(activity, activity.getString(R.string.invalid_age));
                            }
                            int age = Utils.getAge(mCalendar);

                            if (age < 5) {
                                mDOB = "";
                                mBinding.etAge.setText(mDOB);
                                activity.showSnackbarFromTop(activity, activity.getString(R.string.invalid_age));
                            }

                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "Age>" + e.getMessage());

                        }

                    }
                }, year, month,
                day);

        Calendar minCal = Calendar.getInstance();
        minCal.set(Calendar.YEAR, 1940);
        datePickerDialog.getDatePicker().setMinDate(minCal.getTimeInMillis());

        datePickerDialog.show();
    }

    private static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(pDateString);
        return new Date().after(date);
    }

    private void setProfilePic(String profileUrl) {
        if (StringUtils.isNullOrEmpty(profileUrl))
            return;
        Picasso.with(itemView.getContext())
                .load(profileUrl).placeholder(R.drawable.ic_user_circle).
                error(R.drawable.ic_user_circle).into(mBinding.imageUser);
    }

}
