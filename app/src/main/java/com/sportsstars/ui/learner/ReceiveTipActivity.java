package com.sportsstars.ui.learner;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityReceiveTipBinding;
import com.sportsstars.model.ReceiveTipModel;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ReceiveTipActivity extends BaseActivity implements View.OnClickListener {

    private ActivityReceiveTipBinding mBinding;
    private ReceiveTipModel receiveTipModel;
    private String coachNameFromIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_receive_tip);
        try {
            getIntentData();
            initViews();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if(intent != null && intent.hasExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_TIP_DETAILS)) {
            receiveTipModel = getIntent().getParcelableExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_TIP_DETAILS);
        }
        if(intent != null && intent.hasExtra(Constants.BundleKey.COACH_NAME)) {
            coachNameFromIntent = getIntent().getStringExtra(Constants.BundleKey.COACH_NAME);
        }
    }

    private void initViews() {
        mBinding.topHeader.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.topHeader.tvTitle.setText(getString(R.string.matchday_tip));
        mBinding.topHeader.ivBack.setVisibility(View.VISIBLE);

        mBinding.topHeader.ivBack.setOnClickListener(this);
        mBinding.rlPlay.setOnClickListener(this);

        String coachName = "Your coach";
        if(receiveTipModel != null && !TextUtils.isEmpty(receiveTipModel.getCoachName())) {
            coachName = receiveTipModel.getCoachName();
        } else {
            if(!TextUtils.isEmpty(coachNameFromIntent)) {
                coachName = coachNameFromIntent;
            }
        }
        String receiveTipDetails = getResources().getString(R.string.tip_details, coachName);
        mBinding.tvTipDetails.setText(receiveTipDetails);

        if(receiveTipModel != null && !TextUtils.isEmpty(receiveTipModel.getTipVideoThumbnailUrl())) {
            mBinding.pbLoading.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(receiveTipModel.getTipVideoThumbnailUrl())
                    .into(mBinding.ivThumbnail, new Callback() {
                        @Override
                        public void onSuccess() {
                            mBinding.pbLoading.setVisibility(View.INVISIBLE);
                            mBinding.rlPlay.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            mBinding.pbLoading.setVisibility(View.INVISIBLE);
                            mBinding.rlPlay.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.rl_play:
                if(receiveTipModel != null && !TextUtils.isEmpty(receiveTipModel.getTipVideoUrl())) {
                    if (receiveTipModel.getTipVideoUrl().contains("http")) {
                        launchVideoPlayer();
                    } else {
                        if (new File(receiveTipModel.getTipVideoUrl()).exists()) {
                            launchVideoPlayer();
                        } else {
                            showSnackbarFromTop(this, getResources().getString(R.string.tip_video_removed));
                        }
                    }
                } else {
                    showSnackbarFromTop(this, getResources().getString(R.string.tip_video_removed));
                }
                break;
        }
    }

    private void launchVideoPlayer() {
        Bundle bundlePlay = new Bundle();
        bundlePlay.putString(Constants.BundleKey.VIDEO_URL, receiveTipModel.getTipVideoUrl());
        Navigator.getInstance().navigateToActivityWithData(ReceiveTipActivity.this, PlayerActivity.class, bundlePlay);
    }

    @Override
    public String getActivityName() {
        return ReceiveTipActivity.class.getName();
    }

}
