package com.sportsstars.ui.learner.sessions;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sportsstars.MVVM.adapter.MyHorizontalBookingAdapter;
import com.sportsstars.MVVM.entities.mysession.BookingList;
import com.sportsstars.MVVM.entities.mysession.LearnerSavedBookingListResponse;
import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.MVVM.ui.learner.LearnerViewAllBookings;
import com.sportsstars.R;
import com.sportsstars.adapters.ItemOffsetDecoration;
import com.sportsstars.databinding.FragmentLearnerSessionListBinding;
import com.sportsstars.interfaces.HorizontalListItemClickListener;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.CalendarSyncBaseResponse;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.learner.ReceiveTipActivity;
import com.sportsstars.ui.learner.adapter.MyBookingAdapter;
import com.sportsstars.ui.learner.events.LearnerEventDetailsActivity;
import com.sportsstars.ui.search.SearchCoachActivity;
import com.sportsstars.ui.session.SessionCoachDetails;
import com.sportsstars.ui.session.SessionDetailsLearnerActivity;
import com.sportsstars.ui.speaker.search.SearchSpeakerTypeActivity;
import com.sportsstars.ui.speaker.search.SelectEventTypeActivity;
import com.sportsstars.ui.speaker.search.SelectSpeakerSportsActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;
import com.sportsstars.widget.WrapContentLinearLayoutManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

public class LearnerSessionListFragment extends BaseFragment implements ListItemClickListener,
        HorizontalListItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 112;

    private FragmentLearnerSessionListBinding mBinding;

    private MyHorizontalBookingAdapter mSavedBookingAdapter;
    private MyBookingAdapter mBookingAdapter;

    private ArrayList<BookingList> horizontalSavedBookingList = new ArrayList<>();
    private ArrayList<BookingList> verticalSavedBookingList = new ArrayList<>();

    private WrapContentLinearLayoutManager mVerticalLayoutManager;
    private WrapContentLinearLayoutManager mHorizontalLayoutManager;

    private int pageNo = 1;
    private int perPageRecords;
    private int listCount;
    private boolean isListLoading;

    private int horizontalPageNo = 1;
    private int horizontalPerPageRecords;
    private int horizontalListCount;
    private boolean isHorizontalListLoading;

    private boolean isLoadingCombinedList;

    private WeakReference<LearnerHomeActivity> mActivity;

    public static LearnerSessionListFragment newInstance() {
        return new LearnerSessionListFragment();
    }

    @Override
    public String getFragmentName() {
        return LearnerSessionListFragment.class.getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((LearnerHomeActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_learner_session_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (LearnerHomeActivity.selectedTabPos == LearnerHomeActivity.MY_BOOKING) {
            if (horizontalSavedBookingList != null && horizontalSavedBookingList.size() > 0) {
                horizontalSavedBookingList.clear();
            }
            refreshHorizontalList();

            if (verticalSavedBookingList != null && verticalSavedBookingList.size() > 0) {
                verticalSavedBookingList.clear();
            }
            refreshList();
        }
    }

    private void redirectScreenFromNotificationPanel() {
        Log.d("session_log", "LearnerSessionListing - inside redirectFromNotificationPanel");
        if (mActivity != null) {
            if (mActivity.get().isFromNotificationPanel) {
                if (mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP) {
                    launchReceiveTipFromNotificationPanel();
                } else if (mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_ACCEPTED) {
                    if (mActivity.get().notificationPayloadSessionId != 0) {
                        Log.d("session_log", "LearnerSessionListing - redirectFromNotificationPanel - notificationPayloadSessionId : "
                                + mActivity.get().notificationPayloadSessionId);
                        launchEventDetailsFromNotificationPanel();
                    }
                }  else if (mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_DECLINED) {
                    if (mActivity.get().notificationPayloadSessionId != 0) {
                        launchEventDetailsFromNotificationPanel();
                    }
                } else {
                    if (mActivity.get().notificationPayloadSessionId != 0) {
                        launchSessionDetailsFromNotificationPanel();
                    }
                }
            }
        }
    }

    private void launchEventDetailsFromNotificationPanel() {
        if (Utils.isNetworkAvailable()) {
            if (mActivity != null) {
                if (mActivity.get().isFromNotificationPanel
                        && mActivity.get().notificationTypeId != Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP
                        && mActivity.get().notificationPayloadSessionId != 0) {
                    Intent i = new Intent(getActivity(), LearnerEventDetailsActivity.class);
                    i.putExtra(Constants.BundleKey.SESSION_ID_KEY, mActivity.get().notificationPayloadSessionId);
                    getActivity().startActivity(i);
                    mActivity.get().isFromNotificationPanel = false;
                    mActivity.get().notificationPayloadSessionId = 0;
                } else {
//                    mActivity.get().isFromNotificationPanel = false;
//                    mActivity.get().notificationPayloadSessionId = 0;
                }
            }
        } else {
            if (mActivity != null && mActivity.get().isFromNotificationPanel
                    && mActivity.get().notificationTypeId != Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP
                    && mActivity.get().notificationPayloadSessionId != 0) {
                mActivity.get().isFromNotificationPanel = false;
                mActivity.get().notificationPayloadSessionId = 0;
            }
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void launchSessionDetailsFromNotificationPanel() {
        if (Utils.isNetworkAvailable()) {
            if (mActivity != null) {
                if (mActivity.get().isFromNotificationPanel
                        && mActivity.get().notificationTypeId != Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP
                        && mActivity.get().notificationPayloadSessionId != 0) {
                    Intent i = new Intent(getActivity(), SessionDetailsLearnerActivity.class);
                    i.putExtra(getResources().getString(R.string.booking_id_extra), mActivity.get().notificationPayloadSessionId);
                    startActivity(i);
                    mActivity.get().isFromNotificationPanel = false;
                    mActivity.get().notificationPayloadSessionId = 0;
                } else {
//                    mActivity.get().isFromNotificationPanel = false;
//                    mActivity.get().notificationPayloadSessionId = 0;
                }
            }
        } else {
            if (mActivity != null && mActivity.get().isFromNotificationPanel
                    && mActivity.get().notificationTypeId != Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP
                    && mActivity.get().notificationPayloadSessionId != 0) {
                mActivity.get().isFromNotificationPanel = false;
                mActivity.get().notificationPayloadSessionId = 0;
            }
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void launchReceiveTipFromNotificationPanel() {
        if (Utils.isNetworkAvailable()) {
            if (mActivity != null) {
                if (mActivity.get().isFromNotificationPanel
                        && mActivity.get().notificationTypeId == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP
                        && mActivity.get().notificationPayloadSessionId == 0) {
                    if (mActivity.get().notificationPayloadReceiveTipModel != null) {
                        Intent i = new Intent(getActivity(), ReceiveTipActivity.class);
                        i.putExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_TIP_DETAILS, mActivity.get().notificationPayloadReceiveTipModel);
                        startActivity(i);
                        mActivity.get().isFromNotificationPanel = false;
                        mActivity.get().notificationPayloadSessionId = 0;
                        mActivity.get().notificationPayloadReceiveTipModel = null;
                    }
                } else {
//                    mActivity.get().isFromNotificationPanel = false;
//                    mActivity.get().notificationPayloadSessionId = 0;
                }
            }
        } else {
            if (mActivity != null && mActivity.get().isFromNotificationPanel
                    && mActivity.get().notificationTypeId == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP
                    && mActivity.get().notificationPayloadSessionId == 0) {
                mActivity.get().isFromNotificationPanel = false;
                mActivity.get().notificationPayloadSessionId = 0;
                mActivity.get().notificationPayloadReceiveTipModel = null;
            }
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void initViews() {
        ItemOffsetDecoration decorationVertical = new ItemOffsetDecoration(10);
        mVerticalLayoutManager = new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mBookingAdapter = new MyBookingAdapter(getActivity(), verticalSavedBookingList, this);
        mBinding.rvSessions.setAdapter(mBookingAdapter);
        mBinding.rvSessions.setLayoutManager(mVerticalLayoutManager);
        mBinding.rvSessions.addItemDecoration(decorationVertical);
        mBinding.swipeRefreshLayout.setOnRefreshListener(this);
        mBinding.rvSessions.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                    return;
                }
                try {
                    checkIfItsLastPage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ItemOffsetDecoration decorationHorizontal = new ItemOffsetDecoration(5);
        mHorizontalLayoutManager = new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mSavedBookingAdapter = new MyHorizontalBookingAdapter(getActivity(), horizontalSavedBookingList, this);
        mBinding.rvHorizontalSaved.setAdapter(mSavedBookingAdapter);
        mBinding.rvHorizontalSaved.setLayoutManager(mHorizontalLayoutManager);
        mBinding.rvHorizontalSaved.addItemDecoration(decorationHorizontal);
        mBinding.rvHorizontalSaved.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                    return;
                }
                try {
                    checkHorizontalIfItsLastPage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.tvViewAllLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable()) {
                    Navigator.getInstance().navigateToActivity(getActivity(), LearnerViewAllBookings.class);
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    private void checkIfItsLastPage() {
        int visibleItemCount = mVerticalLayoutManager.getChildCount();
        int totalItemCount = mVerticalLayoutManager.getItemCount();
        int pastVisibleItems = mVerticalLayoutManager.findFirstVisibleItemPosition();
        if (!isListLoading) {
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                if (shouldCallApi()) {
                    ++pageNo;
                    getSessionsList(pageNo);
                }
            }
        }
    }

    private boolean shouldCallApi() {
        return this.pageNo * perPageRecords <= listCount;
    }

    private void checkHorizontalIfItsLastPage() {
        int visibleItemCount = mHorizontalLayoutManager.getChildCount();
        int totalItemCount = mHorizontalLayoutManager.getItemCount();
        int pastVisibleItems = mHorizontalLayoutManager.findFirstVisibleItemPosition();
        if (!isHorizontalListLoading) {
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                if (shouldCallHorizontalApi()) {
                    ++horizontalPageNo;
                    getHorizontalSavedSessionsList(horizontalPageNo);
                }
            }
        }
    }

    private boolean shouldCallHorizontalApi() {
        return this.horizontalPageNo * horizontalPerPageRecords <= horizontalListCount;
    }

    private void getSessionsList(int pageNo) {
        try {
            if (!mBinding.swipeRefreshLayout.isRefreshing()) {
                processToShowDialog();
            }
            isListLoading = true;
            AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
            webServices.getMyBooking(pageNo, 10)
                    .enqueue(new BaseCallback<LearnerSavedBookingListResponse>(getBaseActivity()) {

                        @Override
                        public void onSuccess(LearnerSavedBookingListResponse response) {
                            try {
                                if (response != null) {
                                    if (response.getStatus() == 1) {
                                        if (response.getResult() != null && response.getResult().getData() != null) {
                                            ArrayList<BookingList> localVerticalSavedBookingList = (ArrayList<BookingList>) response.getResult().getData();
                                            if (localVerticalSavedBookingList != null && !localVerticalSavedBookingList.isEmpty()) {
                                                showEmptyListError(false, false);
                                                if (mBinding.swipeRefreshLayout.isRefreshing()) {
                                                    if (verticalSavedBookingList != null && !verticalSavedBookingList.isEmpty()) {
                                                        verticalSavedBookingList.clear();
                                                    }
                                                }

                                                listCount = response.getResult().getTotal();
                                                perPageRecords = response.getResult().getPerPage();
                                                if (verticalSavedBookingList != null)
                                                    verticalSavedBookingList.addAll(localVerticalSavedBookingList);
                                                if (mBookingAdapter != null) {
                                                    mBookingAdapter.notifyDataSetChanged();
                                                }
                                                deleteCancelledEventsVertical(verticalSavedBookingList);

                                                if (isLoadingCombinedList && pageNo == 1) {
                                                    getHorizontalSavedSessionsList(horizontalPageNo);
                                                }
                                            } else {
                                                if (verticalSavedBookingList != null && !verticalSavedBookingList.isEmpty()) {
                                                    return;
                                                }
                                                showEmptyListError(true, false);
                                                if (isLoadingCombinedList) {
                                                    showEmptyHorizontalListError(true);
                                                    isLoadingCombinedList = false;
                                                }
                                                redirectScreenFromNotificationPanel();
                                            }
                                        }
                                    }
                                }

                                if (!isLoadingCombinedList) {
                                    mBinding.swipeRefreshLayout.setRefreshing(false);
                                }
                                isListLoading = false;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(Call<LearnerSavedBookingListResponse> call, BaseResponse baseResponse) {
                            mBinding.swipeRefreshLayout.setRefreshing(false);
                            isListLoading = false;
                            try {
                                getBaseActivity().hideProgressBar();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (verticalSavedBookingList != null && verticalSavedBookingList.isEmpty()) {
                                showEmptyListError(true, false);
                                if (isLoadingCombinedList) {
                                    showEmptyHorizontalListError(true);
                                }
                                try {
                                    if (baseResponse != null && baseResponse.getMessage() != null
                                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                        showSnackbarFromTop(baseResponse.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getHorizontalSavedSessionsList(int horizontalPageNo) {
        try {
            if (!mBinding.swipeRefreshLayout.isRefreshing()) {
                processToShowDialog();
            }
            isHorizontalListLoading = true;
            AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
            webServices.getSavedBooking(horizontalPageNo, 10)
                    .enqueue(new BaseCallback<LearnerSavedBookingListResponse>(getBaseActivity()) {

                        @Override
                        public void onSuccess(LearnerSavedBookingListResponse response) {
                            if (response != null) {
                                if (response.getStatus() == 1) {
                                    if (response.getResult() != null && response.getResult().getData() != null) {
                                        ArrayList<BookingList> localHorizontalSavedBookingList = (ArrayList<BookingList>) response.getResult().getData();
                                        if (localHorizontalSavedBookingList != null && !localHorizontalSavedBookingList.isEmpty()) {
                                            showEmptyHorizontalListError(false);
                                            if (mBinding.swipeRefreshLayout.isRefreshing()) {
                                                if (horizontalSavedBookingList != null && !horizontalSavedBookingList.isEmpty()) {
                                                    horizontalSavedBookingList.clear();
                                                }
                                            }

                                            horizontalListCount = response.getResult().getTotal();
                                            horizontalPerPageRecords = response.getResult().getPerPage();
                                            if (horizontalSavedBookingList != null)
                                                horizontalSavedBookingList.addAll(localHorizontalSavedBookingList);
                                            if (mSavedBookingAdapter != null) {
                                                mSavedBookingAdapter.notifyDataSetChanged();
                                            }
                                            deleteCancelledEventsHorizontal(horizontalSavedBookingList);
                                        } else {
                                            if (horizontalSavedBookingList != null && !horizontalSavedBookingList.isEmpty()) {
                                                return;
                                            }
                                            showEmptyHorizontalListError(true);
                                        }
                                    }
                                }

                                mBinding.swipeRefreshLayout.setRefreshing(false);
                                isLoadingCombinedList = false;
                                isHorizontalListLoading = false;

                                refreshEventsEntryToCalendar();

                                redirectScreenFromNotificationPanel();
                            }
                        }

                        @Override
                        public void onFail(Call<LearnerSavedBookingListResponse> call, BaseResponse baseResponse) {
                            mBinding.swipeRefreshLayout.setRefreshing(false);
                            isHorizontalListLoading = false;
                            isLoadingCombinedList = false;
                            try {
                                getBaseActivity().hideProgressBar();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (horizontalSavedBookingList != null && horizontalSavedBookingList.isEmpty()) {
                                showEmptyHorizontalListError(true);
                                try {
                                    if (baseResponse != null && baseResponse.getMessage() != null
                                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                        showSnackbarFromTop(baseResponse.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteCancelledEventsVertical(ArrayList<BookingList> verticalSavedBookingList) {
        if (verticalSavedBookingList != null && verticalSavedBookingList.size() > 0) {
            for (BookingList bookingList : verticalSavedBookingList) {
                if (bookingList != null && bookingList.getStatus() != 0) {
                    if (bookingList.getStatus() == Utils.STATUS_BOOKING.CANCELLED) {
                        CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
                        calendarSyncResponse.setTitle("StarFinda Coaching Session with " + bookingList.getCoachName() + " for " + bookingList.getSportsName());
                        calendarSyncResponse.setSportName(bookingList.getSportsName());
                        calendarSyncResponse.setSessionDate(bookingList.getSessionDate());
                        calendarSyncResponse.setLearnerName(bookingList.getName());
                        calendarSyncResponse.setFacility(bookingList.getFacilityAddress());
                        calendarSyncResponse.setCoachName(bookingList.getCoachName());
                        CalendarSyncHelper.getInstance().deleteCalendarEvent(mActivity.get(), calendarSyncResponse);
                    }
                }
            }
        }
    }

    private void deleteCancelledEventsHorizontal(ArrayList<BookingList> horizontalSavedBookingList) {
        if (horizontalSavedBookingList != null && horizontalSavedBookingList.size() > 0) {
            for (BookingList bookingList : horizontalSavedBookingList) {
                if (bookingList != null && bookingList.getStatus() != 0) {
                    if (bookingList.getStatus() == Utils.STATUS_BOOKING.CANCELLED) {
                        CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
                        calendarSyncResponse.setTitle("StarFinda Coaching Session with " + bookingList.getCoachName() + " for " + bookingList.getSportsName());
                        calendarSyncResponse.setSportName(bookingList.getSportsName());
                        calendarSyncResponse.setSessionDate(bookingList.getSessionDate());
                        calendarSyncResponse.setLearnerName(bookingList.getName());
                        calendarSyncResponse.setFacility(bookingList.getFacilityAddress());
                        calendarSyncResponse.setCoachName(bookingList.getCoachName());
                        CalendarSyncHelper.getInstance().deleteCalendarEvent(mActivity.get(), calendarSyncResponse);
                    }
                }
            }
        }
    }

    @Override
    public void onListItemClicked(int position) {
        if (Utils.isNetworkAvailable()) {
            if (verticalSavedBookingList != null && !verticalSavedBookingList.isEmpty()) {
                BookingList bookingList = verticalSavedBookingList.get(position);
                Intent i = new Intent(getActivity(), SessionCoachDetails.class);
                i.putExtra(getString(R.string.booking_id_extra), bookingList.getSessionId());
                startActivity(i);
            }
        } else {
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    @Override
    public void onHorizontalListItemClicked(int position) {
        if (Utils.isNetworkAvailable()) {
            if (horizontalSavedBookingList != null && !horizontalSavedBookingList.isEmpty()) {
                BookingList bookingList = horizontalSavedBookingList.get(position);
                Intent i = new Intent(getActivity(), SessionCoachDetails.class);
                i.putExtra(getString(R.string.booking_id_extra), bookingList.getSessionId());
                startActivity(i);
            }
        } else {
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void showEmptyListError(boolean showError, boolean showInternetError) {
        try {
            if (showError) {
                mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                mBinding.tvNoSessions.setVisibility(View.GONE);
                mBinding.rvSessions.setVisibility(View.GONE);
                if (showInternetError) {
                    mBinding.tvSavedBooking.setVisibility(View.GONE);
                    mBinding.tvViewAllLink.setVisibility(View.GONE);
                    mBinding.tvOthers.setVisibility(View.GONE);
                    mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setVisibility(View.VISIBLE);
                    mBinding.tvNoSessions.setText(getResources().getString(R.string.no_internet));
                } else {
                    mBinding.tvSavedBooking.setVisibility(View.GONE);
                    mBinding.tvViewAllLink.setVisibility(View.GONE);
                    mBinding.tvOthers.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setText(getResources().getString(R.string.no_sessions_msg));
                    mBinding.rlNoSessionsParent.setVisibility(View.VISIBLE);
                    mBinding.btnSearchCoach.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundleCoach = new Bundle();
                            bundleCoach.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, true);
                            Navigator.getInstance().navigateToActivityWithData(getActivity(), SearchCoachActivity.class, bundleCoach);
                        }
                    });
                    mBinding.btnSearchSpeaker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            showSnackbarFromTop("Coming Soon");

                            Bundle bundleSpeaker = new Bundle();
                            bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                            bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
                            bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
                            //Navigator.getInstance().navigateToActivityWithData(getActivity(), SearchSpeakerTypeActivity.class, bundleSpeaker);
                            Navigator.getInstance().navigateToActivityWithData(getActivity(), SelectEventTypeActivity.class, bundleSpeaker);
                            //Navigator.getInstance().navigateToActivityWithData(getActivity(), SelectSpeakerSportsActivity.class, bundleSpeaker);
                        }
                    });
                }
            } else {
                mBinding.tvNoSessions.setVisibility(View.GONE);
                mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                mBinding.rvSessions.setVisibility(View.VISIBLE);
//            mBinding.tvSavedBooking.setVisibility(View.VISIBLE);
//            mBinding.tvViewAllLink.setVisibility(View.VISIBLE);
                mBinding.tvOthers.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmptyHorizontalListError(boolean showError) {
        if (showError) {
            mBinding.tvSavedBooking.setVisibility(View.GONE);
            mBinding.tvViewAllLink.setVisibility(View.GONE);
            mBinding.rvHorizontalSaved.setVisibility(View.GONE);
        } else {
            mBinding.tvSavedBooking.setVisibility(View.VISIBLE);
            mBinding.tvViewAllLink.setVisibility(View.VISIBLE);
            mBinding.rvHorizontalSaved.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList() {
        try {
            if (!Utils.isNetworkAvailable()) {
                if (mBinding.swipeRefreshLayout.isRefreshing()) {
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                }
                showSnackbarFromTop(getResources().getString(R.string.no_internet));
                showEmptyListError(true, true);
                showEmptyHorizontalListError(true);
                return;
            }
            pageNo = 1;
            isListLoading = false;
            listCount = 0;
            perPageRecords = 0;
//        if(sessionList != null && !sessionList.isEmpty()) {
//            sessionList.clear();
//        }
            if (Utils.isNetworkAvailable()) {
                isLoadingCombinedList = true;
                horizontalPageNo = 1;
                isHorizontalListLoading = false;
                horizontalListCount = 0;
                horizontalPerPageRecords = 0;
                getSessionsList(pageNo);
            } else {
                showEmptyListError(true, true);
                showEmptyHorizontalListError(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshHorizontalList() {
        try {
            if (!Utils.isNetworkAvailable()) {
                if (mBinding.swipeRefreshLayout.isRefreshing()) {
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                }
                showSnackbarFromTop(getResources().getString(R.string.no_internet));
                showEmptyHorizontalListError(true);
                return;
            }
            horizontalPageNo = 1;
            isHorizontalListLoading = false;
            horizontalListCount = 0;
            horizontalPerPageRecords = 0;
//        if (Utils.isNetworkAvailable()) {
//            getHorizontalSavedSessionsList(horizontalPageNo);
//        } else {
//            showEmptyHorizontalListError(true);
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Utils.isNetworkAvailable()) {
                        getBookingEventListToCalendar();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.permission_not_granted_for_calendar), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void refreshEventsEntryToCalendar() {
        try {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR)
                    != PackageManager.PERMISSION_GRANTED) {
                askCalendarPermission();
            } else {
                if (Utils.isNetworkAvailable()) {
                    getBookingEventListToCalendar();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void getBookingEventListToCalendar() {
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCalendarSyncData().enqueue(new BaseCallback<CalendarSyncBaseResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(CalendarSyncBaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().size() > 0) {
                        //CalendarSyncHelper.getInstance().deleteAllCalendarEvents(getActivity());
                        CalendarSyncHelper.getInstance().updateCalendarEventList(getActivity(), response.getResult());
                    }
                }
            }

            @Override
            public void onFail(Call<CalendarSyncBaseResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
