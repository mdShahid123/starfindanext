package com.sportsstars.ui.learner.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentAboutLearnerBinding;
import com.sportsstars.databinding.ItemProfileLearnerBinding;
import com.sportsstars.model.SportIPlay;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.util.Constants;
import com.sportsstars.util.StringUtils;
import com.sportsstars.widget.CustomTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by atul on 01/05/18.
 * To inject activity reference.
 */

public class AboutFragment extends BaseFragment {

    private List<SportIPlay> mSportIPlays;
    private FragmentAboutLearnerBinding mBinding;
//    private boolean isFromSessionDetails;
//    private String learnerNameForDisplay;

    public static AboutFragment newInstance(ArrayList<SportIPlay> sportIPlays,
                                            boolean fromSessionDetails, String learnerName) {
        AboutFragment aboutFragment = new AboutFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.BundleKey.DATA, sportIPlays);
        bundle.putBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, fromSessionDetails);
        bundle.putString(Constants.BundleKey.LEARNER_NAME, learnerName);
        aboutFragment.setArguments(bundle);
        return aboutFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_learner, container, false);
        initView();
        return mBinding.getRoot();
    }

    private void initView() {
        if (getArguments() != null) {

            boolean isFromSessionDetails = getArguments().getBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, false);
            String learnerNameForDisplay = getArguments().getString(Constants.BundleKey.LEARNER_NAME, "");
            mSportIPlays = getArguments().getParcelableArrayList(Constants.BundleKey.DATA);

            setSportsPlayText(isFromSessionDetails, learnerNameForDisplay);

            if (mSportIPlays != null) {
                setSports();
                for (SportIPlay iPlay : mSportIPlays) {
                    ItemProfileLearnerBinding player = DataBindingUtil.inflate(getLayoutInflater(), R.layout.item_profile_learner, null, false);
                    ItemProfileLearnerBinding team = DataBindingUtil.inflate(getLayoutInflater(), R.layout.item_profile_learner, null, false);
                    ItemProfileLearnerBinding club = DataBindingUtil.inflate(getLayoutInflater(), R.layout.item_profile_learner, null, false);

                    if (player != null && team != null && club != null) {
                        player.setSportName(getString(R.string.txt_fav) + iPlay.getName() + getString(R.string.txt_plr));
                        player.setName(iPlay.getFavPlayer());
                        team.setSportName(getString(R.string.txt_fav) + iPlay.getName() + getString(R.string.txt_team));
                        team.setName(iPlay.getTeamName());
                        club.setSportName(getString(R.string.clubs_played_for) + " " + iPlay.getName());
                        club.setName(iPlay.getClubPlayedFor());
                        mBinding.llSubSection.addView(player.getRoot());
                        mBinding.llSubSection.addView(team.getRoot());
                        mBinding.llSubSection.addView(club.getRoot());
                    }
                }
            }
        }
    }

    private void setSportsPlayText(boolean isFromSessionDetails, String learnerNameForDisplay) {
        String textToDisplay = "";
        if(isFromSessionDetails) {
            if(mSportIPlays != null && mSportIPlays.size() > 0) {
                if(mSportIPlays.size() > 1) {
                    textToDisplay = "Sports " + StringUtils.toProperCase(learnerNameForDisplay)  + " Plays";
                } else {
                    textToDisplay = "Sport " + StringUtils.toProperCase(learnerNameForDisplay) + " Plays";
                }
            }
        } else {
            if(mSportIPlays != null && mSportIPlays.size() > 0) {
                if(mSportIPlays.size() > 1) {
                    textToDisplay = getResources().getString(R.string.txt_sip_plural);
                } else {
                    textToDisplay = getResources().getString(R.string.txt_sip);
                }
            }
        }
        mBinding.tvSportsIPlay.setText(textToDisplay);
    }

    private void setSports() {
        String name = "";
        for (SportIPlay iPlay : mSportIPlays) {
            if (getContext() != null) {
                CustomTextView customTextView = new CustomTextView(getContext());
                customTextView.setCustomFont(getContext(), getString(R.string.font_anr));
                customTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_session_80));
                customTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                customTextView.setPadding(0, 6, 0, 0);
                name = iPlay.getName() + (iPlay.getSkillLevel() == Constants.SKILL_SET.BEGINNER ? getString(R.string.txt_beg) : iPlay.getSkillLevel() == Constants.SKILL_SET.INTERMEDIATE ? getString(R.string.txt_int) : getString(R.string.txt_per));
                customTextView.setText(name);
                mBinding.llSports.addView(customTextView);
            }
        }
    }

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }
}
