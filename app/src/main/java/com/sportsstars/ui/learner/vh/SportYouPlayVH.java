package com.sportsstars.ui.learner.vh;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemLearnerSypBinding;
import com.sportsstars.model.Team;
import com.sportsstars.network.request.auth.learner.SportYouPlay;
import com.sportsstars.ui.coach.AddTeamActivity;
import com.sportsstars.ui.search.SearchCoachActivityNext;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;

import de.measite.minidns.record.A;

/**
 * Created by atul on 30/04/18.
 * To inject activity reference.
 */

public class SportYouPlayVH extends RecyclerView.ViewHolder {

    private ItemLearnerSypBinding mBinding;
    private IRemoveYouSport iRemoveYouSport;

    public SportYouPlayVH(ItemLearnerSypBinding binding, IRemoveYouSport removeYouSport) {
        super(binding.getRoot());
        mBinding = binding;
        iRemoveYouSport = removeYouSport;
    }

    public void bindData(final SportYouPlay sportYouPlay, final int index) {
        mBinding.setIndex(index);
        mBinding.setSportUPlay(sportYouPlay);
        mBinding.tvSport.setText(Utils.getNumericText(String.valueOf(index)));
        sportYouPlay.setSkillSet(sportYouPlay.getSkillSet() == 0 ? Constants.SKILL_SET.BEGINNER : sportYouPlay.getSkillSet());
        setSkillSet(sportYouPlay.getSkillSet());
        mBinding.tvRemoveSyp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iRemoveYouSport.inRemove(index);
            }
        });
        mBinding.etSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.EXTRA_SPORTS_LIST, sportYouPlay.getSports());
                bundle.putBoolean(Constants.FROM_LEARNER_PROFILE, true);
                bundle.putInt(Constants.BundleKey.INDEX, index);
                Navigator.getInstance().navigateToActivityForResultWithData((Activity) mBinding.getRoot().getContext(), SearchCoachActivityNext.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST);
            }
        });
        mBinding.etFavTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sportName = mBinding.etSport.getText().toString().trim();
                if(sportName != null && !TextUtils.isEmpty(sportName)) {
                    Bundle b = new Bundle();
                    b.putInt(Constants.BundleKey.INDEX, index);
                    b.putInt(Constants.BundleKey.ID, sportYouPlay.getSports().getId());
                    b.putBoolean(Constants.FROM_LEARNER_PROFILE, true);

                    if(sportYouPlay != null && sportYouPlay.getTeamsLis() != null) {
                        if(sportYouPlay.getTeamsLis().size() > 0) {
                            if(sportYouPlay.getTeamsLis().get(0).getId() == 0
                                    && TextUtils.isEmpty(sportYouPlay.getTeamsLis().get(0).getName())) {
                                sportYouPlay.getTeamsLis().clear();
                                ArrayList<Team> teamsList = new ArrayList<>();
                                Team team = new Team();
                                team.setId(sportYouPlay.getFavouriteTeam());
                                team.setName(sportYouPlay.getFavouriteTeamName());
                                team.setIsSelected(true);
                                teamsList.add(team);
                                sportYouPlay.setTeamsLis(teamsList);
                                Log.d("learner_log", "sportYouPlay.getTeamsLis() > 0 but " +
                                        "sportYouPlay.getTeamsLis().get(0).getId() == 0 -- size : "
                                        + sportYouPlay.getTeamsLis().size());
                            } else {
                                Log.d("learner_log", "sportYouPlay.getTeamsLis() > 0 but " +
                                        "sportYouPlay.getTeamsLis().get(0).getId() != 0 -- size : "
                                        + sportYouPlay.getTeamsLis().size());
                            }
                        } else {
                            ArrayList<Team> teamsList = new ArrayList<>();
                            Team team = new Team();
                            team.setId(sportYouPlay.getFavouriteTeam());
                            team.setName(sportYouPlay.getFavouriteTeamName());
                            team.setIsSelected(true);
                            teamsList.add(team);
                            sportYouPlay.setTeamsLis(teamsList);
                            Log.d("learner_log", "sportYouPlay.getTeamsLis() is empty");
                        }
                    } else {
                        ArrayList<Team> teamsList = new ArrayList<>();
                        Team team = new Team();
                        team.setId(sportYouPlay.getFavouriteTeam());
                        team.setName(sportYouPlay.getFavouriteTeamName());
                        team.setIsSelected(true);
                        teamsList.add(team);
                        sportYouPlay.setTeamsLis(teamsList);
                        Log.d("learner_log", "sportYouPlay.getTeamsLis() is null");
                    }
                    b.putParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST, sportYouPlay.getTeamsLis());
                    Navigator.getInstance().navigateToActivityForResultWithData((Activity) mBinding.getRoot().getContext(), AddTeamActivity.class, b, Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS);
                } else {
                    try {
                        Toast.makeText((Activity) mBinding.getRoot().getContext(),
                                mBinding.getRoot().getContext().getResources().getString(R.string.please_select_sport),
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        mBinding.relBeginner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sportYouPlay.setSkillSet(Constants.SKILL_SET.BEGINNER);
                setSkillSet(Constants.SKILL_SET.BEGINNER);
            }
        });
        mBinding.relIntermediate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sportYouPlay.setSkillSet(Constants.SKILL_SET.INTERMEDIATE);
                setSkillSet(Constants.SKILL_SET.INTERMEDIATE);
            }
        });
        mBinding.relExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sportYouPlay.setSkillSet(Constants.SKILL_SET.EXPERT);
                setSkillSet(Constants.SKILL_SET.EXPERT);
            }
        });
    }

    private void setSkillSet(int skillSet) {
        switch (skillSet) {
            case Constants.SKILL_SET.BEGINNER:
                setSkillSet(mBinding.relBeginner, mBinding.tvBeginner);
                break;
            case Constants.SKILL_SET.INTERMEDIATE:
                setSkillSet(mBinding.relIntermediate, mBinding.tvIntermediate);
                break;
            case Constants.SKILL_SET.EXPERT:
                setSkillSet(mBinding.relExpert, mBinding.tvExpert);
                break;
        }
    }

    private void setSkillSet(RelativeLayout relSelected, TextView tvSelected) {
        Context context = mBinding.getRoot().getContext();
        mBinding.relBeginner.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_grey));
        mBinding.tvBeginner.setTextColor(ContextCompat.getColor(context, R.color.black_25));

        mBinding.relIntermediate.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_grey));
        mBinding.tvIntermediate.setTextColor(ContextCompat.getColor(context, R.color.black_25));

        mBinding.relExpert.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_grey));
        mBinding.tvExpert.setTextColor(ContextCompat.getColor(context, R.color.black_25));

        relSelected.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_rounded_border_green_bg_grey));
        tvSelected.setTextColor(ContextCompat.getColor(context, R.color.green_gender));

    }

    public interface IRemoveYouSport {
        void inRemove(int index);
    }
}
