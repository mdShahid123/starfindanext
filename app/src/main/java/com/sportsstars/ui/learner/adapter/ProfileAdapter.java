package com.sportsstars.ui.learner.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemLearnerProfileBinding;
import com.sportsstars.databinding.ItemLearnerSypBinding;
import com.sportsstars.databinding.ItemSypLayoutBinding;
import com.sportsstars.model.Sports;
import com.sportsstars.model.Team;
import com.sportsstars.network.request.auth.learner.ProfileRequest;
import com.sportsstars.network.request.auth.learner.SportYouPlay;
import com.sportsstars.ui.learner.vh.ProfileVH;
import com.sportsstars.ui.learner.vh.SportYouPlayVH;
import com.sportsstars.ui.learner.vh.SypVH;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by atul on 30/04/18.
 * To inject activity reference.
 */

public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SportYouPlayVH.IRemoveYouSport {

    private Bitmap mBitmap;
    private ProfileRequest mProfileRequest;
    private List<SportYouPlay> mSportYouPlays;
    private IAddYouSport mListener;
    private Activity mActivity;
    private String mSuburb;
    private ProfileVH mProfileVH;

    public ProfileAdapter(ProfileRequest profileRequest, IAddYouSport listener, Activity activity) {
        mListener = listener;
        mProfileRequest = profileRequest;
        mSportYouPlays = mProfileRequest.getSportsView();
        mActivity = activity;
        setViewType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == Constants.VIEW_TYPE.VIEW_HEADER) {
            mProfileVH = new ProfileVH((ItemLearnerProfileBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_learner_profile, parent, false), mActivity);
            viewHolder = mProfileVH;
        } else if (viewType == Constants.VIEW_TYPE.VIEW_ITEM) {
            viewHolder = new SportYouPlayVH((ItemLearnerSypBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_learner_syp, parent, false), this);
        } else {
            viewHolder = new SypVH((ItemSypLayoutBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_syp_layout, parent, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProfileVH) {
            ((ProfileVH) holder).bindData(mProfileRequest, mBitmap);
        } else if (holder instanceof SportYouPlayVH) {
            ((SportYouPlayVH) holder).bindData(mSportYouPlays.get(holder.getAdapterPosition()), holder.getAdapterPosition());
        } else if (holder instanceof SypVH) {
            ((SypVH) holder).getBinding().tvAas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.inAddSport();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mSportYouPlays.get(position).getViewType() == Constants.VIEW_TYPE.VIEW_HEADER) {
            return Constants.VIEW_TYPE.VIEW_HEADER;
        } else if (mSportYouPlays.get(position).getViewType() == Constants.VIEW_TYPE.VIEW_ITEM) {
            return Constants.VIEW_TYPE.VIEW_ITEM;
        } else {
            return Constants.VIEW_TYPE.VIEW_FOOTER;
        }
    }

    public void addAnotherSport() {
        SportYouPlay youPlay = new SportYouPlay();
        youPlay.setViewType(Constants.VIEW_TYPE.VIEW_ITEM);
        mSportYouPlays.add(mSportYouPlays.size() - 1, youPlay);
        notifyItemInserted(mSportYouPlays.size() - 1);
    }

    private void setViewType() {
        if (mSportYouPlays.size() == 0) {
            SportYouPlay youPlay = new SportYouPlay();
            youPlay.setViewType(Constants.VIEW_TYPE.VIEW_HEADER);
            mSportYouPlays.add(youPlay);
            youPlay = new SportYouPlay();
            youPlay.setViewType(Constants.VIEW_TYPE.VIEW_ITEM);
            mSportYouPlays.add(youPlay);
            youPlay = new SportYouPlay();
            youPlay.setViewType(Constants.VIEW_TYPE.VIEW_FOOTER);
            mSportYouPlays.add(youPlay);
            return;
        }
        SportYouPlay youPlay;
        for (int index = 0; index < mSportYouPlays.size(); index++) {
            if (index == 0 && mSportYouPlays.get(index).getViewType() != Constants.VIEW_TYPE.VIEW_HEADER) {
                youPlay = new SportYouPlay();
                youPlay.setViewType(Constants.VIEW_TYPE.VIEW_HEADER);
                mSportYouPlays.add(index, youPlay);
                continue;
            }
            if (index == (mSportYouPlays.size() - 1) && mSportYouPlays.get(index).getViewType() != Constants.VIEW_TYPE.VIEW_FOOTER) {
                mSportYouPlays.get(index).setViewType(Constants.VIEW_TYPE.VIEW_ITEM);
                youPlay = new SportYouPlay();
                youPlay.setViewType(Constants.VIEW_TYPE.VIEW_FOOTER);
                mSportYouPlays.add(mSportYouPlays.size(), youPlay);
                break;
            }
            mSportYouPlays.get(index).setViewType(Constants.VIEW_TYPE.VIEW_ITEM);
        }
    }

    public void updateSyp(int index, Sports sport) {
        SportYouPlay youPlay = mProfileRequest.getSportsView().get(index);
        youPlay.setFirstSport(sport.getId());
        youPlay.setFirstSportName(sport.getName());
        youPlay.setSports(sport);
        notifyItemChanged(index);

        updateTeam(index, getEmptyTeamList());
    }

    private ArrayList<Team> getEmptyTeamList() {
        ArrayList<Team> emptyTeamsList = new ArrayList<>();
        Team emptyTeam = new Team();
        emptyTeam.setId(0);
        emptyTeam.setName("");
        emptyTeam.setIsSelected(false);
        emptyTeamsList.add(emptyTeam);

        return emptyTeamsList;
    }

    public void updateTeam(int index, ArrayList<Team> teamsList) {
        SportYouPlay youPlay = mProfileRequest.getSportsView().get(index);
        youPlay.setFavouriteTeam(teamsList.get(0).getId());
        youPlay.setFavouriteTeamName(teamsList.get(0).getName());
        youPlay.setTeamsLis(teamsList);
        youPlay.setFavouriteTeamsName(Utils.getTeamsName(teamsList));
        notifyItemChanged(index);
    }

    public void updateProfilePic(Bitmap bitmap) {
        mBitmap = bitmap;
        notifyItemChanged(0);
    }

    @Override
    public int getItemCount() {
        return mSportYouPlays.size();
    }

    @Override
    public void inRemove(int index) {
        if (index >= mSportYouPlays.size() - 1)
            return;
        mSportYouPlays.remove(index);
        notifyItemRemoved(index);
    }

    public interface IAddYouSport {
        void inAddSport();
    }

    public void updateProfileRequestForSuburbLocation(String mSuburb) {
        mProfileRequest.setPostCode(mSuburb);
        notifyDataSetChanged();
    }

}
