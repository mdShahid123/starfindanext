package com.sportsstars.ui.learner.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sportsstars.MVVM.entities.mysession.BookingList;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentMyEventsListItemBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.ui.session.SessionDetailsLearnerActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyBookingAdapter extends RecyclerView.Adapter<MyBookingAdapter.MyViewHolder> {

    private List<BookingList> mBookingList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ListItemClickListener listItemClickListener;

    public MyBookingAdapter(Activity mActivity, List<BookingList> mBookingList, ListItemClickListener listItemClickListener) {
        this.mBookingList = mBookingList;
        this.mActivity = mActivity;
        this.listItemClickListener = listItemClickListener;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public MyBookingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FragmentMyEventsListItemBinding searchListBinding = DataBindingUtil.inflate(mInflator, R.layout.fragment_my_events_list_item, parent, false);

        return new MyBookingAdapter.MyViewHolder(searchListBinding);
    }

    @Override
    public void onBindViewHolder(MyBookingAdapter.MyViewHolder holder, int position) {
        if (mBookingList != null && mBookingList.size() > 0) {
            holder.bindData(mBookingList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mBookingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        FragmentMyEventsListItemBinding myEventsListItemBinding;

        public MyViewHolder(FragmentMyEventsListItemBinding fragmentMyEventsListItemBinding) {
            super(fragmentMyEventsListItemBinding.getRoot());
            this.myEventsListItemBinding = fragmentMyEventsListItemBinding;
        }

        public FragmentMyEventsListItemBinding getBinding() {
            return myEventsListItemBinding;
        }

        public void bindData(BookingList bookingList) {
            if(bookingList != null) {
                myEventsListItemBinding.setBookingList(bookingList);
                String utcDateTime = bookingList.getSessionDate();
                String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                        DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
                Log.d("mylog", "MyBookingAdapter - utcDateTime : " + utcDateTime
                        + " :: localDateTime : " + localDateTime);
                myEventsListItemBinding.tvDate.setText(Utils.getFormattedDateTime(localDateTime));
                myEventsListItemBinding.tvTime.setText(Utils.parseTimeForDDMM(localDateTime));
                Picasso.with(mActivity).load(bookingList.getSessionSportImage()).placeholder(R.drawable.cricket_dummy).error(R.drawable.cricket_dummy).into(myEventsListItemBinding.imvSportsImage);
                if(bookingList != null && !TextUtils.isEmpty(bookingList.getIcon())) {
                    Picasso.with(mActivity)
                            .load(bookingList.getIcon())
                            .placeholder(R.drawable.ic_cricket_green)
                            .error(R.drawable.ic_cricket_green)
                            .into(myEventsListItemBinding.imvSports);
                }
                myEventsListItemBinding.rootView.setOnClickListener((View v) -> {
                    if(Utils.isNetworkAvailable()) {
                        if(mActivity != null && bookingList != null) {
                            Intent i = new Intent(mActivity, SessionDetailsLearnerActivity.class);
                            i.putExtra(mActivity.getString(R.string.booking_id_extra), bookingList.getSessionId());
                            mActivity.startActivity(i);
                        }
                    } else {
                        if(mActivity != null) {
                            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.no_internet),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

                if(bookingList.getCoachType() == Constants.COACH_TYPE.STAR) {
                    myEventsListItemBinding.imageView3.setImageResource(R.drawable.ic_star);
                } else {
                    myEventsListItemBinding.imageView3.setImageResource(R.drawable.ic_private_coach);
                }
            }
        }

    }

}
