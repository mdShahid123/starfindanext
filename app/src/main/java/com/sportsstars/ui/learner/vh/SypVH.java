package com.sportsstars.ui.learner.vh;

import android.support.v7.widget.RecyclerView;

import com.sportsstars.databinding.ItemSypLayoutBinding;

/**
 * Created by atul on 30/04/18.
 * To inject activity reference.
 */

public class SypVH extends RecyclerView.ViewHolder {

    ItemSypLayoutBinding mBinding;

    public SypVH(ItemSypLayoutBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public ItemSypLayoutBinding getBinding() {
        return mBinding;
    }
}
