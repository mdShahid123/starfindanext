package com.sportsstars.ui.learner.events;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.appster.chatlib.constants.AppConstants;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.chat.utils.DateTimeUtils;
import com.sportsstars.databinding.ActivityLearnerEventDetailsBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.interfaces.SpeakerSelectedListener;
import com.sportsstars.model.UserModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.PayForBookingRequest;
import com.sportsstars.network.request.auth.speaker.CancelBookingStatusRequest;
import com.sportsstars.network.response.speaker.EventDetails;
import com.sportsstars.network.response.speaker.EventDetailsResponse;
import com.sportsstars.network.response.speaker.GuestSpeakerSessionRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.learner.adapter.SpeakerEventInfoAdapter;
import com.sportsstars.ui.payment.ManageCardListActivity;
import com.sportsstars.ui.speaker.search.SpeakerProfileActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class LearnerEventDetailsActivity extends BaseActivity implements View.OnClickListener, ListItemClickListener,
        SpeakerSelectedListener {

    private ActivityLearnerEventDetailsBinding mBinding;
    private int sessionId;
    private EventDetails mEventDetails;
    private SpeakerEventInfoAdapter mSpeakerEventInfoAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<GuestSpeakerSessionRequest> mGuestSpeakerSessionList = new ArrayList<>();
    private double mTotalPrice;
    private ArrayList<Integer> mSelectedSpeakerIdList = new ArrayList<>();

    private static final String TIME_FORMAT = "REMAINING TIME - %02dH:%02dM:%02dS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_learner_event_details);

        getIntentData();
        initViews();
    }

    private void getIntentData() {
        if(getIntent().hasExtra(Constants.BundleKey.SESSION_ID_KEY)) {
            sessionId = getIntent().getIntExtra(Constants.BundleKey.SESSION_ID_KEY, 0);
        }
        //showToast("session id : " + sessionId);
    }

    private void initViews() {
        mBinding.buttonBooking.setOnClickListener(this);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mSpeakerEventInfoAdapter = new SpeakerEventInfoAdapter(LearnerEventDetailsActivity.this,
                mGuestSpeakerSessionList, this, this, -1);
        mBinding.rvSpeakers.setAdapter(mSpeakerEventInfoAdapter);
        mBinding.rvSpeakers.setLayoutManager(mLayoutManager);

        if(Utils.isNetworkAvailable()) {
            if(sessionId == 0) {
                return;
            }
            getEventDetails(sessionId);
        } else {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void getEventDetails(int sessionId) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.getEventDetails(sessionId).enqueue(new BaseCallback<EventDetailsResponse>(LearnerEventDetailsActivity.this) {
            @Override
            public void onSuccess(EventDetailsResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        try {
//                            if(response.getMessage() != null
//                                    && !TextUtils.isEmpty(response.getMessage())) {
//                                showSnackbarFromTop(LearnerEventDetailsActivity.this, response.getMessage());
//                            }
                            mEventDetails = response.getResult();
                            updateUI();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(LearnerEventDetailsActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<EventDetailsResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void updateUI() {
        try {
            if(mEventDetails == null) {
                return;
            }
            mBinding.llDeclineAccept.setVisibility(View.VISIBLE);

            displayAthletesProfilePicture();

//            if(mEventDetails.getEventUserInfo() != null) {
//                if (mEventDetails.getEventUserInfo().getProfileImage() != null
//                        && !TextUtils.isEmpty(mEventDetails.getEventUserInfo().getProfileImage())) {
//                    Picasso.with(this)
//                            .load(mEventDetails.getEventUserInfo().getProfileImage())
//                            .placeholder(R.drawable.ic_user_circle)
//                            .error(R.drawable.ic_user_circle)
//                            .into(mBinding.imageProfile1);
//
//                    Picasso.with(this)
//                            .load(mEventDetails.getEventUserInfo().getProfileImage())
//                            .placeholder(R.drawable.ic_user_circle)
//                            .error(R.drawable.ic_user_circle)
//                            .into(mBinding.ivBackgroundImage);
//                }
//
////                if(mEventDetails.getEventUserInfo().getName() != null
////                        && !TextUtils.isEmpty(mEventDetails.getEventUserInfo().getName())) {
////                    mBinding.tvName.setText(mEventDetails.getEventUserInfo().getName());
////                }
//            }

            mBinding.tvName.setText(getResources().getString(R.string.athletes));

            String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
            if(upcomingText.startsWith("AWAITING")) {
                upcomingText = upcomingText.replace("\n", " ");
            }
            mBinding.tvUpcoming.setText(upcomingText);
            mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(this, mEventDetails.getStatus()));

            if(mEventDetails.getAddress() != null && !TextUtils.isEmpty(mEventDetails.getAddress())) {
                mBinding.tvLocation.setText(mEventDetails.getAddress());
            }

            String utcDateTime = mEventDetails.getSessionStartTime();
            String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
            Log.d("mylog", "SpeakerEventDetails - utcDateTime : " + utcDateTime
                    + " :: localDateTime : " + localDateTime);
            mBinding.time.setText(Utils.parseTimeForDDMM(localDateTime));
            mBinding.date.setText(Utils.parseTimeForDDMMYY(localDateTime));

            if (mEventDetails.getEventLogo() != null
                    && !TextUtils.isEmpty(mEventDetails.getEventLogo())) {
                Picasso.with(this)
                        .load(mEventDetails.getEventLogo())
                        .placeholder(R.drawable.ic_birthday)
                        .error(R.drawable.ic_birthday)
                        .into(mBinding.ivEventLogo);
            }

            if(mEventDetails.getEventName() != null && !TextUtils.isEmpty(mEventDetails.getEventName())) {
                mBinding.tvEventName.setText(mEventDetails.getEventName());
            }

            if(mEventDetails.getNoOfAttendees() != null && !TextUtils.isEmpty(mEventDetails.getNoOfAttendees())) {
                mBinding.tvNoOfAttendees.setText(Utils.getNoOfAttendeesText(mEventDetails.getNoOfAttendees()));
            }

            if(mEventDetails.getDuration() != 0) {
                mBinding.tvDuration.setText(getResources().getString(R.string.duration_with_mins, mEventDetails.getDuration()));
            }

            int priceInt = (int) mEventDetails.getTotalBudget();
            mBinding.price.setText("$"+priceInt);

            if(mGuestSpeakerSessionList != null && mGuestSpeakerSessionList.size() > 0) {
                mGuestSpeakerSessionList.clear();
            }
            mGuestSpeakerSessionList.addAll(mEventDetails.getGuestSpeakerSessionRequest());
            if(mSpeakerEventInfoAdapter != null) {
                if(mEventDetails != null) {
                    mSpeakerEventInfoAdapter.setEventStatus(mEventDetails.getStatus());
                }
                mSpeakerEventInfoAdapter.notifyDataSetChanged();
            }

            if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.CANCELLED
                    || mEventDetails.getStatus() == Utils.STATUS_BOOKING.COMPLETED
                    || mEventDetails.getStatus() == Utils.STATUS_BOOKING.ONGOING) {
                mBinding.tvRemainingTime.setVisibility(View.GONE);
                mBinding.llDeclineAccept.setVisibility(View.GONE);
                mBinding.relTotalPrice.setVisibility(View.GONE);
                mBinding.buttonBooking.setText(getResources().getString(R.string.book));
            } else if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.UPCOMING) {
                mBinding.tvRemainingTime.setVisibility(View.GONE);
                mBinding.llDeclineAccept.setVisibility(View.VISIBLE);
                mBinding.relTotalPrice.setVisibility(View.GONE);
                mBinding.buttonBooking.setText(getResources().getString(R.string.cancel));
            }

            if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.PAYMENT_PENDING) {
                mBinding.tvRemainingTime.setVisibility(View.GONE);
            }

            if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
                startCountDownTimer();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void startCountDownTimer() {
        try {
            String createdAt = "";
            String currentTime = "";
            long millisecondsToFinish;
            if(mEventDetails != null && !TextUtils.isEmpty(mEventDetails.getCreatedAt())
                    && !TextUtils.isEmpty(mEventDetails.getCurrentTime())) {
                createdAt = mEventDetails.getCreatedAt();
                currentTime = mEventDetails.getCurrentTime();

                Date dateCreated = DateTimeUtils.getDateFromString(createdAt, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
                Date dateCurrent = DateTimeUtils.getDateFromString(currentTime, DateFormatUtils.SERVER_DATE_FORMAT_TIME);

                Calendar calendarCreated = Calendar.getInstance();
                calendarCreated.setTime(dateCreated);
                //calendarCreated.add(Calendar.HOUR, Constants.HOURS_TO_ADD_IN_TIMER);
                calendarCreated.add(Calendar.MINUTE, mEventDetails.getTimer());

                Calendar calendarCurrent = Calendar.getInstance();
                calendarCurrent.setTime(dateCurrent);

                // millis for 2 days i.e 48 hours - 172800000

                millisecondsToFinish = calendarCreated.getTimeInMillis() - calendarCurrent.getTimeInMillis();

                //millisecondsToFinish = 1000*60* mEventDetails.getTimer();
                if (millisecondsToFinish > Constants.ONE_SECOND_INTERVAL) {
                    new CountDownTimer(millisecondsToFinish, Constants.ONE_SECOND_INTERVAL) { // adjust the milli seconds here

                        @Override
                        public void onTick(long millisUntilFinished) {

                            mBinding.tvRemainingTime.setText(String.format(TIME_FORMAT,
                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

//                        mBinding.tvRemainingTime.setText(formatMilliSecondsToTime(millisUntilFinished));
                        }

                        @Override
                        public void onFinish() {
                            mBinding.tvRemainingTime.setText("REMAINING TIME - FINISHED");
                            mBinding.tvRemainingTime.setVisibility(View.GONE);
                            mBinding.llDeclineAccept.setVisibility(View.GONE);
                            mBinding.relTotalPrice.setVisibility(View.GONE);

//                            mEventDetails.setStatus(Utils.STATUS_BOOKING.PAYMENT_PENDING);
//                            String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
//                            if(upcomingText.startsWith("AWAITING")) {
//                                upcomingText = upcomingText.replace("\n", " ");
//                            }
//                            mBinding.tvUpcoming.setText(upcomingText);
//                            mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(
//                                    LearnerEventDetailsActivity.this, mEventDetails.getStatus()));

                            if(Utils.isNetworkAvailable()) {
                                if(sessionId == 0) {
                                    return;
                                }
                                getEventDetails(sessionId);
                            }
                        }
                    }.start();
                } else {
                    mBinding.tvRemainingTime.setText("REMAINING TIME - FINISHED");
                    mBinding.tvRemainingTime.setVisibility(View.GONE);
                    mBinding.llDeclineAccept.setVisibility(View.GONE);
                    mBinding.relTotalPrice.setVisibility(View.GONE);

//                    mEventDetails.setStatus(Utils.STATUS_BOOKING.PAYMENT_PENDING);
//                    String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
//                    if(upcomingText.startsWith("AWAITING")) {
//                        upcomingText = upcomingText.replace("\n", " ");
//                    }
//                    mBinding.tvUpcoming.setText(upcomingText);
//                    mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(
//                            LearnerEventDetailsActivity.this, mEventDetails.getStatus()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private String formatMilliSecondsToTime(long milliseconds) {
//        String timeString = "";
//        try {
//            int seconds = (int) (milliseconds / 1000) % 60;
//            int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
//            int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
//            timeString = "REMAINING TIME - " + twoDigitString(hours) + "H:"
//                    + twoDigitString(minutes) + "M:" + twoDigitString(seconds) + "S";
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return timeString;
//    }
//
//    private String twoDigitString(long number) {
//
//        if (number == 0) {
//            return "00";
//        }
//
//        if (number / 10 == 0) {
//            return "0" + number;
//        }
//
//        return String.valueOf(number);
//    }

    private void displayAthletesProfilePicture() {
        if(mEventDetails != null && mEventDetails.getGuestSpeakerSessionRequest() != null
                && mEventDetails.getGuestSpeakerSessionRequest().size() > 0) {

            if(mEventDetails.getGuestSpeakerSessionRequest().get(0).getEventUserInfo() != null
                    && !TextUtils.isEmpty(mEventDetails.getGuestSpeakerSessionRequest()
                    .get(0).getEventUserInfo().getProfileImage())) {
                Picasso.with(this)
                        .load(mEventDetails.getGuestSpeakerSessionRequest()
                                .get(0).getEventUserInfo().getProfileImage())
                        .placeholder(R.drawable.ic_user_circle)
                        .error(R.drawable.ic_user_circle)
                        .into(mBinding.imageProfile1);

                Picasso.with(this)
                        .load(mEventDetails.getGuestSpeakerSessionRequest()
                                .get(0).getEventUserInfo().getProfileImage())
                        .placeholder(R.drawable.ic_user_circle)
                        .error(R.drawable.ic_user_circle)
                        .into(mBinding.ivBackgroundImage);
            }

            if(mEventDetails.getGuestSpeakerSessionRequest().size() > 1) {
                mBinding.imageProfile2.setVisibility(View.VISIBLE);
                if(mEventDetails.getGuestSpeakerSessionRequest().get(1).getEventUserInfo() != null
                        && !TextUtils.isEmpty(mEventDetails.getGuestSpeakerSessionRequest()
                        .get(1).getEventUserInfo().getProfileImage())) {
                    Picasso.with(this)
                            .load(mEventDetails.getGuestSpeakerSessionRequest()
                                    .get(1).getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(mBinding.imageProfile2);

                    Picasso.with(this)
                            .load(mEventDetails.getGuestSpeakerSessionRequest()
                                    .get(1).getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(mBinding.ivBackgroundImage);
                }
            }

            if(mEventDetails.getGuestSpeakerSessionRequest().size() > 2) {
                mBinding.imageProfile3.setVisibility(View.VISIBLE);
                if(mEventDetails.getGuestSpeakerSessionRequest().get(2).getEventUserInfo() != null
                        && !TextUtils.isEmpty(mEventDetails.getGuestSpeakerSessionRequest()
                        .get(2).getEventUserInfo().getProfileImage())) {
                    Picasso.with(this)
                            .load(mEventDetails.getGuestSpeakerSessionRequest()
                                    .get(2).getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(mBinding.imageProfile3);

                    Picasso.with(this)
                            .load(mEventDetails.getGuestSpeakerSessionRequest()
                                    .get(2).getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(mBinding.ivBackgroundImage);
                }
            }

            if(mEventDetails.getGuestSpeakerSessionRequest().size() > 3) {
                mBinding.imageProfile4.setVisibility(View.VISIBLE);
                if(mEventDetails.getGuestSpeakerSessionRequest().get(3).getEventUserInfo() != null
                        && !TextUtils.isEmpty(mEventDetails.getGuestSpeakerSessionRequest()
                        .get(3).getEventUserInfo().getProfileImage())) {
                    Picasso.with(this)
                            .load(mEventDetails.getGuestSpeakerSessionRequest()
                                    .get(3).getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(mBinding.imageProfile4);

                    Picasso.with(this)
                            .load(mEventDetails.getGuestSpeakerSessionRequest()
                                    .get(3).getEventUserInfo().getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(mBinding.ivBackgroundImage);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_booking:
                if(Utils.isNetworkAvailable()) {
                    if(mEventDetails != null) {
                        if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.UPCOMING) {
                            showBookingCancelConfirmationDialog();
                        } else {
                            if(isInputValid()) {
                                //launchAddPaymentScreen();
                                showBookingConfirmationDialog();
                            }
                        }
                    }
                } else {
                    showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private void showBookingCancelConfirmationDialog() {
        Alert.showOkCancelDialog(LearnerEventDetailsActivity.this,
                getResources().getString(R.string.cancel_booking_dialog_title),
                getResources().getString(R.string.cancel_booking_learner_speaker_dialog_message),
                getResources().getString(R.string.no),
                getResources().getString(R.string.yes),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(Utils.isNetworkAvailable()) {
                            cancelBookingStatus();
                        } else {
                            showSnackbarFromTop(LearnerEventDetailsActivity.this,
                                    getResources().getString(R.string.no_internet));
                        }
                    }
                },
                new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {

                    }
                }
        );
    }

    private void cancelBookingStatus() {

        if(sessionId == 0) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this,
                    getResources().getString(R.string.event_is_invalid));
            return;
        }

        CancelBookingStatusRequest cancelBookingStatusRequest = new CancelBookingStatusRequest();
        cancelBookingStatusRequest.setSessionId(sessionId);

        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.cancelBookingStatus(cancelBookingStatusRequest).enqueue(new BaseCallback<BaseResponse>(LearnerEventDetailsActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                        finish();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                    //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                }
                finish();
            }
        });
    }

    private void showBookingConfirmationDialog() {
        Alert.showEventBookingConfirmationDialog(LearnerEventDetailsActivity.this,
                getResources().getString(R.string.title_event_confirmation_dialog),
                getResources().getString(R.string.message_event_confirmation_dialog),
                getResources().getString(R.string.text_event_confirmation_dialog_disclaimer),
                getResources().getString(R.string.text_event_confirmation_dialog_terms),
                getResources().getString(R.string.no_return),
                getResources().getString(R.string.yes_proceed),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        launchAddPaymentScreen();
                    }
                },
                new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {

                    }
                }
        );
    }

    private boolean isInputValid() {
        if(sessionId == 0) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.booking_id_is_invalid));
            return false;
        }
        if(mSelectedSpeakerIdList == null) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.please_select_available_speaker));
            return false;
        }
        if(mSelectedSpeakerIdList.size() == 0) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.please_select_available_speaker));
            return false;
        }
        UserModel userModel = PreferenceUtil.getUserModel();
        if(userModel == null) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.you_are_not_logged_in));
            return false;
        }
        if(userModel.getUserId() == 0) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.you_are_not_logged_in));
            return false;
        }
        if(PreferenceUtil.getCurrentUserRole() != Constants.USER_ROLE.LEARNER) {
            showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.switch_to_learner));
            return false;
        }
        return  true;
    }

    private void launchAddPaymentScreen() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_BOOKING, true);
        bundle.putInt(Constants.BundleKey.BOOKING_ID, sessionId);
        bundle.putDouble(Constants.BundleKey.BOOKING_AMOUNT, mTotalPrice);
        bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
        bundle.putIntegerArrayList(Constants.BundleKey.SPEAKER_IDS_LIST, mSelectedSpeakerIdList);
        Navigator.getInstance().navigateToActivityWithData(LearnerEventDetailsActivity.this, ManageCardListActivity.class, bundle);

        finish();
    }

    private void callBookingApi() {
        PayForBookingRequest payForBookingRequest = new PayForBookingRequest();
        payForBookingRequest.setSessionId(sessionId);
        payForBookingRequest.setPaymentNounce("");
        payForBookingRequest.setAmount(0.0);
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.payForBooking(payForBookingRequest).enqueue(new BaseCallback<BaseResponse>(LearnerEventDetailsActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                        finish();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                    //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                }
                finish();
            }
        });
    }

    @Override
    public void onSpeakerSelected(int position, boolean isSelected) {
        try {
            if(mGuestSpeakerSessionList != null && mGuestSpeakerSessionList.size() > 0) {
                GuestSpeakerSessionRequest guestSpeakerSessionRequest = mGuestSpeakerSessionList.get(position);
                if(guestSpeakerSessionRequest != null) {
                    guestSpeakerSessionRequest.setSelected(isSelected);
                    if(isSelected) {
                        mTotalPrice = mTotalPrice + guestSpeakerSessionRequest.getSessionPrice();
                        if(mSelectedSpeakerIdList != null) {
                            mSelectedSpeakerIdList.add((Integer) guestSpeakerSessionRequest.getSpeakerId());
                            Log.d("booking_log", "mSelectedSpeakerIdList size after adding - " +
                                    " size : " + mSelectedSpeakerIdList.size());
                        }
                    } else {
                        mTotalPrice = mTotalPrice - guestSpeakerSessionRequest.getSessionPrice();
                        if(mSelectedSpeakerIdList != null && mSelectedSpeakerIdList.size() > 0) {
                            mSelectedSpeakerIdList.remove((Integer) guestSpeakerSessionRequest.getSpeakerId());
                            Log.d("booking_log", "mSelectedSpeakerIdList size after removing - " +
                                    " size : " + mSelectedSpeakerIdList.size());
                        }
                    }
                    mBinding.tvTotalPriceValue.setText(getResources().getString(R.string.min_budget_with_dollar,
                            (int) mTotalPrice));
                    if(mSpeakerEventInfoAdapter != null) {
                        if(mEventDetails != null) {
                            mSpeakerEventInfoAdapter.setEventStatus(mEventDetails.getStatus());
                        }
                        mSpeakerEventInfoAdapter.notifyDataSetChanged();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSpeakerSelectedForChat(int position) {
        if(mGuestSpeakerSessionList != null && mGuestSpeakerSessionList.size() > 0) {
            GuestSpeakerSessionRequest guestSpeakerSessionRequest = mGuestSpeakerSessionList.get(position);
            startChat(guestSpeakerSessionRequest);
        }
    }

    @Override
    public void onListItemClicked(int position) {
        if(mGuestSpeakerSessionList != null && mGuestSpeakerSessionList.size() > 0) {
            GuestSpeakerSessionRequest guestSpeakerSessionRequest = mGuestSpeakerSessionList.get(position);
            if(Utils.isNetworkAvailable()) {
                if(guestSpeakerSessionRequest != null) {
                    getSpeakerProfile(guestSpeakerSessionRequest, position);
                }
            } else {
                showSnackbarFromTop(LearnerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
            }
        }
    }

    private void startChat(GuestSpeakerSessionRequest guestSpeakerSessionRequest) {
        if(guestSpeakerSessionRequest != null && guestSpeakerSessionRequest.getEventUserInfo() != null
                && guestSpeakerSessionRequest.getEventUserInfo().getJid() != null
                && !TextUtils.isEmpty(guestSpeakerSessionRequest.getEventUserInfo().getJid())) {
            String userName = guestSpeakerSessionRequest.getEventUserInfo().getJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, guestSpeakerSessionRequest.getEventUserInfo().getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, guestSpeakerSessionRequest.getEventUserInfo().getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, guestSpeakerSessionRequest.getEventUserInfo().getJid());//.replace("246","260"));
//                intent.putExtra(AppConstants.KEY_TO_USER, _currentBookingList.getLearnerJid());
                startActivity(intent);
            } else {
                showToast("Username of this profile not found to initiate chat.");
                //com.appster.chatlib.utils.Utils.showToast(this, "enter friend username");
            }
        }
    }

    private void getSpeakerProfile(GuestSpeakerSessionRequest guestSpeakerSessionRequest, int position) {
        int speakerId = guestSpeakerSessionRequest.getSpeakerId();
        boolean isSelected = guestSpeakerSessionRequest.isSelected();
        Bundle bundleSpeakerProfile = new Bundle();
        bundleSpeakerProfile.putInt(Constants.BundleKey.POSITION, position);
        bundleSpeakerProfile.putInt(Constants.BundleKey.SPEAKER_ID, speakerId);
        bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_SELECTED, isSelected);
//        if(guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING
//                || guestSpeakerSessionRequest.getStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
//            bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_AWAITING_OR_DECLINED, true);
//        } else {
//            bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_AWAITING_OR_DECLINED, false);
//        }

        if(mEventDetails != null) {
            bundleSpeakerProfile.putInt(Constants.BundleKey.EVENT_BOOKING_STATUS, mEventDetails.getStatus());
            bundleSpeakerProfile.putInt(Constants.BundleKey.SPEAKER_STATUS, guestSpeakerSessionRequest.getStatus());
            bundleSpeakerProfile.putInt(Constants.BundleKey.IS_SPEAKER_PAID, guestSpeakerSessionRequest.getIsPaid());
        }
        bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_FROM_SEARCH, false);

        Navigator.getInstance().navigateToActivityForResultWithData(LearnerEventDetailsActivity.this,
                SpeakerProfileActivity.class, bundleSpeakerProfile, Constants.REQUEST_CODE.REQUEST_CODE_SPEAKER_PROFILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SPEAKER_PROFILE) {
            if(resultCode == RESULT_OK) {
                try {
                    if(data != null) {
                        int position = data.getIntExtra(Constants.BundleKey.POSITION, 0);
                        boolean isSelected = data.getBooleanExtra(Constants.BundleKey.IS_SELECTED, false);
                        if(mGuestSpeakerSessionList != null && mGuestSpeakerSessionList.size() > 0) {
                            GuestSpeakerSessionRequest guestSpeakerSessionRequest = mGuestSpeakerSessionList.get(position);
                            if(guestSpeakerSessionRequest != null && guestSpeakerSessionRequest.getSpeakerId() != 0) {
                                guestSpeakerSessionRequest.setSelected(isSelected);
                                if(isSelected) {
                                    mTotalPrice = mTotalPrice + guestSpeakerSessionRequest.getSessionPrice();
                                    if(mSelectedSpeakerIdList != null) {
                                        mSelectedSpeakerIdList.add((Integer) guestSpeakerSessionRequest.getSpeakerId());
                                    }
                                } else {
                                    mTotalPrice = mTotalPrice - guestSpeakerSessionRequest.getSessionPrice();
                                    if(mSelectedSpeakerIdList != null && mSelectedSpeakerIdList.size() > 0) {
                                        mSelectedSpeakerIdList.remove((Integer) guestSpeakerSessionRequest.getSpeakerId());
                                    }
                                }
                                mBinding.tvTotalPriceValue.setText(getResources().getString(R.string.min_budget_with_dollar,
                                        (int) mTotalPrice));
                                if(mSpeakerEventInfoAdapter != null) {
                                    if(mEventDetails != null) {
                                        mSpeakerEventInfoAdapter.setEventStatus(mEventDetails.getStatus());
                                    }
                                    mSpeakerEventInfoAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public String getActivityName() {
        return "LearnerEventDetails";
    }

}
