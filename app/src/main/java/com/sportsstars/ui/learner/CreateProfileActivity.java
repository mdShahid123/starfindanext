package com.sportsstars.ui.learner;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityLearnerProfileBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.model.Sports;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.request.auth.learner.ProfileRequest;
import com.sportsstars.network.request.auth.learner.SportYouPlay;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.learner.adapter.ProfileAdapter;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.ImageUtil;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by atul on 26/04/18.
 * To inject activity reference.
 */

public class CreateProfileActivity extends BaseActivity implements ProfileAdapter.IAddYouSport {

    private static final int UPLOAD_TYPE = 1;
    public static final int REQUEST_CODE = 3123;
    private static final int ASPECT_RATIO_SQUARE_X = 1;
    private static final int ASPECT_RATIO_SQUARE_Y = 1;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 2122;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1221;

    private Dialog mDialog;
    public static boolean mIsEdit;
    private Uri mCropImageUri;
    private Bitmap mCroppedImage;
    private CropImageView mCropImageView;
    private ProfileAdapter mProfileAdapter;
    private ProfileRequest mProfileRequest;
    private ActivityLearnerProfileBinding mBinding;
    private boolean isForLoginResult;
    private UserProfileModel mProfileModel;
    private UserProfileInstance mUserProfileInstance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_learner_profile);
//        LocalBroadcastManager.getInstance(this).registerReceiver(mSuburbLocationReceiver,
//                new IntentFilter(Constants.INTENT_FILTER_SUBURB_BROADCAST));
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }
        initView();
    }

//    private BroadcastReceiver mSuburbLocationReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String suburbLocation = intent.getStringExtra(Constants.INTENT_EXTRA_SUBURB);
//            showToast("Suburb location from BaseActivity - " + suburbLocation);
//            Log.d("receiver", "suburbLocation from BaseActivity : " + suburbLocation);
//        }
//    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
//        try {
//            if(mSuburbLocationReceiver != null) {
//                LocalBroadcastManager.getInstance(this).unregisterReceiver(mSuburbLocationReceiver);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(mIsEdit) {
            if(otpVerifiedEvent.isOtpVerified()) {
                finish();
            }
        }
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Log.d("learnerlog", "CreateProfileActivity - initView - bundle is not null");
            if (getIntent().hasExtra(Constants.EXTRA_IS_LOGIN_RESULT)) {
                isForLoginResult = bundle.getBoolean(Constants.EXTRA_IS_LOGIN_RESULT);
            }
            Log.d("learnerlog", "CreateProfileActivity - initView - isForLoginResult : " + isForLoginResult);
            if (!isForLoginResult) {
                if(getIntent().hasExtra(Constants.BundleKey.DATA)) {
                    showViewToEditProfile(bundle);
                } else {
                    showViewToCreateProfile();
                }
            } else {
                showViewToCreateProfile();
            }
        } else {
            Log.d("learnerlog", "CreateProfileActivity - initView - bundle is null");
            showViewToCreateProfile();
        }
    }

    private void showViewToEditProfile(Bundle bundle) {
        Log.d("learnerlog", "CreateProfileActivity - initView - showViewToEditProfile");
        mBinding.headerId.setPadding(0, 30, 0, 20);
        setUpHeader(mBinding.headerId, getString(R.string.edit_profile), "");
        mIsEdit = true;
        mProfileRequest = bundle.getParcelable(Constants.BundleKey.DATA);
        mBinding.btnNext.setText(getResources().getString(R.string.save));
        addSportUPlay();
    }

    private void showViewToCreateProfile() {
        Log.d("learnerlog", "CreateProfileActivity - initView - showViewToCreateProfile");
        mIsEdit = false;
        mProfileRequest = new ProfileRequest();
        setUpHeader(mBinding.headerId, getString(R.string.create_profile), getString(R.string.we_need_to_know));
        mBinding.btnNext.setText(getResources().getString(R.string.next));
        getCoachProfileStatus();
    }

    private void addSportUPlay() {

        mProfileAdapter = new ProfileAdapter(mProfileRequest, this, this);
        mBinding.rvProfileSyp.setHasFixedSize(true);
        mBinding.rvProfileSyp.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvProfileSyp.setAdapter(mProfileAdapter);
        mBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsEdit) {
                    editProfile();
                    return;
                }
                createProfile();
            }
        });
    }

    private void getCoachProfileStatus() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.LEARNER);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(CreateProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        //                    mProfileModel = response.getResult();
//                    mUserProfileInstance = UserProfileInstance.getInstance();
//                    mUserProfileInstance.setUserProfileModel(mProfileModel);
//                    setPreviousData();

                        if(mProfileRequest == null) {
                            mProfileRequest = new ProfileRequest();
                        }
                        mProfileRequest.setName(response.getResult().getName());
                        mProfileRequest.setDob(response.getResult().getDob());
                        mProfileRequest.setMobileNumber(response.getResult().getMobileNumber());
                        mProfileRequest.setGender(response.getResult().getGender());
                        mProfileRequest.setProfileImage(response.getResult().getProfileImage());
                        mProfileRequest.setPostCode(response.getResult().getSuburb().getAddress());
                        addSportUPlay();
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(CreateProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST) {
                if (data != null) {
                    int index = data.getIntExtra(Constants.BundleKey.INDEX, -1);
                    if (index != -1) {
                        Sports sports = data.getParcelableExtra(Constants.EXTRA_SPORT);
                        mProfileAdapter.updateSyp(index, sports);
                    }
                }
            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS) {
                if (data != null) {
                    int index = data.getIntExtra(Constants.BundleKey.INDEX, -1);
                    if (index != -1) {
                        ArrayList<Team> teamsList = data.getParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST);
                        if (teamsList != null && !teamsList.isEmpty())
                            mProfileAdapter.updateTeam(index, teamsList);
                    }
                }
            } else if (requestCode == REQUEST_CODE) {

                getCaptureUri(data);

            } else if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_LOCATION) {
                if(data != null) {
                    String suburbLocation = data.getStringExtra(Constants.EXTRA_ADDRESS);
                    //showToast("suburbLocation onActivityResult - " + suburbLocation);
                    if(!TextUtils.isEmpty(suburbLocation)) {
                        if(mProfileAdapter != null) {
                            mProfileRequest.setPostCode(suburbLocation);
                            mProfileAdapter.updateProfileRequestForSuburbLocation(suburbLocation);
                        }
                    }
//                    Intent intent = new Intent(Constants.INTENT_FILTER_SUBURB_BROADCAST);
//                    intent.putExtra(Constants.INTENT_EXTRA_SUBURB, suburbLocation);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(data);
                }
            }
        }
    }

    private void getCaptureUri(Intent data) {
        showCropDialog(data);
        Uri imageUri = Utils.getPickImageResultUri(data, this);
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                Utils.isUriRequiresPermissions(imageUri, this)) {

            requirePermissions = true;
            mCropImageUri = imageUri;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }

        if (!requirePermissions) {
            mCropImageView.setImageUriAsync(imageUri);
            String path = Utils.getRealPathFromURI(CreateProfileActivity.this, imageUri);
            LogUtils.LOGD("path>>", path);

            try {
                if (Utils.getBitmapFromUri(this, imageUri) == null) {
                    if (mDialog != null && mDialog.isShowing()) {
                        showSnackbarFromTop(this, "Please select valid image");
                        mDialog.dismiss();
                    }
                }
            } catch (IOException e) {
                LogUtils.LOGE("CreateProfileActivity", "bmp err>" + e.getMessage());
            }
        }
    }

    public void showCropDialog(final Intent data) {
        mDialog = new Dialog(this, android.R.style.Theme_Light);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_image_crop);
        mCropImageView = (CropImageView) mDialog.findViewById(R.id.image_crop);
        mCropImageView.setAspectRatio(ASPECT_RATIO_SQUARE_X, ASPECT_RATIO_SQUARE_Y);

        Button btnCrop = mDialog.findViewById(R.id.button_crop);
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Bitmap cropped = mCropImageView.getCroppedImage();
                if (cropped != null) {
                    mProfileAdapter.updateProfilePic(cropped);
                    mCroppedImage = cropped;

                    Uri imageUri = Utils.getPickImageResultUri(data, CreateProfileActivity.this);
                    mCropImageView.setImageUriAsync(imageUri);

                    Uri uri = Utils.getImageUri(CreateProfileActivity.this, cropped);
                    String path = Utils.getRealPathFromURI(CreateProfileActivity.this, uri);
                    LogUtils.LOGD("path>>", path);

                    uploadImage();
                } else {
                    mCroppedImage = null;
                }
            }
        });

        Button buttonCancel = mDialog.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void createProfile() {
        if (mProfileRequest.getSportsView().size() > 2) {
            mProfileRequest.setSports(mProfileRequest.getSportsView().subList(1, mProfileRequest.getSportsView().size() - 1));
        }
        String msg = isInputValid();
        if (!StringUtils.isNullOrEmpty(msg)) {
            try {
                showSnackbarFromTop(CreateProfileActivity.this, msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

//        Log.d("create_learner", "profile request : " + mProfileRequest);
//        Log.d("create_learner", "profile request name : " + mProfileRequest.getName());
//        Log.d("create_learner", "profile request account type : " + mProfileRequest.getName());
//        Log.d("create_learner", "profile request company name : " + mProfileRequest.getCompanyName());
//        Log.d("create_learner", "profile request abn number : " + mProfileRequest.getAbnNumber());

        if(mProfileRequest.getAccountType() == Constants.ACCOUNT_TYPE.PERSONAL) {
            mProfileRequest.setAbnNumber(0);
            mProfileRequest.setCompanyName("");
        }

        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.learnerCreateProfile(mProfileRequest).enqueue(new BaseCallback<BaseResponse>(CreateProfileActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
//                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
//                        showToast(response.getMessage());
//                        //showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
//                    }
                    if (isForLoginResult) {
                        setResult(Constants.RESULT_CODE_PROFILE_COMPLETED);
                        finish();
                    } else {
                        Navigator.getInstance().navigateToActivity(CreateProfileActivity.this, LearnerHomeActivity.class);
                        finish();
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void editProfile() {
        if (mProfileRequest.getSportsView().size() > 2) {
            mProfileRequest.setSports(mProfileRequest.getSportsView().subList(1, mProfileRequest.getSportsView().size() - 1));
        }
        String msg = isInputValid();
        if (!StringUtils.isNullOrEmpty(msg)) {
            showSnackbarFromTop(CreateProfileActivity.this, msg);
            return;
        }

//        if(mProfileRequest.getAccountType() == Constants.ACCOUNT_TYPE.PERSONAL) {
//            mProfileRequest.setAbnNumber(0);
//            mProfileRequest.setCompanyName("");
//        }

        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.learnerUpdateProfile(mProfileRequest).enqueue(new BaseCallback<BaseResponse>(CreateProfileActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        //showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
                        //showToast("Profile updated successfully.");
                    }

                    Alert.showAlertDialog(CreateProfileActivity.this,
                            getString(R.string.profile_updated),
                            getString(R.string.profile_successfully_updated),
                            getString(R.string.okay),
                            new Alert.OnOkayClickListener() {
                                @Override
                                public void onOkay() {
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            });

//                    setResult(RESULT_OK);
//                    finish();
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void uploadImage() {
        showProgressBar();
        File file = ImageUtil.getFileFromBitmap(mCroppedImage, CreateProfileActivity.this);

        RequestBody fBody = null;
        if (file != null) {
            fBody = RequestBody.create(MediaType.parse("image"), file);
        }
        RequestBody type = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE);

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.uploadImage(type, fBody).enqueue(new BaseCallback<BaseResponse>(CreateProfileActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() != 1) {
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                } else {
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            case MY_PERMISSIONS_REQUEST_CAMERA:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);

                } else {
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCropImageView.setImageUriAsync(mCropImageUri);
                } else {
                    //Toast.makeText(getApplicationContext(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private String isInputValid() {
        if (StringUtils.isNullOrEmpty(mProfileRequest.getName())) {
            return getString(R.string.empty_name);
        }
        if (TextUtils.isEmpty(mProfileRequest.getDob())) {
            return getString(R.string.empty_age);
        }
        if(mIsEdit) {
            if (TextUtils.isEmpty(mProfileRequest.getMobileNumber())) {
                return getString(R.string.empty_mobile_no);
            }
            if (mProfileRequest.getMobileNumber().length() < 10) {
                return getString(R.string.invalid_mobile_no);
            }
        }
        if (TextUtils.isEmpty(mProfileRequest.getPostCode())) {
            return getString(R.string.empty_suburb_learner);
        }
        if (mProfileRequest.getPostCode().length() < getResources().getInteger(R.integer.post_code_length)) {
            return getString(R.string.invalid_suburb_learner);
        }
        if(mProfileRequest.getAccountType() == Constants.ACCOUNT_TYPE.NONE) {
            return getString(R.string.txt_account_type);
        }
        if(mProfileRequest.getAccountType() == Constants.ACCOUNT_TYPE.CORPORATE) {
            if(TextUtils.isEmpty(mProfileRequest.getCompanyName())) {
                return getString(R.string.txt_company_name);
            }
            if(mProfileRequest.getAbnNumber() == 0) {
                return getString(R.string.txt_abn_empty);
            }
            if(TextUtils.isEmpty(String.valueOf(mProfileRequest.getAbnNumber()))) {
                return getString(R.string.txt_abn_empty);
            }
            if(!TextUtils.isEmpty(String.valueOf(mProfileRequest.getAbnNumber())) &&
                    String.valueOf(mProfileRequest.getAbnNumber()).length() > 0 &&
                    String.valueOf(mProfileRequest.getAbnNumber()).length() < 11) {
                return getString(R.string.txt_abn_limit);
            }
        }
        for (SportYouPlay youPlay : mProfileRequest.getSports()) {
            if (TextUtils.isEmpty(youPlay.getFirstSportName())) {
                return getString(R.string.txt_syp);
            }
            if (TextUtils.isEmpty(youPlay.getFavouritePlayer())) {
                return getString(R.string.txt_fp);
            }
            if (TextUtils.isEmpty(youPlay.getFavouriteTeamName())) {
                return getString(R.string.txt_ft);
            }
//            if (TextUtils.isEmpty(youPlay.getClubPlayedFor())) {
//                return getString(R.string.club_played_empty_msg);
//            }
        }
        return null;
    }

    @Override
    public void inAddSport() {
        if (mProfileRequest.getSportsView().size() > 11) {
            showSnackbarFromTop(this, getString(R.string.txt_rem_sport));
            return;
        }
        mProfileAdapter.addAnotherSport();
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }

    @Override
    public void onBackPressed() {
        if(isForLoginResult && !mIsEdit) {
            PreferenceUtil.setIsLogin(false);
        }
        super.onBackPressed();
    }

}
