package com.sportsstars.ui.learner;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.appster.chatlib.constants.AppConstants;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.databinding.ActivityLearnerViewProfileBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.model.LearnerProfile;
import com.sportsstars.model.ProfileLearner;
import com.sportsstars.model.SportIPlay;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ReportUserRequest;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.fixture.CreateFixtureListActivity;
import com.sportsstars.ui.learner.fragment.AboutFragment;
import com.sportsstars.ui.learner.fragment.FixtureFragment;
import com.sportsstars.ui.settings.ReportUserActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by atul on 01/05/18.
 * To inject activity reference.
 */

public class ViewProfileActivity extends BaseActivity implements View.OnClickListener {

    private Fragment mFragment;
    private LearnerProfile mProfile;
    private FragmentTransaction mTransaction;
    private ActivityLearnerViewProfileBinding mBinding;
    private boolean isAboutTabSelected;

    private boolean isFromSessionDetails;
    private boolean isCoachSessionCompleted;
    private ReportUserRequest reportUserRequest;

    public static boolean isFixtureOrSessionAvailable = true;

    private int eventBookingStatus = -1;
    private int speakerStatus = -1;
    private int isSpeakerPaid = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_learner_view_profile);
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }
        isFixtureOrSessionAvailable = true;
        initBinding();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(otpVerifiedEvent.isOtpVerified()) {
            //finish();
            if(Utils.isNetworkAvailable()) {
                getLearnerProfile();
            } else {
                showSnackbarFromTop(ViewProfileActivity.this, getResources().getString(R.string.no_internet));
            }
        }
    }

    private void initBinding() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFromSessionDetails = bundle.getBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, false);
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_BOOKING_STATUS)) {
                eventBookingStatus = bundle.getInt(Constants.BundleKey.EVENT_BOOKING_STATUS, -1);
            }
            if(getIntent().hasExtra(Constants.BundleKey.SPEAKER_STATUS)) {
                speakerStatus = bundle.getInt(Constants.BundleKey.SPEAKER_STATUS, -1);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_SPEAKER_PAID)) {
                isSpeakerPaid = bundle.getInt(Constants.BundleKey.IS_SPEAKER_PAID, -1);
            }
            getDataFromSessionDetails(isFromSessionDetails);
            setViewVisibility(isFromSessionDetails);
            ProfileLearner pfl = bundle.getParcelable(Constants.BundleKey.DATA);
            if (pfl != null) {
                mProfile = new LearnerProfile();
                mProfile.setResult(pfl);
                bindDataSet();
                setEmptyForIncompleteLearner(pfl);
            }
        }
        mBinding.tvEdit.setOnClickListener(this);
        mBinding.btnAbout.setOnClickListener(this);
        mBinding.btnFixture.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.ivChat.setOnClickListener(this);

    }

    private void setViewVisibility(boolean isFromSessionDetails) {
        if(isFromSessionDetails) {
            mBinding.ivBack.setVisibility(View.VISIBLE);
            mBinding.ivChat.setVisibility(View.VISIBLE);
            mBinding.tvEdit.setVisibility(View.INVISIBLE);
            mBinding.llMobileNumber.setVisibility(View.GONE);
            mBinding.tabLayout.setVisibility(View.GONE);

            if(isCoachSessionCompleted) {
                mBinding.ivChat.setImageResource(R.drawable.ic_report_user);
            } else {
                mBinding.ivChat.setImageResource(R.drawable.ic_message);
                if(eventBookingStatus == Utils.STATUS_BOOKING.CANCELLED) {
                    mBinding.ivChat.setVisibility(View.GONE);
                } else if(eventBookingStatus == Utils.STATUS_BOOKING.COMPLETED) {
                    mBinding.ivChat.setVisibility(View.GONE);
                } else if(eventBookingStatus == Utils.STATUS_BOOKING.ONGOING) {
                    mBinding.ivChat.setVisibility(View.VISIBLE);
                } else if(eventBookingStatus == Utils.STATUS_BOOKING.UPCOMING) {
                    mBinding.ivChat.setVisibility(View.VISIBLE);
                } else if(eventBookingStatus == Utils.STATUS_BOOKING.PAYMENT_PENDING) {
                    mBinding.ivChat.setVisibility(View.VISIBLE);
                } else if(eventBookingStatus == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
                    mBinding.ivChat.setVisibility(View.GONE);
                }

                if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
                    mBinding.ivChat.setVisibility(View.GONE);
                } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
                    if(isSpeakerPaid == 1) {
                        mBinding.ivChat.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.ivChat.setVisibility(View.GONE);
                    }
                } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
                    mBinding.ivChat.setVisibility(View.GONE);
                }
            }
        } else {
            mBinding.ivBack.setVisibility(View.INVISIBLE);
            mBinding.ivChat.setVisibility(View.INVISIBLE);
            mBinding.tvEdit.setVisibility(View.VISIBLE);
            mBinding.llMobileNumber.setVisibility(View.VISIBLE);
            mBinding.tabLayout.setVisibility(View.VISIBLE);
        }
    }

    private void getDataFromSessionDetails(boolean isFromSessionDetails) {
        if(isFromSessionDetails) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                isCoachSessionCompleted = bundle.getBoolean(Constants.BundleKey.IS_COACH_SESSION_COMPLETED, false);
                reportUserRequest = bundle.getParcelable(Constants.BundleKey.REPORT_USER_DATA);
            }
        }
    }

    private void setEmptyForIncompleteLearner(ProfileLearner pfl) {
        if(pfl != null) {
            if(!isProfileCompleted(pfl)) {
                mBinding.tvLbp.setText("");
            } else {
                mBinding.tvLbp.setText(pfl.getGender() == Constants.GENDER_TYPE.MALE ?
                        getResources().getString(R.string.male) : getResources().getString(R.string.female));
            }
        }
    }

    private void setEmptyForIncompleteLearner(LearnerProfile mProfile) {
        if(mProfile != null && mProfile.getResult() != null) {
            ProfileLearner pfl = mProfile.getResult();
            if(!isProfileCompleted(pfl)) {
                mBinding.tvLbp.setText("");
            } else {
                mBinding.tvLbp.setText(pfl.getGender() == Constants.GENDER_TYPE.MALE ?
                        getResources().getString(R.string.male) : getResources().getString(R.string.female));
            }
        }
    }

    private boolean isProfileCompleted(ProfileLearner pfl) {
        boolean isComplete = true;
        if(pfl.getGender() == 0 && TextUtils.isEmpty(pfl.getDob()) && TextUtils.isEmpty(pfl.getMobileNumber())
                && TextUtils.isEmpty(pfl.getPostCode()) && pfl.getAge() == 0) {
            isComplete = false;
        }
        return isComplete;
    }

    private void switchToAbout() {
        isAboutTabSelected = true;
        mBinding.btnFixture.setBackgroundColor(ContextCompat.getColor(this, R.color.white_color));
        mBinding.btnAbout.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));
        mTransaction = getSupportFragmentManager().beginTransaction();

        ArrayList<SportIPlay> sportIPlayArrayList;
        if(mProfile != null && mProfile.getResult() != null
                && mProfile.getResult().getLearnerSport() != null && mProfile.getResult().getLearnerSport().size() > 0) {
            sportIPlayArrayList = mProfile.getResult().getLearnerSport();
        } else {
            sportIPlayArrayList = new ArrayList<>();
        }

        String learnerName = "StarFinda Learner";
        if(mProfile != null && mProfile.getResult() != null && mProfile.getResult().getName() != null
                && !TextUtils.isEmpty(mProfile.getResult().getName())) {
            learnerName = mProfile.getResult().getName();
        }

        mFragment = AboutFragment.newInstance(sportIPlayArrayList, isFromSessionDetails, learnerName);
        mTransaction.replace(R.id.profileContainer, mFragment);
        mTransaction.commit();
    }

    private void switchToFixture() {
        isAboutTabSelected = false;
        mBinding.btnAbout.setBackgroundColor(ContextCompat.getColor(this, R.color.white_color));
        mBinding.btnFixture.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));
        mTransaction = getSupportFragmentManager().beginTransaction();
        mFragment = FixtureFragment.newInstance();
        mTransaction.replace(R.id.profileContainer, mFragment);
        mTransaction.commit();
    }

    private void bindDataSet() {
        mBinding.setProfile(mProfile.getResult());
        switchToAbout();
        setProfilePic();
    }

    private void setProfilePic() {
        if (StringUtils.isNullOrEmpty(mProfile.getResult().getProfileImage()))
            return;
        Picasso.with(ViewProfileActivity.this)
                .load(mProfile.getResult().getProfileImage()).placeholder(R.drawable.ic_user_circle).
                error(R.drawable.ic_user_circle).into(mBinding.ivProfilePic,
                new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        mBinding.ivBgHeader.setImageDrawable(mBinding.ivProfilePic.getDrawable());
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAbout:
                switchToAbout();
                break;
            case R.id.btnFixture:
                switchToFixture();
                break;
            case R.id.tvEdit:
                if(Utils.isNetworkAvailable()) {
                    if(isAboutTabSelected) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.BundleKey.DATA, mProfile.getProfileData());
                        Navigator.getInstance().navigateToActivityForResultWithData(this, CreateProfileActivity.class, bundle, 202);
                    } else {
                        if(isFixtureOrSessionAvailable) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, true);
                            Navigator.getInstance().navigateToActivityForResultWithData(this, CreateFixtureListActivity.class, bundle, 303);
                        } else {
                            showSnackbarFromTop(ViewProfileActivity.this, getResources().getString(R.string.no_upcoming_session_or_fixture));
                        }
                    }
                } else {
                    showSnackbarFromTop(ViewProfileActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.ivChat:
                if(isCoachSessionCompleted) {
                    if(Utils.isNetworkAvailable()) {
                        launchReportUser(reportUserRequest);
                    } else {
                        showSnackbarFromTop(ViewProfileActivity.this, getResources().getString(R.string.no_internet));
                    }
                } else {
                    startChat();
                }
                break;
        }
    }

    private void launchReportUser(ReportUserRequest reportUserRequest) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, reportUserRequest);
        Navigator.getInstance().navigateToActivityWithData(ViewProfileActivity.this,
                ReportUserActivity.class, bundle);
    }

    private void startChat() {
        if(mProfile!=null)
        {
            String userName = mProfile.getResult().getJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, mProfile.getResult().getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, mProfile.getResult().getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, mProfile.getResult().getJid());//.replace("246","260"));
//                intent.putExtra(AppConstants.KEY_TO_USER, _currentBookingList.getLearnerJid());
                startActivity(intent);
            } else {
                com.appster.chatlib.utils.Utils.showToast(this, "enter friend username");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 202) {
            if(Utils.isNetworkAvailable()) {
                getLearnerProfile();
            } else {
                showSnackbarFromTop(ViewProfileActivity.this, getResources().getString(R.string.no_internet));
            }
        } else if(resultCode == RESULT_OK && requestCode == 303) {
            // TODO: 09/05/18 - to update fixture fragment on reload
        }
    }

    private void getLearnerProfile() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.LEARNER);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewLearnerProfile(request).enqueue(new BaseCallback<LearnerProfile>(ViewProfileActivity.this) {
            @Override
            public void onSuccess(LearnerProfile response) {
                if (response != null && response.getStatus() == 1) {
                    mProfile = response;
                    bindDataSet();
                    setEmptyForIncompleteLearner(mProfile);
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(ViewProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<LearnerProfile> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(ViewProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(!isFromSessionDetails) {
            if(mProfile != null && mProfile.getResult() != null) {
                Intent intent = new Intent();
                intent.putExtra(Constants.BundleKey.DATA, mProfile.getResult());
                setResult(RESULT_OK, intent);
            }
        }
        finish();
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }

}
