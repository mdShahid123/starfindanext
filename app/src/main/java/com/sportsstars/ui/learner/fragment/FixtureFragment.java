/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.learner.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentFixtureLearnerBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.FixtureData;
import com.sportsstars.network.response.auth.FixtureList;
import com.sportsstars.network.response.auth.FixtureListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.fixture.CreateFixtureListActivity;
import com.sportsstars.ui.learner.ViewProfileActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;

/**
 * Created by atul on 01/05/18.
 * To inject activity reference.
 */

public class FixtureFragment extends BaseFragment implements View.OnClickListener {

    private FragmentFixtureLearnerBinding mBinding;
    private ArrayList<FixtureData> mFixtureList = new ArrayList<>();
    private int currentPage;
    private int perPageOffset;
    private int totalCount;
    private int currentSelectedFixturePos = -1;
    private boolean hasInternet;
    private FixtureListResponse fixtureListResponse = null;

    public static FixtureFragment newInstance() {
        FixtureFragment fixtureFragment = new FixtureFragment();
        Bundle bundle = new Bundle();
        fixtureFragment.setArguments(bundle);
        return fixtureFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_fixture_learner, container, false);
        initView();
        return mBinding.getRoot();
    }

    private void initView() {
        mBinding.ivPreviousFixture.setOnClickListener(this);
        mBinding.ivNextFixture.setOnClickListener(this);
        mBinding.rlBtnCreateFixture.setOnClickListener(this);
//        NetworkMonitor.initialize();
//        if(NetworkMonitor.isNetworkAvailable()) {
//            hasInternet = true;
//            getFixturesList();
//        } else {
//            hasInternet = false;
//            showFixtureData(false);
//            showSnackbarFromTop(getResources().getString(R.string.no_internet));
//        }
    }

    private void showFixtureData(boolean hasFixture) {
        if(hasFixture) {
            mBinding.rlEmptyFixture.setVisibility(View.INVISIBLE);
            mBinding.llFilledFixture.setVisibility(View.VISIBLE);
        } else {
            mBinding.llFilledFixture.setVisibility(View.INVISIBLE);
            mBinding.rlEmptyFixture.setVisibility(View.VISIBLE);
            if (hasInternet) {
                mBinding.tvEmptyFixture.setText(getResources().getString(R.string.no_fixture_msg));
                mBinding.rlBtnCreateFixture.setVisibility(View.VISIBLE);
                mBinding.tvEmptyFixtureTitle.setVisibility(View.VISIBLE);
                mBinding.viewDivider.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvEmptyFixture.setText(getResources().getString(R.string.no_internet));
                mBinding.rlBtnCreateFixture.setVisibility(View.GONE);
                mBinding.tvEmptyFixtureTitle.setVisibility(View.INVISIBLE);
                mBinding.viewDivider.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void getFixturesList() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.fixtureList().enqueue(new BaseCallback<FixtureListResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(FixtureListResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        if(response.getResult() != null) {
//                            currentPage = response.getResult().getCurrentPage();
//                            perPageOffset = response.getResult().getPerPage();
//                            totalCount = response.getResult().getTotal();
//                            mFixtureList = response.getResult().getData();

                            if(fixtureListResponse != null) {
                                fixtureListResponse = null;
                            }
                            fixtureListResponse = response;
                            isFixtureOrSessionAvailable();

                            mFixtureList = new ArrayList<>();
                            ArrayList<FixtureData> localFixtureDataList = new ArrayList<>();

                            if(response.getResult() != null && response.getResult().getFixtureList() != null
                                    && response.getResult().getFixtureList().size() > 0) {
                                ArrayList<FixtureList> fixtureLists = response.getResult().getFixtureList();
                                if(fixtureLists != null && fixtureLists.size() > 0) {
                                    for(FixtureList fixtureList : fixtureLists) {
                                        if(fixtureList != null && fixtureList.getSportFixtures() != null
                                                && fixtureList.getSportFixtures().size() > 0) {
                                            for(FixtureData fixtureData : fixtureList.getSportFixtures()) {
                                                fixtureData.setSportId(fixtureList.getSportId());
                                                fixtureData.setIcon(fixtureList.getSportIcon());
                                                fixtureData.setName(fixtureList.getSportName());
                                                fixtureData.setId(fixtureData.getId());
                                                fixtureData.setPlayDate(fixtureData.getPlayDate());
                                                fixtureData.setPlayTime(fixtureData.getPlayTime());
                                                //fixtureData.setFixtureDateTime(fixtureData.getFixtureDateTime());

                                                // CHANGES DONE HERE TO RESOLVE ISSUE COMING FROM BACKEND IN DATE TIME FORMAT
                                                String correctedFixtureDateTime = correctFixtureDateTimeFormat(fixtureData);
                                                fixtureData.setFixtureDateTime(correctedFixtureDateTime);

                                                localFixtureDataList.add(fixtureData);
                                                Log.d("fixture_log", "localFixtureDataList size : "
                                                        + localFixtureDataList.size());
                                            }
                                        }
                                    }
                                }
                                mFixtureList.addAll(localFixtureDataList);
                                loadUI();
                            } else {
                                mFixtureList.addAll(localFixtureDataList);
                                loadUI();
                            }
                        }
                    } else {
                        showFixtureData(false);
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<FixtureListResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadUI() {
        if(mFixtureList != null && mFixtureList.size() > 0) {
            showFixtureData(true);
            currentSelectedFixturePos = 0;
            setData(currentSelectedFixturePos);
        } else {
            showFixtureData(false);
            currentSelectedFixturePos = -1;
            showSnackbarFromTop(getResources().getString(R.string.no_fixture_added_yet));
        }
    }

    private String correctFixtureDateTimeFormat(FixtureData fixtureData) {
        String[] dateTimeInUtcArr = fixtureData.getFixtureDateTime().split(" ");
        String dateUtcStr = dateTimeInUtcArr[0];
        String timeUtcStr = dateTimeInUtcArr[1];

        String newFixtureTime = "";
        String[] timeUtcArr = timeUtcStr.split(":");
        if(timeUtcArr.length == 3) {
            newFixtureTime = timeUtcStr;
        } else if(timeUtcArr.length == 2) {
            newFixtureTime = timeUtcStr + ":00";
        }

        return dateUtcStr + " " + newFixtureTime;
    }

    private void setData(int currentSelectedFixturePos) {

        FixtureData fixtureData = mFixtureList.get(currentSelectedFixturePos);

        if(!TextUtils.isEmpty(fixtureData.getIcon())) {
            Picasso.with(getBaseActivity())
                    .load(fixtureData.getIcon())
                    //.placeholder(R.drawable.ic_image_preview_placeholder)
                    //.error(R.drawable.ic_image_preview_placeholder)
                    .into((mBinding.ivSportIcon));
        }

        mBinding.tvSportName.setText(getString(R.string.fixtures, fixtureData.getName()));

        mBinding.tvFixtureWeek.setText(getString(R.string.week_fixtures, (currentSelectedFixturePos+1)));

        String fixtureDateTimeLocal = DateFormatUtils.convertUtcToLocal(fixtureData.getFixtureDateTime(),
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.FIXTURE_DATE_FORMAT_TIME);

        String dtStart = "";
        String time = "";
        String[] fixtureDateTimeArr = fixtureDateTimeLocal.split(" ");
        if(fixtureDateTimeArr != null && fixtureDateTimeArr.length > 1) {
            dtStart = fixtureDateTimeArr[0];
            time = fixtureDateTimeArr[1];
            Log.d("fixture_log", "dtStart : " + dtStart
                    + " time : " + time);
        }

        //String dtStart = fixtureData.getPlayDate();
        SimpleDateFormat format = new SimpleDateFormat(DateFormatUtils.SERVER_DATE_FORMAT, Locale.getDefault());
        try {
            Date date = format.parse(dtStart);
            Log.d("fixture_log"," date object : " + date);

            String day          = (String) DateFormat.format("dd",   date); // 20
            //String monthString  = (String) DateFormat.format("MMM",  date); // Jun
            String monthString  = (String) DateFormat.format("MMMM",  date); // June

            mBinding.tvCalendarDate.setText(day);

            mBinding.tvFixtureDate.setText(day + Utils.getDateOfTheMonthSuffix(Integer.parseInt(day)).toUpperCase() +
                    " " + monthString);

        } catch (ParseException e) {
            LogUtils.LOGE("mylog", e.getMessage());
        }

        SimpleDateFormat _24HrFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        SimpleDateFormat _12HrFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        try {
            Date _24HrDate = _24HrFormat.parse(time);
            //Log.d("mylog", "24 hr date : "+ _24HrDate);
            //Log.d("mylog", "12 hr format time : "+ _12HrFormat.format(_24HrDate));
            mBinding.tvFixtureTime.setText(_12HrFormat.format(_24HrDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivPreviousFixture:
                goPrevious();
                break;
            case R.id.ivNextFixture:
                goNext();
                break;
            case R.id.rlBtnCreateFixture:
                if(Utils.isNetworkAvailable()) {
                    if(isFixtureOrSessionAvailable()) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, true);
                        Navigator.getInstance().navigateToActivityForResultWithData(getActivity(),
                                CreateFixtureListActivity.class, bundle, 303);
                    } else {
                        showSnackbarFromTop(getResources().getString(R.string.no_upcoming_session_or_fixture));
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private boolean isFixtureOrSessionAvailable() {
        boolean isAvailable = true;
        ViewProfileActivity.isFixtureOrSessionAvailable = true;
        if(fixtureListResponse != null && fixtureListResponse.getResult() != null) {
            if((fixtureListResponse.getResult().getFixtureList() == null
                    || fixtureListResponse.getResult().getFixtureList().size() == 0)
                && (fixtureListResponse.getResult().getUpcomingSports() == null
                    || fixtureListResponse.getResult().getUpcomingSports().size() == 0)) {
                ViewProfileActivity.isFixtureOrSessionAvailable = false;
                isAvailable = false;
            } else {
                ViewProfileActivity.isFixtureOrSessionAvailable = true;
                isAvailable = true;
            }
        }

        return isAvailable;
    }

    private void goPrevious() {
        if(mFixtureList != null && mFixtureList.size() > 0) {
            if(currentSelectedFixturePos > 0) {
                currentSelectedFixturePos--;
                setData(currentSelectedFixturePos);
            } else {
                showSnackbarFromTop(getResources().getString(R.string.cant_go_back));
            }
        }
    }

    private void goNext() {
        if(mFixtureList != null && mFixtureList.size() > 0) {
            if(currentSelectedFixturePos < mFixtureList.size()-1) {
                currentSelectedFixturePos++;
                setData(currentSelectedFixturePos);
            } else {
                showSnackbarFromTop(getResources().getString(R.string.you_are_on_last_fixture));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Utils.isNetworkAvailable()) {
            hasInternet = true;
            getFixturesList();
        } else {
            hasInternet = false;
            showFixtureData(false);
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }

}
