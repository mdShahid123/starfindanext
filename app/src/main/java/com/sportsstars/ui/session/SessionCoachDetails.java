package com.sportsstars.ui.session;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.appster.chatlib.constants.AppConstants;
import com.sportsstars.MVVM.entities.mysession.BookingList;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.databinding.ActivitySessionCoachDetailsBinding;
import com.sportsstars.model.LearnerProfile;
import com.sportsstars.model.ProfileLearner;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.BookMarkRequest;
import com.sportsstars.network.request.auth.CalendarSyncPostRequest;
import com.sportsstars.network.request.auth.ReportUserRequest;
import com.sportsstars.network.response.BookMarkResponse;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.response.SessionDetailsResponse;
import com.sportsstars.network.response.WeatherInfo;
import com.sportsstars.network.response.WeatherInformationResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.settings.ReportUserActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import retrofit2.Call;

public class SessionCoachDetails extends BaseActivity implements Alert.OnOkayClickListener {

    ActivitySessionCoachDetailsBinding mBinding;
    int mSessionId;
    BookingList _currentBookingList = new BookingList();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_session_coach_details);

        mSessionId = getIntent().getIntExtra(getString(R.string.booking_id_extra), 0);
        getSessionDetails(mSessionId);

        mBinding.buttonCancelSession.setOnClickListener((View v) -> {

            if(canCancelThisBooking(_currentBookingList)) {
                showCancelBookingDialog();
            } else {
                showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.coach_cancel_booking_hours_limit));
            }

        });
        mBinding.buttonResheduleSession.setOnClickListener((View v) -> {
            showRescheduleBookingDialog();
        });
        mBinding.relUserInfo.setOnClickListener((View v) -> {

            if(Utils.isNetworkAvailable()) {
                getLearnerProfile(true);
            } else {
                showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.no_internet));
            }

        });

        mBinding.ivChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startChat();
                if(!Utils.isCoachSessionCompleted(_currentBookingList)) {
                    if(Utils.isNetworkAvailable()) {
                        getLearnerProfile(false);
                    } else {
                        showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.no_internet));
                    }
                }
            }
        });

       //getWeatherAPI();
    }

    private boolean canCancelThisBooking(BookingList _currentBookingList) {
        boolean canCancel = false;

        if(_currentBookingList != null && _currentBookingList.getSessionDate() != null
                && !TextUtils.isEmpty(_currentBookingList.getSessionDate())) {
            String sessionDateTimeUTC = _currentBookingList.getSessionDate();
            String sessionDateTimeLocal = DateFormatUtils.convertUtcToLocal(sessionDateTimeUTC,
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
//            Log.d("mylog", "SessionCoachDetails - cancelSession - sessionDateTimeUTC : "
//                    + sessionDateTimeUTC + " :: sessionDateTimeLocal : " + sessionDateTimeLocal);
            String todayDateTime = Utils.getTodayDateTime();
//            Log.d("mylog", "SessionCoachDetails - cancelSession - todayDateTime : " + todayDateTime);

            long diffInHours = DateFormatUtils.getHoursDifference(todayDateTime, sessionDateTimeLocal);
//            Log.d("mylog", "SessionCoachDetails - cancelSession - diffInHours : " + diffInHours);

            if(diffInHours > 24) {
                canCancel = true;
            }
        }

        return canCancel;
    }

    private void showRescheduleBookingDialog() {

        Alert.showAlertDialogWithCancel(this, "RESCHEDULE BOOKING", "Would you like to reschedule your\nbooking instead of cancelling it?", "NO",
                "YES", false, "",
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(_currentBookingList !=  null) {
                            if(_currentBookingList.getStatus() == Utils.STATUS_BOOKING.CANCELLED
                                    || _currentBookingList.getStatus() == Utils.STATUS_BOOKING.COMPLETED) {
                                showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.can_not_reschedule_booking));
                            } else {
                                if(Utils.isNetworkAvailable()) {
                                    //rescheduleBooking();
                                    getLearnerProfile(false);
                                } else {
                                    showSnackbarFromTop(SessionCoachDetails.this,
                                            getResources().getString(R.string.no_internet));
                                }
                            }
                        }
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });

    }

//    private void rescheduleBooking() {
//        // Start chat with learner from here.
//        startChat();
//    }

    private void getLearnerProfile(boolean isViewingProfile) {
        if (Utils.isNetworkAvailable()) {
            if (_currentBookingList != null) {
                showProgressBar();
                AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
                authWebServices.getLearnerProfile(_currentBookingList.getLearnerId()).enqueue(new BaseCallback<LearnerProfile>(this) {
                    @Override
                    public void onSuccess(LearnerProfile response) {
                        if(response != null) {
                            if(response.getStatus() == 1) {
                                if(isViewingProfile) {
                                    ProfileLearner profileLearner = response.getResult();
                                    if (profileLearner != null) {
                                        profileLearner.setJid(_currentBookingList.getLearnerJid());
                                        profileLearner.setJkey(_currentBookingList.getLearnerJkey());
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, true);
                                        bundle.putBoolean(Constants.BundleKey.IS_COACH_SESSION_COMPLETED, Utils.isCoachSessionCompleted(_currentBookingList));
                                        bundle.putParcelable(Constants.BundleKey.DATA, profileLearner);
                                        bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, buildReportUserRequest(_currentBookingList));
                                        Navigator.getInstance().navigateToActivityWithData(
                                                SessionCoachDetails.this,
                                                com.sportsstars.ui.learner.ViewProfileActivity.class,
                                                bundle);
                                    }
                                } else {
                                    startChat();
                                }
                            } else {
                                try {
                                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                        showSnackbarFromTop(SessionCoachDetails.this, response.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(Call<LearnerProfile> call, BaseResponse baseResponse) {
                        try {
                            hideProgressBar();
                            if (baseResponse != null && baseResponse.getMessage() != null
                                    && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                //Log.d("mylog", baseResponse.getMessage());
                                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    private void startChat() {
        if(_currentBookingList != null) {
            String userName = _currentBookingList.getLearnerJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, _currentBookingList.getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, _currentBookingList.getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, _currentBookingList.getLearnerJid().replace("246","260"));
//                intent.putExtra(AppConstants.KEY_TO_USER, _currentBookingList.getLearnerJid());
                startActivity(intent);
            } else {
                showToast("Username of this profile not found to initiate chat.");
                //com.appster.chatlib.utils.Utils.showToast(this, "enter friend username");
            }
        }
    }

    private void showCancelBookingDialog() {

        Alert.showAlertDialogWithCancel(this,
                getResources().getString(R.string.cancel_booking_dialog_title),
                getResources().getString(R.string.cancel_booking_coach_dialog_message),
                getResources().getString(R.string.no_return),
                getResources().getString(R.string.yes_cancel),
                false,
                "",
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(_currentBookingList !=  null) {
                            if(_currentBookingList.getStatus() == Utils.STATUS_BOOKING.CANCELLED
                                    || _currentBookingList.getStatus() == Utils.STATUS_BOOKING.COMPLETED) {
                                showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.can_not_cancel_booking));
                            } else {
                                if(Utils.isNetworkAvailable()) {
                                    cancelBooking();
                                } else {
                                    showSnackbarFromTop(SessionCoachDetails.this,
                                            getResources().getString(R.string.no_internet));
                                }
                            }
                        }
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });
    }

    @Override
    public String getActivityName() {
        return "SessionDetailsLearnerActivity";
    }

    private void getSessionDetails(int _sessionId) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.getSessionDetails(_sessionId).enqueue(new BaseCallback<SessionDetailsResponse>(SessionCoachDetails.this) {
            @Override
            public void onSuccess(SessionDetailsResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        _currentBookingList = response.getResult();
                        setDataOnUI(response.getResult());
                        getWeatherAPI();
                    }
                } else {
                    try {
                        mBinding.llCancelReschedule.setVisibility(View.GONE);
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionCoachDetails.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<SessionDetailsResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });

    }

    private void setBookMark() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.setBookMark(createPayloadForBookMark()).enqueue(new BaseCallback<BookMarkResponse>(SessionCoachDetails.this) {
            @Override
            public void onSuccess(BookMarkResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if (_currentBookingList.getBookmarkedId() == 0) {
                        _currentBookingList.setBookmarkedId(1);
                    } else {
                        _currentBookingList.setBookmarkedId(0);
                    }
                    //  setbookMarkUI();
                } else {
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionCoachDetails.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BookMarkResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void openEmail(String email) {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Query");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    private void cancelBooking() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.cancelBooking(createPayloadForBookMark()).enqueue(new BaseCallback<BookMarkResponse>(SessionCoachDetails.this) {
            @Override
            public void onSuccess(BookMarkResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    calendarSyncDelete(_currentBookingList);
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionCoachDetails.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BookMarkResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void calendarSyncDelete(BookingList bookingList) {
        if(bookingList != null) {
            processToShowDialog();
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
            CalendarSyncPostRequest calendarSyncPostRequest = new CalendarSyncPostRequest();
            calendarSyncPostRequest.setSessionId(bookingList.getSessionId());
            calendarSyncPostRequest.setCoachId(bookingList.getCoachId());
            calendarSyncPostRequest.setLearnerId(bookingList.getLearnerId());
            calendarSyncPostRequest.setSessionDate(bookingList.getSessionDate());
            authWebServices.calendarSyncDelete(calendarSyncPostRequest).enqueue(new BaseCallback<BaseResponse>(this) {
                @Override
                public void onSuccess(BaseResponse response) {
                    if(response != null && response.getStatus() == 1) {

                        CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
                        calendarSyncResponse.setCoachName(bookingList.getCoachName());
                        calendarSyncResponse.setFacility(bookingList.getFacilityAddress());
                        calendarSyncResponse.setLearnerName(bookingList.getName());
                        calendarSyncResponse.setSessionDate(bookingList.getSessionDate());
                        calendarSyncResponse.setSportName(bookingList.getSportsName());
                        calendarSyncResponse.setTitle("StarFinda Coaching Session with "
                                + bookingList.getName() + " for "
                                + bookingList.getSportsName());
                        CalendarSyncHelper.getInstance().deleteCalendarEvent(SessionCoachDetails.this, calendarSyncResponse);

                        //showToast(getResources().getString(R.string.session_cancelled));

                        Alert.showAlertDialog(SessionCoachDetails.this,
                                "BOOKING CANCELLED",
                                "You have successfully cancelled your session with " + _currentBookingList.getName() + ".",
                                getString(R.string.txt_dlg_btn_ok),
                                SessionCoachDetails.this);
                    }
                }

                @Override
                public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                    try {
                        hideProgressBar();
                        if(baseResponse != null && baseResponse.getMessage() != null
                                && !TextUtils.isEmpty(baseResponse.getMessage())) {
                            //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    BookMarkRequest createPayloadForBookMark() {
        BookMarkRequest request = new BookMarkRequest();
        request.setSessionId(mSessionId);
        return request;
    }

    private void setDataOnUI(BookingList _bookingList) {
        if (_bookingList.getProfileImage() != null
                && !_bookingList.getProfileImage().equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(_bookingList.getProfileImage())
                    .placeholder(R.drawable.ic_user_green)
                    .error(R.drawable.ic_user_green)
                    .into(mBinding.imageTopBanner);
        }

        if (_bookingList.getProfileImage() != null
                && !_bookingList.getProfileImage().equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(_bookingList.getProfileImage())
                    .placeholder(R.drawable.ic_user_green)
                    .error(R.drawable.ic_user_green)
                    .into(mBinding.ivBackgroundImage);
        }

        mBinding.tvName.setText(_bookingList.getName());
        mBinding.tvUpcoming.setText(Utils.getCurrentAndUpcomingText(_bookingList.getStatus()));
        mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(SessionCoachDetails.this, _bookingList.getStatus()));

        String utcDateTime = _bookingList.getSessionDate();
        String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
        Log.d("mylog", "SessionCoachDetails - utcDateTime : " + utcDateTime
                + " :: localDateTime : " + localDateTime);
        mBinding.time.setText(Utils.parseTimeForDDMM(localDateTime));
        mBinding.date.setText(Utils.parseTimeForDDMMYY(localDateTime));

        mBinding.price.setText("$" + _bookingList.getSessionPrice());
        mBinding.tvSportName.setText(_bookingList.getSportsName());
        if (_bookingList.getIcon() != null
                && !_bookingList.getIcon().equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(_bookingList.getIcon())
                    .placeholder(R.drawable.ic_cricket_green)
                    .error(R.drawable.ic_cricket_green)
                    .into(mBinding.ivSportImage);
        }
        mBinding.tvSportName.setText(_bookingList.getSportsName());

        switch (_bookingList.getBookingType()) {
            case Constants.BOOKING_TYPE.ONE_ON_ONE:
                mBinding.tvOneOnOne.setText(getString(R.string.group_one_on_one));
                break;
            case Constants.BOOKING_TYPE.SMALL:
                mBinding.tvOneOnOne.setText(getString(R.string.group_small));
                break;
            case Constants.BOOKING_TYPE.LARGE:
                mBinding.tvOneOnOne.setText(getString(R.string.group_large));
                break;
        }

        mBinding.tvHandsOn.setText(_bookingList.getSessionType() == Constants.SESSION_TYPE.HANDS_ON ? getString(R.string.txt_handsOn) : getString(R.string.inspirational));
        mBinding.tvDescVal.setText(_bookingList.getSessionType() == Constants.SESSION_TYPE.HANDS_ON ? getString(R.string.hands_on_info) : getString(R.string.inspirational_info));
        //mBinding.tvDescVal.setText(_bookingList.getDescription());
        mBinding.tvCoachName.setText(_bookingList.getName());
        mBinding.tvFacilityName.setText(_bookingList.getFacilityName());
        mBinding.tvFacilityAddress.setText(_bookingList.getFacilityAddress());
        mBinding.llGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + _bookingList.getFacilityLat() + ">,<" + _bookingList.getFacilityLong() + ">?q=<" + _bookingList.getFacilityLat() + ">,<" + _bookingList.getFacilityLong() + ">(" + _bookingList.getFacilityName() + ")"));
                startActivity(intent);
            }
        });

        if (_bookingList.getPrevBookings() > 1) {
            mBinding.tvCoachType.setText("You have coached " + _bookingList.getName() + " before");
        } else {
            mBinding.tvCoachType.setText("You have not coached " + _bookingList.getName() + " before");
        }

//        if (_bookingList.getCoachType() == Constants.COACH_TYPE.PRIVATE) {
//            mBinding.tvCoachType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_private_coach, 0, 0, 0);
//            mBinding.tvCoachType.setText(getString(R.string.private_text));
//
//        } else if (_bookingList.getCoachType() == Constants.COACH_TYPE.STAR) {
//            mBinding.tvCoachType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_star_green, 0, 0, 0);
//            mBinding.tvCoachType.setText(getString(R.string.star));
//
//        }
        if (_bookingList.getProfileImage() != null
                && !_bookingList.getProfileImage().equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(_bookingList.getProfileImage())
                    .placeholder(R.drawable.ic_user_green)
                    .error(R.drawable.ic_user_green)
                    .into(mBinding.imageUser);
        }

        if(Utils.isCoachSessionCompleted(_bookingList)) {
            //mBinding.ivChat.setImageResource(R.drawable.ic_report_user);
            mBinding.llCancelReschedule.setVisibility(View.GONE);
            mBinding.ivChat.setImageResource(R.drawable.ic_message);
            mBinding.ivChat.setVisibility(View.INVISIBLE);
        } else {
            mBinding.llCancelReschedule.setVisibility(View.VISIBLE);
            mBinding.ivChat.setImageResource(R.drawable.ic_message);
        }

        mBinding.ivChat.setOnClickListener((View v) -> {
            if(Utils.isCoachSessionCompleted(_bookingList)) {
//                if(Utils.isNetworkAvailable()) {
//                    launchReportUser(_bookingList);
//                } else {
//                    showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.no_internet));
//                }
            } else {
                if(Utils.isNetworkAvailable()) {
                    getLearnerProfile(false);
                } else {
                    showSnackbarFromTop(SessionCoachDetails.this, getResources().getString(R.string.no_internet));
                }
            }
        });

    }

    private void launchReportUser(BookingList _bookingList) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, buildReportUserRequest(_bookingList));
        Navigator.getInstance().navigateToActivityWithData(SessionCoachDetails.this,
                ReportUserActivity.class, bundle);
    }

    private ReportUserRequest buildReportUserRequest(BookingList _bookingList) {
        ReportUserRequest reportUserRequest = new ReportUserRequest();
        reportUserRequest.setReportTo(_bookingList.getLearnerId());
        reportUserRequest.setReportToPersona(Constants.USER_ROLE.LEARNER);
        reportUserRequest.setReportFromPersona(Constants.USER_ROLE.COACH);
        reportUserRequest.setReportText("");

        return reportUserRequest;
    }

    private void getWeatherAPI() {
        if(_currentBookingList != null
                && !TextUtils.isEmpty(_currentBookingList.getFacilityCity())
                && !TextUtils.isEmpty(_currentBookingList.getFacilityZipcode())) {
            Calendar calendar = Calendar.getInstance();

            int dateInt = calendar.get(Calendar.DATE);
            int monthInt = calendar.get(Calendar.MONTH);
            monthInt = monthInt +1;
            int yearInt = calendar.get(Calendar.YEAR);

            String dateStr = "";
            String monthStr = "";

            if(dateInt < 10) {
                dateStr = "0"+dateInt;
            }

            if (monthInt < 10) {
                monthStr = "0"+monthInt;
            }

            String dateToday = ""+yearInt+"-"+monthStr+"-"+dateStr;
            //Log.d("mylog", "getWeatherAPI - dateToday : " + dateToday);
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
//        authWebServices.getWeatherInformation("Melbourne", dateToday).enqueue(new BaseCallback<WeatherInformationResponse>(this) {
//            @Override
//            public void onSuccess(WeatherInformationResponse response) {
//                if(response != null && response.getStatus() == 1) {
//                    updateWeatherUI(response);
//                }
//            }
//
//            @Override
//            public void onFail(Call<WeatherInformationResponse> call, BaseResponse baseResponse) {
//                try {
//                    if(baseResponse != null && baseResponse.getMessage() != null
//                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
//                        //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

            String sessionDateTime = _currentBookingList.getSessionDate();
            //String[] sessionDateTimeArr = sessionDateTime.split(" ");
            //String sessionDate = sessionDateTimeArr[0];

            authWebServices.getWeatherInformation(""+_currentBookingList.getFacilityCity(),
                    ""+_currentBookingList.getFacilityZipcode(), ""+sessionDateTime)
                    .enqueue(new BaseCallback<WeatherInformationResponse>(this) {
                        @Override
                        public void onSuccess(WeatherInformationResponse response) {
                            if(response != null && response.getStatus() == 1) {
                                if(response.getResult() != null) {
                                    showWeatherUI(true);
                                    updateWeatherUI(response);
                                } else {
                                    showWeatherUI(false);
                                }
                            } else {
                                showWeatherUI(false);
                            }
                        }

                        @Override
                        public void onFail(Call<WeatherInformationResponse> call, BaseResponse baseResponse) {
                            try {
                                hideProgressBar();
                                showWeatherUI(false);
                                if(baseResponse != null && baseResponse.getMessage() != null
                                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                    //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }
    }

    private void showWeatherUI(boolean showInfo) {
        if(showInfo) {
            mBinding.rlWeatherInfo.setVisibility(View.VISIBLE);
            mBinding.rlNoWeather.setVisibility(View.GONE);
        } else {
            mBinding.rlWeatherInfo.setVisibility(View.GONE);
            mBinding.rlNoWeather.setVisibility(View.VISIBLE);
        }
    }

    private void updateWeatherUI(WeatherInformationResponse response) {
        WeatherInfo weatherInfo = response.getResult();
        if(weatherInfo != null) {
            if (weatherInfo.getWeatherIcon() != null
                    && !weatherInfo.getWeatherIcon().equalsIgnoreCase("")) {
                Picasso.with(this)
                        .load(weatherInfo.getWeatherIcon())
                        .into(mBinding.ivWeather);
            }
            if(!TextUtils.isEmpty(weatherInfo.getTemperatureValue())
                    && !TextUtils.isEmpty(weatherInfo.getTemperatureUnit())) {
                String tempStr = ""+weatherInfo.getTemperatureValue();
                String newTempStr = "";
                if(tempStr.contains(".")) {
                    newTempStr = tempStr.substring(0, tempStr.lastIndexOf("."));
//                    String[] tempStrArr = tempStr.split(".");
//                    if(tempStrArr != null && tempStrArr.length > 1) {
//                        newTempStr = tempStrArr[0];
//                    } else {
//                        newTempStr = tempStr;
//                    }
                } else {
                    newTempStr = tempStr;
                }
                mBinding.tvTemperature.setText(Html.fromHtml(""+newTempStr+Constants.DEGREE_IN_UNICODE));
            }
            if(!TextUtils.isEmpty(weatherInfo.getHumidityValue())
                    && !TextUtils.isEmpty(weatherInfo.getHumidityUnit())) {
                try {
                    Double humidityValDouble = Double.parseDouble(weatherInfo.getHumidityValue());
                    int humidityValInt = humidityValDouble.intValue();
                    mBinding.tvHumidity.setText("Humidity: " + humidityValInt + weatherInfo.getHumidityUnit());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(!TextUtils.isEmpty(weatherInfo.getWindSpeedValue())
                    && !TextUtils.isEmpty(weatherInfo.getWindSpeedUnit())) {
                if(weatherInfo.getWindSpeedUnit().equalsIgnoreCase("mph")) {
                    try {
                        Double windSpeedMph = Double.parseDouble(weatherInfo.getWindSpeedValue());
                        Double windSpeedKMph = windSpeedMph * 1.60;
                        int windSpeedInt = windSpeedKMph.intValue();
                        mBinding.tvWindSpeed.setText("Wind Speed: "+windSpeedInt+" km/h");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        //Double windSpeedKMph = Double.parseDouble(weatherInfo.getWindSpeedValue());
                        //int windSpeedInt = windSpeedKMph.intValue();
                        int kmphSpeed = Utils.convertMpsToKmph(Double.parseDouble(weatherInfo.getWindSpeedValue()));
                        mBinding.tvWindSpeed.setText("Wind Speed: "+kmphSpeed+" km/h");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onOkay() {
        finish();
    }

}
