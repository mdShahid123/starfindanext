package com.sportsstars.ui.session;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.appster.chatlib.constants.AppConstants;
import com.sportsstars.MVVM.entities.mysession.BookingList;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.databinding.ActivitySessionNewBinding;
import com.sportsstars.model.RescheduleBookingModel;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.BookMarkRequest;
import com.sportsstars.network.request.auth.CalendarSyncPostRequest;
import com.sportsstars.network.request.auth.ReportUserRequest;
import com.sportsstars.network.response.BookMarkResponse;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.response.SessionDetailsResponse;
import com.sportsstars.network.response.WeatherInfo;
import com.sportsstars.network.response.WeatherInformationResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.ViewProfileActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.settings.ReportUserActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import retrofit2.Call;

public class SessionDetailsLearnerActivity extends BaseActivity implements Alert.OnOkayClickListener {

    ActivitySessionNewBinding mBinding;
    int mSessionId;
    BookingList _currentBookingList = new BookingList();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_session_new);
        mSessionId = getIntent().getIntExtra(getString(R.string.booking_id_extra), 0);
        getSessionDetails(mSessionId);
//        mBinding.imgBookmark.setOnClickListener((View v) -> {
//            setBookMark();
//        });
//        mBinding.ivChat.setOnClickListener((View v) -> {
////            setBookMark();
//            startChat();
//
//        });

        mBinding.buttonCancelSession.setOnClickListener((View v) -> {

            checkCancellationValidity(_currentBookingList);

        });
        mBinding.buttonResheduleSession.setOnClickListener((View v) -> {

            if(canRescheduleThisBooking(_currentBookingList)) {
                showRescheduleBookingDialog();
            } else {
                showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.msg_reschedule_less_than_24_hr));
            }

        });

        mBinding.relUserInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable()) {
                    //Navigator.getInstance().navigateToActivity(SessionDetailsLearnerActivity.this, CoachViewProfileActivity.class);
                    if(_currentBookingList != null) {
                        getCoachProfile(_currentBookingList.getCoachId(), true);
                    }
                } else {
                    showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });

        //getWeatherAPI();
    }


    //******* This API is not in use, not getting proper information from this API, need to check with backend. ******//
//    private void checkCancellation() {
//        processToShowDialog();
//        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
//        webServices.checkCancellation(_currentBookingList.getSessionId()).enqueue(new BaseCallback<BaseResponse>(SessionDetailsLearnerActivity.this) {
//            @Override
//            public void onSuccess(BaseResponse response) {
//                if(response != null && response.getStatus() == 1) {
//                    showToast("check cancellation fetched successfully !!");
//                } else {
//                    showToast("check cancellation failed");
//                }
//            }
//
//            @Override
//            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
//            }
//        });
//    }

//    private void startChat() {
//        if(_currentBookingList!=null)
//        {
//            String userName = _currentBookingList.getCoachJid().split("@")[0];
//            if (!TextUtils.isEmpty(userName)) {
//                Intent intent = new Intent(this, ChatThreadActivity.class);
////                intent.putExtra(AppConstants.KEY_TO_USERNAME, userName);
//                intent.putExtra(AppConstants.KEY_TO_USERNAME, _currentBookingList.getCoachName());
//                intent.putExtra(AppConstants.KEY_TO_USER_PIC, _currentBookingList.getCoachProfileImage());
//                intent.putExtra(AppConstants.KEY_TO_USER, _currentBookingList.getCoachJid());
//                startActivity(intent);
//            } else {
//                com.appster.chatlib.utils.Utils.showToast(this, "enter friend username");
//            }
//        }
//    }

    private void getCoachProfile(int id, boolean isViewingProfile) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfileDetail(id).enqueue(new BaseCallback<GetProfileResponse>(SessionDetailsLearnerActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null) {
                            UserProfileInstance.getInstance().setUserProfileModel(response.getResult());
                            if(isViewingProfile) {
                                launchViewProfile();
                            } else {
                                startChat();
                            }
                        }
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(SessionDetailsLearnerActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SessionDetailsLearnerActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void launchViewProfile() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, true);
        bundle.putBoolean(Constants.BundleKey.IS_COACH_SESSION_COMPLETED, Utils.isCoachSessionCompleted(_currentBookingList));
        bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, buildReportUserRequest(_currentBookingList));
        Navigator.getInstance().navigateToActivityWithData(SessionDetailsLearnerActivity.this, ViewProfileActivity.class, bundle);
    }

    private ReportUserRequest buildReportUserRequest(BookingList _bookingList) {
        ReportUserRequest reportUserRequest = new ReportUserRequest();
        reportUserRequest.setReportTo(_bookingList.getCoachId());
        reportUserRequest.setReportToPersona(Constants.USER_ROLE.COACH);
        reportUserRequest.setReportFromPersona(Constants.USER_ROLE.LEARNER);
        reportUserRequest.setReportText("");

        return reportUserRequest;
    }

    private void showCancelBookingDialog(String cancellationMessage) {

        Alert.showAlertDialogWithCancel(this,
                getResources().getString(R.string.cancel_booking_dialog_title),
                getResources().getString(R.string.cancel_booking_learner_dialog_message),
                getResources().getString(R.string.no_return),
                getResources().getString(R.string.yes_cancel),
                true, cancellationMessage,
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(_currentBookingList !=  null) {
                            if(_currentBookingList.getStatus() == Utils.STATUS_BOOKING.CANCELLED
                                    || _currentBookingList.getStatus() == Utils.STATUS_BOOKING.COMPLETED) {
                                showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.can_not_cancel_booking));
                            } else {
                                if(Utils.isNetworkAvailable()) {
                                    cancelBooking();
                                } else {
                                    showSnackbarFromTop(SessionDetailsLearnerActivity.this,
                                            getResources().getString(R.string.no_internet));
                                }
                            }
                        }
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });

    }

    private void showRescheduleBookingDialog() {

        Alert.showAlertDialogWithCancel(this,
                getResources().getString(R.string.reschedule_booking_dialog_title),
                getResources().getString(R.string.reschedule_booking_dialog_message),
                getResources().getString(R.string.no),
                getResources().getString(R.string.yes),
                false, "",
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(_currentBookingList !=  null) {
                            if(_currentBookingList.getStatus() == Utils.STATUS_BOOKING.CANCELLED
                                    || _currentBookingList.getStatus() == Utils.STATUS_BOOKING.COMPLETED) {
                                showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.can_not_reschedule_booking));
                            } else {
                                if(Utils.isNetworkAvailable()) {
                                    rescheduleBooking();
                                } else {
                                    showSnackbarFromTop(SessionDetailsLearnerActivity.this,
                                            getResources().getString(R.string.no_internet));
                                }
                            }
                        }
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });

    }

    private void rescheduleBooking() {
        if(_currentBookingList != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.BundleKey.RESCHEDULE_DATA, buildRescheduleModel());
//            bundle.putInt(Constants.BundleKey.SESSION_ID, _currentBookingList.getSessionId());
//            bundle.putInt(Constants.BundleKey.SLOT_ID, _currentBookingList.getSlotId());
//            bundle.putInt(Constants.BundleKey.COACH_ID, _currentBookingList.getCoachId());
//            bundle.putString(Constants.BundleKey.COACH_NAME, _currentBookingList.getCoachName());
//            bundle.putString(Constants.BundleKey.SPORTS_NAME, _currentBookingList.getSportsName());
            Navigator.getInstance().navigateToActivityForResultWithData(
                    SessionDetailsLearnerActivity.this,
                    RescheduleBookingActivity.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_RESCHEDULE_BOOKING);
        }
    }

    private RescheduleBookingModel buildRescheduleModel() {
        RescheduleBookingModel rescheduleModel = new RescheduleBookingModel();
        rescheduleModel.setSessionId(_currentBookingList.getSessionId());
        rescheduleModel.setSlotId(_currentBookingList.getSlotId());
        rescheduleModel.setCoachId(_currentBookingList.getCoachId());
        rescheduleModel.setCoachName(_currentBookingList.getCoachName());
        rescheduleModel.setLearnerId(_currentBookingList.getLearnerId());
        rescheduleModel.setLearnerName(_currentBookingList.getName());
        rescheduleModel.setSportsName(_currentBookingList.getSportsName());
        rescheduleModel.setFacilityAddress(_currentBookingList.getFacilityAddress());
        rescheduleModel.setSessionDateTime(_currentBookingList.getSessionDate());

        return rescheduleModel;
    }

    private boolean canRescheduleThisBooking(BookingList _currentBookingList) {
        boolean canCancel = false;

        if(_currentBookingList != null && _currentBookingList.getSessionDate() != null
                && !TextUtils.isEmpty(_currentBookingList.getSessionDate())) {
            String sessionDateTimeUTC = _currentBookingList.getSessionDate();
            String sessionDateTimeLocal = DateFormatUtils.convertUtcToLocal(sessionDateTimeUTC,
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
//            Log.d("mylog", "SessionCoachDetails - cancelSession - sessionDateTimeUTC : "
//                    + sessionDateTimeUTC + " :: sessionDateTimeLocal : " + sessionDateTimeLocal);
            String todayDateTime = Utils.getTodayDateTime();
//            Log.d("mylog", "SessionCoachDetails - cancelSession - todayDateTime : " + todayDateTime);

            long diffInHours = DateFormatUtils.getHoursDifference(todayDateTime, sessionDateTimeLocal);
//            Log.d("mylog", "SessionCoachDetails - cancelSession - diffInHours : " + diffInHours);

            if(diffInHours > 24) {
                canCancel = true;
            }
        }

        return canCancel;
    }

    private void checkCancellationValidity(BookingList _currentBookingList) {
        if(_currentBookingList != null && _currentBookingList.getSessionDate() != null
                && !TextUtils.isEmpty(_currentBookingList.getSessionDate())) {
            String sessionDateTimeUTC = _currentBookingList.getSessionDate();
            String sessionDateTimeLocal = DateFormatUtils.convertUtcToLocal(sessionDateTimeUTC,
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
//            Log.d("mylog", "SessionCoachDetails - cancelSession - sessionDateTimeUTC : "
//                    + sessionDateTimeUTC + " :: sessionDateTimeLocal : " + sessionDateTimeLocal);
            String todayDateTime = Utils.getTodayDateTime();
//            Log.d("mylog", "SessionCoachDetails - cancelSession - todayDateTime : " + todayDateTime);

            long diffInHours = DateFormatUtils.getHoursDifference(todayDateTime, sessionDateTimeLocal);
//            Log.d("mylog", "SessionCoachDetails - cancelSession - diffInHours : " + diffInHours);

            if(diffInHours > 72) {
                showCancelBookingDialog(getResources().getString(R.string.msg_cancel_more_than_72_hr));
            } else if(diffInHours > 24 && diffInHours <=72) {
                showCancelBookingDialog(getResources().getString(R.string.msg_cancel_more_than_24_hr_less_than_72_hr));
            } else if(diffInHours <= 24) {
                showCancelBookingDialog(getResources().getString(R.string.msg_cancel_less_than_24_hr));
            }
        }
    }

    @Override
    public String getActivityName() {
        return "SessionDetailsLearnerActivity";
    }

    private void getSessionDetails(int _sessionId) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.getSessionDetails(_sessionId).enqueue(new BaseCallback<SessionDetailsResponse>(SessionDetailsLearnerActivity.this) {
            @Override
            public void onSuccess(SessionDetailsResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        _currentBookingList = response.getResult();
                        setDataOnUI(response.getResult());
                        getWeatherAPI();
                    }
                } else {
                    try {
                        mBinding.llCancelReschedule.setVisibility(View.GONE);
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionDetailsLearnerActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<SessionDetailsResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });

    }

    private void setBookMark() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.setBookMark(createPayloadForBookMark()).enqueue(new BaseCallback<BookMarkResponse>(SessionDetailsLearnerActivity.this) {
            @Override
            public void onSuccess(BookMarkResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if (_currentBookingList.getBookmarkedId() == 0) {
                        _currentBookingList.setBookmarkedId(1);
                    } else {
                        _currentBookingList.setBookmarkedId(0);
                    }
                    setbookMarkUI();

                    try {
                        if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            Alert.showAlertDialog(SessionDetailsLearnerActivity.this,
                                    getString(R.string.success),
                                    response.getMessage(),
                                    getString(R.string.okay),
                                    new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {

                                        }
                                    });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionDetailsLearnerActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BookMarkResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void cancelBooking() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.cancelBooking(createPayloadForBookMark()).enqueue(new BaseCallback<BookMarkResponse>(SessionDetailsLearnerActivity.this) {
            @Override
            public void onSuccess(BookMarkResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    mBinding.imgChat.setVisibility(View.GONE);
                    calendarSyncDelete(_currentBookingList);
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionDetailsLearnerActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BookMarkResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void calendarSyncDelete(BookingList bookingList) {
        if(bookingList != null) {
            processToShowDialog();
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
            CalendarSyncPostRequest calendarSyncPostRequest = new CalendarSyncPostRequest();
            calendarSyncPostRequest.setSessionId(bookingList.getSessionId());
            calendarSyncPostRequest.setCoachId(bookingList.getCoachId());
            calendarSyncPostRequest.setLearnerId(bookingList.getLearnerId());
            calendarSyncPostRequest.setSessionDate(bookingList.getSessionDate());
            authWebServices.calendarSyncDelete(calendarSyncPostRequest).enqueue(new BaseCallback<BaseResponse>(this) {
                @Override
                public void onSuccess(BaseResponse response) {
                    if(response != null && response.getStatus() == 1) {

                        CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
                        calendarSyncResponse.setCoachName(bookingList.getCoachName());
                        calendarSyncResponse.setFacility(bookingList.getFacilityAddress());
                        calendarSyncResponse.setLearnerName(bookingList.getName());
                        calendarSyncResponse.setSessionDate(bookingList.getSessionDate());
                        calendarSyncResponse.setSportName(bookingList.getSportsName());
                        calendarSyncResponse.setTitle("StarFinda Coaching Session with "
                                + bookingList.getName() + " for "
                                + bookingList.getSportsName());
                        CalendarSyncHelper.getInstance().deleteCalendarEvent(SessionDetailsLearnerActivity.this, calendarSyncResponse);

                        //finish();
                        //showToast(getResources().getString(R.string.session_cancelled));

                        Alert.showAlertDialog(SessionDetailsLearnerActivity.this,
                                "BOOKING CANCELLED",
                                "You have successfully cancelled your session with " + _currentBookingList.getCoachName() + ".",
                                getString(R.string.txt_dlg_btn_ok),
                                SessionDetailsLearnerActivity.this);
                    }
                }

                @Override
                public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                    try {
                        hideProgressBar();
                        if(baseResponse != null && baseResponse.getMessage() != null
                                && !TextUtils.isEmpty(baseResponse.getMessage())) {
                            //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    BookMarkRequest createPayloadForBookMark() {
        BookMarkRequest request = new BookMarkRequest();
        request.setSessionId(mSessionId);
        return request;
    }

    private void setDataOnUI(BookingList _bookingList) {
        if (_bookingList.getCoachProfileImage() != null && !_bookingList.getCoachProfileImage().equalsIgnoreCase(""))
            Picasso.with(this).load(_bookingList.getCoachProfileImage()).placeholder(R.drawable.ic_user_green).error(R.drawable.ic_user_green).into(mBinding.imageTopBanner);
        if (_bookingList.getCoachProfileImage() != null && !_bookingList.getCoachProfileImage().equalsIgnoreCase(""))
            Picasso.with(this).load(_bookingList.getCoachProfileImage()).placeholder(R.drawable.ic_user_green).error(R.drawable.ic_user_green).into(mBinding.ivBackgroundImage);
        mBinding.tvName.setText(_bookingList.getCoachName());
        mBinding.tvUpcoming.setText(Utils.getCurrentAndUpcomingText(_bookingList.getStatus()));
        mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(SessionDetailsLearnerActivity.this, _bookingList.getStatus()));

        String utcDateTime = _bookingList.getSessionDate();
        String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
        Log.d("mylog", "SessionDetailsLearnerActivity - utcDateTime : " + utcDateTime
                + " :: localDateTime : " + localDateTime);

        mBinding.time.setText(Utils.parseTimeForDDMM(localDateTime));
        mBinding.date.setText(Utils.parseTimeForDDMMYY(localDateTime));
        mBinding.price.setText("$" + _bookingList.getSessionPrice());
        mBinding.tvSportName.setText(_bookingList.getSportsName());
        if (_bookingList.getIcon() != null && !_bookingList.getIcon().equalsIgnoreCase(""))
            Picasso.with(this).load(_bookingList.getIcon()).placeholder(R.drawable.ic_cricket_green).error(R.drawable.ic_cricket_green).into(mBinding.sportsImage);
        mBinding.tvSportName.setText(_bookingList.getSportsName());

        switch (_bookingList.getBookingType()) {
            case Constants.BOOKING_TYPE.ONE_ON_ONE:
                mBinding.tvGroupName.setText(getString(R.string.group_one_on_one));
                break;
            case Constants.BOOKING_TYPE.SMALL:
                mBinding.tvGroupName.setText(getString(R.string.group_small));
                break;
            case Constants.BOOKING_TYPE.LARGE:
                mBinding.tvGroupName.setText(getString(R.string.group_large));
                break;
        }

        mBinding.tvQuality.setText(_bookingList.getSessionType() == Constants.SESSION_TYPE.HANDS_ON ? getString(R.string.txt_handsOn) : getString(R.string.inspirational));
        mBinding.tvDescVal.setText(_bookingList.getSessionType() == Constants.SESSION_TYPE.HANDS_ON ? getString(R.string.hands_on_info) : getString(R.string.inspirational_info));
        //mBinding.tvDescVal.setText(_bookingList.getDescription());
        mBinding.tvCoachName.setText(_bookingList.getCoachName());
        //setbookMarkUI();

        if (_bookingList.getCoachType() == Constants.COACH_TYPE.PRIVATE) {
            mBinding.tvCoachType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_private_coach, 0, 0, 0);
            mBinding.tvCoachType.setText(getString(R.string.private_coach));

        } else if (_bookingList.getCoachType() == Constants.COACH_TYPE.STAR) {
            mBinding.tvCoachType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_star_green, 0, 0, 0);
            mBinding.tvCoachType.setText(getString(R.string.star_coach));

        }
        if (_bookingList.getCoachProfileImage() != null && !_bookingList.getCoachProfileImage().equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(_bookingList.getCoachProfileImage())
                    .placeholder(R.drawable.ic_user_circle)
                    .error(R.drawable.ic_user_circle)
                    .into(mBinding.imageUser);
        }

        mBinding.tvFacilityName.setText(_bookingList.getFacilityName());
        mBinding.tvFacilityAddress.setText(_bookingList.getFacilityAddress());
        mBinding.llGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + _bookingList.getFacilityLat() + ">,<" + _bookingList.getFacilityLong() + ">?q=<" + _bookingList.getFacilityLat() + ">,<" + _bookingList.getFacilityLong() + ">(" + _bookingList.getFacilityName() + ")"));
                startActivity(intent);
            }
        });

        if(Utils.isCoachSessionCompleted(_bookingList)) {
            //mBinding.imgBookmark.setImageResource(R.drawable.ic_report_user);
            mBinding.llCancelReschedule.setVisibility(View.GONE);
            mBinding.imgChat.setVisibility(View.GONE);
            setbookMarkUI();
        } else {
            mBinding.llCancelReschedule.setVisibility(View.VISIBLE);
            mBinding.imgChat.setVisibility(View.VISIBLE);
            setbookMarkUI();
        }

        mBinding.imgBookmark.setOnClickListener((View v) -> {
            if(Utils.isCoachSessionCompleted(_bookingList)) {
//                if(Utils.isNetworkAvailable()) {
//                    launchReportUser(_bookingList);
//                } else {
//                    showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.no_internet));
//                }

                if(Utils.isNetworkAvailable()) {
                    setBookMark();
                } else {
                    showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.no_internet));
                }
            } else {
                if(Utils.isNetworkAvailable()) {
                    setBookMark();
                } else {
                    showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });

        mBinding.imgChat.setOnClickListener((View v) -> {

            if(!Utils.isCoachSessionCompleted(_bookingList)) {
                if(Utils.isNetworkAvailable()) {
                    getCoachProfile(_bookingList.getCoachId(), false);
                } else {
                    showSnackbarFromTop(SessionDetailsLearnerActivity.this, getResources().getString(R.string.no_internet));
                }
            }

        });

    }

    private void startChat() {
        UserProfileModel userProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if(userProfileModel != null) {
            String userName = userProfileModel.getJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
//                intent.putExtra(AppConstants.KEY_TO_USERNAME, userName);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, userProfileModel.getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, userProfileModel.getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, userProfileModel.getJid());
                startActivity(intent);
            } else {
                showToast("Username of this profile not found to initiate chat.");
                //com.appster.chatlib.utils.Utils.showToast(this, "enter username");
            }
        }
    }

    private void launchReportUser(BookingList _bookingList) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, buildReportUserRequest(_bookingList));
        Navigator.getInstance().navigateToActivityWithData(SessionDetailsLearnerActivity.this,
                ReportUserActivity.class, bundle);
    }

    private void setbookMarkUI() {
        if (_currentBookingList.getBookmarkedId() == 0) {
            mBinding.imgBookmark.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_bookmark_inactive));
        } else {
            mBinding.imgBookmark.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_bookmark_active));
        }
    }

    private void getWeatherAPI() {
        if(_currentBookingList != null
                && !TextUtils.isEmpty(_currentBookingList.getFacilityCity())
                && !TextUtils.isEmpty(_currentBookingList.getFacilityZipcode())) {
            Calendar calendar = Calendar.getInstance();

            int dateInt = calendar.get(Calendar.DATE);
            int monthInt = calendar.get(Calendar.MONTH);
            monthInt = monthInt +1;
            int yearInt = calendar.get(Calendar.YEAR);

            String dateStr = "";
            String monthStr = "";

            if(dateInt < 10) {
                dateStr = "0"+dateInt;
            }

            if (monthInt < 10) {
                monthStr = "0"+monthInt;
            }

            String dateToday = ""+yearInt+"-"+monthStr+"-"+dateStr;
            //Log.d("mylog", "getWeatherAPI - dateToday : " + dateToday);
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
//        authWebServices.getWeatherInformation("Melbourne", dateToday).enqueue(new BaseCallback<WeatherInformationResponse>(this) {
//            @Override
//            public void onSuccess(WeatherInformationResponse response) {
//                if(response != null && response.getStatus() == 1) {
//                    updateWeatherUI(response);
//                }
//            }
//
//            @Override
//            public void onFail(Call<WeatherInformationResponse> call, BaseResponse baseResponse) {
//                try {
//                    if(baseResponse != null && baseResponse.getMessage() != null
//                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
//                        //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

            String sessionDateTime = _currentBookingList.getSessionDate();
            //String[] sessionDateTimeArr = sessionDateTime.split(" ");
            //String sessionDate = sessionDateTimeArr[0];

            authWebServices.getWeatherInformation(""+_currentBookingList.getFacilityCity(),
                    ""+_currentBookingList.getFacilityZipcode(), ""+sessionDateTime)
                    .enqueue(new BaseCallback<WeatherInformationResponse>(this) {
                        @Override
                        public void onSuccess(WeatherInformationResponse response) {
                            if(response != null && response.getStatus() == 1) {
                                if(response.getResult() != null) {
                                    showWeatherUI(true);
                                    updateWeatherUI(response);
                                } else {
                                    showWeatherUI(false);
                                }
                            } else {
                                showWeatherUI(false);
                            }
                        }

                        @Override
                        public void onFail(Call<WeatherInformationResponse> call, BaseResponse baseResponse) {
                            try {
                                hideProgressBar();
                                showWeatherUI(false);
                                if(baseResponse != null && baseResponse.getMessage() != null
                                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                    //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }
    }

    private void showWeatherUI(boolean showInfo) {
        if(showInfo) {
            mBinding.rlWeatherInfo.setVisibility(View.VISIBLE);
            mBinding.rlNoWeather.setVisibility(View.GONE);
        } else {
            mBinding.rlWeatherInfo.setVisibility(View.GONE);
            mBinding.rlNoWeather.setVisibility(View.VISIBLE);
        }
    }

    private void updateWeatherUI(WeatherInformationResponse response) {
        WeatherInfo weatherInfo = response.getResult();
        if(weatherInfo != null) {
            if (weatherInfo.getWeatherIcon() != null
                    && !weatherInfo.getWeatherIcon().equalsIgnoreCase("")) {
                Picasso.with(this)
                        .load(weatherInfo.getWeatherIcon())
                        .into(mBinding.ivWeather);
            }
            if(!TextUtils.isEmpty(weatherInfo.getTemperatureValue())
                    && !TextUtils.isEmpty(weatherInfo.getTemperatureUnit())) {
                //String tempStr = Html.fromHtml(""+weatherInfo.getTemperatureValue()+weatherInfo.getTemperatureUnit(), Html.FROM_HTML_MODE_LEGACY);
                String tempStr = ""+weatherInfo.getTemperatureValue();
                String newTempStr = "";
                if(tempStr.contains(".")) {
                    newTempStr = tempStr.substring(0, tempStr.lastIndexOf("."));
//                    String[] tempStrArr = tempStr.split(".");
//                    if(tempStrArr != null && tempStrArr.length > 1) {
//                        newTempStr = tempStrArr[0];
//                    } else {
//                        newTempStr = tempStr;
//                    }
                } else {
                    newTempStr = tempStr;
                }
                mBinding.tvTemperature.setText(Html.fromHtml(""+newTempStr+Constants.DEGREE_IN_UNICODE));
            }
            if(!TextUtils.isEmpty(weatherInfo.getHumidityValue())
                    && !TextUtils.isEmpty(weatherInfo.getHumidityUnit())) {
                try {
                    Double humidityValDouble = Double.parseDouble(weatherInfo.getHumidityValue());
                    int humidityValInt = humidityValDouble.intValue();
                    mBinding.tvHumidity.setText("Humidity: " + humidityValInt + weatherInfo.getHumidityUnit());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(!TextUtils.isEmpty(weatherInfo.getWindSpeedValue())
                    && !TextUtils.isEmpty(weatherInfo.getWindSpeedUnit())) {
                if(weatherInfo.getWindSpeedUnit().equalsIgnoreCase("mph")) {
                    try {
                        Double windSpeedMph = Double.parseDouble(weatherInfo.getWindSpeedValue());
                        Double windSpeedKMph = windSpeedMph * 1.60;
                        int windSpeedInt = windSpeedKMph.intValue();
                        mBinding.tvWindSpeed.setText("Wind Speed: "+windSpeedInt+" km/h");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        //Double windSpeedKMph = Double.parseDouble(weatherInfo.getWindSpeedValue());
                        //int windSpeedInt = windSpeedKMph.intValue();
                        int kmphSpeed = Utils.convertMpsToKmph(Double.parseDouble(weatherInfo.getWindSpeedValue()));
                        mBinding.tvWindSpeed.setText("Wind Speed: "+kmphSpeed+" km/h");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onOkay() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_RESCHEDULE_BOOKING) {
                getSessionDetails(mSessionId);
            }
        }
    }

}
