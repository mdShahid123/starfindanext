package com.sportsstars.ui.session;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySelectFacilityBinding;
import com.sportsstars.eventbus.LocationEvent;
import com.sportsstars.model.TrainingSession;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.CreateSessionResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

/**
 * Created by Ram on 10/01/18.
 */
public class SelectFacilityActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(SelectFacilityActivity.class);
    private static final int REQUEST_CODE_LOCATION = 101;
    private ActivitySelectFacilityBinding mBinder;
    private TrainingSession session;
    private double mLatitude;
    private double mLongitude;
    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_select_facility);

        mBinder.layLocation.setOnClickListener(this);
        mBinder.tvPublish.setOnClickListener(this);
        mBinder.ivBack.setOnClickListener(this);

        if (getIntent() != null) {
            if (getIntent().getParcelableExtra(Constants.EXTRA_SESSION_DATA) != null) {
                session = getIntent().getParcelableExtra(Constants.EXTRA_SESSION_DATA);
            }
        }

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        LocationUtils.addFragment(this, this);

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lay_location:
                Intent intent = new Intent(this, SelectLocationActivity.class);
                startActivityForResult(intent, REQUEST_CODE_LOCATION);
                break;

            case R.id.tv_publish:

                submitSessionData();
                break;

            case R.id.iv_back:
                finish();
                break;

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_LOCATION:
                if(resultCode == RESULT_OK) {
                    if(data != null) {
                        mLatitude = data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0);
                        mLongitude = data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0);
                        String address = data.getStringExtra(Constants.EXTRA_ADDRESS);

                        mBinder.tvLocationAddress.setText(address);
                    }else {
                        mBinder.tvLocationAddress.setText("");
                    }
                }
        }
    }

    @Override
    public String getActivityName() {
        return null;
    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        //hideProgressBar();
        LogUtils.LOGD(TAG, "onEvent location update...");
        Location location = event.getMessage();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();

        String address = getGeocodeAddress(location.getLatitude(), location.getLongitude());
        mBinder.tvLocationAddress.setText(address);
    }

    private String getGeocodeAddress(double latitude, double longitude) {
        List<Address> addresses = null;
        String address = "Unknown";

        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
//            errorMessage = getString(R.string.service_not_available);
//            Log.e(TAG, errorMessage, ioException);
            LogUtils.LOGE(TAG, ioException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
//            errorMessage = getString(R.string.invalid_lat_long_used);
//            Log.e(TAG, errorMessage + ". " +
//                    "Latitude = " + location.getLatitude() +
//                    ", Longitude = " +
//                    location.getLongitude(), illegalArgumentException);
            LogUtils.LOGE(TAG, illegalArgumentException.getMessage());

        }

        // Handle case where no address was found.
        if (addresses != null && addresses.size() != 0) {
            address = addresses.get(0).getAddressLine(0);
        }

        return address;
    }

    private void submitSessionData() {
        if (!validate()) {
            return;
        }

        session.setFacilityName(mBinder.edtFacilityName.getText().toString());
        session.setFacilityAddress(mBinder.tvLocationAddress.getText().toString());
        session.setFacilityLat(String.valueOf(mLatitude));
        session.setFacilityLong(String.valueOf(mLongitude));

        Call<CreateSessionResponse> call = RequestController.getInstance().createService(AuthWebServices.class,
                true).createSession(session);
        showProgressBar();

        call.enqueue(new BaseCallback<CreateSessionResponse>(this) {
            @Override
            public void onSuccess(CreateSessionResponse response) {
                hideProgressBar();
                showToast(getString(R.string.session_published));
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFail(Call<CreateSessionResponse> call, BaseResponse baseResponse) {
                LogUtils.LOGD(TAG, "Create session failed");
                hideProgressBar();
            }
        });
    }

    private boolean validate() {
        if (mBinder.edtFacilityName.getText().toString().trim().length() < 1) {
            mBinder.lineFacility.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_red_color));
            showSnackbarFromTop(getApplicationContext(), getString(R.string.facility_field_mandatory));
            return false;
        }

        return true;
    }
}

