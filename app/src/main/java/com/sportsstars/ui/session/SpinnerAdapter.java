package com.sportsstars.ui.session;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sportsstars.R;

/**
 * Created by ramkumar on 10/01/18.
 */

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    String[] list;
    LayoutInflater inflter;

    public SpinnerAdapter(Context applicationContext, String[] list) {
        this.context = applicationContext;
        this.list = list;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflter.inflate(R.layout.item_spinner, null);

            holder = new ViewHolder();

            holder.spinnerText=(TextView)convertView.findViewById(R.id.textView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.spinnerText.setText(list[i]);

        return convertView;
    }

    private class ViewHolder {
        TextView spinnerText;
    }


}