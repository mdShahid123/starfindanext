package com.sportsstars.ui.session;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySelectLocationBinding;
import com.sportsstars.eventbus.LocationEvent;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ram on 10/01/18.
 */
public class SelectLocationActivity extends BaseActivity implements View.OnClickListener,
        OnMapReadyCallback, GoogleMap.OnMarkerDragListener {

    private static final String TAG = LogUtils.makeLogTag(SelectLocationActivity.class);
    private ActivitySelectLocationBinding mBinder;
    private GoogleMap mMap;
    private Geocoder geocoder;
    private Location mLocation;
    private String mAddressString;
    private double mLat, mLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_select_location);

        mBinder.tvDone.setOnClickListener(this);
        mBinder.ivBack.setOnClickListener(this);

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //showProgressBar();
        LocationUtils.addFragment(this, this);

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_done:
                if (mLocation != null) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.EXTRA_LATITUDE, mLat);
                    intent.putExtra(Constants.EXTRA_LONGITUDE, mLong);
                    intent.putExtra(Constants.EXTRA_ADDRESS, mAddressString);
                    setResult(RESULT_OK, intent);
                } else {
                    setResult(RESULT_OK);
                }

                finish();
                break;

            case R.id.iv_back:
                finish();
                break;

        }

    }

    @Override
    public String getActivityName() {
        return null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LogUtils.LOGD(TAG, "Map is ready");
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        LogUtils.LOGD(TAG, "onPointerCaptureChanged");
    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        try {
            //hideProgressBar();
            LogUtils.LOGD(TAG, "onEvent location update...");
            mLocation = event.getMessage();
            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            moveCamera(latLng);

            mLat = mLocation.getLatitude();
            mLong = mLocation.getLongitude();

            mAddressString = getGeocodeAddress(mLocation.getLatitude(), mLocation.getLongitude());
            mBinder.tvAddress.setText(mAddressString);

            mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.ic_map_pin))).setDraggable(true);

//        mMap.addMarker(new MarkerOptions().position(latLng)).setDraggable(true);
            mMap.setOnMarkerDragListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveCamera(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    }

    private String getGeocodeAddress(double latitude, double longitude) {
        List<Address> addresses = null;
        String address = "Unknown";

        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
//            errorMessage = getString(R.string.service_not_available);
//            Log.e(TAG, errorMessage, ioException);
            LogUtils.LOGE(TAG, ioException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
//            errorMessage = getString(R.string.invalid_lat_long_used);
//            Log.e(TAG, errorMessage + ". " +
//                    "Latitude = " + location.getLatitude() +
//                    ", Longitude = " +
//                    location.getLongitude(), illegalArgumentException);
            LogUtils.LOGE(TAG, illegalArgumentException.getMessage());

        }

        // Handle case where no address was found.
        if (addresses != null && addresses.size() != 0) {
            address = addresses.get(0).getAddressLine(0);
        }

        return address;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        try {
            if(marker != null && marker.getPosition() != null) {
                mLat = marker.getPosition().latitude;
                mLong = marker.getPosition().longitude;
                mAddressString = getGeocodeAddress(marker.getPosition().latitude, marker.getPosition().longitude);
                mBinder.tvAddress.setText(mAddressString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

