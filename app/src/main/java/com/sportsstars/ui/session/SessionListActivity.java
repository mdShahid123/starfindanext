package com.sportsstars.ui.session;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySessionlistBinding;
import com.sportsstars.databinding.ItemSessionBinding;
import com.sportsstars.interfaces.SessionItemClick;
import com.sportsstars.model.Coach;
import com.sportsstars.model.Session;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.SessionListRequest;
import com.sportsstars.network.response.auth.SessionListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;

import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by abhaykant on 21/01/18.
 */

public class SessionListActivity extends BaseActivity implements SessionItemClick,View.OnClickListener{
    private static final String TAG=LogUtils.makeLogTag(SessionListActivity.class);
    private ActivitySessionlistBinding mBinder;
    private int mCoachId;

    @Override
    public String getActivityName() {
        return SessionListActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_sessionlist);
        mBinder.headerId.tvTitle.setText(getString(R.string.session_by));
        mBinder.headerId.ivBack.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            Coach coach=bundle.getParcelable(Constants.EXTRA_COACH);

            if(coach!=null) {
                mCoachId = coach.getId();
                if (mCoachId != 0) {
                    mBinder.headerId.tvTitle.setText(getString(R.string.session_by)+" "+coach.getName());
                    getSessionList();
                }
            }
        }

    }


    private void getSessionList() {
        processToShowDialog();

        SessionListRequest sessionListRequest= new SessionListRequest();
        sessionListRequest.setCoachId(mCoachId);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.sessionList(sessionListRequest).enqueue(new BaseCallback<SessionListResponse>(SessionListActivity.this) {
            @Override
            public void onSuccess(SessionListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    //mBinder.tvEmailInfo.setVisibility(View.VISIBLE);
                    if(response.getResult() != null && response.getResult().size() > 0) {
                        setAdapter(response.getResult());
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionListActivity.this,response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mBinder.noSessionFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFail(Call<SessionListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SessionListActivity.this,baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setAdapter(ArrayList<Session> sessions){

       SessionListAdapter mAdapter = new SessionListAdapter(this,sessions,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mBinder.recSessionList.setLayoutManager(mLayoutManager);
        mBinder.recSessionList.setItemAnimator(new DefaultItemAnimator());
        mBinder.recSessionList.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mBinder.noSessionFound.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(ItemSessionBinding itemSessionBinding, Session session, int pos) {

        LogUtils.LOGD(TAG,"sessionName:"+session.getFacilityName());
        Bundle bundle=new Bundle();
        bundle.putInt(Constants.EXTRA_SESSION_ID,session.getId());
        bundle.putParcelable(Constants.EXTRA_SESSION,session);
        Navigator.getInstance().navigateToActivityWithData(this,SessionDetailsActivity.class,bundle);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
