package com.sportsstars.ui.session;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityLearnerProfileViewBinding;
import com.sportsstars.databinding.ItemFlowSportsViewProfileBinding;
import com.sportsstars.model.Sports;
import com.sportsstars.model.UserModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.LearnerProfileViewResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.CreateProfileActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.AwsUploadListner;
import com.sportsstars.util.AwsUtil;
import com.sportsstars.util.BitmapUtils;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Calendar;

import retrofit2.Call;

/**
 * Created by Ram on 10/01/18.
 */
public class LearnerProfileViewActivity extends BaseActivity implements View.OnClickListener, AwsUploadListner {

    private static final String TAG = LogUtils.makeLogTag(LearnerProfileViewActivity.class);
    private ActivityLearnerProfileViewBinding mBinder;
    private UserModel profileViewData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_learner_profile_view);

        Utils.setTransparentTheme(this);

        mBinder.ivBack.setOnClickListener(this);
        mBinder.tvEditProfile.setOnClickListener(this);
        getProfile();

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_edit_profile:
                Intent intent = new Intent(this, CreateProfileActivity.class);
                intent.putExtra(Constants.EXTRA_PROFILE, profileViewData);
                startActivityForResult(intent,Constants.REQUEST_CODE.REQUEST_CODE_EDIT_PROFILE);
                //finish();
                break;

            case R.id.iv_back:
                finish();
                break;

        }

    }

    @Override
    public String getActivityName() {
        return LearnerProfileViewActivity.class.getSimpleName();
    }

    private void getProfile() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getProfile().enqueue(new BaseCallback<LearnerProfileViewResponse>
                (LearnerProfileViewActivity.this) {
            @Override
            public void onSuccess(LearnerProfileViewResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        profileViewData = response.getResult();
                        setProfileData(response.getResult());
                    }
                }
            }

            @Override
            public void onFail(Call<LearnerProfileViewResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void setProfileData(UserModel learnerProfile) {

        if (!TextUtils.isEmpty(learnerProfile.getProfileImage())) {
            if (localImageExists(learnerProfile.getProfileImage())) {
                loadFromLocal(learnerProfile.getProfileImage());
            } else {
                AwsUtil.beginDownload(learnerProfile.getProfileImage(), AwsUtil.getTransferUtility(this)
                        , this, this);
            }
        }

        mBinder.tvUserName.setText(learnerProfile.getName());
        mBinder.tvGender.setText(learnerProfile.getGender() == 1 ? getString(R.string.male) : getString(R.string.female));

        if (learnerProfile.getLeanerSport() != null && learnerProfile.getLeanerSport().size() > 0) {
            int skillLevel = learnerProfile.getLeanerSport().get(0).getSkillLevel();
            String skill = "";
            if (skillLevel == 1) {
                skill = getString(R.string.beginner);
            } else if (skillLevel == 2) {
                skill = getString(R.string.intermediate);
            } else {
                skill =  getString(R.string.expert);
            }

            mBinder.tvSkillSet.setText(skill);

            for (Sports sport : learnerProfile.getLeanerSport()) {
                addSportsToLayout(sport.getName());
            }
        }

        mBinder.tvLocationAddress.setText(learnerProfile.getSuburb().getAddress());
        String age = getAge(learnerProfile.getAge()) + " "+getString(R.string.years);
        mBinder.tvAge.setText(age);
        String[] arr = learnerProfile.getFavPlayer().split(" ");
        String name = "";

        for (int i = 0; i < arr.length; i++) {
            arr[i] = Character.toUpperCase(arr[i].charAt(0)) + arr[i].substring(1);
            name = name + " " + arr[i];
        }

        String favPlayer = getString(R.string.my_fav_player_all_time)+" " + name;
        mBinder.tvFavPlayer.setText(favPlayer);

    }

    private void loadFromLocal(String fileName) {
        File mediaDir = new File(Environment.getExternalStoragePublicDirectory(Environment
                .DIRECTORY_PICTURES), getString(R.string.app_name));
        if (mediaDir.exists()) {
            String filepath = mediaDir.getPath() + File.separator + fileName;
            setImage(filepath);
        }
    }

    private boolean localImageExists(String fileName) {
        File mediaDir = new File(Environment.getExternalStoragePublicDirectory(Environment
                .DIRECTORY_PICTURES), getString(R.string.app_name));

        if (mediaDir.exists()) {
            File file = new File(mediaDir.getPath(), fileName);
            return file.exists();
        }

        return false;
    }

    private int getAge(String date) {
        String[] parts = date.split("-");
        Calendar calendar = Calendar.getInstance();
        int age = 0;

        if (parts.length > 2) {
            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            int days = Integer.parseInt(parts[2]);

            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, days);

            try {
                age = Utils.getAge(calendar);
            } catch (Exception e) {
                LogUtils.LOGE(TAG,"getAge>"+e.getMessage());

            }
        }
        return age;
    }

    private void addSportsToLayout(String text) {
        final ItemFlowSportsViewProfileBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from
                (getApplicationContext()).inflate(R.layout.item_flow_sports_view_profile, mBinder.flowLayoutSports, false));

        flowBinding.tvSport.setText(text);

        mBinder.flowLayoutSports.addView(flowBinding.getRoot());

    }

    @Override
    public void onUploaded(String url, boolean success) {

    }



    @Override
    public void onDownLoad(String path, boolean success) {
        LogUtils.LOGD(TAG, "AWS Path " + path);


        if (success && path != null) {
            setImage(path);
        }
    }

    private void setImage(String imgPath) {
        try {
            File f = new File(imgPath);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));

            if(b!=null) {
                Bitmap bmp = BitmapUtils.scaleBitmapImage(this, imgPath, b.getWidth(), b.getHeight());
                mBinder.ivProfilePic.setImageBitmap(bmp);
                mBinder.ivProfileCover.setImageBitmap(bmp);
            }
        } catch (FileNotFoundException e) {
            LogUtils.LOGE(TAG, "setImage " + e.getMessage());
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_EDIT_PROFILE) {
                mBinder.flowLayoutSports.removeAllViews();
                getProfile();

            }
        }
    }
}

