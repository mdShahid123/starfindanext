package com.sportsstars.ui.session;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySessionDetailBinding;
import com.sportsstars.model.Session;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.SessionDetailRequest;
import com.sportsstars.network.response.auth.SessionDetailResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import retrofit2.Call;

/**
 * Created by abhaykant on 20/01/18.
 */

public class SessionDetailsActivity extends BaseActivity implements View.OnClickListener{
   private ActivitySessionDetailBinding mBinder;
   private int mSessionId;
    @Override
    public String getActivityName() {
        return SessionDetailsActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_session_detail);
        mBinder.headerId.parent.setBackgroundColor(ContextCompat.getColor(this,R.color.black));
        mBinder.headerId.tvTitle.setText(getString(R.string.session_details));
        mBinder.headerId.ivRightImg.setImageResource(R.drawable.ic_bookmark_bg);
        mBinder.headerId.ivRightImg.setVisibility(View.VISIBLE);

        init();
    }
    private void init(){
        mBinder.headerId.ivBack.setOnClickListener(this);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            mSessionId=bundle.getInt(Constants.EXTRA_SESSION_ID);
            Session sessionInfo=bundle.getParcelable(Constants.EXTRA_SESSION);

            if(sessionInfo!=null) {
                mBinder.tvCoachName.setText(sessionInfo.getCoachName());
                if (sessionInfo.getCoachType() == Constants.COACH_TYPE.STAR) {
                    mBinder.tvCoachType.setText(getString(R.string.star_coach));
                } else {
                    mBinder.tvCoachType.setText(getString(R.string.private_coach));
                }

                if(!TextUtils.isEmpty(sessionInfo.getProfileImage())){

                    Picasso.with(this)
                            .load(sessionInfo.getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(( mBinder.imageUser));

                }
            }


            getSessionDetails(mSessionId);
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    private void getSessionDetails(int sessionId) {
        processToShowDialog();

        SessionDetailRequest request= new SessionDetailRequest();
        request.setSessionId(sessionId);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.sessionDetails(request).enqueue(new BaseCallback<SessionDetailResponse>(SessionDetailsActivity.this) {
            @Override
            public void onSuccess(SessionDetailResponse response) {
                if (response != null && response.getStatus() == 1) {
                    //mBinder.tvEmailInfo.setVisibility(View.VISIBLE);
                    if(response.getResult() != null) {
                        setData(response.getResult());
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SessionDetailsActivity.this,response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<SessionDetailResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SessionDetailsActivity.this,baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setData(Session session){
        mBinder.tvSessionName.setText(session.getFacilityName());
        mBinder.tvSportName.setText(session.getSportName());
        String[] sessionTypesArr = getResources().getStringArray(R.array.session_type);
        mBinder.tvSessionType.setText(sessionTypesArr[session.getSessionType()-1]);

        String[] bookingTypesArr = getResources().getStringArray(R.array.booking_type);
        mBinder.tvBookingType.setText(bookingTypesArr[session.getBookingType()-1]);
        mBinder.tvDescVal.setText(session.getDescription());

        if((session.getSessionType()-1)==0){
            mBinder.tvSessionType.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_inspirational, 0, 0);;
        }else {
            mBinder.tvSessionType.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_hand_green, 0, 0);;
        }


        if((session.getBookingType()-1)==0){
            mBinder.tvBookingType.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_user_green, 0, 0);
        }else if((session.getBookingType()-1)==1){
            mBinder.tvBookingType.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_small_group, 0, 0);

        }else {
            mBinder.tvBookingType.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_large_group, 0, 0);
        }

        if(session.getSessionDate()!=null && !TextUtils.isEmpty(session.getSessionDate())){
            String [] data=session.getSessionDate().split(" ");
            if(data.length>1){
                //holder.sessionBinding.tvSessionDate.setText(data[0]);
                //holder.sessionBinding.tvSessionTime.setText(data[1].substring(0,5));
                String date=Utils.parseDateForDDMMYY(session.getSessionDate());
                if(date!=null && date.contains(" ")) {
                    mBinder.tvSessionDate.setText(date.split(" ")[0]);
                    mBinder.tvSessionTime.setText(data[1].substring(0,5));
                }
            }

        }




        if(!TextUtils.isEmpty(session.getSessionPhoto())){

            Picasso.with(this)
                    .load(session.getSessionPhoto())
                    .placeholder(R.drawable.ic_placeholder_session_bg)
                    .error(R.drawable.ic_placeholder_session_bg)
                    .into(( mBinder.ivSession));

        }
    }

}
