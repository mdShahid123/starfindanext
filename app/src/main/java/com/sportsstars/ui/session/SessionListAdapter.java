package com.sportsstars.ui.session;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemSessionBinding;
import com.sportsstars.interfaces.SessionItemClick;
import com.sportsstars.model.Session;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abhaykant on 21/01/18.
 */

public class SessionListAdapter extends RecyclerView.Adapter<SessionListAdapter.SessionListViewHolder> {
    private static final String TAG = LogUtils.makeLogTag(SessionListAdapter.class);

    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ArrayList<Session> mSessionList;
    private SessionItemClick mClickListener;


    public void setSessionList(ArrayList<Session> arrayList,SessionItemClick click) {
        this.mSessionList = arrayList;
        mClickListener=click;
        notifyDataSetChanged();
    }


    public SessionListAdapter(Activity mActivity, ArrayList<Session> sessionList,SessionItemClick sessionItemClick) {
        this.mActivity = mActivity;
        this.mSessionList=sessionList;
        this.mClickListener = sessionItemClick;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public SessionListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemSessionBinding searchListBinding = DataBindingUtil.inflate(mInflator, R.layout.item_session, parent, false);
        return new SessionListViewHolder(searchListBinding);
    }

    @Override
    public void onBindViewHolder(final SessionListViewHolder holder, int position) {
        LogUtils.LOGD(TAG,"CountAre  SearchView>" + mSessionList);

        if (mSessionList!=null && mSessionList.size() > 0) {
            final Session session = mSessionList.get(position);
            holder.sessionBinding.tvPrice.setText(mActivity.getString(R.string.dollar)+session.getSessionPrice());
            holder.sessionBinding.tvSessionName.setText(session.getFacilityName());
            holder.sessionBinding.tvCoachName.setText(session.getCoachName());


            if(!TextUtils.isEmpty(session.getSessionPhoto())){

                Picasso.with(mActivity)
                        .load(session.getSessionPhoto())
                        .placeholder(R.drawable.ic_placeholder_session_bg)
                        .error(R.drawable.ic_placeholder_session_bg)
                        .into(( holder.getBinding().ivSession));

            }

            if(!TextUtils.isEmpty(session.getReferencecLogo())){

                Picasso.with(mActivity)
                        .load(session.getReferencecLogo())
                        .placeholder(R.drawable.ic_cricket_green)
                        .error(R.drawable.ic_cricket_green)
                        .into(( holder.getBinding().ivSports));

            }

            if(session.getSessionDate()!=null && !TextUtils.isEmpty(session.getSessionDate())){
                String [] data=session.getSessionDate().split(" ");
                if(data.length>1){
                    //holder.sessionBinding.tvSessionDate.setText(data[0]);
                    //holder.sessionBinding.tvSessionTime.setText(data[1].substring(0,5));
                    String date=Utils.parseDateForDDMMYY(session.getSessionDate());
                    if(date!=null && date.contains(" ")) {
                        holder.sessionBinding.tvSessionDate.setText(date.split(" ")[0]);
                        holder.sessionBinding.tvSessionTime.setText(data[1].substring(0,5));
                    }
                }

            }


        }
    }

    @Override
    public int getItemCount() {
        return mSessionList != null ? mSessionList.size() : 0;
    }

    public void clearItems() {
        if(mSessionList !=null&& mSessionList.size()>0) {
            mSessionList.clear();
            notifyDataSetChanged();
        }
    }



    class SessionListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemSessionBinding sessionBinding;

        private SessionListViewHolder(ItemSessionBinding sessionBinding) {
            super(sessionBinding.getRoot());
            this.sessionBinding = sessionBinding;
            sessionBinding.getRoot().setOnClickListener(this);
        }

        public ItemSessionBinding getBinding() {
            return sessionBinding;
        }

        @Override
        public void onClick(View view) {
            LogUtils.LOGD(TAG,"clicked");
           mClickListener.onItemClick(sessionBinding,mSessionList.get(getAdapterPosition()),getAdapterPosition());
        }
    }

}



