package com.sportsstars.ui.session;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityCreateSessionBinding;
import com.sportsstars.model.TrainingSession;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.AwsUploadListner;
import com.sportsstars.util.AwsUtil;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Utils;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Ram on 09/01/18.
 */
public class CreateSessionActivity extends BaseActivity implements View.OnClickListener, AwsUploadListner {
    private static final String TAG = LogUtils.makeLogTag(CreateSessionActivity.class);
    private static final int MIN_PRICE = 10;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 101;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 102;
    private static final int REQUEST_CODE = 201;
    private static final int REQUEST_CODE_FACILITY = 202;
    private static final long BOOKING_MIN_HOURS = (long) 2 * 60 * 60 * 1000;
    private static final long SESSION_LIMIT_DAYS = (long)30 * 24 * 60 * 60 * 1000;
    private final int ASPECT_RATIO_SQUARE_X = 2;
    private final int ASPECT_RATIO_SQUARE_Y = 1;
    private ActivityCreateSessionBinding mBinder;
    private Calendar mCalendar;
    private Uri mCropImageUri;
    private Dialog mDialog;
    private CropImageView mCropImageView;
    private Bitmap mBitmap;
    private String mPhotoUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_create_session);
        mCalendar = Calendar.getInstance();

//        Utils.setTransparentTheme(this);

        final String[] bookingTypeList = getResources().getStringArray(R.array.booking_type);
        mBinder.spinnerBookingType.setAdapter(new SpinnerAdapter(getApplicationContext(),
                bookingTypeList));

        final String[] sessionTypeList = getResources().getStringArray(R.array.session_type);

        mBinder.spinnerSessionType.setAdapter(new SpinnerAdapter(getApplicationContext(),
                sessionTypeList));

        mBinder.ivBack.setOnClickListener(this);
        mBinder.tvBookingType.setText(bookingTypeList[0]);
        mBinder.tvBookingType.setOnClickListener(this);

        mBinder.tvSessionType.setText(sessionTypeList[0]);
        mBinder.tvSessionType.setOnClickListener(this);
        mBinder.tvNext.setOnClickListener(this);
        mBinder.layProfilePic.setOnClickListener(this);

//        mBinder.tvTime.setOnClickListener(this);
//        mBinder.tvDate.setOnClickListener(this);

        mBinder.edtRules.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.edt_rules) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.performClick();
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mBinder.edtDescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.edt_rules) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.performClick();
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mBinder.spinnerBookingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mBinder.tvBookingType.setText(bookingTypeList[mBinder.spinnerBookingType
                        .getSelectedItemPosition
                                ()]);
                mBinder.spinnerBookingType.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mBinder.spinnerBookingType.setVisibility(View.GONE);
            }
        });

        mBinder.spinnerSessionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mBinder.tvSessionType.setText(sessionTypeList[mBinder.spinnerSessionType.getSelectedItemPosition()]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinder.tvTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = mCalendar.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateSessionActivity.this, new TimePickerDialog.OnTimeSetListener
                        () {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mBinder.tvTime.setText(formatTime(selectedHour, selectedMinute));
                        mCalendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                        mCalendar.set(Calendar.MINUTE, selectedMinute);

                    }
                }, hour, minute, DateFormat.is24HourFormat(CreateSessionActivity.this));//Yes 24 hour
                // time

                mTimePicker.show();

            }
        });

        mBinder.tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Use the current date as the default date in the picker
//                    final Calendar c = Calendar.getInstance();
                int year = mCalendar.get(Calendar.YEAR);
                int month = mCalendar.get(Calendar.MONTH);
                int day = mCalendar.get(Calendar.DAY_OF_MONTH);

                final DatePickerDialog datePickerDialog;
                // Create a new instance of DatePickerDialog and return it
                datePickerDialog = new DatePickerDialog(CreateSessionActivity.this, new
                        DatePickerDialog
                                .OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int
                                    day) {

                                mCalendar.set(Calendar.YEAR, year);
                                mCalendar.set(Calendar.MONTH, month);
                                mCalendar.set(Calendar.DAY_OF_MONTH, day);

                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                                simpleDateFormat.applyPattern("dd MMM yyyy");
                                String date = simpleDateFormat.format(mCalendar.getTime());
                                mBinder.tvDate.setText(date);
                            }
                        }, year, month,
                        day);

                datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                Calendar calendarMax = Calendar.getInstance();
                calendarMax.add(Calendar.MONTH, 1);
                datePickerDialog.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());

                datePickerDialog.show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.tv_booking_type:
                mBinder.spinnerBookingType.setVisibility(View.VISIBLE);
                mBinder.spinnerBookingType.performClick();
                break;

            case R.id.tv_session_type:
                mBinder.spinnerSessionType.setVisibility(View.VISIBLE);
                mBinder.spinnerSessionType.performClick();
                break;

            case R.id.tv_time:
//                showTimePickerDialog();

                break;

            case R.id.tv_next:
                proceedToFacilitySelection();
                break;

            case R.id.lay_profile_pic:
                /*if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                            this, Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);


                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }

                } else {

                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        Utils.askStoragePermission(MY_PERMISSIONS_REQUEST_WRITE_STORAGE, this);
                    } else {
                        startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                    }
                }*/

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    /*if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(
                            this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                                .CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }*/

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                } else {
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                }


                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                getCaptureUri(data);

                mBinder.tvAddPhoto.setVisibility(View.GONE);
            }

            if (requestCode == REQUEST_CODE_FACILITY) {
                finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;


            case MY_PERMISSIONS_REQUEST_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCropImageView.setImageUriAsync(mCropImageUri);
                } else {
                    Toast.makeText(getApplicationContext(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void getCaptureUri(Intent data) {
        showCropDialog();
        Uri imageUri = Utils.getPickImageResultUri(data, this);

        // For API >= 23 we need to check specifically that we have permissions to read external storage,
        // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                Utils.isUriRequiresPermissions(imageUri, this)) {

            // request permissions and handle the result in onRequestPermissionsResult()
            requirePermissions = true;
            mCropImageUri = imageUri;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            //LogUtils.LOGD(TAG, "Permission Granted>");

        }

        if (!requirePermissions) {
            mCropImageView.setImageUriAsync(imageUri);
            //LogUtils.LOGD(TAG, " Image Cropped>");
            String path = getRealPathFromURI(imageUri);
            LogUtils.LOGD("path>>", path);
            //initS3Upload(path);

            AwsUtil.beginUpload(path, Constants.AWS_FOLDER_SESSION, AwsUtil.getTransferUtility(this),
                    this);

            try {
                if (Utils.getBitmapFromUri(this, imageUri) == null) {
                    //LogUtils.LOGD(TAG, " null>");
                    if (mDialog != null && mDialog.isShowing()) {
                        showSnackbarFromTop(this, "Please select valid image");
                        mDialog.dismiss();
                    }
                }
            } catch (IOException e) {
                //LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());
            }


        }
    }

    public void showCropDialog() {
        mDialog = new Dialog(this, android.R.style.Theme_Light);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_image_crop);
        mCropImageView = (CropImageView) mDialog.findViewById(R.id.image_crop);

        mCropImageView.setAspectRatio(ASPECT_RATIO_SQUARE_X, ASPECT_RATIO_SQUARE_Y);


        Button btnCrop = (Button) mDialog.findViewById(R.id.button_crop);
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                // Bitmap cropped = mCropImageView.getCroppedImage(500, 200);
                Bitmap cropped = mCropImageView.getCroppedImage();
                if (cropped != null) {

                    mBitmap = cropped;
                    mBinder.ivPic.setImageBitmap(cropped);
                    //uploadImg(cropped, VEHICALE_IMAGE_TYPE, mLoanDetail.getId());

                }
            }
        });

        Button buttonCancel = (Button) mDialog.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void proceedToFacilitySelection() {
        if (!validate())
            return;

        TrainingSession session = new TrainingSession();
        session.setSportId(1);

        String[] bookingTypes = getResources().getStringArray(R.array.booking_type);
        int bookingType = Utils.getBookingType(bookingTypes, mBinder.tvBookingType.getText()
                .toString());
        session.setBookingType(bookingType);

        String[] sessionTypes = getResources().getStringArray(R.array.session_type);
        int sessionType = Utils.getSessionType(sessionTypes, mBinder.tvSessionType.getText()
                .toString());
        session.setSessionType(sessionType);
        session.setSessionPhoto(mPhotoUrl);
        session.setTitle(mBinder.edtTitle.getText().toString());
//        session.setTitle("title");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        String date = simpleDateFormat.format(mCalendar.getTime());

//        session.setSessionDateTime("2018-01-07 12:08:10");
        session.setSessionDateTime(date);
        double price = Double.parseDouble(mBinder.edtPrice.getText().toString().trim());

        session.setPrice(price);
        session.setRules(mBinder.edtRules.getText().toString());
        session.setDescription(mBinder.edtDescription.getText().toString());


        Intent intent = new Intent(this, SelectFacilityActivity.class);
        intent.putExtra("session", session);
        startActivityForResult(intent, REQUEST_CODE_FACILITY);
    }

    private boolean validate() {
        if (mBinder.edtTitle.getText().toString().trim().length() < 1) {
            showSnackbarFromTop(getApplicationContext(), getString(R.string.title_field_mandatory));
            mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_red_color));
            return false;
        }

        if (mBinder.tvTime.getText().toString().length() < 1) {
            showSnackbarFromTop(getApplicationContext(), getString(R.string.time_field_mandatory));
            mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_red_color));
            return false;
        } else {
            if (mCalendar.getTimeInMillis() <= (Calendar.getInstance().getTimeInMillis() +
                    BOOKING_MIN_HOURS)) {
                showSnackbarFromTop(getApplicationContext(), getString(R.string
                        .time_field_future));
                mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                        .divider_color));
                mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                        .divider_red_color));
                return false;
            }
        }

        if (mBinder.tvDate.getText().toString().length() < 1) {
            showSnackbarFromTop(getApplicationContext(), getString(R.string.date_field_mandatory));
            mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineDate.setBackground(ContextCompat.getDrawable(this, R.color.divider_red_color));
            return false;
        }

        if (mBinder.edtPrice.getText().toString().trim().length() < 1) {
            showSnackbarFromTop(getApplicationContext(), getString(R.string
                    .price_field_mandatory));
            mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineDate.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.linePrice.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_red_color));
            return false;
        } else {
            double price = 0.0;
            try {
                price = Double.parseDouble(mBinder.edtPrice.getText().toString());
            } catch (ArithmeticException ex) {
                LogUtils.LOGE(TAG, ex.getMessage());
            }

            if (price < MIN_PRICE) {
                showSnackbarFromTop(getApplicationContext(), getString(R.string
                        .price_field_criteria, MIN_PRICE));
                mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                        .divider_color));
                mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                        .divider_color));
                mBinder.lineDate.setBackground(ContextCompat.getDrawable(this, R.color
                        .divider_color));
                mBinder.linePrice.setBackground(ContextCompat.getDrawable(this, R.color
                        .divider_red_color));
                return false;
            }
        }

        if (mBinder.edtRules.getText().toString().trim().length() < 1) {
            showSnackbarFromTop(getApplicationContext(), getString(R.string.rule_field_mandatory));
            mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineDate.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.linePrice.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            return false;
        }

        if (mBinder.edtDescription.getText().toString().trim().length() < 1) {
            showSnackbarFromTop(getApplicationContext(), getString(R.string.description_field_mandatory));
            mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.lineDate.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            mBinder.linePrice.setBackground(ContextCompat.getDrawable(this, R.color
                    .divider_color));
            return false;
        }

        mBinder.lineDate.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));
        mBinder.lineTime.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));
        mBinder.linePrice.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));
        mBinder.lineTitle.setBackground(ContextCompat.getDrawable(this, R.color
                .divider_color));

        return true;
    }

    @Override
    public String getActivityName() {
        return null;
    }

    private String formatTime(int hour, int min) {
        String format;
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        String hrs = String.valueOf(hour);
        String mins = String.valueOf(min);

        if (hour / 10 == 0) {
            hrs = "0" + hour;
        }

        if (min / 10 == 0) {
            mins = "0" + min;
        }

        return hrs + ":" + mins +
                " " + format;
    }

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    @Override
    public void onUploaded(String url, boolean success) {
        LogUtils.LOGD("url>>>", url);
        mPhotoUrl = url;
    }



    @Override
    public void onDownLoad(String path, boolean success) {

    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user

        }
    }
}

