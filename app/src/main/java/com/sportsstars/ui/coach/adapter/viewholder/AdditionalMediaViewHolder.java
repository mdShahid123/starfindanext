package com.sportsstars.ui.coach.adapter.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.databinding.ItemAdditionalMediaBinding;
import com.sportsstars.model.AdditionalMedia;
import com.squareup.picasso.Picasso;


public class AdditionalMediaViewHolder extends RecyclerView.ViewHolder {

    private ItemAdditionalMediaBinding mBinding;

    public AdditionalMediaViewHolder(ItemAdditionalMediaBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindData(Context context, AdditionalMedia additionalMedia, final RemoveMediaCallback listener, int position) {
        Picasso.with(context).load(additionalMedia.getThumbImageUrl()).into(mBinding.ivAdditionalMedia);
        if (additionalMedia.getType() == 7) {
            mBinding.ivVideoPlay.setVisibility(View.VISIBLE);
        } else {
            mBinding.ivVideoPlay.setVisibility(View.INVISIBLE);
        }
        mBinding.ivRemoveMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMediaRemoved(position);

            }
        });
    }

    public interface RemoveMediaCallback {
        void onMediaRemoved(int position);
    }

}
