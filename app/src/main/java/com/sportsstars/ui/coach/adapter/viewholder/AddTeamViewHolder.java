package com.sportsstars.ui.coach.adapter.viewholder;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemAddTeamBinding;
import com.sportsstars.interfaces.AddTeamItemClick;
import com.sportsstars.model.Team;

/**
 * Created by rohantaneja on 17/04/18.
 */

public class AddTeamViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ItemAddTeamBinding mBinding;
    private Context mContext;
    private AddTeamItemClick mTeamClickListener;
    private Team mTeam;

    public AddTeamViewHolder(Context context, View itemView, AddTeamItemClick teamClickListener) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);

        mContext = context;
        mBinding.clAddTeamParent.setOnClickListener(this);
        mTeamClickListener = teamClickListener;
    }

    public void bindData(Team team) {
        mTeam = team;

        if (mTeam.isSelected()) {
            mBinding.tvTeamName.setTextColor(ContextCompat.getColor(mContext, R.color.selected_item_text_color));
            mBinding.tvTeamName.setCustomFont(mContext, mContext.getString(R.string.font_an_db));
            mBinding.ivTeamSelection.setImageResource(R.drawable.ic_check_selected);
        } else {
            mBinding.tvTeamName.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            mBinding.tvTeamName.setCustomFont(mContext, mContext.getString(R.string.font_anr));
            mBinding.ivTeamSelection.setImageResource(R.drawable.ic_check_unselected);
        }

        mBinding.tvTeamName.setText(team.getName());
    }

    public void hideDivider() {
        mBinding.vFooter.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cl_add_team_parent:
                mTeamClickListener.onTeamSelected(mTeam);
                break;
        }
    }

}
