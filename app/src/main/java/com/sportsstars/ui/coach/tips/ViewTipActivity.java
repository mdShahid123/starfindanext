package com.sportsstars.ui.coach.tips;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityViewTipBinding;
import com.sportsstars.network.response.coachtips.CoachTips;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ViewTipActivity extends BaseActivity implements View.OnClickListener {

    private ActivityViewTipBinding mBinding;
    private CoachTips coachTips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_tip);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.COACH_TIPS)) {
                coachTips = bundle.getParcelable(Constants.BundleKey.COACH_TIPS);
            }
        }
        initViews();
        setData();
    }

    private void initViews() {
        mBinding.topHeader.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.topHeader.tvTitle.setText(getString(R.string.view_tip));
        mBinding.topHeader.ivBack.setVisibility(View.VISIBLE);
        mBinding.topHeader.tvEdit.setVisibility(View.VISIBLE);

        mBinding.topHeader.ivBack.setOnClickListener(this);
        mBinding.topHeader.tvEdit.setOnClickListener(this);
        mBinding.rlPlay.setOnClickListener(this);

        mBinding.clTitle.tvLabel.setText(getResources().getString(R.string.title));
        mBinding.clTitle.etValue.setHint(getResources().getString(R.string.example_tip));
        mBinding.clTitle.ivSpinner.setVisibility(View.GONE);

        mBinding.clSport.tvLabel.setText(getResources().getString(R.string.sport));
        mBinding.clSport.etValue.setHint(getResources().getString(R.string.example_cricket));
        mBinding.clSport.ivSpinner.setVisibility(View.GONE);

        mBinding.clType.tvLabel.setText(getResources().getString(R.string.type));
        mBinding.clType.etValue.setHint(getResources().getString(R.string.example_video));
        mBinding.clType.ivSpinner.setVisibility(View.GONE);
    }

    private void setData() {
        if(coachTips != null) {
            if(!TextUtils.isEmpty(coachTips.getTitle())) {
                mBinding.clTitle.etValue.setText(coachTips.getTitle());
            }
            if(!TextUtils.isEmpty(coachTips.getSportName())) {
                mBinding.clSport.etValue.setText(coachTips.getSportName());
            }
//            if(coachTips.getType() != null && coachTips.getType() > 0) {
//                mBinding.clType.etValue.setText(coachTips.getType() == 1 ?
//                        getResources().getString(R.string.video) : getResources().getString(R.string.text));
//            }
            mBinding.clType.etValue.setText(getResources().getString(R.string.video));
            if(!TextUtils.isEmpty(coachTips.getImageUrl())) {
                mBinding.pbLoading.setVisibility(View.VISIBLE);
                Picasso.with(this)
                        .load(coachTips.getImageUrl())
                        .into(mBinding.ivThumbnail, new Callback() {
                            @Override
                            public void onSuccess() {
                                mBinding.pbLoading.setVisibility(View.INVISIBLE);
                                mBinding.rlPlay.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                mBinding.pbLoading.setVisibility(View.INVISIBLE);
                                mBinding.rlPlay.setVisibility(View.INVISIBLE);
                            }
                        });
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.tv_edit:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.BundleKey.COACH_TIPS, coachTips);
                Navigator.getInstance().navigateToActivityWithData(this, AddTipActivity.class, bundle);
                break;
            case R.id.rl_play:
                if(coachTips != null && !TextUtils.isEmpty(coachTips.getVideoUrl())) {
                    if(coachTips.getVideoUrl().contains("http")) {
                        launchVideoPlayer();
                    } else {
                        if(new File(coachTips.getVideoUrl()).exists()) {
                            launchVideoPlayer();
                        } else {
                            showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
                        }
                    }
                } else {
                    showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
                }
                break;
        }
    }

    private void launchVideoPlayer() {
        Bundle bundlePlay = new Bundle();
        bundlePlay.putString(Constants.BundleKey.VIDEO_URL, coachTips.getVideoUrl());
        Navigator.getInstance().navigateToActivityWithData(ViewTipActivity.this, PlayerActivity.class, bundlePlay);
    }

    @Override
    public String getActivityName() {
        return ViewTipActivity.class.getSimpleName();
    }

}
