package com.sportsstars.ui.coach.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iceteck.silicompressorr.SiliCompressor;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentEditCoachPersonalBinding;
import com.sportsstars.model.AdditionalMedia;
import com.sportsstars.model.Sports;
import com.sportsstars.model.SuburbInfo;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.CoachCreateProfileStep1Request;
import com.sportsstars.network.response.auth.AdditionalMediaResponse;
import com.sportsstars.network.response.auth.CoachCreateProfileStep1Response;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.VerifyPhoneActivity;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.coach.AddTeamActivity;
import com.sportsstars.ui.coach.adapter.AdditionalMediaAdapter;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.search.SearchCoachActivityNext;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.FileUtils;
import com.sportsstars.util.ImagePickerUtils;
import com.sportsstars.util.ImageUtil;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class CoachPersonalProfileEditFragment extends BaseFragment implements View.OnClickListener,
        ImagePickerUtils.OnImagePickerListener, AdditionalMediaAdapter.IUploadCallback, Alert.OnOkayClickListener {

    private FragmentEditCoachPersonalBinding fragmentEditCoachPersonalBinding;
    private static final String TAG = "CoachPersonal";
    private Calendar mCalendar;
    private String mDOB, mDobFormatted;
    private Dialog mDialog;
    private Sports sports;
    private CropImageView mCropImageView;
    private Uri mCropImageUri;
    private SuburbInfo mSuburb;
    private int mCoachType;
    private static final int UPLOAD_TYPE = 1;
    private int mGenderType;
    private Bitmap mCroppedImage;
    private ArrayList<Team> mTeamsList;
    private UserProfileModel mProfileModel;
    private List<AdditionalMedia> mMediaPaths = new ArrayList<>();
    private UserProfileInstance mUserProfileInstance;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1221;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE_VIDEO = 1222;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2122;
    private static final int UPLOAD_TYPE_ADDITIONAL_MEDIA_IMAGE = 3;
    private static final int UPLOAD_TYPE_ADDITIONAL_MEDIA_VIDEO = 7;
    private static final int REQUEST_CODE = 3123;
    private static final int ASPECT_RATIO_SQUARE_X = 1;
    private static final int ASPECT_RATIO_SQUARE_Y = 1;
    private static final int REQUEST_CODE_LOCATION = 101;
    private AdditionalMediaAdapter mAdditionalMediaAdapter;
    private boolean isInitialized;

    @Override
    public String getFragmentName() {
        return CoachPersonalProfileEditFragment.class.getSimpleName();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentEditCoachPersonalBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_coach_personal, container, false);
        initUI();

//        if(!isInitialized)
//        {
//            fragmentEditCoachPersonalBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_coach_personal, container, false);
//            initUI();
//            isInitialized = true;
//        }

        return fragmentEditCoachPersonalBinding.getRoot();
    }

    private void initUI() {
        fragmentEditCoachPersonalBinding.etAge.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.imageCamera.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.rlMale.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.rlFemale.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.relStar.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.relPrivate.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.buttonNext.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.etSuburb.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.etSyc.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.tvStarInfo.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.tvPrivateInfo.setOnClickListener(this);
        fragmentEditCoachPersonalBinding.etMobileNo.setOnClickListener(this);

        //fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusable(false);

        mAdditionalMediaAdapter = new AdditionalMediaAdapter(getActivity(), this, mMediaPaths);
        fragmentEditCoachPersonalBinding.rvMediaListRecyclerview.setLayoutManager(new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false));
        fragmentEditCoachPersonalBinding.rvMediaListRecyclerview.setAdapter(mAdditionalMediaAdapter);


        mCalendar = Calendar.getInstance();
        sports = new Sports();
        mGenderType = Constants.GENDER_TYPE.MALE;
        mCoachType = Constants.COACH_TYPE.STAR;

        setGenderType(fragmentEditCoachPersonalBinding.rlMale, fragmentEditCoachPersonalBinding.tvMale);
        setCoachType(fragmentEditCoachPersonalBinding.relStar, fragmentEditCoachPersonalBinding.tvStar,
                fragmentEditCoachPersonalBinding.tvStarInfo);

        fragmentEditCoachPersonalBinding.etAboutMe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.et_about_me) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.performClick();
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        fragmentEditCoachPersonalBinding.etAccolades.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.et_accolades) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });


        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if (mProfileModel != null) {
            setUserInfo();
        }
    }

    private void setUserInfo() {
        if (mProfileModel != null) {

            //Profile Image
            if (mProfileModel.getProfileImage() != null && !TextUtils.isEmpty(mProfileModel.getProfileImage())) {
                Picasso.with(getActivity())
                        .load(mProfileModel.getProfileImage()).placeholder(R.drawable.ic_user_circle).error(R.drawable.ic_user_circle).into(fragmentEditCoachPersonalBinding.imageUser);
            }

            //Name
            fragmentEditCoachPersonalBinding.etName.setText(mProfileModel.getName());

            //DOB
            fragmentEditCoachPersonalBinding.etAge.setText(Utils.getUIFormattedDate(mProfileModel.getDob()));
            mDobFormatted = mProfileModel.getDob();

            //Mobile
            fragmentEditCoachPersonalBinding.etMobileNo.setText(mProfileModel.getMobileNumber());

            //Address
            if (mProfileModel.getSuburb() != null && mProfileModel.getSuburb().getAddress() != null) {
                fragmentEditCoachPersonalBinding.etSuburb.setText(mProfileModel.getSuburb().getAddress());
                mSuburb = new SuburbInfo();
                mSuburb = mProfileModel.getSuburb();
            }

            //Gender
            mGenderType = mProfileModel.getGender();
            if(mGenderType==Constants.GENDER_TYPE.MALE)
            {
                setGenderType(fragmentEditCoachPersonalBinding.rlMale, fragmentEditCoachPersonalBinding.tvMale);
            }
            else
            {
                setGenderType(fragmentEditCoachPersonalBinding.rlFemale, fragmentEditCoachPersonalBinding.tvFemale);
            }

            //Sport you coach
            if (mProfileModel.getSportDetail() != null && !StringUtils.isNullOrEmpty(mProfileModel.getSportDetail().getName())) {
                fragmentEditCoachPersonalBinding.etSyc.setText(mProfileModel.getSportDetail().getName());
                sports.setId(mProfileModel.getSportDetail().getId());
                sports.setName(mProfileModel.getSportDetail().getName());
                sports.setReferencecLogo(mProfileModel.getSportDetail().getIcon());
            }

            //Coach Type
            setCoachType(fragmentEditCoachPersonalBinding.relStar, fragmentEditCoachPersonalBinding.tvStar,
                    fragmentEditCoachPersonalBinding.tvStarInfo);
            if (mProfileModel.getCoachType() == Constants.COACH_TYPE.PRIVATE) {
                mCoachType = Constants.COACH_TYPE.PRIVATE;
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusable(true);
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusableInTouchMode(true);
                setCoachType(fragmentEditCoachPersonalBinding.relPrivate, fragmentEditCoachPersonalBinding.tvPrivate,
                        fragmentEditCoachPersonalBinding.tvPrivateInfo);
            }
            else {
                mCoachType = Constants.COACH_TYPE.STAR;
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusable(false);
                setCoachType(fragmentEditCoachPersonalBinding.relStar, fragmentEditCoachPersonalBinding.tvStar,
                        fragmentEditCoachPersonalBinding.tvStarInfo);
            }

            //About me
            fragmentEditCoachPersonalBinding.etAboutMe.setText(mProfileModel.getAboutMe());

            //Upload Video
//            fragmentEditCoachPersonalBinding.etUploadVideo.setText(mProfileModel.getYoutubeLink());


            //Additional Media
            mAdditionalMediaAdapter.setAdditionalMedias(mProfileModel.getMediaUrl());


            mTeamsList = mProfileModel.getTeamPlayedFor();
            fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText(Utils.getCommaSepString(mProfileModel.getTeamPlayedFor()));

            //Accolades
            fragmentEditCoachPersonalBinding.etAccolades.setText(mProfileModel.getAccolades());

            if(mSuburb == null || TextUtils.isEmpty(String.valueOf(mSuburb.getLat()))
                    || TextUtils.isEmpty(String.valueOf(mSuburb.getLng()))
                    || TextUtils.isEmpty(mSuburb.getAddress())) {
                mSuburb = new SuburbInfo();
                mSuburb.setLat(mProfileModel.getSuburb().getLat());
                mSuburb.setLng(mProfileModel.getSuburb().getLng());
                mSuburb.setAddress(mProfileModel.getSuburb().getAddress());
            }

            if(mProfileModel.getSportDetail() != null && !TextUtils.isEmpty(mProfileModel.getSportDetail().getName())) {
                sports.setId(mProfileModel.getSportDetail().getId());
                sports.setName(mProfileModel.getSportDetail().getName());
                sports.setReferencecLogo(mProfileModel.getSportDetail().getIcon());
            }

//            mSuburb = new SuburbInfo();
//            mSuburb.setLat(mProfileModel.getSuburb().getLat());
//            mSuburb.setLng(mProfileModel.getSuburb().getLng());
//            mSuburb.setAddress(mProfileModel.getSuburb().getAddress());

        }
        fragmentEditCoachPersonalBinding.etName.requestFocus();
        if (fragmentEditCoachPersonalBinding.etName.getText() != null && !TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etName.getText())) {
            fragmentEditCoachPersonalBinding.etName.setSelection(fragmentEditCoachPersonalBinding.etName.getText().length());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_age:
                showDatePicker();
                break;

            case R.id.image_camera:
//                if (ActivityCompat.checkSelfPermission(getContext(),
//                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(getContext(),
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//
//                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            MY_PERMISSIONS_REQUEST_CAMERA);
//                } else {
//                    startActivityForResult(Utils.getPickImageChooserIntent(getActivity()), REQUEST_CODE);
//                }

                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Permission is granted");
                        startActivityForResult(Utils.getPickImageChooserIntent(getActivity()), REQUEST_CODE);
                    } else {
                        Log.d(TAG, "Permission is revoked");
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Log.d(TAG, "Permission is granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(getActivity()), REQUEST_CODE);
                }
                break;

            case R.id.rel_private:
//                showSnackbarFromTop("Coming Soon");

                mCoachType = Constants.COACH_TYPE.PRIVATE;
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText("");
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusable(true);
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusableInTouchMode(true);
                setCoachType(fragmentEditCoachPersonalBinding.relPrivate, fragmentEditCoachPersonalBinding.tvPrivate,
                        fragmentEditCoachPersonalBinding.tvPrivateInfo);
                break;

            case R.id.rel_star:
                mCoachType = Constants.COACH_TYPE.STAR;
                //fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText("");

                //mTeamsList = mProfileModel.getTeamPlayedFor();
                if(mTeamsList != null && mTeamsList.size() > 0) {
                    mTeamsList.clear();
                    fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText("");
                    //fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText(Utils.getCommaSepString(mTeamsList));
                } else {
                    fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText("");
                }
                fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setFocusable(false);
                setCoachType(fragmentEditCoachPersonalBinding.relStar, fragmentEditCoachPersonalBinding.tvStar,
                        fragmentEditCoachPersonalBinding.tvStarInfo);
                break;

            case R.id.rlMale:
                mGenderType = Constants.GENDER_TYPE.MALE;
                setGenderType(fragmentEditCoachPersonalBinding.rlMale, fragmentEditCoachPersonalBinding.tvMale);
                break;

            case R.id.rlFemale:
                mGenderType = Constants.GENDER_TYPE.FEMALE;
                setGenderType(fragmentEditCoachPersonalBinding.rlFemale, fragmentEditCoachPersonalBinding.tvFemale);
                break;

            case R.id.button_next:
                if(Utils.isNetworkAvailable()) {
                    if (isInputValid()) {
                        updateCoachPersonalProfile();
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.et_suburb:
                if(Utils.isNetworkAvailable()) {
                    launchUpdateLocation();
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.etSyc:
                if(Utils.isNetworkAvailable()) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.EXTRA_SPORTS_LIST, sports);
                    bundle.putBoolean(Constants.FROM_COACH_INFO, true);
                    Navigator.getInstance().navigateForResultWithBundle(getActivity(), this,
                            SearchCoachActivityNext.class, Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST, bundle);
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.et_teams_you_played:
                if (mCoachType == Constants.COACH_TYPE.STAR) {
                    if(sports != null) {
                        if(sports.getName() != null && !TextUtils.isEmpty(sports.getName())) {
                            if(sports.getId() != 0) {
                                if(Utils.isNetworkAvailable()) {
                                    Bundle b = new Bundle();
                                    b.putInt(Constants.BundleKey.ID, sports.getId());
                                    b.putParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST, (ArrayList<? extends Parcelable>) mTeamsList);
                                    Navigator.getInstance().navigateForResultWithBundle(getActivity(), this,
                                            AddTeamActivity.class, Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS, b);
                                } else {
                                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                                }
                            } else {
                                showSnackbarFromTop(getResources().getString(R.string.please_select_sport));
                            }
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.please_select_sport));
                        }
                    } else {
                        showSnackbarFromTop(getResources().getString(R.string.please_select_sport));
                    }
                }
                break;

            case R.id.tv_star_info:
                showStarInfoDialog();
//                if(mCoachType == Constants.COACH_TYPE.STAR) {
//                    showStarInfoDialog();
//                }
                break;

            case R.id.tv_private_info:
                showPrivateInfoDialog();
//                if(mCoachType == Constants.COACH_TYPE.PRIVATE) {
//                    showPrivateInfoDialog();
//                }
                break;

            case R.id.et_mobile_no:
                // go to phone number screen
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, true);
                Navigator.getInstance().navigateToActivityWithData(getActivity(), VerifyPhoneActivity.class, bundle);
                break;
        }
    }

    private void showStarInfoDialog() {
        String title = getResources().getString(R.string.star_coach);
        String message = getResources().getString(R.string.star_coach_info_msg);
        Alert.showInfoDialog(getActivity(),
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showPrivateInfoDialog() {
        String title = getResources().getString(R.string.private_coach);
        String message = getResources().getString(R.string.private_coach_info_msg);
        Alert.showInfoDialog(getActivity(),
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(getBaseActivity(), new
                DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int
                            day) {

//                            Calendar calendar = Calendar.getInstance();
//                            calendar.set(Calendar.YEAR, year);
//                            calendar.set(Calendar.MONTH, month);
//                            calendar.set(Calendar.DAY_OF_MONTH, day);

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormated = yyyymmddFormat.format(mCalendar.getTime());


                        try {

                            if (date != null && isValidDate(date)) {
                                mDOB = date;
                                mDobFormatted = dateFormated;
                                fragmentEditCoachPersonalBinding.etAge.setText(mDOB);
                            } else {
                                mDOB = "";
                                mDobFormatted = "";
                                fragmentEditCoachPersonalBinding.etAge.setText(mDOB);
                                showSnackbarFromTop(getString(R.string.invalid_age));
                            }
                            int age = Utils.getAge(mCalendar);

                            if (age < 5) {
                                mDOB = "";
                                mDobFormatted = "";
                                fragmentEditCoachPersonalBinding.etAge.setText(mDOB);
                                showSnackbarFromTop(getString(R.string.invalid_age));
                            }
                            LogUtils.LOGD(TAG, "Age:" + Utils.getAge(mCalendar));

                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "Age>" + e.getMessage());

                        }

                    }
                }, year, month,
                day);

        Calendar minCal = Calendar.getInstance();
        minCal.set(Calendar.YEAR, 1940);
        datePickerDialog.getDatePicker().setMinDate(minCal.getTimeInMillis());

        datePickerDialog.show();
    }

    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(pDateString);
        return new Date().after(date);
    }

    private void setCoachType(RelativeLayout relSelected, TextView tvSelected, TextView tvSelectedInfo) {

        fragmentEditCoachPersonalBinding.relStar.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_grey));
        fragmentEditCoachPersonalBinding.tvStar.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));

        fragmentEditCoachPersonalBinding.relPrivate.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_grey));
        fragmentEditCoachPersonalBinding.tvPrivate.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));

        fragmentEditCoachPersonalBinding.tvPrivateInfo.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));
        fragmentEditCoachPersonalBinding.tvPrivateInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);

        fragmentEditCoachPersonalBinding.tvStarInfo.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));
        fragmentEditCoachPersonalBinding.tvStarInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);

        relSelected.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_border_green_bg_grey));
        tvSelected.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_gender));

        tvSelectedInfo.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_gender));
        tvSelectedInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);

    }

    private void setGenderType(RelativeLayout relSelected, TextView tvSelected) {

        fragmentEditCoachPersonalBinding.rlFemale.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_grey));
        fragmentEditCoachPersonalBinding.tvFemale.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));

        fragmentEditCoachPersonalBinding.rlMale.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_grey));
        fragmentEditCoachPersonalBinding.tvMale.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));

        relSelected.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_border_green_bg_grey));
        tvSelected.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_gender));

    }

    private void launchUpdateLocation() {
        Intent intent = new Intent(getActivity(), SearchCoachLocationActivity.class);
        intent.putExtra(Constants.EXTRA_TITLE, getString(R.string.select_location));
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {

                getCaptureUri(data);

            }
            else if (requestCode == Constants.GET_VIDEO) {
                File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getActivity().getPackageName() + "/media/videos");
                if (f.mkdirs() || f.isDirectory()) {
                    if(FileUtils.isVideoFileExists(getActivity(), data.getData())) {
                        //compress and output new video specs
                        new VideoCompressAsyncTask(getActivity()).execute(data.getData().toString(), f.getPath());
                    } else {
                        showSnackbarFromTop(getResources().getString(R.string.unable_to_fetch_video));
                    }
                }
            }
            else if (requestCode == REQUEST_CODE_LOCATION) {
                if (data != null) {
                    double mLatitude = data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0);
                    double mLongitude = data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0);
                    String mAddress = data.getStringExtra(Constants.EXTRA_ADDRESS);
                    mSuburb = new SuburbInfo();
                    mSuburb.setLng(mLongitude);
                    mSuburb.setLat(mLatitude);
                    mSuburb.setAddress(mAddress);

                    if (mAddress != null) {
                        fragmentEditCoachPersonalBinding.etSuburb.setText(mAddress);
                    }
                } else {
                    mSuburb = null;
                    fragmentEditCoachPersonalBinding.etSuburb.setText("");
                }
            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST) {
                if (data != null) {
                    sports = data.getParcelableExtra(Constants.EXTRA_SPORT);
                    fragmentEditCoachPersonalBinding.etSyc.setText(sports.getName());
                    fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText("");
                    if(mTeamsList != null && mTeamsList.size() > 0) {
                        mTeamsList.clear();
                    }
                }
            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS) {
                if (data != null) {
                    mTeamsList = data.getParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST);
                    fragmentEditCoachPersonalBinding.etTeamsYouPlayed.setText(Utils.getCommaSepString(mTeamsList));
                }
            }
        }
    }


    private void getCaptureUri(Intent data) {
        showCropDialog(data);
        Uri imageUri = Utils.getPickImageResultUri(data, getActivity());

        // For API >= 23 we need to check specifically that we have permissions to read external storage,
        // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                Utils.isUriRequiresPermissions(imageUri, getActivity())) {

            // request permissions and handle the result in onRequestPermissionsResult()
            requirePermissions = true;
            mCropImageUri = imageUri;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            //LogUtils.LOGD(TAG, "Permission Granted>");

        }

        if (!requirePermissions) {
            mCropImageView.setImageUriAsync(imageUri);
            //LogUtils.LOGD(TAG, " Image Cropped>");
            String path = Utils.getRealPathFromURI(getActivity(), imageUri);
            LogUtils.LOGD("path>>", path);
            //initS3Upload(path);
            /* showProgressBar();
            AwsUtil.beginUpload(path, Constants.AWS_FOLDER_PROFILE, AwsUtil.getTransferUtility
                            (this),
                    this);*/

            try {
                if (Utils.getBitmapFromUri(getActivity(), imageUri) == null) {
                    //LogUtils.LOGD(TAG, " null>");
                    if (mDialog != null && mDialog.isShowing()) {
                        showSnackbarFromTop("Please select valid image");
                        mDialog.dismiss();
                    }
                }
            } catch (IOException e) {
                //LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());
                LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());

            }


        }
    }

    public void showCropDialog(final Intent data) {
        mDialog = new Dialog(getActivity(), android.R.style.Theme_Light);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_image_crop);
        mCropImageView = (CropImageView) mDialog.findViewById(R.id.image_crop);

        mCropImageView.setAspectRatio(ASPECT_RATIO_SQUARE_X, ASPECT_RATIO_SQUARE_Y);


        Button btnCrop = (Button) mDialog.findViewById(R.id.button_crop);
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                // Bitmap cropped = mCropImageView.getCroppedImage(500, 200);
                Bitmap cropped = mCropImageView.getCroppedImage();
                if (cropped != null) {

                    fragmentEditCoachPersonalBinding.imageUser.setImageBitmap(cropped);
                    mCroppedImage = cropped;

                    Uri imageUri = Utils.getPickImageResultUri(data, getActivity());
                    mCropImageView.setImageUriAsync(imageUri);
                    //LogUtils.LOGD(TAG, " Image Cropped>");

                    Uri uri = Utils.getImageUri(getActivity(), cropped);
                    String path = Utils.getRealPathFromURI(getActivity(), uri);
                    LogUtils.LOGD("path>>", path);


                    /*
                    showProgressBar();
                    AwsUtil.beginUpload(path, Constants.AWS_FOLDER_PROFILE, AwsUtil.getTransferUtility
                                    (CoachCreateProfileActivity.this),
                            CoachCreateProfileActivity.this);
*/


                    fragmentEditCoachPersonalBinding.buttonNext.setEnabled(false);
                    uploadImage();
                } else {
                    mCroppedImage = null;
                }
            }
        });

        Button buttonCancel = (Button) mDialog.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void uploadImage() {
        showProgressBar();
        File file = ImageUtil.getFileFromBitmap(mCroppedImage, getActivity());

        RequestBody fbody = null;
        if (file != null) {
            fbody = RequestBody.create(MediaType.parse("image"), file);
        }
        RequestBody type = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE);

        AuthWebServices client = RequestController.getInstance().createService(AuthWebServices.class, true);
        client.uploadImage(type, fbody).enqueue(new BaseCallback<BaseResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(BaseResponse response) {
                fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                if (response != null && response.getStatus() == 1) {
                    //LogUtils.LOGD(TAG, "Image Upload success");
                    // showSnackbarFromTop(CoachCreateProfileActivity.this,response.getMessage());
                } else {
                    fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                    if(response != null && response.getMessage() != null
                            && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean isInputValid() {
        boolean isValid = true;

        if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etName.getText().toString().trim())) {
            isValid = false;
            fragmentEditCoachPersonalBinding.etName.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_name));

        } else if (TextUtils.isEmpty(mDobFormatted)) {
            isValid = false;
            fragmentEditCoachPersonalBinding.etAge.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_age));

        } else if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etMobileNo.getText().toString().trim())) {
            isValid = false;

            fragmentEditCoachPersonalBinding.etMobileNo.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_mobile_no));

        } else if (fragmentEditCoachPersonalBinding.etMobileNo.getText().toString().length() < 10) {
            isValid = false;

            fragmentEditCoachPersonalBinding.etMobileNo.requestFocus();
            showSnackbarFromTop(getString(R.string.invalid_mobile_no));

        } else if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etSuburb.getText().toString().trim())) {
            isValid = false;
            fragmentEditCoachPersonalBinding.etSuburb.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_suburb));
        }

        // COMMENTED YOUTUBE VIDEO LINK MANDATORY CHECK ON CLIENT FEEDBACK

//        else if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etUploadVideo.getText().toString().trim())) {
//            isValid = false;
//            fragmentEditCoachPersonalBinding.etUploadVideo.requestFocus();
//            showSnackbarFromTop(getString(R.string.empty_upload_video));
//        }
//
//        else if (!StringUtils.isValidUrl(fragmentEditCoachPersonalBinding.etUploadVideo.getText().toString().trim())) {
//            isValid = false;
//            fragmentEditCoachPersonalBinding.etUploadVideo.requestFocus();
//            showSnackbarFromTop(getString(R.string.empty_upload_link));
//        }

        else if (StringUtils.isNullOrEmpty(fragmentEditCoachPersonalBinding.etSyc.getText().toString().trim())) {
            isValid = false;
            showSnackbarFromTop(getString(R.string.txt_err_syc));

        } else if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etTeamsYouPlayed.getText().toString().trim())) {
            isValid = false;

            fragmentEditCoachPersonalBinding.etTeamsYouPlayed.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_teams));
        } else if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etAboutMe.getText().toString().trim())) {
            isValid = false;

            fragmentEditCoachPersonalBinding.etAboutMe.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_about_me));
        } else if (TextUtils.isEmpty(fragmentEditCoachPersonalBinding.etAccolades.getText().toString().trim())) {
            isValid = false;

            fragmentEditCoachPersonalBinding.etAccolades.requestFocus();
            showSnackbarFromTop(getString(R.string.empty_accolades));
        }

        return isValid;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(getBaseActivity()), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(getBaseActivity(), getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE_VIDEO:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    openNativeVideoPicker();

                } else {
                    // permission denied,
                    Toast.makeText(getBaseActivity(), getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;


            case MY_PERMISSIONS_REQUEST_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(getBaseActivity()), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(getBaseActivity(), getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCropImageView.setImageUriAsync(mCropImageUri);
                } else {
                    Toast.makeText(getBaseActivity(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void updateCoachPersonalProfile() {
        processToShowDialog();

        CoachCreateProfileStep1Request request = new CoachCreateProfileStep1Request();
        request.setName(fragmentEditCoachPersonalBinding.etName.getText().toString());
        request.setDob(mDobFormatted);
        request.setMobileNumber(fragmentEditCoachPersonalBinding.etMobileNo.getText().toString());
        request.setGender(mGenderType);
        request.setCoachedSports(sports.getId());
        request.setSuburb(mSuburb);
        request.setCoachType(mCoachType);
        request.setAboutMe(fragmentEditCoachPersonalBinding.etAboutMe.getText().toString());
        request.setAccolades(fragmentEditCoachPersonalBinding.etAccolades.getText().toString());
        if (mCoachType == Constants.COACH_TYPE.PRIVATE)
            request.getTeamPlayedFor().addAll(Arrays.asList(fragmentEditCoachPersonalBinding.etTeamsYouPlayed.getText().toString().split("\\s*,\\s*")));
        else
            request.getTeamPlayedFor().addAll(Utils.getTeamIdList(mTeamsList));
//        request.setVideoLink(fragmentEditCoachPersonalBinding.etUploadVideo.getText().toString());

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.coachCreateProfileStep1(request).enqueue(new BaseCallback<CoachCreateProfileStep1Response>(getBaseActivity()) {
            @Override
            public void onSuccess(CoachCreateProfileStep1Response response) {
                if (response != null && response.getStatus() == 1) {
                    LogUtils.LOGD(TAG, "success: Name:" + response.getResult().getName());
                    Alert.showAlertDialog(getBaseActivity(), getString(R.string.profile_updated_title), getString(R.string.profile_updated_message), getString(R.string.okay), CoachPersonalProfileEditFragment.this);
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachCreateProfileStep1Response> call, BaseResponse baseResponse) {
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void success(String name, String path, int type) {
        fragmentEditCoachPersonalBinding.buttonNext.setEnabled(false);
        LogUtils.LOGD("IMAGE PATH","PATH : "+path);
        if(path.endsWith(".png")||path.endsWith(".jpg")||path.endsWith(".jpeg")||path.endsWith(".PNG")||path.endsWith("JPG")||path.endsWith("JPEG"))
        {
            uploadImage(new File(path), type);
        }
        else
        {
            showSnackbarFromTop("Please select a valid image.");
        }


//        mMediaPaths.add(path);
//        mAdditionalMediaAdapter.notifyDataSetChanged();
    }

    @Override
    public void fail(String message) {
        LogUtils.LOGD(">>>>>>", "" + message);
    }

    @Override
    public void onAddMediaSelected() {
        showAdditionalMediaDialog();
    }

    public void showImagePicker() {
        ImagePickerUtils.add(getBaseActivity().getSupportFragmentManager(), this, 3);
    }


    @Override
    public void onRemoveMediaSelected(int position) {
        removeDoc(mMediaPaths.get(position).getId(),position,false);
    }


    private void uploadImage(final File file, final int type) {
//        String title = mCertificateList.get(type).getCertificateTitle();
//        if (StringUtils.isNullOrEmpty(title)) {
//            showSnackbarFromTop(this, getString(R.string.invalid_ttl));
//            return;
//        }
//        RequestBody certificateTtl = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), title);
        showProgressBar();
        RequestBody fBody = null;
        if (file != null) {
            fBody = RequestBody.create(MediaType.parse("image/*"), file);
        }
        RequestBody rbType = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE_ADDITIONAL_MEDIA_IMAGE);


        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.uploadAdditionalMedia(rbType, fBody).enqueue(new BaseCallback<AdditionalMediaResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(AdditionalMediaResponse certificateResponse) {
                if (certificateResponse != null && certificateResponse.getStatus() == 1) {
                    fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                    if(certificateResponse.getResult() != null) {
                        mAdditionalMediaAdapter.setAdditionalMedia(certificateResponse.getResult());
//                    showSnackbarFromTop(AddCertificatesActivity.this, mPreviousDocId == 0 ? certificateResponse.getMessage() : getString(R.string.txt_upd_suc));
//                    addOrUpdateCertificate(type);
//                    if (mPreviousDocId != 0)
//                        removeDoc(mPreviousDocId, type, true);
                    }
                } else {
                    fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                    if(certificateResponse != null && certificateResponse.getMessage() != null
                            && !TextUtils.isEmpty(certificateResponse.getMessage())) {
                        showSnackbarFromTop( certificateResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<AdditionalMediaResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void removeDoc(int id, final int position, final boolean inBg) {
        if (!inBg)
            showProgressBar();

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.removeAdditionalMedia(id).enqueue(new BaseCallback<BaseResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (inBg) {
                        return;
                    }
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop( response.getMessage());
                    }
                    mMediaPaths.remove(position);
                    mAdditionalMediaAdapter.notifyDataSetChanged();
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void uploadVideo(String videoPath, String videoThumb) {
        showProgressBar();
        File videoFile = null;
        if (!videoPath.contains("http")) {
            videoFile = new File(videoPath);
            if (videoFile == null || !videoFile.exists()) {
                showSnackbarFromTop(getString(R.string.error_msg_no_video_file));
                return;
            }
        }

        File imageFile = null;
        if (!videoThumb.contains("http")) {
            imageFile = new File(videoThumb);
            if (imageFile == null || !imageFile.exists()) {
                showSnackbarFromTop(getString(R.string.error_msg_no_image_file));
                return;
            }
        }

        MultipartBody.Part imagePart = null;
        if (imageFile != null) {
            RequestBody imageFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_IMAGE), imageFile);
            imagePart = MultipartBody.Part.createFormData("imageFile", imageFile.getName(), imageFileBody);
        }

        MultipartBody.Part videoPart = null;
        if (videoFile != null) {
            RequestBody videoFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_VIDEO), videoFile);
            videoPart = MultipartBody.Part.createFormData("videoFile", videoFile.getName(), videoFileBody);
        }

        RequestBody type = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE_ADDITIONAL_MEDIA_VIDEO);

        AuthWebServices client = RequestController.getInstance().createService(AuthWebServices.class, true);
        client.uploadAdditionalMediaVideo(type, imagePart, videoPart).enqueue(new BaseCallback<AdditionalMediaResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(AdditionalMediaResponse certificateResponse) {
                if (certificateResponse != null && certificateResponse.getStatus() == 1) {
                    fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                    if(certificateResponse.getResult() != null) {
                        mAdditionalMediaAdapter.setAdditionalMedia(certificateResponse.getResult());
//                    showSnackbarFromTop(AddCertificatesActivity.this, mPreviousDocId == 0 ? certificateResponse.getMessage() : getString(R.string.txt_upd_suc));
//                    addOrUpdateCertificate(type);
//                    if (mPreviousDocId != 0)
//                        removeDoc(mPreviousDocId, type, true);
                    }
                } else {
                    fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                    if(certificateResponse != null && certificateResponse.getMessage() != null
                            && !TextUtils.isEmpty(certificateResponse.getMessage())) {
                        showSnackbarFromTop( certificateResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<AdditionalMediaResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                fragmentEditCoachPersonalBinding.buttonNext.setEnabled(true);
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void checkPermissionAndOpenVideoPicker()
    {
        boolean requirePermissions = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requirePermissions = true;
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE_VIDEO);
        }

        if (!requirePermissions) {
            openNativeVideoPicker();
        }
    }


    private void showAdditionalMediaDialog() {
        final CharSequence[] items = {"Photos", "Videos","Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Additional Media");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Photos")) {
                    dialog.dismiss();
                    showImagePicker();
                } else if (items[which].equals("Videos")) {
                    dialog.dismiss();
                    checkPermissionAndOpenVideoPicker();
                }
                else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openNativeVideoPicker() {
        Intent videoLibraryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        videoLibraryIntent.setType("video/*");
        videoLibraryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(videoLibraryIntent, Constants.GET_VIDEO);
    }

    private void showVideoPreview(String videoPath) {
        if (videoPath != null && videoPath.length() > 0) {
            Log.d("mylog", "LINE 399 - AddTip - onActivityResult - "
                    + "\n" + "Video path : " + videoPath);
            final File sourceFile = new File(videoPath);
            if (sourceFile.exists()) {
                if (!FileUtils.isMaxSizeFile(sourceFile)) {
                    String videoThumbnailPath =
                            FileUtils.createVideoThumbnail(videoPath, PreferenceUtil.getUserModel().getUserId());
                    if (!TextUtils.isEmpty(videoThumbnailPath)) {
                        File videoThumbFile = new File(videoThumbnailPath);
                        if (videoThumbFile.exists()) {
                            Log.d("mylog", "LINE 408 - AddTip - Video thumbnail path : " + videoThumbnailPath);
                            uploadVideo(videoPath, videoThumbnailPath);
                        }
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.max_file_size_limit_reached));
                }
            } else {
                showSnackbarFromTop(getResources().getString(R.string.unable_to_fetch_video));
            }
        } else {
            showSnackbarFromTop(getResources().getString(R.string.unable_to_fetch_video));
        }
    }

    private void showVideoPreview(Intent data) {
        if (data != null) {
            try {
                Uri uri = data.getData();
                if (uri != null) {
                    String videoPath = FileUtils.getRealVideoPathFromURI(getBaseActivity(), uri.toString());
                    if (videoPath != null && videoPath.length() > 0) {
                        Log.d("mylog", "LINE 399 - AddTip - onActivityResult - "
                                + "Video uri : " + uri.getPath()
                                + "\n" + "Video path : " + videoPath);
                        final File sourceFile = new File(videoPath);
                        if (sourceFile.exists()) {
                            if (!FileUtils.isMaxSizeFile(sourceFile)) {
                                String videoThumbnailPath =
                                        FileUtils.createVideoThumbnail(videoPath, PreferenceUtil.getUserModel().getUserId());
                                if (!TextUtils.isEmpty(videoThumbnailPath)) {
                                    File videoThumbFile = new File(videoThumbnailPath);
                                    if (videoThumbFile.exists()) {
                                        Log.d("mylog", "LINE 408 - AddTip - Video thumbnail path : " + videoThumbnailPath);

//                                        videoFilePath = videoPath;
//                                        imageFilePath = videoThumbnailPath;

                                        uploadVideo(videoPath, videoThumbnailPath);
                                    }
                                }
                            } else {
                                showSnackbarFromTop(getResources().getString(R.string.max_file_size_limit_reached));
                            }
                        } else {
                            showSnackbarFromTop( getResources().getString(R.string.unable_to_fetch_video));
                        }
                    } else {
                        showSnackbarFromTop( getResources().getString(R.string.unable_to_fetch_video));
                    }
                } else {
                    com.sportsstars.util.LogUtils.LOGD("mylog", " uri is null");
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context){
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar("Compressing Video");
        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(Uri.parse(paths[0]), paths[1],0,0,600000);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return  filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            Log.i("Silicompressor", "Path: "+compressedFilePath);
            hideProgressDialog();
            showVideoPreview(compressedFilePath);

        }
    }


    @Override
    public void onOkay() {
        getActivity().finish();
    }
}

