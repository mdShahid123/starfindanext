package com.sportsstars.ui.coach.fragment;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.edmodo.rangebar.RangeBar;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentEditCoachCoachingBinding;
import com.sportsstars.model.CoachInfo;
import com.sportsstars.model.SportDetail;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.CoachCreateProfileStep1Response;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.StringUtils;

import java.util.Locale;

import retrofit2.Call;

public class CoachCoachingEditFragment extends BaseFragment implements View.OnClickListener, Alert.OnClickListener, Alert.OnOkayClickListener {

    private static final int MAX_RANGE_AGE = 100;
    private static final int MIN_RANGE_AGE = 8;
    private static final int TICK_COUNT_AGE = MAX_RANGE_AGE - MIN_RANGE_AGE + 1;

    private boolean isHandsOn;
    private boolean isInspirational;
    private CoachInfo mCoachInfo;
    private UserProfileModel mProfileModel;

    FragmentEditCoachCoachingBinding fragmentEditCoachCoachingBinding;
    private boolean isInitialized;

    @Override
    public String getFragmentName() {
        return CoachCoachingEditFragment.class.getSimpleName();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentEditCoachCoachingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_coach_coaching, container, false);
        initUI();

//        if(!isInitialized)
//        {
//            fragmentEditCoachCoachingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_coach_coaching, container, false);
//            initUI();
//            isInitialized = true;
//        }

        return fragmentEditCoachCoachingBinding.getRoot();
    }

    private void initUI() {
        mCoachInfo = new CoachInfo();
        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if (mProfileModel.getSportDetail() == null)
            mProfileModel.setSportDetail(new SportDetail());
        setCoachingProfileData();
        fragmentEditCoachCoachingBinding.setCoachInf(mProfileModel);


        fragmentEditCoachCoachingBinding.rangeBarAge.setTickCount(TICK_COUNT_AGE);
        fragmentEditCoachCoachingBinding.rangeBarAge.setTickHeight(0);
        fragmentEditCoachCoachingBinding.rangeBarAge.setBarWeight(10);
        fragmentEditCoachCoachingBinding.rangeBarAge.setConnectingLineColor(ContextCompat.getColor(getActivity(), R.color.green_gender));
        fragmentEditCoachCoachingBinding.rangeBarAge.setBarColor(ContextCompat.getColor(getActivity(), R.color.divider_color));
        fragmentEditCoachCoachingBinding.rangeBarAge.setThumbImageNormal(R.drawable.ic_oval_shadow);
        fragmentEditCoachCoachingBinding.rangeBarAge.setThumbImagePressed(R.drawable.ic_oval_shadow);

//        mBinding.rangeBarAge.setThumbIndices(0, TICK_COUNT_AGE - 1);

        int min = Integer.parseInt(mProfileModel.getAgeGroupFrom());
        int max = Integer.parseInt(mProfileModel.getAgeGroupTo());

        int left = min - MIN_RANGE_AGE;
        int right = max - MIN_RANGE_AGE - 1;

        fragmentEditCoachCoachingBinding.rangeBarAge.setThumbIndices(left, right);

        mProfileModel.setAgeGroupFrom(String.format(Locale.getDefault(), "%d YEARS", min));
        mProfileModel.setAgeGroupTo(String.format(Locale.getDefault(), "%d YEARS", max));
        setCallback();
    }


    private void setCallback() {
        fragmentEditCoachCoachingBinding.rangeBarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int min, int max) {
                if(min > max) {
                    int temp = min;
                    min = max;
                    max = temp;
                }

//                if (min < 0) min = 0;
//                if (max > (TICK_COUNT_AGE)) max = TICK_COUNT_AGE;

                if (min < 0) {
                    min = 0;
                    fragmentEditCoachCoachingBinding.rangeBarAge.setThumbIndices(min, max);
                }

                if (max > (TICK_COUNT_AGE - 1)) {
                    max = TICK_COUNT_AGE - 1;
                    fragmentEditCoachCoachingBinding.rangeBarAge.setThumbIndices(min, max);
                }

                int minAge = min + MIN_RANGE_AGE;
                int maxAge = max + MIN_RANGE_AGE;

                fragmentEditCoachCoachingBinding.tvAgeMin.setText(String.format(Locale.getDefault(), "%d YEARS", minAge));
                fragmentEditCoachCoachingBinding.tvAgeMax.setText(String.format(Locale.getDefault(), "%d YEARS", maxAge));

//                mBinding.tvAgeMin.setText(String.format(Locale.getDefault(), "%d YEARS", min < 1 ? 1 : min + 1));
//                mBinding.tvAgeMax.setText(String.format(Locale.getDefault(), "%d YEARS", max > 100 ? 100 : max + 1));
            }
        });
        fragmentEditCoachCoachingBinding.swSgp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    fragmentEditCoachCoachingBinding.etSgpGst.setText("");
                fragmentEditCoachCoachingBinding.sgParent.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        fragmentEditCoachCoachingBinding.swLgp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    fragmentEditCoachCoachingBinding.etLgpGst.setText("");
                fragmentEditCoachCoachingBinding.lgParent.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        fragmentEditCoachCoachingBinding.btnNext.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.tvOooGst.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.etSgpGst.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.etLgpGst.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.relHandsOn.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.relInspirational.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.tvHandsOnInfo.setOnClickListener(this);
        fragmentEditCoachCoachingBinding.tvInspirationalInfo.setOnClickListener(this);
    }

    private void setCoachingProfileData() {
        if (mProfileModel != null) {
            if (mProfileModel.getAgeGroupFrom().contains(" "))
                mProfileModel.setAgeGroupFrom(getAgeFrom());
            if (mProfileModel.getAgeGroupTo().contains(" "))
                mProfileModel.setAgeGroupTo(getAgeTo());
            if (!StringUtils.isNullOrEmpty(mProfileModel.getOneOneBookingTypePrice()) && mProfileModel.getOneOneBookingTypePrice().indexOf('$') == -1)
                mProfileModel.setOneOneBookingTypePrice(getString(R.string.dollar) + mProfileModel.getOneOneBookingTypePrice() + getString(R.string.txt_ph));
            if (!StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice())) {
                fragmentEditCoachCoachingBinding.swSgp.setChecked(true);
                fragmentEditCoachCoachingBinding.sgParent.setVisibility(View.VISIBLE);
                if (mProfileModel.getSmallBookingTypePrice().indexOf('$') == -1)
                    mProfileModel.setSmallBookingTypePrice(getString(R.string.dollar) + mProfileModel.getSmallBookingTypePrice() + getString(R.string.txt_ph));
            }
            if (!StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice())) {
                fragmentEditCoachCoachingBinding.swLgp.setChecked(true);
                fragmentEditCoachCoachingBinding.lgParent.setVisibility(View.VISIBLE);
                if (mProfileModel.getLargeBookingTypePrice().indexOf('$') == -1)
                    mProfileModel.setLargeBookingTypePrice(getString(R.string.dollar) + mProfileModel.getLargeBookingTypePrice() + getString(R.string.txt_ph));
            }
            if (!StringUtils.isNullOrEmpty(mProfileModel.getSessionType())) {
                isHandsOn = mProfileModel.getSessionType().equals("1") || mProfileModel.getSessionType().equals("3");
                isInspirational = mProfileModel.getSessionType().equals("2") || mProfileModel.getSessionType().equals("3");
            }
            handsOn();
            inspirational();
        }
    }

    private String getAgeFrom() {
        if (mProfileModel.getAgeGroupFrom().contains(" ")) {
            return mProfileModel.getAgeGroupFrom().substring(0, mProfileModel.getAgeGroupFrom().indexOf(" "));
        }
        return "8";
    }

    private String getAgeTo() {
        if (mProfileModel.getAgeGroupTo().contains(" ")) {
            return mProfileModel.getAgeGroupTo().substring(0, mProfileModel.getAgeGroupTo().indexOf(" "));
        }
        return "100";
    }

    private void inspirational() {
        if (isInspirational) {
            fragmentEditCoachCoachingBinding.relInspirational.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_border_green_bg_grey));
            fragmentEditCoachCoachingBinding.tvInspirational.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_gender));

            fragmentEditCoachCoachingBinding.tvInspirationalInfo.setTextColor(ContextCompat.getColor(getActivity(),R.color.green_gender));
            fragmentEditCoachCoachingBinding.tvInspirationalInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
            return;
        }
        fragmentEditCoachCoachingBinding.relInspirational.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_grey));
        fragmentEditCoachCoachingBinding.tvInspirational.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));

        fragmentEditCoachCoachingBinding.tvInspirationalInfo.setTextColor(ContextCompat.getColor(getActivity(),R.color.black_25));
        fragmentEditCoachCoachingBinding.tvInspirationalInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);
    }

    private void handsOn() {
        if (isHandsOn) {
            fragmentEditCoachCoachingBinding.relHandsOn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_border_green_bg_grey));
            fragmentEditCoachCoachingBinding.tvHandOn.setTextColor(ContextCompat.getColor(getActivity(), R.color.green_gender));

            fragmentEditCoachCoachingBinding.tvHandsOnInfo.setTextColor(ContextCompat.getColor(getActivity(),R.color.green_gender));
            fragmentEditCoachCoachingBinding.tvHandsOnInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
            return;
        }
        fragmentEditCoachCoachingBinding.relHandsOn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_rounded_grey));
        fragmentEditCoachCoachingBinding.tvHandOn.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_25));

        fragmentEditCoachCoachingBinding.tvHandsOnInfo.setTextColor(ContextCompat.getColor(getActivity(),R.color.black_25));
        fragmentEditCoachCoachingBinding.tvHandsOnInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);
    }

    @Override
    public void onClick(View v) {
        if (v == fragmentEditCoachCoachingBinding.btnNext) {
            String errorMsg = validate();
            if (errorMsg != null) {
                showSnackbarFromTop(errorMsg);
                return;
            }
            updateCoachCoachingProfile();
        } else if (v == fragmentEditCoachCoachingBinding.relHandsOn) {
            isHandsOn = !isHandsOn;
            handsOn();
        } else if (v == fragmentEditCoachCoachingBinding.relInspirational) {
            isInspirational = !isInspirational;
            inspirational();
        } else if (v == fragmentEditCoachCoachingBinding.tvOooGst) {
            Alert.createYesNoEditAlert(getActivity(), getString(R.string.txt_oto_lbl), getString(R.string.txt_gst_hnt), getString(R.string.txt_gst_et_hnt), getOneOnOne(), getString(R.string.txt_ok), this, Constants.SESSIONS.OOO).show();
        } else if (v == fragmentEditCoachCoachingBinding.etSgpGst) {
            Alert.createYesNoEditAlert(getActivity(), getString(R.string.txt_sgp_lbl), getString(R.string.txt_gst_hnt), getString(R.string.txt_gst_et_hnt), getSmallOne(), getString(R.string.txt_ok), this, Constants.SESSIONS.SGP).show();
        } else if (v == fragmentEditCoachCoachingBinding.etLgpGst) {
            Alert.createYesNoEditAlert(getActivity(), getString(R.string.txt_lgp_lbl), getString(R.string.txt_gst_hnt), getString(R.string.txt_gst_et_hnt), getLargeOne(), getString(R.string.txt_ok), this, Constants.SESSIONS.LGP).show();
        } else if (v == fragmentEditCoachCoachingBinding.tvHandsOnInfo) {
            showHandsOnInfoDialog();
        } else if (v == fragmentEditCoachCoachingBinding.tvInspirationalInfo) {
            showInspirationalInfoDialog();
        }
    }

    private void showHandsOnInfoDialog() {
        String title = getResources().getString(R.string.txt_sst_opt1);
        String message = getResources().getString(R.string.hands_on_info);
        Alert.showInfoDialog(getActivity(),
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showInspirationalInfoDialog() {
        String title = getResources().getString(R.string.txt_sst_opt2);
        String message = getResources().getString(R.string.inspirational_info);
        Alert.showInfoDialog(getActivity(),
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private String getOneOnOne() {
        if (!StringUtils.isNullOrEmpty(mProfileModel.getOneOneBookingTypePrice())) {
            if (mProfileModel.getOneOneBookingTypePrice().startsWith("$"))
                return mProfileModel.getOneOneBookingTypePrice().substring(1, mProfileModel.getOneOneBookingTypePrice().indexOf('/'));
            else
                return mProfileModel.getOneOneBookingTypePrice();
        }
        return "";
    }

    private String getSmallOne() {
        if (!StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice())) {
            if (mProfileModel.getSmallBookingTypePrice().startsWith("$"))
                return mProfileModel.getSmallBookingTypePrice().substring(1, mProfileModel.getSmallBookingTypePrice().indexOf('/'));
            else
                return mProfileModel.getSmallBookingTypePrice();
        }
        return "";
    }

    private String getLargeOne() {
        if (!StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice())) {
            if (mProfileModel.getLargeBookingTypePrice().startsWith("$"))
                return mProfileModel.getLargeBookingTypePrice().substring(1, mProfileModel.getLargeBookingTypePrice().indexOf('/'));
            else
                return mProfileModel.getLargeBookingTypePrice();
        }
        return "";
    }

    @Override
    public void onPositive(DialogInterface dialog, String data, int type) {
        if(data!=null) {
            String price = data != null && (data.indexOf('$') != -1 && data.length() == 1) ? "" : data + getString(R.string.txt_ph);
            try {
                Double.parseDouble(data.substring(data.indexOf('$') + 1));
            } catch (Exception e) {
                data = "$";
                price = "";
            }
            switch (type) {
                case Constants.SESSIONS.OOO:
                    if (data.equalsIgnoreCase("$")) {
                        showSnackbarFromTop(getString(R.string.txt_err_fee));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) < 1) {
                        showSnackbarFromTop(getString(R.string.txt_ooo_err_fee_min));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) > 5000) {
                        showSnackbarFromTop(getString(R.string.txt_ooo_err_fee_max));
                        return;
                    } else if (StringUtils.isNullOrEmpty(data) || (data.indexOf('$') != -1 && data.length() == 1)) {
                        showSnackbarFromTop(getString(R.string.txt_err_fee));
                        //fragmentEditCoachCoachingBinding.etLgpGst.setText("");
                        return;
                    }
//                else if (!StringUtils.isNullOrEmpty(getSmallOne())) {
//                    double sgp = Double.parseDouble(getSmallOne());
//                    double ooo = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(sgp, ooo) <= 0) {
//                        showSnackbarFromTop(getString(R.string.txt_err_one_on_one_greater_than_small_group));
//                        return;
//                    }
//                    //fragmentEditCoachCoachingBinding.etLgpGst.setText(price);
//                }
//                else if (!StringUtils.isNullOrEmpty(getLargeOne())) {
//                    double lgp = Double.parseDouble(getLargeOne());
//                    double ooo = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(lgp, ooo) <= 0) {
//                        showSnackbarFromTop(getString(R.string.txt_err_one_on_one_greater_than_large_group));
//                        return;
//                    }
//                    //fragmentEditCoachCoachingBinding.etLgpGst.setText(price);
//                }

                    fragmentEditCoachCoachingBinding.tvOooGst.setText(price);
//                fragmentEditCoachCoachingBinding.swSgp.setChecked(false);
//                fragmentEditCoachCoachingBinding.swLgp.setChecked(false);
                    fragmentEditCoachCoachingBinding.swSgp.setChecked(StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice()) ? false : true);
                    fragmentEditCoachCoachingBinding.swLgp.setChecked(StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice()) ? false : true);
                    break;
                case Constants.SESSIONS.SGP:
                    if (data.equalsIgnoreCase("$")) {
                        showSnackbarFromTop(getString(R.string.txt_err_sgf));
                        return;
                    } else if (StringUtils.isNullOrEmpty(getOneOnOne())) {
                        fragmentEditCoachCoachingBinding.etSgpGst.setText("");
                        showSnackbarFromTop(getString(R.string.txt_err_fee));
                        return;
                    } else if (StringUtils.isNullOrEmpty(data) || (data.indexOf('$') != -1 && data.length() == 1)) {
                        fragmentEditCoachCoachingBinding.etSgpGst.setText("");
//                    fragmentEditCoachCoachingBinding.swLgp.setChecked(false);
                        fragmentEditCoachCoachingBinding.swLgp.setChecked(StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice()) ? false : true);
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) < 1) {
                        showSnackbarFromTop(getString(R.string.txt_sgp_err_fee_min));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) > 5000) {
                        showSnackbarFromTop(getString(R.string.txt_sgp_err_fee_max));
                        return;
                    } else {
//                    double sgp = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(sgp, Double.parseDouble(getOneOnOne())) <= 0) {
//                        showSnackbarFromTop(getString(R.string.txt_err_so_ph));
//                        return;
//                    } else if (!StringUtils.isNullOrEmpty(getLargeOne())) {
//                        double lgp = Double.parseDouble(getLargeOne());
//                        if (Double.compare(sgp, lgp) >= 0) {
//                            showSnackbarFromTop(getString(R.string.txt_err_sl_ph));
//                            return;
//                        }
//                    }
                        fragmentEditCoachCoachingBinding.etSgpGst.setText(price);
                    }
                    break;
                case Constants.SESSIONS.LGP:
                    if (data.equalsIgnoreCase("$")) {
                        showSnackbarFromTop(getString(R.string.txt_err_lgf));
                        return;
                    } else if (StringUtils.isNullOrEmpty(getOneOnOne())) {
                        fragmentEditCoachCoachingBinding.etLgpGst.setText("");
                        showSnackbarFromTop(getString(R.string.txt_err_fee));
                        return;
                    } else if (StringUtils.isNullOrEmpty(data) || (data.indexOf('$') != -1 && data.length() == 1)) {
                        fragmentEditCoachCoachingBinding.etLgpGst.setText("");
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) < 1) {
                        showSnackbarFromTop(getString(R.string.txt_lgp_err_fee_min));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) > 5000) {
                        showSnackbarFromTop(getString(R.string.txt_lgp_err_fee_max));
                        return;
                    } else if (StringUtils.isNullOrEmpty(getSmallOne())) {
//                    double ooo = Double.parseDouble(getOneOnOne());
//                    double lgp = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(lgp, ooo) <= 0) {
//                        showSnackbarFromTop(getString(R.string.txt_err_lo_ph));
//                        return;
//                    }
                        fragmentEditCoachCoachingBinding.etLgpGst.setText(price);
                    } else {
//                    double sgp = Double.parseDouble(getSmallOne());
//                    double lgp = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(lgp, sgp) <= 0) {
//                        showSnackbarFromTop(getString(R.string.txt_err_ls_ph));
//                        return;
//                    }
                        fragmentEditCoachCoachingBinding.etLgpGst.setText(price);
                    }

            }
            hideKeyboard();
        }
    }

    @Override
    public void onNegative(DialogInterface dialog) {

    }

    private String validate() {
        if (!isHandsOn && !isInspirational)
            return getString(R.string.txt_err_ssn);
        if (StringUtils.isNullOrEmpty(mProfileModel.getSpecialities()))
            return getString(R.string.txt_err_esp);
        if (StringUtils.isNullOrEmpty(mProfileModel.getOneOneBookingTypePrice()))
            return getString(R.string.txt_err_fee);
        if (fragmentEditCoachCoachingBinding.swSgp.isChecked() && StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice()))
            return getString(R.string.txt_err_sgf);
        if (fragmentEditCoachCoachingBinding.swLgp.isChecked() && StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice()))
            return getString(R.string.txt_err_lgf);
        return null;
    }

    private void updateCoachCoachingProfile() {
        processToShowDialog();
        mCoachInfo.setAgeGroupFrom(getAgeFrom());
        mProfileModel.setAgeGroupFrom(mCoachInfo.getAgeGroupFrom());
        mCoachInfo.setAgeGroupTo(getAgeTo());
        mProfileModel.setAgeGroupTo(mCoachInfo.getAgeGroupTo());
        mCoachInfo.setOneOneBookingTypePrice(getOneOnOne());
        mProfileModel.setOneOneBookingTypePrice(mCoachInfo.getOneOneBookingTypePrice());
        mCoachInfo.setSmallBookingTypePrice(getSmallOne());
        mProfileModel.setSmallBookingTypePrice(mCoachInfo.getSmallBookingTypePrice());
        mCoachInfo.setLargeBookingTypePrice(getLargeOne());
        mProfileModel.setLargeBookingTypePrice(mCoachInfo.getLargeBookingTypePrice());
        mCoachInfo.setSpecialities(mProfileModel.getSpecialities());
        mCoachInfo.setSessionType(isHandsOn && isInspirational ? Constants.SESSIONS.SESSION_BOTH : isHandsOn ? Constants.SESSIONS.SESSION_HANDS_ON : Constants.SESSIONS.SESSION_INSPIRATIONAL);
        mProfileModel.setSessionType(mCoachInfo.getSessionType());
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.coachCreateProfileStep2(mCoachInfo).enqueue(new BaseCallback<CoachCreateProfileStep1Response>(getBaseActivity()) {
            @Override
            public void onSuccess(CoachCreateProfileStep1Response response) {
                if (response != null && response.getStatus() == 1) {
                    Alert.showAlertDialog(getBaseActivity(), getString(R.string.profile_updated_title), getString(R.string.profile_updated_message), getString(R.string.okay), CoachCoachingEditFragment.this);
                    //LogUtils.LOGD(TAG, "success: Name:" + response.getResult().getName());
                } else {
                    if(response != null && response.getMessage() != null
                            && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<CoachCreateProfileStep1Response> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(TAG, "failed: Name:" + call.toString());
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onOkay() {
        getActivity().finish();
    }
}
