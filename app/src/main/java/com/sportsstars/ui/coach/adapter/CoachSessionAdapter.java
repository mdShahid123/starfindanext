/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.coach.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.CoachSessionListItemBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.response.coachsession.CoachSession;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CoachSessionAdapter extends RecyclerView.Adapter<CoachSessionAdapter.MyViewHolder> {

    private ArrayList<CoachSession> mSessionList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ListItemClickListener mListItemClickListener;

    public CoachSessionAdapter(Activity mActivity, ArrayList<CoachSession> mSessionList,
                               ListItemClickListener mListItemClickListener) {
        this.mSessionList = mSessionList;
        this.mActivity = mActivity;
        this.mListItemClickListener = mListItemClickListener;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CoachSessionListItemBinding mBinding = DataBindingUtil.inflate(mInflator, R.layout.coach_session_list_item, parent, false);
        return new MyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (mSessionList != null && mSessionList.size() > 0) {
            holder.bindData(mSessionList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return mSessionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CoachSessionListItemBinding coachSessionListItemBinding;

        public MyViewHolder(CoachSessionListItemBinding coachSessionListItemBinding) {
            super(coachSessionListItemBinding.getRoot());
            this.coachSessionListItemBinding = coachSessionListItemBinding;
        }

        public CoachSessionListItemBinding getBinding() {
            return coachSessionListItemBinding;
        }

        public void bindData(CoachSession coachSession, int position) {
            coachSessionListItemBinding.setCoachSession(coachSession);
            if(coachSession != null && !TextUtils.isEmpty(coachSession.getIcon())) {
                Picasso.with(mActivity)
                        .load(coachSession.getIcon())
                        .placeholder(R.drawable.ic_cricket_green)
                        .error(R.drawable.ic_cricket_green)
                        .into(coachSessionListItemBinding.ivSportImage);
            }
            if(coachSession != null && !TextUtils.isEmpty(coachSession.getProfileImage())) {
                Picasso.with(mActivity)
                        .load(coachSession.getProfileImage())
                        .placeholder(R.drawable.ic_nav_profile_unselected)
                        .error(R.drawable.ic_nav_profile_unselected)
                        .into(coachSessionListItemBinding.ivProfilePic);
            }
            if(coachSession != null && !TextUtils.isEmpty(coachSession.getSessionDate())) {
                String utcDateTime = coachSession.getSessionDate();
                String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                        DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
                Log.d("mylog", "CoachSessionAdapter - utcDateTime : " + utcDateTime
                        + " :: localDateTime : " + localDateTime);
                coachSessionListItemBinding.tvSessionDate.setText(Utils.getFormattedDateTime(localDateTime));
                coachSessionListItemBinding.tvSessionTime.setText(Utils.parseTimeForDDMM(localDateTime));
            }
            coachSessionListItemBinding.rlSessionItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListItemClickListener.onListItemClicked(position);
                }
            });
        }

    }

}
