/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.coach.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.sportsstars.R;
import com.sportsstars.databinding.CoachTipsListItemBinding;
import com.sportsstars.interfaces.CoachTipsItemClickListener;
import com.sportsstars.network.response.coachtips.CoachTips;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CoachTipsAdapter extends RecyclerView.Adapter<CoachTipsAdapter.MyViewHolder> {

    private ArrayList<CoachTips> mTipsList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private CoachTipsItemClickListener mListItemClickListener;
    private ViewBinderHelper binderHelper = new ViewBinderHelper();

    public CoachTipsAdapter(Activity mActivity, ArrayList<CoachTips> mTipsList,
                            CoachTipsItemClickListener mListItemClickListener) {
        this.mTipsList = mTipsList;
        this.mActivity = mActivity;
        this.mListItemClickListener = mListItemClickListener;
        mInflator = LayoutInflater.from(mActivity);
        binderHelper.setOpenOnlyOne(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CoachTipsListItemBinding mBinding = DataBindingUtil.inflate(mInflator, R.layout.coach_tips_list_item, parent, false);
        return new MyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (mTipsList != null && mTipsList.size() > 0) {
            holder.bindData(mTipsList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return mTipsList.size();
    }

    public void updateList(ArrayList<CoachTips> coachTipsList) {
        mTipsList = coachTipsList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CoachTipsListItemBinding coachTipsListItemBinding;

        public MyViewHolder(CoachTipsListItemBinding coachTipsListItemBinding) {
            super(coachTipsListItemBinding.getRoot());
            this.coachTipsListItemBinding = coachTipsListItemBinding;
        }

        public CoachTipsListItemBinding getBinding() {
            return coachTipsListItemBinding;
        }

        public void bindData(CoachTips coachTips, int position) {

            binderHelper.bind(coachTipsListItemBinding.swipeLayout, String.valueOf(position));

            coachTipsListItemBinding.setCoachTips(coachTips);

            if(coachTips != null && !TextUtils.isEmpty(coachTips.getIcon())) {
                Picasso.with(mActivity)
                        .load(coachTips.getIcon())
                        .placeholder(R.drawable.ic_cricket_green)
                        .error(R.drawable.ic_cricket_green)
                        .into(coachTipsListItemBinding.ivSportImage);
            }

            if(coachTips != null && !TextUtils.isEmpty(coachTips.getImageUrl())) {
                Picasso.with(mActivity)
                        .load(coachTips.getImageUrl())
                        .into(coachTipsListItemBinding.ivTipThumbnail);
            }

            coachTipsListItemBinding.flTipsItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListItemClickListener.onCoachTipsItemClick(position);
                }
            });

            coachTipsListItemBinding.flDeleteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        binderHelper.closeLayout(String.valueOf(position));
                        mListItemClickListener.onCoachTipsItemDelete(position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

}
