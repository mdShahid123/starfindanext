package com.sportsstars.ui.coach;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CompoundButton;

import com.sportsstars.R;
import com.sportsstars.databinding.EditCoachProfileActivityBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.ui.coach.fragment.CoachCoachingEditFragment;
import com.sportsstars.ui.coach.fragment.CoachPersonalProfileEditFragment;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class EditCoachProfileActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    private EditCoachProfileActivityBinding mEditCoachProfileActivityBinding;
    private CoachPersonalProfileEditFragment coachPersonalProfileEditFragment = new CoachPersonalProfileEditFragment();
    private CoachCoachingEditFragment coachCoachingEditFragment = new CoachCoachingEditFragment();
    private int mCurrentTabId;

    @Override
    public String getActivityName() {
        return EditCoachProfileActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEditCoachProfileActivityBinding = DataBindingUtil.setContentView(this, R.layout.edit_coach_profile_activity);
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }
        initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void initUI() {
        mEditCoachProfileActivityBinding.header.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mEditCoachProfileActivityBinding.header.tvTitle.setText(getString(R.string.edit_profile));

        switchToPersonalEdit();

        mEditCoachProfileActivityBinding.personalRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentTabId != mEditCoachProfileActivityBinding.personalRadioBtn.getId()) {
                    mEditCoachProfileActivityBinding.personalRadioBtn.setChecked(false);
                    Alert.showAlertDialogWithBottomButton(EditCoachProfileActivity.this, getString(R.string.save_changes), getString(R.string.sure_to_switch_tabs), getString(R.string.cancel), getString(R.string.proceed), new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            switchToPersonalEdit();
                        }
                    }, new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {
                            switchToCoachingEdit();
                        }
                    });
                }
            }
        });

        mEditCoachProfileActivityBinding.coachingRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentTabId != mEditCoachProfileActivityBinding.coachingRadioBtn.getId()) {
                    mEditCoachProfileActivityBinding.coachingRadioBtn.setChecked(false);
                    Alert.showAlertDialogWithBottomButton(EditCoachProfileActivity.this, getString(R.string.save_changes), getString(R.string.sure_to_switch_tabs), getString(R.string.cancel), getString(R.string.proceed), new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            switchToCoachingEdit();
                        }
                    }, new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {
                            switchToPersonalEdit();
                        }
                    });
                }
            }
        });
        Typeface font = Typeface.createFromAsset(this.getAssets(),
                getResources().getString(R.string.font_bebas_neue));
        mEditCoachProfileActivityBinding.personalRadioBtn.setTypeface(font);
        mEditCoachProfileActivityBinding.coachingRadioBtn.setTypeface(font);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.personalRadioBtn:
                if (isChecked) {
                    mEditCoachProfileActivityBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.black));
                    switchToPersonalEdit();
                } else
                    mEditCoachProfileActivityBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
                break;
            case R.id.coachingRadioBtn:
                if (isChecked) {
                    mEditCoachProfileActivityBinding.coachingRadioBtn.setTextColor(getResources().getColor(R.color.black));
                    switchToCoachingEdit();
                } else
                    mEditCoachProfileActivityBinding.coachingRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
                break;
        }
    }

    public void switchToPersonalEdit() {
        mCurrentTabId = mEditCoachProfileActivityBinding.personalRadioBtn.getId();
        mEditCoachProfileActivityBinding.personalRadioBtn.setChecked(true);
        mEditCoachProfileActivityBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.black));
        mEditCoachProfileActivityBinding.coachingRadioBtn.setChecked(false);
        mEditCoachProfileActivityBinding.coachingRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
        coachPersonalProfileEditFragment = new CoachPersonalProfileEditFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.editCoachProfilcontainer, coachPersonalProfileEditFragment).commit();
    }

    public void switchToCoachingEdit() {
        mCurrentTabId = mEditCoachProfileActivityBinding.coachingRadioBtn.getId();
        mEditCoachProfileActivityBinding.coachingRadioBtn.setChecked(true);
        mEditCoachProfileActivityBinding.coachingRadioBtn.setTextColor(getResources().getColor(R.color.black));
        mEditCoachProfileActivityBinding.personalRadioBtn.setChecked(false);
        mEditCoachProfileActivityBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
        coachCoachingEditFragment = new CoachCoachingEditFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.editCoachProfilcontainer, coachCoachingEditFragment).commit();
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(otpVerifiedEvent.isOtpVerified()) {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
