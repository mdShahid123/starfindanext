package com.sportsstars.ui.coach.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.interfaces.AddTeamItemClick;
import com.sportsstars.model.Team;
import com.sportsstars.ui.coach.adapter.viewholder.AddTeamViewHolder;

import java.util.List;

/**
 * Created by rohantaneja on 17/04/18.
 */
public class AddTeamRecyclerViewAdapter extends RecyclerView.Adapter<AddTeamViewHolder> implements AddTeamItemClick {

    private final Context mContext;
    private List<Team> mTeamsList;
    private AddTeamItemClick mTeamClickListener;

    public AddTeamRecyclerViewAdapter(List<Team> teamsList, Context context, AddTeamItemClick teamClickListener) {
        mTeamsList = teamsList;
        mContext = context;
        mTeamClickListener = teamClickListener;
    }

    @Override
    public AddTeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_team, parent, false);
        return new AddTeamViewHolder(mContext, v, this);
    }

    @Override
    public void onBindViewHolder(AddTeamViewHolder holder, int position) {
        Team team = mTeamsList.get(position);

        holder.bindData(team);

        //need to inform about last position to hide divider in list item
        if (position == mTeamsList.size() - 1)
            holder.hideDivider();

    }

    @Override
    public int getItemCount() {
        return mTeamsList == null ? 0 : mTeamsList.size();
    }

    @Override
    public void onTeamSelected(Team team) {
        mTeamClickListener.onTeamSelected(team);
    }
}