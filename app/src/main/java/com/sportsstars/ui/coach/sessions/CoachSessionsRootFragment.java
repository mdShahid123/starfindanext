/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.coach.sessions;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentCoachSessionsRootBinding;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.common.HomeActivity;

import java.lang.ref.WeakReference;

public class CoachSessionsRootFragment extends BaseFragment {

    private FragmentCoachSessionsRootBinding mBinding;
    private FragmentManager mFragmentManagerCoach;
    private WeakReference<HomeActivity> mActivity;

    public static CoachSessionsRootFragment newInstance() {
        return new CoachSessionsRootFragment();
    }

    @Override
    public String getFragmentName() {
        return CoachSessionsRootFragment.class.getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((HomeActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coach_sessions_root, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        mFragmentManagerCoach = getActivity().getSupportFragmentManager();

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                getResources().getString(R.string.font_bebas_neue));
        mBinding.radioBtn1.setTypeface(font);
        mBinding.radioBtn2.setTypeface(font);

        mBinding.radioBtn1.setChecked(true);
        loadFragment(0, true);
        mBinding.radioGroupHeader.setOnCheckedChangeListener(radioListener);
    }

    private RadioGroup.OnCheckedChangeListener radioListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                switch (checkedId) {
                    case R.id.radio_btn_1:
                        loadFragment(0, true);
                        break;
                    case R.id.radio_btn_2:
                        loadFragment(1, true);
                        break;
                }
            }
        }
    };

    private void loadFragment(int position, boolean isReload) {
        switch (position) {
            case 0:
                CoachSessionListFragment coachSessionListFragment = CoachSessionListFragment.newInstance();
                if (mFragmentManagerCoach != null) {
                    mFragmentManagerCoach.beginTransaction().replace(R.id.frame_layout, coachSessionListFragment).commit();
                }
                if (mActivity != null) {
                    mActivity.get().setUpHeaderForCoachHome(mActivity.get().mBinder.headerId,
                            getResources().getString(R.string.my_sessions), "");
                }
                break;
            case 1:
                CoachTipsListFragment coachTipsListFragment = CoachTipsListFragment.newInstance();
                if (mFragmentManagerCoach != null) {
                    mFragmentManagerCoach.beginTransaction().replace(R.id.frame_layout, coachTipsListFragment).commit();
                }
                if (mActivity != null) {
                    mActivity.get().setUpHeaderForCoachHome(mActivity.get().mBinder.headerId,
                            getResources().getString(R.string.my_tips), "");
                }
                break;
        }
    }

}
