package com.sportsstars.ui.coach;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentSetAvailabilityBinding;
import com.sportsstars.model.SelectedDates;
import com.sportsstars.model.SlotTime;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetAvailabilityRequest;
import com.sportsstars.network.request.auth.SetAvailabilityRequest;
import com.sportsstars.network.request.auth.UnavailableMeRequest;
import com.sportsstars.network.request.auth.UserRoleRequest;
import com.sportsstars.network.response.auth.AvailabilityResult;
import com.sportsstars.network.response.auth.GetCoachAvailabilityResponse;
import com.sportsstars.network.response.auth.UserRoleResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.AvailabilityTimeAdapter;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;

import static com.sportsstars.util.LogUtils.LOGE;

/**
 * Created by abhaykant on 16/04/18.
 */

public class SetCoachAvailabilityFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(SetCoachAvailabilityFragment.class);
    private FragmentSetAvailabilityBinding mBinding;

    private String mCurrentMonthStart, mCurrentDayStart;
    private String mCurrentMonthEnd, mCurrentDayEnd;
    private String mCurrentYearStart, mCurrentYearEnd;

    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private Calendar mCalender;
    private int weekDaysCount = 0;
    private ArrayList<Integer> mSelectedAvailabilityHours;

    private ArrayList<SlotTime> mCurrentSlotTimes;
    private String mSelectedDate;
    private String[] selectedWeekDays;
    private String[] selectedWeekDates;
    private ArrayList<SelectedDates> mSelectedWeekDatesList;
    private ArrayList<AvailabilityResult> mAvailabiltyResults;

    private String mTodayDate;
    private int mTodayTime;
    private int mCount = 0;

    private String mNextDayDate;
    private int mNextDayTime;

    private WeakReference<HomeActivity> mActivity;

    private boolean isFutureWeek;

    private String startDateFormatted, endDateFormatted;

    private String startDateCalOff, endDateCalOff;
    private boolean isTurningOffCalendar;

    @Override
    public String getFragmentName() {
        return SetCoachAvailabilityFragment.class.getSimpleName();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((HomeActivity) getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
//        try {
//            if(isTurningOffCalendar) {
//                init();
//                isTurningOffCalendar = false;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_set_availability, container, false);
        init();
        return mBinding.getRoot();
    }

    private void init() {

        PreferenceUtil.setCoachLaunchFirstTime(true);

        mCount = 0;
        weekDaysCount = 0;
        mCurrentMonthStart = "";
        mCurrentDayStart = "";
        mCurrentMonthEnd = "";
        mCurrentDayEnd = "";
        mCurrentYearStart = "";
        mCurrentYearEnd = "";
        mSelectedDate = "";
        mSelectedWeekDatesList = new ArrayList<>();
        mBinding.ivNext.setOnClickListener(this);
        mBinding.ivPrevious.setOnClickListener(this);
        mBinding.tvWeekRange.setOnClickListener(this);

        mBinding.itemMon.getRoot().setOnClickListener(this);
        mBinding.itemMon.getRoot().setOnClickListener(this);
        mBinding.itemTue.getRoot().setOnClickListener(this);
        mBinding.itemWed.getRoot().setOnClickListener(this);
        mBinding.itemThu.getRoot().setOnClickListener(this);
        mBinding.itemFri.getRoot().setOnClickListener(this);
        mBinding.itemSat.getRoot().setOnClickListener(this);
        mBinding.itemSun.getRoot().setOnClickListener(this);
        mBinding.buttonAddBookingsCal.setOnClickListener(this);
        mBinding.buttonCopyFromPrevious.setOnClickListener(this);

        mBinding.itemTue.tvDay.setText(getString(R.string.tue));
        mBinding.itemWed.tvDay.setText(getString(R.string.wed));
        mBinding.itemThu.tvDay.setText(getString(R.string.thu));
        mBinding.itemFri.tvDay.setText(getString(R.string.fri));
        mBinding.itemSat.tvDay.setText(getString(R.string.sat));
        mBinding.itemSun.tvDay.setText(getString(R.string.sun));

        mTodayDate = Utils.getTodayDate();
        mTodayTime = Utils.getTodayTime();

        mNextDayDate = Utils.getNextDayDate();
        mNextDayTime = Utils.getNextDayTime();

        startDateCalOff = mNextDayDate;
        endDateCalOff = mNextDayDate;

        isFutureWeek = true;
        selectedWeekDays = getWeekDay(Calendar.getInstance());
        setDay(selectedWeekDays, mNextDayDate);
        setWeekRangeUi();
        if (!StringUtils.isNullOrEmpty(mCurrentDayStart)) {
            startDateFormatted = mCurrentYearStart + "-" + mCurrentMonthStart + "-" + mCurrentDayStart;
            endDateFormatted = mCurrentYearEnd + "-" + mCurrentMonthEnd + "-" + mCurrentDayEnd;
            getAvailability(startDateFormatted, endDateFormatted, true);
        }

        if (Utils.isNetworkAvailable() && mActivity != null && mActivity.get().isFromNotificationPanel) {
            userRoleApi();
        }

        if(mActivity != null) {
            mActivity.get().tvTurnOff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Creating the instance of PopupMenu
                    PopupMenu popup = new PopupMenu(getActivity(), mActivity.get().tvTurnOff);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.popup_menu_calendar_off, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            if(Utils.isNetworkAvailable()) {
                                turnOffCalendar(item);
                            } else {
                                showSnackbarFromTop(getResources().getString(R.string.no_internet));
                            }
                            //Toast.makeText(BaseActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
            });
        }
    }

    private void turnOffCalendar(MenuItem item) {
        String startDate = "";
        String endDate = "";
        if(item.getItemId() == R.id.one_day) {
            startDate = startDateCalOff;
            endDate = endDateCalOff;
            Log.d("date_log", "One Day - start date : " + startDate + " end date : " + endDate);
            showCalendarOffDialog(startDate, endDate, 1);
        } else if(item.getItemId() == R.id.one_week) {
            startDate = startDateCalOff;
            endDate = endDateCalOff;
            endDateCalOff = Utils.getNextWeekDate(startDate);
            endDate = endDateCalOff;
            Log.d("date_log", "One Week - start date : " + startDate + " end date : " + endDate);
            showCalendarOffDialog(startDate, endDate, 2);
        } else if(item.getItemId() == R.id.one_month) {
            startDate = startDateCalOff;
            endDate = endDateCalOff;
            endDateCalOff = Utils.getNextMonthDate(startDate);
            endDate = endDateCalOff;
            Log.d("date_log", "One Month - start date : " + startDate + " end date : " + endDate);
            showCalendarOffDialog(startDate, endDate, 3);
        }

        //unavailableCalendar(startDate, endDate);
    }

    private void showCalendarOffDialog(final String startDate, final  String endDate, final int turnOffType) {
        String startDateSimpleFormat = startDate;
        String endDateSimpleFormat = endDate;
        try {
            DateFormat originalFormat = new SimpleDateFormat(DateFormatUtils.SERVER_DATE_FORMAT, Locale.getDefault());
            DateFormat targetFormat = new SimpleDateFormat(DateFormatUtils.SIMPLE_DATE_FORMAT, Locale.getDefault());

            Date dateStart = originalFormat.parse(startDate);
            startDateSimpleFormat = targetFormat.format(dateStart);

            Date dateEnd = originalFormat.parse(endDate);
            endDateSimpleFormat = targetFormat.format(dateEnd);

            String message = "";
            if(turnOffType == 1) {
                message = "Are you sure you want to turn off calendar for " + startDateSimpleFormat;
            } else {
                message = "Are you sure you want to turn off calendar from " + startDateSimpleFormat
                        + " to " + endDateSimpleFormat;
            }
            Alert.showOkCancelDialog(getActivity(),
                    getResources().getString(R.string.turn_off_calendar),
                    message,
                    getResources().getString(R.string.no),
                    getResources().getString(R.string.yes),
                    new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            unavailableCalendar(startDate, endDate);
                        }
                    },
                    new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {

                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unavailableCalendar(String startDate, String endDate) {
        isTurningOffCalendar = false;
        if(Utils.isNetworkAvailable()) {
            UnavailableMeRequest unavailableMeRequest = new UnavailableMeRequest();
            unavailableMeRequest.setSlotDateStart(startDate);
            unavailableMeRequest.setSlotDateEnd(endDate);
            processToShowDialog();
            AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
            webServices.unavailableMe(unavailableMeRequest).enqueue(new BaseCallback<BaseResponse>(mActivity.get()) {
                @Override
                public void onSuccess(BaseResponse response) {
                    if (response != null) {
                        if (response.getStatus() == 1) {
                            if(!TextUtils.isEmpty(response.getMessage())) {
                                showToast(response.getMessage());
                                //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                            }
                            //isTurningOffCalendar = true;
                            //onResume();

                            getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                                    mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                        } else {
                            if(!TextUtils.isEmpty(response.getMessage())) {
                                showToast(response.getMessage());
                                //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                            }
                            isTurningOffCalendar = false;
                        }
                    }
                }

                @Override
                public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                    if(!TextUtils.isEmpty(baseResponse.getMessage())) {
                        showToast(baseResponse.getMessage());
                        //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                    }
                    isTurningOffCalendar = false;
                }
            });
        } else {
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void userRoleApi() {

        int mUserRole = Constants.USER_ROLE.COACH;
        PreferenceUtil.setCurrentUserRole(mUserRole);

        UserRoleRequest userRoleRequest = new UserRoleRequest();
        userRoleRequest.setUserLoggedInType(mUserRole);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.userRole(userRoleRequest).enqueue(new BaseCallback<UserRoleResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(UserRoleResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        Log.d("mylog", "User role updated on server.");
                    } else {
                        Log.d("mylog", "User role not updated on server.");
                    }
                }
                if (mActivity != null && mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED) {
                    mActivity.get().isFromNotificationPanel = false;
                }
            }

            @Override
            public void onFail(Call<UserRoleResponse> call, BaseResponse baseResponse) {
                if (mActivity != null && mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED) {
                    mActivity.get().isFromNotificationPanel = false;
                }
                try {
                    if (mActivity != null) {
                        mActivity.get().hideProgressBar();
                    }
                    if (baseResponse != null
                            && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        Log.d("mylog", "User role not updated to coach. Error - " + baseResponse.getMessage());
                        //showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setDay(String[] days, String selectedDateFormated) {
        if (days == null) {
            return;
        }

        mBinding.itemMon.tvDate.setText(days[0]);
        mBinding.itemTue.tvDate.setText(days[1]);
        mBinding.itemWed.tvDate.setText(days[2]);
        mBinding.itemThu.tvDate.setText(days[3]);
        mBinding.itemFri.tvDate.setText(days[4]);
        mBinding.itemSat.tvDate.setText(days[5]);
        mBinding.itemSun.tvDate.setText(days[6]);

        int selectedDay = 0;
        if (!StringUtils.isNullOrEmpty(selectedDateFormated) && selectedDateFormated.contains("-")) {
            String selectedDateFormatedDup = selectedDateFormated;
            String[] selectedDateFormatedDupArr = selectedDateFormatedDup.split("-");
            selectedDay = Integer.parseInt(selectedDateFormatedDupArr[2]);
        }

        int pos = 0;
        if (selectedDay > 0) {
            for (int i = 0; i < days.length; i++) {
                if (selectedDay == Integer.parseInt(days[i])) {
                    pos = i;
                    break;
                }
            }
        }

        if (pos == 0) {
            setSelectedDateAvailability(mBinding.itemMon.getRoot(), mBinding.itemMon.tvDate, mBinding.itemMon.tvDay);
        } else if (pos == 1) {
            setSelectedDateAvailability(mBinding.itemTue.getRoot(), mBinding.itemTue.tvDate, mBinding.itemTue.tvDay);

        } else if (pos == 2) {
            setSelectedDateAvailability(mBinding.itemWed.getRoot(), mBinding.itemWed.tvDate, mBinding.itemWed.tvDay);

        } else if (pos == 3) {
            setSelectedDateAvailability(mBinding.itemThu.getRoot(), mBinding.itemThu.tvDate, mBinding.itemThu.tvDay);

        } else if (pos == 4) {
            setSelectedDateAvailability(mBinding.itemFri.getRoot(), mBinding.itemFri.tvDate, mBinding.itemFri.tvDay);

        } else if (pos == 5) {
            setSelectedDateAvailability(mBinding.itemSat.getRoot(), mBinding.itemSat.tvDate, mBinding.itemSat.tvDay);

        } else if (pos == 6) {
            setSelectedDateAvailability(mBinding.itemSun.getRoot(), mBinding.itemSun.tvDate, mBinding.itemSun.tvDay);

        }

        if (mSelectedWeekDatesList != null && mSelectedWeekDatesList.size() > 0) {
            mSelectedDate = mSelectedWeekDatesList.get(pos).getSelectedDate();
            LogUtils.LOGD(TAG, "selectedDate:::" + mSelectedDate);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_next:
//                try {
//                    int  dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
//                    if(dateStatus == Constants.DATE_STATUS.GREATER) {
//                        isFutureWeek = true;
//                    } else {
//                        isFutureWeek = false;
//                    }
//
//                    showToast("isFutureWeek : " + isFutureWeek);
//
//                    if(isFutureWeek) {
//                        mBinding.buttonCopyFromPrevious.setVisibility(View.VISIBLE);
//                    } else {
//                        mBinding.buttonCopyFromPrevious.setVisibility(View.GONE);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                selectedWeekDays = getWeekDayNext();
                String dateFormated = mSelectedWeekDatesList.get(0).getSelectedDate();
                setDay(selectedWeekDays, dateFormated);

                startDateCalOff = dateFormated;
                endDateCalOff = dateFormated;

                //setDay(getWeekDayNext());
                setWeekRangeUi();

                getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);

//                if(Utils.isSelectedDatePresentInWeek(selectedWeekDays, mTodayDate)) {
//                    mBinding.buttonCopyFromPrevious.setVisibility(View.VISIBLE);
//                    mBinding.buttonAddBookingsCal.setVisibility(View.VISIBLE);
//                } else {
//                    mBinding.buttonCopyFromPrevious.setVisibility(View.GONE);
//                    mBinding.buttonAddBookingsCal.setVisibility(View.GONE);
//                }

                if(mCount == 0) {
                    mCount++;
                }
                break;


            case R.id.iv_previous:
                try {
                    mCount++;

                    // PREVIOUS WORKING CODE TO DISABLE PREVIOUS WEEK SELECTION
//                    int  dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
//                    if(dateStatus == Constants.DATE_STATUS.GREATER) {
//                        selectedWeekDays = getWeekDayPrev();
//
//                        String dateFormatedPrev = "";
//                        String validDate = getValidDate();
//                        if(!TextUtils.isEmpty(validDate)) {
//                            dateFormatedPrev = validDate;
//                        } else {
//                            mSelectedWeekDatesList.get(0).getSelectedDate() ;
//                        }
//                       // String dateFormatedPrev = mSelectedWeekDatesList.get(0).getSelectedDate();
//                        setDay(selectedWeekDays, dateFormatedPrev);
//                        setWeekRangeUi();
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(), mSelectedWeekDatesList.get(6).getSelectedDate(),true);
//
//                    }

                    // NEW CODE TO ENABLE PREVIOUS WEEK SELECTION

//                    try {
//                        int  dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
//                        if(dateStatus == Constants.DATE_STATUS.GREATER) {
//                            isFutureWeek = true;
//                        } else {
//                            isFutureWeek = false;
//                        }
//
//                        showToast("isFutureWeek : " + isFutureWeek);
//
//                        if(isFutureWeek) {
//                            mBinding.buttonCopyFromPrevious.setVisibility(View.VISIBLE);
//                        } else {
//                            mBinding.buttonCopyFromPrevious.setVisibility(View.GONE);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                    selectedWeekDays = getWeekDayPrev();
                    String dateFormattedPrevious = mSelectedWeekDatesList.get(0).getSelectedDate();
                    setDay(selectedWeekDays, dateFormattedPrevious);

                    startDateCalOff = dateFormattedPrevious;
                    endDateCalOff = dateFormattedPrevious;

                    //setDay(getWeekDayNext());
                    setWeekRangeUi();

                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);

//                    if(Utils.isSelectedDatePresentInWeek(selectedWeekDays, mTodayDate)) {
//                        mBinding.buttonCopyFromPrevious.setVisibility(View.VISIBLE);
//                        mBinding.buttonAddBookingsCal.setVisibility(View.VISIBLE);
//                    } else {
//                        mBinding.buttonCopyFromPrevious.setVisibility(View.GONE);
//                        mBinding.buttonAddBookingsCal.setVisibility(View.GONE);
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


            case R.id.tv_week_range:
                showDatePicker();
                break;

            case R.id.item_mon:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(0).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemMon.getRoot(), mBinding.itemMon.tvDate, mBinding.itemMon.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(), mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemMon.getRoot(), mBinding.itemMon.tvDate, mBinding.itemMon.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(), mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;
            case R.id.item_tue:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(1).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemTue.getRoot(), mBinding.itemTue.tvDate, mBinding.itemTue.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
//                                mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemTue.getRoot(), mBinding.itemTue.tvDate, mBinding.itemTue.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                            mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;
            case R.id.item_wed:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(2).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemWed.getRoot(),mBinding.itemWed.tvDate,mBinding.itemWed.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
//                                mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemWed.getRoot(),mBinding.itemWed.tvDate,mBinding.itemWed.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                            mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;
            case R.id.item_thu:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(3).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemThu.getRoot(),mBinding.itemThu.tvDate,mBinding.itemThu.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
//                                mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemThu.getRoot(),mBinding.itemThu.tvDate,mBinding.itemThu.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                            mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;
            case R.id.item_fri:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(4).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemFri.getRoot(),mBinding.itemFri.tvDate,mBinding.itemFri.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
//                                mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemFri.getRoot(),mBinding.itemFri.tvDate,mBinding.itemFri.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                            mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;
            case R.id.item_sat:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(5).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemSat.getRoot(),mBinding.itemSat.tvDate,mBinding.itemSat.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
//                                mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemSat.getRoot(),mBinding.itemSat.tvDate,mBinding.itemSat.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                            mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;
            case R.id.item_sun:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(6).getSelectedDate();

                    startDateCalOff = mSelectedDate;
                    endDateCalOff = mSelectedDate;

//                    if(Utils.compareDates(mTodayDate, mSelectedDate)) {
//                        setSelectedDateAvailability(mBinding.itemSun.getRoot(),mBinding.itemSun.tvDate,mBinding.itemSun.tvDay);
//                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
//                                mSelectedWeekDatesList.get(6).getSelectedDate(),false);
//                    } else {
//                        mSelectedDate = mTodayDate;
//                    }

                    setSelectedDateAvailability(mBinding.itemSun.getRoot(),mBinding.itemSun.tvDate,mBinding.itemSun.tvDay);
                    getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),
                            mSelectedWeekDatesList.get(6).getSelectedDate(),false);
                } catch (Exception e) {
                    LogUtils.LOGE(TAG,e.getMessage());
                }
                break;

            case R.id.button_add_bookings_cal:
                //setAvailablityCurrentSlot();
                if(Utils.isNetworkAvailable()) {

                    mTodayTime = Utils.getTodayTime();
                    mTodayDate = Utils.getTodayDate();
//                mNextDayDate = Utils.getNextDayDate();
//                mNextDayTime = Utils.getNextDayTime();
                    try {
                        int dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
                        if(dateStatus == Constants.DATE_STATUS.GREATER) {
                            long dateDiff = DateFormatUtils.getDateDifference(mTodayDate, mSelectedDate);
                            if(dateDiff > 1) {
                                //notifyAdapter(selectedSlotTime,mAdapter,availabilitySlotTime);
                                showSetAvailabilityDialog();
                            } else {
                                showSetAvailabilityDialog();
                            }
                        } else if ((dateStatus == Constants.DATE_STATUS.EQUAL)) {
                            //notifyAdapter(selectedSlotTime,mAdapter,availabilitySlotTime);
                            //showSnackbarFromTop(getString(R.string.slot_time_24_hour_limit));
                            showSetAvailabilityDialog();
                        } else {
                            if(Utils.isSelectedDatePresentInWeek(mSelectedWeekDatesList, mTodayDate)) {
                                showSetAvailabilityDialog();
                            } else {
                                showSnackbarFromTop(getString(R.string.set_slot_passed_week_msg));
                            }
                        }
                    } catch (ParseException e) {
                        LogUtils.LOGE(TAG,e.getMessage());
                    }

                    //showSetAvailabilityDialog();

                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.button_copy_from_previous:
                if(Utils.isNetworkAvailable()) {

                    mTodayTime = Utils.getTodayTime();
                    mTodayDate = Utils.getTodayDate();
//                mNextDayDate = Utils.getNextDayDate();
//                mNextDayTime = Utils.getNextDayTime();
                    try {
                        int dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
                        if(dateStatus == Constants.DATE_STATUS.GREATER) {
                            long dateDiff = DateFormatUtils.getDateDifference(mTodayDate, mSelectedDate);
                            if(dateDiff > 1) {
                                //notifyAdapter(selectedSlotTime,mAdapter,availabilitySlotTime);
                                copyPreviousWeekAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);
                            } else {
                                copyPreviousWeekAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);
                            }
                        } else if ((dateStatus == Constants.DATE_STATUS.EQUAL)) {
                            //notifyAdapter(selectedSlotTime,mAdapter,availabilitySlotTime);
                            //showSnackbarFromTop(getString(R.string.slot_time_24_hour_limit));
                            copyPreviousWeekAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);
                        } else {
                            if(Utils.isSelectedDatePresentInWeek(mSelectedWeekDatesList, mTodayDate)) {
                                copyPreviousWeekAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);
                            } else {
                                showSnackbarFromTop(getString(R.string.copy_slot_passed_week_msg));
                            }
                        }
                    } catch (ParseException e) {
                        LogUtils.LOGE(TAG,e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //copyPreviousWeekAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(),mSelectedWeekDatesList.get(6).getSelectedDate(),true);
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private void showSetAvailabilityDialog() {
        Alert.showAlertDialogWithCancel(
                getActivity(),
                getResources().getString(R.string.set_availability_alert_title),
                getResources().getString(R.string.set_availability_alert_msg),
                getResources().getString(R.string.no),
                getResources().getString(R.string.yes),
                false, "",
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if (Utils.isNetworkAvailable()) {
                            setAvailablityCurrentSlot();
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });
    }

    private String getValidDate() {
        String mDay="";
        mTodayDate = Utils.getTodayDate();
//        mNextDayDate = Utils.getNextDayDate();
        if(mSelectedWeekDatesList!=null && mSelectedWeekDatesList.size() > 0) {
            for(SelectedDates selectedDate:mSelectedWeekDatesList){
                if(selectedDate.getSelectedDate().equalsIgnoreCase(mTodayDate)){
                    mDay = mTodayDate;
                    break;
                }
            }
        }
        return mDay;
    }

    private void setSelectedDateAvailability(View view, TextView tvSelectedDay, TextView tvSelectedDate) {
        Typeface fontfaceAnr = Typeface.createFromAsset(getActivity().getAssets(),
                getActivity().getString(R.string.font_anr));
        Typeface fontfaceMedium = Typeface.createFromAsset(getActivity().getAssets(),
                getActivity().getString(R.string.font_avenir_medium));

        mBinding.itemMon.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemMon.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemMon.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemMon.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemMon.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemTue.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemTue.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemTue.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemTue.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemTue.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemWed.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemWed.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemWed.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemWed.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemWed.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemThu.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemThu.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemThu.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemThu.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemThu.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemFri.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemFri.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemFri.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemFri.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemFri.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemSat.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemSat.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSat.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSat.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemSat.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemSun.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemSun.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSun.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSun.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemSun.tvDay.setTypeface(fontfaceAnr);

        view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.app_btn_green));
        tvSelectedDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        tvSelectedDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

        tvSelectedDate.setTypeface(fontfaceMedium);
        tvSelectedDay.setTypeface(fontfaceMedium);

    }

    private void setWeekRangeUi() {
        try {
            String currentDayStartStr = mCurrentDayStart;
            int currentDayStartInt = Integer.parseInt(currentDayStartStr);
            String daySuffixStart = "th";
            daySuffixStart = Utils.getDateOfTheMonthSuffix(currentDayStartInt);
            daySuffixStart = daySuffixStart.toLowerCase();

            String currentDayEndStr = mCurrentDayEnd;
            int currentDayEndInt = Integer.parseInt(currentDayEndStr);
            String daySuffixEnd = "th";
            daySuffixEnd = Utils.getDateOfTheMonthSuffix(currentDayEndInt);
            daySuffixEnd = daySuffixEnd.toLowerCase();

            mBinding.tvWeekRange.setText(mCurrentDayStart + daySuffixStart + " " + Utils.getMonthInString(Integer.parseInt(mCurrentMonthStart)) + " - " + mCurrentDayEnd + daySuffixEnd + " " + Utils.getMonthInString(Integer.parseInt(mCurrentMonthEnd)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String[] getWeekDay(Calendar now) {
        mSelectedWeekDatesList = new ArrayList<>();
        // Calendar now = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        String[] days = new String[7];
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 1;

        now.add(Calendar.DAY_OF_MONTH, delta);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        for (int i = 0; i < 7; i++) {
            days[i] = format.format(now.getTime());
            Log.i(TAG, "getWeekDay>>" + now.getTime());
            String date = format.format(now.getTime());

            if (date != null) {
                String dateArr[] = date.split("/");
                if (i == 0) {
                    mCurrentMonthStart = dateArr[1];
                    mCurrentDayStart = dateArr[0];
                    mCurrentYearStart = dateArr[2];

                }
                if (i == 6) {
                    mCurrentMonthEnd = dateArr[1];
                    mCurrentDayEnd = dateArr[0];
                    mCurrentYearEnd = dateArr[2];

                }

                days[i] = dateArr[0];
                SelectedDates selectedDate = new SelectedDates();
                selectedDate.setSelectedDate(dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0]);
                selectedDate.setSelectedDays(dateArr[0]);
                mSelectedWeekDatesList.add(selectedDate);

                now.add(Calendar.DAY_OF_MONTH, 1);
            }

        }
        mCalender = now;

        return days;

    }


    public String[] getWeekDayPrev() {
        mSelectedWeekDatesList = new ArrayList<>();
        weekDaysCount--;
        //Calendar now1 = Calendar.getInstance();
        Calendar now1 = mCalender;
        Calendar now = (Calendar) now1.clone();

        // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

        String[] days = new String[7];
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 1;
        now.add(Calendar.WEEK_OF_YEAR, weekDaysCount);
        now.add(Calendar.DAY_OF_MONTH, delta);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        for (int i = 0; i < 7; i++) {
            String date = format.format(now.getTime());
            if (date != null) {
                String dateArr[] = date.split("/");
                if (i == 0) {
                    mCurrentMonthStart = dateArr[1];
                    mCurrentDayStart = dateArr[0];
                    mCurrentYearStart = dateArr[2];

                }
                if (i == 6) {
                    mCurrentMonthEnd = dateArr[1];
                    mCurrentDayEnd = dateArr[0];
                    mCurrentYearEnd = dateArr[2];

                }

                days[i] = dateArr[0];

                SelectedDates selectedDate = new SelectedDates();
                selectedDate.setSelectedDate(dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0]);
                selectedDate.setSelectedDays(dateArr[0]);
                mSelectedWeekDatesList.add(selectedDate);

                now.add(Calendar.DAY_OF_MONTH, 1);
            }

        }

        return days;

    }


    public String[] getWeekDayNext() {
        mSelectedWeekDatesList = new ArrayList<>();
        if (mCount != 0) {
            weekDaysCount++;
        }
        //Calendar now1 = Calendar.getInstance();
        Calendar now1 = mCalender;

        Calendar now = (Calendar) now1.clone();

        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

        String[] days = new String[7];
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 1;
        now.add(Calendar.WEEK_OF_YEAR, weekDaysCount);
        now.add(Calendar.DAY_OF_MONTH, delta);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        for (int i = 0; i < 7; i++) {
            //days [i] = format.format(now.getTime());
            String date = format.format(now.getTime());
            if (date != null) {
                String dateArr[] = date.split("/");
                if (i == 0) {
                    mCurrentMonthStart = dateArr[1];
                    mCurrentDayStart = dateArr[0];
                    mCurrentYearStart = dateArr[2];

                }
                if (i == 6) {
                    mCurrentMonthEnd = dateArr[1];
                    mCurrentDayEnd = dateArr[0];
                    mCurrentYearEnd = dateArr[2];

                }

                days[i] = dateArr[0];

                SelectedDates selectedDate = new SelectedDates();
                selectedDate.setSelectedDate(dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0]);
                selectedDate.setSelectedDays(dateArr[0]);
                mSelectedWeekDatesList.add(selectedDate);

                now.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        return days;

    }


    private void showDatePicker() {
        final Calendar mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(getActivity(), new
                DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int
                            day) {

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormated = yyyymmddFormat.format(mCalendar.getTime());

                        startDateCalOff = dateFormated;
                        endDateCalOff = dateFormated;

                        mCalender = mCalendar;
                        try {

                            if (date != null) {
                               /* mDOB = date;
                                mDobFormatted=dateFormated;
                                mBinder.etAge.setText(mDOB);*/
                                mCount++;
                                weekDaysCount = -1;

                                setDay(getWeekDay(mCalendar), dateFormated);
                                setWeekRangeUi();
                                getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(), mSelectedWeekDatesList.get(6).getSelectedDate(), true);

                            }


                        } catch (Exception e) {
                            LOGE(TAG, "Age>" + e.getMessage());

                        }

                    }
                }, year, month,
                day);

        //Calendar minCal = Calendar.getInstance();
        //  minCal.set(Calendar.DATE, minCal.getFirstDayOfWeek());

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        datePickerDialog.show();
    }

    private void logServerList(ArrayList<SlotTime> slotTimeArrayList) {
        for(int i = 0; i < slotTimeArrayList.size(); i++) {
            SlotTime slotTime = slotTimeArrayList.get(i);
            Log.d("slot_log", "logServerList - "
                    + " slotTime.getId : " +  slotTime.getId()
                    + " slotTime.getSlotTime : " + slotTime.getSlotTime()
                    + " slotTime.getStatus : " + slotTime.getStatus()
                    + " slotTime.getZone : " + slotTime.getZone()
            );
        }
    }

    private void setDefaultTimeSlots(String date) {

        try {

            ArrayList<SlotTime> mDefaultSlotTimes = Utils.getDefaultCoachAvailabilityTimeSlot();
            final AvailabilityResult availabilitySlotTime = new AvailabilityResult();
            availabilitySlotTime.setSlotDate(date);

            for (int i = 0; i < mAvailabiltyResults.size(); i++) {
                ArrayList<SlotTime> slotTimeArrayList = mAvailabiltyResults.get(i).getSlotTime();

                if (date.equalsIgnoreCase(mAvailabiltyResults.get(i).getSlotDate())) {
                    logServerList(slotTimeArrayList);

                    // Below code is used earlier for displaying available and unavailable slots.

//                for (int j = 0; j < slotTimeArrayList.size(); j++) {
//                    for (int k = 0; k < mDefaultSlotTimes.size(); k++) {
//                        if (slotTimeArrayList.get(j).getSlotTime() == mDefaultSlotTimes.get(k).getSlotTime()) {
//                            Log.d("slot_log", "Set Availability slot - "
//                                    + " slotTimeArrayList.get(j).getSlotTime() : " + slotTimeArrayList.get(j).getSlotTime()
//                                    + " mDefaultSlotTimes.get(k).getSlotTime() : " + mDefaultSlotTimes.get(k).getSlotTime()
//                                    + " slotTimeArrayList.get(j).getStatus() : " + slotTimeArrayList.get(j).getStatus()
//                                    + " mDefaultSlotTimes.get(k).getStatus() : " + mDefaultSlotTimes.get(k).getStatus()
//                            );
//                            mDefaultSlotTimes.get(k).setId(slotTimeArrayList.get(j).getId());
//                            mDefaultSlotTimes.get(k).setStatus(slotTimeArrayList.get(j).getStatus());
//                            mDefaultSlotTimes.get(k).setSlotTime(slotTimeArrayList.get(j).getSlotTime());
//                        }
//                    }
//                }


                    // Below is the new code used to show unavailable and available slots.

                    ArrayList<Integer> unavailableSlotPositionList = new ArrayList<>();

                    for (SlotTime localSlotTimeObj : mDefaultSlotTimes) {
                        boolean isExist = false;
                        for (SlotTime serverSlotTimeObj : slotTimeArrayList) {
                            if (localSlotTimeObj.getSlotTime() == serverSlotTimeObj.getSlotTime()) {
                                isExist = true;
                            }
                            if(isExist) {
                                localSlotTimeObj.setId(serverSlotTimeObj.getId());
                                localSlotTimeObj.setStatus(serverSlotTimeObj.getStatus());
                                localSlotTimeObj.setSlotTime(serverSlotTimeObj.getSlotTime());

                                unavailableSlotPositionList.add(serverSlotTimeObj.getSlotTime());
                                break;
                            } else {

//                                if(localSlotTimeObj.getSlotTime() >= 0 && localSlotTimeObj.getSlotTime() <= 7) {
//                                    localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
//                                } else if(localSlotTimeObj.getSlotTime() >= 8 && localSlotTimeObj.getSlotTime() <= 20) {
//                                    localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                                } else if(localSlotTimeObj.getSlotTime() >= 21 && localSlotTimeObj.getSlotTime() <= 23) {
//                                    localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
//                                }

//                            if(localSlotTimeObj.getSlotTime() >= 8 && localSlotTimeObj.getSlotTime() <= 20) {
//                                localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                            } else {
//                                localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
//                            }

                                boolean serverExists = false;
                                for (SlotTime serverSlotTimeObjLocal : slotTimeArrayList) {
                                    if(serverSlotTimeObjLocal.getStatus() == 0) {
                                        serverExists = true;
                                        break;
                                    }
                                }

                                if(serverExists) {
                                    localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
                                } else {
                                    if(localSlotTimeObj.getSlotTime() >= 8 && localSlotTimeObj.getSlotTime() <= 20) {
                                        localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
                                    } else {
                                        localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
                                    }
                                }

                            }
                        }
                    }

//                    ArrayList<Integer> availableSlotPositionList = new ArrayList<>();
//                    for(int kl = 0; kl < 24; kl++) {
//                        if(!unavailableSlotPositionList.contains(kl)) {
//                            availableSlotPositionList.add(kl);
//                        }
//                    }
//
//                    for(int slotPosition : availableSlotPositionList) {
//                        //Log.d("slot_log", " availableSlotPositionList - available position : " + slotPosition);
//                        if(slotPosition >= 0 && slotPosition <= 7) {
//                            mDefaultSlotTimes.get(slotPosition).setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                        } else if(slotPosition >= 21 && slotPosition <= 23) {
//                            mDefaultSlotTimes.get(slotPosition).setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                        }
//                    }

                }
            }

            availabilitySlotTime.setSlotTime(mDefaultSlotTimes);
            mCurrentSlotTimes = mDefaultSlotTimes;
            final AvailabilityTimeAdapter mAdapter = new AvailabilityTimeAdapter(getActivity(), availabilitySlotTime);
            mBinding.gridviewTime.setAdapter(mAdapter);

            mBinding.gridviewTime.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                    SlotTime selectedSlotTime = availabilitySlotTime.getSlotTime().get(pos);
                    mTodayTime = Utils.getTodayTime();
                    mTodayDate = Utils.getTodayDate();
//                mNextDayDate = Utils.getNextDayDate();
//                mNextDayTime = Utils.getNextDayTime();
                    try {
                        int dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
                        if (dateStatus == Constants.DATE_STATUS.GREATER) {
                            long dateDiff = DateFormatUtils.getDateDifference(mTodayDate, mSelectedDate);
                            if (dateDiff > 1) {
                                notifyAdapter(selectedSlotTime, mAdapter, availabilitySlotTime);
                            } else {
                                if (selectedSlotTime.getSlotTime() <= mTodayTime) {
                                    showSnackbarFromTop(getString(R.string.slot_time_24_hour_limit));
                                } else {
                                    notifyAdapter(selectedSlotTime, mAdapter, availabilitySlotTime);
                                }
                            }
                        } else if ((dateStatus == Constants.DATE_STATUS.EQUAL
                                && mTodayTime < selectedSlotTime.getSlotTime())) {
                            //notifyAdapter(selectedSlotTime,mAdapter,availabilitySlotTime);
                            showSnackbarFromTop(getString(R.string.slot_time_24_hour_limit));
                        } else {
                            showSnackbarFromTop(getString(R.string.slot_passed));
                        }
                    } catch (ParseException e) {
                        LogUtils.LOGE(TAG, e.getMessage());
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void notifyAdapter(SlotTime selectedSlotTime, AvailabilityTimeAdapter mAdapter, AvailabilityResult availabilitySlotTime) {
        if (selectedSlotTime.getStatus() != Constants.TIME_SLOT_BOOK_STATUS.SLOT_BOOKED_STATUS) {
            if (selectedSlotTime.getStatus() == Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS) {
                selectedSlotTime.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
            } else {
                selectedSlotTime.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
            }
        }
        mAdapter.notifyDataSetChanged();
        mCurrentSlotTimes = availabilitySlotTime.getSlotTime();
    }

    private void getAvailability(final String startDate, String endDate, boolean isShowLoader) {
        if (isShowLoader) {
            if (mActivity != null && !mActivity.get().isFromNotificationPanel) {
                processToShowDialog();
            }
        }

        GetAvailabilityRequest request = new GetAvailabilityRequest();
        request.setSlotDateStart(startDate);
        request.setSlotDateEnd(endDate);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getAvailability(request).enqueue(new BaseCallback<GetCoachAvailabilityResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(GetCoachAvailabilityResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null) {
                        mAvailabiltyResults = response.getResult();
                        setDefaultTimeSlots(mSelectedDate);
                    }
                } else {
                    try {
                        if (response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetCoachAvailabilityResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void copyPreviousWeekAvailability(final String startDate, String endDate, boolean isShowLoader) {
        if (isShowLoader) {
            if (mActivity != null && !mActivity.get().isFromNotificationPanel) {
                processToShowDialog();
            }
        }

        GetAvailabilityRequest request = new GetAvailabilityRequest();
        request.setSlotDateStart(startDate);
        request.setSlotDateEnd(endDate);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.setPreviousWeekAvailablity(request).enqueue(new BaseCallback<GetCoachAvailabilityResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(GetCoachAvailabilityResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null) {
                        mAvailabiltyResults = response.getResult();
                        setDefaultTimeSlots(mSelectedDate);
                    }
                } else {
                    try {
                        if (response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetCoachAvailabilityResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void setAvailability(String date, ArrayList<Integer> slotHours) {
        processToShowDialog();

        SetAvailabilityRequest request = new SetAvailabilityRequest();
        request.setSlotDate(date);
        request.setSlotHour(slotHours);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.setAvailability(request).enqueue(new BaseCallback<BaseResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && (response.getStatus() == 1)) {
                    try {
                        if (response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response != null) {
                        try {
                            if (response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setAvailablityCurrentSlot() {
        if (mCurrentSlotTimes != null) {
            mSelectedAvailabilityHours = new ArrayList<>();
            for (SlotTime slotTime : mCurrentSlotTimes) {
                if (slotTime.getStatus() == Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS) {
                    mSelectedAvailabilityHours.add(slotTime.getSlotTime());
                }
            }
            if (!StringUtils.isNullOrEmpty(mSelectedDate)) {
                setAvailability(mSelectedDate, mSelectedAvailabilityHours);
            }
        }
    }

}

