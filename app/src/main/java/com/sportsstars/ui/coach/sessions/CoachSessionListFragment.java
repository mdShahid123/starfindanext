/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.coach.sessions;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentCoachSessionListBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.UserRoleRequest;
import com.sportsstars.network.response.CalendarSyncBaseResponse;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.response.auth.UserRoleResponse;
import com.sportsstars.network.response.coachsession.CoachSession;
import com.sportsstars.network.response.coachsession.CoachSessionListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.adapter.CoachSessionAdapter;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.session.SessionCoachDetails;
import com.sportsstars.util.Constants;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

public class CoachSessionListFragment extends BaseFragment implements ListItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    public static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 112;

    private FragmentCoachSessionListBinding mBinding;
    private CoachSessionAdapter mCoachSessionAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<CoachSession> sessionList = new ArrayList<>();
    private int pageNo = 1;
    private int perPageRecords;
    private int listCount;
    private boolean isListLoading;
    private WeakReference<HomeActivity> mActivity;

    public static CoachSessionListFragment newInstance() {
        return new CoachSessionListFragment();
    }

    @Override
    public String getFragmentName() {
        return CoachSessionListFragment.class.getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((HomeActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coach_session_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        //refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(HomeActivity.selectedTabPos == HomeActivity.Home_FRAGMENT_POS) {
            if(sessionList != null && sessionList.size() > 0) {
                sessionList.clear();
            }
            refreshList();
        }
    }

    private void initViews() {
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mCoachSessionAdapter = new CoachSessionAdapter(getActivity(), sessionList, this);
        mBinding.rvSessions.setAdapter(mCoachSessionAdapter);
        mBinding.rvSessions.setLayoutManager(mLayoutManager);
        mBinding.swipeRefreshLayout.setOnRefreshListener(this);
        mBinding.rvSessions.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                    return;
                }
                checkIfItsLastPage();
            }
        });
    }

    private void checkIfItsLastPage() {
        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
        if (!isListLoading) {
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                if (shouldCallApi()) {
                    ++pageNo;
                    getSessionsList(pageNo);
                }
            }
        }
    }

    private boolean shouldCallApi() {
        return this.pageNo * perPageRecords <= listCount;
    }

    private void getSessionsList(int pageNo) {
        if (!mBinding.swipeRefreshLayout.isRefreshing()
                || mActivity.get().isFromNotificationPanel) {
            processToShowDialog();
        }
        isListLoading = true;
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachSessionList(pageNo)
                .enqueue(new BaseCallback<CoachSessionListResponse>(getBaseActivity()) {

                    @Override
                    public void onSuccess(CoachSessionListResponse response) {
                        try {
                            if (response != null && response.getStatus() == 1) {
                                if(response.getResult() != null && response.getResult().getData() != null) {
                                    ArrayList<CoachSession> localSessionList = response.getResult().getData();
                                    if (localSessionList != null && !localSessionList.isEmpty()) {
                                        showEmptyListError(false, false);
                                        if (mBinding.swipeRefreshLayout.isRefreshing()) {
                                            if (sessionList != null && !sessionList.isEmpty()) {
                                                sessionList.clear();
                                            }
                                        }

                                        listCount = response.getResult().getTotal();
                                        perPageRecords = response.getResult().getPerPage();
                                        if(sessionList!=null) {
                                            sessionList.addAll(localSessionList);
                                        }
                                        mCoachSessionAdapter.notifyDataSetChanged();
                                        deleteCancelledEventsFromCalendar(sessionList);
                                    } else {
                                        if (sessionList != null && !sessionList.isEmpty()) {
                                            return;
                                        }
                                        showEmptyListError(true, false);
                                    }
                                } else {
                                    if (sessionList != null && !sessionList.isEmpty()) {
                                        return;
                                    }
                                    showEmptyListError(true, false);
                                }
                            }
                            mBinding.swipeRefreshLayout.setRefreshing(false);
                            isListLoading = false;

                            refreshEventsEntryToCalendar();

                            if(mActivity != null) {
                                if(mActivity.get().notificationTypeId
                                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED
                                        && mActivity.get().notificationPayloadSessionId == 0) {
                                    if(Utils.isNetworkAvailable() && mActivity != null
                                            && mActivity.get().isFromNotificationPanel) {
                                        userRoleApi();
                                    }
                                } else if(mActivity.get().notificationTypeId
                                        != Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED
                                        && mActivity.get().notificationPayloadSessionId != 0) {
                                    launchSessionDetailsFromNotificationPanel();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(Call<CoachSessionListResponse> call, BaseResponse baseResponse) {
                        mBinding.swipeRefreshLayout.setRefreshing(false);
                        isListLoading = false;
                        if (sessionList != null && sessionList.isEmpty()) {
                            showEmptyListError(true, false);
                            try {
                                if (baseResponse != null && baseResponse.getMessage() != null
                                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                    showSnackbarFromTop(baseResponse.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        try {
                            getBaseActivity().hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void userRoleApi() {

        int mUserRole = Constants.USER_ROLE.COACH;
        PreferenceUtil.setCurrentUserRole(mUserRole);

        UserRoleRequest userRoleRequest = new UserRoleRequest();
        userRoleRequest.setUserLoggedInType(mUserRole);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.userRole(userRoleRequest).enqueue(new BaseCallback<UserRoleResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(UserRoleResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        Log.d("mylog", "User role updated on server.");
                    } else {
                        Log.d("mylog", "User role not updated on server.");
                    }
                }
                if(mActivity != null && mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED) {
                    mActivity.get().isFromNotificationPanel = false;
                }
            }

            @Override
            public void onFail(Call<UserRoleResponse> call, BaseResponse baseResponse) {
                if(mActivity != null && mActivity.get().notificationTypeId
                        == Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED) {
                    mActivity.get().isFromNotificationPanel = false;
                }
                try {
                    if(mActivity != null) {
                        mActivity.get().hideProgressBar();
                    }
                    if (baseResponse != null
                            && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        Log.d("mylog", "User role not updated to coach. Error - " + baseResponse.getMessage());
                        //showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void launchSessionDetailsFromNotificationPanel() {
        if(Utils.isNetworkAvailable()) {
            if(mActivity != null) {
                if(mActivity.get().isFromNotificationPanel && mActivity.get().notificationPayloadSessionId != 0) {
                    Intent i = new Intent(getActivity(), SessionCoachDetails.class);
                    i.putExtra(getResources().getString(R.string.booking_id_extra), mActivity.get().notificationPayloadSessionId);
                    startActivity(i);
                    mActivity.get().isFromNotificationPanel = false;
                    mActivity.get().notificationPayloadSessionId = 0;
                } else {
//                    mActivity.get().isFromNotificationPanel = false;
//                    mActivity.get().notificationPayloadSessionId = 0;
                }
            }
        } else {
            if(mActivity != null) {
                mActivity.get().isFromNotificationPanel = false;
                mActivity.get().notificationPayloadSessionId = 0;
            }
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void deleteCancelledEventsFromCalendar(ArrayList<CoachSession> sessionList) {
        if(sessionList != null && sessionList.size() > 0) {
            for(CoachSession coachSession : sessionList) {
                if(coachSession != null && coachSession.getStatus() != 0) {
                    if(coachSession.getStatus() == Utils.STATUS_BOOKING.CANCELLED) {
                        CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
                        calendarSyncResponse.setTitle("StarFinda Coaching Session with " + coachSession.getName() + " for " + coachSession.getSportsName());
                        calendarSyncResponse.setSportName(coachSession.getSportsName());
                        calendarSyncResponse.setSessionDate(coachSession.getSessionDate());
                        calendarSyncResponse.setLearnerName(coachSession.getName());
                        calendarSyncResponse.setFacility(coachSession.getFacilityAddress());
                        calendarSyncResponse.setCoachName("");
                        CalendarSyncHelper.getInstance().deleteCalendarEvent(mActivity.get(), calendarSyncResponse);
                    }
                }
            }
        }
    }

    @Override
    public void onListItemClicked(int position) {
        if(Utils.isNetworkAvailable()) {
            if (sessionList != null && !sessionList.isEmpty()) {
                CoachSession coachSession = sessionList.get(position);
                Intent i = new Intent(getActivity(), SessionCoachDetails.class);
                i.putExtra(getString(R.string.booking_id_extra), coachSession.getSessionId());
                startActivity(i);
            }
        } else {
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void showEmptyListError(boolean showError, boolean showInternetError) {
        try {
            if (showError) {
                mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                mBinding.tvNoSessions.setVisibility(View.GONE);
                mBinding.rvSessions.setVisibility(View.GONE);
                if (showInternetError) {
                    mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setVisibility(View.VISIBLE);
                    mBinding.tvNoSessions.setText(getResources().getString(R.string.no_internet));
                } else {
                    mBinding.tvNoSessions.setVisibility(View.GONE);
                    mBinding.tvNoSessions.setText(getResources().getString(R.string.no_sessions_msg));
                    mBinding.rlNoSessionsParent.setVisibility(View.VISIBLE);
                    mBinding.btnNoSessions.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(mActivity != null) {
                                mActivity.get().bottomBar.setCurrentItem(HomeActivity.SET_AVAILABILITY_FRAGMENT_POS);
                            }
                        }
                    });
                }
            } else {
                mBinding.tvNoSessions.setVisibility(View.GONE);
                mBinding.rlNoSessionsParent.setVisibility(View.GONE);
                mBinding.rvSessions.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList() {
        if (!Utils.isNetworkAvailable()) {
            if (mBinding.swipeRefreshLayout.isRefreshing()) {
                mBinding.swipeRefreshLayout.setRefreshing(false);
            }
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
            showEmptyListError(true, true);
            return;
        }
        pageNo = 1;
        isListLoading = false;
        listCount = 0;
        perPageRecords = 0;
//        if(sessionList != null && !sessionList.isEmpty()) {
//            sessionList.clear();
//        }
        if (Utils.isNetworkAvailable()) {
            getSessionsList(pageNo);
        } else {
            showEmptyListError(true, true);
        }
    }

    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(Utils.isNetworkAvailable()) {
                        getBookingEventListToCalendar();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.permission_not_granted_for_calendar), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void refreshEventsEntryToCalendar() {
        try {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR)
                    != PackageManager.PERMISSION_GRANTED) {
                askCalendarPermission();
            } else {
                if(Utils.isNetworkAvailable()) {
                    getBookingEventListToCalendar();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void getBookingEventListToCalendar() {
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCalendarSyncData().enqueue(new BaseCallback<CalendarSyncBaseResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(CalendarSyncBaseResponse response) {
                if(response != null && response.getStatus() == 1) {
                    if(response.getResult() != null && response.getResult().size() > 0) {
                        //CalendarSyncHelper.getInstance().deleteAllCalendarEvents(getActivity());
                        CalendarSyncHelper.getInstance().updateCalendarEventList(getActivity(), response.getResult());
                    }
                }
            }

            @Override
            public void onFail(Call<CalendarSyncBaseResponse> call, BaseResponse baseResponse) {
                try {
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
