/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.coach.sessions;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentCoachTipsListBinding;
import com.sportsstars.interfaces.CoachTipsItemClickListener;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.coachtips.DeleteTipRequest;
import com.sportsstars.network.response.coachtips.CoachTips;
import com.sportsstars.network.response.coachtips.CoachTipsListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.adapter.CoachTipsAdapter;
import com.sportsstars.ui.coach.tips.AddTipActivity;
import com.sportsstars.ui.coach.tips.ViewTipActivity;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;

import retrofit2.Call;

public class CoachTipsListFragment extends BaseFragment implements CoachTipsItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private FragmentCoachTipsListBinding mBinding;
    private CoachTipsAdapter mCoachTipsAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<CoachTips> tipsList = new ArrayList<>();
    private int pageNo = 1;
    private int perPageRecords;
    private int listCount;
    private boolean isListLoading;

    public static CoachTipsListFragment newInstance() {
        return new CoachTipsListFragment();
    }

    @Override
    public String getFragmentName() {
        return CoachTipsListFragment.class.getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coach_tips_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        //refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (HomeActivity.selectedTabPos == HomeActivity.Home_FRAGMENT_POS) {
            if (tipsList != null && tipsList.size() > 0) {
                tipsList.clear();
            }
            refreshList();
        }
    }

    private void initViews() {
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mCoachTipsAdapter = new CoachTipsAdapter(getActivity(), tipsList, this);
        mBinding.rvTips.setAdapter(mCoachTipsAdapter);
        mBinding.rvTips.setLayoutManager(mLayoutManager);
        mBinding.swipeRefreshLayout.setOnRefreshListener(this);
        mBinding.rvTips.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                    return;
                }
                checkIfItsLastPage();
            }
        });
        mBinding.tvCreateTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.getInstance().navigateToActivityForResult(getActivity(),
                        AddTipActivity.class, Constants.REQUEST_CODE.REQUEST_CODE_ADD_TIP);
            }
        });
    }

    private void checkIfItsLastPage() {
        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
        if (!isListLoading) {
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                if (shouldCallApi()) {
                    ++pageNo;
                    getCoachTipsList(pageNo);
                }
            }
        }
    }

    private boolean shouldCallApi() {
        return this.pageNo * perPageRecords <= listCount;
    }

    private void getCoachTipsList(int pageNo) {
        if (!mBinding.swipeRefreshLayout.isRefreshing()) {
            processToShowDialog();
        }
        isListLoading = true;
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachTipsList(pageNo)
                .enqueue(new BaseCallback<CoachTipsListResponse>(getBaseActivity()) {

                    @Override
                    public void onSuccess(CoachTipsListResponse response) {
                        if (response != null && response.getStatus() == 1) {
                            if (response.getResult() != null && response.getResult().getData() != null) {
                                ArrayList<CoachTips> localTipsList = response.getResult().getData();
                                if (localTipsList != null && !localTipsList.isEmpty()) {
                                    showEmptyListError(false, false);
                                    if (mBinding.swipeRefreshLayout.isRefreshing()) {
                                        if (tipsList != null && !tipsList.isEmpty()) {
                                            tipsList.clear();
                                        }
                                    }

                                    listCount = response.getResult().getTotal();
                                    perPageRecords = response.getResult().getPerPage();
                                    if (tipsList != null)
                                        tipsList.addAll(localTipsList);
                                    mCoachTipsAdapter.notifyDataSetChanged();
                                } else {
                                    if (tipsList != null && !tipsList.isEmpty()) {
                                        return;
                                    }
                                    showEmptyListError(true, false);
                                }
                            } else {
                                if (tipsList != null && !tipsList.isEmpty()) {
                                    return;
                                }
                                showEmptyListError(true, false);
                            }
                        }

                        mBinding.swipeRefreshLayout.setRefreshing(false);
                        isListLoading = false;
                    }

                    @Override
                    public void onFail(Call<CoachTipsListResponse> call, BaseResponse baseResponse) {
                        mBinding.swipeRefreshLayout.setRefreshing(false);
                        isListLoading = false;
                        if (tipsList != null && tipsList.isEmpty()) {
                            showEmptyListError(true, false);
                            try {
                                if (baseResponse != null && baseResponse.getMessage() != null
                                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                    showSnackbarFromTop(baseResponse.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        try {
                            getBaseActivity().hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void showEmptyListError(boolean showError, boolean showInternetError) {
        if (showError) {
            mBinding.tvNoTips.setVisibility(View.VISIBLE);
            mBinding.rvTips.setVisibility(View.GONE);
            if (showInternetError) {
                mBinding.tvNoTips.setText(getResources().getString(R.string.no_internet));
            } else {
                mBinding.tvNoTips.setText(getResources().getString(R.string.no_tips_msg));
            }
        } else {
            mBinding.rvTips.setVisibility(View.VISIBLE);
            mBinding.tvNoTips.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onCoachTipsItemClick(int position) {
        try {
            if (tipsList != null && !tipsList.isEmpty()) {
                CoachTips coachTips = tipsList.get(position);
//            showToast("onListItemClicked - tip name : " + coachTips.getTitle()
//                    + " tip image url : " + coachTips.getImageUrl()
//            + " tip video url : " + coachTips.getVideoUrl());
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.BundleKey.COACH_TIPS, coachTips);
                Navigator.getInstance().navigateToActivityWithData(getActivity(), ViewTipActivity.class, bundle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCoachTipsItemDelete(int position) {
        try {
            if (tipsList != null && !tipsList.isEmpty()) {
                CoachTips coachTips = tipsList.get(position);
                if (coachTips != null) {
                    int tipId = coachTips.getId();
//                showToast("onCoachTipsItemDelete - tip name : " + coachTips.getTitle()
//                    + " tip id : " + tipId);
                    if (Utils.isNetworkAvailable()) {
                        deleteSelectedTip(new DeleteTipRequest(tipId), position);
                    } else {
                        showSnackbarFromTop(getResources().getString(R.string.no_internet));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void deleteSelectedTip(DeleteTipRequest deleteTipRequest, int position) {
        if (!mBinding.swipeRefreshLayout.isRefreshing()) {
            processToShowDialog();
        }
        isListLoading = true;
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.deleteTip(deleteTipRequest)
                .enqueue(new BaseCallback<BaseResponse>(getBaseActivity()) {

                    @Override
                    public void onSuccess(BaseResponse response) {
                        if (response != null) {
                            if (response.getStatus() == 1) {
                                try {
                                    if (response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                        showSnackbarFromTop(response.getMessage());
                                    }
                                    tipsList.remove(position);
                                    if (mCoachTipsAdapter != null) {
                                        mCoachTipsAdapter.updateList(tipsList);
                                        mCoachTipsAdapter.notifyDataSetChanged();

                                        //mCoachTipsAdapter.notifyItemRemoved(position);
                                    }
                                    if (tipsList != null && tipsList.size() == 0) {
                                        showEmptyListError(true, false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            mBinding.swipeRefreshLayout.setRefreshing(false);
                            isListLoading = false;
                        }
                    }

                    @Override
                    public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                        mBinding.swipeRefreshLayout.setRefreshing(false);
                        isListLoading = false;
                        try {
                            if (baseResponse != null && baseResponse.getMessage() != null
                                    && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                showSnackbarFromTop(baseResponse.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            getBaseActivity().hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void refreshList() {
        if (!Utils.isNetworkAvailable()) {
            if (mBinding.swipeRefreshLayout.isRefreshing()) {
                mBinding.swipeRefreshLayout.setRefreshing(false);
            }
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
            showEmptyListError(true, true);
            return;
        }
        pageNo = 1;
        isListLoading = false;
        listCount = 0;
        perPageRecords = 0;
//        if(tipsList != null && !tipsList.isEmpty()) {
//            tipsList.clear();
//        }
        if (Utils.isNetworkAvailable()) {
            getCoachTipsList(pageNo);
        } else {
            showEmptyListError(true, true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_ADD_TIP) {
            refreshList();
        }
    }

}
