package com.sportsstars.ui.coach.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemAddAdditionalMediaBinding;
import com.sportsstars.databinding.ItemAdditionalMediaBinding;
import com.sportsstars.model.AdditionalMedia;
import com.sportsstars.ui.coach.adapter.viewholder.AdditionalMediaFooterViewHolder;
import com.sportsstars.ui.coach.adapter.viewholder.AdditionalMediaViewHolder;
import com.sportsstars.util.Constants;

import java.util.List;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AdditionalMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdditionalMediaFooterViewHolder.AddMediaCallback, AdditionalMediaViewHolder.RemoveMediaCallback {

    private final Context mContext;
    private IUploadCallback iUploadCallback;
    private List<AdditionalMedia> mMediaPaths;

    public AdditionalMediaAdapter(Context context,IUploadCallback uploadCallback, List<AdditionalMedia> mediaPaths) {
        mContext = context;
        iUploadCallback = uploadCallback;
        mMediaPaths = mediaPaths;
    }

    public void setAdditionalMedia(AdditionalMedia additionalMedia) {
//        MediaLink additionalMedia = new MediaLink();
//        additionalMedia.setId(id);
//        additionalMedia.setMediaUrl(filePath);
        mMediaPaths.add(additionalMedia);
        notifyDataSetChanged();
    }

    public void setAdditionalMedias(List<AdditionalMedia> additionalMedias) {
//        MediaLink additionalMedia = new MediaLink();
//        additionalMedia.setId(id);
//        additionalMedia.setMediaUrl(filePath);
        mMediaPaths.addAll(additionalMedias);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == Constants.ADDITIONAL_MEDIA.FOOTER) {
            viewHolder = new AdditionalMediaFooterViewHolder((ItemAddAdditionalMediaBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_add_additional_media, parent, false));
        } else
            viewHolder = new AdditionalMediaViewHolder((ItemAdditionalMediaBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_additional_media, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AdditionalMediaViewHolder) {
            ((AdditionalMediaViewHolder) holder).bindData(mContext,mMediaPaths.get(position), this, holder.getAdapterPosition());
        } else if (holder instanceof AdditionalMediaFooterViewHolder) {
            ((AdditionalMediaFooterViewHolder) holder).bindData(this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position<mMediaPaths.size())
        {
            return Constants.ADDITIONAL_MEDIA.MEDIA;
        }
        else
        {
            return Constants.ADDITIONAL_MEDIA.FOOTER;
        }
    }

    @Override
    public int getItemCount() {
        if(mMediaPaths.size()<10)
        {
            return mMediaPaths.size()+1;
        }
        else
        {
            return mMediaPaths.size();
        }

    }

//    @Override
//    public void onOptionSelected() {
//        mMediaPaths.add(mMediaPaths.size() - 1, new Certificate().addNewCertificate(mMediaPaths.size()-2));
//        notifyItemInserted(mMediaPaths.size() - 1);
//    }
//
//    public void setDocImg(int position, String filePath, int id) {
//        Certificate certificate = mMediaPaths.get(position);
//        certificate.setId(id);
//        certificate.setCertificateUrl(filePath);
//        notifyItemChanged(position);
//    }
//
//    @Override
//    public void onUploadSelected(int type, int id) {
//        iUploadCallback.onUploadSelected(type, id);
//    }
//
//    @Override
//    public void onDeleteSel(int id, int position) {
//        iUploadCallback.onDelSelected(id, position);
//    }

    @Override
    public void onMediaRemoved(int position) {
        iUploadCallback.onRemoveMediaSelected(position);
    }

    @Override
    public void onAddMediaSelected() {
        iUploadCallback.onAddMediaSelected();
    }

    public interface IUploadCallback {
        void onAddMediaSelected();
        void onRemoveMediaSelected( int position);
    }

}
