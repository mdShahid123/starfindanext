package com.sportsstars.ui.coach;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.chat.chat.ImagePreviewActivity;
import com.sportsstars.databinding.ActivityViewCoachProfileBinding;
import com.sportsstars.interfaces.RecyclerPageChangeListener;
import com.sportsstars.model.Certificate;
import com.sportsstars.model.Facility;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.ImageAdapter;
import com.sportsstars.ui.auth.adapter.LinePagerIndicatorDecoration;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;

public class CoachViewProfileActivity extends BaseActivity implements View.OnClickListener, ImageAdapter.MediaClickCallBack, RecyclerPageChangeListener {

    private ActivityViewCoachProfileBinding mActivityViewCoachProfileBinding;
    private UserProfileModel userProfileModel;

    @Override
    public String getActivityName() {
        return CoachViewProfileActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityViewCoachProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_coach_profile);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCoachProfileStatus();
    }

    private void initUI() {
        userProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if (userProfileModel != null) {
            mActivityViewCoachProfileBinding.setCoachInfo(userProfileModel);
            setProfilePic();
//            setYoutubeView();
            setMediaView();
            setCerView();
            setTeamView();
            setHonourView();
            setSessionView();
            setLocView();
            setSpecialityView();
            String age = getString(R.string.txt_frm) + userProfileModel.getAgeGroupFrom() + getString(R.string.txt_yr) + getString(R.string.txt_to) + userProfileModel.getAgeGroupTo() + getString(R.string.txt_yr);
            ((TextView) mActivityViewCoachProfileBinding.blAges.findViewById(R.id.tvSpecList)).setText(age);

//            mActivityViewCoachProfileBinding.youtubeParent.setOnClickListener(this);
            mActivityViewCoachProfileBinding.editTextView.setOnClickListener(this);
        }
    }


    private void getCoachProfileStatus() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(CoachViewProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(userProfileModel != null) {
                        userProfileModel = null;
                        UserProfileInstance.getInstance().setUserProfileModel(null);
                    }
                    userProfileModel = response.getResult();
                    UserProfileInstance.getInstance().setUserProfileModel(userProfileModel);
                    initUI();
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CoachViewProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(CoachViewProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setCerView() {
        try {
            mActivityViewCoachProfileBinding.llCertificate.removeAllViews();
            List<Certificate> list = userProfileModel.getCertificateList();
            if(list != null) {
                if(list.size() == 0) {
                    mActivityViewCoachProfileBinding.tvAccreditationTitle.setVisibility(View.GONE);
                    return;
                }
                for (Certificate certificate : list) {
                    if (certificate.getCertificateTitle().contains(Constants.PUBLIC_LIABILITY_INSURANCE) || certificate.getCertificateTitle().contains(Constants.POLICE_CHECK)) {
                        View view = getLayoutInflater().inflate(R.layout.item_bullet_with_icon_tv, null);
                        ((ImageView) view.findViewById(R.id.ivBulletIcon)).setImageResource(R.drawable.ic_verified_coach);
                        ((TextView) view.findViewById(R.id.tvSpecList)).setText(certificate.getCertificateTitle());
                        mActivityViewCoachProfileBinding.llCertificate.addView(view);
                    } else if (certificate.getCertificateTitle().contains(Constants.WORK_WITH_CHILDREN_CERTIFICATE) || certificate.getCertificateTitle().contains("Work With Children Certificate")) {
                        View view = getLayoutInflater().inflate(R.layout.item_bullet_with_icon_tv, null);
                        ((ImageView) view.findViewById(R.id.ivBulletIcon)).setImageResource(R.drawable.ic_children);
                        ((TextView) view.findViewById(R.id.tvSpecList)).setText(certificate.getCertificateTitle());
                        mActivityViewCoachProfileBinding.llCertificate.addView(view);
                    } else {
                        View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
                        ((TextView) view.findViewById(R.id.tvSpecList)).setText(certificate.getCertificateTitle());
                        mActivityViewCoachProfileBinding.llCertificate.addView(view);
                    }

//            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
//            ((TextView) view.findViewById(R.id.tvSpecList)).setText(StringUtils.toCamelCase(certificate.getCertificateTitle()));
//            mBinding.llCertificate.addView(view);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProfilePic() {
        if (userProfileModel.getSportDetail() != null)
            Picasso.with(CoachViewProfileActivity.this)
                    .load(userProfileModel.getSportDetail().getIcon()).placeholder(R.drawable.ic_cricket_green).
                    error(R.drawable.ic_cricket_green).into(mActivityViewCoachProfileBinding.ivSportIcon);
        if (StringUtils.isNullOrEmpty(userProfileModel.getProfileImage()))
            return;
        Picasso.with(CoachViewProfileActivity.this)
                .load(userProfileModel.getProfileImage()).placeholder(R.drawable.ic_user_circle).
                error(R.drawable.ic_user_circle).into(mActivityViewCoachProfileBinding.ivProfilePic,
                new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        mActivityViewCoachProfileBinding.ivBgHeader.setImageDrawable(mActivityViewCoachProfileBinding.ivProfilePic.getDrawable());
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

//    private void setYoutubeView() {
//        String youtubeUrl = Utils.extractYoutubeThumbnail(userProfileModel.getYoutubeLink());
//        if (!StringUtils.isNullOrEmpty(youtubeUrl))
//            Picasso.with(this)
//                    .load(youtubeUrl).placeholder(R.drawable.media_place_holder).
//                    error(R.drawable.media_place_holder).into(mActivityViewCoachProfileBinding.ivThumbnail);
//    }

    private void setTeamView() {
        mActivityViewCoachProfileBinding.llTeams.removeAllViews();
        //mActivityViewCoachProfileBinding.llTeams.removeAllViewsInLayout();
        List<Team> list = userProfileModel.getTeamPlayedFor();
        for (Team team : list) {
            if(team != null && !TextUtils.isEmpty(team.getName())) {
                View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
                ((TextView) view.findViewById(R.id.tvSpecList)).setText(team.getName());
                mActivityViewCoachProfileBinding.llTeams.addView(view);
            }
        }
    }

    private void setHonourView() {
        mActivityViewCoachProfileBinding.llHonour.removeAllViews();
        if (!StringUtils.isNullOrEmpty(userProfileModel.getAccolades())) {
            String[] items = userProfileModel.getAccolades().split("\\s*,\\s*");
            drawView(items, mActivityViewCoachProfileBinding.llHonour);
        }
    }

    private void drawView(String[] items, LinearLayout container) {
        for (String item : items) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
            ((TextView) view.findViewById(R.id.tvSpecList)).setText(item);
            container.addView(view);
        }
    }

    private void setLocView() {
        mActivityViewCoachProfileBinding.llLocations.removeAllViews();
        List<Facility> list = userProfileModel.getCoachLocation();
        for (Facility facility : list) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_with_loc_icon_tv, null);
            ((TextView) view.findViewById(R.id.tvFacilityName)).setText(facility.getFacilityName());
            ((ImageView) view.findViewById(R.id.ivBulletIcon)).setImageResource(R.drawable.ic_location_pin_small);
            ((TextView) view.findViewById(R.id.tvFacilityAddress)).setText(facility.getFacilityAddress());
            mActivityViewCoachProfileBinding.llLocations.addView(view);
        }
    }

    private void setSpecialityView() {
        mActivityViewCoachProfileBinding.llEsp.removeAllViews();
        if (!StringUtils.isNullOrEmpty(userProfileModel.getSpecialities())) {
            String[] items = userProfileModel.getSpecialities().split("\\s*,\\s*");
            drawView(items, mActivityViewCoachProfileBinding.llEsp);
        }
    }

    private void setSessionView() {
        mActivityViewCoachProfileBinding.llSession.removeAllViews();
        if (!StringUtils.isNullOrEmpty(userProfileModel.getSessionType())) {
            String[] items = userProfileModel.getSessionType().equals(Constants.SESSIONS.SESSION_HANDS_ON) ? new String[]{getString(R.string.txt_handsOn)} : userProfileModel.getSessionType().equals(Constants.SESSIONS.SESSION_INSPIRATIONAL) ? new String[]{getString(R.string.txt_inspirational)} : new String[]{getString(R.string.txt_handsOn), getString(R.string.txt_inspirational)};
            drawView(items, mActivityViewCoachProfileBinding.llSession);
        }
    }

    private void setMediaView() {

        //showToast("media url size : " + userProfileModel.getMediaUrl().size());
        if(userProfileModel.getMediaUrl().size() > 0) {
            mActivityViewCoachProfileBinding.mediaContainer.setVisibility(View.VISIBLE);
            mActivityViewCoachProfileBinding.tvPagesMedia.setVisibility(View.VISIBLE);
        } else {
            mActivityViewCoachProfileBinding.mediaContainer.setVisibility(View.GONE);
            mActivityViewCoachProfileBinding.tvPagesMedia.setVisibility(View.GONE);
        }

        ImageAdapter imageAdapter = new ImageAdapter(userProfileModel.getMediaUrl(),this);
        mActivityViewCoachProfileBinding.mediaContainer.setHasFixedSize(true);
        mActivityViewCoachProfileBinding.mediaContainer.setLayoutManager(new LinearLayoutManager(CoachViewProfileActivity.this, LinearLayoutManager.HORIZONTAL, false));
        mActivityViewCoachProfileBinding.mediaContainer.setAdapter(imageAdapter);

        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        mActivityViewCoachProfileBinding.mediaContainer.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(mActivityViewCoachProfileBinding.mediaContainer);

        mActivityViewCoachProfileBinding.mediaContainer.addItemDecoration(new LinePagerIndicatorDecoration(this));

        if(userProfileModel.getMediaUrl().size() > 0) {
            mActivityViewCoachProfileBinding.tvPagesMedia.setText("1/" + userProfileModel.getMediaUrl().size());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editTextView:
                Navigator.getInstance().navigateToActivity(this, EditCoachProfileActivity.class);
                break;
//            case R.id.youtubeParent:
//                if(userProfileModel != null && !TextUtils.isEmpty(userProfileModel.getYoutubeLink())) {
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.BundleKey.DATA, userProfileModel.getYoutubeLink());
//                    Navigator.getInstance().navigateToActivityWithData(this, YouTubeMediaActivity.class, bundle);
//                } else {
//                    showSnackbarFromTop(CoachViewProfileActivity.this, getResources().getString(R.string.error_youtube));
//                }
//                break;
        }
    }

    @Override
    public void onMediaClicked(int position) {
        if(userProfileModel.getMediaUrl().get(position).getType()==7)
        {
            Bundle bundlePlay = new Bundle();
            bundlePlay.putString(Constants.BundleKey.VIDEO_URL, userProfileModel.getMediaUrl().get(position).getImageUrl());
            Navigator.getInstance().navigateToActivityWithData(this, PlayerActivity.class, bundlePlay);
        }
        else
        {
            Intent intent = new Intent(this, ImagePreviewActivity.class);
            intent.putExtra(ImagePreviewActivity.EXTRA_IMAGE_URL, userProfileModel.getMediaUrl().get(position).getImageUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onPageChanged(int position) {
        if(userProfileModel.getMediaUrl().size()>0)
        {
            mActivityViewCoachProfileBinding.tvPagesMedia.setText((position+1)+"/"+userProfileModel.getMediaUrl().size());
        }
    }
}
