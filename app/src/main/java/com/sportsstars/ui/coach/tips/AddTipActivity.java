package com.sportsstars.ui.coach.tips;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.iceteck.silicompressorr.SiliCompressor;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddTipBinding;
import com.sportsstars.model.Sports;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.coachtips.CoachTips;
import com.sportsstars.network.response.coachtips.CreateTipResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.ui.search.SearchCoachActivityNext;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.FileUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URISyntaxException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AddTipActivity extends BaseActivity implements View.OnClickListener, Alert.OnOkayClickListener {

    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 1234;
    private ActivityAddTipBinding mBinding;
    private boolean isEditModeEnabled;
    private CoachTips coachTips, coachTipsResponse;
    private int tipsType;
    private int sportId;
    private String imageFilePath, videoFilePath;
    private boolean isUploadButtonClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_tip);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (getIntent().hasExtra(Constants.BundleKey.COACH_TIPS)) {
                coachTips = bundle.getParcelable(Constants.BundleKey.COACH_TIPS);
                if (coachTips != null) {
                    isEditModeEnabled = true;
                }
            } else {
                isEditModeEnabled = false;
            }
        }

        initViews();
        if (isEditModeEnabled) {
            setDataOnUI();
        }
    }

    private void setDataOnUI() {
        sportId = coachTips.getSportId();
        tipsType = coachTips.getType();

        mBinding.clTitle.etValue.setText(coachTips.getTitle());
        if (mBinding.clTitle.etValue.getText().toString().length() > 0) {
            mBinding.clTitle.etValue.setSelection(mBinding.clTitle.etValue.getText().toString().length());
        }

        mBinding.clSport.tvLabel.setText(getResources().getString(R.string.sport));
        mBinding.clSport.etValue.setText(coachTips.getSportName());
        mBinding.clSport.ivSpinner.setVisibility(View.VISIBLE);
        mBinding.clSport.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);

        mBinding.clType.tvLabel.setText(getResources().getString(R.string.type));
//        mBinding.clType.etValue.setText(tipsType == 1 ?
//                getResources().getString(R.string.video) : getResources().getString(R.string.text));
        mBinding.clType.etValue.setText(getResources().getString(R.string.video));
        mBinding.clType.ivSpinner.setVisibility(View.VISIBLE);
        mBinding.clType.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);

        videoFilePath = coachTips.getVideoUrl();
        imageFilePath = coachTips.getImageUrl();

        setViewsVisibility(tipsType, imageFilePath);
//        if(tipsType == Constants.TIPS_TYPE.VIDEO) {
//            if(!TextUtils.isEmpty(coachTips.getImageUrl())) {
//                Picasso.with(AddTipActivity.this)
//                        .load(coachTips.getImageUrl())
//                        .into(mBinding.ivThumbnail);
//            }
//
//            mBinding.rlPlay.setVisibility(View.VISIBLE);
//        }

        if (!TextUtils.isEmpty(coachTips.getImageUrl())) {
            Picasso.with(AddTipActivity.this)
                    .load(coachTips.getImageUrl())
                    .into(mBinding.ivThumbnail);
        }

        mBinding.rlPlay.setVisibility(View.VISIBLE);
    }

    private void initViews() {

        mBinding.topHeader.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        if (isEditModeEnabled) {
            mBinding.topHeader.tvTitle.setText(getString(R.string.edit_tip));
            mBinding.tvUploadVideo.setText(getResources().getString(R.string.update_video));
        } else {
            mBinding.topHeader.tvTitle.setText(getString(R.string.add_tip));
            mBinding.tvUploadVideo.setText(getResources().getString(R.string.upload_video));
        }

        mBinding.topHeader.ivBack.setVisibility(View.VISIBLE);
        mBinding.topHeader.ivBack.setOnClickListener(this);
        mBinding.btnSave.setOnClickListener(this);
        mBinding.tvUploadVideo.setOnClickListener(this);
        mBinding.rlPlay.setOnClickListener(this);

        mBinding.spinnerTipsType.setSelection(-1);

        mBinding.clSport.tvLabel.setText(getResources().getString(R.string.select_sport));
        mBinding.clSport.etValue.setHint(getResources().getString(R.string.example_cricket));
        mBinding.clSport.etValue.setOnClickListener(this);
        mBinding.clSport.ivSpinner.setVisibility(View.VISIBLE);

        mBinding.clType.tvLabel.setText(getResources().getString(R.string.select_type));
        mBinding.clType.etValue.setHint(getResources().getString(R.string.example_video));
        mBinding.clType.ivSpinner.setVisibility(View.VISIBLE);

        //set tips type spinner
        ArrayAdapter<String> tipsTypeAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner_transparent, getResources().getStringArray(R.array.tips_type_array)) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(ContextCompat.getColor(AddTipActivity.this, R.color.hint_text_color));
                } else {
                    tv.setTextColor(ContextCompat.getColor(AddTipActivity.this, R.color.txt_clr_green));
                }
                return view;
            }
        };
        mBinding.spinnerTipsType.setAdapter(tipsTypeAdapter);
        tipsTypeAdapter.setDropDownViewResource(R.layout.item_booking_spinner);
        mBinding.spinnerTipsType.setOnItemSelectedListener(tipsTypeSelectedListener);
    }

    AdapterView.OnItemSelectedListener tipsTypeSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                return;
            }
            mBinding.clType.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
            mBinding.clType.etValue.setText(String.valueOf(parent.getItemAtPosition(position)));

            tipsType = (position == 1 ? Constants.TIPS_TYPE.VIDEO : Constants.TIPS_TYPE.TEXT);

            mBinding.spinnerTipsType.setSelection(-1);

            ((TextView) view).setText(null);

            if (tipsType != Constants.TIPS_TYPE.VIDEO) {
                videoFilePath = null;
                imageFilePath = null;
            }

            setViewsVisibility(tipsType, imageFilePath);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            LogUtils.LOGI("TAG", "Nothing Selected");
        }
    };

    private void setViewsVisibility(int tipsType, String imageFilePath) {

        mBinding.tvSelectedType.setVisibility(View.VISIBLE);
        mBinding.tvSelectedType.setText(getResources().getString(R.string.video));
        mBinding.rlParentThumbnail.setVisibility(View.VISIBLE);
        mBinding.rlPlay.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(imageFilePath)) {
            mBinding.tvPlay.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvPlay.setVisibility(View.INVISIBLE);
        }
//            if(!TextUtils.isEmpty(imageFilePath)) {
//                mBinding.rlParentThumbnail.setVisibility(View.VISIBLE);
//            } else {
//                mBinding.rlParentThumbnail.setVisibility(View.INVISIBLE);
//            }
        mBinding.tvUploadVideo.setVisibility(View.VISIBLE);

//        if(tipsType == Constants.TIPS_TYPE.VIDEO) {
//            mBinding.tvSelectedType.setVisibility(View.VISIBLE);
//            mBinding.tvSelectedType.setText(getResources().getString(R.string.video));
//            mBinding.rlParentThumbnail.setVisibility(View.VISIBLE);
//            mBinding.rlPlay.setVisibility(View.VISIBLE);
//            if(!TextUtils.isEmpty(imageFilePath)) {
//                mBinding.tvPlay.setVisibility(View.VISIBLE);
//            } else {
//                mBinding.tvPlay.setVisibility(View.INVISIBLE);
//            }
////            if(!TextUtils.isEmpty(imageFilePath)) {
////                mBinding.rlParentThumbnail.setVisibility(View.VISIBLE);
////            } else {
////                mBinding.rlParentThumbnail.setVisibility(View.INVISIBLE);
////            }
//            mBinding.tvUploadVideo.setVisibility(View.VISIBLE);
//        } else if(tipsType == Constants.TIPS_TYPE.TEXT) {
//            mBinding.tvSelectedType.setVisibility(View.VISIBLE);
//            mBinding.tvSelectedType.setText(getResources().getString(R.string.text));
//            mBinding.rlParentThumbnail.setVisibility(View.INVISIBLE);
//            mBinding.tvUploadVideo.setVisibility(View.INVISIBLE);
//        }
    }

    @Override
    public String getActivityName() {
        return AddTipActivity.class.getSimpleName();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                isUploadButtonClicked = false;
                onBackPressed();
                break;
            case R.id.btn_save:
                isUploadButtonClicked = false;
                if (ContextCompat.checkSelfPermission(AddTipActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    askStoragePermission();
                } else {
                    if (!isInputValid()) {
                        return;
                    }
                    try {
                        createOrUpdateTip(isEditModeEnabled);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.et_value:
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.FROM_ADD_TIP, true);
                Navigator.getInstance().navigateToActivityForResultWithData(AddTipActivity.this, SearchCoachActivityNext.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST);
                break;
            case R.id.tv_upload_video:
                isUploadButtonClicked = true;
                if (ContextCompat.checkSelfPermission(AddTipActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    askStoragePermission();
                } else {
                    openNativeVideoPicker();
                }
                break;
            case R.id.rl_play:
                if (!TextUtils.isEmpty(videoFilePath)) {
                    if (videoFilePath.contains("http")) {
                        launchVideoPlayer();
                    } else {
                        if (new File(videoFilePath).exists()) {
                            launchVideoPlayer();
                        } else {
                            showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
                        }
                    }
                } else {
                    isUploadButtonClicked = true;
                    if (ContextCompat.checkSelfPermission(AddTipActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        askStoragePermission();
                    } else {
                        openNativeVideoPicker();
                    }
                    //showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
                }
                break;
        }
    }

    private void launchVideoPlayer() {
        Bundle bundlePlay = new Bundle();
        bundlePlay.putString(Constants.BundleKey.VIDEO_URL, videoFilePath);
        Navigator.getInstance().navigateToActivityWithData(AddTipActivity.this, PlayerActivity.class, bundlePlay);
    }

    private void openNativeVideoPicker() {
        Intent videoLibraryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        videoLibraryIntent.setType("video/*");
        videoLibraryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(videoLibraryIntent, Constants.GET_VIDEO);
    }

    private boolean isInputValid() {
        boolean isValid = true;

        if (TextUtils.isEmpty(mBinding.clTitle.etValue.getText().toString().trim())) {
            isValid = false;
            mBinding.clTitle.etValue.requestFocus();
            showSnackbarFromTop(this, getString(R.string.error_msg_no_title));
        } else if (TextUtils.isEmpty(mBinding.clSport.etValue.getText().toString().trim())) {
            isValid = false;
            showSnackbarFromTop(this, getString(R.string.error_msg_no_sport));
        } else if (TextUtils.isEmpty(mBinding.clType.etValue.getText().toString().trim())) {
            isValid = false;
            showSnackbarFromTop(this, getString(R.string.error_msg_no_type));
        }

//        if(tipsType == Constants.TIPS_TYPE.VIDEO) {
//            if(TextUtils.isEmpty(videoFilePath)) {
//                isValid = false;
//                showSnackbarFromTop(this, getString(R.string.error_msg_no_video_file));
//            } else if(TextUtils.isEmpty(imageFilePath)) {
//                isValid = false;
//                showSnackbarFromTop(this, getString(R.string.error_msg_no_image_file));
//            }
//        }

        if (TextUtils.isEmpty(videoFilePath)) {
            isValid = false;
            showSnackbarFromTop(this, getString(R.string.error_msg_no_video_file));
        } else if (TextUtils.isEmpty(imageFilePath)) {
            isValid = false;
            showSnackbarFromTop(this, getString(R.string.error_msg_no_image_file));
        }

        return isValid;
    }

    private void createOrUpdateTip(boolean isEditModeEnabled) {

        RequestBody tipTitleBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), mBinding.clTitle.etValue.getText().toString().trim());

        if (TextUtils.isEmpty(String.valueOf(sportId))) {
            showSnackbarFromTop(this, getString(R.string.error_msg_no_sport));
            return;
        }


        tipsType = Constants.TIPS_TYPE.VIDEO;


        RequestBody tipSportIdBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), String.valueOf(sportId));
        RequestBody tipTypeBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), String.valueOf(tipsType));

        String description = "";
        if (tipsType == Constants.TIPS_TYPE.VIDEO) {
            description = "";
        } else {
            description = "Tip Description";
        }
        RequestBody tipDescriptionBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), description);

        Log.d("mylog", "Before Multipart - Video file path : " + videoFilePath
                + " image file path : " + imageFilePath);
        File videoFile = null;
        if (!videoFilePath.contains("http")) {
            videoFile = new File(videoFilePath);
            if (videoFile == null || !videoFile.exists()) {
                showSnackbarFromTop(this, getString(R.string.error_msg_no_video_file));
                return;
            }
        }

        File imageFile = null;
        if (!imageFilePath.contains("http")) {
            imageFile = new File(imageFilePath);
            if (imageFile == null || !imageFile.exists()) {
                showSnackbarFromTop(this, getString(R.string.error_msg_no_image_file));
                return;
            }
        }

        MultipartBody.Part imagePart = null;
        if (imageFile != null) {
            RequestBody imageFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_IMAGE), imageFile);
            imagePart = MultipartBody.Part.createFormData("imageFile", imageFile.getName(), imageFileBody);
        }

        MultipartBody.Part videoPart = null;
        if (videoFile != null) {
            RequestBody videoFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_VIDEO), videoFile);
            videoPart = MultipartBody.Part.createFormData("videoFile", videoFile.getName(), videoFileBody);
        }

        showProgressBar();

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        Call<CreateTipResponse> call = null;
        if (!isEditModeEnabled) {
            call = client.createTip(tipTitleBody, tipSportIdBody, tipTypeBody, tipDescriptionBody,
                    imagePart, videoPart);
        } else {
            RequestBody tipIdBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                    String.valueOf(coachTips.getId()));

            // This is actual implementation
            call = client.updateTip(tipIdBody, tipTitleBody, tipSportIdBody, tipTypeBody, tipDescriptionBody,
                    imagePart, videoPart);

            // TODO: Getting error from server while updating file also.
            // For time being keeping null for image and video part.
//            call = client.updateTip(tipIdBody, tipTitleBody, tipSportIdBody, tipTypeBody, tipDescriptionBody,
//                    null, null);

        }
        call.enqueue(new BaseCallback<CreateTipResponse>(AddTipActivity.this) {
            @Override
            public void onSuccess(CreateTipResponse createTipResponse) {
                if (createTipResponse != null) {
                    if (createTipResponse.getStatus() == 1) {
                        if (createTipResponse.getResult() != null) {
                            if (!isEditModeEnabled) {
//                            showSnackbarFromTop(AddTipActivity.this,
//                                    getResources().getString(R.string.tip_successfully_added));

                                coachTipsResponse = createTipResponse.getResult();
                                Alert.showAlertDialog(AddTipActivity.this,
                                        getString(R.string.tip_added),
                                        getString(R.string.tip_successfully_added),
                                        getString(R.string.okay),
                                        AddTipActivity.this);
                            } else {
//                            showSnackbarFromTop(AddTipActivity.this,
//                                    getResources().getString(R.string.tip_successfully_updated));

                                coachTipsResponse = createTipResponse.getResult();
                                coachTips = coachTipsResponse;
                                Alert.showAlertDialog(AddTipActivity.this,
                                        getString(R.string.tip_updated),
                                        getString(R.string.tip_successfully_updated),
                                        getString(R.string.okay),
                                        AddTipActivity.this);
                            }
                        }
                    } else {
                        if (!isEditModeEnabled) {
                            showSnackbarFromTop(AddTipActivity.this,
                                    getResources().getString(R.string.failed_to_add_tip));
                        } else {
                            showSnackbarFromTop(AddTipActivity.this,
                                    getResources().getString(R.string.failed_to_update_tip));
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<CreateTipResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddTipActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onOkay() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKey.COACH_TIPS, coachTipsResponse);
        Navigator.getInstance().navigateToActivityWithData(AddTipActivity.this, ViewTipActivity.class, bundle);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Sports sports = data.getParcelableExtra(Constants.EXTRA_SPORT);
                    if (sports != null && !TextUtils.isEmpty(sports.getName())) {
                        mBinding.clSport.etValue.setText(sports.getName());
                        mBinding.clSport.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
                        sportId = sports.getId();
                    }
                }
            }
        } else if (requestCode == Constants.GET_VIDEO) {
            if (data != null && data.getData() != null) {
                //create destination directory
                File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/videos");
                if (f.mkdirs() || f.isDirectory()) {
                    if (FileUtils.isVideoFileExists(AddTipActivity.this, data.getData())) {
                        //compress and output new video specs
                        new VideoCompressAsyncTask(this).execute(data.getData().toString(), f.getPath());
                    } else {
                        showSnackbarFromTop(AddTipActivity.this, getResources().getString(R.string.unable_to_fetch_video));
                    }
                }
            }
        }
    }

    private void showVideoPreview(String videoPath, int tipType) {
        if (videoPath != null && videoPath.length() > 0) {
            Log.d("mylog", "LINE 399 - AddTip - onActivityResult - "
                    + "\n" + "Video path : " + videoPath);
            final File sourceFile = new File(videoPath);
            if (sourceFile.exists()) {
                if (!FileUtils.isMaxSizeFile(sourceFile)) {
                    String videoThumbnailPath =
                            FileUtils.createVideoThumbnail(videoPath, PreferenceUtil.getUserModel().getUserId());
                    if (!TextUtils.isEmpty(videoThumbnailPath)) {
                        File videoThumbFile = new File(videoThumbnailPath);
                        if (videoThumbFile.exists()) {
                            Log.d("mylog", "LINE 408 - AddTip - Video thumbnail path : " + videoThumbnailPath);
                            Picasso.with(this).load(videoThumbFile).into(mBinding.ivThumbnail);

                            videoFilePath = videoPath;
                            imageFilePath = videoThumbnailPath;

                            setViewsVisibility(tipsType, imageFilePath);
                            mBinding.rlPlay.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    showSnackbarFromTop(this, getResources().getString(R.string.max_file_size_limit_reached));
                }
            } else {
                showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
            }
        }
    }

    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isUploadButtonClicked) {
                        openNativeVideoPicker();
                    } else {
                        if (!isInputValid()) {
                            return;
                        }
                        try {
                            createOrUpdateTip(isEditModeEnabled);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(this, "Permission not granted to access device storage.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showStoragePermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
    }

    private void showStoragePermissionExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.storage_permission_title));
        builder.setMessage(getResources().getString(R.string.storage_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(AddTipActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_WRITE_EXTERNAL_STORAGE);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar("Compressing Video");
        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(Uri.parse(paths[0]), paths[1], 0, 0, 600000);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            Log.i("Silicompressor", "Path: " + compressedFilePath);
            hideProgressBar();
            showVideoPreview(compressedFilePath, Constants.GET_VIDEO);

        }
    }


}
