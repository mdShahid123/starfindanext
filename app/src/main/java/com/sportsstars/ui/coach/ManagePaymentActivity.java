package com.sportsstars.ui.coach;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityManagePaymentBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.CoachBankAccount;
import com.sportsstars.network.response.CoachBankAccountResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.payment.AddBankAccountActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import retrofit2.Call;

public class ManagePaymentActivity extends BaseActivity {

    private ActivityManagePaymentBinding mManagePaymentBinding;
    private CoachBankAccount mCoachBankAccount;
    private boolean isEmptyBankAccount;

    @Override
    public String getActivityName() {
        return ManagePaymentActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mManagePaymentBinding = DataBindingUtil.setContentView(this, R.layout.activity_manage_payment);

        initUI();
        setDataOnUI();
    }

    private void initUI() {
        mManagePaymentBinding.header.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mManagePaymentBinding.header.tvTitle.setText(getString(R.string.payment));

        mManagePaymentBinding.managePaymentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable()) {
                    launchAddBankAccountForEdit();
                } else {
                    showSnackbarFromTop(ManagePaymentActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    private void setDataOnUI() {
        if(Utils.isNetworkAvailable()) {
            getBankAccount();
        } else {
            mManagePaymentBinding.managePaymentTextView.setVisibility(View.INVISIBLE);
            mManagePaymentBinding.headerText.setText(getResources().getString(R.string.no_internet));
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    private void launchAddBankAccountForEdit() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, true);
        bundle.putBoolean(Constants.BundleKey.IS_EMPTY_BANK_ACCOUNT, isEmptyBankAccount);
        bundle.putParcelable(Constants.BundleKey.COACH_BANK_ACCOUNT_DATA, mCoachBankAccount);
        Navigator.getInstance().navigateToActivityForResultWithData(ManagePaymentActivity.this,
                AddBankAccountActivity.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_BANK_ACCOUNT);
    }

    private void getBankAccount() {
        processToShowDialog();
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCoachBankAccount().enqueue(new BaseCallback<CoachBankAccountResponse>(this) {
            @Override
            public void onSuccess(CoachBankAccountResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null) {
                            mCoachBankAccount = response.getResult();
                            String accountNumber = mCoachBankAccount.getAccountNumber();
                            if(!TextUtils.isEmpty(accountNumber)) {
                                isEmptyBankAccount = false;
                                mManagePaymentBinding.managePaymentTextView.setVisibility(View.VISIBLE);
                                mManagePaymentBinding.managePaymentTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_selected, 0, R.drawable.ic_arrow_black_next, 0);
                                String last4DigitsAccountNumber = accountNumber.substring(accountNumber.length()-4, accountNumber.length());
                                String accountNumberWithDots = getResources().getString(R.string.bank_account_number_with_dots_unicode, last4DigitsAccountNumber);
                                mManagePaymentBinding.managePaymentTextView.setText(Html.fromHtml(accountNumberWithDots));
                            } else {
                                showEmptyBankAccountView();
                            }
                        } else {
                            showEmptyBankAccountView();
                        }
                    } else {
                        try {
                            showEmptyBankAccountView();
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(ManagePaymentActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<CoachBankAccountResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        mManagePaymentBinding.managePaymentTextView.setVisibility(View.INVISIBLE);
                        mManagePaymentBinding.headerText.setText("SOME ERROR OCCURRED");
                        //Log.d("mylog", baseResponse.getMessage());
                        showSnackbarFromTop(ManagePaymentActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showEmptyBankAccountView() {
        isEmptyBankAccount = true;
        mManagePaymentBinding.managePaymentTextView.setVisibility(View.VISIBLE);
        mManagePaymentBinding.managePaymentTextView.setText(getResources().getString(R.string.add_a_bank_account));
        mManagePaymentBinding.managePaymentTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank, 0, R.drawable.ic_arrow_black_next, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_BANK_ACCOUNT) {
                setDataOnUI();
            }
        }
    }

}
