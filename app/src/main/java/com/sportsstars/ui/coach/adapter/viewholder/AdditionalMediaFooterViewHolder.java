package com.sportsstars.ui.coach.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.databinding.ItemAddAdditionalMediaBinding;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AdditionalMediaFooterViewHolder extends RecyclerView.ViewHolder {

    private ItemAddAdditionalMediaBinding mBinding;

    public AdditionalMediaFooterViewHolder(ItemAddAdditionalMediaBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindData(final AddMediaCallback listener){
        mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAddMediaSelected();
            }
        });
    }

    public interface AddMediaCallback {
        void onAddMediaSelected();
    }

}
