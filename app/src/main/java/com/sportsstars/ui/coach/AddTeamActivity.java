package com.sportsstars.ui.coach;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddTeamBinding;
import com.sportsstars.interfaces.AddTeamItemClick;
import com.sportsstars.model.Team;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.coachprofile.TeamsListBaseResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.adapter.AddTeamRecyclerViewAdapter;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class AddTeamActivity extends BaseActivity implements AddTeamItemClick, View.OnClickListener {

    private ActivityAddTeamBinding mBinding;
    private AddTeamRecyclerViewAdapter mAddTeamAdapter;
    private List<Team> mTeamsList;
    private List<Team> mSelectedTeamsList;

    private int mTotalAPIPages;
    private int mCurrentAPIPage;
    private boolean mIsFetchingDataFromAPI;

    private boolean isLaunchingFirstTime;

    private boolean isFromLearnerProfile;

    @Override
    public String getActivityName() {
        return AddTeamActivity.class.getName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isLaunchingFirstTime = true;
        initUI();
        setupTeamsList();
    }

    private void initUI() {
        Utils.setTransparentTheme(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_team);
        mBinding.bSave.setOnClickListener(this);

        if (getIntent() != null) {
            mTeamsList = getIntent().getParcelableArrayListExtra("teams");
            //mBinding.header.tvTitle.setText(getString(R.string.select_teams));
            mBinding.header.tvTitle.setText(getString(R.string.add_teams_you_ve_player_for));
        } else {
            mBinding.header.tvTitle.setText(getString(R.string.add_teams_you_ve_player_for));
        }

        if(getIntent() != null) {
            if(getIntent().hasExtra(Constants.FROM_LEARNER_PROFILE)) {
                isFromLearnerProfile = getIntent().getBooleanExtra(Constants.FROM_LEARNER_PROFILE, false);
            }
        }

        mBinding.header.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.header.ivBack.setVisibility(View.GONE);
    }

    private void setupTeamsList() {

        if (getIntent() != null && getIntent().getParcelableArrayListExtra(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST) != null) {
            mSelectedTeamsList = getIntent().getParcelableArrayListExtra(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST);

            //Check if teams were already selected previously by the user
            if (mTeamsList != null) {
                for (Team team : mTeamsList) {
                    if (mSelectedTeamsList.contains(team)) {
                        team.setIsSelected(true);
                    }
                }
            }

            if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
                //mBinding.header.tvTitle.setText(getString(R.string.select_teams));
                mBinding.header.tvTitle.setText(getString(R.string.add_teams_you_ve_player_for));
            } else {
                mBinding.header.tvTitle.setText(getString(R.string.add_teams_you_ve_player_for));
            }
        } else {
            mSelectedTeamsList = new ArrayList<>();
        }

        if (mTeamsList == null) {
            mTeamsList = new ArrayList<>();
            mCurrentAPIPage = 1;
            fetchTeamsFromAPI();
        }

        mAddTeamAdapter = new AddTeamRecyclerViewAdapter(mTeamsList, this, this);
        mBinding.rvTeams.setAdapter(mAddTeamAdapter);

        setupPagination();
    }

    private void setupPagination() {

        mBinding.rvTeams.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) mBinding.rvTeams.getLayoutManager();

                int currentlyVisibleCount = layoutManager.getChildCount();
                int visiblePreviouslyCount = layoutManager.findFirstVisibleItemPosition();
                int totalItemsInList = layoutManager.getItemCount();

                if (mCurrentAPIPage < mTotalAPIPages
                        && !mIsFetchingDataFromAPI
                        && (currentlyVisibleCount + visiblePreviouslyCount <= totalItemsInList)) {
                    if (isLaunchingFirstTime) {
                        isLaunchingFirstTime = false;
                        return;
                    }
                    mCurrentAPIPage++;
                    Log.d("AddTeamLog", "AddTeam - API HIT PAGINATION - mCurrentAPIPage : " + mCurrentAPIPage);
                    fetchTeamsFromAPI();
                }
            }
        });

    }

    private void fetchTeamsFromAPI() {
        Log.d("AddTeamLog", "AddTeam - API HIT");
        mIsFetchingDataFromAPI = true;
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);

        // ------ New Change - As per client improvement calling favourite teams API , no check is required for learner or a coach. ------//

        webServices.getTeams(getIntent().getIntExtra(Constants.BundleKey.ID, 0), mCurrentAPIPage).enqueue(new BaseCallback<TeamsListBaseResponse>(this) {
            @Override
            public void onSuccess(TeamsListBaseResponse response) {
                mIsFetchingDataFromAPI = false;
                if (response != null && response.getResult() != null) {
                    mTotalAPIPages = response.getResult().getLastPage();
                    if (response.getResult().getData() != null && response.getResult().getData().size() > 0) {
                        for (Team team : response.getResult().getData()) {
                            if (mSelectedTeamsList.contains(team)) {
                                team.setIsSelected(true);
                            }
                        }
                        mTeamsList.addAll(response.getResult().getData());
                        mAddTeamAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFail(Call<TeamsListBaseResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                hideProgressBar();
            }
        });


        //------------- COMMENTED BELOW CODE TO REMOVE LEARNER OR COACH CHECK FOR GETTING TEAMS ---------------//

        /*

        TeamListRequest request = new TeamListRequest();
        request.setPage(mCurrentAPIPage);
        request.setPageSize(Constants.API_PAGINATION_RESULTS_LENGTH);

        if (getIntent().getBooleanExtra(Constants.FROM_LEARNER_PROFILE, false)) {
            webServices.getTeams(getIntent().getIntExtra(Constants.BundleKey.ID, 0)).enqueue(new BaseCallback<TeamsListBaseResponse>(this) {
                @Override
                public void onSuccess(TeamsListBaseResponse response) {
                    mIsFetchingDataFromAPI = false;
                    if(response != null && response.getResult() != null) {
                        mTotalAPIPages = response.getResult().getLastPage();
                        if(response.getResult().getData() != null && response.getResult().getData().size() > 0) {
                            for (Team team : response.getResult().getData()) {
                                if (mSelectedTeamsList.contains(team)) {
                                    team.setIsSelected(true);
                                }
                            }
                            mTeamsList.addAll(response.getResult().getData());
                            mAddTeamAdapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onFail(Call<TeamsListBaseResponse> call, BaseResponse baseResponse) {
                    //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                    hideProgressBar();
                }
            });
            return;
        }
        webServices.getTeamsList(request).enqueue(new BaseCallback<TeamsListBaseResponse>(this) {
            @Override
            public void onSuccess(TeamsListBaseResponse response) {
                mIsFetchingDataFromAPI = false;
                if(response != null && response.getResult() != null) {
                    mTotalAPIPages = response.getResult().getLastPage();
                    if(response.getResult().getData() != null && response.getResult().getData().size() > 0) {
                        //Check if teams from API were already selected previously by the user
                        //Check if teams were already selected previously by the user
                        for (Team team : response.getResult().getData()) {
                            if (mSelectedTeamsList.contains(team)) {
                                team.setIsSelected(true);
                            }
                        }

                        //update data displayed in list
                        mTeamsList.addAll(response.getResult().getData());
                        mAddTeamAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFail(Call<TeamsListBaseResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                hideProgressBar();
            }
        });

        */

    }


    @Override
    public void onTeamSelected(Team team) {

        boolean isSelected;

        if(isFromLearnerProfile) {

            // IF FROM LEARNER PROFILE THEN USE SINGLE SELECTION OF TEAM

            if (mSelectedTeamsList.contains(team)) {
                //remove from selected teams list
                mSelectedTeamsList.remove(team);
                isSelected = false;
            } else {
                //add to selected teams list
                mSelectedTeamsList.clear();
                mSelectedTeamsList.add(team);
                isSelected = true;
            }

            if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
                if(mSelectedTeamsList.get(0).getId() == 0 && TextUtils.isEmpty(mSelectedTeamsList.get(0).getName())) {
                    mSelectedTeamsList.remove(0);
                }
            }

            for (Team currentTeam : mTeamsList) {
                if (team.equals(currentTeam))
                    currentTeam.setIsSelected(isSelected);
                else
                    currentTeam.setIsSelected(false);
            }

        } else {

            // IF FROM COACH OR SPEAKER PROFILE THEN USE MULTIPLE SELECTION OF TEAM

            if (mSelectedTeamsList.contains(team)) {
                //remove from selected teams list
                mSelectedTeamsList.remove(team);
                isSelected = false;
            } else {
                //add to selected teams list
                mSelectedTeamsList.add(team);
                isSelected = true;
            }

            for (Team currentTeam : mTeamsList) {
                if (team.equals(currentTeam))
                    currentTeam.setIsSelected(isSelected);
            }

        }


        // BELOW CODE IS USED FOR SINGLE SELECTION

//        if (mSelectedTeamsList.contains(team)) {
//            //remove from selected teams list
//            mSelectedTeamsList.remove(team);
//            isSelected = false;
//        } else {
//            //add to selected teams list
//            mSelectedTeamsList.clear();
//            mSelectedTeamsList.add(team);
//            isSelected = true;
//        }
//
//        for (Team currentTeam : mTeamsList) {
//            if (team.equals(currentTeam))
//                currentTeam.setIsSelected(isSelected);
//            else
//                currentTeam.setIsSelected(false);
//
//        }


        // BELOW CODE IS USED FOR MULTIPLE SELECTION

//        if (mSelectedTeamsList.contains(team)) {
//            //remove from selected teams list
//            mSelectedTeamsList.remove(team);
//            isSelected = false;
//        } else {
//            //add to selected teams list
//            mSelectedTeamsList.add(team);
//            isSelected = true;
//        }
//
//        if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
//            if(mSelectedTeamsList.get(0).getId() == 0 && TextUtils.isEmpty(mSelectedTeamsList.get(0).getName())) {
//                mSelectedTeamsList.remove(0);
//            }
//        }
//
//        for (Team currentTeam : mTeamsList) {
//            if (team.equals(currentTeam))
//                currentTeam.setIsSelected(isSelected);
//        }


        mAddTeamAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_save:
                if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
                    if(mSelectedTeamsList.size() == 1 && mSelectedTeamsList.get(0).getId() == 0
                            && TextUtils.isEmpty(mSelectedTeamsList.get(0).getName())) {
                        showSnackbarFromTop(AddTeamActivity.this, getResources().getString(R.string.please_select_team_you_have_played));
                    } else {
                        sendSelectedTeamsList();
                    }
                } else {
                    showSnackbarFromTop(AddTeamActivity.this, getResources().getString(R.string.please_select_team_you_have_played));
                }
                break;
        }
    }

    private void sendSelectedTeamsList() {

        try {
            if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 1) {
                if(mSelectedTeamsList.get(0).getId() == 0 && TextUtils.isEmpty(mSelectedTeamsList.get(0).getName())) {
                    mSelectedTeamsList.remove(0);
                }
            }

            Intent intent = new Intent();
            intent.putExtra(Constants.BundleKey.INDEX, getIntent().getIntExtra(Constants.BundleKey.INDEX, -1));
            intent.putParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST, (ArrayList<? extends Parcelable>) mSelectedTeamsList);
            setResult(RESULT_OK, intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
