package com.sportsstars.ui.coach;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityConfirmBookingBinding;
import com.sportsstars.model.Facility;
import com.sportsstars.model.UserModel;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.CalendarSyncPostRequest;
import com.sportsstars.network.request.auth.CreateBookingRequest;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.response.CreateBookingBaseResponse;
import com.sportsstars.network.response.CreateBookingResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.LoginActivity;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.fixture.CreateFixtureListActivity;
import com.sportsstars.ui.payment.ManageCardListActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ConfirmBookingActivity extends BaseActivity implements View.OnClickListener, Alert.OnOkayClickListener ,Alert.OnCancelClickListener{

    public static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 111;

    private String mSessionType;
    private ActivityConfirmBookingBinding mBinding;
    private UserProfileModel mSelectedCoachModel;
    private List<Facility> mAvailableFacilitiesList;
    private CreateBookingRequest mCreateBookingRequest;
    private String coachEmail;
    private String facilityAddress;
    private String sessionDateAndTime, sessionDateAndTimeUtc;

    @Override
    public String getActivityName() {
        return ConfirmBookingActivity.class.getName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelectedCoachModel = UserProfileInstance.getInstance().getUserProfileModel();
        mAvailableFacilitiesList = mSelectedCoachModel.getCoachLocation();
        mCreateBookingRequest = new CreateBookingRequest();

        initUI();
    }

    private void initUI() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_booking);

        mBinding.topHeader.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.topHeader.tvTitle.setText(getString(R.string.create_booking));
        mBinding.topHeader.ivBack.setVisibility(GONE);

        mBinding.buttonConfirmBooking.setOnClickListener(this);

        setSpinners();

        mBinding.spinnerSessionType.setSelection(-1);
        mBinding.spinnerGroupSize.setSelection(-1);
        mBinding.spinnerFacility.setSelection(-1);

        Bundle bundle = getIntent().getExtras();

        //set date and time
        mBinding.tvDateAndTime.setText("DATE AND TIME");
        mBinding.clDate.tvLabel.setText("DATE");
        mBinding.clDate.ivSpinner.setVisibility(GONE);
        mBinding.clTime.tvLabel.setText("TIME");
        mBinding.clTime.ivSpinner.setVisibility(GONE);

        if (bundle != null) {
            String originalDate = bundle.getString(Constants.SELECTED_SLOT.SLOT_DATE);
            int originalTime = bundle.getInt(Constants.SELECTED_SLOT.SLOT_TIME);
            mSessionType = bundle.getString(Constants.BundleKey.SESSION);
            coachEmail = bundle.getString(Constants.BundleKey.COACH_EMAIL);

            mBinding.clDate.etValue.setText(Utils.getReadableDate(originalDate));
            mBinding.clTime.etValue.setText(Utils.getReadableTime(originalTime));

            sessionDateAndTime = originalDate + " " + Utils.getHourIn24HHMMSSFormat(originalTime);
            sessionDateAndTimeUtc = DateFormatUtils.convertLocalToUtc(sessionDateAndTime,
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);

            int slotId = bundle.getInt(Constants.SELECTED_SLOT.SLOT_ID);
            Log.d("slot_log", "Slot session date time - "
                    + " slotId : " + slotId
                    + " originalDate : " + originalDate
                    + " originalTime : " + originalTime
                    + " sessionDateAndTime : " + sessionDateAndTime
                    + " sessionDateAndTimeUtc : " + sessionDateAndTimeUtc);
            mCreateBookingRequest.setSessionDateTime(sessionDateAndTimeUtc);
            mCreateBookingRequest.setSlotId(slotId);

            mCreateBookingRequest.setSessionStartDateTime(sessionDateAndTimeUtc);

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(DateFormatUtils.getDateInLongFromLocal(sessionDateAndTime));
            cal.setTimeZone(TimeZone.getDefault());
            cal.add(Calendar.HOUR_OF_DAY, 1);
            Date newDateObj = cal.getTime();
            Log.d("slot_log", "Slot session end date and time - "
                    + " older sessionDateAndTime : " + sessionDateAndTime
                    + " cal.getTime() : " + cal.getTime()
                    + " newDateObj : " + newDateObj.getTime()
            );

            String sessionEndDateAndTime = DateFormatUtils.getDateFormatStringFromLong(newDateObj.getTime(),
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, false);
            String sessionEndDateAndTimeUtc = DateFormatUtils.convertLocalToUtc(sessionEndDateAndTime,
                    DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
            Log.d("slot_log", "sessionEndDateAndTime - new dateText - " + sessionEndDateAndTime
                + " sessionEndDateAndTimeUtc : " + sessionEndDateAndTimeUtc);

            mCreateBookingRequest.setSessionEndDateTime(sessionEndDateAndTimeUtc);

        }

        //set session type spinner
        ArrayAdapter<String> sessionTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.SESSIONS.SESSION_HANDS_ON.equals(mSessionType) ? getResources().getStringArray(R.array.session_type_hands) : Constants.SESSIONS.SESSION_INSPIRATIONAL.equals(mSessionType) ? getResources().getStringArray(R.array.session_type_inspirational) : getResources().getStringArray(R.array.session_type_both)) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(ContextCompat.getColor(ConfirmBookingActivity.this, R.color.hint_text_color));
                } else {
                    tv.setTextColor(ContextCompat.getColor(ConfirmBookingActivity.this, R.color.txt_clr_green));
                }
                return view;
            }
        };
        mBinding.spinnerSessionType.setAdapter(sessionTypeAdapter);
        sessionTypeAdapter.setDropDownViewResource(R.layout.item_booking_spinner);
        mBinding.spinnerSessionType.setOnItemSelectedListener(sessionTypeSelectedListener);
        mBinding.clSessionType.etValue.setOnClickListener(sessionTypeClickListener);
        mBinding.clSessionType.ivSpinner.setOnClickListener(sessionTypeClickListener);

        //set group size spinner
        ArrayAdapter<String> groupSizeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.booking_type_caps)) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(ContextCompat.getColor(ConfirmBookingActivity.this, R.color.hint_text_color));
                } else {
                    tv.setTextColor(ContextCompat.getColor(ConfirmBookingActivity.this, R.color.txt_clr_green));
                }
                return view;
            }
        };
        mBinding.spinnerGroupSize.setAdapter(groupSizeAdapter);
        groupSizeAdapter.setDropDownViewResource(R.layout.item_booking_spinner);
        mBinding.spinnerGroupSize.setOnItemSelectedListener(groupSizeSelectedListener);
        mBinding.clGroupSize.etValue.setOnClickListener(groupSizeClickListener);
        mBinding.clGroupSize.ivSpinner.setOnClickListener(groupSizeClickListener);

        //set session details
        mBinding.tvSession.setText("SESSION");
        mBinding.clSessionType.tvLabel.setText("SESSION TYPE");
        mBinding.clSessionType.etValue.setHint("SELECT SESSION TYPE");
        mBinding.clGroupSize.tvLabel.setText("GROUP SIZE");
        mBinding.clGroupSize.etValue.setHint("SELECT GROUP SIZE");

        //set group size spinner
        List<String> facilityNamesList = new ArrayList<>();
        facilityNamesList.add("SELECT FACILITY");
        for (Facility facility : mAvailableFacilitiesList)
            facilityNamesList.add(facility.getFacilityName());

        ArrayAdapter<String> facilitiesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, facilityNamesList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(ContextCompat.getColor(ConfirmBookingActivity.this, R.color.hint_text_color));
                } else {
                    tv.setTextColor(ContextCompat.getColor(ConfirmBookingActivity.this, R.color.txt_clr_green));
                }
                return view;
            }
        };
        mBinding.spinnerFacility.setAdapter(facilitiesAdapter);
        facilitiesAdapter.setDropDownViewResource(R.layout.item_booking_spinner);
        mBinding.spinnerFacility.setOnItemSelectedListener(facilitySelectedListener);
        mBinding.clFacility.etValue.setOnClickListener(facilityClickListener);
        mBinding.clFacility.ivSpinner.setOnClickListener(facilityClickListener);

        //set facility details
        mBinding.tvLocation.setText("LOCATION");
        mBinding.clFacility.tvLabel.setText("FACILITY");
        mBinding.clFacility.etValue.setHint("SELECT FACILITY");

        //set price
        mBinding.tvPrice.setText("PRICE");
        mBinding.clPrice.tvLabel.setText("TOTAL");
        mBinding.clPrice.ivSpinner.setVisibility(GONE);
        mBinding.priceGroup.setVisibility(GONE);
    }

    private void setSpinners() {
    }

    View.OnClickListener sessionTypeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mBinding.spinnerSessionType.performClick();
            mBinding.spinnerSessionType.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener groupSizeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mBinding.spinnerGroupSize.performClick();
            mBinding.spinnerGroupSize.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener facilityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mBinding.spinnerFacility.performClick();
            mBinding.spinnerFacility.setVisibility(View.VISIBLE);
        }
    };

    AdapterView.OnItemSelectedListener sessionTypeSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                return;
            }
            mBinding.clSessionType.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
            mBinding.clSessionType.etValue.setText(String.valueOf(parent.getItemAtPosition(position)));
            int sessionType = (position == 1 ? Constants.SESSION_TYPE.INSPIRATIONAL : Constants.SESSION_TYPE.HANDS_ON);
            mCreateBookingRequest.setSessionType(sessionType);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            LogUtils.LOGI("TAG", "Nothing Selected");
        }
    };

    AdapterView.OnItemSelectedListener groupSizeSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0)
                return;
            String price = "0";
            int bookingType;
            if (position == 1) {

                if (mSelectedCoachModel.getOneOneBookingTypePrice() == null) {
                    showSnackbarFromTop(ConfirmBookingActivity.this, getString(R.string.group_size_unavailable));
                    return;
                }

                price = mSelectedCoachModel.getOneOneBookingTypePrice();
                bookingType = Constants.BOOKING_TYPE.ONE_ON_ONE;
            } else if (position == 2) {

                if (mSelectedCoachModel.getSmallBookingTypePrice() == null) {
                    showSnackbarFromTop(ConfirmBookingActivity.this, getString(R.string.group_size_unavailable));
                    return;
                }

                price = mSelectedCoachModel.getSmallBookingTypePrice();
                bookingType = Constants.BOOKING_TYPE.SMALL;
            } else {

                if (mSelectedCoachModel.getLargeBookingTypePrice() == null) {
                    showSnackbarFromTop(ConfirmBookingActivity.this, getString(R.string.group_size_unavailable));
                    return;
                }

                price = mSelectedCoachModel.getLargeBookingTypePrice();
                bookingType = Constants.BOOKING_TYPE.LARGE;
            }

            mBinding.clGroupSize.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
            mBinding.clGroupSize.etValue.setText(String.valueOf(parent.getItemAtPosition(position)));

            mBinding.clPrice.etValue.setText("$" + (String.valueOf(price)).replaceAll("\\.0*$", ""));
            mCreateBookingRequest.setPrice(Double.parseDouble(price));
            mCreateBookingRequest.setBookingType(bookingType);
            mBinding.priceGroup.setVisibility(VISIBLE);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    AdapterView.OnItemSelectedListener facilitySelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                return;
            }
            mBinding.clFacility.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
            mBinding.clFacility.etValue.setText(String.valueOf(parent.getItemAtPosition(position)));
            mCreateBookingRequest.setFacilityId(mAvailableFacilitiesList.get(position - 1).getFacilityId());

            facilityAddress = mAvailableFacilitiesList.get(position - 1).getFacilityAddress();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_confirm_booking:
                if (isValidBooking()) {
                    boolean isLogin = PreferenceUtil.getIsLogin();
                    if (isLogin) {
                        UserModel userModel = PreferenceUtil.getUserModel();
                        if(userModel != null) {
                            if(!TextUtils.isEmpty(userModel.getEmail())
                                    && !TextUtils.isEmpty(coachEmail)
                                    && userModel.getEmail().equals(coachEmail)) {
                                showSnackbarFromTop(ConfirmBookingActivity.this,
                                        getResources().getString(R.string.booking_with_logged_in_account));
                                return;
                            }
                            if(Utils.isNetworkAvailable()) {
                                launchAddPaymentScreen();
                                //confirmBoooking();
                            } else {
                                showSnackbarFromTop(ConfirmBookingActivity.this, getResources().getString(R.string.no_internet));
                            }
                        }
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, true);
                        Navigator.getInstance().navigateToActivityForResultWithData(ConfirmBookingActivity.this,
                                LoginActivity.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_LOGIN_STATUS);
                    }
                } else
                    showSnackbarFromTop(this, "Please select data in all fields");
                break;
        }
    }

    private void launchAddPaymentScreen() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BundleKey.BOOKING_SESSION_DATE, sessionDateAndTimeUtc);
        bundle.putString(Constants.BundleKey.BOOKING_FACILITY_ADDRESS, facilityAddress);
        bundle.putParcelable(Constants.BundleKey.BOOKING_REQUEST_DATA, mCreateBookingRequest);
        bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
        //Navigator.getInstance().navigateToActivityWithData(ConfirmBookingActivity.this, AddPaymentMethodActivity.class, bundle);
        Navigator.getInstance().navigateToActivityWithData(ConfirmBookingActivity.this, ManageCardListActivity.class, bundle);

        finish();
    }

    private boolean isValidBooking() {
        mCreateBookingRequest.setCoachId(mSelectedCoachModel.getUserId());

        if (mSelectedCoachModel.getSportDetail() == null) {
            showSnackBar("Invalid sport");
            return false;
        }
        mCreateBookingRequest.setSportId(mSelectedCoachModel.getSportDetail().getId());

        return mCreateBookingRequest.isValidBookingRequest();
    }

    private void confirmBoooking() {
        processToShowDialog();

        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.createLearnerBooking(mCreateBookingRequest).enqueue(new BaseCallback<CreateBookingBaseResponse>(this) {
            @Override
            public void onSuccess(CreateBookingBaseResponse response) {
                //Alert.showAlertDialog(ConfirmBookingActivity.this, getString(R.string.booking_confirmed), "You have successfully booked your session with " + mSelectedCoachModel.getName(), getString(R.string.okay), ConfirmBookingActivity.this);

                String msg="You have successfully booked your session with " + mSelectedCoachModel.getName() + "."+"\n\n" +
                        "Would you like to add some more fixtures so "+ mSelectedCoachModel.getName()+" can send you tips on matchday?";

                Alert.showAlertDialogWithBottomButton(ConfirmBookingActivity.this,getString(R.string.success),msg,getString(R.string.do_this_later),getString(R.string.yes_please),
                        ConfirmBookingActivity.this,ConfirmBookingActivity.this);

//                if (ContextCompat.checkSelfPermission(ConfirmBookingActivity.this, Manifest.permission.WRITE_CALENDAR)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    askCalendarPermission();
//                } else {
//                    addBookingEventToCalendar();
//                }

                if(response != null && response.getResult() != null) {
                    calendarSyncPost(response.getResult());
                }

            }

            @Override
            public void onFail(Call<CreateBookingBaseResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                hideProgressBar();
            }
        });

    }

    private void calendarSyncPost(CreateBookingResponse createBookingResponse) {
        if(createBookingResponse != null) {
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
            CalendarSyncPostRequest calendarSyncPostRequest = new CalendarSyncPostRequest();
            calendarSyncPostRequest.setSessionId(createBookingResponse.getSessionId());
            calendarSyncPostRequest.setCoachId(createBookingResponse.getCoachId());
            calendarSyncPostRequest.setLearnerId(createBookingResponse.getLearnerId());
            calendarSyncPostRequest.setSessionDate(createBookingResponse.getSessionDate());
            authWebServices.calendarSyncPost(calendarSyncPostRequest).enqueue(new BaseCallback<BaseResponse>(this) {
                @Override
                public void onSuccess(BaseResponse response) {
                    if(response != null && response.getStatus() == 1) {
                        if (ContextCompat.checkSelfPermission(ConfirmBookingActivity.this, Manifest.permission.WRITE_CALENDAR)
                                != PackageManager.PERMISSION_GRANTED) {
                            askCalendarPermission();
                        } else {
                            addBookingEventToCalendar();
                        }
                    }
                }

                @Override
                public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                    //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                    hideProgressBar();
                }
            });
        }
    }

    @Override
    public void onOkay() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
        if(!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearTop(ConfirmBookingActivity.this, CreateFixtureListActivity.class, bundle);
        } else {
            Navigator.getInstance().navigateToActivityWithData(ConfirmBookingActivity.this, CreateFixtureListActivity.class, bundle);
        }
        finish();
    }

    @Override
    public void onCancel() {
        if(!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearTop(ConfirmBookingActivity.this,
                    LearnerHomeActivity.class, null);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }*/

        if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_LOGIN_STATUS) {
            if (resultCode == RESULT_OK) {
                boolean isLogin = PreferenceUtil.getIsLogin();
                if (isLogin) {
                    UserModel userModel = PreferenceUtil.getUserModel();
                    if(userModel != null) {
                        if(!TextUtils.isEmpty(userModel.getEmail())
                                && !TextUtils.isEmpty(coachEmail)
                                && userModel.getEmail().equals(coachEmail)) {
                            showSnackbarFromTop(ConfirmBookingActivity.this,
                                    getResources().getString(R.string.booking_with_logged_in_account));
                            return;
                        }
                        if(Utils.isNetworkAvailable()) {
                            launchAddPaymentScreen();
                            //confirmBoooking();
                        } else {
                            showSnackbarFromTop(ConfirmBookingActivity.this, getResources().getString(R.string.no_internet));
                        }
                    }
                } else {
                    showSnackBar(getString(R.string.pls_login));
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        boolean isLogin = PreferenceUtil.getIsLogin();
        if (isLogin) {
            UserModel userModel = PreferenceUtil.getUserModel();
            if(userModel != null) {
                if(!TextUtils.isEmpty(userModel.getEmail())
                        && !TextUtils.isEmpty(coachEmail)
                        && userModel.getEmail().equals(coachEmail)) {
                    // Navigate to clear stack
                    if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                        Navigator.getInstance().navigateToActivityWithClearStack(ConfirmBookingActivity.this, WelcomeActivity.class);
                    } else {
                        Navigator.getInstance().navigateToActivityWithClearStack(ConfirmBookingActivity.this, WelcomeActivity.class);
                    }
                    try {
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            showToast(getString(R.string.pls_login));
        }
        super.onBackPressed();
    }

    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addBookingEventToCalendar();
                } else {
                    Toast.makeText(this, "Permission not granted.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(ConfirmBookingActivity.this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void addBookingEventToCalendar() {
        String sportName = "", coachName = "", sessionDateTime = "";
        long startTime = 0;

        if(mSelectedCoachModel == null) {
            return;
        }
        if(TextUtils.isEmpty(sessionDateAndTimeUtc)) {
            return;
        }
        if(TextUtils.isEmpty(facilityAddress)) {
            return;
        }

        if(mSelectedCoachModel != null) {
            sportName = mSelectedCoachModel.getSportDetail().getName();
            coachName = mSelectedCoachModel.getName();
        }
        if(sessionDateAndTimeUtc != null) {
            startTime = Utils.getTimeInMillis(sessionDateAndTimeUtc);
            sessionDateTime = sessionDateAndTimeUtc;
        }
        if(!TextUtils.isEmpty(sportName) && !TextUtils.isEmpty(coachName)
                && !TextUtils.isEmpty(facilityAddress) && startTime != 0) {
//            Utils.addEventToCalendar(ConfirmBookingActivity.this,
//                    sportName + " with " + coachName, facilityAddress, startTime, true);

            CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
            calendarSyncResponse.setCoachName(coachName);
            calendarSyncResponse.setFacility(facilityAddress);
            calendarSyncResponse.setLearnerName("");
            calendarSyncResponse.setSessionDate(sessionDateTime);
            calendarSyncResponse.setSportName(sportName);
            calendarSyncResponse.setTitle("StarFinda Coaching Session with " + coachName + " for " + sportName);
            addBookingEventToCalendar(calendarSyncResponse);
        }
    }

    private void addBookingEventToCalendar(CalendarSyncResponse calendarSyncResponse) {
        CalendarSyncHelper.getInstance().addCalendarEvent(ConfirmBookingActivity.this, calendarSyncResponse);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.LEARNER);
    }

}
