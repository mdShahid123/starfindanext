package com.sportsstars.ui.search;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sportsstars.R;
import com.sportsstars.databinding.ItemCoachResultBinding;
import com.sportsstars.interfaces.CoachBySportItemClick;
import com.sportsstars.model.Coach;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abhaykant on 21/01/18.
 */

public class CoachListBySportAdapter extends RecyclerView.Adapter<CoachListBySportAdapter.CoachListViewHolder> {
    private static final String TAG = LogUtils.makeLogTag(CoachListBySportAdapter.class);

    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ArrayList<Coach> mCoachList;
    private CoachBySportItemClick mClickListener;
    private ImageLoader imageLoader;
    private DisplayImageOptions op;


    public void setSessionList(ArrayList<Coach> arrayList, CoachBySportItemClick click) {
        this.mCoachList = arrayList;
        mClickListener = click;
        notifyDataSetChanged();
    }


    public CoachListBySportAdapter(Activity mActivity, ArrayList<Coach> sessionList,
                                   CoachBySportItemClick coachBySportItemClick) {
        this.mActivity = mActivity;
        this.mCoachList = sessionList;
        this.mClickListener = coachBySportItemClick;
        mInflator = LayoutInflater.from(mActivity);

        op = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_user_circle)
                .showImageForEmptyUri(R.drawable.ic_user_circle)
                .showImageOnFail(R.drawable.ic_user_circle)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new RoundedBitmapDisplayer(20))
                .build();
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public CoachListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemCoachResultBinding searchListBinding = DataBindingUtil.inflate(mInflator, R.layout.item_coach_result, parent, false);
        return new CoachListViewHolder(searchListBinding);
    }

    @Override
    public void onBindViewHolder(final CoachListViewHolder holder, int position) {
        LogUtils.LOGD(TAG, "CountAre  SearchView>" + mCoachList);

        if (mCoachList != null && mCoachList.size() > 0) {
            final Coach coach = mCoachList.get(position);
            holder.getBinding().tvCoachName.setText(coach.getName());

            if (coach.getCoachType() == Constants.COACH_TYPE.PRIVATE) {
                holder.binding.ivCoachType.setBackgroundResource(R.drawable.ic_private_coach);
            } else {
                holder.binding.ivCoachType.setBackgroundResource(R.drawable.ic_star);
            }

            int price = (int) coach.getOneOneBookingTypePrice();
            holder.getBinding().tvCoachCharges.setText("$"+price);
            holder.binding.tvCoachInfo.setText(coach.getDescription());

            Log.d("image_log", "Coach list - onBindViewHolder - "
                    + " position : " + position
                    + " coach id : " + coach.getId()
                    + " coach name  : " + coach.getName()
                    + " coach image url : " + coach.getProfileImage()
            );

            holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);

//            if (!TextUtils.isEmpty(coach.getProfileImage())) {
//                //Picasso.with(mActivity).invalidate(coach.getProfileImage());
//
//                Picasso.with(mActivity)
//                        .load(coach.getProfileImage())
//                        .placeholder(R.drawable.ic_user_circle)
//                        .error(R.drawable.ic_user_circle)
//                        //.memoryPolicy(MemoryPolicy.NO_CACHE)
//                        //.networkPolicy(NetworkPolicy.NO_CACHE)
//                        .into((holder.getBinding().ivProfilePic), new Callback() {
//                            @Override
//                            public void onSuccess() {
//
//                            }
//
//                            @Override
//                            public void onError() {
//                                holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);
//                            }
//                        });
//            } else {
//                //Picasso.with(mActivity).invalidate(coach.getProfileImage());
//                holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);
//            }

            if(coach != null && coach.getName() != null && !TextUtils.isEmpty(coach.getName())
                    && coach.getName().equals("Damian Carroll")) {
                if(TextUtils.isEmpty(coach.getProfileImage())) {
                    holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);
                }
            }

            imageLoader.displayImage(coach.getProfileImage(), holder.getBinding().ivProfilePic, op, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String url, View view) {
                    Log.d("image_log", "Coach list - onLoadingStarted - "
                            + " position : " + position
                            + " coach id : " + coach.getId()
                            + " coach name  : " + coach.getName()
                            + " coach image url : " + url
                    );
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    Log.d("image_log", "Coach list - onLoadingComplete - "
                            + " position : " + position
                            + " coach id : " + coach.getId()
                            + " coach name  : " + coach.getName()
                            + " coach image url : " + imageUri
                    );
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    super.onLoadingFailed(imageUri, view, failReason);
                    Log.d("image_log", "Coach list - onLoadingFailed - "
                            + " position : " + position
                            + " coach id : " + coach.getId()
                            + " coach name  : " + coach.getName()
                            + " coach image url : " + imageUri
                            + " failReason : " + failReason.getCause().toString()
                    );
                    holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return mCoachList != null ? mCoachList.size() : 0;
    }

    public void clearItems() {
        if (mCoachList != null && mCoachList.size() > 0) {
            mCoachList.clear();
            notifyDataSetChanged();
        }
    }


    class CoachListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemCoachResultBinding binding;

        private CoachListViewHolder(ItemCoachResultBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        public ItemCoachResultBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
            LogUtils.LOGD(TAG, "clicked");
            mClickListener.onItemClick(binding, mCoachList.get(getAdapterPosition()),
                    getAdapterPosition
                            ());
        }
    }

}



