package com.sportsstars.ui.search;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoachBinding;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

/**
 * Created by Ram on 14/01/18.
 */

public class SearchCoachActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(SearchCoachActivity.class);
    private static final int BY_SPORT = 1;
    private static final int BY_NAME = 2;
    private ActivitySearchCoachBinding mBinder;
//    private ImageAdapter mImageAdapter;
    private LayoutInflater mLayoutInflater;
    private int mSelectedTab;
    public boolean isCoachSearch;
    public int isPanel;

    @Override
    public String getActivityName() {
        return SearchCoachActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_coach);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_COACH_SEARCH)) {
                isCoachSearch = bundle.getBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                isPanel = bundle.getInt(Constants.BundleKey.IS_PANEL, 0);
                if(isCoachSearch) {
                    mBinder.headerId.tvTitle.setText(R.string.search_for_coach);
                } else {
                    mBinder.headerId.tvTitle.setText(R.string.search_for_speaker);
                }
            } else {
                mBinder.headerId.tvTitle.setText(R.string.search_for_coach);
            }
        } else {
            mBinder.headerId.tvTitle.setText(R.string.search_for_coach);
        }

        init();

        setSearchType(BY_SPORT);

        Utils.setTransparentTheme(this);
    }

    private void init() {
        mBinder.headerId.ivBack.setOnClickListener(this);
//        mBinder.layoutTop.tvByName.setOnClickListener(this);
//        mBinder.layoutTop.tvBySport.setOnClickListener(this);
//        mBinder.tvSearchBy.setOnClickListener(this);

        mLayoutInflater = LayoutInflater.from(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_by_name:
                if(mSelectedTab==BY_NAME){
                    return;
                }
                searchByName();
                break;

            case R.id.tv_by_sport:
                if(mSelectedTab==BY_SPORT){
                    return;
                }
                searchBySport();
                break;

            case R.id.tv_search_by:
                navigateToSearchCoach2();
                break;
        }
    }

    private void navigateToSearchCoach2() {
        Navigator.getInstance().navigateToActivity(this, SearchCoachActivityNext.class);
    }

    private void searchByName() {
        mBinder.layoutTop.tvByName.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_green_8_2));
        mBinder.layoutTop.tvByName.setTextColor(ContextCompat.getColor(this, R.color.black));

        mBinder.layoutTop.tvBySport.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_border_green_3));
        mBinder.layoutTop.tvBySport.setTextColor(ContextCompat.getColor(this, R.color.white_color));

//        mBinder.tvSearchBy.setText(getString(R.string.search_by_sport));

        setSearchType(BY_NAME);

    }

    private void searchBySport() {
        mBinder.layoutTop.tvBySport.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_green_8_2));
        mBinder.layoutTop.tvBySport.setTextColor(ContextCompat.getColor(this, R.color.black));

        mBinder.layoutTop.tvByName.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_border_green_3));
        mBinder.layoutTop.tvByName.setTextColor(ContextCompat.getColor(this, R.color.white_color));

//        mBinder.tvSearchBy.setText(getString(R.string.search_by_coach));

        setSearchType(BY_SPORT);
    }

    private void setSearchType(int searchType) {
        mSelectedTab=searchType;

        switch (searchType) {
            case BY_SPORT:
                pushFragment(Constants.FRAGMENTS.SEARCH_BY_SPORT_FRAGMENT, null, R.id.frame_search_by,
                        false);
                break;

            case BY_NAME:
                pushFragment(Constants.FRAGMENTS.SEARCH_BY_NAME_FRAGMENT, null, R.id
                        .frame_search_by, false);
                break;

        }
    }

}
