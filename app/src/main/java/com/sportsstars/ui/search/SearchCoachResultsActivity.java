package com.sportsstars.ui.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoachResultBinding;
import com.sportsstars.databinding.ItemCoachResultBinding;
import com.sportsstars.interfaces.CoachBySportItemClick;
import com.sportsstars.model.Coach;
import com.sportsstars.model.FilterData;
import com.sportsstars.model.FilterState;
import com.sportsstars.model.SearchFilter;
import com.sportsstars.model.Sports;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.SearchCoachBySportRequest;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.auth.SearchCoachBySportData;
import com.sportsstars.network.response.auth.SearchCoachBySportResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.ViewProfileActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by Ram on 21/01/18.
 */

public class SearchCoachResultsActivity extends BaseActivity implements View.OnClickListener, CoachBySportItemClick {

    private static final String TAG = LogUtils.makeLogTag(SearchCoachResultsActivity.class);
    private static final int REQUEST_CODE_FILTER = 110;
    private ActivitySearchCoachResultBinding mBinder;
    private FilterData mFilterData;
    private SearchFilter mSearchFilter;
    private FilterState mFilterState;
    private Sports mSport;
    private double mLatitude, mLongitude;
    private boolean mResultFound;
    private boolean mRepeatRequest;
    private int mRequestCount;
    private Coach mCoach;
    private int mCurrentPage = 1;
    private int lastPage = 1;
    private boolean isLoading = false;
    LinearLayoutManager mLayoutManager = null;
    ArrayList<Coach> coaches = new ArrayList<>();
    Boolean isShowLoader = true;
    CoachListBySportAdapter mAdapter;

    @Override
    public String getActivityName() {
        return SearchCoachResultsActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_coach_result);
        mBinder.ivBack.setOnClickListener(this);
        mBinder.ivFilter.setOnClickListener(this);

        mBinder.tvSportName.setOnClickListener(this);
        mBinder.tvCoachType.setOnClickListener(this);
        mBinder.tvAddress.setOnClickListener(this);

        if (getIntent() != null) {
            Intent intent = getIntent();

            mCoach = intent.getParcelableExtra(Constants.EXTRA_COACH);
            mSport = intent.getParcelableExtra(Constants.EXTRA_SPORT);

            mLatitude = intent.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0);
            mLongitude = intent.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0);
            String address = intent.getStringExtra(Constants.EXTRA_ADDRESS);

            if (mSport != null) {
                mBinder.tvSportName.setText(mSport.getName());
            }

            if (mCoach != null) {
                String coachType = mCoach.getCoachType() == Constants.COACH_TYPE.STAR ? "Star" : "Private";
                mBinder.tvCoachType.setText(coachType);
            }
            mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mBinder.tvAddress.setText(address);
            setAdapter();
            if (mSport != null && mCoach != null) {
//                ArrayList<Integer> coachType = new ArrayList<>();
//                coachType.add(coach.getCoachType());
                searchCoachBySport(mCoach.getCoachType());
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.iv_filter:

                // Commented no result found check on discussion, filter icon is always enabled now

                //if (!(mRequestCount == 1 && !mResultFound)) {
                Intent intent = new Intent(this, CoachFilterActivity.class);
                intent.putExtra("filter", mFilterData);
                intent.putExtra("coachType", mCoach.getCoachType());
                intent.putExtra("filterState", mFilterState);
                startActivityForResult(intent, REQUEST_CODE_FILTER);
                //}

                break;

            case R.id.tv_sport_name:
                setResult(RESULT_OK);
                finish();
                break;

            case R.id.tv_coach_type:
                finish();
                break;

            case R.id.tv_address:
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_FILTER:
                switch (resultCode) {
                    case RESULT_OK:
                        mSearchFilter = data.getParcelableExtra("filter");
                        mFilterState = data.getParcelableExtra("filterState");

//                        int coachType = data.getIntExtra("coachType", 0);
//                        applyFilter(coachType);
                        applyFilter(mCoach.getCoachType());
                        break;

                    case RESULT_CANCELED:
                        break;
                }
        }
    }

    private void applyFilter(int coachType) {
        mCurrentPage = 1;
        mRequestCount = 0;
        coaches.clear();
        mAdapter.notifyDataSetChanged();
        searchCoachBySport(coachType);
    }

    @Override
    public void onItemClick(ItemCoachResultBinding itemCoachResultBinding, Coach coach, int pos) {
        LogUtils.LOGD(TAG, "coach: " + coach.getName());
        getThisCoachProfile(coach.getId());
    }

    private void searchCoachBySport(int coachType) {
//        showProgressBar();
        Log.d("Initial", "Current Page " + mCurrentPage + " LastPage " + lastPage);
        isLoading = true;
        mResultFound = false;
        if (isShowLoader) {
            mBinder.layoutLoader.setVisibility(View.VISIBLE);
            isShowLoader = false;
        }

        SearchCoachBySportRequest request = new SearchCoachBySportRequest();
        request.setSportId(mSport.getId());
        request.setCoachType(coachType);
        request.setLatitude(mLatitude);
        request.setLongitude(mLongitude);
        request.setFilters(mSearchFilter);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.searchCoachBySport(mCurrentPage, 10, request).enqueue(new BaseCallback<SearchCoachBySportResponse>(this) {
            @Override
            public void onSuccess(SearchCoachBySportResponse response) {
                isLoading = false;
                mBinder.layoutLoader.setVisibility(View.GONE);
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mCurrentPage++;
                        lastPage = Utils.DivideByFifteenRoundingUp(response.getResult().getTotal() / 10f);
                        if (response.getResult() != null && response.getResult().getCoaches() != null) {
                            ArrayList<Coach> _coaches = response.getResult().getCoaches();

                            mRequestCount++;

                            if (!mRepeatRequest) {
                                mRepeatRequest = true;
                                initFilterData(response.getResult());
                            }
                            coaches.addAll(_coaches);
                            mAdapter.notifyDataSetChanged();

                            //setAdapter(coaches);

                            if (coaches.size() > 0) {
                                mResultFound = true;
                                mBinder.tvNoResults.setVisibility(View.GONE);
                            } else {
                                if (mRequestCount == 1)
                                    mBinder.tvNoResults.setVisibility(View.VISIBLE);
                            }
//                        for (Coach item : coaches) {
//                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getProfileImage());
//                        }

                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SearchCoachBySportResponse> call, BaseResponse baseResponse) {
                isLoading = false;
                mBinder.layoutLoader.setVisibility(View.GONE);
                hideProgressBar();
            }
        });
    }


    private void initFilterData(SearchCoachBySportData searchCoachBySportData) {
        mFilterData = new FilterData();
        mFilterData.setMinAge(searchCoachBySportData.getMinAge());
        mFilterData.setMaxAge(searchCoachBySportData.getMaxAge());
        mFilterData.setMinDistance(searchCoachBySportData.getMinDistance());
        mFilterData.setMaxDistance(searchCoachBySportData.getMaxDistance());
        mFilterData.setMinPrice(searchCoachBySportData.getMinPrice());
        mFilterData.setMaxPrice(searchCoachBySportData.getMaxPrice());
        mFilterData.setTeams(searchCoachBySportData.getTeams());
    }

    private void setAdapter() {

        mAdapter = new CoachListBySportAdapter(this, coaches, this);
        mBinder.recCoachList.setLayoutManager(mLayoutManager);
        mBinder.recCoachList.setItemAnimator(new DefaultItemAnimator());
        mBinder.recCoachList.setAdapter(mAdapter);
        mBinder.recCoachList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= 10) {
                        //loadMoreItems();
                        searchCoachBySport(mCoach.getCoachType());
                        Log.d("Paging", "Add More items");
                    }
                }
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    private void getThisCoachProfile(int id) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfileDetail(id).enqueue(new BaseCallback<GetProfileResponse>(SearchCoachResultsActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        UserProfileInstance.getInstance().setUserProfileModel(response.getResult());
                        Navigator.getInstance().navigateToActivity(SearchCoachResultsActivity.this, ViewProfileActivity.class);
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SearchCoachResultsActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SearchCoachResultsActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
