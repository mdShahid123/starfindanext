package com.sportsstars.ui.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentSearchCoachBySport2Binding;
import com.sportsstars.databinding.ItemFlowSportsBinding;
import com.sportsstars.databinding.ItemSportsGridBinding;
import com.sportsstars.model.Sports;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SportsListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.speaker.search.SearchSpeakerResultActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by ramkumar on 19/01/18.
 */

public class FragmentSearchCoachBySportNext extends BaseFragment implements View.OnClickListener, TextWatcher, Runnable {
    private static final String TAG = LogUtils.makeLogTag(FragmentSearchCoachBySportNext.class);
    private WeakReference<SearchCoachActivityNext> mActivity;
    private FragmentSearchCoachBySport2Binding mBinder;
    private ArrayList<Sports> mSportsList;
    private ArrayList<ItemFlowSportsBinding> mItemFlowBindingList;
    private Handler mHandler;
    private Handler mHandler2;
    private String mSearchText;
    private int mSelectedIndex = -1;
    private boolean isCoachSearch;

    public static FragmentSearchCoachBySportNext newInstance() {
        FragmentSearchCoachBySportNext fragment = new FragmentSearchCoachBySportNext();
        Bundle args = new Bundle();
//        args.putString(AppConstants.ARG_PARAM2, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((SearchCoachActivityNext) getActivity());

        if (getArguments() != null) {
//            mUserId = getArguments().getString(AppConstants.ARG_PARAM2);
        }

        mHandler = new Handler(Looper.getMainLooper());
        mHandler2 = new Handler(Looper.getMainLooper());
        mItemFlowBindingList = new ArrayList<>();

        mHandler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                getSportsList("");
            }
        }, 500);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_search_coach_by_sport2,
                container, false);
//        getSportsList("");

        mBinder.etSearch.addTextChangedListener(this);
        mBinder.tvConfirm.setOnClickListener(this);

        return mBinder.getRoot();
    }

    @Override
    public String getFragmentName() {
        return null;
    }

    @Override
    public void run() {
        getSportsList(mSearchText);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String searchText = charSequence.toString().trim();

        mHandler.removeCallbacks(this);
        mHandler.postDelayed(this, 500);
//        getSportsList(searchText);
        mSearchText = searchText;
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void getSportsList(String searchText) {
//        showProgressBar();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName(searchText);
        getSportsRequest.setPage(1);
        getSportsRequest.setPageSize(Constants.SPORTS_PAGE_SIZE);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>
                (mActivity.get()) {
            @Override
            public void onSuccess(SportsListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        mSportsList = response.getResult().getSportsList();

                        mItemFlowBindingList.clear();
                        mBinder.tvNoResults.setVisibility(View.GONE);
                        mBinder.flowLayoutSports.removeAllViews();

                        if (mSportsList.size() == 0) {
                            mBinder.tvNoResults.setVisibility(View.VISIBLE);
                            return;
                        }

                        for (Sports item : mSportsList) {
                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getReferenceImageUrl());
                            addTitleToLayout(item);
                        }
                    }
                } else {
                    mBinder.tvNoResults.setVisibility(View.VISIBLE);
                    mBinder.flowLayoutSports.removeAllViews();
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {
                try {
                    mBinder.tvNoResults.setVisibility(View.VISIBLE);
                    mBinder.flowLayoutSports.removeAllViews();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if(mActivity != null) {
                        mActivity.get().hideProgressBar();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addTitleToLayout(final Sports sport) {
        String text = sport.getName();
        int pos = 0;

        for (int i = 0; i < mSportsList.size(); i++) {
            if (sport.getName().equals(mSportsList.get(i).getName())) {
                pos = i;
                break;
            }
        }

        final ItemFlowSportsBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from
                (mBinder.flowLayoutSports.getContext())
                .inflate(R.layout.item_flow_sports, mBinder.flowLayoutSports, false));

        flowBinding.flowChild.setText(text);
        flowBinding.flowChild.setTag(sport);
        mItemFlowBindingList.add(flowBinding);

        /*final Drawable imgMinus = getResources().getDrawable(R.drawable.ic_minus_green);
        final Drawable imgPlus = getResources().getDrawable(R.drawable.ic_plus_green);

        final int finalPos = pos;
        if (mSportsList.get(finalPos).isSelected()) {
            flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgMinus, null);

        } else {
            flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPlus, null);

        }*/

        final int finalPos = pos;

        flowBinding.flowChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < mSportsList.size(); i++) {
                    mSportsList.get(i).setSelected(false);
                    mItemFlowBindingList.get(i).flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                    mItemFlowBindingList.get(i).flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                }

//                mSportsList.get(finalPos).setSelected(true);
                ((Sports) flowBinding.flowChild.getTag()).setSelected(true);

                flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                Bundle bundle = getArguments();
                if(bundle != null) {
                    if(bundle.getBoolean(Constants.BundleKey.FROM_ADD_TIP, false)) {
                        setResultDataForTip((Sports) flowBinding.flowChild.getTag());
                    } else {
                        setResultData((Sports) flowBinding.flowChild.getTag());
                    }
                } else {
                    setResultData((Sports) flowBinding.flowChild.getTag());
                }

            }
        });

        mBinder.flowLayoutSports.addView(flowBinding.getRoot());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_confirm:

                Sports sports = null;

                for (int index = 0; index < mItemFlowBindingList.size(); index++) {
                    Sports item = (Sports) mItemFlowBindingList.get(index).flowChild.getTag();

                    if (item.isSelected()) {
                        sports = item;
                        break;
                    }
                }

                if (sports == null) {
                    showToast("Please select a sport to proceed");
                    return;
                }
                setResultData(sports);
                break;


        }
    }

    private void setResultData(Sports sports) {
        Bundle bundle = getArguments();
        Intent intent = null;
        if(bundle != null && bundle.getBoolean(Constants.BundleKey.IS_COACH_SEARCH, false)) {
            intent = new Intent(mActivity.get(), SearchCoachTypeActivity.class);
        } else {
            intent = new Intent(mActivity.get(), SearchSpeakerResultActivity.class);
            if(bundle != null) {
                intent.putExtra(Constants.BundleKey.IS_PANEL, bundle.getInt(Constants.BundleKey.IS_PANEL, 0));
            }
        }
        intent.putExtra(Constants.EXTRA_SPORT, sports);
        if (bundle != null) {
            if (bundle.getBoolean(Constants.FROM_COACH_INFO, false)
                    || bundle.getBoolean(Constants.FROM_LEARNER_PROFILE, false)) {
                Activity activity = getActivity();
                if (activity != null) {
                    intent.putExtra(Constants.BundleKey.INDEX, bundle.getInt(Constants.BundleKey.INDEX, -1));
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                    return;
                }
            }
        }
        startActivity(intent);
    }

    private void setResultDataForTip(Sports sports) {
        Bundle bundle = getArguments();
        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_SPORT, sports);
        if (bundle != null) {
            if (bundle.getBoolean(Constants.BundleKey.FROM_ADD_TIP, false)) {
                Activity activity = getActivity();
                if (activity != null) {
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                }
            }
        }
    }

    /*private void searchCoachBySport() {
        showProgressBar();

        SearchFilter searchFilter = new SearchFilter();
//        searchFilter.setAgeMin(1);
//        searchFilter.setAgeMax(80);
//        searchFilter.setPriceMin(1);
//        searchFilter.setPriceMax(200);
//        searchFilter.setDistanceMin(1);
//        searchFilter.setDistanceMax(200);

        SearchCoachBySportRequest request = new SearchCoachBySportRequest();
        request.setSportId(1);
        request.setCoachType(1);
        request.setFilters(searchFilter);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.searchCoachBySport(request).enqueue(new BaseCallback<SearchCoachBySportResponse>
                (mActivity.get()) {
            @Override
            public void onSuccess(SearchCoachBySportResponse response) {

                if (response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getCoaches() != null) {
                        ArrayList<Coach> coaches = response.getResult().getCoaches();
//
                        mImageAdapter = new ImageAdapter(getContext(),
                                coaches);
//
                        mBinder.gridview.setAdapter(mImageAdapter);
//
                        for (Coach item : coaches) {
                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getProfileImage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SearchCoachBySportResponse> call, BaseResponse baseResponse) {

            }
        });
    }*/

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private ArrayList<Sports> sports;

        public ImageAdapter(Context c, ArrayList<Sports> sports) {
            mContext = c;
            this.sports = sports;
        }

        public int getCount() {
            return sports.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            ItemSportsGridBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R
                    .layout.item_sports_grid, parent, false);


            if (convertView == null) {
                // if it's not recycled, initialize some attributes


                binding.tvSport.setText(sports.get(position).getName());
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_send_active);
                binding.ivSportLogo.setImageBitmap(bitmap);

                bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_coach);
                binding.ivSportCover.setImageBitmap(bitmap);

                binding.tvSport.setText(sports.get(position).getName());

//                imageView = new ImageView(mContext);
//                imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
//                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                imageView.setPadding(8, 8, 8, 8);
            } else {
                view = convertView;
            }

//            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.page_1);
//            binding.ivSportLogo.setImageBitmap(bitmap);
//            imageView.setImageResource(sports.get(position).getReferenceImageUrl());
//            return imageView;
            return binding.getRoot();
        }
    }
}
