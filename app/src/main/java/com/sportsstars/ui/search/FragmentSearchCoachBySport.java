package com.sportsstars.ui.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentSearchCoachBySportBinding;
import com.sportsstars.model.Sports;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SportsListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.speaker.search.SearchSpeakerResultActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by ramkumar on 19/01/18.
 */

public class FragmentSearchCoachBySport extends BaseFragment implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(FragmentSearchCoachBySport.class);
    private WeakReference<SearchCoachActivity> mActivity;
    private FragmentSearchCoachBySportBinding mBinder;
    private ImageAdapter mImageAdapter;
    private ArrayList<Sports> mSportsList;

    public static FragmentSearchCoachBySport newInstance() {
        FragmentSearchCoachBySport fragment = new FragmentSearchCoachBySport();
        Bundle args = new Bundle();
//        args.putString(AppConstants.ARG_PARAM2, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mActivity = (BaseActivity) getActivity();
        mActivity = new WeakReference<>((SearchCoachActivity) getActivity());

        if (getArguments() != null) {
//            mUserId = getArguments().getString(AppConstants.ARG_PARAM2);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_search_coach_by_sport, container, false);
        mBinder.tvSearchBy.setOnClickListener(this);
        mBinder.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if(mActivity.get().isCoachSearch) {
                    Intent intent = new Intent(mActivity.get(), SearchCoachTypeActivity.class);
                    intent.putExtra(Constants.EXTRA_SPORT, mSportsList.get(position));
                    intent.putExtra(Constants.BundleKey.IS_COACH_SEARCH, mActivity.get().isCoachSearch);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(mActivity.get(), SearchSpeakerResultActivity.class);
                    intent.putExtra(Constants.EXTRA_SPORT, mSportsList.get(position));
                    intent.putExtra(Constants.BundleKey.IS_COACH_SEARCH, mActivity.get().isCoachSearch);
                    intent.putExtra(Constants.BundleKey.IS_PANEL, mActivity.get().isPanel);
                    startActivity(intent);
                }
            }
        });
        getSportsList();
        return mBinder.getRoot();
    }

    @Override
    public String getFragmentName() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_search_by:
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, mActivity.get().isCoachSearch);
                Navigator.getInstance().navigateToActivityWithData(mActivity.get(), SearchCoachActivityNext.class, bundle);
                //startActivity(new Intent(mActivity.get(), SearchCoachActivityNext.class));
                break;

        }
    }

    private void getSportsList() {
        showProgressBar();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);
        getSportsRequest.setPageSize(Constants.SPORTS_PAGE_SIZE);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>
                (mActivity.get()) {
            @Override
            public void onSuccess(SportsListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        mSportsList = response.getResult().getSportsList();

                        mImageAdapter = new ImageAdapter(getContext(),
                                mSportsList);
//
                        mBinder.gridview.setAdapter(mImageAdapter);
//
                        for (Sports item : mSportsList) {
                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getReferencecLogo());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {
                try {
                    if(mActivity != null) {
                       mActivity.get().hideProgressBar();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private ArrayList<Sports> sportsArrayList;

        public ImageAdapter(Context c, ArrayList<Sports> sports) {
            mContext = c;
            this.sportsArrayList = sports;
        }

        public int getCount() {
            return sportsArrayList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (view == null) {
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.item_sports_grid, null);
            }
            Sports sports = sportsArrayList.get(position);
            TextView tvSportName = view.findViewById(R.id.tv_sport);
            ImageView ivSportsLogo = view.findViewById(R.id.iv_sport_logo);
            ImageView ivSportsCover = view.findViewById(R.id.iv_sport_cover);

            tvSportName.setText(sports.getName());

            if(!TextUtils.isEmpty(sports.getReferenceImageUrl())){
                Picasso.with(mActivity.get())
                        .load(sports.getReferenceImageUrl())
                        .into((ivSportsCover));
            }

            if(!TextUtils.isEmpty(sports.getReferencecLogo())){
                Picasso.with(mActivity.get())
                        .load(sports.getReferencecLogo())
                        .into((ivSportsLogo));
            }
            return view;
        }
    }
}
