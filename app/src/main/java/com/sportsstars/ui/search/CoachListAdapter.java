package com.sportsstars.ui.search;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemCoachByNameBinding;
import com.sportsstars.interfaces.CoachByNameItemClick;
import com.sportsstars.model.Coach;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abhaykant on 21/01/18.
 */

public class CoachListAdapter extends RecyclerView.Adapter<CoachListAdapter.CoachListViewHolder> {
    private static final String TAG = LogUtils.makeLogTag(CoachListAdapter.class);

    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ArrayList<Coach> mCoachList;
    private CoachByNameItemClick mClickListener;


    public void setSessionList(ArrayList<Coach> arrayList,CoachByNameItemClick click) {
        this.mCoachList = arrayList;
        mClickListener=click;
        notifyDataSetChanged();

    }


    public CoachListAdapter(Activity mActivity, ArrayList<Coach> sessionList, CoachByNameItemClick coachByNameItemClick) {
        this.mActivity = mActivity;
        this.mCoachList =sessionList;
        this.mClickListener = coachByNameItemClick;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public CoachListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemCoachByNameBinding searchListBinding = DataBindingUtil.inflate(mInflator, R.layout.item_coach_by_name, parent, false);
        return new CoachListViewHolder(searchListBinding);
    }

    @Override
    public void onBindViewHolder(final CoachListViewHolder holder, int position) {
        LogUtils.LOGD(TAG,"CountAre  SearchView>" + mCoachList);

        if (mCoachList !=null && mCoachList.size() > 0) {
            final Coach coach = mCoachList.get(position);
            holder.getBinding().tvCoachName.setText(coach.getName());

            if(!TextUtils.isEmpty(coach.getProfileImage())){

               Picasso.with(mActivity)
                        .load(coach.getProfileImage())
//                        .placeholder(R.drawable.ic_user_circle)
//                        .error(R.drawable.ic_user_circle)
                        .into(( holder.getBinding().ivCoach));

            }

            holder.getBinding().tvCoachCharges.setText(coach.getOneOneBookingTypePrice()+mActivity.getString(R.string.per_hour));
            if(coach.getDistance()==0){
                holder.getBinding().tvDistance.setText("NA");
            }else {
                holder.getBinding().tvDistance.setText(Utils.getDistance(coach.getDistance()));
            }


        }
    }

    @Override
    public int getItemCount() {
        return mCoachList != null ? mCoachList.size() : 0;
    }

    public void clearItems() {
        if(mCoachList !=null&& mCoachList.size()>0) {
            mCoachList.clear();
            notifyDataSetChanged();
        }
    }



    class CoachListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemCoachByNameBinding sessionBinding;

        private CoachListViewHolder(ItemCoachByNameBinding sessionBinding) {
            super(sessionBinding.getRoot());
            this.sessionBinding = sessionBinding;
            sessionBinding.getRoot().setOnClickListener(this);
        }

        public ItemCoachByNameBinding getBinding() {
            return sessionBinding;
        }

        @Override
        public void onClick(View view) {
            LogUtils.LOGD(TAG,"clicked");
           mClickListener.onItemClick(sessionBinding,mCoachList.get(getAdapterPosition()),getAdapterPosition());
        }
    }

}



