package com.sportsstars.ui.search;

import android.databinding.DataBindingUtil;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentSearchCoachByNameBinding;
import com.sportsstars.databinding.ItemCoachByNameBinding;
import com.sportsstars.eventbus.LocationEvent;
import com.sportsstars.interfaces.CoachByNameItemClick;
import com.sportsstars.model.Coach;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SearchCoachBySportResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.session.SessionListActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;

/**
 * Created by ramkumar on 19/01/18.
 */

public class FragmentSearchCoachByName extends BaseFragment implements CoachByNameItemClick ,View.OnClickListener{
    private static final String TAG = LogUtils.makeLogTag(FragmentSearchCoachByName.class);
    private WeakReference<SearchCoachActivity> mActivity;
    private FragmentSearchCoachByNameBinding mBinder;
    private ArrayList<Coach>mCoachList;
    private double mLatitude;
    private double mLongitude;
    private Geocoder geocoder;
    public static FragmentSearchCoachByName newInstance() {
        FragmentSearchCoachByName fragment = new FragmentSearchCoachByName();
        Bundle args = new Bundle();
//        args.putString(AppConstants.ARG_PARAM2, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((SearchCoachActivity) getActivity());

        if (getArguments() != null) {
//            mUserId = getArguments().getString(AppConstants.ARG_PARAM2);
        }
        mCoachList=new ArrayList<>();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        LocationUtils.addFragment(mActivity.get(), getBaseActivity());
        geocoder = new Geocoder(mActivity.get(), Locale.getDefault());



        if( isAdded() && isAlive()) {
            getCoachList();
        }
    }


    @Subscribe
    public void onEvent(LocationEvent event) {
//        try {
//            getBaseActivity().hideProgressBar();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        LogUtils.LOGD(TAG, "onEvent location update...");
        Location location = event.getMessage();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LogUtils.LOGD(TAG, "Lat/lng:"+mLatitude+" "+mLongitude);

        if( isAdded() && isAlive()) {
            getCoachList();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_search_coach_by_name,
                container, false);
        mBinder.tvSearchBy.setOnClickListener(this);

        return mBinder.getRoot();
    }

    @Override
    public String getFragmentName() {
        return null;
    }

    private void setAdapter(ArrayList<Coach> coaches){

        CoachListAdapter mAdapter = new CoachListAdapter(mActivity.get(),coaches,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity.get());
        mBinder.rvSportsByname.setLayoutManager(mLayoutManager);
        mBinder.rvSportsByname.setItemAnimator(new DefaultItemAnimator());
        mBinder.rvSportsByname.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
    private void getCoachList() {
        showProgressBar();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);
        getSportsRequest.setLat(mLatitude);
        getSportsRequest.setLng(mLongitude);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.coachList(getSportsRequest).enqueue(new BaseCallback<SearchCoachBySportResponse>(mActivity.get()) {
            @Override
            public void onSuccess(SearchCoachBySportResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult()!=null && response.getResult().getCoaches() != null) {
                        mCoachList=response.getResult().getCoaches();
                        setAdapter(mCoachList);

                        mBinder.tvCoachesFound.setText(mCoachList.size()+" "+getString(R.string.coaches_found_near));
                    }
                }
            }

            @Override
            public void onFail(Call<SearchCoachBySportResponse> call, BaseResponse baseResponse) {
                try {
                    if(mActivity != null) {
                        mActivity.get().hideProgressBar();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onItemClick(ItemCoachByNameBinding itemCoachByNameBinding, Coach coach, int pos) {
        Bundle bundle= new Bundle();
        bundle.putParcelable(Constants.EXTRA_COACH,coach);
        Navigator.getInstance().navigateToActivityWithData(mActivity.get(),SessionListActivity.class,bundle);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_search_by:
                Bundle bundle=new Bundle();
                bundle.putBoolean(Constants.EXTRA_IS_SEARCH_BY_NAME,true);
                Navigator.getInstance().navigateToActivityWithData(mActivity.get(),SearchCoachActivityNext.class,bundle);
               // startActivity(new Intent(mActivity.get(), SearchCoachActivity2.class));
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }
}
