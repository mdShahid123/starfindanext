package com.sportsstars.ui.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.edmodo.rangebar.RangeBar;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityCoachFilterBinding;
import com.sportsstars.model.FilterData;
import com.sportsstars.model.FilterState;
import com.sportsstars.model.SearchFilter;
import com.sportsstars.model.Sports;
import com.sportsstars.model.Team;
import com.sportsstars.ui.coach.AddTeamActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ram on 20/01/18.
 */

public class CoachFilterActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(CoachFilterActivity.class);
    private static final int REQUEST_CODE_LOCATION = 101;
    private static int MIN_RANGE_AGE = 10;
    private static int MAX_RANGE_AGE = 100;
    private static int STEP_RANGE_AGE = 1;
    private static int INITIAL_MIN_AGE = 15;
    private static int INITIAL_MAX_AGE = 75;

    private static int MIN_RANGE_PRICE = 10;
    private static int MAX_RANGE_PRICE = 500;
    private static int STEP_RANGE_PRICE = 1;
    private static int INITIAL_MIN_PRICE = 100;
    private static int INITIAL_MAX_PRICE = 300;

    private static int MIN_RANGE_DISTANCE = 1;
    private static int MAX_RANGE_DISTANCE = 100;
    private static int STEP_RANGE_DISTANCE = 1;
    private static int INITIAL_MIN_DISTANCE = 1;
    private static int INITIAL_MAX_DISTANCE = 100;

    //private int mMinDistanceKM, mMaxDistanceKM, mDiffDistance, MD, XD, TC, DIFF;
    private int ageTickCount, priceTickCount, distanceTickCount;
    private int minAgeSelected, maxAgeSelected, minPriceSelected, maxPriceSelected,
            minDistanceSelected, maxDistanceSelected;

    private ActivityCoachFilterBinding mBinder;
    private Sports mSport;
    private int mSportsType = 2; //1.Private, 2.Star
    //    private double mLatitude;
//    private double mLongitude;
//    private String mAddress;
//    private Geocoder geocoder;
//    private int mGender;
    private boolean mMaleSelected;
    private boolean mFemaleSelected;

    private boolean mPrivateSelected;
    private boolean mStarSelected;

    private boolean mHandsOnSelected;
    private boolean mInspirationalSelected;
    private FilterData mSearchFilter;
    private FilterState mFilterState;
    private String[] teams;
    private ArrayList<Team> mTeamsList;
    private int mCoachType;

    private boolean isMaleChecked;
    private boolean isFemaleChecked;
    private boolean isHandsOnChecked;
    private boolean isInspirationalChecked;
    private boolean isAgeRangeChecked;
    private boolean isPriceRangeChecked;
    private boolean isDistanceRangeChecked;
    private boolean isTeamsChecked;

    private boolean isClearButtonClicked;

    @Override
    public String getActivityName() {
        return CoachFilterActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_coach_filter);

        mBinder.buttonApply.setOnClickListener(this);
        mBinder.buttonClear.setOnClickListener(this);

        if (getIntent() != null) {
            mSearchFilter = getIntent().getParcelableExtra("filter");
            mCoachType = getIntent().getIntExtra("coachType", 0);
            mFilterState = getIntent().getParcelableExtra("filterState");

            if (mSearchFilter != null) {
                MIN_RANGE_AGE = mSearchFilter.getMinAge() - 2;
                MAX_RANGE_AGE = mSearchFilter.getMaxAge() + 2;

                if (MAX_RANGE_AGE <= MIN_RANGE_AGE) {
                    MAX_RANGE_AGE = MIN_RANGE_AGE + 10;
                }

                INITIAL_MIN_AGE = MIN_RANGE_AGE;
                INITIAL_MAX_AGE = MAX_RANGE_AGE - 1;

                MIN_RANGE_PRICE = (int) mSearchFilter.getMinPrice() - 150;
                MAX_RANGE_PRICE = (int) mSearchFilter.getMaxPrice() + 150;

                if (MAX_RANGE_PRICE <= MIN_RANGE_PRICE) {
                    MAX_RANGE_PRICE = MIN_RANGE_PRICE + 10;
                }

                INITIAL_MIN_PRICE = MIN_RANGE_PRICE;
                INITIAL_MAX_PRICE = MAX_RANGE_PRICE - 1;


                // ******** NEW CHANGES *********** //
                MIN_RANGE_DISTANCE = (int) mSearchFilter.getMinDistance() - 2;
                MAX_RANGE_DISTANCE = (int) mSearchFilter.getMaxDistance() + 2;

                if (MAX_RANGE_DISTANCE <= MIN_RANGE_DISTANCE) {
                    MAX_RANGE_DISTANCE = MIN_RANGE_DISTANCE + 10;
                }

                INITIAL_MIN_DISTANCE = MIN_RANGE_DISTANCE;
                INITIAL_MAX_DISTANCE = MAX_RANGE_DISTANCE - 1;


                // ********* OLD WORKING CODE ******** //
//                MIN_RANGE_DISTANCE = (int) mSearchFilter.getMinDistance() / 1000 + 1;

//                MD = (int) mSearchFilter.getMinDistance() / 1000;
//                XD = (int) mSearchFilter.getMaxDistance() / 1000 + 1;
//
//                if (XD - MD < 3) XD = MD + 3;
//                TC = XD - MD + 1;
//                DIFF = MD;


                /*MIN_RANGE_DISTANCE = (int) mSearchFilter.getMinDistance() / 1000;
                MAX_RANGE_DISTANCE = (int) mSearchFilter.getMaxDistance() / 1000 + 1;

                if (MAX_RANGE_DISTANCE <= MIN_RANGE_DISTANCE) {
                    MAX_RANGE_DISTANCE = MIN_RANGE_DISTANCE + 1;

                    if (MAX_RANGE_DISTANCE == 2) MAX_RANGE_DISTANCE = 3;
                }

//                INITIAL_MIN_DISTANCE = MIN_RANGE_DISTANCE;
//                INITIAL_MAX_DISTANCE = MAX_RANGE_DISTANCE - 1;

                INITIAL_MIN_DISTANCE = MIN_RANGE_DISTANCE;
                INITIAL_MAX_DISTANCE = MAX_RANGE_DISTANCE;

                mMinDistanceKM = MIN_RANGE_DISTANCE;
                if (mMinDistanceKM < 1) {
                    mMinDistanceKM = 1;
                    MIN_RANGE_DISTANCE = 1;
                }

//                mMaxDistanceKM = MAX_RANGE_DISTANCE + 1;
                mMaxDistanceKM = MAX_RANGE_DISTANCE;
                if (mMaxDistanceKM < 3) mMaxDistanceKM = 3;*/

            }
        }

        init();

        initFilterState();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.button_apply:
                applyFilter();
                break;

            case R.id.button_clear:
                isClearButtonClicked = true;
                mBinder.tvTeamsPlayed.setText(R.string.search_by_team);
                mTeamsList = null;
                init();
                if(isClearButtonClicked) {
                    isClearButtonClicked = false;
                }
                break;

            case R.id.rel_man:
                setMaleSelected();
                break;

            case R.id.rel_woman:
                setFemaleSelected();
                break;

            case R.id.rel_private:
                setPrivateSelected();
//                mStarSelected = true;
//                mPrivateSelected = false;
//                setStarSelected();
                break;

            case R.id.rel_star:
                setStarSelected();

//                mPrivateSelected = true;
//                mStarSelected = false;
//                setPrivateSelected();
                break;

            case R.id.rel_hands_on:
                setHandsOnSelected();
                break;

            case R.id.rel_inspirational:
                setInspirationalSelected();
                break;

            case R.id.tv_teams_played:
                if(mSearchFilter != null) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("teams", mSearchFilter.getTeams());
                    if (mTeamsList != null && mTeamsList.size() > 0) {
                        bundle.putParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST, mTeamsList);
                    }
                    Navigator.getInstance().navigateToActivityForResultWithData(this, AddTeamActivity.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS);
                }
                break;

            case R.id.tv_hands_on_info:
                showHandsOnInfoDialog();
//                if(mHandsOnSelected) {
//                    showHandsOnInfoDialog();
//                }
                break;

            case R.id.tv_inspirational_info:
                showInspirationalInfoDialog();
//                if(mInspirationalSelected) {
//                    showInspirationalInfoDialog();
//                }
                break;
        }
    }

    private void showHandsOnInfoDialog() {
        String title = getResources().getString(R.string.txt_sst_opt1);
        String message = getResources().getString(R.string.hands_on_info);
        Alert.showInfoDialog(CoachFilterActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showInspirationalInfoDialog() {
        String title = getResources().getString(R.string.txt_sst_opt2);
        String message = getResources().getString(R.string.inspirational_info);
        Alert.showInfoDialog(CoachFilterActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS:
                    mTeamsList = data.getParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST);

                    if (mTeamsList == null || mTeamsList.size() == 0) {
                        mBinder.tvTeamsPlayed.setText(getString(R.string.search_by_team));
                        isTeamsChecked = false;
                    } else {
                        mBinder.tvTeamsPlayed.setText(Utils.getCommaSepString(mTeamsList));
                        isTeamsChecked = true;
                    }
                    break;
            }
        }
    }

    private void getMinMaxAgeSelected(int min, int max) {
        minAgeSelected = min + INITIAL_MIN_AGE;
        maxAgeSelected = max + INITIAL_MIN_AGE + 1;

        if(minAgeSelected < mSearchFilter.getMinAge()) {
            minAgeSelected = mSearchFilter.getMinAge();
        }
        if(maxAgeSelected > mSearchFilter.getMaxAge()) {
            maxAgeSelected = mSearchFilter.getMaxAge();
        }

        if(maxAgeSelected <= minAgeSelected && minAgeSelected == mSearchFilter.getMinAge()) {
            maxAgeSelected = minAgeSelected + 1;
        }

        if(minAgeSelected >= maxAgeSelected && maxAgeSelected == mSearchFilter.getMaxAge()) {
            minAgeSelected = maxAgeSelected - 1;
        }

//        Log.d("mylog", "getMinMaxAgeSelected - minAgeSelected : " + minAgeSelected
//            + " maxAgeSelected : " + maxAgeSelected);
    }

    private void getMinMaxDistanceSelected(int min, int max) {

        minDistanceSelected = min + INITIAL_MIN_DISTANCE;
        maxDistanceSelected = max + INITIAL_MIN_DISTANCE + 1;

        int minDistDefault = (int) mSearchFilter.getMinDistance();
        int maxDistDefault = (int) mSearchFilter.getMaxDistance();

        if(minDistanceSelected < minDistDefault) {
            minDistanceSelected = minDistDefault;
        }

        if(maxDistanceSelected > maxDistDefault) {
            maxDistanceSelected = maxDistDefault;
        }

        if(maxDistanceSelected <= minDistanceSelected && minDistanceSelected == minDistDefault) {
            maxDistanceSelected = minDistanceSelected + 1;
        }

        if(minDistanceSelected >= maxDistanceSelected && maxDistanceSelected == maxDistDefault) {
            minDistanceSelected = maxDistanceSelected - 1;
        }

        Log.d("mylog", "getMinMaxDistanceSelected - minDistanceSelected : " + minDistanceSelected
                + " maxDistanceSelected : " + maxDistanceSelected);
    }

    private void getMinMaxPriceSelected(int min, int max) {

        int minPrice = min + INITIAL_MIN_PRICE;
        int maxPrice = max + INITIAL_MIN_PRICE + 1;

        minPriceSelected = minPrice;
        maxPriceSelected = maxPrice;

        int minPriceDefault = (int) mSearchFilter.getMinPrice();
        int maxPriceDefault = (int) mSearchFilter.getMaxPrice();

        if(minPriceSelected < minPriceDefault) {
            minPriceSelected = minPriceDefault;
        }

        if(maxPriceSelected > maxPriceDefault) {
            maxPriceSelected = maxPriceDefault;
        }

        if(maxPriceSelected <= minPriceSelected && minPriceSelected == minPriceDefault) {
            maxPriceSelected = minPriceSelected + 1;
        }

        if(minPriceSelected >= maxPriceSelected && maxPriceSelected == maxPriceDefault) {
            minPriceSelected = maxPriceSelected - 1;
        }

        Log.d("mylog", "getMinMaxPriceSelected - minPriceSelected : " + minPriceSelected
                + " maxPriceSelected : " + maxPriceSelected);
    }

    private void init() {

        isMaleChecked = false;
        isFemaleChecked = false;
        isHandsOnChecked = false;
        isInspirationalChecked = false;
        isAgeRangeChecked = false;
        isPriceRangeChecked = false;
        isDistanceRangeChecked = false;
        isTeamsChecked = false;

        mMaleSelected = true;
        mFemaleSelected = true;
        mPrivateSelected = true;
        mStarSelected = true;
        mHandsOnSelected = true;
        mInspirationalSelected = true;

        setMaleSelected();
        setFemaleSelected();
//        setPrivateSelected();
//        setStarSelected();
        setHandsOnSelected();
        setInspirationalSelected();

        mBinder.relMan.setOnClickListener(this);
        mBinder.relWoman.setOnClickListener(this);
        mBinder.relPrivate.setOnClickListener(this);
        mBinder.relStar.setOnClickListener(this);
        mBinder.relHandsOn.setOnClickListener(this);
        mBinder.relInspirational.setOnClickListener(this);
        mBinder.tvTeamsPlayed.setOnClickListener(this);
        mBinder.tvHandsOnInfo.setOnClickListener(this);
        mBinder.tvInspirationalInfo.setOnClickListener(this);

//        mBinder.headerId.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
//        mBinder.headerId.tvTitle.setText(R.string.filters);
//        mBinder.headerId.ivBack.setOnClickListener(this);

        ageTickCount = (MAX_RANGE_AGE - MIN_RANGE_AGE) / STEP_RANGE_AGE;
        Log.d("mylog", "Filter - ageTickCount : " + ageTickCount);

        mBinder.rangeBarAge.setTickCount(ageTickCount);
        mBinder.rangeBarAge.setTickHeight(0);
        mBinder.rangeBarAge.setBarWeight(10);
        mBinder.rangeBarAge.setConnectingLineColor(ContextCompat.getColor(this, R.color.green_gender));
        mBinder.rangeBarAge.setBarColor(ContextCompat.getColor(this, R.color.divider_color));
        mBinder.rangeBarAge.setThumbImageNormal(R.drawable.ic_oval_shadow);
        mBinder.rangeBarAge.setThumbImagePressed(R.drawable.ic_oval_shadow);

        if (ageTickCount > 1) {
            mBinder.rangeBarAge.setThumbIndices(0, ageTickCount - 1);
        }

        minAgeSelected = INITIAL_MIN_AGE;
        maxAgeSelected = INITIAL_MAX_AGE + 1;
        if(minAgeSelected < mSearchFilter.getMinAge()) {
            minAgeSelected = mSearchFilter.getMinAge();
        }
        if(maxAgeSelected > mSearchFilter.getMaxAge()) {
            maxAgeSelected = mSearchFilter.getMaxAge();
        }
        mBinder.tvAgeMin.setText(String.format(Locale.getDefault(), "%d YEARS", minAgeSelected));
        mBinder.tvAgeMax.setText(String.format(Locale.getDefault(), "%d YEARS", maxAgeSelected));

        mBinder.rangeBarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int min, int max) {
                if(min > max) {
                    int temp = min;
                    min = max;
                    max = temp;
                }
//                Log.d("mylog", "RangeChange - Age - min : " + min + " max : " + max);
                if (min < 0) {
                    min = 0;
//                    mBinder.rangeBarAge.setLeft(min);
                    mBinder.rangeBarAge.setThumbIndices(min, max);
                }
                if (max > (ageTickCount - 1)) {
                    max = ageTickCount - 1;
//                    mBinder.rangeBarAge.setRight(max);
                    mBinder.rangeBarAge.setThumbIndices(min, max);
                }

                getMinMaxAgeSelected(min, max);

                mBinder.tvAgeMin.setText(String.format(Locale.getDefault(), "%d YEARS", minAgeSelected));
                mBinder.tvAgeMax.setText(String.format(Locale.getDefault(), "%d YEARS", maxAgeSelected));

                if(isClearButtonClicked) {
                    isAgeRangeChecked = false;
                } else {
                    isAgeRangeChecked = true;
                }
            }
        });

        priceTickCount = (MAX_RANGE_PRICE - MIN_RANGE_PRICE) / STEP_RANGE_PRICE;
        Log.d("mylog", "Filter - priceTickCount : " + priceTickCount);

        mBinder.rangeBarPrice.setTickCount(priceTickCount);
        mBinder.rangeBarPrice.setTickHeight(0);
        mBinder.rangeBarPrice.setBarWeight(10);
        mBinder.rangeBarPrice.setConnectingLineColor(ContextCompat.getColor(this, R.color.green_gender));
        mBinder.rangeBarPrice.setBarColor(ContextCompat.getColor(this, R.color.divider_color));
        mBinder.rangeBarPrice.setThumbImageNormal(R.drawable.ic_oval_shadow);
        mBinder.rangeBarPrice.setThumbImagePressed(R.drawable.ic_oval_shadow);

        if (priceTickCount > 1) {
            mBinder.rangeBarPrice.setThumbIndices(0, priceTickCount - 1);
        }

//        mBinder.rangeBarPrice.setThumbIndices(INITIAL_MIN_PRICE / STEP_RANGE_PRICE,
//                INITIAL_MAX_PRICE / STEP_RANGE_PRICE);

        minPriceSelected = INITIAL_MIN_PRICE;
        maxPriceSelected = INITIAL_MAX_PRICE + 1;

        int minPriceDefault = (int) mSearchFilter.getMinPrice();
        int maxPriceDefault = (int) mSearchFilter.getMaxPrice();

        if(minPriceSelected < minPriceDefault) {
            minPriceSelected = minPriceDefault;
        }
        if(maxPriceSelected > maxPriceDefault) {
            maxPriceSelected = maxPriceDefault;
        }
        mBinder.tvPriceMin.setText(String.format(Locale.getDefault(), "$%d", minPriceSelected));
        mBinder.tvPriceMax.setText(String.format(Locale.getDefault(), "$%d", maxPriceSelected));

        mBinder.rangeBarPrice.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int min, int max) {
                if(min > max) {
                    int temp = min;
                    min = max;
                    max = temp;
                }
                if (min < 0) {
                    min = 0;
                    mBinder.rangeBarPrice.setThumbIndices(min, max);
                }

                if (max > (priceTickCount - 1)) {
                    max = priceTickCount - 1;
                    mBinder.rangeBarPrice.setThumbIndices(min, max);
                }

                getMinMaxPriceSelected(min, max);

                mBinder.tvPriceMin.setText(String.format(Locale.getDefault(), "$%d", minPriceSelected));
                mBinder.tvPriceMax.setText(String.format(Locale.getDefault(), "$%d", maxPriceSelected));

//                mBinder.tvPriceMin.setText(String.format(Locale.getDefault(), "$%d", minPrice));
//                mBinder.tvPriceMax.setText(String.format(Locale.getDefault(), "$%d", maxPrice));
//                LogUtils.LOGD(TAG, "Min " + min + ", " + max);

                if(isClearButtonClicked) {
                    isPriceRangeChecked = false;
                } else {
                    isPriceRangeChecked = true;
                }
            }
        });

        distanceTickCount = (MAX_RANGE_DISTANCE - MIN_RANGE_DISTANCE) / STEP_RANGE_DISTANCE;
        Log.d("mylog", "Filter - distanceTickCount : " + distanceTickCount);

        mBinder.rangeBarDistance.setTickCount(distanceTickCount);
        //mBinder.rangeBarDistance.setTickCount(TC);
        mBinder.rangeBarDistance.setTickHeight(0);
        mBinder.rangeBarDistance.setBarWeight(10);
        mBinder.rangeBarDistance.setConnectingLineColor(ContextCompat.getColor(this, R.color.green_gender));
        mBinder.rangeBarDistance.setBarColor(ContextCompat.getColor(this, R.color.divider_color));
        mBinder.rangeBarDistance.setThumbImageNormal(R.drawable.ic_oval_shadow);
        mBinder.rangeBarDistance.setThumbImagePressed(R.drawable.ic_oval_shadow);

        if (distanceTickCount > 1) {
            mBinder.rangeBarDistance.setThumbIndices(0, distanceTickCount - 1);
        }

        //mBinder.rangeBarDistance.setThumbIndices(0, TC - 1);

//        int minDisplay = MD;
//        int maxDisplay = XD;

        int minDisplay = MIN_RANGE_DISTANCE;
        int maxDisplay = MAX_RANGE_DISTANCE;

        minDistanceSelected = INITIAL_MIN_DISTANCE;
        maxDistanceSelected = INITIAL_MAX_DISTANCE;

        int minDistDefault = (int) mSearchFilter.getMinDistance();
        int maxDistDefault = (int) mSearchFilter.getMaxDistance();

        if(minDistanceSelected < minDistDefault) {
            minDistanceSelected = minDistDefault;
        }

        if(maxDistanceSelected > maxDistDefault) {
            maxDistanceSelected = maxDistDefault;
        }

        mBinder.tvDistanceMin.setText(String.format(Locale.getDefault(), "%d KM", minDistanceSelected));
        mBinder.tvDistanceMax.setText(String.format(Locale.getDefault(), "%d KM", maxDistanceSelected));

        mBinder.rangeBarDistance.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int min, int max) {
//                LogUtils.LOGE(TAG, "MIN: " + min + ", MAX: " + max);
                if(min > max) {
                    int temp = min;
                    min = max;
                    max = temp;
                }
                if (min < 0) {
                    min = 0;
                    mBinder.rangeBarDistance.setThumbIndices(min, max);
                }

//                if (max > (TC - 1)) {
//                    max = TC - 1;
//                    mBinder.rangeBarDistance.setThumbIndices(min, max);
//                }

                if (max > (distanceTickCount - 1)) {
                    max = distanceTickCount - 1;
                    mBinder.rangeBarDistance.setThumbIndices(min, max);
                }

//                int minD = min + DIFF;
//                int maxD = max + DIFF;

                getMinMaxDistanceSelected(min, max);

                mBinder.tvDistanceMin.setText(String.format(Locale.getDefault(), "%d KM", minDistanceSelected));
                mBinder.tvDistanceMax.setText(String.format(Locale.getDefault(), "%d KM", maxDistanceSelected));

                if(isClearButtonClicked) {
                    isDistanceRangeChecked = false;
                } else {
                    isDistanceRangeChecked = true;
                }
            }
        });

        if(isClearButtonClicked) {
            isAgeRangeChecked = false;
        }

        if(isClearButtonClicked) {
            isPriceRangeChecked = false;
        }

        if(isClearButtonClicked) {
            isDistanceRangeChecked = false;
        }

    }

    private void initFilterState() {
        if (mFilterState == null) return;

        //Gender
        if (mFilterState.getGender() == Constants.GENDER_TYPE.MALE) {
            mMaleSelected = false;
            setMaleSelected();
        } else if (mFilterState.getGender() == Constants.GENDER_TYPE.FEMALE) {
            mFemaleSelected = false;
            setFemaleSelected();
        } else if (mFilterState.getGender() == Constants.GENDER_TYPE.BOTH_SELECTED) { //Both selected
            mMaleSelected = false;
            mFemaleSelected = false;

            setMaleSelected();
            setFemaleSelected();
        } else if (mFilterState.getGender() == 0) { //No one selected
            mMaleSelected = true;
            mFemaleSelected = true;

            setMaleSelected();
            setFemaleSelected();
        }

        //Session Type
        if (mFilterState.getSessionType() == Constants.SESSION_TYPE.HANDS_ON) {
            mHandsOnSelected = false;
            setHandsOnSelected();
        } else if (mFilterState.getSessionType() == Constants.SESSION_TYPE.INSPIRATIONAL) {
            mInspirationalSelected = false;
            setInspirationalSelected();
        } else if (mFilterState.getSessionType() == Constants.SESSION_TYPE.BOTH_SELECTED) {
            mHandsOnSelected = false;
            mInspirationalSelected = false;

            setHandsOnSelected();
            setInspirationalSelected();
        } else if (mFilterState.getSessionType() == 0) {
            mHandsOnSelected = true;
            mInspirationalSelected = true;

            setHandsOnSelected();
            setInspirationalSelected();
        }

        int min = mFilterState.getMinAge() - INITIAL_MIN_AGE;
        int max = mFilterState.getMaxAge() - INITIAL_MIN_AGE - 1;
        Log.d("mylog", "initFilterState - Age - min : " + min + " max : " + max);
        if (min <= max && min >= 0) {
            mBinder.rangeBarAge.setThumbIndices(min, max);
            isAgeRangeChecked = true;
        } else {
            isAgeRangeChecked = false;
        }

        // min = 8 - 8 => 0
        // max = 9 - 8 - 1 => 0



        min = (int) mFilterState.getMinPrice() - INITIAL_MIN_PRICE;
        max = (int) mFilterState.getMaxPrice() - INITIAL_MIN_PRICE - 1;

        if (min <= max && min >= 0) {
            mBinder.rangeBarPrice.setThumbIndices(min, max);
            isPriceRangeChecked = true;
        } else {
            isPriceRangeChecked = false;
        }


//        int left = (int) mFilterState.getMinDistance();
//        int right = (int) mFilterState.getMaxDistance();

        int left = (int) mFilterState.getMinDistance() - INITIAL_MIN_DISTANCE;
        int right = (int) mFilterState.getMaxDistance() - INITIAL_MIN_DISTANCE - 1;
        Log.d("mylog", "initFilterState - Distance - "
                + " mFilterState.getMinDistance() : " + mFilterState.getMinDistance()
                + " mFilterState.getMaxDistance() : " + mFilterState.getMaxDistance()
                + " INITIAL_MIN_DISTANCE : " + INITIAL_MIN_DISTANCE
                + " min : " + left + " max : " + right);


//        int minD = left + DIFF;
//        int maxD = right + DIFF;

        int minD = left;
        int maxD = right;

//        mBinder.tvDistanceMin.setText(String.format(Locale.getDefault(), "%d KM", minD));
//        mBinder.tvDistanceMax.setText(String.format(Locale.getDefault(), "%d KM", maxD));

        if (left <= right && left >= 0) {
            mBinder.rangeBarDistance.setThumbIndices(left, right);
            isDistanceRangeChecked = true;
        } else {
            isDistanceRangeChecked = false;
        }




        if (mFilterState.getTeams() != null && mFilterState.getTeams().size() > 0) {
            mBinder.tvTeamsPlayed.setText(Utils.getCommaSepString(mFilterState.getTeams()));
            mTeamsList = mFilterState.getTeams();
            isTeamsChecked = true;
        } else {
            isTeamsChecked = false;
        }
    }

    private void applyFilter() {
        SearchFilter filter = new SearchFilter();
        FilterState state = new FilterState();

        int left = mBinder.rangeBarAge.getLeftIndex();
        int right = mBinder.rangeBarAge.getRightIndex();

        if (left < 0) left = 0;
        if (right > ageTickCount - 1) right = ageTickCount - 1;

        int minAge = (left + MIN_RANGE_AGE);
        int maxAge = (right + MIN_RANGE_AGE) + 1;

        if(isAgeRangeChecked) {
//            filter.setMinAge(minAge);
//            filter.setMaxAge(maxAge);
//
//            state.setMinAge(minAge);
//            state.setMaxAge(maxAge);

            if(minAgeSelected == mSearchFilter.getMinAge()
                    && maxAgeSelected == mSearchFilter.getMaxAge()) {
                filter.setMinAge(0);
            }  else {
                filter.setMinAge(minAgeSelected);
            }

            if(minAgeSelected == mSearchFilter.getMinAge()
                    && maxAgeSelected == mSearchFilter.getMaxAge()) {
                filter.setMaxAge(0);
            } else {
                filter.setMaxAge(maxAgeSelected);
            }

            //filter.setMinAge(minAgeSelected);
            //filter.setMaxAge(maxAgeSelected);

            state.setMinAge(minAgeSelected);
            state.setMaxAge(maxAgeSelected);
        }



        int minDistance = (mBinder.rangeBarDistance.getLeftIndex());
        int maxDistance = (mBinder.rangeBarDistance.getRightIndex());

        if (minDistance < 0) {
            minDistance = 0;
        }

//        if (maxDistance > (TC - 1)) {
//            maxDistance = TC - 1;
//        }

        if (maxDistance > (distanceTickCount - 1)) {
            maxDistance = distanceTickCount - 1;
        }

        int minDistanceSet = (minDistance + MIN_RANGE_DISTANCE);
        int maxDistanceSet = (maxDistance + MIN_RANGE_DISTANCE) + 1;

        if(isDistanceRangeChecked) {
//            filter.setMinDistance(minDistance * 1000);
//            filter.setMaxDistance((maxDistance + MIN_RANGE_DISTANCE + 1) * 1000);

//            filter.setMinDistance(minDistanceSet);
//            filter.setMaxDistance(maxDistanceSet);
//
//            state.setMinDistance(minDistanceSet);
//            state.setMaxDistance(maxDistanceSet);

            if(minDistanceSelected == mSearchFilter.getMinDistance()
                    && maxDistanceSelected == mSearchFilter.getMaxDistance()) {
                filter.setMinDistance(0);
            }  else {
                filter.setMinDistance(minDistanceSelected * 1000d);
            }

            if(minDistanceSelected == mSearchFilter.getMinDistance()
                    && maxDistanceSelected == mSearchFilter.getMaxDistance()) {
                filter.setMaxDistance(0);
            }  else {
                filter.setMaxDistance(maxDistanceSelected * 1000d);
            }

            //filter.setMinDistance(minDistanceSelected * 1000d);  // Converting Kms to Metres for API
            //filter.setMaxDistance(maxDistanceSelected * 1000d);  // Converting Kms to Metres for API

            state.setMinDistance(minDistanceSelected);
            state.setMaxDistance(maxDistanceSelected);
        }



        left = mBinder.rangeBarPrice.getLeftIndex();
        right = mBinder.rangeBarPrice.getRightIndex();

        if (left < 0) left = 0;
        if (right > priceTickCount - 1) right = priceTickCount - 1;

        double minPrice = (left + MIN_RANGE_PRICE);
        double maxPrice = (right + MIN_RANGE_PRICE) + 1d;

        if(isPriceRangeChecked) {
//            filter.setMinPrice(minPrice);
//            filter.setMaxPrice(maxPrice);
//
//            state.setMinPrice(minPrice);
//            state.setMaxPrice(maxPrice);

            if(minPriceSelected == mSearchFilter.getMinPrice()
                    && maxPriceSelected == mSearchFilter.getMaxPrice()) {
                filter.setMinPrice(0);
            }  else {
                filter.setMinPrice(minPriceSelected);
            }

            if(minPriceSelected == mSearchFilter.getMinPrice()
                    && maxPriceSelected == mSearchFilter.getMaxPrice()) {
                filter.setMaxPrice(0);
            }  else {
                filter.setMaxPrice(maxPriceSelected);
            }

            //filter.setMinPrice(minPriceSelected);
            //filter.setMaxPrice(maxPriceSelected);

            state.setMinPrice(minPriceSelected);
            state.setMaxPrice(maxPriceSelected);
        }



        if (!mHandsOnSelected && !mInspirationalSelected) {
            if(!isHandsOnChecked && !isInspirationalChecked) {
                state.setSessionType(0);
            } else {
                filter.setSessionType(Constants.SESSION_TYPE.BOTH_SELECTED);
                state.setSessionType(0);
            }
        } else if (mHandsOnSelected && mInspirationalSelected) {
            if(isHandsOnChecked && isInspirationalChecked) {
                filter.setSessionType(Constants.SESSION_TYPE.BOTH_SELECTED);
                state.setSessionType(Constants.SESSION_TYPE.BOTH_SELECTED);
            }
        } else {
            if (mHandsOnSelected) {
                if(isHandsOnChecked) {
                    filter.setSessionType(Constants.SESSION_TYPE.HANDS_ON);
                    state.setSessionType(Constants.SESSION_TYPE.HANDS_ON);
                }
            }

            if (mInspirationalSelected) {
                if(isInspirationalChecked) {
                    filter.setSessionType(Constants.SESSION_TYPE.INSPIRATIONAL);
                    state.setSessionType(Constants.SESSION_TYPE.INSPIRATIONAL);
                }
            }
        }

        if (!mMaleSelected && !mFemaleSelected) {
            if(!isMaleChecked && !isFemaleChecked) {
                state.setGender(0);
            } else {
                filter.setGender(Constants.GENDER_TYPE.BOTH_SELECTED);
                state.setGender(0);
            }
        } else if (mMaleSelected && mFemaleSelected) {
            if(isMaleChecked && isFemaleChecked) {
                filter.setGender(Constants.GENDER_TYPE.BOTH_SELECTED);
                state.setGender(Constants.GENDER_TYPE.BOTH_SELECTED);
            }
        } else {
            if (mMaleSelected) {
                if(isMaleChecked) {
                    filter.setGender(Constants.GENDER_TYPE.MALE);
                    state.setGender(Constants.GENDER_TYPE.MALE);
                }
            }

            if (mFemaleSelected) {
                if(isFemaleChecked) {
                    filter.setGender(Constants.GENDER_TYPE.FEMALE);
                    state.setGender(Constants.GENDER_TYPE.FEMALE);
                }
            }
        }

        if(isTeamsChecked) {
            if (mTeamsList != null && mTeamsList.size() > 0) {
                ArrayList<String> teams = new ArrayList<>();

                if (mCoachType == Constants.COACH_TYPE.STAR) {
                    for (Team team : mTeamsList) {
                        teams.add(String.valueOf(team.getId()));
                    }
                } else {
                    for (Team team : mTeamsList) {
                        teams.add(team.getName());
                    }
                }

                filter.setTeams(teams);

                state.setTeams(mTeamsList);

            }
        } else {
            if(mTeamsList != null && mTeamsList.size() > 0) {
                state.setTeams(mTeamsList);
            }
        }

        int coachType = mCoachType;
        state.setCoachType(mCoachType);

//        if (!mPrivateSelected) {
//            state.setCoachType(Constants.COACH_TYPE.PRIVATE);
//            coachType = Constants.COACH_TYPE.PRIVATE;
//        } else {
//            state.setCoachType(Constants.COACH_TYPE.STAR);
//            coachType = Constants.COACH_TYPE.STAR;
//        }

        Intent intent = new Intent();
        intent.putExtra("filter", filter);
        intent.putExtra("filterState", state);
        intent.putExtra("coachType", coachType);

        setResult(RESULT_OK, intent);
        finish();
    }

    private void setMaleSelected() {
        if (mMaleSelected) {
            mMaleSelected = false;
            mBinder.relMan.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
//            mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvMan.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isMaleChecked = false;
        } else {
            mMaleSelected = true;
            mBinder.relMan.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvMan.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isMaleChecked = true;
        }
    }

    private void setFemaleSelected() {
        if (mFemaleSelected) {
            mFemaleSelected = false;
            mBinder.relWoman.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
//            mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvWoman.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isFemaleChecked = false;
        } else {
            mFemaleSelected = true;
            mBinder.relWoman.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvWoman.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isFemaleChecked = true;
        }
    }

    private void setPrivateSelected() {
        mPrivateSelected = true;
        mBinder.relPrivate.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_border_green_bg_grey));
//        mBinder.ivCheckPrivate.setImageResource(R.drawable.ic_check_selected);
        mBinder.tvPrivate.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

        mStarSelected = false;
        mBinder.relStar.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_grey));
//        mBinder.ivCheckStar.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvStar.setTextColor(ContextCompat.getColor(this, R.color.black_25));
//        }
    }

    private void setStarSelected() {
        mStarSelected = true;
        mBinder.relStar.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_border_green_bg_grey));
        mBinder.ivCheckStar.setImageResource(R.drawable.ic_check_selected);
        mBinder.tvStar.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

        mPrivateSelected = false;
        mBinder.relPrivate.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_grey));
        mBinder.ivCheckPrivate.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvPrivate.setTextColor(ContextCompat.getColor(this, R.color.black_25));

    }

    /*private void setPrivateSelected() {
        if (mPrivateSelected) {
            mPrivateSelected = false;
            mBinder.relPrivate.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
            mBinder.ivCheckPrivate.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvPrivate.setTextColor(ContextCompat.getColor(this, R.color.black_25));
        } else {
            mPrivateSelected = true;
            mBinder.relPrivate.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
            mBinder.ivCheckPrivate.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvPrivate.setTextColor(ContextCompat.getColor(this, R.color.green_gender));
        }
    }

    private void setStarSelected() {
        if (mStarSelected) {
            mStarSelected = false;
            mBinder.relStar.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
            mBinder.ivCheckStar.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvStar.setTextColor(ContextCompat.getColor(this, R.color.black_25));
        } else {
            mStarSelected = true;
            mBinder.relStar.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
            mBinder.ivCheckStar.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvStar.setTextColor(ContextCompat.getColor(this, R.color.green_gender));
        }
    }*/

    private void setHandsOnSelected() {
        if (mHandsOnSelected) {
            mHandsOnSelected = false;
            mBinder.relHandsOn.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
//            mBinder.ivCheckHandsOn.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvHandsOn.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isHandsOnChecked = false;

            mBinder.tvHandsOnInfo.setTextColor(ContextCompat.getColor(this,R.color.black_25));
            mBinder.tvHandsOnInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);

        } else {
            mHandsOnSelected = true;
            mBinder.relHandsOn.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckHandsOn.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvHandsOn.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isHandsOnChecked = true;

            mBinder.tvHandsOnInfo.setTextColor(ContextCompat.getColor(this,R.color.green_gender));
            mBinder.tvHandsOnInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
        }
    }

    private void setInspirationalSelected() {
        if (mInspirationalSelected) {
            mInspirationalSelected = false;
            mBinder.relInspirational.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
//            mBinder.ivCheckInspirational.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvInspirational.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isInspirationalChecked = false;

            mBinder.tvInspirationalInfo.setTextColor(ContextCompat.getColor(this,R.color.black_25));
            mBinder.tvInspirationalInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);

        } else {
            mInspirationalSelected = true;
            mBinder.relInspirational.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckInspirational.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvInspirational.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isInspirationalChecked = true;

            mBinder.tvInspirationalInfo.setTextColor(ContextCompat.getColor(this,R.color.green_gender));
            mBinder.tvInspirationalInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
        }
    }

    private void selectPrivate() {
        if (mSportsType == 2) {
//            mBinder.tvPrivate.setBackground(getResources().getDrawable(R.drawable
//                    .shape_rounded_green));
//            mBinder.tvStar.setBackground(getResources().getDrawable(R.drawable
//                    .shape_rounded_black));
//            mBinder.buttonFindCoaches.setText("Find private coaches around you");
//            mSportsType = 1;
        }
    }

    private void selectStar() {
        if (mSportsType == 1) {
//            mBinder.tvStar.setBackground(getResources().getDrawable(R.drawable
//                    .shape_rounded_green));
//            mBinder.tvPrivate.setBackground(getResources().getDrawable(R.drawable
//                    .shape_rounded_black));
//            mBinder.buttonFindCoaches.setText("Find star coaches around you");
//            mSportsType = 2;
        }
    }

    private void launchUpdateLocation() {
        Intent intent = new Intent(this, SearchCoachLocationActivity.class);
        intent.putExtra(Constants.EXTRA_SPORT, mSport);
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    /*private void launchSearchResults() {
        Coach coach = new Coach();
        coach.setCoachType(mSportsType);

        Intent intent = new Intent(this, SearchCoachResultsActivity.class);
        intent.putExtra(Constants.EXTRA_SPORT, mSport);
        intent.putExtra(Constants.EXTRA_COACH, coach);
        intent.putExtra(Constants.EXTRA_LATITUDE, mLatitude);
        intent.putExtra(Constants.EXTRA_LONGITUDE, mLongitude);
        intent.putExtra(Constants.EXTRA_ADDRESS, mAddress);

        startActivity(intent);
    }*/
}
