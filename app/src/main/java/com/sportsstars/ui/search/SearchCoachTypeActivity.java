package com.sportsstars.ui.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoachTypeBinding;
import com.sportsstars.eventbus.LocationEvent;
import com.sportsstars.eventbus.LocationPermission;
import com.sportsstars.model.Coach;
import com.sportsstars.model.Sports;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ram on 20/01/18.
 */

public class SearchCoachTypeActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(SearchCoachTypeActivity.class);
    private static final int REQUEST_CODE_LOCATION = 101;

    private ActivitySearchCoachTypeBinding mBinder;
    private Sports mSport;
    private int mSportsType = Constants.COACH_TYPE.STAR; //2.Private, 1.Star
    private double mLatitude;
    private double mLongitude;
    private String mAddress;
    private Geocoder geocoder;
    private boolean isLocationFound;

    @Override
    public String getActivityName() {
        return SearchCoachTypeActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_coach_type);
        mBinder.headerId.tvTitle.setText(R.string.search_for_coach);

        init();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        //showProgressBar();
        LocationUtils.addFragment(this, this);

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        mSportsType = Constants.COACH_TYPE.STAR;
        selectStar();

        Utils.setTransparentTheme(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void init() {
        mBinder.headerId.ivBack.setOnClickListener(this);
        mBinder.tvPrivate.setOnClickListener(this);
        mBinder.tvStar.setOnClickListener(this);
        mBinder.buttonFindCoaches.setOnClickListener(this);
        mBinder.tvUpdateLocation.setOnClickListener(this);
        mBinder.tvStarInfo.setOnClickListener(this);
        mBinder.tvPrivateInfo.setOnClickListener(this);
        mBinder.tvSportName.setOnClickListener(this);

//        Drawable[] drawables = mBinder.tvSportName.getCompoundDrawables();

        if (getIntent() != null) {
            mSport = getIntent().getParcelableExtra(Constants.EXTRA_SPORT);

            if (mSport != null) {
                mBinder.tvSportName.setText(mSport.getName());

                if(!TextUtils.isEmpty(mSport.getBackgroundImage())) {
                    Picasso.with(this)
                            .load(mSport.getBackgroundImage())
//                            .placeholder(R.drawable.ic_user_circle)
//                            .error(R.drawable.ic_user_circle)
                            .into((mBinder.ivBackground));
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.tv_private:
//                showSnackbarFromTop(SearchCoachTypeActivity.this, "Coming Soon");

                mSportsType = Constants.COACH_TYPE.PRIVATE;
                selectPrivate();
                break;

            case R.id.tv_star:
                mSportsType = Constants.COACH_TYPE.STAR;
                selectStar();
                break;

            case R.id.tv_update_location:
                launchUpdateLocation();
                break;

            case R.id.button_find_coaches:
                launchSearchResults();
                break;

            case R.id.tv_star_info:
                showStarInfoDialog();
//                if(mSportsType == Constants.COACH_TYPE.STAR) {
//                    showStarInfoDialog();
//                }
                break;

            case R.id.tv_private_info:
                showPrivateInfoDialog();
//                if(mSportsType == Constants.COACH_TYPE.PRIVATE) {
//                    showPrivateInfoDialog();
//                }
                break;

            case R.id.tv_sport_name:
                finish();
                break;
        }
    }

    private void showStarInfoDialog() {
        String title = getResources().getString(R.string.star_coach);
        String message = getResources().getString(R.string.star_coach_info_msg);
        Alert.showInfoDialog(SearchCoachTypeActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showPrivateInfoDialog() {
        String title = getResources().getString(R.string.private_coach);
        String message = getResources().getString(R.string.private_coach_info_msg);
        Alert.showInfoDialog(SearchCoachTypeActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        try {
            //hideProgressBar();
            isLocationFound = true;
            LogUtils.LOGD(TAG, "onEvent location update...");
            Location location = event.getMessage();
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();

            mAddress = getGeocodeAddress(location.getLatitude(), location.getLongitude());
            mBinder.tvAddress.setText(mAddress);
            Log.d("location_log", "SearchCoachType - mAddressString : " + mAddress
                    + " latitude : " + location.getLatitude() + " longitude : " + location.getLongitude());
            try {
                if(!TextUtils.isEmpty(mAddress) && mAddress.equalsIgnoreCase("Unknown")) {
                    Alert.showInfoDialog(SearchCoachTypeActivity.this,
                            getResources().getString(R.string.address_not_found_title),
                            getResources().getString(R.string.address_not_found_desc),
                            getResources().getString(R.string.okay),
                            new Alert.OnOkayClickListener() {
                                @Override
                                public void onOkay() {
                                }});
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onEvent(LocationPermission permission) {
        //hideProgressBar();
        isLocationFound = false;
        showToast(getString(R.string.location_permission_not_granted));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_LOCATION:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        mLatitude = data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0);
                        mLongitude = data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0);
                        mAddress = data.getStringExtra(Constants.EXTRA_ADDRESS);

                        if (mAddress != null) {
                            Log.d("location_log", "SearchCoachType - mAddressString : " + mAddress
                                    + " latitude : " + mLatitude + " longitude : " + mLongitude);
                            mBinder.tvAddress.setText(mAddress);
                        }
                    }
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_COACH:
                if(resultCode == RESULT_OK) {
                    finish();
                }
                break;
        }
    }

    private void selectPrivate() {
        if(mSportsType == Constants.COACH_TYPE.STAR) {
            mSportsType = Constants.COACH_TYPE.STAR;
            selectStar();
        }

        if (mSportsType == Constants.COACH_TYPE.PRIVATE) {
            mBinder.tvPrivate.setBackground(getResources().getDrawable(R.drawable
                    .shape_rounded_green_without_fill));
            mBinder.tvPrivate.setTextColor(ContextCompat.getColor(this,R.color.green_gender));


            mBinder.tvStar.setBackground(getResources().getDrawable(R.drawable
                    .shape_rounded_grey));
            mBinder.tvStar.setTextColor(ContextCompat.getColor(this,R.color.black_25));

            mBinder.buttonFindCoaches.setText(getString(R.string.find_private_coaches));
            mSportsType = Constants.COACH_TYPE.PRIVATE;

            mBinder.tvPrivateInfo.setTextColor(ContextCompat.getColor(this,R.color.green_gender));
            mBinder.tvPrivateInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);

            mBinder.tvStarInfo.setTextColor(ContextCompat.getColor(this,R.color.black_25));
            mBinder.tvStarInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);
        }
    }

    private void selectStar() {
        if (mSportsType == Constants.COACH_TYPE.STAR) {
            mBinder.tvStar.setBackground(getResources().getDrawable(R.drawable
                    .shape_rounded_green_without_fill));
            mBinder.tvStar.setTextColor(ContextCompat.getColor(this,R.color.green_gender));

            mBinder.tvPrivate.setBackground(getResources().getDrawable(R.drawable
                    .shape_rounded_grey));
            mBinder.tvPrivate.setTextColor(ContextCompat.getColor(this,R.color.black_25));

            mBinder.buttonFindCoaches.setText(getString(R.string.find_star_coaches));
            mSportsType = Constants.COACH_TYPE.STAR;

            mBinder.tvStarInfo.setTextColor(ContextCompat.getColor(this,R.color.green_gender));
            mBinder.tvStarInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);

            mBinder.tvPrivateInfo.setTextColor(ContextCompat.getColor(this,R.color.black_25));
            mBinder.tvPrivateInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);
        }
    }

    private void launchUpdateLocation() {
        Intent intent = new Intent(this, SearchCoachLocationActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    private void launchSearchResults() {

        if(mBinder.tvAddress.getText() != null && !TextUtils.isEmpty(mBinder.tvAddress.getText())) {
            Coach coach = new Coach();
            coach.setCoachType(mSportsType);

            Intent intent = new Intent(this, SearchCoachResultsActivity.class);
            intent.putExtra(Constants.EXTRA_SPORT, mSport);
            intent.putExtra(Constants.EXTRA_COACH, coach);
            intent.putExtra(Constants.EXTRA_LATITUDE, mLatitude);
            intent.putExtra(Constants.EXTRA_LONGITUDE, mLongitude);
            intent.putExtra(Constants.EXTRA_ADDRESS, mAddress);

            startActivityForResult(intent, Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_COACH);
        } else {
            if(!isLocationFound) {
                //showSnackbarFromTop(this,getString(R.string.pls_choose_loc));
            }
        }
    }

    private String getGeocodeAddress(double latitude, double longitude) {
        List<Address> addresses = null;
        String address = "Unknown";

        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
//            errorMessage = getString(R.string.service_not_available);
//            Log.e(TAG, errorMessage, ioException);
            LogUtils.LOGE(TAG, ioException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
//            errorMessage = getString(R.string.invalid_lat_long_used);
//            Log.e(TAG, errorMessage + ". " +
//                    "Latitude = " + location.getLatitude() +
//                    ", Longitude = " +
//                    location.getLongitude(), illegalArgumentException);
            LogUtils.LOGE(TAG, illegalArgumentException.getMessage());

        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }

        // Handle case where no address was found.
        if (addresses != null && addresses.size() != 0) {
            address = addresses.get(0).getAddressLine(0);
        }

        return address;
    }

}
