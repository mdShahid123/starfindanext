package com.sportsstars.ui.search;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoachLocationBinding;
import com.sportsstars.eventbus.LocationEvent;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ram on 10/01/18.
 */
public class SearchCoachLocationActivity extends BaseActivity implements View.OnClickListener,
        OnMapReadyCallback, GoogleMap.OnMarkerDragListener, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LogUtils.makeLogTag(SearchCoachLocationActivity.class);
    private ActivitySearchCoachLocationBinding mBinder;
    private GoogleMap mMap;
    private Geocoder geocoder;
    private Location mLocation;
    private String mAddressString;
    private String mCityString;
    private String mZipCodeString;
    private AutoCompleteTextView mAutocompleteView;
    private PlaceAutocompleteAdapter mAdapter;
    private GoogleApiClient mGoogleApiClient;
    private double mLat;
    private double mLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_coach_location);

        mBinder.tvConfirm.setOnClickListener(this);
        mBinder.ivBack.setOnClickListener(this);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
           String title = bundle.getString(Constants.EXTRA_TITLE);
           mBinder.tvTitle.setText(title);

        }

        mBinder.autocompletePlaces.setOnItemClickListener(mAutocompleteClickListener);
        mBinder.imgClear.setOnClickListener(this);

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //showProgressBar();
        LocationUtils.addFragment(this, this);

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        /*
          Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
          functionality, which automatically sets up the API client to handle Activity lifecycle
          events. If your activity does not extend FragmentActivity, make sure to call connect()
          and disconnect() explicitly.
         */
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        /*
          Register a listener that receives callbacks when a suggestion has been selected
         */
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        /*
          Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
          the entire world.
         */

        AutocompleteFilter filter =
                new AutocompleteFilter.Builder()
                        .setCountry("AU")
                        .build();

        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, null, filter);
        mAutocompleteView.setAdapter(mAdapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_confirm:
                if (mLocation != null) {
//                    Intent intent = new Intent();
//                    intent.putExtra(Constants.EXTRA_LATITUDE, mLocation.getLatitude());
//                    intent.putExtra(Constants.EXTRA_LONGITUDE, mLocation.getLongitude());
//                    intent.putExtra(Constants.EXTRA_ADDRESS, mAddressString);
//                    setResult(RESULT_OK, intent);

                    setResult(RESULT_OK, prepareIntent());
                    finish();
                } else {
                    setResult(RESULT_OK, prepareIntent());

                    //setResult(RESULT_OK);
                }

                finish();
                break;

            case R.id.iv_back:
                finish();
                break;

            case R.id.img_clear:
                mAutocompleteView.setText("");
                break;


        }

    }

    private Intent prepareIntent() {

        Intent intent = new Intent();
        if(mLocation!=null) {
            intent.putExtra(Constants.EXTRA_LATITUDE,  mLat);
            intent.putExtra(Constants.EXTRA_LONGITUDE, mLong);
            intent.putExtra(Constants.EXTRA_ADDRESS, mAddressString);
            intent.putExtra(Constants.EXTRA_CITY_NAME, mCityString);
            intent.putExtra(Constants.EXTRA_POSTAL_CODE, mZipCodeString);
        } else {
            intent.putExtra(Constants.EXTRA_LATITUDE,  mLat);
            intent.putExtra(Constants.EXTRA_LONGITUDE, mLong);
            intent.putExtra(Constants.EXTRA_ADDRESS, mAddressString);
            intent.putExtra(Constants.EXTRA_CITY_NAME, mCityString);
            intent.putExtra(Constants.EXTRA_POSTAL_CODE, mZipCodeString);
        }

//        intent.putExtra(Constants.EXTRA_PLACE_NAME, mPlaceName);
//        intent.putExtra(Constants.EXTRA_POSTAL_CODE, mPostalCode);
//        intent.putExtra(Constants.EXTRA_LATITUDE, mLatitude);
//        intent.putExtra(Constants.EXTRA_LONGITUDE, mLongitude);
//        intent.putExtra(Constants.EXTRA_CITY_NAME, mCity);
//        intent.putExtra(Constants.EXTRA_STATE_NAME, mState);
//        intent.putExtra(Constants.EXTRA_COUNTRY_NAME, mCountry);

//        LogUtils.LOGD(TAG, "Postal Code " + mPostalCode + ", Place " + mPlaceName);

        return intent;
    }

    @Override
    public String getActivityName() {
        return null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LogUtils.LOGD(TAG, "Map is ready");
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        mMap.setMyLocationEnabled(true);
//        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        LogUtils.LOGD(TAG, "onPointerCaptureChanged");

    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        try {
            //hideProgressBar();
            LogUtils.LOGD(TAG, "onEvent location update...");
            mLocation = event.getMessage();
            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            moveCamera(latLng);

            mLat = mLocation.getLatitude();
            mLong = mLocation.getLongitude();

            mAddressString = getGeocodeAddress(mLocation.getLatitude(), mLocation.getLongitude());
            Log.d("location_log", "SearchCoachLocation - mAddressString : " + mAddressString
                    + " latitude : " + mLat + " longitude : " + mLong);
            mBinder.tvAddress.setText(mAddressString);

            addMarker(latLng);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMarker(LatLng latLng) {
        mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory
                .fromResource(R.drawable.ic_map_pin_black))).setDraggable(true);
        mMap.setOnMarkerDragListener(this);
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             * Retrieve the place ID of the selected item from the Adapter.
             * The adapter stores each Place suggestion in a AutocompletePrediction from which we
             * read the place ID and title.
             */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            LogUtils.LOGD(TAG, "Autocomplete item selected: " + primaryText);

            /*
             * Issue a request to the Places Geo Data API to retrieve a Place object with additional
             * details about the place.
             */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            LogUtils.LOGD(TAG, "Called getPlaceById to get Place details for " + placeId);
            hideKeyboard();
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                LogUtils.LOGE(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }

            /**
             * Get the Place object from the buffer.
             */
            final Place place = places.get(0);
            LatLng latLng = place.getLatLng();
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
            mMap.clear();
//            mMap.addMarker(new MarkerOptions().position(latLng));

            addMarker(latLng);

            moveCamera(latLng);

            mLat = latLng.latitude;
            mLong = latLng.longitude;

            mAddressString = getGeocodeAddress(latLng.latitude, latLng.longitude);
            mBinder.tvAddress.setText(mAddressString);

            LogUtils.LOGD(TAG, latLng.toString());

//            Address address = Utils.getReverseGeoCode(PlacesMapActivity.this, latLng);
//
//            setData(address);

            places.release();
        }
    };

    private void moveCamera(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    }

    private String getGeocodeAddress(double latitude, double longitude) {
        List<Address> addresses = null;
        String address = "Unknown";

        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
//            errorMessage = getString(R.string.service_not_available);
//            Log.e(TAG, errorMessage, ioException);
            LogUtils.LOGE(TAG, ioException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
//            errorMessage = getString(R.string.invalid_lat_long_used);
//            Log.e(TAG, errorMessage + ". " +
//                    "Latitude = " + location.getLatitude() +
//                    ", Longitude = " +
//                    location.getLongitude(), illegalArgumentException);
            LogUtils.LOGE(TAG, illegalArgumentException.getMessage());

        }

        // Handle case where no address was found.
        if (addresses != null && addresses.size() != 0) {
            address = addresses.get(0).getAddressLine(0);
            mZipCodeString = addresses.get(0).getPostalCode();
            if(addresses.get(0).getLocality() != null
                    && !TextUtils.isEmpty(addresses.get(0).getLocality())) {
                mCityString = addresses.get(0).getLocality();
            }

            if(TextUtils.isEmpty(mCityString)) {
                if(addresses.get(0).getSubAdminArea() != null
                        && !TextUtils.isEmpty(addresses.get(0).getSubAdminArea())) {
                    mCityString = addresses.get(0).getSubAdminArea();
                }
            }

            if(TextUtils.isEmpty(mCityString)) {
                if(addresses.get(0).getAdminArea() != null
                        && !TextUtils.isEmpty(addresses.get(0).getAdminArea())) {
                    mCityString = addresses.get(0).getAdminArea();
                }
            }
            Log.d("address_log", " Address datas - "
                    //+ " getPremises : " + addresses.get(0).getPremises()
                    //+ " getFeatureName : " + addresses.get(0).getFeatureName()
                    + " getAdminArea : " + addresses.get(0).getAdminArea()
                    + " getSubAdminArea : " + addresses.get(0).getSubAdminArea()
                    + " getLocality : " + addresses.get(0).getLocality()
                    //+ " getSubLocality : " + addresses.get(0).getSubLocality()
                    //+ " getThoroughfare : " + addresses.get(0).getThoroughfare()
                    //+ " getSubThoroughfare : " + addresses.get(0).getSubThoroughfare()
                    + " getPostalCode : " + addresses.get(0).getPostalCode()
                    + " mCityString : " + mCityString
                    + " mZipCodeString : " + mZipCodeString
            );
        }

        return address;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        try {
            if(marker != null && marker.getPosition() != null) {
                mLat = marker.getPosition().latitude;
                mLong = marker.getPosition().longitude;
                mAddressString = getGeocodeAddress(marker.getPosition().latitude, marker.getPosition()
                        .longitude);
                mBinder.tvAddress.setText(mAddressString);

                //Clear the autocomplete text view.
                mBinder.autocompletePlaces.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

