package com.sportsstars.ui.search;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentSearchCoachByNameNextBinding;
import com.sportsstars.model.Coach;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SearchCoachBySportResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.session.SessionListActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by ramkumar on 19/01/18.
 */

public class FragmentSearchCoachByNameNext extends BaseFragment implements Runnable,TextWatcher{
    private static final String TAG = LogUtils.makeLogTag(FragmentSearchCoachByNameNext.class);
    private WeakReference<SearchCoachActivityNext> mActivity;
    private FragmentSearchCoachByNameNextBinding mBinder;
    private ArrayList<Coach>mCoachList;
    private ImageAdapter mImageAdapter;
    private Handler mHandler;
    private String mSearchText="";

    public static FragmentSearchCoachByNameNext newInstance() {
        FragmentSearchCoachByNameNext fragment = new FragmentSearchCoachByNameNext();
        Bundle args = new Bundle();
//        args.putString(AppConstants.ARG_PARAM2, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((SearchCoachActivityNext) getActivity());

        if (getArguments() != null) {
//            mUserId = getArguments().getString(AppConstants.ARG_PARAM2);
        }
        mHandler = new Handler(Looper.getMainLooper());

        //if( isAdded() ) {
            getCoachList(mSearchText, true);
        //}
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_search_coach_by_name_next,
                container, false);

        init();
        return mBinder.getRoot();
    }



    @Override
    public String getFragmentName() {
        return null;
    }

    private void init(){

        mBinder.etSearchBy.addTextChangedListener(this);

        mBinder.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                Bundle bundle= new Bundle();
                bundle.putParcelable(Constants.EXTRA_COACH,mCoachList.get(pos));
                Navigator.getInstance().navigateToActivityWithData(mActivity.get(),SessionListActivity.class,bundle);

            }
        });

    }

    private void getCoachList(String searchText,boolean showLoader) {
        mCoachList=new ArrayList<>();
        GetSportsRequest request = new GetSportsRequest();

        if(showLoader) {
            showProgressBar();
        }

        request.setName(searchText);
        request.setPage(1);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.coachList(request).enqueue(new BaseCallback<SearchCoachBySportResponse>(mActivity.get()) {
            @Override
            public void onSuccess(SearchCoachBySportResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getCoaches() != null) {
                        mCoachList = response.getResult().getCoaches();
                        mImageAdapter = new FragmentSearchCoachByNameNext.ImageAdapter(getContext(), mCoachList);
                        mBinder.gridview.setAdapter(mImageAdapter);
                    }
                }
            }

            @Override
            public void onFail(Call<SearchCoachBySportResponse> call, BaseResponse baseResponse) {
                try {
                    if(mActivity != null) {
                        mActivity.get().hideProgressBar();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void run() {
        getCoachList(mSearchText,false);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        mHandler.removeCallbacks(this);
        mHandler.postDelayed(this, 500);
        mSearchText=charSequence.toString();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private ArrayList<Coach> coaches;

        public ImageAdapter(Context c, ArrayList<Coach> coaches) {
            mContext = c;
            this.coaches = coaches;
        }

        public int getCount() {
            return coaches.size();
        }

        public Object getItem(int position) {
            return coaches.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                view = new View(mContext);
                view = inflater.inflate(R.layout.item_coach_grid, null);

                TextView tvCoachName = (TextView) view
                        .findViewById(R.id.tv_coach_name);
                ImageView imageCoach = (ImageView) view
                        .findViewById(R.id.image_coach);
                //tvCoachName.setText(mobileValues[position]);


                tvCoachName.setText(coaches.get(position).getName());

                if(!TextUtils.isEmpty(coaches.get(position).getProfileImage())){

                    Picasso.with(mActivity.get())
                            .load(coaches.get(position).getProfileImage())
                            .placeholder(R.drawable.ic_user_circle)
                            .error(R.drawable.ic_user_circle)
                            .into(( imageCoach));

                }

            } else {
               // view = convertView;
                view = (View) convertView;

            }

            return view;
        }
    }


}
