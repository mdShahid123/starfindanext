package com.sportsstars.ui.search;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoachSpeakerBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.speaker.search.SearchSpeakerTypeActivity;
import com.sportsstars.ui.speaker.search.SelectEventTypeActivity;
import com.sportsstars.ui.speaker.search.SelectSpeakerSportsActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;

import retrofit2.Call;

/**
 * Created by Ram on 14/01/18.
 */

public class SearchCoachSpeakerActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(SearchCoachSpeakerActivity.class);
    private ActivitySearchCoachSpeakerBinding mBinder;

    @Override
    public String getActivityName() {
        return SearchCoachSpeakerActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_coach_speaker);
        mBinder.headerId.tvTitle.setText(R.string.search);

        init();
    }

    private void init() {
        mBinder.headerId.ivBack.setOnClickListener(this);
        mBinder.relCoachMain.setOnClickListener(this);
        mBinder.relSpeakMain.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.rel_coach_main:
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, true);
                Navigator.getInstance().navigateToActivityWithData(this, SearchCoachActivity.class, bundle);
                break;

            case R.id.rel_speak_main:
//                showSnackbarFromTop(SearchCoachSpeakerActivity.this, "Coming Soon");

                Bundle bundleSpeaker = new Bundle();
                bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
                bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
                //Navigator.getInstance().navigateToActivityWithData(this, SearchSpeakerTypeActivity.class, bundleSpeaker);
                Navigator.getInstance().navigateToActivityWithData(this, SelectEventTypeActivity.class, bundleSpeaker);
                //Navigator.getInstance().navigateToActivityWithData(this, SelectSpeakerSportsActivity.class, bundleSpeaker);
                break;
        }
    }

    private void logoutApi() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.logoutUser().enqueue(new BaseCallback<BaseResponse>(SearchCoachSpeakerActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showToast(response.getMessage());
                    }
                    //mBinder.tvEmailInfo.setVisibility(View.VISIBLE);
                    localLogOut();
                    finish();
                } else {
                    if(response != null && response.getMessage() != null
                            && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(SearchCoachSpeakerActivity.this, response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SearchCoachSpeakerActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
