package com.sportsstars.ui.search;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoach2Binding;
import com.sportsstars.model.Sports;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Utils;

import java.util.ArrayList;

/**
 * Created by Ram on 14/01/18.
 */

public class SearchCoachActivityNext extends BaseActivity implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(SearchCoachActivityNext.class);
    private ActivitySearchCoach2Binding mBinder;
    private ArrayList<Sports> mSportsList;
    private static final int BY_SPORT = 1;
    private static final int BY_NAME = 2;
    private int mSelectedTab;
    private boolean isCoachSearch;

    @Override
    public String getActivityName() {
        return SearchCoachActivityNext.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_coach_2);

        Bundle bundleCoach = getIntent().getExtras();
        if(bundleCoach != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_COACH_SEARCH)) {
                isCoachSearch = bundleCoach.getBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                if(isCoachSearch) {
                    mBinder.layoutTop.headerId.tvTitle.setText(R.string.search_for_coach_by_sport);
                } else {
                    mBinder.layoutTop.headerId.tvTitle.setText(R.string.search_for_speaker_by_sport);
                }
            } else {
                mBinder.layoutTop.headerId.tvTitle.setText(R.string.search_for_coach_by_sport);
            }
        } else {
            mBinder.layoutTop.headerId.tvTitle.setText(R.string.search_for_coach_by_sport);
        }

        init();

        Utils.setTransparentTheme(this);

//        getSportsList();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean isByName = bundle.getBoolean(Constants.EXTRA_IS_SEARCH_BY_NAME);
            if (isByName) {
                searchByName();
                setSearchType(BY_NAME);

            } else {
                searchBySport();
                setSearchType(BY_SPORT);
            }
        } else {
            searchBySport();
            setSearchType(BY_SPORT);
        }


    }

    private void init() {
        mBinder.layoutTop.headerId.ivBack.setOnClickListener(this);
        mBinder.layoutTop.tvByName.setOnClickListener(this);
        mBinder.layoutTop.tvBySport.setOnClickListener(this);
//        mBinder.tvSearchBy.setOnClickListener(this);

//        mLayoutInflater = LayoutInflater.from(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_by_name:
                if (mSelectedTab == BY_NAME) {
                    return;
                }
                searchByName();
                setSearchType(BY_NAME);
                break;

            case R.id.tv_by_sport:
                if (mSelectedTab == BY_SPORT) {
                    return;
                }
                searchBySport();
                setSearchType(BY_SPORT);

                break;

            case R.id.tv_search_by:
                searchBySport();

                break;
        }
    }

    private void searchByName() {
        mBinder.layoutTop.tvByName.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_green_8_2));
        mBinder.layoutTop.tvByName.setTextColor(ContextCompat.getColor(this, R.color.black));

        mBinder.layoutTop.tvBySport.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_border_green_3));
        mBinder.layoutTop.tvBySport.setTextColor(ContextCompat.getColor(this, R.color.white_color));

//        mBinder.tvSearchBy.setText(getString(R.string.search_by_sport));
    }

    private void searchBySport() {
        mBinder.layoutTop.tvBySport.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_green_8_2));
        mBinder.layoutTop.tvBySport.setTextColor(ContextCompat.getColor(this, R.color.black));

        mBinder.layoutTop.tvByName.setBackground(ContextCompat.getDrawable(this, R.drawable
                .shape_rounded_border_green_3));
        mBinder.layoutTop.tvByName.setTextColor(ContextCompat.getColor(this, R.color.white_color));

//        mBinder.tvSearchBy.setText(getString(R.string.search_by_coach));
    }

    /*private void addTitleToLayout(final Sports mSports) {
        String text = mSports.getName();
        int pos = 0;
        for (int i = 0; i < mSportsList.size(); i++) {
            if (mSports.getName().equals(mSportsList.get(i).getName())) {
                pos = i;
                break;
            }
        }


        final ItemFlowSportsBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from(mBinder.flowLayoutSports.getContext())
                .inflate(R.layout.item_flow_sports, mBinder.flowLayoutSports, false));

        flowBinding.flowChild.setText(text);

        final Drawable imgMinus = getResources().getDrawable(R.drawable.ic_minus_green);
        final Drawable imgPlus = getResources().getDrawable(R.drawable.ic_plus_green);

        final int finalPos = pos;
        if (mSportsList.get(finalPos).isSelected()) {
            flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgMinus, null);

        } else {
            flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPlus, null);

        }

        flowBinding.flowChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSportsList.get(finalPos).isSelected()) {
                    mSportsList.get(finalPos).setSelected(false);
                    flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPlus, null);
                } else {
                    mSportsList.get(finalPos).setSelected(true);
                    flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgMinus, null);
                }
            }
        });

        mBinder.flowLayoutSports.addView(flowBinding.getRoot());

    }*/

    private void setSearchType(int searchType) {
        mSelectedTab = searchType;
        switch (searchType) {
            case BY_SPORT:
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.FROM_LEARNER_PROFILE, getIntent().getBooleanExtra(Constants.FROM_LEARNER_PROFILE, false));
                bundle.putInt(Constants.BundleKey.INDEX, getIntent().getIntExtra(Constants.BundleKey.INDEX, -1));
                bundle.putBoolean(Constants.FROM_COACH_INFO, getIntent().getBooleanExtra(Constants.FROM_COACH_INFO, false));
                bundle.putBoolean(Constants.BundleKey.FROM_ADD_TIP, getIntent().getBooleanExtra(Constants.BundleKey.FROM_ADD_TIP, false));
                bundle.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, isCoachSearch);

                pushFragment(Constants.FRAGMENTS.SEARCH_BY_SPORT2_FRAGMENT, bundle, R.id.frame_search_by,
                        false);
                break;

            case BY_NAME:
                pushFragment(Constants.FRAGMENTS.SEARCH_BY_NAME2_FRAGMENT, null, R.id
                        .frame_search_by, false);
                break;

        }
    }

    /*private void getSportsList() {
        processToShowDialog();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>
                (SearchCoachActivity2.this) {
            @Override
            public void onSuccess(SportsListResponse response) {

                if (response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        mSportsList = response.getResult().getSportsList();

                        for (Sports item : mSportsList) {
                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getReferenceImageUrl());
                            addTitleToLayout(item);
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {

            }
        });
    }*/
}
