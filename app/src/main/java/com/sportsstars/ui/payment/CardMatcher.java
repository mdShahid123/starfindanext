package com.sportsstars.ui.payment;

import android.text.TextUtils;
import android.widget.ImageView;

import com.sportsstars.R;
import com.sportsstars.util.Constants;

class CardMatcher {

   /* private static final String VISA = "^4[0-9]{6,}$";
    private static final String MASTER_CARD = "^5[1-5][0-9]{5,}$";
    private static final String AMERICAN_EXPRESS = "^3[47][0-9]{5,}$";
    private static final String DINERS_CLUB = "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$";
    private static final String DISCOVER = "^6(?:011|5[0-9]{2})[0-9]{3,}$";
    public static final String JCB_REGEX="^(?:2131|1800|35[0-9]{3})[0-9]{3,}$";*/


    private static final int VISA_MAX_LENGTH = 19;
    private static final int AMERICAN_EXPRESS_MAX_LENGTH = 18;

    private static final int AMERICAN_EXPRESS_CVV = 4;
    private static final int VISA_CVV = 4;
    private static final int DINNERS_CLUB_MAX_LENGTH = 17;

    public static final String AMERICAN_EXPRESS = "American Express";
    public static final String DINERS = "Diners";
    public static final String DINERS_CLUB = "Diners Club";
    public static final String DISCOVER = "Discover";
    public static final String MASTER_CARD = "MasterCard";
    public static final String MASTER_CARD_WITH_SPACE = "Master Card";
    public static final String VISA = "Visa";
    public static final String JCB = "JCB";
    public static final String MAESTRO = "Maestro";
    public static final String UNIONPAY = "UnionPay";
    public static final String UNIONPAY_WITH_SPACE = "Union Pay";

    public static final int CARD_TYPE_UNKNOWN = 0;
    public static final int CARD_TYPE_AMERICAN_EXPRESS = 1;
    public static final int CARD_TYPE_DINERS_CLUB = 2;
    public static final int CARD_TYPE_DISCOVER = 3;
    public static final int CARD_TYPE_MASTER_CARD = 4;
    public static final int CARD_TYPE_VISA = 5;
    public static final int CARD_TYPE_JCB = 6;
    public static final int CARD_TYPE_MAESTRO = 7;
    public static final int CARD_TYPE_UNIONPAY = 8;

    public static CardInfo getCardType(String regex) {
        CardInfo cardInfo = new CardInfo();

        switch (regex) {
            case Constants.VISA:
                cardInfo.setCardType("VISA");
                cardInfo.setId(R.drawable.ic_visa);
                cardInfo.setMaxLength(VISA_MAX_LENGTH);
                cardInfo.setCvvLength(VISA_CVV);
                break;
            case Constants.MASTER_CARD:
                cardInfo.setCardType("MASTER_CARD");
                cardInfo.setId(R.drawable.ic_mastercard);
                cardInfo.setMaxLength(VISA_MAX_LENGTH);
                cardInfo.setCvvLength(VISA_CVV);
                break;
            case Constants.AMERICAN_EXPRESS:
                cardInfo.setCardType("AMERICAN_EXPRESS");
                cardInfo.setId(R.drawable.ic_amex);
                cardInfo.setMaxLength(AMERICAN_EXPRESS_MAX_LENGTH);
                cardInfo.setCvvLength(AMERICAN_EXPRESS_CVV);
                break;
            case Constants.DISCOVER:
                cardInfo.setCardType("DISCOVER");
                cardInfo.setId(R.drawable.ic_discover);
                cardInfo.setMaxLength(VISA_MAX_LENGTH);
                cardInfo.setCvvLength(VISA_CVV);
                break;

            case Constants.DINERS_CLUB:
                cardInfo.setCardType("DINERS_CLUB");
                cardInfo.setId(R.drawable.ic_diners);
                cardInfo.setMaxLength(DINNERS_CLUB_MAX_LENGTH);
                cardInfo.setCvvLength(VISA_CVV);
                break;

            case Constants.JCB_REGEX:
                cardInfo.setCardType("JCB");
                cardInfo.setId(R.drawable.ic_jcb);
                cardInfo.setMaxLength(VISA_MAX_LENGTH);
                cardInfo.setCvvLength(VISA_CVV);
                break;
        }
        return cardInfo;
    }

    public static int getCardTypeId(String cardBrandName) {
        int cardTypeId = CARD_TYPE_UNKNOWN;
        if(cardBrandName.equalsIgnoreCase(AMERICAN_EXPRESS)) {
            cardTypeId = CARD_TYPE_AMERICAN_EXPRESS;
        } else if(cardBrandName.equalsIgnoreCase(DINERS)) {
            cardTypeId = CARD_TYPE_DINERS_CLUB;
        } else if(cardBrandName.equalsIgnoreCase(DINERS_CLUB)) {
            cardTypeId = CARD_TYPE_DINERS_CLUB;
        } else if(cardBrandName.equalsIgnoreCase(DISCOVER)) {
            cardTypeId = CARD_TYPE_DISCOVER;
        } else if(cardBrandName.equalsIgnoreCase(MASTER_CARD)) {
            cardTypeId = CARD_TYPE_MASTER_CARD;
        } else if(cardBrandName.equalsIgnoreCase(MASTER_CARD_WITH_SPACE)) {
            cardTypeId = CARD_TYPE_MASTER_CARD;
        } else if(cardBrandName.equalsIgnoreCase(VISA)) {
            cardTypeId = CARD_TYPE_VISA;
        } else if(cardBrandName.equalsIgnoreCase(JCB)) {
            cardTypeId = CARD_TYPE_JCB;
        } else if(cardBrandName.equalsIgnoreCase(MAESTRO)) {
            cardTypeId = CARD_TYPE_MAESTRO;
        } else if(cardBrandName.equalsIgnoreCase(UNIONPAY)) {
            cardTypeId = CARD_TYPE_UNIONPAY;
        } else if(cardBrandName.equalsIgnoreCase(UNIONPAY_WITH_SPACE)) {
            cardTypeId = CARD_TYPE_UNIONPAY;
        } else {
            cardTypeId = CARD_TYPE_UNKNOWN;
        }

        return cardTypeId;
    }

    public static String getCardNameByCardType(int cardTypeId) {
        String cardName = "";
        switch (cardTypeId) {
            case CARD_TYPE_AMERICAN_EXPRESS:
                cardName = AMERICAN_EXPRESS;
                break;
            case CARD_TYPE_DINERS_CLUB:
                cardName = DINERS_CLUB;
                break;
            case CARD_TYPE_DISCOVER:
                cardName = DISCOVER;
                break;
            case CARD_TYPE_MASTER_CARD:
                cardName = MASTER_CARD;
                break;
            case CARD_TYPE_VISA:
                cardName = VISA;
                break;
            case CARD_TYPE_JCB:
                cardName = JCB;
                break;
            case CARD_TYPE_MAESTRO:
                cardName = MAESTRO;
                break;
            case CARD_TYPE_UNIONPAY:
                cardName = UNIONPAY;
                break;
            case CARD_TYPE_UNKNOWN:
                cardName = "CARD DETAILS";
                break;
            default:
                cardName = "CARD DETAILS";
                break;
        }

        return cardName;
    }

    public static void setCreditCardIcon(ImageView ivCreditCardIcon, int cardTypeId) {
        switch (cardTypeId) {
            case CARD_TYPE_AMERICAN_EXPRESS:
                ivCreditCardIcon.setImageResource(R.drawable.ic_amex);
                break;
            case CARD_TYPE_DINERS_CLUB:
                ivCreditCardIcon.setImageResource(R.drawable.ic_diners);
                break;
            case CARD_TYPE_DISCOVER:
                ivCreditCardIcon.setImageResource(R.drawable.ic_discover);
                break;
            case CARD_TYPE_MASTER_CARD:
                ivCreditCardIcon.setImageResource(R.drawable.ic_mastercard);
                break;
            case CARD_TYPE_VISA:
                ivCreditCardIcon.setImageResource(R.drawable.ic_visa);
                break;
            case CARD_TYPE_JCB:
                ivCreditCardIcon.setImageResource(R.drawable.ic_jcb);
                break;
            case CARD_TYPE_MAESTRO:
                ivCreditCardIcon.setImageResource(R.drawable.ic_maestro);
                break;
            case CARD_TYPE_UNIONPAY:
                ivCreditCardIcon.setImageResource(R.drawable.bt_ic_vaulted_unionpay);
                break;
            case CARD_TYPE_UNKNOWN:
                ivCreditCardIcon.setImageResource(R.drawable.ic_default_card);
                break;
            default:
                ivCreditCardIcon.setImageResource(R.drawable.ic_default_card);
                break;
        }
    }

    public static void setCreditCardIcon(ImageView ivCreditCardIcon, String creditCardNumber) {
        if(creditCardNumber.matches(Constants.VISA)) {
            ivCreditCardIcon.setImageResource(R.drawable.ic_visa);
        } else if(creditCardNumber.matches(Constants.MASTER_CARD)) {
            ivCreditCardIcon.setImageResource(R.drawable.ic_mastercard);
        } else if(creditCardNumber.matches(Constants.AMERICAN_EXPRESS)) {
            ivCreditCardIcon.setImageResource(R.drawable.ic_amex);
        } else if(creditCardNumber.matches(Constants.DINERS_CLUB)) {
            ivCreditCardIcon.setImageResource(R.drawable.ic_diners);
        } else if(creditCardNumber.matches(Constants.DISCOVER)) {
            ivCreditCardIcon.setImageResource(R.drawable.ic_discover);
        } else if(creditCardNumber.matches(Constants.JCB_REGEX)) {
            ivCreditCardIcon.setImageResource(R.drawable.ic_jcb);
        } else {
            ivCreditCardIcon.setImageResource(R.drawable.ic_default_card);
        }
    }

    public static void setCreditCardIconByBrandName(ImageView ivCreditCardIcon,String cardBrandName) {
        if(cardBrandName != null && !TextUtils.isEmpty(cardBrandName)) {
            if(cardBrandName.equalsIgnoreCase(AMERICAN_EXPRESS)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_amex);
            } else if(cardBrandName.equalsIgnoreCase(DINERS)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_diners);
            } else if(cardBrandName.equalsIgnoreCase(DINERS_CLUB)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_diners);
            } else if(cardBrandName.equalsIgnoreCase(DISCOVER)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_discover);
            } else if(cardBrandName.equalsIgnoreCase(MASTER_CARD)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_mastercard);
            } else if(cardBrandName.equalsIgnoreCase(MASTER_CARD_WITH_SPACE)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_mastercard);
            } else if(cardBrandName.equalsIgnoreCase(VISA)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_visa);
            } else if(cardBrandName.equalsIgnoreCase(JCB)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_jcb);
            } else if(cardBrandName.equalsIgnoreCase(MAESTRO)) {
                ivCreditCardIcon.setImageResource(R.drawable.ic_maestro);
            } else if(cardBrandName.equalsIgnoreCase(UNIONPAY)) {
                ivCreditCardIcon.setImageResource(R.drawable.bt_ic_vaulted_unionpay);
            } else if(cardBrandName.equalsIgnoreCase(UNIONPAY_WITH_SPACE)) {
                ivCreditCardIcon.setImageResource(R.drawable.bt_ic_vaulted_unionpay);
            } else {
                ivCreditCardIcon.setImageResource(R.drawable.ic_default_card);
            }
        } else {
            ivCreditCardIcon.setImageResource(R.drawable.ic_default_card);
        }
    }

}
