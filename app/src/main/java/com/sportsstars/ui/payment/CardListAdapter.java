package com.sportsstars.ui.payment;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.RowItemCardListBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.response.payment.CardData;

import java.util.ArrayList;

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.MyViewHolder> {

    private ArrayList<CardData> mCardList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ListItemClickListener mListItemClickListener;

    public CardListAdapter(Activity mActivity, ArrayList<CardData> mCardList, ListItemClickListener mListItemClickListener) {
        this.mCardList = mCardList;
        this.mActivity = mActivity;
        this.mListItemClickListener = mListItemClickListener;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public CardListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowItemCardListBinding mBinding = DataBindingUtil.inflate(mInflator, R.layout.row_item_card_list, parent, false);
        return new CardListAdapter.MyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(CardListAdapter.MyViewHolder holder, int position) {
        if (mCardList != null && mCardList.size() > 0) {
            holder.bindData(mCardList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return mCardList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowItemCardListBinding rowItemCardListBinding;

        public MyViewHolder(RowItemCardListBinding rowItemCardListBinding) {
            super(rowItemCardListBinding.getRoot());
            this.rowItemCardListBinding = rowItemCardListBinding;
        }

        public RowItemCardListBinding getBinding() {
            return rowItemCardListBinding;
        }

        public void bindData(CardData cardData, int position) {
            try {
                if(cardData.getCardNumber() != null && !TextUtils.isEmpty(cardData.getCardNumber())) {
//                    CryptLib cryptLib = new CryptLib();
//                    String cardNumberDecrypted = cryptLib.decryptCipherTextWithRandomIV(cardData.getCardNumber(), Constants.CIPHER_KEY);
//                    String last4DigitsCardNumber = cardNumberDecrypted.substring(cardNumberDecrypted.length()-4, cardNumberDecrypted.length());
                    String cardNumberWithDots = mActivity.getResources().getString(R.string.card_number_with_dots_unicode, cardData.getCardNumber());
                    rowItemCardListBinding.tvCardNum.setText(Html.fromHtml(cardNumberWithDots));

                    //rowItemCardListBinding.tvCardNum.setText(cardNumberDecrypted);

                    CardMatcher.setCreditCardIconByBrandName(rowItemCardListBinding.ivCardType, cardData.getCardType());
                } else {
                    rowItemCardListBinding.tvCardNum.setText(cardData.getCardId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            rowItemCardListBinding.rlCardItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListItemClickListener.onListItemClicked(position);
                }
            });
        }

    }

}
