package com.sportsstars.ui.payment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityRemoveCardBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.payment.CardData;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import retrofit2.Call;

public class RemoveCardActivity extends BaseActivity implements View.OnClickListener {

    ActivityRemoveCardBinding mBinding;
    CardData mCardData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(RemoveCardActivity.this, R.layout.activity_remove_card);

        getIntentData();
        initViews();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.CARD_DATA)) {
                mCardData = bundle.getParcelable(Constants.BundleKey.CARD_DATA);
            }
        }
    }

    private void initViews() {
        mBinding.headerId.parent.setBackgroundColor(ContextCompat.getColor(this,R.color.black));
        if(mCardData != null && !TextUtils.isEmpty(mCardData.getCardType())) {
            //mBinding.headerId.tvTitle.setText(CardMatcher.getCardNameByCardType(mCardData.getCardType()));
            mBinding.headerId.tvTitle.setText(mCardData.getCardType());
        } else {
            mBinding.headerId.tvTitle.setText(getResources().getString(R.string.card_details));
        }

        setCardData();

        mBinding.btnRemoveCard.setOnClickListener(this);
    }

    private void setCardData() {
        if(mCardData != null) {
            try {
                if(mCardData.getCardNumber() != null && !TextUtils.isEmpty(mCardData.getCardNumber())) {
//                    CryptLib cryptLib = new CryptLib();
//                    String cardNumberDecrypted = cryptLib.decryptCipherTextWithRandomIV(mCardData.getCardNumber(), Constants.CIPHER_KEY);
//                    String last4DigitsCardNumber = cardNumberDecrypted.substring(cardNumberDecrypted.length()-4, cardNumberDecrypted.length());
                    String cardNumberWithDots = getResources().getString(R.string.card_number_with_dots_unicode, mCardData.getCardNumber());

                    mBinding.tvCardNumber.setText(Html.fromHtml(cardNumberWithDots));

                    //mBinding.tvCardNumber.setText(cardNumberDecrypted);
                } else {
                    mBinding.tvCardNumber.setText(mCardData.getCardId());
                }

                if(mCardData.getCardExpiryMonth() != null
                        && !TextUtils.isEmpty(mCardData.getCardExpiryMonth())
                        && mCardData.getCardExpiryYear() != null
                        && !TextUtils.isEmpty(mCardData.getCardExpiryYear())) {
//                    CryptLib cryptLib = new CryptLib();
//                    String cardExpiryMonthDecrypted = cryptLib.decryptCipherTextWithRandomIV(mCardData.getCardExpiryMonth(), Constants.CIPHER_KEY);
//                    String cardExpiryYearDecrypted = cryptLib.decryptCipherTextWithRandomIV(mCardData.getCardExpiryYear(), Constants.CIPHER_KEY);
//                    String yearInTwoDigit = cardExpiryYearDecrypted.substring(2, cardExpiryYearDecrypted.length());

                    String cardMonthStr = mCardData.getCardExpiryMonth();
                    int cardMonthInt = 00;
                    try {
                        cardMonthInt = Integer.parseInt(cardMonthStr);
                        if(cardMonthInt < 10) {
                            cardMonthStr = "0"+cardMonthStr;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mBinding.tvCardExpiry.setText(cardMonthStr + "/" + mCardData.getCardExpiryYear());
                } else {
                    mBinding.tvCardExpiry.setText("12/2020");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnRemoveCard) {
            if(Utils.isNetworkAvailable()) {
                removeCard();
            } else {
                showSnackbarFromTop(RemoveCardActivity.this, getResources().getString(R.string.no_internet));
            }
        }
    }

    private void removeCard() {
        if(mCardData != null && mCardData.getCardId() != null) {
            showRemoveCardConfirmationDialog();
        }
    }

    private void showRemoveCardConfirmationDialog() {
        Alert.showAlertDialogWithCancel(this,
                getResources().getString(R.string.remove_card_dialog_title),
                getResources().getString(R.string.remove_card_dialog_message),
                getResources().getString(R.string.cancel),
                getResources().getString(R.string.okay), false, "",
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        removeCardApi();
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });
    }

    private void removeCardApi() {
        processToShowDialog();
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.removeCard(""+mCardData.getCardId()).enqueue(new BaseCallback<BaseResponse>(this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if(response != null && response.getStatus() == 1) {
                    try {
                        if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            Alert.showAlertDialog(
                                    RemoveCardActivity.this,
                                    getResources().getString(R.string.card_removed_success_title),
                                    response.getMessage(),
                                    getString(R.string.okay), new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {
                                            finish();
                                        }
                                    });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public String getActivityName() {
        return RemoveCardActivity.class.getSimpleName();
    }

}
