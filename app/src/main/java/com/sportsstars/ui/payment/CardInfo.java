/*
 * Copyright © 2017 SeatXchange. All rights reserved.
 * Developed by Appster.
 *
 */

package com.sportsstars.ui.payment;

/**
 * Created by abhay on 26/12/16.
 * A wrapper class for Card info
 */

public class CardInfo {
    private int id;
    private String cardType;
    private int maxLength;
    private int cvvLength;

    public int getCvvLength() {
        return cvvLength;
    }

    public void setCvvLength(int cvvLength) {
        this.cvvLength = cvvLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
