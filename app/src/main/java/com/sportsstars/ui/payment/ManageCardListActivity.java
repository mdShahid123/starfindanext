package com.sportsstars.ui.payment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityManageCardListBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.model.UserModel;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.CalendarSyncPostRequest;
import com.sportsstars.network.request.auth.CreateBookingRequest;
import com.sportsstars.network.request.auth.PayForBookingRequest;
import com.sportsstars.network.request.auth.UpdateDeviceTokenRequest;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.response.CreateBookingBaseResponse;
import com.sportsstars.network.response.CreateBookingResponse;
import com.sportsstars.network.response.payment.CardData;
import com.sportsstars.network.response.payment.CardListResponse;
import com.sportsstars.network.response.payment.TransactionFeeResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.fixture.CreateFixtureListActivity;
import com.sportsstars.ui.learner.events.LearnerEventDetailsActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;

import java.util.ArrayList;

import retrofit2.Call;

public class ManageCardListActivity extends BaseActivity implements View.OnClickListener, ListItemClickListener,
        Alert.OnOkayClickListener, Alert.OnCancelClickListener {

    private static final String TAG = "BraintreeLog";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 111;

    private ActivityManageCardListBinding mBinding;

    private LinearLayoutManager mLayoutManager;
    private CardListAdapter mCardListAdapter;
    private ArrayList<CardData> cardList = new ArrayList<>();

    private UserProfileModel mSelectedCoachModel;
    private CreateBookingRequest mCreateBookingRequest;
    private String facilityAddress, sessionDateAndTimeUtc;
    private boolean isFromSettings;
    //private boolean isPaypalButtonClicked;

    private String token;
    private BraintreeFragment mBraintreeFragment;
    private ConfigurationListener configurationListener;

    private boolean isPaypalClickedDirectly;
    private MultiClickPreventer multiClickPreventer;

    private int mBookingId;
    private double mBookingTotalPrice;
    private boolean isFromSpeakerBooking;
    private ArrayList<Integer> mSelectedSpeakerIdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_manage_card_list);

        mCreateBookingRequest = null;
        getIntentData();
        initViews();
        if(Utils.isNetworkAvailable()) {
            try {
                updateDeviceToken();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getIntentData() {
        mSelectedCoachModel = UserProfileInstance.getInstance().getUserProfileModel();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SETTINGS)) {
                isFromSettings = bundle.getBoolean(Constants.BundleKey.IS_FROM_SETTINGS);
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_SESSION_DATE)) {
                sessionDateAndTimeUtc = bundle.getString(Constants.BundleKey.BOOKING_SESSION_DATE, "");
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_FACILITY_ADDRESS)) {
                facilityAddress = bundle.getString(Constants.BundleKey.BOOKING_FACILITY_ADDRESS, "");
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_REQUEST_DATA)) {
                mCreateBookingRequest = bundle.getParcelable(Constants.BundleKey.BOOKING_REQUEST_DATA);
            }

            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SPEAKER_BOOKING)) {
                isFromSpeakerBooking = bundle.getBoolean(Constants.BundleKey.IS_FROM_SPEAKER_BOOKING, false);
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_ID)) {
                mBookingId = bundle.getInt(Constants.BundleKey.BOOKING_ID);
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_AMOUNT)) {
                mBookingTotalPrice = bundle.getDouble(Constants.BundleKey.BOOKING_AMOUNT);
            }
            if(getIntent().hasExtra(Constants.BundleKey.SPEAKER_IDS_LIST)) {
                mSelectedSpeakerIdList = bundle.getIntegerArrayList(Constants.BundleKey.SPEAKER_IDS_LIST);
            }
        }
    }

    private void initViews() {

        mBinding.headerId.parent.setBackgroundColor(ContextCompat.getColor(this,R.color.black));
        mBinding.headerId.tvTitle.setText(getString(R.string.payment));

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mCardListAdapter = new CardListAdapter(this, cardList, this);
        mBinding.rvCardList.setAdapter(mCardListAdapter);
        mBinding.rvCardList.setLayoutManager(mLayoutManager);

        //mBinding.btnAddCard.setOnClickListener(this);
        mBinding.llNoCardText.setOnClickListener(this);
        mBinding.rlPaypal.setOnClickListener(this);

        if(!isFromSettings) {
            mBinding.rlPaypal.setVisibility(View.GONE);
            mBinding.dividerUsing.rootViewDividerUsing.setVisibility(View.GONE);
        } else {
            mBinding.rlPaypal.setVisibility(View.GONE);
            mBinding.dividerUsing.rootViewDividerUsing.setVisibility(View.GONE);
        }

        multiClickPreventer = new MultiClickPreventer();
    }

    private void updateDeviceToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        String fcmToken = PreferenceUtil.getFCMToken();
        //LogUtils.LOGD("FCM instance token ", token);
        //LogUtils.LOGD("FCM token", token);
        if (TextUtils.isEmpty(token) && TextUtils.isEmpty(fcmToken)) {
        } else if (!TextUtils.isEmpty(token)) {
            PreferenceUtil.setFCMToken(token);
        }

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,false);
        webServices.updateDeviceToken(new UpdateDeviceTokenRequest(PreferenceUtil.getFCMToken())).enqueue(new BaseCallback<BaseResponse>(ManageCardListActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //if(!isPaypalButtonClicked) {
            if(cardList != null && !cardList.isEmpty()) {
                cardList.clear();
            }
            toggleViewsVisibility(true, -1);
            if(Utils.isNetworkAvailable()) {
                getCardList();
            } else {
                showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.no_internet));
            }
        //}
    }

    private void toggleViewsVisibility(boolean hasList, int listCount) {
        if(hasList) {
            if(listCount >= 0 && listCount < 5) {
                mBinding.llNoCardText.setVisibility(View.VISIBLE);
            } else {
                mBinding.llNoCardText.setVisibility(View.GONE);
            }
            mBinding.rvCardList.setVisibility(View.VISIBLE);
        } else {
            mBinding.rvCardList.setVisibility(View.GONE);
            mBinding.llNoCardText.setVisibility(View.VISIBLE);
        }
    }

    private void getCardList() {
        processToShowDialog();
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCardList().enqueue(new BaseCallback<CardListResponse>(this) {
            @Override
            public void onSuccess(CardListResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null && !response.getResult().isEmpty()) {
                            toggleViewsVisibility(true , response.getResult().size());
                            cardList.addAll(response.getResult());
                            if(mCardListAdapter != null) {
                                mCardListAdapter.notifyDataSetChanged();
                            }
                        } else {
                            toggleViewsVisibility(false, 0);
                        }
                    } else {
                        toggleViewsVisibility(false, 0);
                        showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.unable_to_get_card_list));
                    }
                } else {
                    try {
                        toggleViewsVisibility(false, 0);
                        showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.unable_to_get_card_list));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CardListResponse> call, BaseResponse baseResponse) {
                try {
                    toggleViewsVisibility(false, 0);
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //Log.d("mylog", baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

//    private void getBraintreeToken() {
//        mBinding.rlPaypal.setEnabled(false);
//        processToShowDialog();
//        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
//        webServices.getBraintreeClientToken().enqueue(new BaseCallback<BraintreeTokenResponse>(ManageCardListActivity.this) {
//            @Override
//            public void onSuccess(BraintreeTokenResponse response) {
//                if(response != null && response.getStatus() == 1) {
//                    token = response.getResult().getClientToken();
//                    initiateBraintreeFragment(token);
//                } else {
//                    mBinding.rlPaypal.setEnabled(true);
//                    try {
//                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
//                            showSnackbarFromTop(ManageCardListActivity.this, response.getMessage());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFail(Call<BraintreeTokenResponse> call, BaseResponse baseResponse) {
//                mBinding.rlPaypal.setEnabled(true);
//                try {
//                    if(baseResponse != null && baseResponse.getMessage() != null
//                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
////                        Log.d(TAG, "Error while getting Braintree client token - "
////                                + baseResponse.getMessage());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }
//
//    private void initiateBraintreeFragment(String mAuthorization) {
//        try {
//            showProgressBar();
//            if(mBraintreeFragment != null) {
//                mBraintreeFragment = null;
//            }
//            mBraintreeFragment = BraintreeFragment.newInstance(ManageCardListActivity.this, mAuthorization);
//
//            if(configurationListener != null) {
//                Log.d(TAG, "if(configurationListener != null) - call initiatePayment");
////                try {
////                    hideProgressBar();
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
//                initiatePayment(mAuthorization);
//            }
//
//            configurationListener = new ConfigurationListener() {
//                @Override
//                public void onConfigurationFetched(Configuration configuration) {
//                    Log.d(TAG, "When new ConfigurationListener() - call initiatePayment");
//                    Log.d(TAG, "LINE 103 - configuration - getAssetsUrl : " + configuration.getAssetsUrl()
//                            + " getClientApiUrl : " + configuration.getClientApiUrl()
//                            + " getEnvironment : " + configuration.getEnvironment()
//                            + " getMerchantAccountId : " + configuration.getMerchantAccountId()
//                            + " getMerchantId : " + configuration.getMerchantId()
//                            + " toJson : " + configuration.toJson());
////                    try {
////                        hideProgressBar();
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
//                    initiatePayment(mAuthorization);
//                }
//            };
//
//            mBraintreeFragment.addListener(configurationListener);
//
//            mBraintreeFragment.addListener(new PaymentMethodNonceCreatedListener() {
//                @Override
//                public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
//                    hideProgressBar();
//                    Log.d(TAG, "onPaymentMethodNonceCreated");
//                    Log.d(TAG, "Description : " + paymentMethodNonce.getDescription());
//                    Log.d(TAG, "Nonce : " + paymentMethodNonce.getNonce());
//                    Log.d(TAG, "Nonce Type : " + paymentMethodNonce.getTypeLabel());
//
//                    confirmBooking(paymentMethodNonce.getNonce());
//
//                    if (paymentMethodNonce instanceof PayPalAccountNonce) {
//                        PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce)paymentMethodNonce;
//
//                        // Access additional information
//                        String email = payPalAccountNonce.getEmail();
//                        String firstName = payPalAccountNonce.getFirstName();
//                        String lastName = payPalAccountNonce.getLastName();
//                        String phone = payPalAccountNonce.getPhone();
//                        Log.d(TAG, "Paypal email : " + email);
//                        Log.d(TAG, "Paypal firstName : " + firstName);
//                        Log.d(TAG, "Paypal lastName : " + lastName);
//                        Log.d(TAG, "Paypal phone : " + phone);
//
//                        // See PostalAddress.java for details
//                        PostalAddress billingAddress = payPalAccountNonce.getBillingAddress();
//                        PostalAddress shippingAddress = payPalAccountNonce.getShippingAddress();
//                        Log.d(TAG, "Paypal billingAddress : " + billingAddress.getExtendedAddress());
//                        Log.d(TAG, "Paypal shippingAddress : " + shippingAddress.getExtendedAddress());
//                    }
//                }
//            });
//
////            mBraintreeFragment.addListener(new PaymentMethodNoncesUpdatedListener() {
////                @Override
////                public void onPaymentMethodNoncesUpdated(List<PaymentMethodNonce> paymentMethodNonces) {
////                    hideProgressBar();
////                    Log.d(TAG, "onPaymentMethodNoncesUpdated");
////                    for (PaymentMethodNonce nonce : paymentMethodNonces) {
////                        Log.d(TAG, "Description : " + nonce.getDescription());
////                        Log.d(TAG, "Nonce : " + nonce.getNonce());
////                        Log.d(TAG, "Nonce Type : " + nonce.getTypeLabel());
////                    }
////                }
////            });
//
//            mBraintreeFragment.addListener(new BraintreeErrorListener() {
//                @Override
//                public void onError(Exception error) {
//                    hideProgressBar();
//                    mBinding.rlPaypal.setEnabled(true);
//                    Log.d(TAG, "BraintreeErrorListener");
//                    Log.d(TAG, "onError - " + error.getMessage());
//
//                    showSnackbarFromTop(ManageCardListActivity.this, ""+error.getMessage());
//
//                    if (error instanceof ErrorWithResponse) {
//                        ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
//                        BraintreeError cardErrors = errorWithResponse.errorFor("creditCard");
//                        if (cardErrors != null) {
//                            // There is an issue with the credit card.
//                            BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
//                            if (expirationMonthError != null) {
//                                // There is an issue with the expiration month.
//                                Log.d(TAG, "Expiration Error : " + expirationMonthError.getMessage());
//                            }
//                        }
//                    }
//                }
//            });
//
//            // mBraintreeFragment is ready to use!
//            Log.d(TAG, "initiateBraintree - end of try block");
//        } catch (InvalidArgumentException e) {
//            // There was an issue with your authorization string.
//            Log.d(TAG, "initiateBraintree - InvalidArgumentException " + e.getMessage());
//            hideProgressBar();
//            mBinding.rlPaypal.setEnabled(true);
//            e.printStackTrace();
//        } catch (Exception e) {
//            Log.d(TAG, "initiateBraintree - Exception " + e.getMessage());
//            hideProgressBar();
//            mBinding.rlPaypal.setEnabled(true);
//            e.printStackTrace();
//        }
//    }
//
//    private void initiatePayment(String mAuthorization) {
//        if(isPaypalClickedDirectly) {
//            return;
//        }
//        payUsingPaypal();
//    }
//
//    private void payUsingPaypal() {
//        //PayPal.authorizeAccount(mBraintreeFragment);
//        try {
//            showProgressBar();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        setupBraintreeAndStartExpressCheckout();
//    }
//
//    public void setupBraintreeAndStartExpressCheckout() {
//        if(mCreateBookingRequest != null && mCreateBookingRequest.getPrice() != 0.0d) {
//            isPaypalButtonClicked = true;
//            PayPalRequest request = new PayPalRequest(String.valueOf(mCreateBookingRequest.getPrice()))
//                    .currencyCode(Constants.CURRENCY_USD)
//                    .intent(PayPalRequest.INTENT_AUTHORIZE);
//            PayPal.requestOneTimePayment(mBraintreeFragment, request);
//        } else {
//            try {
//                hideProgressBar();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            isPaypalButtonClicked = false;
//            mBinding.rlPaypal.setEnabled(true);
//            showSnackbarFromTop(ManageCardListActivity.this, "Not getting booking amount from confirm booking screen.");
//        }
//    }

    private void confirmBooking(String paymentMethodNonce) {

        //isPaypalButtonClicked = false;

        if(mCreateBookingRequest == null) {
            //mBinding.rlPaypal.setEnabled(true);
            return;
        }

        if(paymentMethodNonce == null) {
            //mBinding.rlPaypal.setEnabled(true);
            return;
        }

        if(TextUtils.isEmpty(paymentMethodNonce)) {
            //mBinding.rlPaypal.setEnabled(true);
            return;
        }

        //mBinding.rlPaypal.setEnabled(false);

        mCreateBookingRequest.setPaymentNounce(paymentMethodNonce);

        processToShowDialog();

        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.createLearnerBooking(mCreateBookingRequest).enqueue(new BaseCallback<CreateBookingBaseResponse>(this) {
            @Override
            public void onSuccess(CreateBookingBaseResponse response) {
                if(response != null && response.getStatus() == 1) {
                    //isPaypalButtonClicked = false;
                    if(response.getResult() != null) {
                        calendarSyncPost(response.getResult());
                    }

//                    String msg="You have successfully booked your session with " + mSelectedCoachModel.getName() + "."+"\n\n" +
//                            "Would you like to add some more fixtures so "+ mSelectedCoachModel.getName()+" can send you tips on matchday?";

//                    String msg = "You have successfully booked your session with " + mSelectedCoachModel.getName() + "."+"\n\n" +
//                            "Create your fixture to receive tips from your Coach, StarFinda Ambassadors or StarFinda Admin.";

                    String msg = "You have successfully booked your session with " + mSelectedCoachModel.getName() + "."+"\n\n";

                    try {
//                        Alert.showAlertDialogWithBottomButton(ManageCardListActivity.this,getString(R.string.success),msg,getString(R.string.do_this_later),getString(R.string.yes_please),
//                                ManageCardListActivity.this,ManageCardListActivity.this);

                        Alert.showInfoDialog(ManageCardListActivity.this,
                                getResources().getString(R.string.session_booked),
                                msg,
                                getResources().getString(R.string.okay),
                                new Alert.OnOkayClickListener() {
                                    @Override
                                    public void onOkay() {
                                        if(!PreferenceUtil.isFromSessionDetails()) {
                                            Navigator.getInstance().navigateToActivityWithClearTop(ManageCardListActivity.this,
                                                    LearnerHomeActivity.class, null);
                                        }
                                        finish();
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                if (ContextCompat.checkSelfPermission(ConfirmBookingActivity.this, Manifest.permission.WRITE_CALENDAR)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    askCalendarPermission();
//                } else {
//                    addBookingEventToCalendar();
//                }

                    //mBinding.rlPaypal.setEnabled(true);

                } else {
                    //isPaypalButtonClicked = false;
                    mBinding.rlPaypal.setEnabled(true);
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(ManageCardListActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CreateBookingBaseResponse> call, BaseResponse baseResponse) {
                //isPaypalButtonClicked = false;
                mBinding.rlPaypal.setEnabled(true);
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(ManageCardListActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void calendarSyncPost(CreateBookingResponse createBookingResponse) {
        if(createBookingResponse != null) {
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
            CalendarSyncPostRequest calendarSyncPostRequest = new CalendarSyncPostRequest();
            calendarSyncPostRequest.setSessionId(createBookingResponse.getSessionId());
            calendarSyncPostRequest.setCoachId(createBookingResponse.getCoachId());
            calendarSyncPostRequest.setLearnerId(createBookingResponse.getLearnerId());
            calendarSyncPostRequest.setSessionDate(createBookingResponse.getSessionDate());
            authWebServices.calendarSyncPost(calendarSyncPostRequest).enqueue(new BaseCallback<BaseResponse>(this) {
                @Override
                public void onSuccess(BaseResponse response) {
                    if(response != null && response.getStatus() == 1) {
                        if (ContextCompat.checkSelfPermission(ManageCardListActivity.this, Manifest.permission.WRITE_CALENDAR)
                                != PackageManager.PERMISSION_GRANTED) {
                            askCalendarPermission();
                        } else {
                            addBookingEventToCalendar();
                        }
                    }
                }

                @Override
                public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                    try {
                        hideProgressBar();
                        if(baseResponse != null && baseResponse.getMessage() != null
                                && !TextUtils.isEmpty(baseResponse.getMessage())) {
                            //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addBookingEventToCalendar();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_not_granted_for_calendar), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(ManageCardListActivity.this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void addBookingEventToCalendar() {
        String sportName = "", coachName = "", sessionDateTime = "";
        long startTime = 0;

        if(mSelectedCoachModel == null) {
            return;
        }
        if(TextUtils.isEmpty(sessionDateAndTimeUtc)) {
            return;
        }
        if(TextUtils.isEmpty(facilityAddress)) {
            return;
        }

        if(mSelectedCoachModel != null) {
            sportName = mSelectedCoachModel.getSportDetail().getName();
            coachName = mSelectedCoachModel.getName();
        }
        if(sessionDateAndTimeUtc != null) {
            startTime = Utils.getTimeInMillis(sessionDateAndTimeUtc);
            sessionDateTime = sessionDateAndTimeUtc;
        }
        if(!TextUtils.isEmpty(sportName) && !TextUtils.isEmpty(coachName)
                && !TextUtils.isEmpty(facilityAddress) && startTime != 0) {
//            Utils.addEventToCalendar(ConfirmBookingActivity.this,
//                    sportName + " with " + coachName, facilityAddress, startTime, true);

            CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
            calendarSyncResponse.setCoachName(coachName);
            calendarSyncResponse.setFacility(facilityAddress);
            calendarSyncResponse.setLearnerName("");
            calendarSyncResponse.setSessionDate(sessionDateTime);
            calendarSyncResponse.setSportName(sportName);
            calendarSyncResponse.setTitle("StarFinda Coaching Session with " + coachName + " for " + sportName);
            addBookingEventToCalendar(calendarSyncResponse);
        }
    }

    private void addBookingEventToCalendar(CalendarSyncResponse calendarSyncResponse) {
        CalendarSyncHelper.getInstance().addCalendarEvent(ManageCardListActivity.this, calendarSyncResponse);
    }

    @Override
    public void onClick(View v) {
        //if(v.getId() == R.id.btnAddCard || v.getId() == R.id.llNoCardText) {
        if(v.getId() == R.id.llNoCardText) {
            multiClickPreventer.preventMultiClick(v);
            if(Utils.isNetworkAvailable()) {
                Navigator.getInstance().navigateToActivity(ManageCardListActivity.this, AddCardActivity.class);
            } else {
                showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.no_internet));
            }
        }

//        else if(v.getId() == R.id.rlPaypal) {
//            multiClickPreventer.preventMultiClick(v);
//            if(Utils.isNetworkAvailable()) {
//                if(mBraintreeFragment == null) {
//                    isPaypalClickedDirectly = false;
//                    getBraintreeToken();
//                    return;
//                }
//                isPaypalClickedDirectly = true;
//                payUsingPaypal();
//            } else {
//                showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.no_internet));
//            }
//        }
    }

    @Override
    public void onListItemClicked(int position) {
        //isPaypalButtonClicked = false;
        if(Utils.isNetworkAvailable()) {
            CardData cardData = cardList.get(position);
            Bundle bundle = new Bundle();
            if(cardData != null) {
                if(isFromSettings) {
                    bundle.putParcelable(Constants.BundleKey.CARD_DATA, cardData);
                    Navigator.getInstance().navigateToActivityWithData(ManageCardListActivity.this, RemoveCardActivity.class, bundle);
                } else {

                    //------- COMMENTED, NOT USING THIS CODE NOW DUE TO FLOW CHANGE IN THE APP ------//
//                    bundle.putString(Constants.BundleKey.BOOKING_SESSION_DATE, sessionDateAndTimeUtc);
//                    bundle.putString(Constants.BundleKey.BOOKING_FACILITY_ADDRESS, facilityAddress);
//                    bundle.putParcelable(Constants.BundleKey.BOOKING_REQUEST_DATA, mCreateBookingRequest);
//                    bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, isFromSettings);
//                    bundle.putParcelable(Constants.BundleKey.CARD_DATA, cardData);
//                    Navigator.getInstance().navigateToActivityWithData(ManageCardListActivity.this, AddPaymentMethodActivity.class, bundle);
//                    finish();


                    //-------   COMMENTED BELOW CODE TO SHOW PAYMENT INFO DIALOG ON CLIENT FEEDBACK ----//
//                    if(!TextUtils.isEmpty(cardData.getCardId())) {
//                        confirmBooking(cardData.getCardId());
//                    } else {
//                        showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.card_id_not_available));
//                    }


                    //-------- NEW CODE TO DISPLAY PAYMENT INFO DIALOG BEFORE ACTUAL PAYMENT ON CLIENT FEEDBACK -----//
                    if(isFromSpeakerBooking) {
                        checkTaxationAmount(mBookingTotalPrice, cardData);
                    } else {
                        if(mCreateBookingRequest != null && mCreateBookingRequest.getPrice() != 0) {
                            checkTaxationAmount(mCreateBookingRequest.getPrice(), cardData);
                        }
                    }


                }
            }
        } else {
            showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void checkTaxationAmount(double price, CardData cardData) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.checkTaxation(price).enqueue(new BaseCallback<TransactionFeeResponse>(ManageCardListActivity.this) {
            @Override
            public void onSuccess(TransactionFeeResponse response) {
                if(response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        showBookingInfoDialog(price, response.getResult().getStripeFee(), cardData);
                    }
                }
            }

            @Override
            public void onFail(Call<TransactionFeeResponse> call, BaseResponse baseResponse) {
                hideProgressBar();

            }
        });
    }

    private void showBookingInfoDialog(double actualBookingAmount, double bookingTaxableAmount, CardData cardData) {

        String bookingInfoMsg = getResources().getString(R.string.booking_info_msg,
                String.valueOf(actualBookingAmount), String.valueOf(bookingTaxableAmount));

        Alert.showAlertDialogWithCancel(this,
                getResources().getString(R.string.booking_info_title),
                bookingInfoMsg,
                getResources().getString(R.string.no_return),
                getResources().getString(R.string.yes_proceed),
                true,
                getResources().getString(R.string.stripe_usage_msg),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(Utils.isNetworkAvailable()) {
                            if(!TextUtils.isEmpty(cardData.getCardId())) {
                                if(isFromSpeakerBooking) {
                                    confirmSpeakerBooking(cardData.getCardId());
                                } else {
                                    confirmCoachBooking(cardData.getCardId());
                                }
                            } else {
                                showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.card_id_not_available));
                            }
                        } else {
                            showSnackbarFromTop(ManageCardListActivity.this, getResources().getString(R.string.no_internet));
                        }
                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });
    }

    private void confirmSpeakerBooking(String paymentMethodNonce) {

        UserModel userModel = PreferenceUtil.getUserModel();
        if(userModel == null) {
            return;
        }
        if(userModel.getUserId() == 0) {
            return;
        }

        //double testPrice = 1.0;
        PayForBookingRequest payForBookingRequest = new PayForBookingRequest();
        //payForBookingRequest.setCustomerId(userModel.getStripeAccId());
        payForBookingRequest.setSessionId(mBookingId);
        payForBookingRequest.setPaymentNounce(paymentMethodNonce);
        payForBookingRequest.setAmount(mBookingTotalPrice);
        //payForBookingRequest.setAmount(testPrice);
        payForBookingRequest.setSpeakerId(mSelectedSpeakerIdList);

        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.payForBooking(payForBookingRequest).enqueue(new BaseCallback<BaseResponse>(ManageCardListActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                        finish();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                    //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                }
                finish();
            }
        });
    }

    private void confirmCoachBooking(String paymentMethodNonce) {
        confirmBooking(paymentMethodNonce);
    }

    @Override
    public void onOkay() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
        if(!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearTop(ManageCardListActivity.this, CreateFixtureListActivity.class, bundle);
        } else {
            Navigator.getInstance().navigateToActivityWithData(ManageCardListActivity.this, CreateFixtureListActivity.class, bundle);
        }
        finish();
    }

    @Override
    public void onCancel() {
        if(!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearTop(ManageCardListActivity.this,
                    LearnerHomeActivity.class, null);
        }
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            hideProgressBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mBinding.rlPaypal.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(mBraintreeFragment != null) {
                mBraintreeFragment.removeListener(configurationListener);
            }
            if(configurationListener != null) {
                configurationListener = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getActivityName() {
        return ManageCardListActivity.class.getSimpleName();
    }

    public class MultiClickPreventer {

        private static final long DELAY_IN_MS = 1500;

        public void preventMultiClick(final View view) {
            if (!view.isClickable()) {
                return;
            }
            view.setClickable(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setClickable(true);
                }
            }, DELAY_IN_MS);
        }
    }

}
