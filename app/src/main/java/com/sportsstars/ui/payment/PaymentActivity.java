package com.sportsstars.ui.payment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityPaymentBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.GetCardResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;

import retrofit2.Call;

/**
 * Created by abhaykant on 17/01/18.
 */

public class PaymentActivity extends BaseActivity implements View.OnClickListener{
    private ActivityPaymentBinding mBinder;
    private boolean isCardAdded;

    @Override
    public String getActivityName() {
        return PaymentActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder= DataBindingUtil.setContentView(this, R.layout.activity_payment);
        mBinder.headerId.parent.setBackgroundColor(ContextCompat.getColor(this,R.color.black));
        mBinder.headerId.tvTitle.setText(getString(R.string.payment));
        init();
    }

    private void init(){
        mBinder.headerId.ivBack.setOnClickListener(this);
        mBinder.tvAddACard.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //getCardApi();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.iv_back:
                onBackPressed();
                break;

                case R.id.tv_add_a_card:
                    //Navigator.getInstance().navigateToActivity(this,AddCardActivity.class);
                    break;

        }
    }


    private void getCardApi() {
        processToShowDialog();


        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.getCardDetails().enqueue(new BaseCallback<GetCardResponse>(PaymentActivity.this) {
            @Override
            public void onSuccess(GetCardResponse response) {
                if (response != null && response.getStatus() == 1) {
                    isCardAdded=true;
                    //mBinder.tvEmailInfo.setVisibility(View.VISIBLE);
                    //showSnackbarFromTop(PaymentActivity.this,response.getMessage());
                    mBinder.tvAddACard.setText(getString(R.string.dots)+" "+response.getResult().getLast4Digit());

                    if (response.getResult().getBrand().equalsIgnoreCase(Constants.VISA_CARD)) {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_visa, 0,0 , 0);
                    } else if (response.getResult().getBrand().equalsIgnoreCase(Constants.MASTER_CARD_TYPE)) {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_mastercard, 0, 0, 0);
                    } else if (response.getResult().getBrand().equalsIgnoreCase(Constants.AMERICAN_EXPRESS_CARD)) {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_amex, 0,0, 0);
                    } else if (response.getResult().getBrand().equalsIgnoreCase(Constants.JCB)) {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_jcb, 0,0 , 0);
                    } else if (response.getResult().getBrand().equalsIgnoreCase(Constants.DINNER_CLUB)) {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_diners, 0, 0, 0);
                    } else if (response.getResult().getBrand().equalsIgnoreCase(Constants.DISCOVER_CARD)) {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_discover,0, 0 , 0);
                    } else {
                        mBinder.tvAddACard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_card_place_holder, 0,0 , 0);
                    }
                }
            }

            @Override
            public void onFail(Call<GetCardResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse!=null && baseResponse.getMessage()!=null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(PaymentActivity.this,baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
