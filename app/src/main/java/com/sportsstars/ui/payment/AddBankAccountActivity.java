package com.sportsstars.ui.payment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddBankAccountBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.AddCoachBankAccountRequest;
import com.sportsstars.network.response.AddCoachBankAccountResponse;
import com.sportsstars.network.response.CoachBankAccount;
import com.sportsstars.network.response.CoachBankAccountResponse;
import com.sportsstars.network.response.auth.CoachProfileStatusResponse;
import com.sportsstars.network.response.auth.ProfileStatusResult;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.coach.ManagePaymentActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.speaker.SpeakerHomeActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.ImagePickerUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AddBankAccountActivity extends BaseActivity implements View.OnClickListener,
            Alert.OnOkayClickListener, ImagePickerUtils.OnImagePickerListener {

    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 1234;

    private ActivityAddBankAccountBinding mBinding;
    private boolean isEditModeEnabled;
    //private long accountNumber, bsbCode;
    //private String accountHolderName;

    private boolean isEmptyBankAccount;
    private CoachBankAccount mCoachBankAccount;
    private String mDOB, mDobFormatted;
    private String imageFilePath;
    private boolean isFileSelected;
    private boolean shouldCallApiWithoutDocument;
    private boolean isDocUploaded;
    private boolean isVerify;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_bank_account);

        shouldCallApiWithoutDocument = false;
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_EDIT_MODE_ENABLED)) {
                isEditModeEnabled = bundle.getBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED);
            }

            if(isEditModeEnabled) {
                if(getIntent().hasExtra(Constants.BundleKey.IS_EMPTY_BANK_ACCOUNT)) {
                    isEmptyBankAccount = bundle.getBoolean(Constants.BundleKey.IS_EMPTY_BANK_ACCOUNT);
                }
                if(getIntent().hasExtra(Constants.BundleKey.COACH_BANK_ACCOUNT_DATA)) {
                    mCoachBankAccount = bundle.getParcelable(Constants.BundleKey.COACH_BANK_ACCOUNT_DATA);
                }
            }
        }
        if(!isEditModeEnabled) {
            setUpHeaderForAddAccount();
        } else {
            setUpHeaderForEditAccount();
        }

        if(isEditModeEnabled) {
            prefillInputFields();
        } else {
            if(Utils.isNetworkAvailable()) {
                getBankAccount();
            } else {
                showSnackbarFromTop(AddBankAccountActivity.this, getResources().getString(R.string.no_internet));
            }
        }
    }

    private void getBankAccount() {
        processToShowDialog();
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCoachBankAccount().enqueue(new BaseCallback<CoachBankAccountResponse>(this) {
            @Override
            public void onSuccess(CoachBankAccountResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null) {
                            mCoachBankAccount = response.getResult();
                            prefillInputFields();
                        }
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                //showSnackbarFromTop(AddBankAccountActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<CoachBankAccountResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //showSnackbarFromTop(AddBankAccountActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setUpHeaderForAddAccount() {
        Utils.setTransparentTheme(this);
        setUpHeader(mBinding.header, getString(R.string.txt_hdr_ttl), getString(R.string.txt_hdr_sub));
//        ImageView ivBack = (ImageView) mBinding.header.findViewById(R.id.iv_hdr_back);
//        ivBack.setVisibility(View.VISIBLE);
//        ivBack.setPadding(8,15,8,3);
        mBinding.btnNext.setText(getResources().getString(R.string.confirm));
        initView();
    }

    private void setUpHeaderForEditAccount() {
        Utils.setTransparentTheme(this);
        setUpHeader(mBinding.header, getString(R.string.edit_bank_account), "");
//        ImageView ivBack = (ImageView) mBinding.header.findViewById(R.id.iv_hdr_back);
//        ivBack.setVisibility(View.VISIBLE);
//        ivBack.setPadding(8,15,8,3);
        mBinding.btnNext.setText(getResources().getString(R.string.confirm));
        initView();
    }

    private void prefillInputFields() {
        if(mCoachBankAccount != null) {
            if(!TextUtils.isEmpty(mCoachBankAccount.getFirstName())) {
                mBinding.etFirstName.setText(mCoachBankAccount.getFirstName());
            } else {
                mBinding.etFirstName.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getLastName())) {
                mBinding.etLastName.setText(mCoachBankAccount.getLastName());
            } else {
                mBinding.etLastName.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getAccountNumber())) {
                mBinding.etBnkAcn.setText(mCoachBankAccount.getAccountNumber());
            } else {
                mBinding.etBnkAcn.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getBsb())) {
                mBinding.etBnkBsb.setText(mCoachBankAccount.getBsb());
            } else {
                mBinding.etBnkBsb.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getAddress())) {
                mBinding.etAddress.setText(mCoachBankAccount.getAddress());
            } else {
                mBinding.etAddress.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getCity())) {
                mBinding.etCity.setText(mCoachBankAccount.getCity());
            } else {
                mBinding.etCity.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getState())) {
                mBinding.etState.setText(mCoachBankAccount.getState());
            } else {
                mBinding.etState.setText("");
            }
            if(!TextUtils.isEmpty(String.valueOf(mCoachBankAccount.getPostalCode()))) {
                mBinding.etPostalCode.setText(String.valueOf(mCoachBankAccount.getPostalCode()));
            } else {
                mBinding.etPostalCode.setText("");
            }
            if(!TextUtils.isEmpty(mCoachBankAccount.getDob())) {
                mDOB = "";
                mDobFormatted = mCoachBankAccount.getDob();
                mDOB = convertDateFormat(mDobFormatted);
                mBinding.etDob.setText(mDOB);
            } else {
                mBinding.etDob.setText("");
            }

            if(mCoachBankAccount != null && mCoachBankAccount.getIsDocUploaded() == 1) {
                isDocUploaded = true;
            } else {
                isDocUploaded = false;
            }
            if(mCoachBankAccount != null && mCoachBankAccount.getIsVerify() == 1) {
                isVerify = true;
            } else {
                isVerify = false;
            }
            showDocumentUploadView(isEditModeEnabled, isDocUploaded, isVerify);
        }
    }

    private String convertDateFormat(String mDobFormatted) {
        String newDateStr = "";

        String[] formattedDateArr = mDobFormatted.split("-");
        if(formattedDateArr.length > 2) {
            String yearStr = formattedDateArr[0];
            String monthStr = formattedDateArr[1];
            String dateStr = formattedDateArr[2];
            newDateStr = dateStr + "/" + monthStr + "/" + yearStr;
        }

        return newDateStr;
    }

    private void updateDocumentUploadView() {
        if(imageFilePath != null && !TextUtils.isEmpty(imageFilePath)) {
            if(imageFilePath.contains("http")) {
                mBinding.tvUploadDocument.setText(getResources().getString(R.string.update_document));
                mBinding.ivDocument.setVisibility(View.VISIBLE);
            } else {
                File file = new File(imageFilePath);
                if(file.exists()) {
                    mBinding.tvUploadDocument.setText(getResources().getString(R.string.update_document));
                    mBinding.ivDocument.setVisibility(View.VISIBLE);
                } else {
                    mBinding.tvUploadDocument.setText(getResources().getString(R.string.add_document));
                    mBinding.ivDocument.setVisibility(View.INVISIBLE);
                }
            }
        } else {
            mBinding.tvUploadDocument.setText(getResources().getString(R.string.add_document));
            mBinding.ivDocument.setVisibility(View.INVISIBLE);
        }
    }

    private void showDocumentUploadView(boolean isEditModeEnabled, boolean isDocUploaded, boolean isVerify) {
        imageFilePath = "";
        if(!isEditModeEnabled) {
            if(isDocUploaded) {
                mBinding.tvUploadDocument.setText(getResources().getString(R.string.update_document));
                mBinding.tvUploadDocument.setVisibility(View.VISIBLE);
                mBinding.ivDocument.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvUploadDocument.setText(getResources().getString(R.string.add_document));
                mBinding.tvUploadDocument.setVisibility(View.VISIBLE);
                mBinding.ivDocument.setVisibility(View.INVISIBLE);
            }
        } else {
            if(isDocUploaded) {
                mBinding.tvUploadDocument.setText(getResources().getString(R.string.update_document));
                mBinding.tvUploadDocument.setVisibility(View.VISIBLE);
                mBinding.ivDocument.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvUploadDocument.setText(getResources().getString(R.string.add_document));
                mBinding.tvUploadDocument.setVisibility(View.VISIBLE);
                mBinding.ivDocument.setVisibility(View.INVISIBLE);
            }

//            if(isVerify && isDocUploaded) {
//                mBinding.tvUploadDocument.setVisibility(View.INVISIBLE);
//                mBinding.ivDocument.setVisibility(View.INVISIBLE);
//            } else {
//                mBinding.tvUploadDocument.setVisibility(View.VISIBLE);
//                mBinding.ivDocument.setVisibility(View.VISIBLE);
//            }

//            mBinding.tvUploadDocument.setVisibility(View.INVISIBLE);
//            mBinding.ivDocument.setVisibility(View.INVISIBLE);
        }
    }

    private void initView() {
        if(!isEditModeEnabled) {
             mBinding.btnNext.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
             mBinding.btnNext.setTextColor(ContextCompat.getColor(this, R.color.btn_txt_clr));

             //updateDocumentUploadView();
            isDocUploaded = false;
            isVerify = false;
            showDocumentUploadView(isEditModeEnabled, isDocUploaded, isVerify);
        } else {
            mBinding.btnNext.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));
            mBinding.btnNext.setTextColor(ContextCompat.getColor(this, R.color.black));

            updateDocumentUploadView();
            isDocUploaded = false;
            isVerify = false;
            if(mCoachBankAccount != null && mCoachBankAccount.getIsDocUploaded() == 1) {
                isDocUploaded = true;
            } else {
                isDocUploaded = false;
            }
            if(mCoachBankAccount != null && mCoachBankAccount.getIsVerify() == 1) {
                isVerify = true;
            } else {
                isVerify = false;
            }
            showDocumentUploadView(isEditModeEnabled, isDocUploaded, isVerify);
        }

        mBinding.etDob.setOnClickListener(this);
        mBinding.btnNext.setOnClickListener(this);
        mBinding.tvUploadDocument.setOnClickListener(this);
    }

    private boolean isInputValid() {
        if(TextUtils.isEmpty(mBinding.etFirstName.getText().toString().trim())) {
            mBinding.etFirstName.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.enter_account_holder_name));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etBnkAcn.getText().toString().trim())) {
            mBinding.etBnkAcn.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.enter_account_no));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etBnkBsb.getText().toString().trim())) {
            mBinding.etBnkBsb.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.enter_bsb_code));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etAddress.getText().toString().trim())) {
            mBinding.etAddress.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.msg_enter_address));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etCity.getText().toString().trim())) {
            mBinding.etCity.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.msg_enter_city));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etState.getText().toString().trim())) {
            mBinding.etState.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.msg_enter_state));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etPostalCode.getText().toString().trim())) {
            mBinding.etPostalCode.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.msg_enter_postal_code));
            return false;
        } else if(mBinding.etPostalCode.getText().length() < 4) {
            mBinding.etPostalCode.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.msg_enter_postal_code_min_length));
            return false;
        } else if(TextUtils.isEmpty(mBinding.etDob.getText().toString().trim())) {
            mBinding.etDob.requestFocus();
            showSnackbarFromTop(this, getResources().getString(R.string.msg_enter_dob));
            return false;
        } else {
            return true;
        }
    }

    private void addBankAccount() {

        processToShowDialog();

        AddCoachBankAccountRequest addCoachBankAccountRequest = new AddCoachBankAccountRequest();
        addCoachBankAccountRequest.setAccountNumber(""+mBinding.etBnkAcn.getText().toString());
        addCoachBankAccountRequest.setBsb(""+mBinding.etBnkBsb.getText().toString());

        // New Parameters
        addCoachBankAccountRequest.setFirstName(""+mBinding.etFirstName.getText().toString());
        addCoachBankAccountRequest.setLastName(""+mBinding.etLastName.getText().toString());
        addCoachBankAccountRequest.setAddress(""+mBinding.etAddress.getText().toString());
        addCoachBankAccountRequest.setCity(""+mBinding.etCity.getText().toString());
        addCoachBankAccountRequest.setState(""+mBinding.etState.getText().toString());
        addCoachBankAccountRequest.setPostalCode(""+mBinding.etPostalCode.getText().toString());
        addCoachBankAccountRequest.setDob(""+mDobFormatted);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.addCoachBankAccount(addCoachBankAccountRequest)
                .enqueue(new BaseCallback<AddCoachBankAccountResponse>(this) {

                    @Override
                    public void onSuccess(AddCoachBankAccountResponse response) {
                        if(response != null) {
                            if (response.getStatus() == 1) {
                                if(!isEditModeEnabled) {
                                    if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                                        getCoachProfileStatus();
                                    } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                                        getGuestSpeakerProfileStatus();
                                    }
                                } else {
                                    Alert.showAlertDialog(AddBankAccountActivity.this,
                                            getString(R.string.txt_dlg_ttl),
                                            getString(R.string.update_bank_account_msg),
                                            getString(R.string.txt_dlg_btn_ok),
                                            AddBankAccountActivity.this);
                                }
                            } else {
                                try {
                                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                        showSnackbarFromTop(AddBankAccountActivity.this, response.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(Call<AddCoachBankAccountResponse> call, BaseResponse baseResponse) {
                        try {
                            hideProgressBar();
                            if (baseResponse != null && baseResponse.getMessage() != null
                                    && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                showSnackbarFromTop(AddBankAccountActivity.this, baseResponse.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void addBankAccountWithDocument(boolean isEditModeEnabled) {

        RequestBody accountNumberBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etBnkAcn.getText().toString().trim());
        RequestBody firstNameBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etFirstName.getText().toString().trim());
        RequestBody lastNameBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etLastName.getText().toString().trim());
        RequestBody bsbBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etBnkBsb.getText().toString().trim());
        RequestBody addressBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etAddress.getText().toString().trim());
        RequestBody cityBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etCity.getText().toString().trim());
        RequestBody stateBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etState.getText().toString().trim());
        RequestBody postalCodeBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mBinding.etPostalCode.getText().toString().trim());
        RequestBody dobBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT),
                ""+mDobFormatted);

        File imageFile = null;
        if (!imageFilePath.contains("http")) {
            imageFile = new File(imageFilePath);
            if (imageFile == null || !imageFile.exists()) {
                showSnackbarFromTop(this, getString(R.string.error_msg_no_document_file));
                return;
            }
        }

        MultipartBody.Part imagePart = null;
        if (imageFile != null) {
            RequestBody imageFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_IMAGE), imageFile);
            imagePart = MultipartBody.Part.createFormData("file", imageFile.getName(), imageFileBody);
        }

        showProgressBar();

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        Call<AddCoachBankAccountResponse> call = client.addCoachBankAccount(
                accountNumberBody,
                firstNameBody,
                lastNameBody,
                bsbBody,
                addressBody,
                cityBody,
                stateBody,
                postalCodeBody,
                dobBody,
                imagePart);
        call.enqueue(new BaseCallback<AddCoachBankAccountResponse>(AddBankAccountActivity.this) {
            @Override
            public void onSuccess(AddCoachBankAccountResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        if(!isEditModeEnabled) {
                            if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                                getCoachProfileStatus();
                            } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                                getGuestSpeakerProfileStatus();
                            }
                        } else {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.update_bank_account_msg),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        }
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(AddBankAccountActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<AddCoachBankAccountResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddBankAccountActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getCoachProfileStatus() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(AddBankAccountActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    ProfileStatusResult result = response.getResult();
                    if(result != null) {
                        if (result.getIsProfileCompleted() == 1 && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
                            Navigator.getInstance().navigateToActivityWithClearStack(AddBankAccountActivity.this, HomeActivity.class);
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result.getIsProfileCompleted() == 1 && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.txt_dlg_bkm),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        } else if (result.getIsProfileCompleted() == 0 && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.txt_dlg_bkm),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        } else if (result.getIsProfileCompleted() == 1
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_REJECTED) {
                            Alert.showInfoDialog(AddBankAccountActivity.this,
                                    getResources().getString(R.string.coach_rejected_by_admin_alert_title),
                                    getResources().getString(R.string.coach_rejected_by_admin),
                                    getResources().getString(R.string.okay),
                                    new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {
                                            finish();
                                        }
                                    });
                        }  else if (result.getIsProfileCompleted() == 1
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_RESUBMITTED) {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.txt_dlg_bkm),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddBankAccountActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddBankAccountActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getGuestSpeakerProfileStatus() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(AddBankAccountActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    ProfileStatusResult result = response.getResult();
                    if(result != null) {
                        if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
                            Navigator.getInstance().navigateToActivityWithClearStack(AddBankAccountActivity.this, SpeakerHomeActivity.class);
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.txt_dlg_bkm),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        } else if (result.getIsGuestProfileCompleted() == 0
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.txt_dlg_bkm),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        } else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_REJECTED) {
                            Alert.showInfoDialog(AddBankAccountActivity.this,
                                    getResources().getString(R.string.coach_rejected_by_admin_alert_title),
                                    getResources().getString(R.string.speaker_rejected_by_admin),
                                    getResources().getString(R.string.okay),
                                    new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {
                                            finish();
                                        }
                                    });
                        }  else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_RESUBMITTED) {
                            Alert.showAlertDialog(AddBankAccountActivity.this,
                                    getString(R.string.txt_dlg_ttl),
                                    getString(R.string.txt_dlg_bkm),
                                    getString(R.string.txt_dlg_btn_ok),
                                    AddBankAccountActivity.this);
                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddBankAccountActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddBankAccountActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onOkay() {
        if(!isEditModeEnabled) {
            //showToast(getResources().getString(R.string.coach_not_verified));
            Navigator.getInstance().navigateToActivityWithClearStack(AddBankAccountActivity.this, WelcomeActivity.class);
        } else {
            setResult(RESULT_OK);
        }
        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.et_dob) {
            showDatePicker();
        } else if(view.getId() == R.id.btnNext) {
            if(Utils.isNetworkAvailable()) {
                if(isInputValid()) {
                    if(shouldCallApiWithoutDocument) {
                        addBankAccount();
                    } else {
                        if(!isEditModeEnabled) {
                            if(imageFilePath != null && !TextUtils.isEmpty(imageFilePath) && isFileSelected) {
                                addBankAccountWithDocument(isEditModeEnabled);
                            } else {
                                addBankAccount();
                                //showSnackbarFromTop(AddBankAccountActivity.this, getResources().getString(R.string.error_msg_no_document_file));
                            }
                        } else {
                            if(imageFilePath != null && !TextUtils.isEmpty(imageFilePath) && isFileSelected) {
                                addBankAccountWithDocument(isEditModeEnabled);
                            } else {
                                addBankAccount();
                            }
                        }
                    }
                }
            } else {
                showSnackbarFromTop(AddBankAccountActivity.this, getResources().getString(R.string.no_internet));
            }
        } else if(view.getId() == R.id.tv_upload_document) {
            if (ContextCompat.checkSelfPermission(AddBankAccountActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                askStoragePermission();
            } else {
                openNativeImagePicker();
            }
        }
    }

    private void showDatePicker() {
        Calendar mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(AddBankAccountActivity.this, new
                DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

//                            Calendar calendar = Calendar.getInstance();
//                            calendar.set(Calendar.YEAR, year);
//                            calendar.set(Calendar.MONTH, month);
//                            calendar.set(Calendar.DAY_OF_MONTH, day);

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormatted = yyyymmddFormat.format(mCalendar.getTime());

                        try {

                            if (date != null && isValidDate(date)) {
                                mDOB = date;
                                mDobFormatted = dateFormatted;
                                mBinding.etDob.setText(mDOB);
                            } else {
                                mDOB = "";
                                mDobFormatted = "";
                                mBinding.etDob.setText(mDOB);
                                showSnackbarFromTop(AddBankAccountActivity.this, getString(R.string.invalid_dob));
                                return;
                            }

                            int age = Utils.getAge(mCalendar);
                            if (age < 5) {
                                mDOB = "";
                                mDobFormatted = "";
                                mBinding.etDob.setText(mDOB);
                                showSnackbarFromTop(AddBankAccountActivity.this, getString(R.string.invalid_dob_age));
                            }
                            LogUtils.LOGD("mylog", "Age : " + Utils.getAge(mCalendar));

                        } catch (Exception e) {
                            LogUtils.LOGE("mylog", "Age : " + e.getMessage());
                        }
                    }
                }, year, month, day);

        Calendar minCal = Calendar.getInstance();
        minCal.set(Calendar.YEAR, 1940);
        datePickerDialog.getDatePicker().setMinDate(minCal.getTimeInMillis());

        datePickerDialog.show();
    }

    private boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat(Utils.DATE_DD_MM_YYYY, Locale.getDefault()).parse(pDateString);
        return new Date().after(date);
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }


    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openNativeImagePicker();
                } else {
                    Toast.makeText(this, "Permission not granted to access device storage.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showStoragePermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
    }

    private void showStoragePermissionExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.storage_permission_title));
        builder.setMessage(getResources().getString(R.string.storage_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(AddBankAccountActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_WRITE_EXTERNAL_STORAGE);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void openNativeImagePicker() {
        ImagePickerUtils.add((this).getSupportFragmentManager(), this, 1);

//        Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
//        imageIntent.setType("image/*");
//        imageIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
//        startActivityForResult(imageIntent, Constants.GET_IMAGE);
    }

    @Override
    public void success(String name, String path, int type) {
        if(path != null && !TextUtils.isEmpty(path)) {
            Log.d("file_path", "File path : " + path);
            File file = new File(path);
            if(file.exists()) {
                isFileSelected = true;
                imageFilePath = path;
                updateDocumentUploadView();
            } else {
                isFileSelected = false;
                imageFilePath = "";
                updateDocumentUploadView();
                showSnackbarFromTop(AddBankAccountActivity.this, getResources().getString(R.string.unable_to_fetch_image));
            }
        } else {
            Log.d("file_path", "File path is null ");
            isFileSelected = false;
            imageFilePath = "";
            updateDocumentUploadView();
            showSnackbarFromTop(AddBankAccountActivity.this, getResources().getString(R.string.unable_to_fetch_image));
        }
    }

    @Override
    public void fail(String message) {
        imageFilePath = "";
        updateDocumentUploadView();
        showSnackbarFromTop(AddBankAccountActivity.this, getResources().getString(R.string.unable_to_fetch_image));
    }

}
