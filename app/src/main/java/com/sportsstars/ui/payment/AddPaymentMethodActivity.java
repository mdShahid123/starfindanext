package com.sportsstars.ui.payment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.Card;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.BraintreeError;
import com.braintreepayments.api.exceptions.ErrorWithResponse;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.CardBuilder;
import com.braintreepayments.api.models.Configuration;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddPaymentMethodBinding;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.CalendarSyncPostRequest;
import com.sportsstars.network.request.auth.CreateBookingRequest;
import com.sportsstars.network.response.CalendarSyncResponse;
import com.sportsstars.network.response.CreateBookingBaseResponse;
import com.sportsstars.network.response.CreateBookingResponse;
import com.sportsstars.network.response.payment.BraintreeTokenResponse;
import com.sportsstars.network.response.payment.CardData;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.fixture.CreateFixtureListActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.CryptLib;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;

import java.util.Calendar;

import retrofit2.Call;

public class AddPaymentMethodActivity extends BaseActivity implements View.OnClickListener, Alert.OnOkayClickListener ,Alert.OnCancelClickListener {

    private static final String TAG = "BraintreeLog";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 111;
    private static final int BUTTON_ID_PAYPAL = 1;
    private static final int BUTTON_ID_CREDIT_CARD = 2;

    private ActivityAddPaymentMethodBinding mBinding;

    private UserProfileModel mSelectedCoachModel;
    private CreateBookingRequest mCreateBookingRequest;

    private int mExpiryMonth;
    private int mExpiryYear;
    private boolean isCvvInfoShown;
    private String token;
    private BraintreeFragment mBraintreeFragment;
    private ConfigurationListener configurationListener;

    private String cardNumber, expiryDate, cvvNumber;

    private String facilityAddress, sessionDateAndTimeUtc;
    private int buttonId;

    private CardData cardData;
    private boolean isFromSettings;

    private boolean isPayButtonClickedDirectly;
    private MultiClickPreventer multiClickPreventer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_payment_method);
        setUpHeader(mBinding.header, getString(R.string.credit_card_details), getString(R.string.credit_card_details_subtitle));
        getIntentData();
        initViews();
        //getBraintreeToken();
    }

    private void initViews() {
        isCvvInfoShown = false;
        mBinding.etExpiryDate.setOnClickListener(this);
        mBinding.buttonPay.setOnClickListener(this);
        mBinding.ivInfo.setOnClickListener(this);
        mBinding.rlPaypal.setOnClickListener(this);
        mBinding.ivInfo.setVisibility(View.VISIBLE);
        mBinding.tvYourCvvLocated.setVisibility(View.GONE);

        mBinding.etCardNumber.addTextChangedListener(new CreditCardTextWatcher());

        if(!isFromSettings && cardData != null) {
            mBinding.etCardNumber.setFocusable(false);
            mBinding.etCardNumber.setFocusableInTouchMode(false);
            mBinding.etCardNumber.setEnabled(false);

            mBinding.etExpiryDate.setFocusable(false);
            mBinding.etExpiryDate.setFocusableInTouchMode(false);
            mBinding.etExpiryDate.setEnabled(false);

            try {
                if(cardData.getCardNumber() != null && !TextUtils.isEmpty(cardData.getCardNumber())) {
                    CryptLib cryptLib = new CryptLib();
                    String cardNumberDecrypted = cryptLib.decryptCipherTextWithRandomIV(cardData.getCardNumber(), Constants.CIPHER_KEY);
                    mBinding.etCardNumber.setText(cardNumberDecrypted);
                }
                if(cardData.getCardExpiryMonth() != null
                        && !TextUtils.isEmpty(cardData.getCardExpiryMonth())
                        && cardData.getCardExpiryYear() != null
                        && !TextUtils.isEmpty(cardData.getCardExpiryYear())) {
                    CryptLib cryptLib = new CryptLib();
                    String cardExpiryMonthDecrypted = cryptLib.decryptCipherTextWithRandomIV(cardData.getCardExpiryMonth(), Constants.CIPHER_KEY);
                    String cardExpiryYearDecrypted = cryptLib.decryptCipherTextWithRandomIV(cardData.getCardExpiryYear(), Constants.CIPHER_KEY);
                    String yearInTwoDigit = cardExpiryYearDecrypted.substring(2, cardExpiryYearDecrypted.length());

                    mBinding.etExpiryDate.setText(cardExpiryMonthDecrypted + "/" + cardExpiryYearDecrypted);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //showToast("AddPaymentMethod - isFromSettings : " + isFromSettings + " is false");
        }

        multiClickPreventer = new MultiClickPreventer();
    }

    private void getIntentData() {
        mSelectedCoachModel = UserProfileInstance.getInstance().getUserProfileModel();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_SESSION_DATE)) {
                sessionDateAndTimeUtc = bundle.getString(Constants.BundleKey.BOOKING_SESSION_DATE, "");
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_FACILITY_ADDRESS)) {
                facilityAddress = bundle.getString(Constants.BundleKey.BOOKING_FACILITY_ADDRESS, "");
            }
            if(getIntent().hasExtra(Constants.BundleKey.BOOKING_REQUEST_DATA)) {
                mCreateBookingRequest = bundle.getParcelable(Constants.BundleKey.BOOKING_REQUEST_DATA);
            }
            if(getIntent().hasExtra(Constants.BundleKey.CARD_DATA)) {
                cardData = bundle.getParcelable(Constants.BundleKey.CARD_DATA);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SETTINGS)) {
                isFromSettings = bundle.getBoolean(Constants.BundleKey.IS_FROM_SETTINGS);
            }
        }
    }

    private void getBraintreeToken() {
        mBinding.buttonPay.setEnabled(false);
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.getBraintreeClientToken().enqueue(new BaseCallback<BraintreeTokenResponse>(AddPaymentMethodActivity.this) {
            @Override
            public void onSuccess(BraintreeTokenResponse response) {
                if(response != null && response.getStatus() == 1) {
                    token = response.getResult().getClientToken();
                    initiateBraintreeFragment(token);
                } else {
                    mBinding.buttonPay.setEnabled(true);
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddPaymentMethodActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BraintreeTokenResponse> call, BaseResponse baseResponse) {
                mBinding.buttonPay.setEnabled(true);
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
//                    Log.d(TAG, "Error while getting Braintree client token - "
//                        + baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initiateBraintreeFragment(String mAuthorization) {
        try {
            mBinding.buttonPay.setEnabled(false);
            showProgressBar();
            if(mBraintreeFragment != null) {
                mBraintreeFragment = null;
            }
            mBraintreeFragment = BraintreeFragment.newInstance(AddPaymentMethodActivity.this, mAuthorization);

            if(configurationListener != null) {
                Log.d(TAG, "if(configurationListener != null) - call initiatePayment");
                try {
                    hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                initiatePayment(mAuthorization);
            }

            configurationListener = new ConfigurationListener() {
                @Override
                public void onConfigurationFetched(Configuration configuration) {
                    Log.d(TAG, "When new ConfigurationListener() - call initiatePayment");
                    Log.d(TAG, "LINE 103 - configuration - getAssetsUrl : " + configuration.getAssetsUrl()
                            + " getClientApiUrl : " + configuration.getClientApiUrl()
                            + " getEnvironment : " + configuration.getEnvironment()
                            + " getMerchantAccountId : " + configuration.getMerchantAccountId()
                            + " getMerchantId : " + configuration.getMerchantId()
                            + " toJson : " + configuration.toJson());
                    try {
                        hideProgressBar();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    initiatePayment(mAuthorization);
                }
            };

            mBraintreeFragment.addListener(configurationListener);

            mBraintreeFragment.addListener(new PaymentMethodNonceCreatedListener() {
                @Override
                public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
                    Log.d(TAG, "onPaymentMethodNonceCreated");
                    Log.d(TAG, "Description : " + paymentMethodNonce.getDescription());
                    Log.d(TAG, "Nonce : " + paymentMethodNonce.getNonce());
                    Log.d(TAG, "Nonce Type : " + paymentMethodNonce.getTypeLabel());

                    hideProgressBar();
                    //mBinding.buttonPay.setEnabled(true);
                    confirmBooking(paymentMethodNonce.getNonce());

                    if (paymentMethodNonce instanceof PayPalAccountNonce) {
                        PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce)paymentMethodNonce;

                        // Access additional information
                        String email = payPalAccountNonce.getEmail();
                        String firstName = payPalAccountNonce.getFirstName();
                        String lastName = payPalAccountNonce.getLastName();
                        String phone = payPalAccountNonce.getPhone();
                        Log.d(TAG, "Paypal email : " + email);
                        Log.d(TAG, "Paypal firstName : " + firstName);
                        Log.d(TAG, "Paypal lastName : " + lastName);
                        Log.d(TAG, "Paypal phone : " + phone);

                        // See PostalAddress.java for details
                        PostalAddress billingAddress = payPalAccountNonce.getBillingAddress();
                        PostalAddress shippingAddress = payPalAccountNonce.getShippingAddress();
                        Log.d(TAG, "Paypal billingAddress : " + billingAddress.getExtendedAddress());
                        Log.d(TAG, "Paypal shippingAddress : " + shippingAddress.getExtendedAddress());
                    }
                }
            });

//            mBraintreeFragment.addListener(new PaymentMethodNoncesUpdatedListener() {
//                @Override
//                public void onPaymentMethodNoncesUpdated(List<PaymentMethodNonce> paymentMethodNonces) {
//                    hideProgressBar();
//                    Log.d(TAG, "onPaymentMethodNoncesUpdated");
//                    for (PaymentMethodNonce nonce : paymentMethodNonces) {
//                        Log.d(TAG, "Description : " + nonce.getDescription());
//                        Log.d(TAG, "Nonce : " + nonce.getNonce());
//                        Log.d(TAG, "Nonce Type : " + nonce.getTypeLabel());
//                    }
//                }
//            });

            mBraintreeFragment.addListener(new BraintreeErrorListener() {
                @Override
                public void onError(Exception error) {
                    hideProgressBar();
                    mBinding.buttonPay.setEnabled(true);
                    Log.d(TAG, "BraintreeErrorListener");
                    Log.d(TAG, "onError - " + error.getMessage());

                    showSnackbarFromTop(AddPaymentMethodActivity.this, ""+error.getMessage());

                    if (error instanceof ErrorWithResponse) {
                        ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
                        BraintreeError cardErrors = errorWithResponse.errorFor("creditCard");
                        if (cardErrors != null) {
                            // There is an issue with the credit card.
                            BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
                            if (expirationMonthError != null) {
                                // There is an issue with the expiration month.
                                Log.d(TAG, "Expiration Error : " + expirationMonthError.getMessage());
                            }
                        }
                    }
                }
            });

            // mBraintreeFragment is ready to use!
            Log.d(TAG, "initiateBraintree - end of try block");
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            Log.d(TAG, "initiateBraintree - InvalidArgumentException " + e.getMessage());
            hideProgressBar();
            mBinding.buttonPay.setEnabled(true);
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "initiateBraintree - Exception " + e.getMessage());
            hideProgressBar();
            mBinding.buttonPay.setEnabled(true);
            e.printStackTrace();
        }
    }

    private void initiatePayment(String mAuthorization) {
        switch (buttonId) {
            case BUTTON_ID_PAYPAL:
                payUsingPaypal();
                break;
            case BUTTON_ID_CREDIT_CARD:
                Log.d("btnClick", "payUsingCreditCard called from initiatePayment");
                if(isPayButtonClickedDirectly) {
                    return;
                }
                payUsingCreditCard();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_expiry_date:
                Dialog dialog = setDateAlert();
                dialog.show();
                break;

            case R.id.button_pay:
                multiClickPreventer.preventMultiClick(view);
                buttonId = BUTTON_ID_CREDIT_CARD;
                if(Utils.isNetworkAvailable()) {
                    if(mBraintreeFragment == null) {
                        isPayButtonClickedDirectly = false;
                        getBraintreeToken();
                        return;
                    }
                    Log.d("btnClick", "payUsingCreditCard called directly when mBraintreeFragment != null");
                    isPayButtonClickedDirectly = true;
                    payUsingCreditCard();
                } else {
                    showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.iv_info:
                showHideCvvInfo();
                break;

            case R.id.rlPaypal:
                buttonId = BUTTON_ID_PAYPAL;
                if(Utils.isNetworkAvailable()) {
                    if(mBraintreeFragment == null) {
                        getBraintreeToken();
                        return;
                    }
                    payUsingPaypal();
                } else {
                    showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private void payUsingCreditCard() {
        mBinding.buttonPay.setEnabled(false);
        if (isInputValid()) {
            showProgressBar();

            CardBuilder cardBuilder = new CardBuilder()
                    .cardNumber(cardNumber)
                    .expirationDate(expiryDate)
                    .cvv(cvvNumber);

            Card.tokenize(mBraintreeFragment, cardBuilder);
        }
    }

    private void payUsingPaypal() {
        //PayPal.authorizeAccount(mBraintreeFragment);
        try {
            showProgressBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupBraintreeAndStartExpressCheckout();
    }

    private Dialog setDateAlert() {
        int MAX_YEAR;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        Calendar cal = Calendar.getInstance();

        View dialog = inflater.inflate(R.layout.date_picker_dialog, null);
        final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setValue(cal.get(Calendar.MONTH) + 1);

        final int year = cal.get(Calendar.YEAR);
        yearPicker.setMinValue(year);
        MAX_YEAR = year + 20;
        yearPicker.setMaxValue(MAX_YEAR);
        yearPicker.setValue(year);
        final int currentMonth = cal.get(Calendar.MONTH);

        builder.setView(dialog)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), 0);
                        mExpiryMonth = monthPicker.getValue();
                        mExpiryYear = yearPicker.getValue();
                        String yearInString = "" + yearPicker.getValue();
                        //String yearInTwoDigit = yearInString.substring(2, yearInString.length());

                        if (year == mExpiryYear && currentMonth + 1 > mExpiryMonth) {
                            mBinding.etExpiryDate.setError(getString(R.string.month_error));
                            showSnackbarFromTop(AddPaymentMethodActivity.this, getString(R.string.month_error));
                            mBinding.etExpiryDate.setText("");
                        } else {
                            mBinding.etExpiryDate.setError(null);
                            if (monthPicker.getValue() > 9) {
                                mBinding.etExpiryDate.setText("" + monthPicker.getValue() + "/" + yearInString);
                            } else {
                                mBinding.etExpiryDate.setText("0" + monthPicker.getValue() + "/" + yearInString);
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //getDialog().cancel();
                        dialog.cancel();

                    }
                });
        return builder.create();
    }

    private boolean isInputValid() {
        cardNumber = mBinding.etCardNumber.getText().toString().trim();
        if(TextUtils.isEmpty(cardNumber)) {
            mBinding.buttonPay.setEnabled(true);
            showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.enter_card_number));
            return false;
        }

        cardNumber = cardNumber.replaceAll(" ","");
        cardNumber = cardNumber.trim();

        if(cardNumber.length() < Constants.MIN_CREDIT_CARD_NUMBER_LIMIT) {
            mBinding.buttonPay.setEnabled(true);
            showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.min_limit_card_number));
            return false;
        }

        expiryDate = mBinding.etExpiryDate.getText().toString().trim();
        if(TextUtils.isEmpty(expiryDate)) {
            mBinding.buttonPay.setEnabled(true);
            showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.enter_expiry_date));
            return false;
        }

        cvvNumber = mBinding.etCvv.getText().toString().trim();
        if(TextUtils.isEmpty(cvvNumber)) {
            mBinding.buttonPay.setEnabled(true);
            showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.enter_cvv_number));
            return false;
        }

        if(cvvNumber.length() < Constants.MIN_CVV_NUMBER_LIMIT) {
            mBinding.buttonPay.setEnabled(true);
            showSnackbarFromTop(AddPaymentMethodActivity.this, getResources().getString(R.string.min_limit_cvv_number));
            return false;
        }

        return true;
    }

    private void showHideCvvInfo() {
        if(isCvvInfoShown) {
            mBinding.tvYourCvvLocated.setVisibility(View.INVISIBLE);
            isCvvInfoShown = false;
        } else {
            mBinding.tvYourCvvLocated.setVisibility(View.VISIBLE);
            isCvvInfoShown = true;
        }
    }

    public void setupBraintreeAndStartExpressCheckout() {
        if(mCreateBookingRequest != null && mCreateBookingRequest.getPrice() != 0.0d) {
            PayPalRequest request = new PayPalRequest(String.valueOf(mCreateBookingRequest.getPrice()))
                    .currencyCode(Constants.CURRENCY_USD)
                    .intent(PayPalRequest.INTENT_AUTHORIZE);
            PayPal.requestOneTimePayment(mBraintreeFragment, request);
        } else {
            showSnackbarFromTop(AddPaymentMethodActivity.this, "Not getting booking amount from confirm booking screen.");
        }
    }

    private void confirmBooking(String paymentMethodNonce) {

        if(mCreateBookingRequest == null) {
            mBinding.buttonPay.setEnabled(true);
            return;
        }

        if(paymentMethodNonce == null) {
            mBinding.buttonPay.setEnabled(true);
            return;
        }

        if(TextUtils.isEmpty(paymentMethodNonce)) {
            mBinding.buttonPay.setEnabled(true);
            return;
        }

        mBinding.buttonPay.setEnabled(false);

        mCreateBookingRequest.setPaymentNounce(paymentMethodNonce);

        processToShowDialog();

        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.createLearnerBooking(mCreateBookingRequest).enqueue(new BaseCallback<CreateBookingBaseResponse>(this) {
            @Override
            public void onSuccess(CreateBookingBaseResponse response) {
                if(response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        calendarSyncPost(response.getResult());
                    }

                    String msg="You have successfully booked your session with " + mSelectedCoachModel.getName() + "."+"\n\n" +
                            "Would you like to add some more fixtures so "+ mSelectedCoachModel.getName()+" can send you tips on matchday?";

                    try {
                        Alert.showAlertDialogWithBottomButton(AddPaymentMethodActivity.this,getString(R.string.success),msg,getString(R.string.do_this_later),getString(R.string.yes_please),
                                AddPaymentMethodActivity.this,AddPaymentMethodActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                if (ContextCompat.checkSelfPermission(ConfirmBookingActivity.this, Manifest.permission.WRITE_CALENDAR)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    askCalendarPermission();
//                } else {
//                    addBookingEventToCalendar();
//                }

                    mBinding.buttonPay.setEnabled(true);

                } else {
                    mBinding.buttonPay.setEnabled(true);
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddPaymentMethodActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CreateBookingBaseResponse> call, BaseResponse baseResponse) {
                mBinding.buttonPay.setEnabled(true);
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //showSnackbarFromTop(AddPaymentMethodActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void calendarSyncPost(CreateBookingResponse createBookingResponse) {
        if(createBookingResponse != null) {
            AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
            CalendarSyncPostRequest calendarSyncPostRequest = new CalendarSyncPostRequest();
            calendarSyncPostRequest.setSessionId(createBookingResponse.getSessionId());
            calendarSyncPostRequest.setCoachId(createBookingResponse.getCoachId());
            calendarSyncPostRequest.setLearnerId(createBookingResponse.getLearnerId());
            calendarSyncPostRequest.setSessionDate(createBookingResponse.getSessionDate());
            authWebServices.calendarSyncPost(calendarSyncPostRequest).enqueue(new BaseCallback<BaseResponse>(this) {
                @Override
                public void onSuccess(BaseResponse response) {
                    if(response != null && response.getStatus() == 1) {
                        if (ContextCompat.checkSelfPermission(AddPaymentMethodActivity.this, Manifest.permission.WRITE_CALENDAR)
                                != PackageManager.PERMISSION_GRANTED) {
                            askCalendarPermission();
                        } else {
                            addBookingEventToCalendar();
                        }
                    }
                }

                @Override
                public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                    try {
                        hideProgressBar();
                        if(baseResponse != null && baseResponse.getMessage() != null
                                && !TextUtils.isEmpty(baseResponse.getMessage())) {
                            //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public void onOkay() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
        if(!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearTop(AddPaymentMethodActivity.this, CreateFixtureListActivity.class, bundle);
        } else {
            Navigator.getInstance().navigateToActivityWithData(AddPaymentMethodActivity.this, CreateFixtureListActivity.class, bundle);
        }
        finish();
    }

    @Override
    public void onCancel() {
        if(!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearTop(AddPaymentMethodActivity.this,
                    LearnerHomeActivity.class, null);
        }
        finish();
    }

    @Override
    public String getActivityName() {
        return AddPaymentMethodActivity.class.getSimpleName();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mBraintreeFragment != null) {
            mBraintreeFragment.removeListener(configurationListener);
        }
        if(configurationListener != null) {
            configurationListener = null;
        }
    }

    // PERMISSION RELATED TASKS //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addBookingEventToCalendar();
                } else {
                    Toast.makeText(this, "Permission not granted to access device default calendar.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(AddPaymentMethodActivity.this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void addBookingEventToCalendar() {
        String sportName = "", coachName = "", sessionDateTime = "";
        long startTime = 0;

        if(mSelectedCoachModel == null) {
            return;
        }
        if(TextUtils.isEmpty(sessionDateAndTimeUtc)) {
            return;
        }
        if(TextUtils.isEmpty(facilityAddress)) {
            return;
        }

        if(mSelectedCoachModel != null) {
            sportName = mSelectedCoachModel.getSportDetail().getName();
            coachName = mSelectedCoachModel.getName();
        }
        if(sessionDateAndTimeUtc != null) {
            startTime = Utils.getTimeInMillis(sessionDateAndTimeUtc);
            sessionDateTime = sessionDateAndTimeUtc;
        }

        if(!TextUtils.isEmpty(sportName) && !TextUtils.isEmpty(coachName)
                && !TextUtils.isEmpty(facilityAddress) && startTime != 0) {
//            Utils.addEventToCalendar(ConfirmBookingActivity.this,
//                    sportName + " with " + coachName, facilityAddress, startTime, true);

            CalendarSyncResponse calendarSyncResponse = new CalendarSyncResponse();
            calendarSyncResponse.setCoachName(coachName);
            calendarSyncResponse.setFacility(facilityAddress);
            calendarSyncResponse.setLearnerName("");
            calendarSyncResponse.setSessionDate(sessionDateTime);
            calendarSyncResponse.setSportName(sportName);
            calendarSyncResponse.setTitle("StarFinda Coaching Session with " + coachName + " for " + sportName);
            addBookingEventToCalendar(calendarSyncResponse);
        }
    }

    private void addBookingEventToCalendar(CalendarSyncResponse calendarSyncResponse) {
        CalendarSyncHelper.getInstance().addCalendarEvent(AddPaymentMethodActivity.this, calendarSyncResponse);
    }

    public class CreditCardTextWatcher implements TextWatcher {

        private static final char space = ' ';

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove all spacing char
            int pos = 0;
            while (true) {
                if (pos >= s.length()) break;
                if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                    s.delete(pos, pos + 1);
                } else {
                    pos++;
                }
            }

            // Insert char where needed.
            pos = 4;
            while (true) {
                if (pos >= s.length()) break;
                final char c = s.charAt(pos);
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space);
                }
                pos += 5;
            }
        }

    }

    public class MultiClickPreventer {

        private static final long DELAY_IN_MS = 1500;

        public void preventMultiClick(final View view) {
            if (!view.isClickable()) {
                return;
            }
            view.setClickable(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setClickable(true);
                }
            }, DELAY_IN_MS);
        }
    }

}
