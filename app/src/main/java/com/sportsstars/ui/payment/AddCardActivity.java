package com.sportsstars.ui.payment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.sportsstars.BuildConfig;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddCardBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.AddCardRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.CryptLib;
import com.sportsstars.util.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.util.Calendar;

import retrofit2.Call;

//import com.braintreepayments.api.Card;
//import com.braintreepayments.api.Card;

public class AddCardActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "BraintreeLog";

    private ActivityAddCardBinding mBinder;

    private int mExpiryMonth;
    private int mExpiryYear;
    private boolean isCvvInfoShown;
    private String token;
    private BraintreeFragment mBraintreeFragment;
    private ConfigurationListener configurationListener;
    private String cardNumber, expiryDate, cvvNumber;

    private boolean isAddCardClickedDirectly;
    private MultiClickPreventer multiClickPreventer;

    private Card mCard;
    private String mStripeToken = "";

    @Override
    public String getActivityName() {
        return BaseActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_add_card);

        initViews();
    }

    private void initViews() {
        isCvvInfoShown = false;

        mBinder.headerId.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinder.headerId.tvTitle.setText(getString(R.string.add_new_credit_card));
        mBinder.etExpiryDate.setOnClickListener(this);
        mBinder.buttonAddCard.setOnClickListener(this);
        mBinder.headerId.ivBack.setOnClickListener(this);
        mBinder.ivInfo.setOnClickListener(this);
        mBinder.ivInfo.setVisibility(View.VISIBLE);
        mBinder.tvYourCvvLocated.setVisibility(View.GONE);

        mBinder.etCardNumber.addTextChangedListener(new CreditCardTextWatcher());

        multiClickPreventer = new MultiClickPreventer();
    }

//    private void getBraintreeToken() {
//        mBinder.buttonAddCard.setEnabled(false);
//        //mBinder.buttonAddCard.setVisibility(View.GONE);
//        processToShowDialog();
//        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
//        webServices.getBraintreeClientToken().enqueue(new BaseCallback<BraintreeTokenResponse>(AddCardActivity.this) {
//            @Override
//            public void onSuccess(BraintreeTokenResponse response) {
//                if(response != null && response.getStatus() == 1) {
//                    token = response.getResult().getClientToken();
//                    initiateBraintreeFragment(token);
//                } else {
//                    mBinder.buttonAddCard.setEnabled(true);
//                    //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//                    try {
//                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
//                            showSnackbarFromTop(AddCardActivity.this, response.getMessage());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFail(Call<BraintreeTokenResponse> call, BaseResponse baseResponse) {
//                mBinder.buttonAddCard.setEnabled(true);
//                //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//                try {
//                    if(baseResponse != null && baseResponse.getMessage() != null
//                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
////                        Log.d(TAG, "Error while getting Braintree client token - "
////                                + baseResponse.getMessage());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }

//    private void initiateBraintreeFragment(String mAuthorization) {
//        try {
//            mBinder.buttonAddCard.setEnabled(false);
//            //mBinder.buttonAddCard.setVisibility(View.GONE);
//            showProgressBar();
//            if(mBraintreeFragment != null) {
//                mBraintreeFragment = null;
//            }
//            mBraintreeFragment = BraintreeFragment.newInstance(AddCardActivity.this, mAuthorization);
//
//            if(configurationListener != null) {
//                //hideProgressBar();
//                initiateCreditCardValidation(mAuthorization);
//            }
//
//            configurationListener = new ConfigurationListener() {
//                @Override
//                public void onConfigurationFetched(Configuration configuration) {
//                    Log.d(TAG, "LINE 131 - configuration - getAssetsUrl : " + configuration.getAssetsUrl()
//                            + " getClientApiUrl : " + configuration.getClientApiUrl()
//                            + " getEnvironment : " + configuration.getEnvironment()
//                            + " getMerchantAccountId : " + configuration.getMerchantAccountId()
//                            + " getMerchantId : " + configuration.getMerchantId()
//                            + " toJson : " + configuration.toJson());
//                    //hideProgressBar();
//                    initiateCreditCardValidation(mAuthorization);
//                }
//            };
//
//            mBraintreeFragment.addListener(configurationListener);
//
//            mBraintreeFragment.addListener(new PaymentMethodNonceCreatedListener() {
//                @Override
//                public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
//                    try {
//                        Log.d(TAG, "onPaymentMethodNonceCreated");
//                        Log.d(TAG, "Description : " + paymentMethodNonce.getDescription());
//                        Log.d(TAG, "Nonce : " + paymentMethodNonce.getNonce());
//                        Log.d(TAG, "Nonce Type : " + paymentMethodNonce.getTypeLabel());
//
//                        if (paymentMethodNonce instanceof PayPalAccountNonce) {
//                            hideProgressBar();
//                            mBinder.buttonAddCard.setEnabled(true);
//                            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//
//                            PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce)paymentMethodNonce;
//
//                            // Access additional information
//                            String email = payPalAccountNonce.getEmail();
//                            String firstName = payPalAccountNonce.getFirstName();
//                            String lastName = payPalAccountNonce.getLastName();
//                            String phone = payPalAccountNonce.getPhone();
//                            Log.d(TAG, "Paypal email : " + email);
//                            Log.d(TAG, "Paypal firstName : " + firstName);
//                            Log.d(TAG, "Paypal lastName : " + lastName);
//                            Log.d(TAG, "Paypal phone : " + phone);
//
//                            // See PostalAddress.java for details
//                            PostalAddress billingAddress = payPalAccountNonce.getBillingAddress();
//                            PostalAddress shippingAddress = payPalAccountNonce.getShippingAddress();
//                            Log.d(TAG, "Paypal billingAddress : " + billingAddress.getExtendedAddress());
//                            Log.d(TAG, "Paypal shippingAddress : " + shippingAddress.getExtendedAddress());
//                        } else {
//                            CardNonce cardNonce = (CardNonce) paymentMethodNonce;
//                            Log.d(TAG, "CardNonce Description : " + cardNonce.getDescription());
//                            Log.d(TAG, "CardNonce Nonce : " + cardNonce.getNonce());
//                            Log.d(TAG, "CardNonce Nonce Type Label : " + cardNonce.getTypeLabel());
//                            Log.d(TAG, "CardNonce Nonce Type Id : " + cardNonce.getCardType());
//
//                            hideProgressBar();
//                            String nonce = paymentMethodNonce.getNonce();
//                            if(nonce != null && !TextUtils.isEmpty(nonce)) {
//                                addCardApi(nonce, cardNonce.getTypeLabel());
//                            } else {
//                                hideProgressBar();
//                                mBinder.buttonAddCard.setEnabled(true);
//                                //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    } catch (Exception e) {
//                        hideProgressBar();
//                        mBinder.buttonAddCard.setEnabled(true);
//                        //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//                        Log.d(TAG, "onPaymentMethodNonceCreated error occurred - " + e.getMessage());
//                        e.printStackTrace();
//                    }
//                }
//            });
//
////            mBraintreeFragment.addListener(new PaymentMethodNoncesUpdatedListener() {
////                @Override
////                public void onPaymentMethodNoncesUpdated(List<PaymentMethodNonce> paymentMethodNonces) {
////                    hideProgressBar();
////                    Log.d(TAG, "onPaymentMethodNoncesUpdated");
////                    for (PaymentMethodNonce nonce : paymentMethodNonces) {
////                        Log.d(TAG, "Description : " + nonce.getDescription());
////                        Log.d(TAG, "Nonce : " + nonce.getNonce());
////                        Log.d(TAG, "Nonce Type : " + nonce.getTypeLabel());
////                    }
////                }
////            });
//
//            mBraintreeFragment.addListener(new BraintreeErrorListener() {
//                @Override
//                public void onError(Exception error) {
//                    hideProgressBar();
//                    mBinder.buttonAddCard.setEnabled(true);
//                    //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//                    Log.d(TAG, "BraintreeErrorListener");
//                    Log.d(TAG, "onError - " + error.getMessage());
//                    showSnackbarFromTop(AddCardActivity.this, ""+error.getMessage());
//                    if (error instanceof ErrorWithResponse) {
//                        ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
//                        BraintreeError cardErrors = errorWithResponse.errorFor("creditCard");
//                        if (cardErrors != null) {
//                            // There is an issue with the credit card.
//                            BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
//                            if (expirationMonthError != null) {
//                                // There is an issue with the expiration month.
//                                Log.d(TAG, "Expiration Error : " + expirationMonthError.getMessage());
//                            }
//                        }
//                    }
//                }
//            });
//
//            // mBraintreeFragment is ready to use!
//            Log.d(TAG, "initiateBraintree - end of try block");
//        } catch (InvalidArgumentException e) {
//            // There was an issue with your authorization string.
//            Log.d(TAG, "initiateBraintree - InvalidArgumentException " + e.getMessage());
//            hideProgressBar();
//            mBinder.buttonAddCard.setEnabled(true);
//            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//            e.printStackTrace();
//        } catch (Exception e) {
//            Log.d(TAG, "initiateBraintree - Exception " + e.getMessage());
//            hideProgressBar();
//            mBinder.buttonAddCard.setEnabled(true);
//            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
//            e.printStackTrace();
//        }
//    }
//
//    private void initiateCreditCardValidation(String mAuthorization) {
//        if(isAddCardClickedDirectly) {
//            return;
//        }
//        addCreditCard();
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_expiry_date:
                Dialog dialog = setDateAlert();
                dialog.show();
                break;

            case R.id.button_add_card:
                multiClickPreventer.preventMultiClick(view);
                if(Utils.isNetworkAvailable()) {
//                    if(mBraintreeFragment == null) {
//                        isAddCardClickedDirectly = false;
//                        getBraintreeToken();
//                        return;
//                    }
                    isAddCardClickedDirectly = true;
                    addCreditCard();
                } else {
                    showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.iv_info:
                showHideCvvInfo();
                break;
        }
    }

    private boolean isInputValid() {
        cardNumber = mBinder.etCardNumber.getText().toString().trim();
        if(TextUtils.isEmpty(cardNumber)) {
            showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.enter_card_number));
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
            return false;
        }

        cardNumber = cardNumber.replaceAll(" ","");
        cardNumber = cardNumber.trim();

        if(cardNumber.length() < Constants.MIN_CREDIT_CARD_NUMBER_LIMIT) {
            showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.min_limit_card_number));
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
            return false;
        }

        expiryDate = mBinder.etExpiryDate.getText().toString().trim();
        if(TextUtils.isEmpty(expiryDate)) {
            showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.enter_expiry_date));
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
            return false;
        }

        cvvNumber = mBinder.etCvv.getText().toString().trim();
        if(TextUtils.isEmpty(cvvNumber)) {
            showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.enter_cvv_number));
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
            return false;
        }

        if(cvvNumber.length() < Constants.MIN_CVV_NUMBER_LIMIT) {
            showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.min_limit_cvv_number));
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
            return false;
        }

        mCard = new Card(cardNumber, mExpiryMonth, mExpiryYear, cvvNumber);
        if (!mCard.validateCard()) {
            showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.credit_card_invalid));
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            return false;
        }

        return true;
    }

    private void addCreditCard() {
        mBinder.buttonAddCard.setEnabled(false);
        //mBinder.buttonAddCard.setVisibility(View.GONE);
        if (isInputValid()) {
//            showProgressBar();

//            CardBuilder cardBuilder = new CardBuilder()
//                    .cardNumber(cardNumber)
//                    .expirationDate(expiryDate)
//                    .cvv(cvvNumber)
//                    .validate(true);
//            com.braintreepayments.api.Card.tokenize(mBraintreeFragment, cardBuilder);

            saveCard();
        }
    }

    private void showHideCvvInfo() {
        if(isCvvInfoShown) {
            mBinder.tvYourCvvLocated.setVisibility(View.INVISIBLE);
            isCvvInfoShown = false;
        } else {
            mBinder.tvYourCvvLocated.setVisibility(View.VISIBLE);
            isCvvInfoShown = true;
        }
    }

    private Dialog setDateAlert() {
        int MAX_YEAR;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        Calendar cal = Calendar.getInstance();

        View dialog = inflater.inflate(R.layout.date_picker_dialog, null);
        final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setValue(cal.get(Calendar.MONTH) + 1);

        final int year = cal.get(Calendar.YEAR);
        yearPicker.setMinValue(year);
        MAX_YEAR = year + 20;
        yearPicker.setMaxValue(MAX_YEAR);
        yearPicker.setValue(year);
        final int currentMonth = cal.get(Calendar.MONTH);

        builder.setView(dialog)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //
                        // listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), 0);
                        mExpiryMonth = monthPicker.getValue();
                        mExpiryYear = yearPicker.getValue();
                        String yearInString = "" + yearPicker.getValue();
                        //String yearInTwoDigit = yearInString.substring(2, yearInString.length());

                        if (year == mExpiryYear && currentMonth + 1 > mExpiryMonth) {
                            mBinder.etExpiryDate.setError(getString(R.string.month_error));
                            showSnackbarFromTop(AddCardActivity.this, getString(R.string.month_error));
                            mBinder.etExpiryDate.setText("");
                        } else {
                            mBinder.etExpiryDate.setError(null);

                            if (monthPicker.getValue() > 9) {
                                mBinder.etExpiryDate.setText("" + monthPicker.getValue() + "/" + yearInString);

                            } else {
                                mBinder.etExpiryDate.setText("0" + monthPicker.getValue() + "/" + yearInString);

                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //getDialog().cancel();
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    private void addCardApi(String cardNonce, String cardBrandName) {
        processToShowDialog();

        AddCardRequest addCardRequest = new AddCardRequest();
        addCardRequest.setCardToken(cardNonce);

        //cardNumber
        String expiryMonth = "";
        String expiryYear = "";
        String[] expiryDateStrArray = expiryDate.split("/");
        if(expiryDateStrArray != null && expiryDateStrArray.length > 1) {
            expiryMonth = expiryDateStrArray[0];
            expiryYear = expiryDateStrArray[1];
        }

        try {
            CryptLib cryptLib = new CryptLib();
            String cardNumberEncrypted = cryptLib.encryptPlainTextWithRandomIV(cardNumber, Constants.CIPHER_KEY);
            String expiryMonthEncrypted = cryptLib.encryptPlainTextWithRandomIV(expiryMonth, Constants.CIPHER_KEY);
            String expiryYearEncrypted = cryptLib.encryptPlainTextWithRandomIV(expiryYear, Constants.CIPHER_KEY);

//            addCardRequest.setCardNumber(cardNumberEncrypted);
//            addCardRequest.setCardExpiryMonth(expiryMonthEncrypted);
//            addCardRequest.setCardExpiryYear(expiryYearEncrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        addCardRequest.setCardType(CardMatcher.getCardTypeId(cardBrandName));


        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.addCard(addCardRequest).enqueue(new BaseCallback<BaseResponse>(AddCardActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showToast(response.getMessage());
                                //showSnackbarFromTop(AddCardActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finish();
                    } else {
                        mBinder.buttonAddCard.setEnabled(true);
                        //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(AddCardActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    mBinder.buttonAddCard.setEnabled(true);
                    //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                mBinder.buttonAddCard.setEnabled(true);
                //mBinder.buttonAddCard.setVisibility(View.VISIBLE);
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddCardActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mBraintreeFragment != null) {
            mBraintreeFragment.removeListener(configurationListener);
        }
        if(configurationListener != null) {
            configurationListener = null;
        }
    }

    public class CreditCardTextWatcher implements TextWatcher {

        private static final char space = ' ';

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove all spacing char
            int pos = 0;
            while (true) {
                if (pos >= s.length()) break;
                if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                    s.delete(pos, pos + 1);
                } else {
                    pos++;
                }
            }

            // Insert char where needed.
            pos = 4;
            while (true) {
                if (pos >= s.length()) break;
                final char c = s.charAt(pos);
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space);
                }
                pos += 5;
            }
        }

    }

    public class MultiClickPreventer {

        private static final long DELAY_IN_MS = 1500;

        public void preventMultiClick(final View view) {
            if (!view.isClickable()) {
                return;
            }
            view.setClickable(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setClickable(true);
                }
            }, DELAY_IN_MS);
        }
    }

    /**
     * This function used to get Stripe Token based on user Card Info, Return Success if Card information is valid and return a token
     */
    private void saveCard() {
        showProgressBar();
        try {
            final Stripe stripe = new Stripe(this, BuildConfig.STRIPE_PUBLISHABLE_KEY);
            stripe.createToken(
                    mCard,
                    new TokenCallback() {
                        @Override
                        public void onError(Exception error) {
                            // Show localized error message
                            hideProgressBar();
                            mBinder.buttonAddCard.setEnabled(true);
                            mStripeToken = "";
                            Log.d(TAG, "Stripe Token Err >> " + error.getMessage());
                            showSnackbarFromTop(AddCardActivity.this, error.getLocalizedMessage());
                        }

                        @Override
                        public void onSuccess(Token token) {
                            //stripeToken = token.getId();
                            hideProgressBar();
                            mStripeToken = token.getId();
                            Log.d(TAG, "Stripe Token >> " + mStripeToken
                                    + " main token data - "
                                    + " id : " + token.getId()
                                    + " type : " + token.getType()
                                    + " bank account : " + token.getBankAccount()
                                    + " card brand : " + token.getCard().getBrand()
                                    + " created : " + token.getCreated().toString()
                                    + " live mode : " + token.getLivemode()
                                    + " used : " + token.getUsed()
                            );

                            if(Utils.isNetworkAvailable()) {
                                addCardApi(mStripeToken, "");
                            } else {
                                mBinder.buttonAddCard.setEnabled(true);
                                showSnackbarFromTop(AddCardActivity.this, getResources().getString(R.string.no_internet));
                            }
                        }
                    }
            );
        } catch (Exception e) {
            hideProgressBar();
            mBinder.buttonAddCard.setEnabled(true);
            mStripeToken = "";
            Log.d(TAG, "StripeTokenErr>>" + e.getMessage());
            e.printStackTrace();
        }
    }

}
