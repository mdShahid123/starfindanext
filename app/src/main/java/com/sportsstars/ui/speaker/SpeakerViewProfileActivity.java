package com.sportsstars.ui.speaker;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.chat.chat.ImagePreviewActivity;
import com.sportsstars.databinding.ActivitySpeakerViewProfileBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.interfaces.RecyclerPageChangeListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.speaker.EventList;
import com.sportsstars.network.response.speaker.EventListResponse;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.network.response.speaker.EventSubcategoryDetail;
import com.sportsstars.network.response.speaker.GuestSpeakerEvents;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.ImageAdapter;
import com.sportsstars.ui.auth.adapter.LinePagerIndicatorDecoration;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.ui.speaker.adapter.SpeakerProfileEventListAdapter;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.sportsstars.widget.SimpleDividerItemDecoration;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import retrofit2.Call;

public class SpeakerViewProfileActivity extends BaseActivity implements View.OnClickListener,
        ImageAdapter.MediaClickCallBack, RecyclerPageChangeListener, ListItemClickListener {

    private ActivitySpeakerViewProfileBinding mBinding;
    private UserProfileModel mUserProfileModel;
    private ArrayList<EventViewHolderModel> mGlobalEventList = new ArrayList<>();
    private SpeakerProfileEventListAdapter mSpeakerProfileEventListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_speaker_view_profile);

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

//        mUserProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
//        bindDataSet();
//        initViews();
//        setListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(otpVerifiedEvent.isOtpVerified()) {
            //finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setListeners();
        if(Utils.isNetworkAvailable()) {
            getGuestSpeakerProfile();
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }

//        mUserProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
//        bindDataSet();
//        initViews();
//        setListeners();
//        if(Utils.isNetworkAvailable()) {
//            getEventList();
//        } else {
//            mBinding.rvEventList.setVisibility(View.GONE);
//            mBinding.tvNoEvents.setVisibility(View.VISIBLE);
//            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
//        }
    }

    private void setListeners() {
        mBinding.tvEdit.setOnClickListener(this);
    }

    private void bindDataSet() {
        mBinding.setSpeakerInfo(mUserProfileModel);
    }

    private void initViews() {
        setProfilePic();
        setSportIcon();
        setGender();
        setTravelType();
        //setEventList();
        //setCerView();
        setTeamView();
        //setLocView();
        setHonourView();
        //setSessionView();
        //setSpecialityView();
        setMediaView();
    }

    private void setEventList() {
        mSpeakerProfileEventListAdapter = new SpeakerProfileEventListAdapter(this, mGlobalEventList, this);
        //mBinding.rvEventInfo.setHasFixedSize(true);
        mBinding.rvEventList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvEventList.addItemDecoration(new SimpleDividerItemDecoration(SpeakerViewProfileActivity.this));
        mBinding.rvEventList.setAdapter(mSpeakerProfileEventListAdapter);
    }

    private void setHonourView() {
        mBinding.llHonour.removeAllViews();
        if (!StringUtils.isNullOrEmpty(mUserProfileModel.getAccolades())) {
            String[] items = mUserProfileModel.getAccolades().split("\\s*,\\s*");
            drawView(items, mBinding.llHonour);
        }
    }

    private void setTeamView() {
        mBinding.llTeams.removeAllViews();
        List<Team> list = mUserProfileModel.getTeamPlayedFor();
        for (Team team : list) {
            if(team != null && !TextUtils.isEmpty(team.getName())) {
                View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
                ((TextView) view.findViewById(R.id.tvSpecList)).setText(team.getName());
                mBinding.llTeams.addView(view);
            }
        }
    }

    private void drawView(String[] items, LinearLayout container) {
        for (String item : items) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
            ((TextView) view.findViewById(R.id.tvSpecList)).setText(item);
            container.addView(view);
        }
    }

    private void setMediaView() {

        if(mUserProfileModel.getMediaUrl().size() > 0) {
            mBinding.mediaContainer.setVisibility(View.VISIBLE);
            mBinding.tvPagesMedia.setVisibility(View.VISIBLE);
            mBinding.tvMediaHeader.setVisibility(View.VISIBLE);
        } else {
            mBinding.mediaContainer.setVisibility(View.GONE);
            mBinding.tvPagesMedia.setVisibility(View.GONE);
            mBinding.tvMediaHeader.setVisibility(View.GONE);
        }

        ImageAdapter imageAdapter = new ImageAdapter(mUserProfileModel.getMediaUrl(),this);
        mBinding.mediaContainer.setHasFixedSize(true);
        mBinding.mediaContainer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mBinding.mediaContainer.setAdapter(imageAdapter);

        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        mBinding.mediaContainer.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(mBinding.mediaContainer);

        mBinding.mediaContainer.addItemDecoration(new LinePagerIndicatorDecoration(this));

        if(mUserProfileModel.getMediaUrl().size() > 0) {
            mBinding.tvPagesMedia.setText("1/"+mUserProfileModel.getMediaUrl().size());
        }

    }

    private void getEventList() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.getEventList().enqueue(new BaseCallback<EventListResponse>(SpeakerViewProfileActivity.this) {
            @Override
            public void onSuccess(EventListResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null && response.getResult().size() > 0) {
                            if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
                                mGlobalEventList.clear();
                            }
                            generateEventList(response.getResult());
                            mBinding.rvEventList.setVisibility(View.VISIBLE);
                            mBinding.tvNoEvents.setVisibility(View.GONE);
                            setEventList();
                        } else {
                            mBinding.rvEventList.setVisibility(View.GONE);
                            mBinding.tvNoEvents.setVisibility(View.VISIBLE);
                        }
                    } else {
                        try {
                            mBinding.rvEventList.setVisibility(View.GONE);
                            mBinding.tvNoEvents.setVisibility(View.VISIBLE);
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(SpeakerViewProfileActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<EventListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    mBinding.rvEventList.setVisibility(View.GONE);
                    mBinding.tvNoEvents.setVisibility(View.VISIBLE);
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerViewProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void generateEventList(ArrayList<EventList> localEventList) {
        if(localEventList != null && localEventList.size() > 0) {
            for(int i = 0; i < localEventList.size(); i++) {
                EventList eventList = localEventList.get(i);
                if(eventList != null) {
                    boolean isEventPriceAvailable = isEventPriceAvailable(eventList);
                    if(isEventPriceAvailable) {
                        EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
                        eventViewHolderModel.setId(eventList.getId());
                        eventViewHolderModel.setName(eventList.getName());
                        eventViewHolderModel.setDescription(eventList.getDescription());
                        eventViewHolderModel.setEventLogo(eventList.getEventLogo());
                        eventViewHolderModel.setStatus(eventList.getStatus());
                        eventViewHolderModel.setGstValue(eventList.getGstValue());
                        eventViewHolderModel.setEventSlot(eventList.getEventSlot());
                        eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_EVENT);
                        eventViewHolderModel.setChecked(isEventPriceAvailable);
                        eventViewHolderModel.setEventCategories(eventList.getEventCategories());

                        if(eventList.getSelectedEventSubcategories() != null
                                && eventList.getSelectedEventSubcategories().size() > 0) {
                            for(EventSubcategory eventSubcategory : eventList.getSelectedEventSubcategories()) {
                                eventSubcategory.setSelected(true);
                            }
                        }

                        eventViewHolderModel.setSelectedEventSubcategories(eventList.getSelectedEventSubcategories());

                        if(eventViewHolderModel.getEventCategories() != null
                                && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                                && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
                            for(int z = 0; z < eventViewHolderModel.getEventCategories().getEventSubcategories().size(); z++) {
                                EventSubcategory eventSubcategory
                                        = eventViewHolderModel.getEventCategories().getEventSubcategories().get(z);
                                eventSubcategory.setEventSubcategoryId(eventSubcategory.getId());

                                EventSubcategoryDetail eventSubcategoryDetail = new EventSubcategoryDetail();
                                eventSubcategoryDetail.setId(eventSubcategory.getId());
                                eventSubcategoryDetail.setName(eventSubcategory.getName());

                                eventSubcategory.setEventSubcategoryDetail(eventSubcategoryDetail);
                            }
                        }

                        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                            for(EventSubcategory eventSubcategory : eventViewHolderModel.getSelectedEventSubcategories()) {
                                eventSubcategory.setId(eventSubcategory.getEventSubcategoryId());
                                eventSubcategory.setSelected(true);
                            }
                        }

                        if(eventViewHolderModel.getEventCategories() != null
                                && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                                && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {

                            if(eventViewHolderModel.getSelectedEventSubcategories() != null
                                    && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {

                                for(EventSubcategory eventSubcategoryGlobal :
                                        eventViewHolderModel.getEventCategories().getEventSubcategories()) {
                                    for(EventSubcategory eventSubcategorySelected :
                                            eventViewHolderModel.getSelectedEventSubcategories()) {
                                        if(eventSubcategoryGlobal.getEventSubcategoryId()
                                                == eventSubcategorySelected.getEventSubcategoryId()) {
                                            eventSubcategoryGlobal.setSelected(true);
                                            eventSubcategorySelected.setSelected(true);
                                        }
                                    }
                                }

                            }

                        }

                        removeDuplicateEventSubcategories(eventViewHolderModel);

                        mGlobalEventList.add(eventViewHolderModel);
                    }
                }
            }
        }
    }

    private void removeDuplicateEventSubcategories(EventViewHolderModel eventViewHolderModel) {
        LinkedHashSet<EventSubcategory> duplicateRemovalHashSet = new LinkedHashSet<>();
        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
            duplicateRemovalHashSet.addAll(eventViewHolderModel.getSelectedEventSubcategories());
            if(duplicateRemovalHashSet.size() > 0) {
                Log.d("speaker_event_log", " duplicate removal set size : "
                        + duplicateRemovalHashSet.size()
                        + " for " + eventViewHolderModel.getName());
                eventViewHolderModel.getSelectedEventSubcategories().clear();
                eventViewHolderModel.getSelectedEventSubcategories().addAll(duplicateRemovalHashSet);
            }
        }
    }

    private boolean isEventPriceAvailable(EventList eventList) {
        boolean isChecked = false;
        int slotCount = 0;
        if(eventList.getEventSlot() != null && eventList.getEventSlot().size() > 0) {
            for(int j = 0; j < eventList.getEventSlot().size(); j++) {
                GuestSpeakerEvents guestSpeakerEvents = eventList.getEventSlot().get(j).getGuestSpeakerEvents();
                if(guestSpeakerEvents == null) {
                    isChecked = false;
                    //break;
                } else {
                    isChecked = true;
                    slotCount++;
                }
            }

            if(slotCount > 0) {
                isChecked = true;
            } else {
                isChecked = false;
            }
        }
        return isChecked;
    }

    private void setSpecialityView() {
        mBinding.llEsp.removeAllViews();
        if (!StringUtils.isNullOrEmpty(mUserProfileModel.getSpecialities())) {
            String[] items = mUserProfileModel.getSpecialities().split("\\s*,\\s*");
            drawView(items, mBinding.llEsp);
        }
    }

    private void setGender() {
        mBinding.tvGender.setText(mUserProfileModel.getGender() == Constants.GENDER_TYPE.MALE ?
                getResources().getString(R.string.male) : getResources().getString(R.string.female));
    }

    private void setProfilePic() {
        if(mUserProfileModel == null) {
            return;
        }
        if(StringUtils.isNullOrEmpty(mUserProfileModel.getProfileImage())) {
            return;
        }

        Picasso.with(SpeakerViewProfileActivity.this)
                .load(mUserProfileModel.getProfileImage())
                .placeholder(R.drawable.ic_user_circle)
                .error(R.drawable.ic_user_circle)
                .into(mBinding.ivProfilePic,
                new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        mBinding.ivBgHeader.setImageDrawable(mBinding.ivProfilePic.getDrawable());
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    private void setSportIcon() {
        if(mUserProfileModel == null) {
            return;
        }
        if(mUserProfileModel.getSportDetail() == null) {
            return;
        }
        if(StringUtils.isNullOrEmpty(mUserProfileModel.getSportDetail().getIcon())) {
            return;
        }

        Picasso.with(SpeakerViewProfileActivity.this)
                .load(mUserProfileModel.getSportDetail().getIcon())
                .placeholder(R.drawable.cricket_icon)
                .error(R.drawable.cricket_icon)
                .into(mBinding.ivUserSport,
                        new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
    }

    private void setTravelType() {
        if(mUserProfileModel == null) {
            return;
        }
        if(mUserProfileModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.YES) {
            mBinding.tvTravelInterState.setText(getResources().getString(R.string.yes));
            mBinding.tvInterStateTravelType.setText(getResources().getString(R.string.yes_small));
        } else if(mUserProfileModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.NO) {
            mBinding.tvTravelInterState.setText(getResources().getString(R.string.no));
            mBinding.tvInterStateTravelType.setText(getResources().getString(R.string.no_small));
        } else if(mUserProfileModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.UNSELECTED) {
            mBinding.tvTravelInterState.setText(getResources().getString(R.string.no));
            mBinding.tvInterStateTravelType.setText(getResources().getString(R.string.no_small));
        }
    }

    @Override
    public String getActivityName() {
        return "SpeakerViewProfile";
    }

    @Override
    public void onPageChanged(int position) {
        if(mUserProfileModel.getMediaUrl().size() > 0) {
            mBinding.tvPagesMedia.setText((position+1)+"/"+mUserProfileModel.getMediaUrl().size());
        }
    }

    @Override
    public void onMediaClicked(int position) {
        if(mUserProfileModel.getMediaUrl().get(position).getType() == 7) {
            Bundle bundlePlay = new Bundle();
            bundlePlay.putString(Constants.BundleKey.VIDEO_URL, mUserProfileModel.getMediaUrl().get(position).getImageUrl());
            Navigator.getInstance().navigateToActivityWithData(this, PlayerActivity.class, bundlePlay);
        } else {
            Intent intent = new Intent(this, ImagePreviewActivity.class);
            intent.putExtra(ImagePreviewActivity.EXTRA_IMAGE_URL, mUserProfileModel.getMediaUrl().get(position).getImageUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == mBinding.tvEdit) {
            if(Utils.isNetworkAvailable()) {
                Navigator.getInstance().navigateToActivity(this, EditSpeakerProfileActivity.class);
            } else {
                showSnackbarFromTop(SpeakerViewProfileActivity.this, getResources().getString(R.string.no_internet));
            }
        }
    }

    @Override
    public void onListItemClicked(int position) {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(position);
            if(eventViewHolderModel != null) {
                Alert.showSpeakerEventInfoAlertDialog(this, eventViewHolderModel,
                        new Alert.OnCancelClickListener() {
                            @Override
                            public void onCancel() {

                            }
                });
            }
        }
    }

    private void getGuestSpeakerProfile() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.GUEST_SPEAKER);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(SpeakerViewProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mUserProfileModel = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mUserProfileModel);
                        if(mUserProfileModel != null) {
                            bindDataSet();
                            initViews();
                            setListeners();
                            if(Utils.isNetworkAvailable()) {
                                getEventList();
                            } else {
                                mBinding.rvEventList.setVisibility(View.GONE);
                                mBinding.tvNoEvents.setVisibility(View.VISIBLE);
                                showSnackbarFromTop(SpeakerViewProfileActivity.this, getResources().getString(R.string.no_internet));
                            }
                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SpeakerViewProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerViewProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
