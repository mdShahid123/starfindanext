package com.sportsstars.ui.speaker.events;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.appster.chatlib.constants.AppConstants;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.chat.utils.DateTimeUtils;
import com.sportsstars.databinding.ActivitySpeakerEventDetailsBinding;
import com.sportsstars.model.LearnerProfile;
import com.sportsstars.model.ProfileLearner;
import com.sportsstars.model.UserModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.speaker.CancelBookingStatusRequest;
import com.sportsstars.network.request.auth.speaker.UpdateBookingStatusRequest;
import com.sportsstars.network.response.speaker.EventDetails;
import com.sportsstars.network.response.speaker.EventDetailsResponse;
import com.sportsstars.network.response.speaker.GuestSpeakerSessionRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.learner.events.LearnerEventDetailsActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class SpeakerEventDetailsActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySpeakerEventDetailsBinding mBinding;
    private int mSessionId;
    private EventDetails mEventDetails;
    private int speakerStatus = -1;
    private int isSpeakerPaid = -1;
    private static final String TIME_FORMAT = "REMAINING TIME - %02dH:%02dM:%02dS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_speaker_event_details);

        getIntentData();
        initViews();
    }

    private void getIntentData() {
        if(getIntent().hasExtra(Constants.BundleKey.SESSION_ID_KEY)) {
            mSessionId = getIntent().getIntExtra(Constants.BundleKey.SESSION_ID_KEY, 0);
        }
        //showToast("session id : " + mSessionId);
    }

    private void initViews() {
        mBinding.buttonAcceptBooking.setOnClickListener(this);
        mBinding.buttonDeclineBooking.setOnClickListener(this);
        mBinding.relUserInfo.setOnClickListener(this);
        mBinding.ivChat.setOnClickListener(this);
        mBinding.buttonCancelBooking.setVisibility(View.GONE);
        mBinding.buttonCancelBooking.setOnClickListener(this);

        if(Utils.isNetworkAvailable()) {
            if(mSessionId == 0) {
                return;
            }
            getEventDetails(mSessionId);
        } else {
            showSnackbarFromTop(SpeakerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
        }



    }

    private void getEventDetails(int sessionId) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.getEventDetails(sessionId).enqueue(new BaseCallback<EventDetailsResponse>(SpeakerEventDetailsActivity.this) {
            @Override
            public void onSuccess(EventDetailsResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        try {
                            mEventDetails = response.getResult();
//                            if(response.getMessage() != null
//                                    && !TextUtils.isEmpty(response.getMessage())) {
//                                showSnackbarFromTop(SpeakerEventDetailsActivity.this, response.getMessage());
//                            }
                            updateUI();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SpeakerEventDetailsActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<EventDetailsResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void updateUI() {
        if(mEventDetails == null) {
            return;
        }
        mBinding.llDeclineAccept.setVisibility(View.VISIBLE);

        if(mEventDetails.getEventUserInfo() != null) {
            if (mEventDetails.getEventUserInfo().getProfileImage() != null
                    && !TextUtils.isEmpty(mEventDetails.getEventUserInfo().getProfileImage())) {
                Picasso.with(this)
                        .load(mEventDetails.getEventUserInfo().getProfileImage())
                        .placeholder(R.drawable.ic_user_circle)
                        .error(R.drawable.ic_user_circle)
                        .into(mBinding.imageProfile);

                Picasso.with(this)
                        .load(mEventDetails.getEventUserInfo().getProfileImage())
                        .placeholder(R.drawable.ic_user_circle)
                        .error(R.drawable.ic_user_circle)
                        .into(mBinding.ivBackgroundImage);

                Picasso.with(this)
                        .load(mEventDetails.getEventUserInfo().getProfileImage())
                        .placeholder(R.drawable.ic_user_circle)
                        .error(R.drawable.ic_user_circle)
                        .into(mBinding.ivImageUser);
            }

            if(mEventDetails.getEventUserInfo().getName() != null
                    && !TextUtils.isEmpty(mEventDetails.getEventUserInfo().getName())) {
                mBinding.tvName.setText(mEventDetails.getEventUserInfo().getName());
                mBinding.tvLearnerName.setText(mEventDetails.getEventUserInfo().getName());
            }
        }

        String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
        if(upcomingText.startsWith("AWAITING")) {
            upcomingText = upcomingText.replace("\n", " ");
        }
        mBinding.tvUpcoming.setText(upcomingText);
        mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(this, mEventDetails.getStatus()));

        if(mEventDetails.getAddress() != null && !TextUtils.isEmpty(mEventDetails.getAddress())) {
            mBinding.tvLocation.setText(mEventDetails.getAddress());
        }

        String utcDateTime = mEventDetails.getSessionStartTime();
        String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
        Log.d("mylog", "SpeakerEventDetails - utcDateTime : " + utcDateTime
                + " :: localDateTime : " + localDateTime);
        mBinding.time.setText(Utils.parseTimeForDDMM(localDateTime));
        mBinding.date.setText(Utils.parseTimeForDDMMYY(localDateTime));

        if (mEventDetails.getEventLogo() != null
                && !TextUtils.isEmpty(mEventDetails.getEventLogo())) {
            Picasso.with(this)
                    .load(mEventDetails.getEventLogo())
                    .placeholder(R.drawable.ic_birthday)
                    .error(R.drawable.ic_birthday)
                    .into(mBinding.ivEventLogo);
        }

        if(mEventDetails.getEventName() != null && !TextUtils.isEmpty(mEventDetails.getEventName())) {
            mBinding.tvEventName.setText(mEventDetails.getEventName());
        }

        if(mEventDetails.getNoOfAttendees() != null && !TextUtils.isEmpty(mEventDetails.getNoOfAttendees())) {
            mBinding.tvNoOfAttendees.setText(Utils.getNoOfAttendeesText(mEventDetails.getNoOfAttendees()));
        }

        if(mEventDetails.getDuration() != 0) {
            mBinding.tvDuration.setText(getResources().getString(R.string.duration_with_mins, mEventDetails.getDuration()));
        }

        int priceInt = (int) mEventDetails.getTotalBudget();
        mBinding.price.setText("$"+priceInt);

        showHideButtonsOnSpeakerStatus();

        if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.CANCELLED
                || mEventDetails.getStatus() == Utils.STATUS_BOOKING.COMPLETED
                || mEventDetails.getStatus() == Utils.STATUS_BOOKING.ONGOING
                || mEventDetails.getStatus() == Utils.STATUS_BOOKING.UPCOMING) {
            mBinding.llDeclineAccept.setVisibility(View.GONE);
            mBinding.tvRemainingTime.setVisibility(View.GONE);
        }

        if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.PAYMENT_PENDING) {
            mBinding.tvRemainingTime.setVisibility(View.GONE);
            mBinding.buttonCancelBooking.setVisibility(View.GONE);
        }

        if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.UPCOMING) {
            mBinding.llDeclineAccept.setVisibility(View.GONE);
            if(isSpeakerPaid == 1) {
                mBinding.buttonCancelBooking.setVisibility(View.VISIBLE);
            } else {
                mBinding.buttonCancelBooking.setVisibility(View.GONE);
            }
        }

        if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
            mBinding.tvRemainingTime.setVisibility(View.GONE);
            startCountDownTimer();
        }

        showHideChatIcon();

    }

    private void showHideChatIcon() {
        int eventBookingStatus = mEventDetails.getStatus();
        if(eventBookingStatus == Utils.STATUS_BOOKING.CANCELLED) {
            mBinding.ivChat.setVisibility(View.GONE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.COMPLETED) {
            mBinding.ivChat.setVisibility(View.GONE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.ONGOING) {
            mBinding.ivChat.setVisibility(View.VISIBLE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.UPCOMING) {
            mBinding.ivChat.setVisibility(View.VISIBLE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.PAYMENT_PENDING) {
            mBinding.ivChat.setVisibility(View.VISIBLE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
            mBinding.ivChat.setVisibility(View.GONE);
        }

        if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
            mBinding.ivChat.setVisibility(View.GONE);
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
            if(eventBookingStatus == Utils.STATUS_BOOKING.CANCELLED) {
                mBinding.ivChat.setVisibility(View.GONE);
            } else if(eventBookingStatus == Utils.STATUS_BOOKING.COMPLETED) {
                mBinding.ivChat.setVisibility(View.GONE);
            } else if(eventBookingStatus == Utils.STATUS_BOOKING.ONGOING) {
                mBinding.ivChat.setVisibility(View.VISIBLE);
                if(isSpeakerPaid == 1) {
                    mBinding.ivChat.setVisibility(View.VISIBLE);
                } else {
                    mBinding.ivChat.setVisibility(View.GONE);
                }
            } else if(eventBookingStatus == Utils.STATUS_BOOKING.UPCOMING) {
                mBinding.ivChat.setVisibility(View.VISIBLE);
                if(isSpeakerPaid == 1) {
                    mBinding.ivChat.setVisibility(View.VISIBLE);
                } else {
                    mBinding.ivChat.setVisibility(View.GONE);
                }
            } else if(eventBookingStatus == Utils.STATUS_BOOKING.PAYMENT_PENDING) {
                mBinding.ivChat.setVisibility(View.VISIBLE);
                if(isSpeakerPaid == 1) {
                    mBinding.ivChat.setVisibility(View.VISIBLE);
                } else {
                    mBinding.ivChat.setVisibility(View.GONE);
                }
            } else if(eventBookingStatus == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
                mBinding.ivChat.setVisibility(View.GONE);
            }
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
            mBinding.ivChat.setVisibility(View.GONE);
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.NO_RESPONSE) {
            mBinding.ivChat.setVisibility(View.GONE);
        }
    }

    private void showHideButtonsOnSpeakerStatus() {
        UserModel userModel = PreferenceUtil.getUserModel();
        if(userModel == null) {
            return;
        }
        if(userModel.getUserId() == 0) {
            return;
        }
        int currentUserId = userModel.getUserId();
        if(mEventDetails != null && mEventDetails.getGuestSpeakerSessionRequest() != null
                && mEventDetails.getGuestSpeakerSessionRequest().size() > 0) {
            for(GuestSpeakerSessionRequest guestSpeakerSessionRequest : mEventDetails.getGuestSpeakerSessionRequest()) {
                if(guestSpeakerSessionRequest.getSpeakerId() == currentUserId) {
                    speakerStatus = guestSpeakerSessionRequest.getStatus();
                    isSpeakerPaid = guestSpeakerSessionRequest.getIsPaid();
                    if(mEventDetails.getStatus() == Utils.STATUS_BOOKING.PAYMENT_PENDING
                            || mEventDetails.getStatus() == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
                        setEventStatusAgainstSpeakerStatus();
                    }
//                    else {
//                        setEventStatusForCancelledSpeaker();
//                    }
                    if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
                        mBinding.llDeclineAccept.setVisibility(View.VISIBLE);
                        mBinding.buttonDeclineBooking.setVisibility(View.VISIBLE);
                        mBinding.buttonAcceptBooking.setVisibility(View.VISIBLE);
                    } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
                        mBinding.llDeclineAccept.setVisibility(View.VISIBLE);
                        mBinding.buttonDeclineBooking.setVisibility(View.VISIBLE);
                        mBinding.buttonAcceptBooking.setVisibility(View.GONE);
                    } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
                        mBinding.llDeclineAccept.setVisibility(View.GONE);
                        mBinding.tvRemainingTime.setVisibility(View.GONE);
                    } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.NO_RESPONSE) {
                        mBinding.llDeclineAccept.setVisibility(View.GONE);
                        mBinding.tvRemainingTime.setVisibility(View.GONE);
                    }
                    break;
                }
            }
        }
    }

    private void setEventStatusAgainstSpeakerStatus() {
        if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
            mEventDetails.setStatus(Utils.STATUS_BOOKING.AWAITING_RESPONSE);
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
            mEventDetails.setStatus(Utils.STATUS_BOOKING.PAYMENT_PENDING);
            mBinding.tvRemainingTime.setVisibility(View.GONE);
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
            mEventDetails.setStatus(Utils.STATUS_BOOKING.CANCELLED);
            mBinding.llDeclineAccept.setVisibility(View.GONE);
            mBinding.tvRemainingTime.setVisibility(View.GONE);
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.NO_RESPONSE) {
            mEventDetails.setStatus(Utils.STATUS_BOOKING.CANCELLED);
            mBinding.llDeclineAccept.setVisibility(View.GONE);
            mBinding.tvRemainingTime.setVisibility(View.GONE);
        }
        String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
        if(upcomingText.startsWith("AWAITING")) {
            upcomingText = upcomingText.replace("\n", " ");
        }
        mBinding.tvUpcoming.setText(upcomingText);
        mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(this, mEventDetails.getStatus()));
    }

    private void setEventStatusForCancelledSpeaker() {
        if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
            mEventDetails.setStatus(Utils.STATUS_BOOKING.CANCELLED);
            mBinding.llDeclineAccept.setVisibility(View.GONE);
            mBinding.tvRemainingTime.setVisibility(View.GONE);

            String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
            if(upcomingText.startsWith("AWAITING")) {
                upcomingText = upcomingText.replace("\n", " ");
            }
            mBinding.tvUpcoming.setText(upcomingText);
            mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(this, mEventDetails.getStatus()));
        }
    }

    private void startCountDownTimer() {
        try {
            String createdAt = "";
            String currentTime = "";
            long millisecondsToFinish;
            if(mEventDetails != null && !TextUtils.isEmpty(mEventDetails.getCreatedAt())
                    && !TextUtils.isEmpty(mEventDetails.getCurrentTime())) {
                createdAt = mEventDetails.getCreatedAt();
                currentTime = mEventDetails.getCurrentTime();

                Date dateCreated = DateTimeUtils.getDateFromString(createdAt, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
                Date dateCurrent = DateTimeUtils.getDateFromString(currentTime, DateFormatUtils.SERVER_DATE_FORMAT_TIME);

                Calendar calendarCreated = Calendar.getInstance();
                calendarCreated.setTime(dateCreated);
                //calendarCreated.add(Calendar.HOUR, Constants.HOURS_TO_ADD_IN_TIMER);
                calendarCreated.add(Calendar.MINUTE, mEventDetails.getTimer());

                Calendar calendarCurrent = Calendar.getInstance();
                calendarCurrent.setTime(dateCurrent);

                // millis for 2 days i.e 48 hours - 172800000

                millisecondsToFinish = calendarCreated.getTimeInMillis() - calendarCurrent.getTimeInMillis();
                //millisecondsToFinish = 1000*60* mEventDetails.getTimer();
                if (millisecondsToFinish > Constants.ONE_SECOND_INTERVAL) {
                    new CountDownTimer(millisecondsToFinish, Constants.ONE_SECOND_INTERVAL) { // adjust the milli seconds here

                        @Override
                        public void onTick(long millisUntilFinished) {

                            mBinding.tvRemainingTime.setText(String.format(TIME_FORMAT,
                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

//                        mBinding.tvRemainingTime.setText(formatMilliSecondsToTime(millisUntilFinished));
                        }

                        @Override
                        public void onFinish() {
                            mBinding.tvRemainingTime.setText("REMAINING TIME - FINISHED");
                            mBinding.tvRemainingTime.setVisibility(View.GONE);
                            mBinding.llDeclineAccept.setVisibility(View.GONE);

//                            mEventDetails.setStatus(Utils.STATUS_BOOKING.PAYMENT_PENDING);
//                            String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
//                            if(upcomingText.startsWith("AWAITING")) {
//                                upcomingText = upcomingText.replace("\n", " ");
//                            }
//                            mBinding.tvUpcoming.setText(upcomingText);
//                            mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(
//                                    SpeakerEventDetailsActivity.this, mEventDetails.getStatus()));

                            if(Utils.isNetworkAvailable()) {
                                if(mSessionId == 0) {
                                    return;
                                }
                                getEventDetails(mSessionId);
                            }
                        }
                    }.start();
                } else {
                    mBinding.tvRemainingTime.setText("REMAINING TIME - FINISHED");
                    mBinding.tvRemainingTime.setVisibility(View.GONE);
                    mBinding.llDeclineAccept.setVisibility(View.GONE);

//                    mEventDetails.setStatus(Utils.STATUS_BOOKING.PAYMENT_PENDING);
//                    String upcomingText = Utils.getCurrentAndUpcomingText(mEventDetails.getStatus());
//                    if(upcomingText.startsWith("AWAITING")) {
//                        upcomingText = upcomingText.replace("\n", " ");
//                    }
//                    mBinding.tvUpcoming.setText(upcomingText);
//                    mBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(
//                            SpeakerEventDetailsActivity.this, mEventDetails.getStatus()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button_accept_booking:
                if(Utils.isNetworkAvailable()) {
                     updateBookingStatus(Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED);
                } else {
                    showSnackbarFromTop(SpeakerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.button_decline_booking:
                if(Utils.isNetworkAvailable()) {
                    updateBookingStatus(Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED);
                } else {
                    showSnackbarFromTop(SpeakerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.rel_user_info:
                if(Utils.isNetworkAvailable()) {
                    getLearnerProfile(true);
                } else {
                    showSnackbarFromTop(SpeakerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.ivChat:
                if(Utils.isNetworkAvailable()) {
                    getLearnerProfile(false);
                } else {
                    showSnackbarFromTop(SpeakerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
            case R.id.button_cancel_booking:
                if(Utils.isNetworkAvailable()) {
                    if(mEventDetails != null
                            && mEventDetails.getStatus() == Utils.STATUS_BOOKING.UPCOMING) {
                        showBookingCancelConfirmationDialog();
                    }
                } else {
                    showSnackbarFromTop(SpeakerEventDetailsActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private void showBookingCancelConfirmationDialog() {
        Alert.showOkCancelDialog(SpeakerEventDetailsActivity.this,
                getResources().getString(R.string.cancel_booking_dialog_title),
                getResources().getString(R.string.cancel_booking_learner_speaker_dialog_message),
                getResources().getString(R.string.no),
                getResources().getString(R.string.yes),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        if(Utils.isNetworkAvailable()) {
                            //cancelBookingStatus();
                            updateBookingStatus(Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED);
                        } else {
                            showSnackbarFromTop(SpeakerEventDetailsActivity.this,
                                    getResources().getString(R.string.no_internet));
                        }
                    }
                },
                new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {

                    }
                }
        );
    }

    private void cancelBookingStatus() {

        if(mSessionId == 0) {
            showSnackbarFromTop(SpeakerEventDetailsActivity.this,
                    getResources().getString(R.string.event_is_invalid));
            return;
        }

        CancelBookingStatusRequest cancelBookingStatusRequest = new CancelBookingStatusRequest();
        cancelBookingStatusRequest.setSessionId(mSessionId);

        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.cancelBookingStatus(cancelBookingStatusRequest).enqueue(new BaseCallback<BaseResponse>(SpeakerEventDetailsActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                        finish();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                    //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                }
                finish();
            }
        });
    }

    private void updateBookingStatus(int speakerAvailabilityStatus) {

        if(mSessionId == 0) {
            return;
        }

        UserModel userModel = PreferenceUtil.getUserModel();
        if(userModel == null) {
            return;
        } else if(userModel.getUserId() == 0) {
            return;
        }

        if(speakerAvailabilityStatus == 0) {
            return;
        }

        UpdateBookingStatusRequest updateBookingStatusRequest = new UpdateBookingStatusRequest();
        updateBookingStatusRequest.setSessionId(mSessionId);
        updateBookingStatusRequest.setSpeakerId(userModel.getUserId());
        updateBookingStatusRequest.setStatus(speakerAvailabilityStatus);

        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateBookingStatus(updateBookingStatusRequest).enqueue(new BaseCallback<BaseResponse>(SpeakerEventDetailsActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                        finish();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(!TextUtils.isEmpty(baseResponse.getMessage())) {
                    showToast(baseResponse.getMessage());
                    //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                }
                finish();
            }
        });
    }

    private void getLearnerProfile(boolean isViewingProfile) {
        if (Utils.isNetworkAvailable()) {
            if (mEventDetails != null
                    && mEventDetails.getEventUserInfo() != null
                    && mEventDetails.getEventUserInfo().getId() != 0) {
                showProgressBar();
                AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
                authWebServices.getLearnerProfile(mEventDetails.getEventUserInfo().getId()).enqueue(new BaseCallback<LearnerProfile>(this) {
                    @Override
                    public void onSuccess(LearnerProfile response) {
                        if(response != null) {
                            if(response.getStatus() == 1) {
                                if(isViewingProfile) {
                                    ProfileLearner profileLearner = response.getResult();
                                    if (profileLearner != null) {
                                        profileLearner.setJid(mEventDetails.getEventUserInfo().getJid());
                                        profileLearner.setJkey(mEventDetails.getEventUserInfo().getJkey());
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, true);
                                        //bundle.putBoolean(Constants.BundleKey.IS_COACH_SESSION_COMPLETED, Utils.isCoachSessionCompleted(_currentBookingList));
                                        bundle.putParcelable(Constants.BundleKey.DATA, profileLearner);
                                        //bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, buildReportUserRequest(_currentBookingList));
                                        if(mEventDetails != null) {
                                            bundle.putInt(Constants.BundleKey.EVENT_BOOKING_STATUS, mEventDetails.getStatus());
                                            bundle.putInt(Constants.BundleKey.SPEAKER_STATUS, speakerStatus);
                                            bundle.putInt(Constants.BundleKey.IS_SPEAKER_PAID, isSpeakerPaid);
                                        }
                                        bundle.putBoolean(Constants.BundleKey.IS_FROM_EVENT_DETAILS, true);
                                        Navigator.getInstance().navigateToActivityWithData(
                                                SpeakerEventDetailsActivity.this,
                                                com.sportsstars.ui.learner.ViewProfileActivity.class,
                                                bundle);
                                    }
                                } else {
                                    startChat();
                                }
                            } else {
                                try {
                                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                        showSnackbarFromTop(SpeakerEventDetailsActivity.this, response.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(Call<LearnerProfile> call, BaseResponse baseResponse) {
                        try {
                            hideProgressBar();
                            if (baseResponse != null && baseResponse.getMessage() != null
                                    && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                //Log.d("mylog", baseResponse.getMessage());
                                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    private void startChat() {
        if(mEventDetails != null && mEventDetails.getEventUserInfo() != null
                && mEventDetails.getEventUserInfo().getJid() != null
                && !TextUtils.isEmpty(mEventDetails.getEventUserInfo().getJid())) {
            String userName = mEventDetails.getEventUserInfo().getJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, mEventDetails.getEventUserInfo().getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, mEventDetails.getEventUserInfo().getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, mEventDetails.getEventUserInfo().getJid());//.replace("246","260"));
//                intent.putExtra(AppConstants.KEY_TO_USER, _currentBookingList.getLearnerJid());
                startActivity(intent);
            } else {
                showToast("Username of this profile not found to initiate chat.");
                //com.appster.chatlib.utils.Utils.showToast(this, "enter friend username");
            }
        }
    }

    @Override
    public String getActivityName() {
        return "SpeakerEventDetails";
    }

}
