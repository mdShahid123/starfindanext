package com.sportsstars.ui.speaker.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sportsstars.R;
import com.sportsstars.databinding.ItemCoachResultBinding;
import com.sportsstars.databinding.ItemSpeakerResultBinding;
import com.sportsstars.interfaces.CoachBySportItemClick;
import com.sportsstars.interfaces.SpeakerBySportItemClick;
import com.sportsstars.interfaces.SpeakerSelectedListener;
import com.sportsstars.model.Coach;
import com.sportsstars.model.Speaker;
import com.sportsstars.ui.search.CoachListBySportAdapter;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;

import java.util.ArrayList;

public class SpeakerListBySportAdapter extends RecyclerView.Adapter<SpeakerListBySportAdapter.SpeakerListViewHolder> {

    private static final String TAG = "SpeakerListBySport";

    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ArrayList<Speaker> mSpeakerList;
    private SpeakerBySportItemClick mClickListener;
    private ImageLoader imageLoader;
    private DisplayImageOptions op, opSport;
    private SpeakerSelectedListener mSpeakerSelectedListener;

    public SpeakerListBySportAdapter(Activity mActivity, ArrayList<Speaker> speakerList,
                                     SpeakerBySportItemClick speakerBySportItemClick,
                                     SpeakerSelectedListener speakerSelectedListener) {
        this.mActivity = mActivity;
        this.mSpeakerList = speakerList;
        this.mClickListener = speakerBySportItemClick;
        mInflator = LayoutInflater.from(mActivity);

        op = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_user_circle)
                .showImageForEmptyUri(R.drawable.ic_user_circle)
                .showImageOnFail(R.drawable.ic_user_circle)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new RoundedBitmapDisplayer(20))
                .build();

        opSport = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.cricket_icon)
                .showImageForEmptyUri(R.drawable.cricket_icon)
                .showImageOnFail(R.drawable.cricket_icon)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new RoundedBitmapDisplayer(20))
                .build();
        imageLoader = ImageLoader.getInstance();

        mSpeakerSelectedListener = speakerSelectedListener;
    }

    @Override
    public SpeakerListBySportAdapter.SpeakerListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSpeakerResultBinding searchListBinding = DataBindingUtil.inflate(mInflator, R.layout.item_speaker_result, parent, false);
        return new SpeakerListBySportAdapter.SpeakerListViewHolder(searchListBinding);
    }

    @Override
    public void onBindViewHolder(final SpeakerListBySportAdapter.SpeakerListViewHolder holder, int position) {
        if (mSpeakerList != null && mSpeakerList.size() > 0) {
            final Speaker speaker = mSpeakerList.get(position);
            holder.getBinding().tvSpeakerName.setText(speaker.getName());
            holder.binding.ivSportIcon.setImageResource(R.drawable.cricket_icon);
            int price = (int) speaker.getSlotPrice();
            holder.getBinding().tvSpeakerCharges.setText("$"+price);
            holder.binding.tvSpeakerInfo.setText(speaker.getDescription());

            Log.d("image_log", "Speaker list - onBindViewHolder - "
                    + " position : " + position
                    + " coach id : " + speaker.getUserId()
                    + " coach name  : " + speaker.getName()
                    + " coach image url : " + speaker.getProfileImage()
            );

            holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);

            imageLoader.displayImage(speaker.getProfileImage(), holder.getBinding().ivProfilePic, op, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String url, View view) {
                    Log.d("image_log", "Speaker list - onLoadingStarted - "
                            + " position : " + position
                            + " coach id : " + speaker.getId()
                            + " coach name  : " + speaker.getName()
                            + " coach image url : " + url
                    );
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    Log.d("image_log", "Speaker list - onLoadingComplete - "
                            + " position : " + position
                            + " coach id : " + speaker.getId()
                            + " coach name  : " + speaker.getName()
                            + " coach image url : " + imageUri
                    );
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    super.onLoadingFailed(imageUri, view, failReason);
                    Log.d("image_log", "Speaker list - onLoadingFailed - "
                            + " position : " + position
                            + " coach id : " + speaker.getId()
                            + " coach name  : " + speaker.getName()
                            + " coach image url : " + imageUri
                            + " failReason : " + failReason.getCause().toString()
                    );
                    holder.getBinding().ivProfilePic.setImageResource(R.drawable.ic_user_circle);
                }
            });

            imageLoader.displayImage(speaker.getSportIcon(), holder.getBinding().ivSportIcon, opSport,
                    new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String url, View view) {
                            Log.d("image_log", "Speaker list - onLoadingStarted - "
                                + " position : " + position
                                + " coach id : " + speaker.getId()
                                + " coach name  : " + speaker.getName()
                                + " coach image url : " + url
                            );
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            Log.d("image_log", "Speaker list - onLoadingComplete - "
                                + " position : " + position
                                + " coach id : " + speaker.getId()
                                + " coach name  : " + speaker.getName()
                                + " coach image url : " + imageUri
                            );
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            Log.d("image_log", "Speaker list - onLoadingFailed - "
                                + " position : " + position
                                + " coach id : " + speaker.getId()
                                + " coach name  : " + speaker.getName()
                                + " coach image url : " + imageUri
                                + " failReason : " + failReason.getCause().toString()
                            );
                            holder.getBinding().ivSportIcon.setImageResource(R.drawable.cricket_icon);
                        }
            });

            if(speaker.isSelected()) {
                holder.getBinding().ivCheckbox.setImageResource(R.drawable.ic_checked);
            } else {
                holder.getBinding().ivCheckbox.setImageResource(R.drawable.ic_unchecked_circle);
            }

            holder.getBinding().ivCheckbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mSpeakerSelectedListener != null) {
                        if(speaker.isSelected()) {
                            mSpeakerSelectedListener.onSpeakerSelected(position, false);
                        } else {
                            mSpeakerSelectedListener.onSpeakerSelected(position, true);
                        }
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return mSpeakerList != null ? mSpeakerList.size() : 0;
    }

    public void clearItems() {
        if (mSpeakerList != null && mSpeakerList.size() > 0) {
            mSpeakerList.clear();
            notifyDataSetChanged();
        }
    }

    class SpeakerListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemSpeakerResultBinding binding;

        private SpeakerListViewHolder(ItemSpeakerResultBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        public ItemSpeakerResultBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
            LogUtils.LOGD(TAG, "clicked");
            mClickListener.onItemClick(binding, mSpeakerList.get(getAdapterPosition()), getAdapterPosition());
        }

    }

}
