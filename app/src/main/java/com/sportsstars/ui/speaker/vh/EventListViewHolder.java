package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import com.sportsstars.databinding.ItemEventsListViewHolderBinding;
import com.sportsstars.interfaces.EventInfoListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.ui.speaker.adapter.EventSlotAdapter;
import com.sportsstars.util.Utils;

public class EventListViewHolder extends RecyclerView.ViewHolder {

    private ItemEventsListViewHolderBinding mBinding;
    private Activity mActivity;
    private EventInfoListener mEventInfoListener;

    public EventListViewHolder(ItemEventsListViewHolderBinding binding, Activity activity, EventInfoListener eventInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mEventInfoListener = eventInfoListener;
    }

    public void bindData(EventViewHolderModel eventViewHolderModel, int position) {
        if(eventViewHolderModel != null) {
            if(position == 0) {
                mBinding.tvEventsHeader.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvEventsHeader.setVisibility(View.GONE);
            }

            mBinding.tvEventName.setText(eventViewHolderModel.getName());

            EventSlotAdapter eventSlotAdapter = new EventSlotAdapter(mActivity, eventViewHolderModel,
                    position, eventViewHolderModel.getEventSlot(), mEventInfoListener);
            mBinding.rvEventSlots.setLayoutManager(new LinearLayoutManager(mActivity));
            mBinding.rvEventSlots.addItemDecoration(new DividerItemDecoration(mActivity, RecyclerView.VERTICAL));
            mBinding.rvEventSlots.setAdapter(eventSlotAdapter);

            mBinding.switchEvent.setOnCheckedChangeListener(null);

            if(eventViewHolderModel.isChecked()) {
                mBinding.switchEvent.setChecked(true);
                mBinding.rvEventSlots.setVisibility(View.VISIBLE);
                mBinding.tvEventCategories.setVisibility(View.VISIBLE);
                mBinding.etEventSubcategories.setVisibility(View.VISIBLE);
                mBinding.tvAddEventCategories.setVisibility(View.VISIBLE);
            } else {
                mBinding.switchEvent.setChecked(false);
                mBinding.rvEventSlots.setVisibility(View.GONE);
                mBinding.tvEventCategories.setVisibility(View.GONE);
                mBinding.etEventSubcategories.setVisibility(View.GONE);
                mBinding.tvAddEventCategories.setVisibility(View.GONE);
            }

            mBinding.switchEvent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(mEventInfoListener != null) {
                        mEventInfoListener.onEventSwitchChangedListener(position, isChecked);
                    }
                }
            });

            if(eventViewHolderModel.getEventCategories() != null) {
                if(!TextUtils.isEmpty(eventViewHolderModel.getEventCategories().getName())) {
                    mBinding.tvEventCategories.setText(eventViewHolderModel.getEventCategories().getName());
                }
            }

            if(eventViewHolderModel.getSelectedEventSubcategories() != null
                    && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                mBinding.etEventSubcategories.setText(
                        Utils.getCommaSepSubcategoriesString(eventViewHolderModel.getSelectedEventSubcategories()));
            } else {
                mBinding.etEventSubcategories.setText("");
            }

            //mBinding.etEventSubcategories.setFocusableInTouchMode(true);
            mBinding.tvAddEventCategories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mEventInfoListener != null) {
                        mEventInfoListener.onEventCategoriesClickListener(position);
                    }
                }
            });

        }
    }

}
