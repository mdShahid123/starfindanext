package com.sportsstars.ui.speaker.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySelectSpeakerSportsBinding;
import com.sportsstars.eventbus.BackNavigationEvent;
import com.sportsstars.interfaces.SportsItemSelectedListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.Sports;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SportsListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.search.FragmentSearchCoachBySport;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import retrofit2.Call;

public class SelectSpeakerSportsActivity extends BaseActivity implements SportsItemSelectedListener {

    private ActivitySelectSpeakerSportsBinding mBinding;

    private ImageAdapter mImageAdapter;
    private ArrayList<Sports> mSportsList;
    private ArrayList<Sports> mSelectedSportsList = new ArrayList<>();
    private EventViewHolderModel eventViewHolderModel;
    private int eventId;
    private String eventName;
    private boolean isCoachSearch;
    private int isPanel;
    private boolean isAllSportsSelected;
    private boolean isFromSpeakerFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_speaker_sports);

        getIntentData();
        initViews();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        if(Utils.isNetworkAvailable()) {
            getSportsList();
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(BackNavigationEvent backNavigationEvent) {
        if(backNavigationEvent.isBackNavigated()) {
            finish();
        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_VIEW_HOLDER)) {
                eventViewHolderModel = bundle.getParcelable(Constants.BundleKey.EVENT_VIEW_HOLDER);
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_ID)) {
                eventId = bundle.getInt(Constants.BundleKey.EVENT_ID, 0);
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_NAME)) {
                eventName = bundle.getString(Constants.BundleKey.EVENT_NAME);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_COACH_SEARCH)) {
                isCoachSearch = bundle.getBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_PANEL)) {
                isPanel = bundle.getInt(Constants.BundleKey.IS_PANEL, 0);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SPEAKER_FILTER)) {
                isFromSpeakerFilter = bundle.getBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
            }
//            if(isFromSpeakerFilter) {
//                if(getIntent().hasExtra(Constants.BundleKey.SPORTS_LIST)) {
//                    mSelectedSportsList = bundle.getParcelableArrayList(Constants.BundleKey.SPORTS_LIST);
//                }
//                if(getIntent().hasExtra(Constants.BundleKey.ALL_SPORTS_SELECTED)) {
//                    isAllSportsSelected = bundle.getBoolean(Constants.BundleKey.ALL_SPORTS_SELECTED, false);
//                }
//            }
        }
    }

    private void initViews() {
        mBinding.layoutTop.headerId.tvTitle.setText(R.string.search_for_sports);
        mBinding.etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.EXTRA_SEARCH_SPORTS_LIST, mSportsList);
                Navigator.getInstance().navigateToActivityForResultWithData(
                        SelectSpeakerSportsActivity.this,
                        SearchSpeakerSportsActivity.class,
                        bundle,
                        Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_SPEAKER_SPORTS);
            }
        });
//        mBinding.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                showToast("Go to search results activity");
//            }
//        });
        mBinding.buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable()) {
                    if(isFromSpeakerFilter) {
                         goBackToFilterScreen();
                    } else {
                        launchSpeakerSearchResult();
                    }
                } else {
                    showSnackbarFromTop(SelectSpeakerSportsActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    private void goBackToFilterScreen() {
        if(isInputValid()) {
            getSportData();
            Intent intent = new Intent();
            intent.putExtra(Constants.BundleKey.EVENT_VIEW_HOLDER, eventViewHolderModel);
            intent.putExtra(Constants.BundleKey.EVENT_ID, eventViewHolderModel.getId());
            intent.putExtra(Constants.BundleKey.EVENT_NAME, eventViewHolderModel.getName());
            intent.putParcelableArrayListExtra(Constants.BundleKey.SPORTS_LIST, mSelectedSportsList);

            if(mSelectedSportsList.size() == mSportsList.size()-1) {
                isAllSportsSelected = true;
            } else {
                isAllSportsSelected = false;
            }
            intent.putExtra(Constants.BundleKey.ALL_SPORTS_SELECTED, isAllSportsSelected);


            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void getSportData() {
//        if(mSelectedSportsList != null
//                && mSelectedSportsList.size() > 0) {
//            Sports mSport = mSelectedSportsList.get(0);
//            Log.d("sport_log", "speaker select sport - setResult - "
//                    +" mSport id : " + mSport.getId() + " mSport name : " + mSport.getName());
//        }
    }

    private void launchSpeakerSearchResult() {
        if(isInputValid()) {
            Bundle bundleSearchSpeaker = new Bundle();
            bundleSearchSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, isCoachSearch);
            bundleSearchSpeaker.putInt(Constants.BundleKey.IS_PANEL, isPanel);
            bundleSearchSpeaker.putParcelable(Constants.BundleKey.EVENT_VIEW_HOLDER, eventViewHolderModel);
            bundleSearchSpeaker.putInt(Constants.BundleKey.EVENT_ID, eventId);
            bundleSearchSpeaker.putString(Constants.BundleKey.EVENT_NAME, eventName);
            bundleSearchSpeaker.putParcelableArrayList(Constants.BundleKey.SPORTS_LIST, mSelectedSportsList);

            if(mSelectedSportsList.size() == mSportsList.size()-1) {
                isAllSportsSelected = true;
            } else {
                isAllSportsSelected = false;
            }
            bundleSearchSpeaker.putBoolean(Constants.BundleKey.ALL_SPORTS_SELECTED, isAllSportsSelected);

//            Log.d("select_sports", "Selected sports list size : " + mSelectedSportsList.size()
//                + " selected sports list is : " + mSelectedSportsList);
            Navigator.getInstance().navigateToActivityWithData(
                    SelectSpeakerSportsActivity.this, SearchSpeakerResultActivity.class, bundleSearchSpeaker);

            deselectAllSports();
        }
    }

    private void deselectAllSports() {
        for(Sports sports : mSportsList) {
            sports.setSelected(false);
        }
        mImageAdapter.notifyDataSetChanged();
        isAllSportsSelected = false;
        if(mSelectedSportsList != null) {
            mSelectedSportsList.clear();
        }
    }

    private boolean isInputValid() {
        if(mSelectedSportsList == null) {
            showSnackbarFromTop(SelectSpeakerSportsActivity.this, getResources().getString(R.string.selected_sports_list_invalid));
            return false;
        } else if(mSelectedSportsList.size() == 0) {
            showSnackbarFromTop(SelectSpeakerSportsActivity.this, getResources().getString(R.string.selected_sports_list_empty));
            return false;
        } else if(eventId == 0) {
            showSnackbarFromTop(SelectSpeakerSportsActivity.this, getResources().getString(R.string.selected_event_is_invalid));
            return false;
        } else {
            return true;
        }
    }

    private void getSportsList() {
        processToShowDialog();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);
        getSportsRequest.setPageSize(Constants.ALL_SPORTS_PAGE_SIZE);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>(SelectSpeakerSportsActivity.this) {
            @Override
            public void onSuccess(SportsListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        generateAllSportsObject();
                        mSportsList.addAll(response.getResult().getSportsList());
                        //mSportsList = response.getResult().getSportsList();
                        //setSelectedSports();
                        mImageAdapter = new ImageAdapter(SelectSpeakerSportsActivity.this, mSportsList,
                                SelectSpeakerSportsActivity.this);
                        mBinding.gridView.setAdapter(mImageAdapter);
//                        for (Sports item : mSportsList) {
//                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getReferencecLogo());
//                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SelectSpeakerSportsActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {
                try {
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SelectSpeakerSportsActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setSelectedSports() {
        if(isFromSpeakerFilter) {
            if(mSportsList != null && mSportsList.size() > 0) {
                if(isAllSportsSelected) {
                    for(Sports sportsAll : mSportsList) {
                        sportsAll.setSelected(true);
                    }
                } else {
                    for(Sports sportsOuter : mSportsList) {
                        if(mSelectedSportsList != null && mSelectedSportsList.size() > 0) {
                            for(Sports sportsInner : mSelectedSportsList) {
                                if(sportsOuter.getId() == sportsInner.getId()) {
                                    sportsOuter.setSelected(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public String getActivityName() {
        return "SelectSpeakerSports";
    }

    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        private ArrayList<Sports> sportsArrayList;
        private SportsItemSelectedListener mSportsItemSelectedListener;

        public ImageAdapter(Context c, ArrayList<Sports> sports, SportsItemSelectedListener sportsItemSelectedListener) {
            mContext = c;
            this.sportsArrayList = sports;
            mSportsItemSelectedListener = sportsItemSelectedListener;
        }

        public int getCount() {
            return sportsArrayList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.item_multiple_sports_grid, null);
            }

            RelativeLayout rlSportsGrid = view.findViewById(R.id.rl_sports_grid);
            TextView tvSportName = view.findViewById(R.id.tv_sport);
            ImageView ivSportsLogo = view.findViewById(R.id.iv_sport_logo);
            ImageView ivSportsCover = view.findViewById(R.id.iv_sport_cover);
            ImageView ivSportsCheckbox = view.findViewById(R.id.iv_sport_checkbox);

            Sports sports = sportsArrayList.get(position);
            if(sports != null) {
                tvSportName.setText(sports.getName());

                if(!TextUtils.isEmpty(sports.getReferenceImageUrl())) {
                    Picasso.with(mContext)
                            .load(sports.getReferenceImageUrl())
                            .into((ivSportsCover));
                }

                if(!TextUtils.isEmpty(sports.getReferencecLogo())) {
                    Picasso.with(mContext)
                            .load(sports.getReferencecLogo())
                            .into((ivSportsLogo));
                }

                if(sports.getId() == 0
                        && sports.getName().equals(mContext.getResources().getString(R.string.all_sports))) {
                    ivSportsCover.setImageResource(R.drawable.ic_all_sports_bg);
                    ivSportsLogo.setImageResource(R.drawable.ic_all_sports);
                }

                if(sports.isSelected()) {
                    ivSportsCheckbox.setImageResource(R.drawable.ic_checked);
                } else {
                    ivSportsCheckbox.setImageResource(R.drawable.ic_unchecked);
                }

                rlSportsGrid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(sports.isSelected()) {
                            mSportsItemSelectedListener.onSportsItemSelected(position, false);
                        } else {
                            mSportsItemSelectedListener.onSportsItemSelected(position, true);
                        }
                    }
                });
            }

            return view;
        }

    }

    @Override
    public void onSportsItemSelected(int position, boolean isSelected) {
        if(mSportsList != null && mSportsList.size() > 0) {
            Sports sports = mSportsList.get(position);
            if(sports != null) {
                if(isAllSportsObject(sports)) {
                    sports.setSelected(isSelected);
                    checkForAllSportsSelection(sports);
                } else {
                    sports.setSelected(isSelected);
                    Sports allSports = mSportsList.get(0);
                    if(isSelected) {
                        mSelectedSportsList.add(sports);
                    } else {
                        allSports.setSelected(false);
                        mSelectedSportsList.remove(sports);
                    }

//                    Log.d("sports_log", "mSelectedSportsList size : " + mSelectedSportsList.size());

                    if(mSelectedSportsList.size() == mSportsList.size()-1) {
                        allSports.setSelected(true);
                    } else {
                        allSports.setSelected(false);
                    }
                }

                if(mImageAdapter != null) {
                    mImageAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void checkForAllSportsSelection(Sports allSportsObj) {
        if(isAllSportsObject(allSportsObj)) {
            for(Sports sportsObj : mSportsList) {
                sportsObj.setSelected(allSportsObj.isSelected());
            }

            if(allSportsObj.isSelected()) {
                mSelectedSportsList.clear();
                for(Sports sportsNonAllSports : mSportsList) {
                    if(!isAllSportsObject(sportsNonAllSports)) {
                        mSelectedSportsList.add(sportsNonAllSports);
                    }
                }
            } else {
                mSelectedSportsList.clear();
            }
//            Log.d("sports_log", "mSelectedSportsList size : " + mSelectedSportsList.size());
        }
    }

    private void generateAllSportsObject() {
        Sports sports = new Sports();
        sports.setId(0);
        sports.setName(getResources().getString(R.string.all_sports));
        sports.setSelected(false);
        sports.setReferencecLogo("");
        sports.setBackgroundImage("");
        sports.setDescription("");
        sports.setReferenceImageUrl("");
        sports.setSkillLevel(-1);

        if(mSportsList != null && mSportsList.size() > 0) {
            mSportsList.clear();
            mSportsList.add(sports);
//            Log.d("speaker_log", "mSportsList size after clear : " + mSportsList.size());
        } else {
            mSportsList = new ArrayList<>();
            mSportsList.add(sports);
//            Log.d("speaker_log", "mSportsList size after new creation : " + mSportsList.size());
        }
    }

    private boolean isAllSportsObject(Sports sports) {
        if(sports != null && sports.getId() == 0
                && sports.getName().equals(getResources().getString(R.string.all_sports))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_SPEAKER_SPORTS) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    if(mSportsList == null) {
                        mSportsList = new ArrayList<>();
                    }
                    if(mSelectedSportsList == null) {
                        mSelectedSportsList = new ArrayList<>();
                    }
                    if(mSportsList.size() > 0) {
                        mSportsList.clear();
                    }
                    if(mSelectedSportsList.size() > 0) {
                        mSelectedSportsList.clear();
                    }
                    mSportsList.addAll(data.getParcelableArrayListExtra(Constants.EXTRA_SEARCH_SPORTS_LIST));
                    for(Sports sports : mSportsList) {
                        if(sports != null) {
                            if(isAllSportsObject(sports)) {
                                if(sports.isSelected()) {
                                    mSelectedSportsList.clear();
                                    for(Sports sportsNonAllSports : mSportsList) {
                                        if(!isAllSportsObject(sportsNonAllSports)) {
                                            mSelectedSportsList.add(sportsNonAllSports);
                                        }
                                    }
                                    break;
                                }
                            } else if(!isAllSportsObject(sports)) {
                                if(sports.isSelected()) {
                                    mSelectedSportsList.add(sports);
                                }
                            }
                        }
                    }
                    if(mImageAdapter != null) {
                        mImageAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

}
