package com.sportsstars.ui.speaker.search;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchEventTypeBinding;
import com.sportsstars.databinding.ItemFlowSportsBinding;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.speaker.EventList;
import com.sportsstars.network.response.speaker.EventListResponse;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.network.response.speaker.EventSubcategoryDetail;
import com.sportsstars.network.response.speaker.GuestSpeakerEvents;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import retrofit2.Call;

public class SearchEventTypeActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySearchEventTypeBinding mBinding;
    private ArrayList<EventViewHolderModel> mGlobalEventList = new ArrayList<>();
    private ArrayList<ItemFlowSportsBinding> mItemFlowBindingList;
    private ArrayList<EventViewHolderModel> mEventFilterList = new ArrayList<>();
    private boolean isNoSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_event_type);

        initViews();
        setTextChangeListener();
        if(Utils.isNetworkAvailable()) {
            getEventList();
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    private void initViews() {
        mBinding.layoutTop.headerId.tvTitle.setText(R.string.select_event_title);
        mBinding.tvSelect.setOnClickListener(this);
        mBinding.tvSelect.setVisibility(View.GONE);

        mItemFlowBindingList = new ArrayList<>();
        mBinding.flowLayoutSports.removeAllViews();
    }

    private void getEventList() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.getEventList().enqueue(new BaseCallback<EventListResponse>(SearchEventTypeActivity.this) {
            @Override
            public void onSuccess(EventListResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null && response.getResult().size() > 0) {
                            generateEventList(response.getResult());
                            setEventList();
                        } else {

                        }
                        //showToast("Getting event list successfully !!!");
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(SearchEventTypeActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<EventListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SearchEventTypeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void generateEventList(ArrayList<EventList> localEventList) {
        if(localEventList != null && localEventList.size() > 0) {
            for(int i = 0; i < localEventList.size(); i++) {
                EventList eventList = localEventList.get(i);
                if(eventList != null) {
                    EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
                    eventViewHolderModel.setId(eventList.getId());
                    eventViewHolderModel.setName(eventList.getName());
                    eventViewHolderModel.setDescription(eventList.getDescription());
                    eventViewHolderModel.setEventLogo(eventList.getEventLogo());
                    eventViewHolderModel.setBackgroundLogo(eventList.getBackgroundLogo());
                    eventViewHolderModel.setStatus(eventList.getStatus());
                    eventViewHolderModel.setGstValue(eventList.getGstValue());
                    eventViewHolderModel.setEventSlot(eventList.getEventSlot());
                    eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_EVENT);
                    eventViewHolderModel.setChecked(isEventPriceAvailable(eventList));
                    eventViewHolderModel.setEventCategories(eventList.getEventCategories());

                    if(eventList.getSelectedEventSubcategories() != null
                            && eventList.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventList.getSelectedEventSubcategories()) {
                            eventSubcategory.setSelected(true);
                        }
                    }

                    eventViewHolderModel.setSelectedEventSubcategories(eventList.getSelectedEventSubcategories());

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
                        for(int z = 0; z < eventViewHolderModel.getEventCategories().getEventSubcategories().size(); z++) {
                            EventSubcategory eventSubcategory
                                    = eventViewHolderModel.getEventCategories().getEventSubcategories().get(z);
                            eventSubcategory.setEventSubcategoryId(eventSubcategory.getId());

                            EventSubcategoryDetail eventSubcategoryDetail = new EventSubcategoryDetail();
                            eventSubcategoryDetail.setId(eventSubcategory.getId());
                            eventSubcategoryDetail.setName(eventSubcategory.getName());

                            eventSubcategory.setEventSubcategoryDetail(eventSubcategoryDetail);
                        }
                    }

                    if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventViewHolderModel.getSelectedEventSubcategories()) {
                            eventSubcategory.setId(eventSubcategory.getEventSubcategoryId());
                            eventSubcategory.setSelected(true);
                        }
                    }

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {

                        if (eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {

                            for (EventSubcategory eventSubcategoryGlobal :
                                    eventViewHolderModel.getEventCategories().getEventSubcategories()) {
                                for (EventSubcategory eventSubcategorySelected :
                                        eventViewHolderModel.getSelectedEventSubcategories()) {
                                    if (eventSubcategoryGlobal.getEventSubcategoryId()
                                            == eventSubcategorySelected.getEventSubcategoryId()) {
                                        eventSubcategoryGlobal.setSelected(true);
                                    }
                                }
                            }

                        }

                    }

                    removeDuplicateEventSubcategories(eventViewHolderModel);

                    mGlobalEventList.add(eventViewHolderModel);
                }
            }
        }
    }

    private void removeDuplicateEventSubcategories(EventViewHolderModel eventViewHolderModel) {
        LinkedHashSet<EventSubcategory> duplicateRemovalHashSet = new LinkedHashSet<>();
        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
            duplicateRemovalHashSet.addAll(eventViewHolderModel.getSelectedEventSubcategories());
            if(duplicateRemovalHashSet.size() > 0) {
                Log.d("speaker_event_log", " duplicate removal set size : "
                        + duplicateRemovalHashSet.size()
                        + " for " + eventViewHolderModel.getName());
                eventViewHolderModel.getSelectedEventSubcategories().clear();
                eventViewHolderModel.getSelectedEventSubcategories().addAll(duplicateRemovalHashSet);
            }
        }
    }

    private boolean isEventPriceAvailable(EventList eventList) {
        boolean isChecked = false;
        int slotCount = 0;
        if(eventList.getEventSlot() != null && eventList.getEventSlot().size() > 0) {
            for(int j = 0; j < eventList.getEventSlot().size(); j++) {
                GuestSpeakerEvents guestSpeakerEvents = eventList.getEventSlot().get(j).getGuestSpeakerEvents();
                if(guestSpeakerEvents == null) {
                    isChecked = false;
                    //break;
                } else {
                    isChecked = true;
                    slotCount++;
                }
            }

            if(slotCount > 0) {
                isChecked = true;
            } else {
                isChecked = false;
            }
        }
        return isChecked;
    }

    private void setEventList() {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            if(mEventFilterList != null && mEventFilterList.size() > 0) {
                mEventFilterList.clear();
                mEventFilterList.addAll(mGlobalEventList);
            }
            for(EventViewHolderModel eventViewHolderModel : mGlobalEventList) {
                if(eventViewHolderModel != null) {
                    addTitleToLayout(eventViewHolderModel);
                }
            }
        }
    }

    private void addTitleToLayout(final EventViewHolderModel eventViewHolderModel) {

        if(eventViewHolderModel != null) {

            String eventName = "";

            if(eventViewHolderModel.getName() != null
                    && !TextUtils.isEmpty(eventViewHolderModel.getName())) {
                eventName = eventViewHolderModel.getName();
            }

            final ItemFlowSportsBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from
                    (mBinding.flowLayoutSports.getContext())
                    .inflate(R.layout.item_flow_sports, mBinding.flowLayoutSports, false));

            if(flowBinding != null) {

                flowBinding.flowChild.setText(eventName);
                flowBinding.flowChild.setTag(eventViewHolderModel);
                if(eventViewHolderModel.isSelected()) {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                } else {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                }

                mItemFlowBindingList.add(flowBinding);

                flowBinding.flowChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                    for (int i = 0; i < mEventSubcategories.size(); i++) {
//                        mEventSubcategories.get(i).setSelected(false);
//                        mItemFlowBindingList.get(i).flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
//                        mItemFlowBindingList.get(i).flowChild.setTextColor(getResources().getColor(R.color.green_gender));
//                    }

                        EventViewHolderModel eventViewHolderModelLocal = (EventViewHolderModel) flowBinding.flowChild.getTag();
                        if(eventViewHolderModelLocal.isSelected()) {
                            flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                            flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                            eventViewHolderModelLocal.setSelected(false);
                        } else {
                            flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                            flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                            eventViewHolderModelLocal.setSelected(true);
                        }

                        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
                            for(EventViewHolderModel eventViewHolderModelGlobal : mGlobalEventList) {
                                if(eventViewHolderModelGlobal.getId() == eventViewHolderModelLocal.getId()) {
                                    if(eventViewHolderModelLocal.isSelected()) {
                                        eventViewHolderModelLocal.setSelected(true);
                                    } else {
                                        eventViewHolderModelLocal.setSelected(false);
                                    }
                                }
                            }
                        }

                        setResultForEventList((EventViewHolderModel) flowBinding.flowChild.getTag());

                        //setResultForEventList(mGlobalEventList);

                    }
                });

                mBinding.flowLayoutSports.addView(flowBinding.getRoot());

            }

        }

    }

    private void setTextChangeListener() {
        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //removeSubcategoriesSelection();
                String constraint = s.toString();
                if(constraint != null && constraint.length() > 0) {
                    constraint = constraint.toUpperCase();
                    ArrayList<EventViewHolderModel> filters = new ArrayList<>();
                    for(int i = 0; i < mGlobalEventList.size(); i++) {
                        if(mGlobalEventList.get(i).getName().toUpperCase().startsWith(constraint)) {
                            EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
                            eventViewHolderModel.setId(mGlobalEventList.get(i).getId());
                            eventViewHolderModel.setName(mGlobalEventList.get(i).getName());
                            eventViewHolderModel.setBackgroundLogo(mGlobalEventList.get(i).getBackgroundLogo());
                            eventViewHolderModel.setSelected(mGlobalEventList.get(i).isSelected());
                            eventViewHolderModel.setChecked(mGlobalEventList.get(i).isChecked());
                            eventViewHolderModel.setSelectedEventSubcategories(mGlobalEventList.get(i).getSelectedEventSubcategories());
                            eventViewHolderModel.setEventCategories(mGlobalEventList.get(i).getEventCategories());
                            eventViewHolderModel.setEventLogo(mGlobalEventList.get(i).getEventLogo());
                            eventViewHolderModel.setSpecialities(mGlobalEventList.get(i).getSpecialities());
                            eventViewHolderModel.setViewType(mGlobalEventList.get(i).getViewType());
                            eventViewHolderModel.setEventSlot(mGlobalEventList.get(i).getEventSlot());
                            eventViewHolderModel.setGstValue(mGlobalEventList.get(i).getGstValue());
                            eventViewHolderModel.setStatus(mGlobalEventList.get(i).getStatus());
                            eventViewHolderModel.setDescription(mGlobalEventList.get(i).getDescription());

                            filters.add(eventViewHolderModel);
                        }
                    }
                    if(mEventFilterList != null) {
                        if(mEventFilterList.size() > 0) {
                            mEventFilterList.clear();
                        }
                        mEventFilterList.addAll(filters);
                    }
                } else {
                    if(mEventFilterList != null) {
                        if(mEventFilterList.size() > 0) {
                            mEventFilterList.clear();
                        }
                        mEventFilterList.addAll(mGlobalEventList);
                    }
                }
                if(mEventFilterList != null && mEventFilterList.size() > 0) {
                    mBinding.tvNoResults.setVisibility(View.GONE);
                    mBinding.flowLayoutSports.setVisibility(View.VISIBLE);
                    isNoSearchResult = false;
                } else {
                    mBinding.tvNoResults.setVisibility(View.VISIBLE);
                    mBinding.flowLayoutSports.setVisibility(View.GONE);
                    initViews();
                    isNoSearchResult = true;
                }

                populateFilterList(isNoSearchResult);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Method used to remove event selection from the list.
     */
    private void removeEventSelection() {
        if(mEventFilterList != null && mEventFilterList.size() > 0) {
            for(int i = 0; i < mEventFilterList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mEventFilterList.get(i);
                eventViewHolderModel.setSelected(false);
            }

            if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
                for(int i = 0; i < mGlobalEventList.size(); i++) {
                    EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                    eventViewHolderModel.setSelected(false);
                }
            }
        }

        initViews();
    }

    private void populateFilterList(boolean isNoSearchResult) {
        if(isNoSearchResult) {
            mItemFlowBindingList = new ArrayList<>();
            mBinding.flowLayoutSports.removeAllViews();

            if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
                if(mEventFilterList != null && mEventFilterList.size() > 0) {
                    mEventFilterList.clear();
                    mEventFilterList.addAll(mGlobalEventList);
                }
                for(EventViewHolderModel eventViewHolderModel : mGlobalEventList) {
                    if(eventViewHolderModel != null) {
                        addTitleToLayout(eventViewHolderModel);
                    }
                }
            }
        } else {
            mItemFlowBindingList = new ArrayList<>();
            mBinding.flowLayoutSports.removeAllViews();

            if(mEventFilterList != null && mEventFilterList.size() > 0) {
                for(EventViewHolderModel eventViewHolderModel : mEventFilterList) {
                    if(eventViewHolderModel != null) {
                        addTitleToLayout(eventViewHolderModel);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
//        if(v.getId() == R.id.tv_select) {
//            int isSelectedCount = 0;
//            for(int i = 0; i < mGlobalEventList.size(); i++) {
//                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
//                if(eventViewHolderModel.isSelected()) {
//                    isSelectedCount++;
//                }
//                Log.d("category_log", "Event subcategories list - "
//                        + " eventViewHolderModel id : " + eventViewHolderModel.getId()
//                        + " eventViewHolderModel isSelected : " + eventViewHolderModel.isSelected()
//                        + " isSelected count : " + isSelectedCount
//                );
//            }
//
//            if(isSelectedCount >= 1) {
//                setResultForEventList(mGlobalEventList);
//            } else {
//                showSnackbarFromTop(SearchEventTypeActivity.this,
//                        getResources().getString(R.string.please_select_one_event));
//            }
//        }
    }

//    private void setResultForEventList(ArrayList<EventViewHolderModel> eventList) {
//        Intent intent = new Intent();
//        intent.putParcelableArrayListExtra(Constants.EXTRA_EVENTS_LIST, (ArrayList<? extends Parcelable>) eventList);
//        setResult(Activity.RESULT_OK, intent);
//        finish();
//    }

    private void setResultForEventList(EventViewHolderModel eventViewHolderModel) {
        Intent intent = new Intent();
        intent.putExtra(Constants.BundleKey.EVENT_VIEW_HOLDER, eventViewHolderModel);
        intent.putExtra(Constants.BundleKey.EVENT_ID, eventViewHolderModel.getId());
        intent.putExtra(Constants.BundleKey.EVENT_NAME, eventViewHolderModel.getName());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public String getActivityName() {
        return "SearchEventType";
    }

}
