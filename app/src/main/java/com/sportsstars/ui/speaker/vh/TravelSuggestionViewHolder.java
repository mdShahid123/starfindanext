package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemTravelSuggestionViewHolderBinding;
import com.sportsstars.interfaces.TravelInfoListener;
import com.sportsstars.model.TravelViewHolderModel;

public class TravelSuggestionViewHolder extends RecyclerView.ViewHolder {

    private ItemTravelSuggestionViewHolderBinding mBinding;
    private Activity mActivity;
    private TravelInfoListener mTravelInfoListener;

    public TravelSuggestionViewHolder(ItemTravelSuggestionViewHolderBinding binding, Activity activity, TravelInfoListener travelInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mTravelInfoListener = travelInfoListener;
    }

    public void bindData(TravelViewHolderModel travelViewHolderModel, int position) {
        String providerText = mActivity.getResources().getString(R.string.provider_webjet);
        Spannable providerTextSpannable = new SpannableString(providerText);
        providerTextSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.green_gender)),
                providerText.indexOf(":")+2, providerText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        providerTextSpannable.setSpan(new ClickableSpan() {
//            @Override
//            public void onClick(View widget) {
//                Toast.makeText(mActivity, "WebJet clicked from ViewHolder", Toast.LENGTH_LONG).show();
//                if(mTravelInfoListener != null) {
//                    mTravelInfoListener.onTravelSuggestionClickListener();
//                }
//            }
//        },providerText.indexOf(":")+2,providerText.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.tvProviderText.setText(providerTextSpannable);
        mBinding.tvProviderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onTravelSuggestionClickListener();
                }
            }
        });
    }

}
