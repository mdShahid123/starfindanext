package com.sportsstars.ui.speaker;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CompoundButton;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityEditSpeakerProfileBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.speaker.editprofile.SpeakerEventEditFragment;
import com.sportsstars.ui.speaker.editprofile.SpeakerPersonalEditFragment;
import com.sportsstars.ui.speaker.editprofile.SpeakerTravelEditFragment;
import com.sportsstars.util.Alert;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class EditSpeakerProfileActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    private ActivityEditSpeakerProfileBinding mBinding;
    private SpeakerPersonalEditFragment speakerPersonalEditFragment = new SpeakerPersonalEditFragment();
    private SpeakerEventEditFragment speakerEventEditFragment = new SpeakerEventEditFragment();
    private SpeakerTravelEditFragment speakerTravelEditFragment = new SpeakerTravelEditFragment();
    private int mCurrentTabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_speaker_profile);
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }
        initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(otpVerifiedEvent.isOtpVerified()) {
            finish();
        }
    }

    private void initUI() {
        mBinding.header.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.header.tvTitle.setText(getString(R.string.edit_profile));

        switchToPersonalEdit();

        mBinding.personalRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentTabId != mBinding.personalRadioBtn.getId()) {
                    mBinding.personalRadioBtn.setChecked(false);
                    Alert.showAlertDialogWithBottomButton(EditSpeakerProfileActivity.this, getString(R.string.save_changes), getString(R.string.sure_to_switch_tabs), getString(R.string.cancel), getString(R.string.proceed), new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            switchToPersonalEdit();
                        }
                    }, new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {
                            if(mCurrentTabId == mBinding.speakingRadioBtn.getId()) {
                                switchToSpeakingEdit();
                            } else {
                                switchToTravelEdit();
                            }
                        }
                    });
                }
            }
        });

        mBinding.speakingRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentTabId != mBinding.speakingRadioBtn.getId()) {
                    mBinding.speakingRadioBtn.setChecked(false);
                    Alert.showAlertDialogWithBottomButton(EditSpeakerProfileActivity.this, getString(R.string.save_changes), getString(R.string.sure_to_switch_tabs), getString(R.string.cancel), getString(R.string.proceed), new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            switchToSpeakingEdit();
                        }
                    }, new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {
                            if(mCurrentTabId == mBinding.personalRadioBtn.getId()) {
                                switchToPersonalEdit();
                            } else {
                                switchToTravelEdit();
                            }
                        }
                    });
                }
            }
        });

        mBinding.travelRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentTabId != mBinding.travelRadioBtn.getId()) {
                    mBinding.travelRadioBtn.setChecked(false);
                    Alert.showAlertDialogWithBottomButton(EditSpeakerProfileActivity.this, getString(R.string.save_changes), getString(R.string.sure_to_switch_tabs), getString(R.string.cancel), getString(R.string.proceed), new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            switchToTravelEdit();
                        }
                    }, new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {
                            if(mCurrentTabId == mBinding.personalRadioBtn.getId()) {
                                switchToPersonalEdit();
                            } else {
                                switchToSpeakingEdit();
                            }
                        }
                    });
                }
            }
        });

        Typeface font = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.font_bebas_neue));
        mBinding.personalRadioBtn.setTypeface(font);
        mBinding.speakingRadioBtn.setTypeface(font);
        mBinding.travelRadioBtn.setTypeface(font);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.personalRadioBtn:
                if (isChecked) {
                    mBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.black));
                    switchToPersonalEdit();
                } else
                    mBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
                break;
            case R.id.speakingRadioBtn:
                if (isChecked) {
                    mBinding.speakingRadioBtn.setTextColor(getResources().getColor(R.color.black));
                    switchToSpeakingEdit();
                } else
                    mBinding.speakingRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
                break;
            case R.id.travelRadioBtn:
                if (isChecked) {
                    mBinding.travelRadioBtn.setTextColor(getResources().getColor(R.color.black));
                    switchToTravelEdit();
                } else
                    mBinding.travelRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
                break;
        }
    }

    public void switchToPersonalEdit() {
        mCurrentTabId = mBinding.personalRadioBtn.getId();
        setDefaultAllRadioButtons();
        mBinding.personalRadioBtn.setChecked(true);
        mBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.black));
        speakerPersonalEditFragment = new SpeakerPersonalEditFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.editSpeakerProfileContainer, speakerPersonalEditFragment).commit();
    }

    public void switchToSpeakingEdit() {
        mCurrentTabId = mBinding.speakingRadioBtn.getId();
        setDefaultAllRadioButtons();
        mBinding.speakingRadioBtn.setChecked(true);
        mBinding.speakingRadioBtn.setTextColor(getResources().getColor(R.color.black));
        speakerEventEditFragment = new SpeakerEventEditFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.editSpeakerProfileContainer, speakerEventEditFragment).commit();
    }

    public void switchToTravelEdit() {
        mCurrentTabId = mBinding.travelRadioBtn.getId();
        setDefaultAllRadioButtons();
        mBinding.travelRadioBtn.setChecked(true);
        mBinding.travelRadioBtn.setTextColor(getResources().getColor(R.color.black));
        speakerTravelEditFragment = new SpeakerTravelEditFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.editSpeakerProfileContainer, speakerTravelEditFragment).commit();
    }

    private void setDefaultAllRadioButtons() {
        mBinding.personalRadioBtn.setChecked(false);
        mBinding.personalRadioBtn.setTextColor(getResources().getColor(R.color.white_color));

        mBinding.speakingRadioBtn.setChecked(false);
        mBinding.speakingRadioBtn.setTextColor(getResources().getColor(R.color.white_color));

        mBinding.travelRadioBtn.setChecked(false);
        mBinding.travelRadioBtn.setTextColor(getResources().getColor(R.color.white_color));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public String getActivityName() {
        return "EditSpeakerProfile";
    }

}
