package com.sportsstars.ui.speaker.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.network.response.speaker.EventSlot;

import java.util.ArrayList;

public class SlotInfoPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<EventSlot> eventSlotsList;

    public SlotInfoPagerAdapter(Context context, ArrayList<EventSlot> eventSlotsList) {
        this.context = context;
        this.eventSlotsList = eventSlotsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //return super.instantiateItem(container, position);

        TextView tvEventPrice = null;
        TextView tvEventDuration = null;

        View itemView = inflater.inflate(R.layout.item_slot_price_info, container, false);

        tvEventPrice = (TextView) itemView.findViewById(R.id.tv_slot_price);
        tvEventDuration = (TextView) itemView.findViewById(R.id.tv_slot_name);

        String slotName = "";
        String slotPrice = "";

        EventSlot eventSlot = eventSlotsList.get(position);
        if(eventSlot != null) {
            if(eventSlot.getGuestSpeakerEvents() == null) {
                slotName = eventSlot.getSlotText();
                slotPrice = "NA";
            } else {
                slotName = eventSlot.getSlotText();
                if(eventSlot.getGuestSpeakerEvents().getSlotPrice() != 0) {
                    slotPrice = "$"+String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice());
                } else if(eventSlot.getGuestSpeakerEvents().getSlotPrice() == 0) {
                    slotPrice = "$"+String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice());
                } else {
                    slotPrice = "NA";
                }
            }
        }

        if(!TextUtils.isEmpty(slotPrice)) {
            tvEventPrice.setText(slotPrice);
        } else {
            tvEventPrice.setText("NA");
        }

        if(!TextUtils.isEmpty(slotName)) {
            tvEventDuration.setText(slotName);
        } else {
            tvEventDuration.setText("NA");
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return eventSlotsList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public int getItemId(int pos) {
        return pos;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

}
