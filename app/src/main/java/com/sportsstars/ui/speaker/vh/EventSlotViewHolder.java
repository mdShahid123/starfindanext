package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemEventSlotViewHolderBinding;
import com.sportsstars.interfaces.EventInfoListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.network.response.speaker.EventSlot;

public class EventSlotViewHolder extends RecyclerView.ViewHolder {

    private ItemEventSlotViewHolderBinding mBinding;
    private Activity mActivity;
    private EventInfoListener mEventInfoListener;

    public EventSlotViewHolder(ItemEventSlotViewHolderBinding binding, Activity activity, EventInfoListener eventInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mEventInfoListener = eventInfoListener;
    }

    public void bindData(EventViewHolderModel eventViewHolderModel, int parentPosition, EventSlot eventSlot, int slotPosition) {
        if(eventSlot != null) {
            mBinding.tvSlotName.setText(eventSlot.getSlotText());
            if(eventSlot.getGuestSpeakerEvents() != null) {
                if(!TextUtils.isEmpty(String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice()))
                        && eventSlot.getGuestSpeakerEvents().getSlotPrice() != 0) {
                    mBinding.tvSlotFee.setText("$"+String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice()));
                } else if(!TextUtils.isEmpty(String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice()))
                        && eventSlot.getGuestSpeakerEvents().getSlotPrice() == 0) {
                    mBinding.tvSlotFee.setText("$"+String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice()));
//                    mBinding.tvSlotFee.setText("");
//                    mBinding.tvSlotFee.setHint(mActivity.getResources().getString(R.string.enter_fee));
                }
            } else {
                mBinding.tvSlotFee.setText("");
                mBinding.tvSlotFee.setHint(mActivity.getResources().getString(R.string.enter_fee));
            }

            int gstValue = eventViewHolderModel.getGstValue();
            if(gstValue == 0) {
                gstValue = 10;
            }
            //String gstTaxText = "Your fee (inc. "+gstValue+"% GST)";
            String gstTaxText = "Your fee (Pls add GST to price)";
            mBinding.tvSlotTax.setText(gstTaxText);
            mBinding.tvSlotFee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mEventInfoListener != null) {
                        mEventInfoListener.onEventFeeClickListener(parentPosition, slotPosition);
                    }
                }
            });
        }
    }

}
