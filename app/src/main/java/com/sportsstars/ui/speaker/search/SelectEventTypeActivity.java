package com.sportsstars.ui.speaker.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySelectEventTypeBinding;
import com.sportsstars.eventbus.BackNavigationEvent;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.speaker.EventList;
import com.sportsstars.network.response.speaker.EventListResponse;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.network.response.speaker.EventSubcategoryDetail;
import com.sportsstars.network.response.speaker.GuestSpeakerEvents;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import retrofit2.Call;

public class SelectEventTypeActivity extends BaseActivity {

    private ActivitySelectEventTypeBinding mBinding;
    private ImageAdapter mEventImageAdapter;
    private ArrayList<EventViewHolderModel> mGlobalEventList = new ArrayList<>();

    private boolean isFromSpeakerFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_event_type);

        getIntentData();
        initViews();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        if(Utils.isNetworkAvailable()) {
            getEventList();
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SPEAKER_FILTER)) {
                isFromSpeakerFilter = bundle.getBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(BackNavigationEvent backNavigationEvent) {
        if(backNavigationEvent.isBackNavigated()) {
            finish();
        }
    }

    private void initViews() {
        mBinding.layoutTop.headerId.tvTitle.setText(R.string.select_your_events);
        mBinding.etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.getInstance().navigateToActivityForResult(
                        SelectEventTypeActivity.this,
                        SearchEventTypeActivity.class,
                        Constants.REQUEST_CODE.REQUEST_CODE_EVENT_TYPE);
            }
        });
        mBinding.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(position);
                if(eventViewHolderModel == null) {
                    showToast(getResources().getString(R.string.selected_event_is_invalid));
                    return;
                }
                if(eventViewHolderModel.getId() == 0) {
                    showToast(getResources().getString(R.string.selected_event_is_invalid));
                    return;
                }
                if(TextUtils.isEmpty(eventViewHolderModel.getName())) {
                    showToast(getResources().getString(R.string.selected_event_is_invalid));
                    return;
                }

                if(isFromSpeakerFilter) {
                    goBackToFilterScreen(eventViewHolderModel);
                } else {
                    launchSpeakerSportsScreen(eventViewHolderModel);
                }
            }
        });
    }

    private void getEventList() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.getEventList().enqueue(new BaseCallback<EventListResponse>(SelectEventTypeActivity.this) {
            @Override
            public void onSuccess(EventListResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null && response.getResult().size() > 0) {
                            generateEventList(response.getResult());
                            setEventList();
                        } else {

                        }
                        //showToast("Getting event list successfully !!!");
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(SelectEventTypeActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<EventListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SelectEventTypeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void generateEventList(ArrayList<EventList> localEventList) {
        if(localEventList != null && localEventList.size() > 0) {
            for(int i = 0; i < localEventList.size(); i++) {
                EventList eventList = localEventList.get(i);
                if(eventList != null) {
                    EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
                    eventViewHolderModel.setId(eventList.getId());
                    eventViewHolderModel.setName(eventList.getName());
                    eventViewHolderModel.setDescription(eventList.getDescription());
                    eventViewHolderModel.setEventLogo(eventList.getEventLogo());
                    eventViewHolderModel.setBackgroundLogo(eventList.getBackgroundLogo());
                    eventViewHolderModel.setStatus(eventList.getStatus());
                    eventViewHolderModel.setGstValue(eventList.getGstValue());
                    eventViewHolderModel.setEventSlot(eventList.getEventSlot());
                    eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_EVENT);
                    eventViewHolderModel.setChecked(isEventPriceAvailable(eventList));
                    eventViewHolderModel.setEventCategories(eventList.getEventCategories());

                    if(eventList.getSelectedEventSubcategories() != null
                            && eventList.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventList.getSelectedEventSubcategories()) {
                            eventSubcategory.setSelected(true);
                        }
                    }

                    eventViewHolderModel.setSelectedEventSubcategories(eventList.getSelectedEventSubcategories());

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
                        for(int z = 0; z < eventViewHolderModel.getEventCategories().getEventSubcategories().size(); z++) {
                            EventSubcategory eventSubcategory
                                    = eventViewHolderModel.getEventCategories().getEventSubcategories().get(z);
                            eventSubcategory.setEventSubcategoryId(eventSubcategory.getId());

                            EventSubcategoryDetail eventSubcategoryDetail = new EventSubcategoryDetail();
                            eventSubcategoryDetail.setId(eventSubcategory.getId());
                            eventSubcategoryDetail.setName(eventSubcategory.getName());

                            eventSubcategory.setEventSubcategoryDetail(eventSubcategoryDetail);
                        }
                    }

                    if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventViewHolderModel.getSelectedEventSubcategories()) {
                            eventSubcategory.setId(eventSubcategory.getEventSubcategoryId());
                            eventSubcategory.setSelected(true);
                        }
                    }

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {

                        if (eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {

                            for (EventSubcategory eventSubcategoryGlobal :
                                    eventViewHolderModel.getEventCategories().getEventSubcategories()) {
                                for (EventSubcategory eventSubcategorySelected :
                                        eventViewHolderModel.getSelectedEventSubcategories()) {
                                    if (eventSubcategoryGlobal.getEventSubcategoryId()
                                            == eventSubcategorySelected.getEventSubcategoryId()) {
                                        eventSubcategoryGlobal.setSelected(true);
                                    }
                                }
                            }

                        }

                    }

                    removeDuplicateEventSubcategories(eventViewHolderModel);

                    mGlobalEventList.add(eventViewHolderModel);
                }
            }
        }
    }

    private void removeDuplicateEventSubcategories(EventViewHolderModel eventViewHolderModel) {
        LinkedHashSet<EventSubcategory> duplicateRemovalHashSet = new LinkedHashSet<>();
        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
            duplicateRemovalHashSet.addAll(eventViewHolderModel.getSelectedEventSubcategories());
            if(duplicateRemovalHashSet.size() > 0) {
                Log.d("speaker_event_log", " duplicate removal set size : "
                        + duplicateRemovalHashSet.size()
                        + " for " + eventViewHolderModel.getName());
                eventViewHolderModel.getSelectedEventSubcategories().clear();
                eventViewHolderModel.getSelectedEventSubcategories().addAll(duplicateRemovalHashSet);
            }
        }
    }

    private boolean isEventPriceAvailable(EventList eventList) {
        boolean isChecked = false;
        int slotCount = 0;
        if(eventList.getEventSlot() != null && eventList.getEventSlot().size() > 0) {
            for(int j = 0; j < eventList.getEventSlot().size(); j++) {
                GuestSpeakerEvents guestSpeakerEvents = eventList.getEventSlot().get(j).getGuestSpeakerEvents();
                if(guestSpeakerEvents == null) {
                    isChecked = false;
                    //break;
                } else {
                    isChecked = true;
                    slotCount++;
                }
            }

            if(slotCount > 0) {
                isChecked = true;
            } else {
                isChecked = false;
            }
        }
        return isChecked;
    }

    private void setEventList() {
        mEventImageAdapter = new ImageAdapter(SelectEventTypeActivity.this, mGlobalEventList);
        mBinding.gridView.setAdapter(mEventImageAdapter);
    }

    @Override
    public String getActivityName() {
        return "SelectEventType";
    }

    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        private ArrayList<EventViewHolderModel> eventsList;

        public ImageAdapter(Context c, ArrayList<EventViewHolderModel> events) {
            mContext = c;
            this.eventsList = events;
        }

        public int getCount() {
            return eventsList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (view == null) {
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.item_events_grid, null);
            }
            EventViewHolderModel eventViewHolderModel = eventsList.get(position);
            TextView tvEventName = view.findViewById(R.id.tv_event_name);
            ImageView ivEventCover = view.findViewById(R.id.iv_event_cover);

            tvEventName.setText(eventViewHolderModel.getName());

            if(!TextUtils.isEmpty(eventViewHolderModel.getBackgroundLogo())) {
                Picasso.with(mContext)
                        .load(eventViewHolderModel.getBackgroundLogo())
                        .into((ivEventCover));
            }

            return view;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_EVENT_TYPE) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    EventViewHolderModel eventViewHolderModel = data.getParcelableExtra(Constants.BundleKey.EVENT_VIEW_HOLDER);
                    int eventId = data.getIntExtra(Constants.BundleKey.EVENT_ID, 0);
                    String eventName = data.getStringExtra(Constants.BundleKey.EVENT_NAME);

                    if(eventId == 0) {
                        showToast(getResources().getString(R.string.selected_event_is_invalid));
                        return;
                    }
                    if(TextUtils.isEmpty(eventName)) {
                        showToast(getResources().getString(R.string.selected_event_is_invalid));
                        return;
                    }

                    if(isFromSpeakerFilter) {
                        goBackToFilterScreen(eventViewHolderModel);
                    } else {
                        launchSpeakerSportsScreen(eventViewHolderModel);
                    }
                }
            }
        }
    }

    private void goBackToFilterScreen(EventViewHolderModel eventViewHolderModel) {
        if(eventViewHolderModel != null) {
            Intent intent = new Intent();
            intent.putExtra(Constants.BundleKey.EVENT_VIEW_HOLDER, eventViewHolderModel);
            intent.putExtra(Constants.BundleKey.EVENT_ID, eventViewHolderModel.getId());
            intent.putExtra(Constants.BundleKey.EVENT_NAME, eventViewHolderModel.getName());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void launchSpeakerSportsScreen(EventViewHolderModel eventViewHolderModel) {
        if(eventViewHolderModel != null) {
            Bundle bundleSpeaker = new Bundle();
            bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
            bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
            bundleSpeaker.putParcelable(Constants.BundleKey.EVENT_VIEW_HOLDER, eventViewHolderModel);
            bundleSpeaker.putInt(Constants.BundleKey.EVENT_ID, eventViewHolderModel.getId());
            bundleSpeaker.putString(Constants.BundleKey.EVENT_NAME, eventViewHolderModel.getName());
            bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
            Navigator.getInstance().navigateToActivityWithData(
                    SelectEventTypeActivity.this, SelectSpeakerSportsActivity.class, bundleSpeaker);
        }
    }

}
