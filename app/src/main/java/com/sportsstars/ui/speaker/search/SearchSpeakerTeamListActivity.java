package com.sportsstars.ui.speaker.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchSpeakerTeamListBinding;
import com.sportsstars.interfaces.AddTeamItemClick;
import com.sportsstars.model.Team;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.coachprofile.TeamListRequest;
import com.sportsstars.network.response.coachprofile.TeamsListBaseResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.AddTeamActivity;
import com.sportsstars.ui.coach.adapter.AddTeamRecyclerViewAdapter;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class SearchSpeakerTeamListActivity extends BaseActivity implements View.OnClickListener, AddTeamItemClick {

    private ActivitySearchSpeakerTeamListBinding mBinding;

    private ArrayList<Integer> mSportIdsList;
    private ArrayList<Team> mSelectedTeamsList;
    private List<Team> mTeamsList;

    private AddTeamRecyclerViewAdapter mAddTeamAdapter;

    private int mTotalAPIPages;
    private int mCurrentAPIPage;
    private boolean mIsFetchingDataFromAPI;

    private boolean isLaunchingFirstTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_speaker_team_list);

        isLaunchingFirstTime = true;
        getIntentData();
        initViews();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.SPORT_IDS_LIST)) {
                mSportIdsList = bundle.getIntegerArrayList(Constants.BundleKey.SPORT_IDS_LIST);
            }
            if(getIntent().hasExtra(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST)) {
                mSelectedTeamsList = bundle.getParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST);
            }
        }
    }

    private void initViews() {
        Utils.setTransparentTheme(this);

        mBinding.header.tvTitle.setText(getString(R.string.please_select_your_teams));
        mBinding.header.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.header.ivBack.setVisibility(View.GONE);

        mBinding.bSave.setOnClickListener(this);

        setupTeamsList();
    }

    private void setupTeamsList() {
        if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
            //Check if teams were already selected previously by the user
            if (mTeamsList != null && mTeamsList.size() > 0) {
                for (Team team : mTeamsList) {
                    if (mSelectedTeamsList.contains(team)) {
                        team.setIsSelected(true);
                    }
                }
            }
        } else {
            mSelectedTeamsList = new ArrayList<>();
        }

        if (mTeamsList == null) {
            mTeamsList = new ArrayList<>();
            mCurrentAPIPage = 1;
            if(Utils.isNetworkAvailable()) {
                fetchTeamsFromAPI();
            } else {
                showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
            }
        }

        mAddTeamAdapter = new AddTeamRecyclerViewAdapter(mTeamsList, this, this);
        mBinding.rvTeams.setAdapter(mAddTeamAdapter);

        setupPagination();
    }

    private void setupPagination() {

        mBinding.rvTeams.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) mBinding.rvTeams.getLayoutManager();

                int currentlyVisibleCount = layoutManager.getChildCount();
                int visiblePreviouslyCount = layoutManager.findFirstVisibleItemPosition();
                int totalItemsInList = layoutManager.getItemCount();

                if (mCurrentAPIPage < mTotalAPIPages
                        && !mIsFetchingDataFromAPI
                        && (currentlyVisibleCount + visiblePreviouslyCount <= totalItemsInList)) {
                    if (isLaunchingFirstTime) {
                        isLaunchingFirstTime = false;
                        return;
                    }
                    mCurrentAPIPage++;
                    Log.d("AddTeamLog", "AddTeam - API HIT PAGINATION - mCurrentAPIPage : " + mCurrentAPIPage);
                    if(Utils.isNetworkAvailable()) {
                        fetchTeamsFromAPI();
                    } else {
                        showSnackbarFromTop(SearchSpeakerTeamListActivity.this, getResources().getString(R.string.no_internet));
                    }
                }
            }
        });

    }

    private void fetchTeamsFromAPI() {
        Log.d("AddTeamLog", "AddTeam - API HIT");
        mIsFetchingDataFromAPI = true;
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);

        // ------ New Change - As per client improvement calling favourite teams API , no check is required for learner or a coach. ------//

        TeamListRequest teamListRequest = new TeamListRequest();
        teamListRequest.setSportId(mSportIdsList);

        webServices.getTeamsList(teamListRequest, mCurrentAPIPage).enqueue(new BaseCallback<TeamsListBaseResponse>(this) {
            @Override
            public void onSuccess(TeamsListBaseResponse response) {
                mIsFetchingDataFromAPI = false;
                if (response != null && response.getResult() != null) {
                    mTotalAPIPages = response.getResult().getLastPage();
                    if (response.getResult().getData() != null && response.getResult().getData().size() > 0) {
                        for (Team team : response.getResult().getData()) {
                            if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
                                if (mSelectedTeamsList.contains(team)) {
                                    team.setIsSelected(true);
                                }
                            }
                        }
                        mTeamsList.addAll(response.getResult().getData());
                        mAddTeamAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFail(Call<TeamsListBaseResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                hideProgressBar();
            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_save:
                if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
                    if(mSelectedTeamsList.size() == 1 && mSelectedTeamsList.get(0).getId() == 0
                            && TextUtils.isEmpty(mSelectedTeamsList.get(0).getName())) {
                        sendEmptySelectedList();
                        //showSnackbarFromTop(SearchSpeakerTeamListActivity.this, getResources().getString(R.string.please_select_your_fav_teams));
                    } else {
                        sendSelectedTeamsList();
                    }
                } else {
                    sendEmptySelectedList();
                    //showSnackbarFromTop(SearchSpeakerTeamListActivity.this, getResources().getString(R.string.please_select_your_fav_teams));
                }
                break;
        }
    }

    private void sendSelectedTeamsList() {
        try {
            if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 1) {
                if(mSelectedTeamsList.get(0).getId() == 0 && TextUtils.isEmpty(mSelectedTeamsList.get(0).getName())) {
                    mSelectedTeamsList.remove(0);
                }
            }

            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST, (ArrayList<? extends Parcelable>) mSelectedTeamsList);
            setResult(RESULT_OK, intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEmptySelectedList() {
        try {
            if(mSelectedTeamsList != null) {
                mSelectedTeamsList.clear();
            }
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST, (ArrayList<? extends Parcelable>) mSelectedTeamsList);
            setResult(RESULT_OK, intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTeamSelected(Team team) {
        boolean isSelected;
        if (mSelectedTeamsList.contains(team)) {
            //remove from selected teams list
            mSelectedTeamsList.remove(team);
            isSelected = false;
        } else {
            //add to selected teams list
            mSelectedTeamsList.add(team);
            isSelected = true;
        }

        for (Team currentTeam : mTeamsList) {
            if (team.equals(currentTeam))
                currentTeam.setIsSelected(isSelected);
        }

        if(mAddTeamAdapter != null) {
            mAddTeamAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public String getActivityName() {
        return "SearchSpeakerTeamList";
    }

}
