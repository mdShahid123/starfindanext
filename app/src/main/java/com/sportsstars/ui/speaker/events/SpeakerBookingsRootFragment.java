/*
 * Copyright © 2018 StarFinda. All rights reserved.
 * Developed by Appster.
 */

package com.sportsstars.ui.speaker.events;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentSpeakerBookingsRootBinding;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.speaker.SpeakerHomeActivity;

import java.lang.ref.WeakReference;

public class SpeakerBookingsRootFragment extends BaseFragment {

    private FragmentSpeakerBookingsRootBinding mBinding;
    private FragmentManager mFragmentManagerSpeaker;
    private WeakReference<SpeakerHomeActivity> mActivity;

    public static SpeakerBookingsRootFragment newInstance() {
        return new SpeakerBookingsRootFragment();
    }

    @Override
    public String getFragmentName() {
        return "SpeakerBookings";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((SpeakerHomeActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_speaker_bookings_root, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        mFragmentManagerSpeaker = getActivity().getSupportFragmentManager();

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
                getResources().getString(R.string.font_bebas_neue));
        mBinding.radioBtn1.setTypeface(font);
        mBinding.radioBtn2.setTypeface(font);

        mBinding.radioBtn1.setChecked(true);
        loadFragment(0, true);
        mBinding.radioGroupHeader.setOnCheckedChangeListener(radioListener);
    }

    private RadioGroup.OnCheckedChangeListener radioListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                switch (checkedId) {
                    case R.id.radio_btn_1:
                        loadFragment(0, true);
                        break;
                    case R.id.radio_btn_2:
                        loadFragment(1, true);
                        break;
                }
            }
        }
    };

    private void loadFragment(int position, boolean isReload) {
        switch (position) {
            case 0:
                SpeakerEventListFragment speakerEventListFragment = SpeakerEventListFragment.newInstance();
                if (mFragmentManagerSpeaker != null) {
                    mFragmentManagerSpeaker.beginTransaction().replace(R.id.frame_layout, speakerEventListFragment).commit();
                }
                if (mActivity != null) {
                    mActivity.get().setUpHeaderForCoachHome(mActivity.get().mBinder.headerId,
                            getResources().getString(R.string.my_bookings), "");
                }
                break;
            case 1:
                SpeakerPanelListFragment speakerPanelListFragment = SpeakerPanelListFragment.newInstance();
                if (mFragmentManagerSpeaker != null) {
                    mFragmentManagerSpeaker.beginTransaction().replace(R.id.frame_layout, speakerPanelListFragment).commit();
                }
                if (mActivity != null) {
                    mActivity.get().setUpHeaderForCoachHome(mActivity.get().mBinder.headerId,
                            getResources().getString(R.string.my_bookings), "");
                }
                break;
        }
    }

}
