package com.sportsstars.ui.speaker;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemEventsListViewHolderBinding;
import com.sportsstars.databinding.ItemSpecialitiesViewHolderBinding;
import com.sportsstars.interfaces.EventInfoListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.ui.speaker.vh.EventListViewHolder;
import com.sportsstars.ui.speaker.vh.SpecialitiesViewHolder;
import com.sportsstars.util.Constants;

import java.util.ArrayList;

public class EventInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private ArrayList<EventViewHolderModel> mGlobalEventList;
    private Activity mActivity;
    private EventInfoListener mEventInfoListener;

    public EventInfoAdapter(Activity activity, ArrayList<EventViewHolderModel> globalEventList, EventInfoListener eventInfoListener) {
        mActivity = activity;
        mGlobalEventList = globalEventList;
        mEventInfoListener = eventInfoListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == Constants.VIEW_TYPE.VIEW_SPECIALITIES) {
            viewHolder = new SpecialitiesViewHolder(
                    (ItemSpecialitiesViewHolderBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.item_specialities_view_holder, parent, false), mActivity, mEventInfoListener);
        } else {
            viewHolder = new EventListViewHolder(
                    (ItemEventsListViewHolderBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.item_events_list_view_holder, parent, false), mActivity, mEventInfoListener);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(position);
        if (holder instanceof SpecialitiesViewHolder) {
            ((SpecialitiesViewHolder) holder).bindData(eventViewHolderModel, position);
        } else {
            ((EventListViewHolder) holder).bindData(eventViewHolderModel, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mGlobalEventList.get(position).getViewType() == Constants.VIEW_TYPE.VIEW_SPECIALITIES) {
            return Constants.VIEW_TYPE.VIEW_SPECIALITIES;
        } else {
            return Constants.VIEW_TYPE.VIEW_EVENT;
        }
    }

    @Override
    public int getItemCount() {
        return mGlobalEventList.size();
    }

}
