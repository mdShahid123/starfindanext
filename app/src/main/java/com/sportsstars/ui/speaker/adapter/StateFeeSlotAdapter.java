package com.sportsstars.ui.speaker.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemTravelSlotViewHolderBinding;
import com.sportsstars.interfaces.TravelInfoListener;
import com.sportsstars.model.TravelViewHolderModel;
import com.sportsstars.network.response.speaker.StateCharges;
import com.sportsstars.ui.speaker.vh.StateSlotViewHolder;

import java.util.ArrayList;

public class StateFeeSlotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<StateCharges> mStateSlots;
    private Activity mActivity;
    private TravelViewHolderModel mTravelViewHolderModel;
    private int mParentPosition;
    private TravelInfoListener mTravelInfoListener;

    public StateFeeSlotAdapter(Activity activity, TravelViewHolderModel travelViewHolderModel, int parentPosition,
                            ArrayList<StateCharges> stateSlots, TravelInfoListener travelInfoListener) {
        mActivity = activity;
        mTravelViewHolderModel = travelViewHolderModel;
        mParentPosition = parentPosition;
        mStateSlots = stateSlots;
        mTravelInfoListener = travelInfoListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = new StateSlotViewHolder(
                (ItemTravelSlotViewHolderBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_travel_slot_view_holder, parent, false), mActivity, mTravelInfoListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((StateSlotViewHolder) holder).bindData(mTravelViewHolderModel, mParentPosition, mStateSlots.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mStateSlots.size();
    }

}
