package com.sportsstars.ui.speaker.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appster.chatlib.constants.AppConstants;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.chat.chat.ImagePreviewActivity;
import com.sportsstars.databinding.ActivitySpeakerProfileBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.interfaces.RecyclerPageChangeListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.speaker.EventList;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.network.response.speaker.EventSubcategoryDetail;
import com.sportsstars.network.response.speaker.GuestSpeakerEvents;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.ImageAdapter;
import com.sportsstars.ui.auth.adapter.LinePagerIndicatorDecoration;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.ui.speaker.adapter.SpeakerProfileEventListAdapter;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.sportsstars.widget.SimpleDividerItemDecoration;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import retrofit2.Call;

public class SpeakerProfileActivity extends BaseActivity implements View.OnClickListener,
        ImageAdapter.MediaClickCallBack, RecyclerPageChangeListener, ListItemClickListener {

    private ActivitySpeakerProfileBinding mBinding;
    private UserProfileModel mUserProfileModel;
    private ArrayList<EventViewHolderModel> mGlobalEventList = new ArrayList<>();
    private SpeakerProfileEventListAdapter mSpeakerProfileEventListAdapter;
    private boolean isLoading = false;

    private int position;
    private int speakerId;
    private boolean isSelected;
    //private boolean isAwaitingOrDeclined;

    private int eventBookingStatus = -1;
    private int speakerStatus = -1;
    private int isSpeakerPaid = -1;
    private boolean isComingFromSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_speaker_profile);

        isLoading = true;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            position = bundle.getInt(Constants.BundleKey.POSITION, 0);
            speakerId = bundle.getInt(Constants.BundleKey.SPEAKER_ID, 0);
            isSelected = bundle.getBoolean(Constants.BundleKey.IS_SELECTED, false);
            //isAwaitingOrDeclined = bundle.getBoolean(Constants.BundleKey.IS_AWAITING_OR_DECLINED, false);

            if(getIntent().hasExtra(Constants.BundleKey.EVENT_BOOKING_STATUS)) {
                eventBookingStatus = bundle.getInt(Constants.BundleKey.EVENT_BOOKING_STATUS, -1);
            }
            if(getIntent().hasExtra(Constants.BundleKey.SPEAKER_STATUS)) {
                speakerStatus = bundle.getInt(Constants.BundleKey.SPEAKER_STATUS, -1);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_SPEAKER_PAID)) {
                isSpeakerPaid = bundle.getInt(Constants.BundleKey.IS_SPEAKER_PAID, -1);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SEARCH)) {
                isComingFromSearch = bundle.getBoolean(Constants.BundleKey.IS_FROM_SEARCH, false);
            }
        }

        if(Utils.isNetworkAvailable()) {
            if(speakerId == 0) {
                showSnackbarFromTop(SpeakerProfileActivity.this, getResources().getString(R.string.speaker_profile_not_exists));
                return;
            }

            mBinding.rvEventList.setVisibility(View.GONE);
            mBinding.tvNoEvents.setVisibility(View.VISIBLE);

            mBinding.mediaContainer.setVisibility(View.GONE);
            mBinding.tvPagesMedia.setVisibility(View.GONE);
            mBinding.tvMediaHeader.setVisibility(View.GONE);

            mBinding.ivChat.setVisibility(View.VISIBLE);
            mBinding.ivChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Utils.isNetworkAvailable()) {
                        startChat();
                    } else {
                        showSnackbarFromTop(SpeakerProfileActivity.this, getResources().getString(R.string.no_internet));
                    }
                }
            });

            getSpeakerDetails(speakerId);
        } else {
            mBinding.rvEventList.setVisibility(View.GONE);
            mBinding.tvNoEvents.setVisibility(View.VISIBLE);
            showSnackbarFromTop(SpeakerProfileActivity.this, getResources().getString(R.string.no_internet));
        }

        if(isSelected) {
            mBinding.btnSelectMe.setText(getResources().getString(R.string.deselect_me));
        } else {
            mBinding.btnSelectMe.setText(getResources().getString(R.string.select_me));
        }

        if(eventBookingStatus == Utils.STATUS_BOOKING.CANCELLED) {
            mBinding.ivChat.setVisibility(View.GONE);
            mBinding.btnSelectMe.setVisibility(View.GONE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.COMPLETED) {
            mBinding.ivChat.setVisibility(View.GONE);
            mBinding.btnSelectMe.setVisibility(View.GONE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.ONGOING) {
            mBinding.ivChat.setVisibility(View.VISIBLE);
            mBinding.btnSelectMe.setVisibility(View.GONE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.UPCOMING) {
            if(isSpeakerPaid == 1) {
                mBinding.ivChat.setVisibility(View.VISIBLE);
            } else {
                mBinding.ivChat.setVisibility(View.GONE);
            }
            mBinding.btnSelectMe.setVisibility(View.GONE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.PAYMENT_PENDING) {
            mBinding.ivChat.setVisibility(View.VISIBLE);
            mBinding.btnSelectMe.setVisibility(View.VISIBLE);
        } else if(eventBookingStatus == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
            if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
                mBinding.btnSelectMe.setVisibility(View.GONE);
                mBinding.ivChat.setVisibility(View.GONE);
            } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
                mBinding.btnSelectMe.setVisibility(View.VISIBLE);
                mBinding.ivChat.setVisibility(View.VISIBLE);
            } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
                mBinding.btnSelectMe.setVisibility(View.GONE);
                mBinding.ivChat.setVisibility(View.GONE);
            } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.NO_RESPONSE) {
                mBinding.btnSelectMe.setVisibility(View.GONE);
                mBinding.ivChat.setVisibility(View.GONE);
            }
        }

        if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
            mBinding.ivChat.setVisibility(View.GONE);
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
            if(isSpeakerPaid == 1) {
                mBinding.ivChat.setVisibility(View.VISIBLE);
            } else {
                mBinding.ivChat.setVisibility(View.GONE);
            }
        } else if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
            mBinding.ivChat.setVisibility(View.GONE);
        }

        if(speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED
                || speakerStatus == Constants.SPEAKER_AVAILABILITY_STATUS.NO_RESPONSE) {
            mBinding.ivChat.setVisibility(View.GONE);
            mBinding.btnSelectMe.setVisibility(View.GONE);
        }

        if(isComingFromSearch) {
            mBinding.ivChat.setVisibility(View.GONE);
            mBinding.btnSelectMe.setVisibility(View.VISIBLE);
        }

        if(!PreferenceUtil.getIsLogin()) {
            mBinding.ivChat.setVisibility(View.GONE);
            mBinding.btnSelectMe.setVisibility(View.VISIBLE);
        }

    }

    private void startChat() {
        if(mUserProfileModel != null) {
            String userName = mUserProfileModel.getJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
//                intent.putExtra(AppConstants.KEY_TO_USERNAME, userName);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, mUserProfileModel.getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, mUserProfileModel.getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, mUserProfileModel.getJid());
                startActivity(intent);
            } else {
                showToast("Username of this profile not found to initiate chat.");
                //com.appster.chatlib.utils.Utils.showToast(this, "enter username");
            }
        }
    }

    private void getSpeakerDetails(int speakerId) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.getSpeakerDetails(speakerId).enqueue(new BaseCallback<GetProfileResponse>(SpeakerProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if(response != null && response.getStatus() == 1) {
                    //showToast("getting speaker details data");
                    mUserProfileModel = response.getResult();
                    bindDataSet();
                    initViews();
                    setListeners();
                    if(response.getResult() != null
                            && response.getResult().getGuestSpeakerEventList() != null
                            && response.getResult().getGuestSpeakerEventList().size() > 0) {
                        getEventList(response.getResult().getGuestSpeakerEventList());
                    } else {
                        mBinding.rvEventList.setVisibility(View.GONE);
                        mBinding.tvNoEvents.setVisibility(View.VISIBLE);
                    }
                } else {
                    if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(SpeakerProfileActivity.this, response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(baseResponse != null && baseResponse.getMessage() != null
                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                    showSnackbarFromTop(SpeakerProfileActivity.this, baseResponse.getMessage());
                }
            }
        });
    }

    private void getEventList(ArrayList<EventList> eventList) {
        if(eventList != null && eventList.size() > 0) {
            if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
                mGlobalEventList.clear();
            }
            generateEventList(eventList);
            mBinding.rvEventList.setVisibility(View.VISIBLE);
            mBinding.tvNoEvents.setVisibility(View.GONE);
            setEventList();
        } else {
            mBinding.rvEventList.setVisibility(View.GONE);
            mBinding.tvNoEvents.setVisibility(View.VISIBLE);
        }
    }

    private void setListeners() {
        mBinding.btnShareMe.setOnClickListener(this);
        mBinding.btnSelectMe.setOnClickListener(this);
    }

    private void bindDataSet() {
        mBinding.setSpeakerInfo(mUserProfileModel);
    }

    private void initViews() {
        setProfilePic();
        setSportIcon();
        setGender();
        setTravelType();
        //setEventList();
        //setCerView();
        setTeamView();
        //setLocView();
        setHonourView();
        //setSessionView();
        //setSpecialityView();
        setMediaView();
    }

    private void setEventList() {
        mSpeakerProfileEventListAdapter = new SpeakerProfileEventListAdapter(this, mGlobalEventList, this);
        //mBinding.rvEventInfo.setHasFixedSize(true);
        mBinding.rvEventList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvEventList.addItemDecoration(new SimpleDividerItemDecoration(SpeakerProfileActivity.this));
        mBinding.rvEventList.setAdapter(mSpeakerProfileEventListAdapter);
    }

    private void setHonourView() {
        mBinding.llHonour.removeAllViews();
        if (!StringUtils.isNullOrEmpty(mUserProfileModel.getAccolades())) {
            String[] items = mUserProfileModel.getAccolades().split("\\s*,\\s*");
            drawView(items, mBinding.llHonour);
        }
    }

    private void setTeamView() {
        mBinding.llTeams.removeAllViews();
        List<Team> list = mUserProfileModel.getTeamPlayedFor();
        for (Team team : list) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
            ((TextView) view.findViewById(R.id.tvSpecList)).setText(team.getName());
            mBinding.llTeams.addView(view);
        }
    }

    private void drawView(String[] items, LinearLayout container) {
        for (String item : items) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
            ((TextView) view.findViewById(R.id.tvSpecList)).setText(item);
            container.addView(view);
        }
    }

    private void setMediaView() {

        if(mUserProfileModel != null) {
            if(mUserProfileModel.getMediaUrl() != null) {
                Log.d("speaker_log", "setMediaView - mUserProfileModel.getMediaUrl().size() : "
                        + mUserProfileModel.getMediaUrl().size());
            } else {
                Log.d("speaker_log", "setMediaView - mUserProfileModel.getMediaUrl() is null");
            }
            if(mUserProfileModel.getMediaUrl() != null && mUserProfileModel.getMediaUrl().size() > 0) {
                mBinding.mediaContainer.setVisibility(View.VISIBLE);
                mBinding.tvPagesMedia.setVisibility(View.VISIBLE);
                mBinding.tvMediaHeader.setVisibility(View.VISIBLE);
            } else {
                mBinding.mediaContainer.setVisibility(View.GONE);
                mBinding.tvPagesMedia.setVisibility(View.GONE);
                mBinding.tvMediaHeader.setVisibility(View.GONE);
            }

            ImageAdapter imageAdapter = new ImageAdapter(mUserProfileModel.getMediaUrl(),this);
            mBinding.mediaContainer.setHasFixedSize(true);
            mBinding.mediaContainer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mBinding.mediaContainer.setAdapter(imageAdapter);

            // add pager behavior
            PagerSnapHelper snapHelper = new PagerSnapHelper();
            mBinding.mediaContainer.setOnFlingListener(null);
            snapHelper.attachToRecyclerView(mBinding.mediaContainer);

            mBinding.mediaContainer.addItemDecoration(new LinePagerIndicatorDecoration(this));

            if(mUserProfileModel.getMediaUrl().size() > 0) {
                mBinding.tvPagesMedia.setText("1/"+mUserProfileModel.getMediaUrl().size());
            }
        }

    }

    private void generateEventList(ArrayList<EventList> localEventList) {
        if(localEventList != null && localEventList.size() > 0) {
            for(int i = 0; i < localEventList.size(); i++) {
                EventList eventList = localEventList.get(i);
                if(eventList != null) {
//                    boolean isEventPriceAvailable = isEventPriceAvailable(eventList);
//                    if(isEventPriceAvailable) {
//                        EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
//                        eventViewHolderModel.setId(eventList.getId());
//                        eventViewHolderModel.setName(eventList.getName());
//                        eventViewHolderModel.setDescription(eventList.getDescription());
//                        eventViewHolderModel.setEventLogo(eventList.getEventLogo());
//                        eventViewHolderModel.setStatus(eventList.getStatus());
//                        eventViewHolderModel.setGstValue(eventList.getGstValue());
//                        eventViewHolderModel.setEventSlot(eventList.getEventSlot());
//                        eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_EVENT);
//                        eventViewHolderModel.setChecked(isEventPriceAvailable);
//
//                        mGlobalEventList.add(eventViewHolderModel);
//                    }

                    EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
                    eventViewHolderModel.setId(eventList.getId());
                    eventViewHolderModel.setName(eventList.getName());
                    eventViewHolderModel.setDescription(eventList.getDescription());
                    eventViewHolderModel.setEventLogo(eventList.getEventLogo());
                    eventViewHolderModel.setStatus(eventList.getStatus());
                    eventViewHolderModel.setGstValue(eventList.getGstValue());
                    eventViewHolderModel.setEventSlot(eventList.getEventSlot());
                    eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_EVENT);
                    eventViewHolderModel.setChecked(true);
                    eventViewHolderModel.setEventCategories(eventList.getEventCategories());

                    if(eventList.getSelectedEventSubcategories() != null
                            && eventList.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventList.getSelectedEventSubcategories()) {
                            eventSubcategory.setSelected(true);
                        }
                    }

                    eventViewHolderModel.setSelectedEventSubcategories(eventList.getSelectedEventSubcategories());

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
                        for(int z = 0; z < eventViewHolderModel.getEventCategories().getEventSubcategories().size(); z++) {
                            EventSubcategory eventSubcategory
                                    = eventViewHolderModel.getEventCategories().getEventSubcategories().get(z);
                            eventSubcategory.setEventSubcategoryId(eventSubcategory.getId());

                            EventSubcategoryDetail eventSubcategoryDetail = new EventSubcategoryDetail();
                            eventSubcategoryDetail.setId(eventSubcategory.getId());
                            eventSubcategoryDetail.setName(eventSubcategory.getName());

                            eventSubcategory.setEventSubcategoryDetail(eventSubcategoryDetail);
                        }
                    }

                    if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventViewHolderModel.getSelectedEventSubcategories()) {
                            eventSubcategory.setId(eventSubcategory.getEventSubcategoryId());
                            eventSubcategory.setSelected(true);
                        }
                    }

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {

                        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {

                            for(EventSubcategory eventSubcategoryGlobal :
                                    eventViewHolderModel.getEventCategories().getEventSubcategories()) {
                                for(EventSubcategory eventSubcategorySelected :
                                        eventViewHolderModel.getSelectedEventSubcategories()) {
                                    if(eventSubcategoryGlobal.getEventSubcategoryId()
                                            == eventSubcategorySelected.getEventSubcategoryId()) {
                                        eventSubcategoryGlobal.setSelected(true);
                                        eventSubcategorySelected.setSelected(true);
                                    }
                                }
                            }

                        }

                    }

                    removeDuplicateEventSubcategories(eventViewHolderModel);

                    mGlobalEventList.add(eventViewHolderModel);
                }
            }
            int listSize = mGlobalEventList.size();
            Log.d("speaker_log", "SpeakerProfile - generateEventList - mGlobalEventList size : " + listSize);
        }
    }

    private void removeDuplicateEventSubcategories(EventViewHolderModel eventViewHolderModel) {
        LinkedHashSet<EventSubcategory> duplicateRemovalHashSet = new LinkedHashSet<>();
        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
            duplicateRemovalHashSet.addAll(eventViewHolderModel.getSelectedEventSubcategories());
            if(duplicateRemovalHashSet.size() > 0) {
                Log.d("speaker_event_log", " duplicate removal set size : "
                        + duplicateRemovalHashSet.size()
                        + " for " + eventViewHolderModel.getName());
                eventViewHolderModel.getSelectedEventSubcategories().clear();
                eventViewHolderModel.getSelectedEventSubcategories().addAll(duplicateRemovalHashSet);
            }
        }
    }

    private boolean isEventPriceAvailable(EventList eventList) {
        boolean isChecked = false;
        if(eventList.getEventSlot() != null && eventList.getEventSlot().size() > 0) {
            for(int j = 0; j < eventList.getEventSlot().size(); j++) {
                GuestSpeakerEvents guestSpeakerEvents = eventList.getEventSlot().get(j).getGuestSpeakerEvents();
                if(guestSpeakerEvents == null) {
                    isChecked = false;
                    break;
                } else {
                    isChecked = true;
                }
            }
        }
        return isChecked;
    }

    private void setSpecialityView() {
        mBinding.llEsp.removeAllViews();
        if (!StringUtils.isNullOrEmpty(mUserProfileModel.getSpecialities())) {
            String[] items = mUserProfileModel.getSpecialities().split("\\s*,\\s*");
            drawView(items, mBinding.llEsp);
        }
    }

    private void setGender() {
        mBinding.tvGender.setText(mUserProfileModel.getGender() == Constants.GENDER_TYPE.MALE ?
                getResources().getString(R.string.male) : getResources().getString(R.string.female));
    }

    private void setProfilePic() {
        if(mUserProfileModel == null) {
            return;
        }
        if(StringUtils.isNullOrEmpty(mUserProfileModel.getProfileImage())) {
            return;
        }

        Picasso.with(SpeakerProfileActivity.this)
                .load(mUserProfileModel.getProfileImage())
                .placeholder(R.drawable.ic_user_circle)
                .error(R.drawable.ic_user_circle)
                .into(mBinding.ivProfilePic,
                        new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                mBinding.ivBgHeader.setImageDrawable(mBinding.ivProfilePic.getDrawable());
                            }

                            @Override
                            public void onError() {

                            }
                        });
    }

    private void setSportIcon() {
        if(mUserProfileModel == null) {
            return;
        }
        if(mUserProfileModel.getSportDetail() == null) {
            return;
        }
        if(StringUtils.isNullOrEmpty(mUserProfileModel.getSportDetail().getIcon())) {
            return;
        }

        Picasso.with(SpeakerProfileActivity.this)
                .load(mUserProfileModel.getSportDetail().getIcon())
                .placeholder(R.drawable.cricket_icon)
                .error(R.drawable.cricket_icon)
                .into(mBinding.ivUserSport,
                        new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
    }

    private void setTravelType() {
        if(mUserProfileModel == null) {
            return;
        }
        if(mUserProfileModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.YES) {
            mBinding.tvTravelInterState.setText(getResources().getString(R.string.yes));
            mBinding.tvInterStateTravelType.setText(getResources().getString(R.string.yes_small));
        } else if(mUserProfileModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.NO) {
            mBinding.tvTravelInterState.setText(getResources().getString(R.string.no));
            mBinding.tvInterStateTravelType.setText(getResources().getString(R.string.no_small));
        } else if(mUserProfileModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.UNSELECTED) {
            mBinding.tvTravelInterState.setText(getResources().getString(R.string.no));
            mBinding.tvInterStateTravelType.setText(getResources().getString(R.string.no_small));
        }
    }

    @Override
    public String getActivityName() {
        return "SpeakerViewProfile";
    }

    @Override
    public void onPageChanged(int position) {
        if(mUserProfileModel.getMediaUrl().size() > 0) {
            mBinding.tvPagesMedia.setText((position+1)+"/"+mUserProfileModel.getMediaUrl().size());
        }
    }

    @Override
    public void onMediaClicked(int position) {
        if(mUserProfileModel.getMediaUrl().get(position).getType() == 7) {
            Bundle bundlePlay = new Bundle();
            bundlePlay.putString(Constants.BundleKey.VIDEO_URL, mUserProfileModel.getMediaUrl().get(position).getImageUrl());
            Navigator.getInstance().navigateToActivityWithData(this, PlayerActivity.class, bundlePlay);
        } else {
            Intent intent = new Intent(this, ImagePreviewActivity.class);
            intent.putExtra(ImagePreviewActivity.EXTRA_IMAGE_URL, mUserProfileModel.getMediaUrl().get(position).getImageUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == mBinding.btnShareMe) {
            shareSpeakerUrl();
            //showSnackbarFromTop(SpeakerProfileActivity.this, "Coming Soon");
        } else if(v == mBinding.btnSelectMe) {
            //showSnackbarFromTop(SpeakerProfileActivity.this, "Coming Soon");
            setResultOnSelection();
        }
    }

    private void shareSpeakerUrl() {

        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.DEEP_LINK
                        + mUserProfileModel.getUserId()
                        + "_" + Constants.USER_ROLE.GUEST_SPEAKER))
                .setDynamicLinkDomain(Constants.COACH_PROFILE_DEEP_LINKS.BASE_DOMAIN)
                // Open links with this app on Android
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder()
                                .setFallbackUrl(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.FALLBACK_URL))
                                .build())
                // Open links with this app on iOS
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder(Constants.COACH_PROFILE_DEEP_LINKS.IOS_PACKAGE_NAME)
                                .setFallbackUrl(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.FALLBACK_URL))
                                .setIpadFallbackUrl(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.FALLBACK_URL))
                                .setAppStoreId(Constants.COACH_PROFILE_DEEP_LINKS.APP_STORE_ID)
                                .setIpadBundleId(Constants.COACH_PROFILE_DEEP_LINKS.IPAD_BUNDLE_ID)
                                .setMinimumVersion(Constants.COACH_PROFILE_DEEP_LINKS.IOS_MIN_VERSION)
                                .build())
                //.setNavigationInfoParameters(new DynamicLink.NavigationInfoParameters.Builder().setForcedRedirectEnabled(true).build())
                .buildDynamicLink();

        Uri dynamicLinkUri = dynamicLink.getUri();
        String finalDeepLinkUrl;
        try {
            finalDeepLinkUrl = URLDecoder.decode(dynamicLinkUri.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LogUtils.LOGD("share_speaker", "Failed during decoding URL");
            return;
        } catch (Exception e) {
            LogUtils.LOGD("share_speaker", "Failed during decoding URL");
            return;
        }

        String completeMessage = "";
        String shareMessage = "Hey! Checkout StarFinda app. \n";
        String playStoreUrl = " Android : https://play.google.com/store/apps/details?id=com.starfinda";
        String appStoreUrl = "\n  iOS : https://itunes.apple.com/us/app/starfinda/id1394386832?ls=1&mt=8";

        completeMessage = shareMessage + playStoreUrl + appStoreUrl;

        Intent shareCoachUrlIntent = new Intent(Intent.ACTION_SEND);
        shareCoachUrlIntent.setType("text/plain");
        shareCoachUrlIntent.putExtra(Intent.EXTRA_TEXT, finalDeepLinkUrl);

        //LogUtils.LOGD(TAG, "Deep Link shared: " + finalDeepLinkUrl);
        startActivity(Intent.createChooser(shareCoachUrlIntent, "Share via"));
    }

    private void setResultOnSelection() {
        if (!isSelected) {
            isSelected = true;
        } else {
            isSelected = false;
        }
        Intent intent = new Intent();
        intent.putExtra(Constants.BundleKey.POSITION, position);
        intent.putExtra(Constants.BundleKey.SPEAKER_ID, speakerId);
        intent.putExtra(Constants.BundleKey.IS_SELECTED, isSelected);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onListItemClicked(int position) {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(position);
            if(eventViewHolderModel != null) {
                Alert.showSpeakerEventInfoAlertDialog(this, eventViewHolderModel,
                        new Alert.OnCancelClickListener() {
                            @Override
                            public void onCancel() {

                            }
                        });
            }
        }
    }

}
