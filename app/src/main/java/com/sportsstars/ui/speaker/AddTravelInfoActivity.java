package com.sportsstars.ui.speaker;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddTravelInfoBinding;
import com.sportsstars.interfaces.TravelInfoListener;
import com.sportsstars.model.TravelViewHolderModel;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.speaker.UpdateSpeakerTravelInfoRequest;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.speaker.GuestSpeakerStates;
import com.sportsstars.network.response.speaker.StateCharges;
import com.sportsstars.network.response.speaker.StateList;
import com.sportsstars.network.response.speaker.StateListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.payment.AddBankAccountActivity;
import com.sportsstars.ui.speaker.adapter.TravelInfoAdapter;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;

import retrofit2.Call;

public class AddTravelInfoActivity extends BaseActivity implements TravelInfoListener, Alert.OnFeeDialogButtonClickListener {

    private static final int REQUEST_CODE_SLOT_FEE = 3001;
    private ActivityAddTravelInfoBinding mBinding;

    private UserProfileInstance mUserProfileInstance;
    private UserProfileModel mProfileModel;
    private int mInterStateTravel, mTravelType;
    private ArrayList<TravelViewHolderModel> mGlobalTravelList = new ArrayList<>();
    private TravelInfoAdapter mTravelInfoAdapter;
    private int mParentPosition, mSlotPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_travel_info);

        setUpHeader(mBinding.headerId, getString(R.string.add_travel_info_title), getString(R.string.add_travel_info_subtitle));

        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if(mProfileModel != null) {
            mInterStateTravel = mProfileModel.getInterStateTravel();
            mTravelType = mProfileModel.getTravelType();
        }
        if(Utils.isNetworkAvailable()) {
            getStateList();
        } else {
            generateTravelInfoObject();
            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.no_internet));
        }

        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable()) {
                    if(isInputValid()) {
                        if(mGlobalTravelList.get(0).getInterStateTravel() != Constants.INTER_STATE_TRAVEL.NO) {
                            Alert.showOkCancelDialog(AddTravelInfoActivity.this,
                                    getResources().getString(R.string.travel_info_alert_title),
                                    getResources().getString(R.string.travel_info_alert_msg),
                                    getResources().getString(R.string.cancel),
                                    getResources().getString(R.string.okay),
                                    new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {
                                            updateGuestSpeakerTravelInfo();
                                        }
                                    },
                                    new Alert.OnCancelClickListener() {
                                        @Override
                                        public void onCancel() {

                                        }
                                    }
                            );
                        } else {
                            updateGuestSpeakerTravelInfo();
                        }
                    }
                } else {
                    showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });

    }

    private boolean isInputValid() {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            int interStateTravel = mGlobalTravelList.get(0).getInterStateTravel();
            int travelType = mGlobalTravelList.get(0).getTravelType();
            if(interStateTravel == Constants.INTER_STATE_TRAVEL.UNSELECTED) {
                showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_select_inter_state_travel));
                return false;
            }
            if(travelType == Constants.TYPE_OF_TRAVEL.UNSELECTED) {
                showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_select_travel_type));
                return false;
            }
            if(interStateTravel != Constants.INTER_STATE_TRAVEL.NO) {
                if(!isStateListHasData()) {
                    showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_state_slot_price));
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isStateListHasData() {
        boolean isStateListHasData = false;
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 1) {
            for(int i = 1; i < mGlobalTravelList.size(); i++) {
                TravelViewHolderModel travelViewHolderModel = mGlobalTravelList.get(i);
                if(travelViewHolderModel != null) {
                    ArrayList<StateCharges> stateChargesList = travelViewHolderModel.getStateCharges();
                    if(stateChargesList != null && stateChargesList.size() > 0) {
                        int stateChargesListSize = stateChargesList.size();
                        int slotPosition = 0;
                        for(int j = 0; j < stateChargesList.size(); j++) {
                            StateCharges stateCharges = stateChargesList.get(j);
                            if(stateCharges != null) {
                                GuestSpeakerStates guestSpeakerStates = stateCharges.getGuestSpeakerStates();
                                if(guestSpeakerStates != null) {
                                    double slotPrice = guestSpeakerStates.getSpeakerCharge();
                                    if(slotPrice == 0) {
                                        //isEventHasData = false;
                                        //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                        //return isEventHasData;
                                    } else if(slotPrice != 0 && slotPrice > 0) {
                                        slotPosition++;
                                        //isEventHasData = true;
                                        //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                        //return isEventHasData;
                                    }
                                } else {
                                    //isEventHasData = false;
                                    //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                    //return isEventHasData;
                                }
                            }
                        }

                        // This condition is used when we need any 1 slot in state slots must be entered with price.
                        if(slotPosition > 0) {
                            isStateListHasData = true;
                            Log.d("speaker_log", "outside loop - slotPosition > 0 " +
                                    "- isStateListHasData : " + isStateListHasData);
                            return isStateListHasData;
                        }

//                        // This condition is used when we need all slots in state slots must be entered with price.
//                        // Commented below code as per requirement because at least 1 slot must be entered with price
//                        // and other slots can be left empty.
//                        if(slotPosition == stateChargesListSize) {
//                            isStateListHasData = true;
//                            Log.d("speaker_log", "outside loop - slotPosition == stateChargesListSize " +
//                                    "- isStateListHasData : " + isStateListHasData);
//                            return isStateListHasData;
//                        }

                    }
                }
            }
        }
        Log.d("speaker_log", "after loop - isStateListHasData : " + isStateListHasData);
        return isStateListHasData;
    }

    private boolean hasStateData(TravelViewHolderModel travelViewHolderModel) {
        boolean isStateHasData = false;
        if(travelViewHolderModel != null) {
            ArrayList<StateCharges> stateChargesList = travelViewHolderModel.getStateCharges();
            if(stateChargesList != null && stateChargesList.size() > 0) {
                int stateChargesListSize = stateChargesList.size();
                int slotPosition = 0;
                for(int j = 0; j < stateChargesList.size(); j++) {
                    StateCharges stateCharges = stateChargesList.get(j);
                    if(stateCharges != null) {
                        GuestSpeakerStates guestSpeakerStates = stateCharges.getGuestSpeakerStates();
                        if(guestSpeakerStates != null) {
                            double slotPrice = guestSpeakerStates.getSpeakerCharge();
                            if(slotPrice == 0) {
                                //isEventHasData = false;
                                //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                //return isEventHasData;
                            } else if(slotPrice != 0 && slotPrice > 0) {
                                slotPosition++;
                                //isEventHasData = true;
                                //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                //return isEventHasData;
                            }
                        } else {
                            //isEventHasData = false;
                            //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                            //return isEventHasData;
                        }
                    }
                }

                // This condition is used when we need any 1 slot in state slots must be entered with price.
                if(slotPosition > 0) {
                    isStateHasData = true;
                    Log.d("speaker_log", "outside loop - slotPosition > 0 " +
                            "- isStateHasData : " + isStateHasData);
                    return isStateHasData;
                }

//                // This condition is used when we need all slots in state slots must be entered with price.
//                // Commented below code as per requirement because at least 1 slot must be entered with price
//                // and other slots can be left empty.
//                if(slotPosition == stateChargesListSize) {
//                    isStateHasData = true;
//                    Log.d("speaker_log", "outside loop - slotPosition == stateChargesListSize " +
//                            "- isStateListHasData : " + isStateHasData);
//                    return isStateHasData;
//                }

            }
        }
        Log.d("speaker_log", "after loop - isStateHasData : " + isStateHasData);
        return isStateHasData;
    }

    private void updateGuestSpeakerTravelInfo() {

        UpdateSpeakerTravelInfoRequest updateSpeakerTravelInfoRequest = new UpdateSpeakerTravelInfoRequest();
        updateSpeakerTravelInfoRequest.setInterStateTravel(mGlobalTravelList.get(0).getInterStateTravel());
        updateSpeakerTravelInfoRequest.setTravelType(mGlobalTravelList.get(0).getTravelType());

        ArrayList<GuestSpeakerStates> guestSpeakerStatesList = getGuestSpeakerStatesList();
        int guestSpeakerStatesListSize = guestSpeakerStatesList.size();
        Log.d("speaker_log", "updateTravelInfo - guestSpeakerStatesList final size : " + guestSpeakerStatesListSize);

        if(updateSpeakerTravelInfoRequest.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.NO) {
            //updateSpeakerTravelInfoRequest.setGuestSpeakerStates(guestSpeakerStatesList);
            updateTravelInfoWithStates(updateSpeakerTravelInfoRequest);
        } else {
            if(guestSpeakerStatesListSize == 0) {
                showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_state_slot_price));
            } else {
                updateSpeakerTravelInfoRequest.setGuestSpeakerStates(guestSpeakerStatesList);
                updateTravelInfoWithStates(updateSpeakerTravelInfoRequest);
            }
        }

    }

    private void updateTravelInfoWithStates(UpdateSpeakerTravelInfoRequest updateSpeakerTravelInfoRequest) {

        processToShowDialog();

        //String updateRequestJson =  new Gson().toJson(updateSpeakerEventInfoRequest);
        //Log.d("speaker_log", "updateEventInfo - updateRequestJson : " + updateRequestJson);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateGuestSpeakerTravelInfo(updateSpeakerTravelInfoRequest).enqueue(
                new BaseCallback<GetProfileResponse>(AddTravelInfoActivity.this) {
                    @Override
                    public void onSuccess(GetProfileResponse response) {
                        if (response != null && response.getStatus() == 1) {
                            Log.d(TAG, "success : updateGuestSpeakerEventInfo");
                            mProfileModel = response.getResult();
                            if(mProfileModel != null && mProfileModel.getName() != null
                                    && !TextUtils.isEmpty(mProfileModel.getName())) {
                                mUserProfileInstance = UserProfileInstance.getInstance();
                                mUserProfileInstance.setUserProfileModel(mProfileModel);

                                Bundle bundle = new Bundle();
                                bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
                                Navigator.getInstance().navigateToActivityWithData(AddTravelInfoActivity.this,
                                        AddBankAccountActivity.class, bundle);

                                finish();
                            } else {
                                showSnackbarFromTop(AddTravelInfoActivity.this, "Not getting profile data properly.");
                            }
                        } else {
                            try {
                                if(response != null && response.getMessage() != null
                                        && !TextUtils.isEmpty(response.getMessage())) {
                                    showSnackbarFromTop(AddTravelInfoActivity.this, response.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                        try {
                            hideProgressBar();
                            if(baseResponse != null && baseResponse.getMessage() != null
                                    && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                showSnackbarFromTop(AddTravelInfoActivity.this, baseResponse.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private ArrayList<GuestSpeakerStates> getGuestSpeakerStatesList() {
        boolean hasStateData = false;
        ArrayList<GuestSpeakerStates> guestSpeakerStatesList = new ArrayList<>();
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 1) {
            for (int i = 1; i < mGlobalTravelList.size(); i++) {
                TravelViewHolderModel travelViewHolderModel = mGlobalTravelList.get(i);
                if (travelViewHolderModel != null && travelViewHolderModel.isChecked()) {
                    hasStateData = hasStateData(travelViewHolderModel);
                    if(hasStateData) {
                        ArrayList<StateCharges> stateChargesList = travelViewHolderModel.getStateCharges();
                        if(stateChargesList != null && stateChargesList.size() > 0) {
                            for(int j = 0; j < stateChargesList.size(); j++) {
                                StateCharges stateCharges = stateChargesList.get(j);
                                if(stateCharges != null) {
                                    GuestSpeakerStates guestSpeakerStates = stateCharges.getGuestSpeakerStates();
                                    if(guestSpeakerStates != null) {
                                        Log.d("speaker_log", "add guestSpeakerStates data into list - "
                                                + " getId : " + guestSpeakerStates.getId()
                                                + " getStateChargeId : " + guestSpeakerStates.getStateChargeId()
                                                + " getSpeakerCharge : " + guestSpeakerStates.getSpeakerCharge()
                                                + " getUserId : " + guestSpeakerStates.getUserId()
                                        );
                                        guestSpeakerStatesList.add(guestSpeakerStates);
                                    }
                                }
                            }
                        }
                    }
//                    else {
//                        break;
//                    }
                }
            }

//            if(!hasStateData) {
//                if(guestSpeakerStatesList != null && guestSpeakerStatesList.size() > 0) {
//                    guestSpeakerStatesList.clear();
//                }
//            }
        }
        return guestSpeakerStatesList;
    }

    private void generateTravelInfoObject() {
        if(mProfileModel != null && mProfileModel.getInterStateTravel() != 0 && mProfileModel.getTravelType() != 0) {
            mInterStateTravel = mProfileModel.getInterStateTravel();
            mTravelType = mProfileModel.getTravelType();
        } else {
            mInterStateTravel = Constants.INTER_STATE_TRAVEL.UNSELECTED;
            mTravelType = Constants.TYPE_OF_TRAVEL.UNSELECTED;
        }

        TravelViewHolderModel travelViewHolderModel = new TravelViewHolderModel();
        travelViewHolderModel.setInterStateTravel(mInterStateTravel);
        travelViewHolderModel.setTravelType(mTravelType);
        travelViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_TRAVEL_TYPE);

        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            mGlobalTravelList.clear();
            mGlobalTravelList.add(travelViewHolderModel);
            Log.d("speaker_log", "mGlobalTravelList size after clear : " + mGlobalTravelList.size());
        } else {
            mGlobalTravelList = new ArrayList<>();
            mGlobalTravelList.add(travelViewHolderModel);
            Log.d("speaker_log", "mGlobalTravelList size after new creation : " + mGlobalTravelList.size());
        }
    }

    private void generateTravelSuggestionObject() {
        TravelViewHolderModel travelViewHolderModel = new TravelViewHolderModel();
        travelViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_TRAVEL_SUGGESTION);
        mGlobalTravelList.add(travelViewHolderModel);
    }

    private void getStateList() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.getStateList().enqueue(new BaseCallback<StateListResponse>(AddTravelInfoActivity.this) {
            @Override
            public void onSuccess(StateListResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null && response.getResult().size() > 0) {
                            if(mProfileModel != null) {
                                if(mProfileModel.getGuestSpeakerStateList() != null
                                        && mProfileModel.getGuestSpeakerStateList().size() > 0) {
                                    mProfileModel.getGuestSpeakerStateList().clear();
                                }
                                mProfileModel.getGuestSpeakerStateList().addAll(response.getResult());
                            }
                            generateTravelInfoObject();
                            generateStateList(response.getResult());
                            generateTravelSuggestionObject();
                            setStateList();
                        } else {
                            generateTravelInfoObject();
                            setStateList();
                        }
                        //showToast("Getting state list successfully !!!");
                    } else {
                        try {
                            generateTravelInfoObject();
                            setStateList();
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(AddTravelInfoActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<StateListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    generateTravelInfoObject();
                    setStateList();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddTravelInfoActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void generateStateList(ArrayList<StateList> localStateList) {
        if(localStateList != null && localStateList.size() > 0) {
            for(int i = 0; i < localStateList.size(); i++) {
                StateList stateList = localStateList.get(i);
                if(stateList != null) {
                    TravelViewHolderModel travelViewHolderModel = new TravelViewHolderModel();

                    travelViewHolderModel.setId(stateList.getId());
                    travelViewHolderModel.setStateName(stateList.getStateName());
                    travelViewHolderModel.setStateCode(stateList.getStateCode());
                    travelViewHolderModel.setStateCharges(stateList.getStateCharges());
                    travelViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_STATE);
                    travelViewHolderModel.setGstValue(stateList.getGstValue());
                    travelViewHolderModel.setChecked(isStatePriceAvailable(stateList));

                    mGlobalTravelList.add(travelViewHolderModel);
                }
            }
        }
    }

    private boolean isStatePriceAvailable(StateList stateList) {
        boolean isChecked = false;
        int slotCount = 0;
        if(stateList.getStateCharges() != null && stateList.getStateCharges().size() > 0) {
            for(int j = 0; j < stateList.getStateCharges().size(); j++) {
                GuestSpeakerStates guestSpeakerStates = stateList.getStateCharges().get(j).getGuestSpeakerStates();
                if(guestSpeakerStates == null) {
                    isChecked = false;
                    //break;
                } else {
                    isChecked = true;
                    slotCount++;
                }
            }

            if(slotCount > 0) {
                isChecked = true;
            } else {
                isChecked = false;
            }
        }
        return isChecked;
    }

    private void setStateList() {
        mTravelInfoAdapter = new TravelInfoAdapter(AddTravelInfoActivity.this, mGlobalTravelList, this);
        //mBinding.rvTravelInfo.setHasFixedSize(true);
        mBinding.rvTravelInfo.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvTravelInfo.setAdapter(mTravelInfoAdapter);
    }

    @Override
    public String getActivityName() {
        return "AddTravelInfo";
    }

    @Override
    public void onInterStateTravelClickListener(int interStateTravel) {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            try {
                mGlobalTravelList.get(0).setInterStateTravel(interStateTravel);
                if(mTravelInfoAdapter != null) {
                    mTravelInfoAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTravelTypeClickListener(int travelType) {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            try {
                mGlobalTravelList.get(0).setTravelType(travelType);
                if(mTravelInfoAdapter != null) {
                    mTravelInfoAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTravelTypeInfoClickListener(int travelType) {
        if(travelType == Constants.TYPE_OF_TRAVEL.ECONOMY) {
            showEconomyInfoDialog();
        } else {
            showVipInfoDialog();
        }
    }

    @Override
    public void onStateSwitchChangedListener(int position, boolean state) {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            try {
                mGlobalTravelList.get(position).setChecked(state);
                if(mTravelInfoAdapter != null) {
                    mTravelInfoAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStateFeeClickListener(int parentPosition, int childPosition) {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            if(parentPosition >= mGlobalTravelList.size()) {
                return;
            }
            mParentPosition = parentPosition;
            mSlotPosition = childPosition;
            TravelViewHolderModel travelViewHolderModel = mGlobalTravelList.get(parentPosition);
            if(travelViewHolderModel != null) {
                if(travelViewHolderModel.getStateCharges() == null) {
                    return;
                }
                if(travelViewHolderModel.getStateCharges().size() == 0) {
                    return;
                }
                if(childPosition >= travelViewHolderModel.getStateCharges().size()) {
                    return;
                }
                StateCharges stateCharges = travelViewHolderModel.getStateCharges().get(childPosition);
                if(stateCharges != null) {
                    String feeText = stateCharges.getFeeText();
                    if(stateCharges.getGuestSpeakerStates() == null) {
                        GuestSpeakerStates guestSpeakerStates = new GuestSpeakerStates();
                        guestSpeakerStates.setSpeakerCharge(0);
                        guestSpeakerStates.setId(0);
                        guestSpeakerStates.setStateChargeId(stateCharges.getId());
                        guestSpeakerStates.setUserId(mProfileModel.getUserId());
                        guestSpeakerStates.setIsSuggested(0);
                        stateCharges.setGuestSpeakerStates(guestSpeakerStates);
                        Log.d("speaker_log", "Inside onStateFeeClickListener - create new guestSpeakerStates object");
                    }
                    String priceData = "";
                    if(stateCharges.getGuestSpeakerStates().getSpeakerCharge() != 0) {
                        priceData = String.valueOf(stateCharges.getGuestSpeakerStates().getSpeakerCharge());
                    } else {
                        priceData = "";
                    }

                    String hintText = getResources().getString(R.string.enter_fee);
                    if(stateCharges.getIsSuggested() == 1 && stateCharges.getSuggestedCharge() != 0) {
                        hintText = "ENTER $"+ stateCharges.getSuggestedCharge();
                    }
                    Alert.createSlotFeeEditAlert(AddTravelInfoActivity.this, travelViewHolderModel.getStateName(),
                            feeText, hintText, priceData, getResources().getString(R.string.yes_small),
                            this, REQUEST_CODE_SLOT_FEE).show();
                }
            }
        }
    }

    @Override
    public void onTravelSuggestionClickListener() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.webjet.com.au")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //showToast("Show Travel Suggestion - https://www.webjet.com.au/flights/");
    }

    @Override
    public void onPositive(DialogInterface dialog, String price, int type) {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            if(mParentPosition >= mGlobalTravelList.size()) {
                return;
            }
            TravelViewHolderModel travelViewHolderModel = mGlobalTravelList.get(mParentPosition);
            if(travelViewHolderModel != null) {
                if(travelViewHolderModel.getStateCharges() == null) {
                    return;
                }
                if(travelViewHolderModel.getStateCharges().size() == 0) {
                    return;
                }
                if(mSlotPosition >= travelViewHolderModel.getStateCharges().size()) {
                    return;
                }
                StateCharges stateCharges = travelViewHolderModel.getStateCharges().get(mSlotPosition);
                if(stateCharges != null) {
                    if(stateCharges.getGuestSpeakerStates() == null) {
                        GuestSpeakerStates guestSpeakerStates = new GuestSpeakerStates();
                        guestSpeakerStates.setSpeakerCharge(0);
                        guestSpeakerStates.setId(0);
                        guestSpeakerStates.setStateChargeId(stateCharges.getId());
                        guestSpeakerStates.setUserId(mProfileModel.getUserId());
                        guestSpeakerStates.setIsSuggested(0);
                        stateCharges.setGuestSpeakerStates(guestSpeakerStates);
                        Log.d("speaker_log", "Inside onPositive - create new guestSpeakerStates object");
                    }

                    try {
                        if(price == null) {
                            stateCharges.getGuestSpeakerStates().setSpeakerCharge(0);
                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
                            if(mTravelInfoAdapter != null) {
                                mTravelInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        }
                        if(TextUtils.isEmpty(price)) {
                            stateCharges.getGuestSpeakerStates().setSpeakerCharge(0);
                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
                            if(mTravelInfoAdapter != null) {
                                mTravelInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        }
                        String priceWithDollarStr = price.trim();
                        if(!TextUtils.isEmpty(priceWithDollarStr) && priceWithDollarStr.length() == 1
                                && priceWithDollarStr.contains("$")) {
                            stateCharges.getGuestSpeakerStates().setSpeakerCharge(0);
                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
                            if(mTravelInfoAdapter != null) {
                                mTravelInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        }
                        String priceWithoutDollarStr = priceWithDollarStr.trim();
                        if(!TextUtils.isEmpty(priceWithoutDollarStr) && priceWithoutDollarStr.length() > 1
                                && priceWithoutDollarStr.startsWith("$")) {
                            priceWithoutDollarStr = priceWithoutDollarStr.substring(1, priceWithoutDollarStr.length());
                        }
                        Double priceInDouble = Double.parseDouble(priceWithoutDollarStr);
                        if(priceInDouble < 1) {
                            stateCharges.getGuestSpeakerStates().setSpeakerCharge(0);
                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.travel_fee_less_than_1));
                            if(mTravelInfoAdapter != null) {
                                mTravelInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        } else if(priceInDouble > 5000) {
                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.travel_fee_greater_than_5000));
                            return;
                        } else {

                            if(stateCharges != null
                                    && stateCharges.getIsSuggested() == 1
                                    && !TextUtils.isEmpty(String.valueOf(stateCharges.getSuggestedCharge()))
                                    && stateCharges.getSuggestedCharge() != 0) {
                                if(stateCharges.getSuggestedCharge() == priceInDouble) {
                                    if(stateCharges.getGuestSpeakerStates() != null) {
                                        stateCharges.getGuestSpeakerStates().setIsSuggested(1);
                                    }
                                } else {
                                    if(stateCharges.getGuestSpeakerStates() != null) {
                                        stateCharges.getGuestSpeakerStates().setIsSuggested(0);
                                    }
                                }
                            } else {
                                if(stateCharges.getGuestSpeakerStates() != null) {
                                    stateCharges.getGuestSpeakerStates().setIsSuggested(0);
                                }
                            }
                            Log.d("speaker_log", "AddTravelInfoActivity - is internal slot suggested - "
                                    + stateCharges.getGuestSpeakerStates().getIsSuggested());

                            stateCharges.getGuestSpeakerStates().setSpeakerCharge(priceInDouble);
                            if(mTravelInfoAdapter != null) {
                                mTravelInfoAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onNegative(DialogInterface dialog, String price, int type) {
        if(mGlobalTravelList != null && mGlobalTravelList.size() > 0) {
            if(mParentPosition >= mGlobalTravelList.size()) {
                return;
            }
            TravelViewHolderModel travelViewHolderModel = mGlobalTravelList.get(mParentPosition);
            if(travelViewHolderModel != null) {
                if(travelViewHolderModel.getStateCharges() == null) {
                    return;
                }
                if(travelViewHolderModel.getStateCharges().size() == 0) {
                    return;
                }
                if(mSlotPosition >= travelViewHolderModel.getStateCharges().size()) {
                    return;
                }
//                StateCharges stateCharges = travelViewHolderModel.getStateCharges().get(mSlotPosition);
//                if(stateCharges != null) {
//                    if(stateCharges.getGuestSpeakerStates() == null) {
//                        GuestSpeakerStates guestSpeakerStates = new GuestSpeakerStates();
//                        guestSpeakerStates.setSpeakerCharge(0);
//                        guestSpeakerStates.setId(0);
//                        guestSpeakerStates.setStateChargeId(0);
//                        guestSpeakerStates.setUserId(mProfileModel.getUserId());
//                        stateCharges.setGuestSpeakerStates(guestSpeakerStates);
//                        Log.d("speaker_log", "Inside onPositive - create new guestSpeakerStates object");
//                    }
//
//                    try {
//                        if(price == null) {
//                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
//                            return;
//                        }
//                        if(TextUtils.isEmpty(price)) {
//                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
//                            return;
//                        }
//                        String priceWithDollarStr = price.trim();
//                        if(!TextUtils.isEmpty(priceWithDollarStr) && priceWithDollarStr.length() == 1
//                                && priceWithDollarStr.contains("$")) {
//                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
//                            return;
//                        }
//                        String priceWithoutDollarStr = priceWithDollarStr.trim();
//                        if(!TextUtils.isEmpty(priceWithoutDollarStr) && priceWithoutDollarStr.length() > 1
//                                && priceWithoutDollarStr.startsWith("$")) {
//                            priceWithoutDollarStr = priceWithoutDollarStr.substring(1, priceWithoutDollarStr.length());
//                        }
//                        Double priceInDouble = Double.parseDouble(priceWithoutDollarStr);
//                        if(priceInDouble < 1) {
//                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.travel_fee_less_than_1));
//                            return;
//                        } else if(priceInDouble > 5000) {
//                            showSnackbarFromTop(AddTravelInfoActivity.this, getResources().getString(R.string.travel_fee_greater_than_5000));
//                            return;
//                        } else {
//                            stateCharges.getGuestSpeakerStates().setSpeakerCharge(priceInDouble);
//                            if(mTravelInfoAdapter != null) {
//                                mTravelInfoAdapter.notifyDataSetChanged();
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
            }
        }
    }

    private void showEconomyInfoDialog() {
        String title = getResources().getString(R.string.economy);
        String message = getResources().getString(R.string.economy_info_msg);
        Alert.showInfoDialog(AddTravelInfoActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showVipInfoDialog() {
        String title = getResources().getString(R.string.vip);
        String message = getResources().getString(R.string.vip_info_msg);
        Alert.showInfoDialog(AddTravelInfoActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

}
