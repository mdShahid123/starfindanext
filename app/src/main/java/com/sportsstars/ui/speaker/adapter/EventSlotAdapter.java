package com.sportsstars.ui.speaker.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemEventSlotViewHolderBinding;
import com.sportsstars.interfaces.EventInfoListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.network.response.speaker.EventSlot;
import com.sportsstars.ui.speaker.vh.EventSlotViewHolder;

import java.util.ArrayList;

public class EventSlotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<EventSlot> mEventSlots;
    private Activity mActivity;
    private EventViewHolderModel mEventViewHolderModel;
    private int mParentPosition;
    private EventInfoListener mEventInfoListener;

    public EventSlotAdapter(Activity activity, EventViewHolderModel eventViewHolderModel, int parentPosition,
                            ArrayList<EventSlot> eventSlots, EventInfoListener eventInfoListener) {
        mActivity = activity;
        mEventViewHolderModel = eventViewHolderModel;
        mParentPosition = parentPosition;
        mEventSlots = eventSlots;
        mEventInfoListener = eventInfoListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = new EventSlotViewHolder(
                    (ItemEventSlotViewHolderBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                            R.layout.item_event_slot_view_holder, parent, false), mActivity, mEventInfoListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((EventSlotViewHolder) holder).bindData(mEventViewHolderModel, mParentPosition, mEventSlots.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mEventSlots.size();
    }

}
