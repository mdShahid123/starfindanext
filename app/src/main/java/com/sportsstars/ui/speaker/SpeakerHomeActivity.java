package com.sportsstars.ui.speaker;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.constants.AppConstants;
import com.appster.chatlib.utils.PrefUtils;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sportsstars.BuildConfig;
import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.chat.UniversalMessageListener;
import com.sportsstars.chat.chat.ArchivedChatsFragment;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.chat.chat.ChatsFragment;
import com.sportsstars.databinding.ActivitySpeakerHomeBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.model.UserModel;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.UpdateDeviceTokenRequest;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.response.CalendarSyncBaseResponse;
import com.sportsstars.network.response.auth.BadgeCountResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.notifications.FromUser;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.fragment.AccountFragment;
import com.sportsstars.ui.auth.fragment.NotificationsFragment;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.speaker.events.SpeakerBookingsRootFragment;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.util.StringUtils;

import retrofit2.Call;

public class SpeakerHomeActivity extends BaseActivity implements View.OnClickListener {

    public ActivitySpeakerHomeBinding mBinder;

    public static final int Home_FRAGMENT_POS = 0;
    public static final int TRACKS_FRAGMENT_POS = 1;
    public static final int MESSAGE_FRAGMENT_POS = 2;
    public static final int PROFILE_FRAGMENT_POS = 3;

    public AHBottomNavigation bottomBar;

    private SpeakerBookingsRootFragment mSpeakerBookingsRootFragment;
    private ChatsFragment mChatsFragment;
    private ArchivedChatsFragment mArchivedChatsFragment;
    public NotificationsFragment mNotificationsFragment;
    private AccountFragment mAccountFragment;

    private int mBadgeCount;
    public boolean isFromNotificationPanel;
    public int notificationTypeId;
    public int notificationPayloadSessionId;
    public FromUser notificationPayloadFromUser;

    public static int selectedTabPos = Home_FRAGMENT_POS;

    @Override
    public String getActivityName() {
        return "SpeakerHome";
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_speaker_home);

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        init();
        selectedTabPos = Home_FRAGMENT_POS;
        bottomBar.setCurrentItem(Home_FRAGMENT_POS);

        getIntentData();
        redirectFromNotificationPanel();

        if (ContextCompat.checkSelfPermission(SpeakerHomeActivity.this, Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            askCalendarPermission();
        } else {
            if(Utils.isNetworkAvailable()) {
                //getBookingEventListToCalendar();
            }
        }

        //Initialize Chat
        start();
        try {
//            ChatController.getInstance().startConnection();
            ChatController.getInstance().setMessageListener(UniversalMessageListener.getInstance());
//            DeliveryManager.getInstance().setReceiptEnable(true);
//            DeliveryManager.getInstance().setAutoDeliveryReceipt(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        addConnectionListener();

        getGuestSpeakerProfileStatus();

    }

    private void getIntentData() {
        Intent intent = getIntent();
        if(intent != null) {
            if(intent.hasExtra(Constants.BundleKey.IS_FROM_NOTIFICATION_PANEL)) {
                isFromNotificationPanel = intent.getBooleanExtra(Constants.BundleKey.IS_FROM_NOTIFICATION_PANEL, false);
            }
            if(isFromNotificationPanel) {
                if(intent.hasExtra(Constants.BundleKey.NOTIFICATION_TYPE_ID)) {
                    notificationTypeId = intent.getIntExtra(Constants.BundleKey.NOTIFICATION_TYPE_ID, 0);
                }
                if(intent.hasExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_SESSION_ID)) {
                    notificationPayloadSessionId = intent.getIntExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_SESSION_ID, 0);
                }
                if(intent.hasExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_FROM_USER)) {
                    notificationPayloadFromUser = intent.getParcelableExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_FROM_USER);
                }
            }
        }
    }

    private void redirectFromNotificationPanel() {
        if(isFromNotificationPanel) {
            PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.GUEST_SPEAKER);
            switch (notificationTypeId) {
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CONFIRMED_FOR_COACH:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CANCELLED_FOR_COACH:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CONFIRMED_FOR_LEARNER:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CANCELLED_FOR_LEARNER:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_RESCHEDULED_FOR_COACH:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_RESCHEDULED_FOR_LEARNER:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_COACH_24:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_COACH_2:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_LEARNER_24:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_LEARNER_2:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_REQUEST:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_CANCELLED:
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_PAYMENT:
                    selectedTabPos = Home_FRAGMENT_POS;
                    bottomBar.setCurrentItem(Home_FRAGMENT_POS);
                    break;
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED:
                    selectedTabPos = Home_FRAGMENT_POS;
                    bottomBar.setCurrentItem(Home_FRAGMENT_POS);
                    break;
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_PROFILE_APPROVED:
                    selectedTabPos = Home_FRAGMENT_POS;
                    bottomBar.setCurrentItem(Home_FRAGMENT_POS);
                    break;
                case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_CHAT:
//                    if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
//                        selectedTabPos = Home_FRAGMENT_POS;
//                        bottomBar.setCurrentItem(Home_FRAGMENT_POS);
//                        return;
//                    }
                    startChat();
                    break;
                default:
                    selectedTabPos = Home_FRAGMENT_POS;
                    bottomBar.setCurrentItem(Home_FRAGMENT_POS);
                    break;
            }
        }
    }

    private void startChat() {
        try {
            if(notificationPayloadFromUser != null) {
                if(!TextUtils.isEmpty(notificationPayloadFromUser.getJid())) {
                    String userName = notificationPayloadFromUser.getJid().split("@")[0];
                    if (!TextUtils.isEmpty(userName)) {
                        if(!TextUtils.isEmpty(notificationPayloadFromUser.getName())) {
                            Intent intent = new Intent(this, ChatThreadActivity.class);
//                            intent.putExtra(AppConstants.KEY_TO_USERNAME, userName);
                            intent.putExtra(AppConstants.KEY_TO_USERNAME, notificationPayloadFromUser.getName());
                            intent.putExtra(AppConstants.KEY_TO_USER_PIC, notificationPayloadFromUser.getProfileImage());
                            intent.putExtra(AppConstants.KEY_TO_USER, notificationPayloadFromUser.getJid());
                            startActivity(intent);
                        } else {
                            showToast("Username of this profile not found to initiate chat.");
                        }
                    } else {
                        showToast("Username of this profile not found to initiate chat.");
                        //com.appster.chatlib.utils.Utils.showToast(this, "enter username");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Utils.isNetworkAvailable()) {
            getBadgeCount();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateDeviceToken();
    }

    private void getBadgeCount() {
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,false);
        webServices.getBadgeCount().enqueue(new BaseCallback<BadgeCountResponse>(SpeakerHomeActivity.this) {
            @Override
            public void onSuccess(BadgeCountResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mBadgeCount = response.getResult().getBadgeCount();
                        if(mBadgeCount > 0) {
                            bottomBar.setNotification(" ",2);
                            bottomBar.setNotificationBackground(ContextCompat.getDrawable(SpeakerHomeActivity.this,R.drawable.ic_notification_badge_bg));
                            bottomBar.setNotificationMarginLeft(12,12);
                        } else {
                            bottomBar.setNotification("",2);
                            bottomBar.setNotificationBackgroundColor(Color.TRANSPARENT);
                            bottomBar.setNotificationMarginLeft(12,12);
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BadgeCountResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(bottomBar.getCurrentItem() == 1) {
            String headerTitle = getHeaderTitle(mBinder.headerId);
            if(StringUtils.isNullOrEmpty(headerTitle)) {
                super.onBackPressed();
            } else {
                if(headerTitle.equalsIgnoreCase(getString(R.string.text_archived_chats))) {
                    loadChat();
                } else {
                    super.onBackPressed();
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    private void clearBadgeOnTab() {
        bottomBar.setNotification("",2);
        bottomBar.setNotificationBackgroundColor(Color.TRANSPARENT);
        bottomBar.setNotificationMarginLeft(12,12);
    }

    private void init() {
        /**
         * initViews is used to initialize this view at app launch
         */
        bottomBar = mBinder.ntbHorizontal;

        // bottomBar = (AHBottomNavigation) findViewById(R.id.ntb_horizontal);
        // bottomBar.setTitleTextSize(Utils.convertSpToPixels(10.0f, this), Utils.convertSpToPixels(10.0f, this));
        bottomBar.addItem(new AHBottomNavigationItem("", R.drawable.ic_nav_first_unselected));
        bottomBar.addItem(new AHBottomNavigationItem("", R.drawable.ic_nav_chat_unselected));
        bottomBar.addItem(new AHBottomNavigationItem("", R.drawable.ic_nav_notif_unselected));
        bottomBar.addItem(new AHBottomNavigationItem("", R.drawable.ic_nav_profile_unselected));

        bottomBar.setDefaultBackgroundColor(ContextCompat.getColor(this, R.color.white_color));
        bottomBar.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomBar.setAccentColor(ContextCompat.getColor(this, R.color.green_gender));
        bottomBar.setInactiveColor(ContextCompat.getColor(this, R.color.gray_nav_bottom_unselected));

        bottomBar.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {

            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                switch (position) {
                    case Home_FRAGMENT_POS:
                        selectedTabPos = Home_FRAGMENT_POS;
                        //setUpHeader(mBinder.headerId, "MY SESSIONS", "");
                        setUpHeaderForCoachHome(mBinder.headerId, "MY BOOKINGS", "");

                        // Creating new instance of CoachSessionsRootFragment always to fix Tips Fragment freeze issue
                        mSpeakerBookingsRootFragment = new SpeakerBookingsRootFragment();
                        pushFragment(mSpeakerBookingsRootFragment, null, R.id.fragment_container);
                        break;

                    case TRACKS_FRAGMENT_POS:
                        selectedTabPos = TRACKS_FRAGMENT_POS;
                        setUpHeaderForChat(mBinder.headerId, "CHATS", "", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                setUpHeader(mBinder.headerId, getString(R.string.text_archived_chats), "");
                                if (mArchivedChatsFragment == null) {
                                    mArchivedChatsFragment = new ArchivedChatsFragment();
                                }
                                pushFragment(mArchivedChatsFragment, null, R.id.fragment_container);
                            }
                        });
                        if (mChatsFragment == null) {
                            mChatsFragment = new ChatsFragment();
                        }
                        pushFragment(mChatsFragment, null, R.id.fragment_container);
                        break;

                    case MESSAGE_FRAGMENT_POS:
                        selectedTabPos = MESSAGE_FRAGMENT_POS;
                        setUpHeader(mBinder.headerId, "NOTIFICATIONS", "");
                        if (mNotificationsFragment == null) {
                            mNotificationsFragment = new NotificationsFragment();
                        }
                        pushFragment(mNotificationsFragment, null, R.id.fragment_container);
                        clearBadgeOnTab();
                        break;

                    case PROFILE_FRAGMENT_POS:
                        selectedTabPos = PROFILE_FRAGMENT_POS;
                        setUpHeader(mBinder.headerId, getString(R.string.account), "");
                        if (mAccountFragment == null) {
                            mAccountFragment = new AccountFragment();
                        }
                        pushFragment(mAccountFragment, null, R.id.fragment_container);
                        break;

                    default:
                        break;

                }
                return true;
            }
        });
    }

    private void loadChat() {
        setUpHeaderForChat(mBinder.headerId, "CHATS", "", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpHeader(mBinder.headerId, getString(R.string.text_archived_chats), "");
                if (mArchivedChatsFragment == null) {
                    mArchivedChatsFragment = new ArchivedChatsFragment();
                }
                pushFragment(mArchivedChatsFragment, null, R.id.fragment_container);
            }
        });
        if (mChatsFragment == null) {
            mChatsFragment = new ChatsFragment();
        }
        pushFragment(mChatsFragment, null, R.id.fragment_container);
    }

    @Override
    public void onClick(View view) {
    }

    private void updateDeviceToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        String fcmToken = PreferenceUtil.getFCMToken();
        //LogUtils.LOGD("FCM instance token ", token);
        //LogUtils.LOGD("FCM token", token);
        if (TextUtils.isEmpty(token) && TextUtils.isEmpty(fcmToken)) {
        } else if (!TextUtils.isEmpty(token)) {
            PreferenceUtil.setFCMToken(token);
        }

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,false);
        webServices.updateDeviceToken(new UpdateDeviceTokenRequest(PreferenceUtil.getFCMToken())).enqueue(new BaseCallback<BaseResponse>(SpeakerHomeActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }


    // PERMISSION RELATED TASKS //

    public static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 112;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
//            case REQUEST_CODE:
//                Navigator.getInstance().navigateToActivity(this, LearnerProfileViewActivity.class);
//                break;
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(Utils.isNetworkAvailable()) {
                        //getBookingEventListToCalendar();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_not_granted_for_calendar), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(SpeakerHomeActivity.this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void getBookingEventListToCalendar() {
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCalendarSyncData().enqueue(new BaseCallback<CalendarSyncBaseResponse>(this) {
            @Override
            public void onSuccess(CalendarSyncBaseResponse response) {
                if(response != null && response.getStatus() == 1) {
                    if(response.getResult() != null && response.getResult().size() > 0) {
                        //CalendarSyncHelper.getInstance().deleteAllCalendarEvents(SpeakerHomeActivity.this);
                        CalendarSyncHelper.getInstance().updateCalendarEventList(SpeakerHomeActivity.this, response.getResult());
                    }
                }
            }

            @Override
            public void onFail(Call<CalendarSyncBaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void start() {
        if (canConnect(PreferenceUtil.getUserModel().getJid(), PreferenceUtil.getUserModel().getJkey())
                || canConnect(PrefUtils.getUsername(), PrefUtils.getPassword())) {
            ChatController.getInstance().setUpConnectionData(
                    com.appster.chatlib.utils.Utils.getUserNameFromJabberId(PreferenceUtil.getUserModel().getJid()),
                    PreferenceUtil.getUserModel().getJid(), PreferenceUtil.getUserModel().getJkey(),
                    BuildConfig.HOST_NAME, Constants.port, BuildConfig.SERVICE_NAME);
            try {
                ChatController.getInstance().startConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean canConnect(String jid, String jKey) {
        if (StringUtils.isNullOrEmpty(jid)||(StringUtils.isNullOrEmpty(jKey))) {
            // TODO: 25/06/18 - Not getting JabberId and JabberKey when signUp using Facebook.
            //com.appster.chatlib.utils.Utils.showToast(this, "please enter username and password");
            return false;
        }

        jid = PreferenceUtil.getUserModel().getJid();
        return true;
    }

    public void addConnectionListener() {
        ChatController.getInstance().setConnectionListener(new ConnectionListener() {
            @Override
            public void connected(XMPPConnection connection) {
                LogUtils.LOGD(">>>>>>","XMPP Connected");
            }

            @Override
            public void authenticated(XMPPConnection connection, boolean resumed) {

            }

            @Override
            public void connectionClosed() {
                LogUtils.LOGD(">>>>>>","XMPP connectionClosed");
            }

            @Override
            public void connectionClosedOnError(Exception e) {

            }

            @Override
            public void reconnectionSuccessful() {
                LogUtils.LOGD(">>>>>>","XMPP reconnectionSuccessful");
            }

            @Override
            public void reconnectingIn(int seconds) {

            }

            @Override
            public void reconnectionFailed(Exception e) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ChatController.getInstance().closeConnection(this);
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(otpVerifiedEvent.isOtpVerified()) {
//            try {
//                finish();
//                Navigator.getInstance().navigateToActivity(SpeakerHomeActivity.this, SpeakerHomeActivity.class);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }
    }

    private void getGuestSpeakerProfileStatus() {
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.GUEST_SPEAKER);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        UserProfileModel mProfileSpeaker = response.getResult();
                        UserModel userModel = PreferenceUtil.getUserModel();
                        userModel.setProfileImage(mProfileSpeaker.getProfileImage());
                        PreferenceUtil.setUserModel(userModel);
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {

            }
        });
    }

}
