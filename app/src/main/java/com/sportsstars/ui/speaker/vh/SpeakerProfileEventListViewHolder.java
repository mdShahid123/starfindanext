package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemEventSpeakerProfileBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.network.response.speaker.EventSlot;
import com.sportsstars.util.StringUtils;
import com.squareup.picasso.Picasso;

public class SpeakerProfileEventListViewHolder extends RecyclerView.ViewHolder {

    private ItemEventSpeakerProfileBinding mBinding;
    private Activity mActivity;
    private ListItemClickListener mListItemClickListener;

    public SpeakerProfileEventListViewHolder(ItemEventSpeakerProfileBinding binding, Activity activity,
                                             ListItemClickListener listItemClickListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mListItemClickListener = listItemClickListener;
    }

    public void bindData(EventViewHolderModel eventViewHolderModel, int position) {
        if(eventViewHolderModel != null) {

            mBinding.tvEventName.setText(eventViewHolderModel.getName());

            mBinding.rlRootEventItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListItemClickListener != null) {
                        mListItemClickListener.onListItemClicked(position);
                    }
                }
            });

            if(StringUtils.isNullOrEmpty(eventViewHolderModel.getEventLogo())) {
                return;
            }
            Picasso.with(mActivity)
                    .load(eventViewHolderModel.getEventLogo())
                    .placeholder(R.drawable.session_details_star)
                    .error(R.drawable.session_details_star)
                    .into(mBinding.ivEventIcon,
                            new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

            mBinding.tvFrom.setVisibility(View.VISIBLE);
            mBinding.tvEventCharges.setVisibility(View.VISIBLE);
            double smallestPrice = getSmallestEventPrice(eventViewHolderModel);
            mBinding.tvEventCharges.setText("$"+smallestPrice);
        }
    }

    private double getSmallestEventPrice(EventViewHolderModel eventViewHolderModel) {
        double smallestPrice = eventViewHolderModel.getEventSlot().get(0).getGuestSpeakerEvents().getSlotPrice();
        for(EventSlot eventSlot : eventViewHolderModel.getEventSlot()) {
            if(eventSlot.getGuestSpeakerEvents().getSlotPrice() >= 0
                    && smallestPrice >= 0) {
                if (eventSlot.getGuestSpeakerEvents().getSlotPrice() < smallestPrice) {
                    smallestPrice = eventSlot.getGuestSpeakerEvents().getSlotPrice();
                }
            }
        }
        return smallestPrice;
    }

}

