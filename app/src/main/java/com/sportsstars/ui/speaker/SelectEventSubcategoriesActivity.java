package com.sportsstars.ui.speaker;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySelectEventSubcategoriesBinding;
import com.sportsstars.databinding.ItemFlowSportsBinding;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;

import java.util.ArrayList;

public class SelectEventSubcategoriesActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySelectEventSubcategoriesBinding mBinding;

    private boolean isInEditMode;
    private int mEventPosition;
    private String mEventCategoryName;
    private ArrayList<EventSubcategory> mEventSubcategories;
    private ArrayList<ItemFlowSportsBinding> mItemFlowBindingList;
    private ArrayList<EventSubcategory> mEventSubcategoriesFilterList = new ArrayList<>();
    private boolean isNoSearchResult;
    private boolean isFromSpeakerFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_event_subcategories);

        mBinding.layoutTop.headerId.tvTitle.setText(R.string.select_subcategories);
        getIntentData();
        initViews();
        setTextChangeListener();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_EDIT_MODE_ENABLED)) {
                isInEditMode = bundle.getBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_POSITION)) {
                mEventPosition = bundle.getInt(Constants.BundleKey.EVENT_POSITION);
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_SUB_CATEGORIES)) {
                mEventSubcategories = bundle.getParcelableArrayList(Constants.BundleKey.EVENT_SUB_CATEGORIES);
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_CATEGORIES_NAME)) {
                mEventCategoryName = bundle.getString(Constants.BundleKey.EVENT_CATEGORIES_NAME);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SPEAKER_FILTER)) {
                isFromSpeakerFilter = bundle.getBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
            }
        }
    }

    private void initViews() {

        if(!TextUtils.isEmpty(mEventCategoryName)) {
            mBinding.layoutTop.headerId.tvTitle.setText(getResources().getString
                    (R.string.search_for_category_title, mEventCategoryName));
            mBinding.etSearch.setHint(getResources().getString(R.string.search_for_category_hint_text, mEventCategoryName));
        } else {
            mBinding.layoutTop.headerId.tvTitle.setText(R.string.select_subcategories);
            mBinding.etSearch.setHint(getResources().getString(R.string.type_subcategories_name));
        }

        mBinding.tvConfirm.setOnClickListener(this);

        mItemFlowBindingList = new ArrayList<>();
        mBinding.flowLayoutSports.removeAllViews();

        if(mEventSubcategories != null && mEventSubcategories.size() > 0) {
            if(mEventSubcategoriesFilterList != null && mEventSubcategoriesFilterList.size() > 0) {
                mEventSubcategoriesFilterList.clear();
                mEventSubcategoriesFilterList.addAll(mEventSubcategories);
            }
            for(EventSubcategory eventSubcategory : mEventSubcategories) {
                if(eventSubcategory != null) {
                    addTitleToLayout(eventSubcategory);
                }
            }
        }

    }

    private void addTitleToLayout(final EventSubcategory eventSubcategory) {

        if(eventSubcategory != null) {

            String eventSubcategoryName = "";

            if(eventSubcategory.getEventSubcategoryDetail() != null
                    && eventSubcategory.getEventSubcategoryDetail().getName() != null
                    && !TextUtils.isEmpty(eventSubcategory.getEventSubcategoryDetail().getName())) {
                eventSubcategoryName = eventSubcategory.getEventSubcategoryDetail().getName();
            }

            final ItemFlowSportsBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from
                    (mBinding.flowLayoutSports.getContext())
                    .inflate(R.layout.item_flow_sports, mBinding.flowLayoutSports, false));

            if(flowBinding != null) {

                flowBinding.flowChild.setText(eventSubcategoryName);
                flowBinding.flowChild.setTag(eventSubcategory);
                if(eventSubcategory.isSelected()) {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                } else {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                }

                mItemFlowBindingList.add(flowBinding);

                flowBinding.flowChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                    for (int i = 0; i < mEventSubcategories.size(); i++) {
//                        mEventSubcategories.get(i).setSelected(false);
//                        mItemFlowBindingList.get(i).flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
//                        mItemFlowBindingList.get(i).flowChild.setTextColor(getResources().getColor(R.color.green_gender));
//                    }

                        EventSubcategory eventSubcategoryLocal = (EventSubcategory) flowBinding.flowChild.getTag();
                        if(eventSubcategoryLocal.isSelected()) {
                            flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                            flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                            eventSubcategory.setSelected(false);
                        } else {
                            flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                            flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                            eventSubcategory.setSelected(true);
                        }

                        if(mEventSubcategories != null && mEventSubcategories.size() > 0) {
                            for(EventSubcategory eventSubcategoryGlobal : mEventSubcategories) {
                                if(eventSubcategoryGlobal.getId() == eventSubcategoryLocal.getId()) {
                                    if(eventSubcategoryLocal.isSelected()) {
                                        eventSubcategoryGlobal.setSelected(true);
                                    } else {
                                        eventSubcategoryGlobal.setSelected(false);
                                    }
                                }
                            }
                        }

//                    setResultForEventCategories((EventSubcategory) flowBinding.flowChild.getTag());

                    }
                });

                mBinding.flowLayoutSports.addView(flowBinding.getRoot());

            }

        }

    }

    private void setTextChangeListener() {
        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //removeSubcategoriesSelection();
                String constraint = s.toString();
                if(constraint != null && constraint.length() > 0) {
                    constraint = constraint.toUpperCase();
                    ArrayList<EventSubcategory> filters = new ArrayList<>();
                    for(int i = 0; i < mEventSubcategories.size(); i++) {
                        if(mEventSubcategories.get(i).getName().toUpperCase().startsWith(constraint)) {
                            EventSubcategory eventSubcategory = new EventSubcategory();
                            eventSubcategory.setId(mEventSubcategories.get(i).getId());
                            eventSubcategory.setEventCategoryId(mEventSubcategories.get(i).getEventCategoryId());
                            eventSubcategory.setEventSubcategoryId(mEventSubcategories.get(i).getEventSubcategoryId());
                            eventSubcategory.setEventId(mEventSubcategories.get(i).getEventId());
                            eventSubcategory.setName(mEventSubcategories.get(i).getName());
                            eventSubcategory.setSelected(mEventSubcategories.get(i).isSelected());
                            eventSubcategory.setEventSubcategoryDetail(mEventSubcategories.get(i).getEventSubcategoryDetail());

                            filters.add(eventSubcategory);
                        }
                    }
                    if(mEventSubcategoriesFilterList != null) {
                        if(mEventSubcategoriesFilterList.size() > 0) {
                            mEventSubcategoriesFilterList.clear();
                        }
                        mEventSubcategoriesFilterList.addAll(filters);
                    }
                } else {
                    if(mEventSubcategoriesFilterList != null) {
                        if(mEventSubcategoriesFilterList.size() > 0) {
                            mEventSubcategoriesFilterList.clear();
                        }
                        mEventSubcategoriesFilterList.addAll(mEventSubcategories);
                    }
                }
                if(mEventSubcategoriesFilterList != null && mEventSubcategoriesFilterList.size() > 0) {
                    mBinding.tvNoResults.setVisibility(View.GONE);
                    mBinding.flowLayoutSports.setVisibility(View.VISIBLE);
                    isNoSearchResult = false;
                } else {
                    mBinding.tvNoResults.setVisibility(View.VISIBLE);
                    mBinding.flowLayoutSports.setVisibility(View.GONE);
                    initViews();
                    isNoSearchResult = true;
                }

                populateFilterList(isNoSearchResult);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Method used to remove subcategory selection from the list.
     */
    private void removeSubcategoriesSelection() {
        if(mEventSubcategoriesFilterList != null && mEventSubcategoriesFilterList.size() > 0) {
            for(int i = 0; i < mEventSubcategoriesFilterList.size(); i++) {
                EventSubcategory eventSubcategory = mEventSubcategoriesFilterList.get(i);
                eventSubcategory.setSelected(false);
            }

            if(mEventSubcategories != null && mEventSubcategories.size() > 0) {
                for(int i = 0; i < mEventSubcategories.size(); i++) {
                    EventSubcategory eventSubcategory = mEventSubcategoriesFilterList.get(i);
                    eventSubcategory.setSelected(false);
                }
            }
        }


        initViews();

    }

    private void populateFilterList(boolean isNoSearchResult) {
        if(isNoSearchResult) {
            mItemFlowBindingList = new ArrayList<>();
            mBinding.flowLayoutSports.removeAllViews();

            if(mEventSubcategories != null && mEventSubcategories.size() > 0) {
                if(mEventSubcategoriesFilterList != null && mEventSubcategoriesFilterList.size() > 0) {
                    mEventSubcategoriesFilterList.clear();
                    mEventSubcategoriesFilterList.addAll(mEventSubcategories);
                }
                for(EventSubcategory eventSubcategory : mEventSubcategories) {
                    if(eventSubcategory != null) {
                        addTitleToLayout(eventSubcategory);
                    }
                }
            }
        } else {
            mItemFlowBindingList = new ArrayList<>();
            mBinding.flowLayoutSports.removeAllViews();

            if(mEventSubcategoriesFilterList != null && mEventSubcategoriesFilterList.size() > 0) {
                for(EventSubcategory eventSubcategory : mEventSubcategoriesFilterList) {
                    if(eventSubcategory != null) {
                        addTitleToLayout(eventSubcategory);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.tv_confirm) {
            int isSelectedCount = 0;
            for(int i = 0; i < mEventSubcategories.size(); i++) {
                EventSubcategory eventSubcategory = mEventSubcategories.get(i);
                if(eventSubcategory.isSelected()) {
                    isSelectedCount++;
                }
                Log.d("category_log", "Event subcategories list - "
                        + " eventSubcategory subcategory id : " + eventSubcategory.getId()
                        + " eventSubcategory isSelected : " + eventSubcategory.isSelected()
                        + " isSelected count : " + isSelectedCount
                );

                if(eventSubcategory != null && eventSubcategory.getEventSubcategoryDetail() != null
                        && eventSubcategory.getEventSubcategoryDetail().getName() != null
                        && !TextUtils.isEmpty(eventSubcategory.getEventSubcategoryDetail().getName())) {
                    Log.d("category_log", "Event subcategories list - event subcategory name : "
                            + eventSubcategory.getEventSubcategoryDetail().getName());
                }
            }

            if(isSelectedCount >= 1) {
                setResultForEventCategoriesList(mEventSubcategories);
            } else {
                if(isFromSpeakerFilter) {
                    setResultForEventCategoriesList(mEventSubcategories);
                } else {
                    showSnackbarFromTop(SelectEventSubcategoriesActivity.this,
                            getResources().getString(R.string.please_select_subcategories));
                }
            }
        }
    }

    private void setResultForEventCategories(EventSubcategory eventSubcategory) {
        Intent intent = new Intent();
        intent.putExtra(Constants.BundleKey.IS_EDIT_MODE_ENABLED, isInEditMode);
        intent.putExtra(Constants.BundleKey.EVENT_POSITION, mEventPosition);
        intent.putExtra(Constants.EXTRA_EVENT_SUB_CATEGORIES, eventSubcategory);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void setResultForEventCategoriesList(ArrayList<EventSubcategory> eventSubcategoryList) {
        Intent intent = new Intent();
        intent.putExtra(Constants.BundleKey.IS_EDIT_MODE_ENABLED, isInEditMode);
        intent.putExtra(Constants.BundleKey.EVENT_POSITION, mEventPosition);
        intent.putExtra(Constants.EXTRA_EVENT_SUB_CATEGORIES_LIST, eventSubcategoryList);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public String getActivityName() {
        return "SelectEventSubcategories";
    }

}
