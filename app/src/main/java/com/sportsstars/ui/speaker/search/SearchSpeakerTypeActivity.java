package com.sportsstars.ui.speaker.search;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchCoachSpeakerBinding;
import com.sportsstars.databinding.ActivitySearchSpeakerTypeBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.search.SearchCoachActivity;
import com.sportsstars.ui.search.SearchCoachSpeakerActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;

import retrofit2.Call;

public class SearchSpeakerTypeActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "SearchSpeakerType";
    private ActivitySearchSpeakerTypeBinding mBinder;

    @Override
    public String getActivityName() {
        return SearchSpeakerTypeActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_search_speaker_type);
        mBinder.headerId.tvTitle.setText(R.string.search_for_a_speaker);
        init();
    }

    private void init() {
        mBinder.headerId.ivBack.setOnClickListener(this);
        mBinder.relSpeakerAthleteMain.setOnClickListener(this);
        mBinder.relSpeakerPanelMain.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.rel_speaker_athlete_main:
                Bundle bundleAthlete = new Bundle();
                bundleAthlete.putInt(Constants.BundleKey.IS_PANEL, 0);
                bundleAthlete.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                Navigator.getInstance().navigateToActivityWithData(this, SearchCoachActivity.class, bundleAthlete);
                break;

            case R.id.rel_speaker_panel_main:
                showSnackbarFromTop(SearchSpeakerTypeActivity.this, "Coming Soon");

//                Bundle bundlePanel = new Bundle();
//                bundlePanel.putInt(Constants.BundleKey.IS_PANEL, 0);
//                bundlePanel.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
//                Navigator.getInstance().navigateToActivityWithData(this, SearchCoachActivity.class, bundlePanel);
                break;
        }
    }

}
