package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sportsstars.databinding.ItemSpecialitiesViewHolderBinding;
import com.sportsstars.interfaces.EventInfoListener;
import com.sportsstars.model.EventViewHolderModel;

public class SpecialitiesViewHolder extends RecyclerView.ViewHolder {

    private ItemSpecialitiesViewHolderBinding mBinding;
    private Activity mActivity;
    private EventInfoListener mEventInfoListener;

    public SpecialitiesViewHolder(ItemSpecialitiesViewHolderBinding binding, Activity activity, EventInfoListener eventInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mEventInfoListener = eventInfoListener;
    }

    public void bindData(EventViewHolderModel eventViewHolderModel, int position) {
        if(eventViewHolderModel != null) {
            mBinding.etSpecialities.setText(eventViewHolderModel.getSpecialities());
            mBinding.etSpecialities.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        InputMethodManager imm = (InputMethodManager) itemView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(mBinding.etSpecialities.getWindowToken(), 0);
//                        mBinding.etSpecialities.setFocusable(false);
//                        mBinding.etSpecialities.setFocusableInTouchMode(true);
//                        Log.d("cert_log", "CertificateViewHolder - "
//                                + " mBinding.tvCertificateTtl : " + mBinding.tvCertificateTtl.getText().toString()
//                                + " editing completed. "
//                                +  " certificate id : " + certificate.getId()
//                        );
//                        uploadCallback.onCertificateTitleChanged(certificate.getId(),
//                                mBinding.tvCertificateTtl.getText().toString().trim());

                        if(mEventInfoListener != null) {
                            mEventInfoListener.onSpecialitiesEntered(mBinding.etSpecialities.getText().toString());
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            mBinding.etSpecialities.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
//                    if(mEventInfoListener != null) {
//                        mEventInfoListener.onSpecialitiesEntered(mBinding.etSpecialities.getText().toString());
//                    }
                    String specialitiesText = mBinding.etSpecialities.getText().toString();
                    Log.d("speaker_log", "afterTextChanged - specialities text : " + specialitiesText);
                    eventViewHolderModel.setSpecialities(specialitiesText);
                }
            });
        }
    }

}
