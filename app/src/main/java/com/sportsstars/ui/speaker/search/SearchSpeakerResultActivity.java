package com.sportsstars.ui.speaker.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchSpeakerResultBinding;
import com.sportsstars.databinding.ItemCoachResultBinding;
import com.sportsstars.databinding.ItemSpeakerResultBinding;
import com.sportsstars.eventbus.BackNavigationEvent;
import com.sportsstars.eventbus.LocationEvent;
import com.sportsstars.eventbus.LocationPermission;
import com.sportsstars.interfaces.SpeakerBySportItemClick;
import com.sportsstars.interfaces.SpeakerSelectedListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.FilterData;
import com.sportsstars.model.FilterState;
import com.sportsstars.model.SearchFilter;
import com.sportsstars.model.Speaker;
import com.sportsstars.model.SpeakerFilterData;
import com.sportsstars.model.SpeakerFilterState;
import com.sportsstars.model.Sports;
import com.sportsstars.model.Team;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.coachprofile.TeamListRequest;
import com.sportsstars.network.request.auth.speaker.SearchSpeakerBySportRequest;
import com.sportsstars.network.response.coachprofile.TeamsListBaseResponse;
import com.sportsstars.network.response.speaker.EventCategory;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.network.response.speaker.EventSubcategoryDetail;
import com.sportsstars.network.response.speaker.SearchSpeakerBySportResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.ui.speaker.EventBookingFormActivity;
import com.sportsstars.ui.speaker.adapter.SpeakerListBySportAdapter;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

public class SearchSpeakerResultActivity extends BaseActivity implements View.OnClickListener, SpeakerBySportItemClick,
        SpeakerSelectedListener {

    private static final String TAG = "SearchSpeakerResult";
    private static final int REQUEST_CODE_LOCATION = 101;

    private ActivitySearchSpeakerResultBinding mBinding;

    private Sports mSport;
    private Geocoder geocoder;
    private boolean isLocationFound;

    private static final int REQUEST_CODE_FILTER = 110;
    private FilterData mFilterData;
    private SearchFilter mSearchFilter;
    private FilterState mFilterState;
    private boolean mResultFound;
    private boolean mRepeatRequest;
    private int mRequestCount;
    private int mCurrentPage = 1;
    private int lastPage = 1;
    private boolean isLoading = false;
    private boolean isScrolling = false;
    private LinearLayoutManager mLayoutManager = null;
    private ArrayList<Speaker> mSpeakerList = new ArrayList<>();
    private Boolean isShowLoader = true;
    private SpeakerListBySportAdapter mAdapter;

    private int isPanel;

    // Filter and Speaker search data variables
    private double mLatitude;
    private double mLongitude;
    private String mAddress;
    private EventViewHolderModel eventViewHolderModel;
    private int eventId;
    private String eventName;
    private ArrayList<Sports> mSelectedSportsList;
    private int minBudget = 0;
    private int maxBudget = 5000;
    private boolean isAllSportsSelected;
    private boolean isAllTeamsSelected;
    private boolean isAllSubcategoriesSelected;
    private ArrayList<Team> mSelectedTeamList = new ArrayList<>();
    private int gender;
    private int travelType;
    private ArrayList<EventSubcategory> mSelectedEventSubcategories = new ArrayList<>();
    private SpeakerFilterData filterData = new SpeakerFilterData();

    // Selected speakers list variable
    private ArrayList<Speaker> mSelectedSpeakerList = new ArrayList<>();
    private ArrayList<Speaker> mRemainingSpeakerList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_speaker_result);

        mBinding.headerId.tvTitle.setText(R.string.search_for_a_athlete);
        Utils.setTransparentTheme(this);

        getIntentData();
        init();

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        //showProgressBar();
        LocationUtils.addFragment(this, this);

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        mCurrentPage = 1;
        callSearchSpeakerApi();

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_VIEW_HOLDER)) {
                eventViewHolderModel = bundle.getParcelable(Constants.BundleKey.EVENT_VIEW_HOLDER);
                getEventSubcategoriesList();
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_ID)) {
                eventId = bundle.getInt(Constants.BundleKey.EVENT_ID, 0);
            }
            if(getIntent().hasExtra(Constants.BundleKey.EVENT_NAME)) {
                eventName = bundle.getString(Constants.BundleKey.EVENT_NAME);
            }
            if(getIntent().hasExtra(Constants.BundleKey.IS_PANEL)) {
                isPanel = bundle.getInt(Constants.BundleKey.IS_PANEL, 0);
            }
            if(getIntent().hasExtra(Constants.BundleKey.SPORTS_LIST)) {
                mSelectedSportsList = bundle.getParcelableArrayList(Constants.BundleKey.SPORTS_LIST);
            }
            if(getIntent().hasExtra(Constants.BundleKey.ALL_SPORTS_SELECTED)) {
                isAllSportsSelected = bundle.getBoolean(Constants.BundleKey.ALL_SPORTS_SELECTED, false);
            }
        }
    }

    private ArrayList<EventSubcategory> getEventSubcategoriesList() {
        if(eventViewHolderModel != null
                && eventViewHolderModel.getEventCategories() != null
                && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
            if(mSelectedEventSubcategories != null && mSelectedEventSubcategories.size() > 0) {
                mSelectedEventSubcategories.clear();
            }
            mSelectedEventSubcategories.addAll(eventViewHolderModel.getEventCategories().getEventSubcategories());
            isAllSubcategoriesSelected = true;
        }
        return mSelectedEventSubcategories;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void init() {
        mBinding.llNextButton.setVisibility(View.GONE);

        mBinding.headerId.ivBack.setOnClickListener(this);
        mBinding.tvUpdateLocation.setOnClickListener(this);
        mBinding.tvSportName.setOnClickListener(this);
        mBinding.tvEventName.setOnClickListener(this);
        mBinding.tvTeamName.setOnClickListener(this);
        mBinding.tvBudgetName.setOnClickListener(this);
        mBinding.buttonNext.setOnClickListener(this);
        mBinding.ivFilter.setOnClickListener(this);

//        Drawable[] drawables = mBinder.tvSportName.getCompoundDrawables();

//        if (getIntent() != null) {
//            mSport = getIntent().getParcelableExtra(Constants.EXTRA_SPORT);
//            isPanel = getIntent().getIntExtra(Constants.BundleKey.IS_PANEL, 0);
//
//            if(mSport == null) {
//                mSport = mSelectedSportsList.get(0);
//            }
//
//            if (mSport != null) {
//                mBinding.tvSportName.setText(mSport.getName());
//
//                if(!TextUtils.isEmpty(mSport.getBackgroundImage())) {
//                    Picasso.with(this)
//                            .load(mSport.getBackgroundImage())
////                            .placeholder(R.drawable.ic_user_circle)
////                            .error(R.drawable.ic_user_circle)
//                            .into((mBinding.ivBackground));
//                }
//            }
//        }

        gender = Constants.GENDER_TYPE.NONE;
        travelType = Constants.INTER_STATE_TRAVEL.UNSELECTED;

        if(mSelectedSportsList != null && mSelectedSportsList.size() > 0) {

            if(mSport == null) {
                mSport = mSelectedSportsList.get(0);
            }

            if (mSport != null) {
//                Log.d("sport_log", "speaker search result - initViews - mSport id : " + mSport.getId()
//                    + " mSport name : " + mSport.getName());
                if(mSelectedSportsList.size() > 1) {
                    if(isAllSportsSelected) {
                        mBinding.tvSportName.setText(getResources().getString(R.string.selected_all_sports_count));
                    } else {
                        mBinding.tvSportName.setText(getResources().getString(R.string.selected_sports_count,
                                mSelectedSportsList.size()));
                    }
                } else {
                    mBinding.tvSportName.setText(mSport.getName());
                }

                if(!TextUtils.isEmpty(mSport.getBackgroundImage())) {
                    Picasso.with(this)
                            .load(mSport.getBackgroundImage())
//                            .placeholder(R.drawable.ic_user_circle)
//                            .error(R.drawable.ic_user_circle)
                            .into((mBinding.ivBackground));
                }

                setEventNameDynamically(eventName);

                mBinding.tvBudgetName.setText(getResources().getString(R.string.min_max_budget_range, minBudget, maxBudget));
            }

        }

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        setAdapter();
    }

    private void setEventNameDynamically(String eventName) {
        if(!TextUtils.isEmpty(eventName)) {
            mBinding.tvEventName.setText(eventName);
            if(eventName.length() > 17) {
                mBinding.tvEventName.setTextSize(15f);
                mBinding.tvEventName.setPadding(0, 29, 0, 29);
            } else {
                mBinding.tvEventName.setTextSize(18f);
                mBinding.tvEventName.setPadding(0, 21, 0, 21);
            }
        }
    }

    private void setAdapter() {
        mAdapter = new SpeakerListBySportAdapter(this, mSpeakerList, this, this);
        mBinding.recCoachList.setLayoutManager(mLayoutManager);
        mBinding.recCoachList.setItemAnimator(new DefaultItemAnimator());
        mBinding.recCoachList.setAdapter(mAdapter);
        mBinding.recCoachList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0 && totalItemCount >= 20) {
                        if(Utils.isNetworkAvailable()) {
                            isScrolling = true;
                            mCurrentPage++;
                            callSearchSpeakerApi();
//                            Log.d("Paging", "Add More items");
                        } else {
                            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.no_internet));
                        }
                    }
                }
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public String getActivityName() {
        return "SearchSpeakerResult";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.tv_update_location:
                launchUpdateLocation();
                break;

            case R.id.tv_sport_name:
                finish();
                break;

            case R.id.tv_event_name:
//                EventBus.getDefault().post(new BackNavigationEvent(true));
//                finish();

                launchSelectEventScreen();
                break;

            case R.id.tv_team_name:
                Bundle bundleTeam = new Bundle();
                bundleTeam.putIntegerArrayList(Constants.BundleKey.SPORT_IDS_LIST, getSportIdsList());
                bundleTeam.putParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST, mSelectedTeamList);
                bundleTeam.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
                Navigator.getInstance().navigateToActivityForResultWithData(
                        SearchSpeakerResultActivity.this,
                        SearchSpeakerTeamListActivity.class,
                        bundleTeam, Constants.REQUEST_CODE.REQUEST_CODE_TEAM_LIST);
                break;

            case R.id.tv_budget_name:
                //showToast("Start Activity for result for Set Budget screen");
                Bundle bundleBudget = new Bundle();
                bundleBudget.putInt(Constants.BundleKey.MIN_BUDGET, minBudget);
                bundleBudget.putInt(Constants.BundleKey.MAX_BUDGET, maxBudget);
                Navigator.getInstance().navigateToActivityForResultWithData(
                        SearchSpeakerResultActivity.this,
                        SetBudgetActivity.class,
                        bundleBudget, Constants.REQUEST_CODE.REQUEST_CODE_SET_BUDGET);
                break;

            case R.id.iv_filter:
                //showSnackbarFromTop(SearchSpeakerResultActivity.this, "Coming Soon");
                launchFilterScreen();
                break;

            case R.id.button_next:
                if(isInputValid()) {
                    //showSnackbarFromTop(SearchSpeakerResultActivity.this, "Coming Soon");
                    launchEventBookingScreen();
                }
                break;
        }
    }

    private void launchSelectEventScreen() {
        if(Utils.isNetworkAvailable()) {
            Bundle bundleSpeaker = new Bundle();
            bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
            bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
            bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, true);
            Navigator.getInstance().navigateToActivityForResultWithData(SearchSpeakerResultActivity.this,
                    SelectEventTypeActivity.class, bundleSpeaker, Constants.REQUEST_CODE.REQUEST_CODE_EVENT_TYPE);
        } else {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void launchEventBookingScreen() {
        updateFilterData();
        //getRemainingSpeakerList();
        getRemainingSpeakersList();
        Bundle bundleBooking = new Bundle();
        bundleBooking.putParcelableArrayList(Constants.BundleKey.SPEAKERS_LIST, mSelectedSpeakerList);
        bundleBooking.putParcelableArrayList(Constants.BundleKey.REMAINING_SPEAKERS_LIST, mRemainingSpeakerList);
        bundleBooking.putParcelable(Constants.BundleKey.SPEAKER_FILTER_DATA, filterData);
        Navigator.getInstance().navigateToActivityWithData(
                SearchSpeakerResultActivity.this,
                EventBookingFormActivity.class,
                bundleBooking
        );
    }

    private void getRemainingSpeakerList() {
        if(mSpeakerList != null && mSpeakerList.size() > 0
                && mSelectedSpeakerList != null && mSelectedSpeakerList.size() > 0) {
            int remainingCount = 20 - mSelectedSpeakerList.size();
            int newSelectionCount = 0;
            for(Speaker speakerOuter : mSpeakerList) {
                for(Speaker speakerInner : mSelectedSpeakerList) {
                    if(newSelectionCount == remainingCount) {
                        break;
                    }
                    if(speakerOuter != null && speakerInner != null) {
                        if(speakerOuter.getUserId() != speakerInner.getUserId()) {
                            if(!mRemainingSpeakerList.contains(speakerOuter)) {
                                mRemainingSpeakerList.add(speakerOuter);
                                newSelectionCount++;
                            }
                        }
                    }
                }
                if(newSelectionCount == remainingCount) {
                    break;
                }
            }
        }
    }

    private void getRemainingSpeakersList() {
        // Prepare a union
        ArrayList<Speaker> unionList = new ArrayList<Speaker>(mSpeakerList);
        unionList.addAll(mSelectedSpeakerList);
        // Prepare an intersection
        ArrayList<Speaker> intersectionList = new ArrayList<Speaker>(mSpeakerList);
        intersectionList.retainAll(mSelectedSpeakerList);
        // Subtract the intersection from the union
        unionList.removeAll(intersectionList);

        int remainingCount = 20 - mSelectedSpeakerList.size();
        if(mRemainingSpeakerList != null && mRemainingSpeakerList.size() > 0) {
            mRemainingSpeakerList.clear();
        }

        // Print the result
        int newSelectionCount = 1;
        for (int i = 0; i < unionList.size(); i++) {
            newSelectionCount = newSelectionCount + i;
            if(newSelectionCount == remainingCount) {
                break;
            }
            Speaker speaker = unionList.get(i);
//            Log.d("booking_log", "Search result screen -  speaker id : " + speaker.getUserId()
//                    + " speaker name : " + speaker.getName());
            mRemainingSpeakerList.add(speaker);
        }
    }

    private void launchFilterScreen() {
        updateFilterData();
        Bundle bundleFilter = new Bundle();
        bundleFilter.putParcelable(Constants.BundleKey.SPEAKER_FILTER_DATA, filterData);
        //bundleFilter.putParcelable(Constants.BundleKey.SPEAKER_FILTER_STATE, filterState);
        Navigator.getInstance().navigateToActivityForResultWithData(
                SearchSpeakerResultActivity.this,
                SpeakerFilterActivity.class,
                bundleFilter,
                Constants.REQUEST_CODE.REQUEST_CODE_SPEAKER_FILTER
        );
    }

    private void updateFilterData() {
        if(filterData == null) {
            filterData = new SpeakerFilterData();
        }
        filterData.setGender(gender);
        filterData.setSelectedEventViewHolder(eventViewHolderModel);
        if(filterData.getSelectedSportsList() != null && filterData.getSelectedSportsList().size() > 0) {
            filterData.getSelectedSportsList().clear();
        }
        filterData.getSelectedSportsList().addAll(mSelectedSportsList);

        if(filterData.getSelectedTeamsList() != null && filterData.getSelectedTeamsList().size() > 0) {
            filterData.getSelectedTeamsList().clear();
        }
        filterData.getSelectedTeamsList().addAll(mSelectedTeamList);
        filterData.setLatitude(mLatitude);
        filterData.setLongitude(mLongitude);
        filterData.setAddress(mAddress);

        if(filterData.getSelectedEventSubcategories() != null && filterData.getSelectedEventSubcategories().size() > 0) {
            filterData.getSelectedEventSubcategories().clear();
        }
        filterData.getSelectedEventSubcategories().addAll(mSelectedEventSubcategories);
        filterData.setMinBudget(minBudget);
        filterData.setMaxBudget(maxBudget);
        filterData.setTravelType(travelType);
        filterData.setAllSportsSelected(isAllSportsSelected);
        filterData.setAllTeamsSelected(isAllTeamsSelected);
        filterData.setAllSubcategoriesSelected(isAllSubcategoriesSelected);
    }

    private ArrayList<Integer> getSportIdsList() {
        ArrayList<Integer> sportIdsList = new ArrayList<>();
        if(mSelectedSportsList != null && mSelectedSportsList.size() > 0) {
            for(Sports sports : mSelectedSportsList) {
                if(sports != null && sports.getId() != 0) {
                    sportIdsList.add(sports.getId());
                }
            }
        }
        return sportIdsList;
    }

    private boolean isInputValid() {
        if(mSelectedSpeakerList == null) {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.selected_speaker_list_invalid));
            return false;
        } else if(mSelectedSpeakerList.size() == 0) {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.selected_speaker_list_empty));
            return false;
        } else if(mSelectedSportsList == null) {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.sports_list_blank));
            return false;
        } else if(mSelectedSportsList.size() == 0) {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.sports_list_blank));
            return false;
        } else if(eventId == 0) {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.selected_event_is_invalid));
            return false;
        } else {
            return true;
        }
    }

    private void launchUpdateLocation() {
        Intent intent = new Intent(this, SearchCoachLocationActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        try {
            //hideProgressBar();
            isLocationFound = true;
            LogUtils.LOGD(TAG, "onEvent location update...");
            Location location = event.getMessage();
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();

            mAddress = getGeocodeAddress(location.getLatitude(), location.getLongitude());
            mBinding.tvAddress.setText(mAddress);
//            Log.d("location_log", "SearchSpeaker - mAddressString : " + mAddress
//                    + " latitude : " + location.getLatitude() + " longitude : " + location.getLongitude());
            try {
                if(!TextUtils.isEmpty(mAddress) && mAddress.equalsIgnoreCase("Unknown")) {
                    Alert.showInfoDialog(SearchSpeakerResultActivity.this,
                            getResources().getString(R.string.address_not_found_title),
                            getResources().getString(R.string.address_not_found_desc),
                            getResources().getString(R.string.okay),
                            new Alert.OnOkayClickListener() {
                                @Override
                                public void onOkay() {
                                }});

                    mBinding.tvNoResults.setVisibility(View.VISIBLE);
                    mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                    mBinding.llNextButton.setVisibility(View.GONE);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(TextUtils.isEmpty(mAddress)) {
                mBinding.tvNoResults.setVisibility(View.VISIBLE);
                mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                mBinding.llNextButton.setVisibility(View.GONE);
                return;
            }

            if(mAddress.equalsIgnoreCase("Unknown")) {
                mBinding.tvNoResults.setVisibility(View.VISIBLE);
                mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                mBinding.llNextButton.setVisibility(View.GONE);
                return;
            }

            if(!TextUtils.isEmpty(mAddress) && !mAddress.equalsIgnoreCase("Unknown")) {
                mBinding.tvNoResults.setVisibility(View.GONE);
                mBinding.tvNoResults.setText(getResources().getString(R.string.loading_list_plz_wait));
                mBinding.llNextButton.setVisibility(View.VISIBLE);
                mCurrentPage = 1;
                callSearchSpeakerApi();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onEvent(LocationPermission permission) {
        //hideProgressBar();
        isLocationFound = false;
        showToast(getString(R.string.location_permission_not_granted));
        finish();
    }

    @Subscribe
    public void onEvent(BackNavigationEvent backNavigationEvent) {
        if(backNavigationEvent.isBackNavigated()) {
            finish();
        }
    }

    private String getGeocodeAddress(double latitude, double longitude) {
        List<Address> addresses = null;
        String address = "Unknown";

        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
//            errorMessage = getString(R.string.service_not_available);
//            Log.e(TAG, errorMessage, ioException);
            LogUtils.LOGE(TAG, ioException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
//            errorMessage = getString(R.string.invalid_lat_long_used);
//            Log.e(TAG, errorMessage + ". " +
//                    "Latitude = " + location.getLatitude() +
//                    ", Longitude = " +
//                    location.getLongitude(), illegalArgumentException);
            LogUtils.LOGE(TAG, illegalArgumentException.getMessage());

        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }

        // Handle case where no address was found.
        if (addresses != null && addresses.size() != 0) {
            address = addresses.get(0).getAddressLine(0);
        }

        return address;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_LOCATION:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        mLatitude = data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0);
                        mLongitude = data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0);
                        mAddress = data.getStringExtra(Constants.EXTRA_ADDRESS);

                        if (mAddress != null) {
//                            Log.d("location_log", "SearchSpeaker - ActivityResult - mAddressString : " + mAddress
//                                    + " latitude : " + mLatitude + " longitude : " + mLongitude);
                            mBinding.tvAddress.setText(mAddress);
                        }

                        if(TextUtils.isEmpty(mAddress)) {
                            mBinding.tvNoResults.setVisibility(View.VISIBLE);
                            mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                            mBinding.llNextButton.setVisibility(View.GONE);
                            return;
                        }

                        if (mAddress != null) {
                            if(mAddress.equalsIgnoreCase("Unknown")) {
                                mBinding.tvNoResults.setVisibility(View.VISIBLE);
                                mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                                mBinding.llNextButton.setVisibility(View.GONE);
                                return;
                            }

                            if(!TextUtils.isEmpty(mAddress) && !mAddress.equalsIgnoreCase("Unknown")) {
                                mBinding.tvNoResults.setVisibility(View.GONE);
                                mBinding.tvNoResults.setText(getResources().getString(R.string.loading_list_plz_wait));
                                mBinding.llNextButton.setVisibility(View.VISIBLE);
                                mCurrentPage = 1;
                                if(mSelectedSpeakerList != null) {
                                    mSelectedSpeakerList.clear();
                                }
                                callSearchSpeakerApi();
                            }
                        }
                    }
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_COACH:
                if(resultCode == RESULT_OK) {
                    finish();
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_SET_BUDGET:
                if(resultCode == RESULT_OK) {
                    if(data != null) {
                        minBudget = data.getIntExtra(Constants.BundleKey.MIN_BUDGET, 0);
                        maxBudget = data.getIntExtra(Constants.BundleKey.MAX_BUDGET, 0);
                        mBinding.tvBudgetName.setText(getResources().getString(R.string.min_max_budget_range, minBudget, maxBudget));
                        mCurrentPage = 1;
                        if(mSelectedSpeakerList != null) {
                            mSelectedSpeakerList.clear();
                        }
                        callSearchSpeakerApi();
                    }
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_TEAM_LIST:
                if(resultCode == RESULT_OK) {
                    if(data != null) {
                        if(mSelectedTeamList == null) {
                            mSelectedTeamList = new ArrayList<>();
                        } else if(mSelectedTeamList.size() > 0) {
                            mSelectedTeamList.clear();
                        }
                        mSelectedTeamList = data.getParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST);
                        if(mSelectedTeamList != null && mSelectedTeamList.size() > 0) {
                            if(mSelectedTeamList.size() == 1) {
                                String teamName = mSelectedTeamList.get(0).getName();
                                if(!TextUtils.isEmpty(teamName)) {
                                    mBinding.tvTeamName.setText(teamName);
                                } else {
                                    mBinding.tvTeamName.setText(getResources().getString(R.string.selected_team_count,
                                            mSelectedTeamList.size()));
                                }
                            } else {
                                mBinding.tvTeamName.setText(getResources().getString(R.string.selected_teams_count,
                                        mSelectedTeamList.size()));
                            }
                        } else {
                            mBinding.tvTeamName.setText(getResources().getString(R.string.all));
                        }
                        mCurrentPage = 1;
                        if(mSelectedSpeakerList != null) {
                            mSelectedSpeakerList.clear();
                        }
                        callSearchSpeakerApi();
                    }
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_EVENT_TYPE:
                if(resultCode == RESULT_OK) {
                    if(data != null) {
                        eventViewHolderModel = data.getParcelableExtra(Constants.BundleKey.EVENT_VIEW_HOLDER);
                        eventId = data.getIntExtra(Constants.BundleKey.EVENT_ID, 0);
                        eventName = data.getStringExtra(Constants.BundleKey.EVENT_NAME);

                        if(eventId == 0) {
                            showToast(getResources().getString(R.string.selected_event_is_invalid));
                            return;
                        }
                        if(TextUtils.isEmpty(eventName)) {
                            showToast(getResources().getString(R.string.selected_event_is_invalid));
                            return;
                        }
                        if(filterData != null) {
                            filterData.setSelectedEventViewHolder(eventViewHolderModel);
                        }

                        if(!TextUtils.isEmpty(eventName)) {
                            setEventNameDynamically(eventName);

                            mCurrentPage = 1;
                            if(mSelectedSpeakerList != null) {
                                mSelectedSpeakerList.clear();
                            }
                            callSearchSpeakerApi();
                        }
                    }
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_SPEAKER_FILTER:
                if(resultCode == RESULT_OK) {
                    filterData = data.getParcelableExtra(Constants.BundleKey.SPEAKER_FILTER_DATA);
                    updateUiWithFilterData(filterData);

                    mCurrentPage = 1;
                    if(mSelectedSpeakerList != null) {
                        mSelectedSpeakerList.clear();
                    }
                    callSearchSpeakerApi();
                }
                break;

            case Constants.REQUEST_CODE.REQUEST_CODE_SPEAKER_PROFILE:
                if(resultCode == RESULT_OK) {
                    if(data != null) {
                        int position = data.getIntExtra(Constants.BundleKey.POSITION, 0);
                        boolean isSelected = data.getBooleanExtra(Constants.BundleKey.IS_SELECTED, false);
                        if(mSpeakerList != null && mSpeakerList.size() > 0) {
                            Speaker speaker = mSpeakerList.get(position);
                            if(speaker != null && speaker.getUserId() != 0) {
                                if(isSelected) {
                                    if(mSelectedSpeakerList != null && mSelectedSpeakerList.size() > 0) {
                                        if(mSelectedSpeakerList.size() == Constants.MAX_SPEAKER_SELECTION_LIMIT) {
                                            showSnackbarFromTop(SearchSpeakerResultActivity.this,
                                                    getResources().getString(R.string.max_speaker_selection_limit_reached));
                                            return;
                                        }
                                    }
                                    speaker.setSelected(true);
                                    mSelectedSpeakerList.add(speaker);
                                } else {
                                    speaker.setSelected(false);
                                    mSelectedSpeakerList.remove(speaker);
                                }
                                if(mAdapter != null) {
                                    mAdapter.notifyDataSetChanged();
                                }
//                                Log.d("speaker_log", "selectedSpeakerList size : " + mSelectedSpeakerList.size());
                            }
                        }
                    }
                }
                break;

        }
    }

    private void updateUiWithFilterData(SpeakerFilterData filterData) {
        if(filterData != null) {
            mLatitude = filterData.getLatitude();
            mLongitude = filterData.getLongitude();
            mAddress = filterData.getAddress();

            if(mSelectedSportsList != null) {
                mSelectedSportsList.clear();
            }
            mSport = null;
            mSelectedSportsList.addAll(filterData.getSelectedSportsList());

            if(mSelectedTeamList != null) {
                mSelectedTeamList.clear();
            }
            mSelectedTeamList.addAll(filterData.getSelectedTeamsList());

            if(mSelectedEventSubcategories != null) {
                mSelectedEventSubcategories.clear();
            }
//            Log.d("speaker_log", "Search Result screen - filterData.getSelectedEventSubcategories() size : "
//                    + filterData.getSelectedEventSubcategories().size());
            mSelectedEventSubcategories.addAll(filterData.getSelectedEventSubcategories());
//            Log.d("speaker_log", "Search Result screen - mSelectedEventSubcategories size : "
//                    + mSelectedEventSubcategories.size());

            eventViewHolderModel = filterData.getSelectedEventViewHolder();
            eventId = eventViewHolderModel.getId();
            eventName = eventViewHolderModel.getName();

            isAllSportsSelected = filterData.isAllSportsSelected();
            isAllTeamsSelected = filterData.isAllTeamsSelected();
            isAllSubcategoriesSelected = filterData.isAllSubcategoriesSelected();

            gender = filterData.getGender();
            travelType = filterData.getTravelType();

            minBudget = (int) filterData.getMinBudget();
            maxBudget = (int) filterData.getMaxBudget();

            if(mSelectedSportsList != null) {
                if(mSelectedSportsList.size() > 0) {
                    if(mSport == null) {
                        mSport = mSelectedSportsList.get(0);
                    }
                    if (mSport != null) {
//                    Log.d("sport_log", "speaker search result - onActivityResult - mSport id : " + mSport.getId()
//                            + " mSport name : " + mSport.getName());
                        if(mSelectedSportsList.size() > 1) {
                            if(isAllSportsSelected) {
                                mBinding.tvSportName.setText(getResources().getString(R.string.selected_all_sports_count));
                            } else {
                                mBinding.tvSportName.setText(getResources().getString(R.string.selected_sports_count,
                                        mSelectedSportsList.size()));
                            }
                        } else {
                            mBinding.tvSportName.setText(mSport.getName());
                        }

                        if(!TextUtils.isEmpty(mSport.getBackgroundImage())) {
                            Picasso.with(this)
                                    .load(mSport.getBackgroundImage())
//                            .placeholder(R.drawable.ic_user_circle)
//                            .error(R.drawable.ic_user_circle)
                                    .into((mBinding.ivBackground));
                        }
                    }
                } else {
                    if(isAllSportsSelected) {
                        mBinding.tvSportName.setText(getResources().getString(R.string.selected_all_sports_count));
                    }
                }
            } else {
                if(isAllSportsSelected) {
                    mBinding.tvSportName.setText(getResources().getString(R.string.selected_all_sports_count));
                }
            }

            if(mSelectedSportsList != null && mSelectedSportsList.size() > 0) {
                if(mSport == null) {
                    mSport = mSelectedSportsList.get(0);
                }
                if (mSport != null) {
//                    Log.d("sport_log", "speaker search result - onActivityResult - mSport id : " + mSport.getId()
//                            + " mSport name : " + mSport.getName());
                    if(mSelectedSportsList.size() > 1) {
                        if(isAllSportsSelected) {
                            mBinding.tvSportName.setText(getResources().getString(R.string.selected_all_sports_count));
                        } else {
                            mBinding.tvSportName.setText(getResources().getString(R.string.selected_sports_count,
                                    mSelectedSportsList.size()));
                        }
                    } else {
                        mBinding.tvSportName.setText(mSport.getName());
                    }

                    if(!TextUtils.isEmpty(mSport.getBackgroundImage())) {
                        Picasso.with(this)
                                .load(mSport.getBackgroundImage())
//                            .placeholder(R.drawable.ic_user_circle)
//                            .error(R.drawable.ic_user_circle)
                                .into((mBinding.ivBackground));
                    }
                }
            }

            setEventNameDynamically(eventName);

            mBinding.tvBudgetName.setText(getResources().getString(R.string.min_max_budget_range, minBudget, maxBudget));

            if(mSelectedTeamList != null && mSelectedTeamList.size() > 0) {
                if(mSelectedTeamList.size() == 1) {
                    String teamName = mSelectedTeamList.get(0).getName();
                    if(!TextUtils.isEmpty(teamName)) {
                        mBinding.tvTeamName.setText(teamName);
                    } else {
                        mBinding.tvTeamName.setText(getResources().getString(R.string.selected_team_count,
                                mSelectedTeamList.size()));
                    }
                } else {
                    mBinding.tvTeamName.setText(getResources().getString(R.string.selected_teams_count,
                            mSelectedTeamList.size()));
                }
            } else {
                mBinding.tvTeamName.setText(getResources().getString(R.string.all));
            }

            mBinding.tvAddress.setText(mAddress);
        }
    }

    private void callSearchSpeakerApi() {
        if(Utils.isNetworkAvailable()) {
            searchSpeakerBySport();
        } else {
            showSnackbarFromTop(SearchSpeakerResultActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void searchSpeakerBySport() {
//        showProgressBar();
//        Log.d("speaker_log", "Search Speaker - Current Page " + mCurrentPage + " LastPage " + lastPage);
        isLoading = true;
        mResultFound = false;
        if (!isScrolling) {
            mBinding.layoutLoader.setVisibility(View.VISIBLE);
            mBinding.llNextButton.setVisibility(View.GONE);
        }

        SearchSpeakerBySportRequest request = new SearchSpeakerBySportRequest();

        ArrayList<Integer> sportIdsList = new ArrayList<>();
        if(!isAllSportsSelected) {
            for(Sports sports : mSelectedSportsList) {
//                Log.d("sport_log", "Speaker search result - before sending search data - "
//                    + " sports id : " + sports.getId() + " sports name : " + sports.getName());
                sportIdsList.add(sports.getId());
            }
        }

        request.setSportId(sportIdsList);

        ArrayList<Integer> eventIdsList = new ArrayList<>();
        eventIdsList.add(eventId);

        request.setEventId(eventIdsList);

        double minBudgetDouble = (double) minBudget;
        double maxBudgetDouble = (double) maxBudget;
        request.setMinBudget(minBudgetDouble);
        request.setMaxBudget(maxBudgetDouble);

        request.setLatitude(mLatitude);
        request.setLongitude(mLongitude);

        ArrayList<Integer> teamId = new ArrayList<>();
        if(mSelectedTeamList != null && mSelectedTeamList.size() > 0) {
            for(Team team : mSelectedTeamList) {
                if(team != null && team.getId() != 0) {
                    teamId.add(team.getId());
                }
            }
        }
        request.setTeamId(teamId);

        if(gender == Constants.GENDER_TYPE.BOTH_SELECTED) {
            request.setGender(Constants.GENDER_TYPE.NONE);
        } else {
            request.setGender(gender);
        }

        if(travelType == Constants.INTER_STATE_TRAVEL.BOTH_SELECTED) {
            request.setTravelType(Constants.INTER_STATE_TRAVEL.UNSELECTED);
        } else {
            request.setTravelType(travelType);
        }

        ArrayList<Integer> subcategoriesId = new ArrayList<>();
        if(mSelectedEventSubcategories != null && mSelectedEventSubcategories.size() > 0) {
            for(EventSubcategory eventSubcategory : mSelectedEventSubcategories) {
                if(eventSubcategory != null && eventSubcategory.getEventSubcategoryDetail() != null
                        && eventSubcategory.getEventSubcategoryDetail().getId() != 0) {
                    subcategoriesId.add(eventSubcategory.getEventSubcategoryDetail().getId());
                }
            }
        }
        request.setSubcategoriesId(subcategoriesId);

        //request.setPageSize(10);
        //request.setPage(mCurrentPage);
        //request.setFilters(mSearchFilter);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.searchSpeakerBySport(mCurrentPage, 20, request).enqueue(new BaseCallback<SearchSpeakerBySportResponse>(this) {
            @Override
            public void onSuccess(SearchSpeakerBySportResponse response) {

                if(!isScrolling) {
                    if(mSpeakerList != null) {
                        mSpeakerList.clear();
                    }
                    if(mSelectedSpeakerList != null) {
                        mSelectedSpeakerList.clear();
                    }
                }

                isLoading = false;
                isScrolling = false;
                //mBinding.layoutLoader.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.layoutLoader.setVisibility(View.GONE);
                    }
                }, 3000);
                if (response != null && response.getStatus() == 1) {
                    //showToast("Getting speaker list successfully !!");
                    if(response.getResult() != null) {
                        //mCurrentPage++;
                        lastPage = Utils.DivideByFifteenRoundingUp(response.getResult().getTotal() / 10f);
                        if (response.getResult() != null && response.getResult().getSpeakers() != null) {
                            ArrayList<Speaker> localSpeakerList = response.getResult().getSpeakers();

                            if (!mRepeatRequest) {
                                mRepeatRequest = true;
//                                initFilterData(response.getResult());
                            }
                            mSpeakerList.addAll(localSpeakerList);
                            mAdapter.notifyDataSetChanged();

                            //setAdapter();

                            if(mSpeakerList != null) {
                                if(mSpeakerList.size() > 0) {
                                    mResultFound = true;
                                    mBinding.tvNoResults.setVisibility(View.GONE);
                                    mBinding.tvNoResults.setText(getResources().getString(R.string.loading_list_plz_wait));
                                    mBinding.llNextButton.setVisibility(View.VISIBLE);
                                } else {
                                    mBinding.tvNoResults.setVisibility(View.VISIBLE);
                                    mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                                    mBinding.llNextButton.setVisibility(View.GONE);
                                }
                            } else {
                                mBinding.tvNoResults.setVisibility(View.VISIBLE);
                                mBinding.tvNoResults.setText(getResources().getString(R.string.no_results_found));
                                mBinding.llNextButton.setVisibility(View.GONE);
                            }

//                            if (mSpeakerList.size() > 0) {
//                                mResultFound = true;
//                                mBinding.tvNoResults.setVisibility(View.GONE);
//                                mBinding.llNextButton.setVisibility(View.VISIBLE);
//                            } else {
//                                if (mRequestCount == 1) {
//                                    mBinding.tvNoResults.setVisibility(View.VISIBLE);
//                                    mBinding.llNextButton.setVisibility(View.GONE);
//                                } else {
//                                    if(mSpeakerList != null && mSpeakerList.size() > 0) {
//                                        mBinding.tvNoResults.setVisibility(View.VISIBLE);
//                                        mBinding.llNextButton.setVisibility(View.GONE);
//                                    }
//                                }
//                            }

                            mRequestCount++;

//                        for (Coach item : coaches) {
//                            LogUtils.LOGD(TAG, "Sport " + item.getName() + " " + item.getProfileImage());
//                        }
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SearchSpeakerBySportResponse> call, BaseResponse baseResponse) {
                isLoading = false;
                isScrolling = false;
                mBinding.layoutLoader.setVisibility(View.GONE);
                hideProgressBar();
                if(baseResponse != null && baseResponse.getMessage() != null
                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                    showSnackbarFromTop(SearchSpeakerResultActivity.this, baseResponse.getMessage());
                }
            }
        });
    }

//    private void initFilterData(SearchSpeakerBySportData searchSpeakerBySportData) {
//        mFilterData = new FilterData();
//        mFilterData.setMinAge(searchCoachBySportData.getMinAge());
//        mFilterData.setMaxAge(searchCoachBySportData.getMaxAge());
//        mFilterData.setMinDistance(searchCoachBySportData.getMinDistance());
//        mFilterData.setMaxDistance(searchCoachBySportData.getMaxDistance());
//        mFilterData.setMinPrice(searchCoachBySportData.getMinPrice());
//        mFilterData.setMaxPrice(searchCoachBySportData.getMaxPrice());
//        mFilterData.setTeams(searchCoachBySportData.getTeams());
//    }

    @Override
    public void onItemClick(ItemSpeakerResultBinding itemCoachResultBinding, Speaker speaker, int position) {
        if(speaker != null) {
            LogUtils.LOGD(TAG, "speaker : " + speaker.getName()
                + " is selected : " + speaker.isSelected());
            getSpeakerProfile(speaker, position);
        }
    }

    private void getSpeakerProfile(Speaker speaker, int position) {
        int speakerId = speaker.getUserId();
        boolean isSelected = speaker.isSelected();
        Bundle bundleSpeakerProfile = new Bundle();
        bundleSpeakerProfile.putInt(Constants.BundleKey.POSITION, position);
        bundleSpeakerProfile.putInt(Constants.BundleKey.SPEAKER_ID, speakerId);
        bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_SELECTED, isSelected);
        bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_AWAITING_OR_DECLINED, false);
        bundleSpeakerProfile.putBoolean(Constants.BundleKey.IS_FROM_SEARCH, true);
        Navigator.getInstance().navigateToActivityForResultWithData(SearchSpeakerResultActivity.this,
                SpeakerProfileActivity.class, bundleSpeakerProfile, Constants.REQUEST_CODE.REQUEST_CODE_SPEAKER_PROFILE);
    }

    @Override
    public void onSpeakerSelected(int position, boolean isSelected) {
        if(mSpeakerList != null && mSpeakerList.size() > 0) {
            Speaker speaker = mSpeakerList.get(position);
            if(speaker != null) {
                if(isSelected) {
                    if(mSelectedSpeakerList != null && mSelectedSpeakerList.size() > 0) {
                        if(mSelectedSpeakerList.size() == Constants.MAX_SPEAKER_SELECTION_LIMIT) {
                            showSnackbarFromTop(SearchSpeakerResultActivity.this,
                                    getResources().getString(R.string.max_speaker_selection_limit_reached));
                            return;
                        }
                    }
                    speaker.setSelected(true);
                    mSelectedSpeakerList.add(speaker);
                } else {
                    speaker.setSelected(false);
                    mSelectedSpeakerList.remove(speaker);
                }
                if(mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
//                Log.d("speaker_log", "selectedSpeakerList size : " + mSelectedSpeakerList.size());
            }
        }
    }

    @Override
    public void onSpeakerSelectedForChat(int position) {

    }

    //    private void fetchTeamsFromAPI() {
//        Log.d("AddTeamLog", "AddTeam - API HIT");
//        processToShowDialog();
//
//        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
//
//        // ------ New Change - As per client improvement calling favourite teams API , no check is required for learner or a coach. ------//
//
//        TeamListRequest teamListRequest = new TeamListRequest();
//        teamListRequest.setSportId(mSportIdsList);
//
//        webServices.getTeamsList(teamListRequest, mCurrentAPIPage).enqueue(new BaseCallback<TeamsListBaseResponse>(this) {
//            @Override
//            public void onSuccess(TeamsListBaseResponse response) {
//                if (response != null && response.getResult() != null) {
//                    if (response.getResult().getData() != null && response.getResult().getData().size() > 0) {
//                        for (Team team : response.getResult().getData()) {
//                            if(mSelectedTeamsList != null && mSelectedTeamsList.size() > 0) {
//                                if (mSelectedTeamsList.contains(team)) {
//                                    team.setIsSelected(true);
//                                }
//                            }
//                        }
//                        mTeamsList.addAll(response.getResult().getData());
//                        mAddTeamAdapter.notifyDataSetChanged();
//                    }
//                }
//            }
//
//            @Override
//            public void onFail(Call<TeamsListBaseResponse> call, BaseResponse baseResponse) {
//                //LogUtils.LOGD(getActivityName(), baseResponse.getMessage());
//                hideProgressBar();
//            }
//        });
//    }

}
