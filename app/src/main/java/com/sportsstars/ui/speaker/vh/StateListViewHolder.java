package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;

import com.sportsstars.databinding.ItemStatesListViewHolderBinding;
import com.sportsstars.interfaces.TravelInfoListener;
import com.sportsstars.model.TravelViewHolderModel;
import com.sportsstars.ui.speaker.adapter.StateFeeSlotAdapter;

public class StateListViewHolder extends RecyclerView.ViewHolder {

    private ItemStatesListViewHolderBinding mBinding;
    private Activity mActivity;
    private TravelInfoListener mTravelInfoListener;

    public StateListViewHolder(ItemStatesListViewHolderBinding binding, Activity activity, TravelInfoListener travelInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mTravelInfoListener = travelInfoListener;
    }

    public void bindData(TravelViewHolderModel travelViewHolderModel, int position) {
        if(travelViewHolderModel != null) {
            if(position == 1) {
                mBinding.tvStatesHeader.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvStatesHeader.setVisibility(View.GONE);
            }

            mBinding.tvStateName.setText(travelViewHolderModel.getStateName());

            StateFeeSlotAdapter stateFeeSlotAdapter = new StateFeeSlotAdapter(mActivity, travelViewHolderModel,
                    position, travelViewHolderModel.getStateCharges(), mTravelInfoListener);
            mBinding.rvStateSlots.setLayoutManager(new LinearLayoutManager(mActivity));
            mBinding.rvStateSlots.setAdapter(stateFeeSlotAdapter);

            mBinding.switchState.setOnCheckedChangeListener(null);

            if(travelViewHolderModel.isChecked()) {
                mBinding.switchState.setChecked(true);
                mBinding.rvStateSlots.setVisibility(View.VISIBLE);
            } else {
                mBinding.switchState.setChecked(false);
                mBinding.rvStateSlots.setVisibility(View.GONE);
            }

            mBinding.switchState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(mTravelInfoListener != null) {
                        mTravelInfoListener.onStateSwitchChangedListener(position, isChecked);
                    }
                }
            });

        }
    }

}
