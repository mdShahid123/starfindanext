package com.sportsstars.ui.speaker;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iceteck.silicompressorr.SiliCompressor;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySpeakerCreateProfileBinding;
import com.sportsstars.model.AdditionalMedia;
import com.sportsstars.model.Sports;
import com.sportsstars.model.SuburbInfo;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.request.auth.speaker.UpdateSpeakerProfileInfoRequest;
import com.sportsstars.network.response.auth.AdditionalMediaResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.AddTeamActivity;
import com.sportsstars.ui.coach.adapter.AdditionalMediaAdapter;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.search.SearchCoachActivityNext;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.FileUtils;
import com.sportsstars.util.ImagePickerUtils;
import com.sportsstars.util.ImageUtil;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class SpeakerCreateProfileActivity extends BaseActivity implements View.OnClickListener,
        ImagePickerUtils.OnImagePickerListener, AdditionalMediaAdapter.IUploadCallback {

    private static final String TAG = "SpeakerCreateProfile";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1221;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE_VIDEO = 1222;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2122;
    private static final int REQUEST_CODE = 3123;
    private static final int ASPECT_RATIO_SQUARE_X = 1;
    private static final int ASPECT_RATIO_SQUARE_Y = 1;
    private static final int REQUEST_CODE_LOCATION = 101;

    private ActivitySpeakerCreateProfileBinding mBinder;

    private Calendar mCalendar;
    private String mDOB, mDobFormatted;
    private Dialog mDialog;
    private Sports sports;
    private CropImageView mCropImageView;
    private Uri mCropImageUri;
    private SuburbInfo mSuburb;

    private static final int UPLOAD_TYPE = 1;
    private static final int UPLOAD_TYPE_ADDITIONAL_MEDIA_IMAGE = 3;
    private static final int UPLOAD_TYPE_ADDITIONAL_MEDIA_VIDEO = 7;

    private int mGenderType;
    private Bitmap mCroppedImage;
    private ArrayList<Team> mTeamsList = new ArrayList<>();
    private UserProfileModel mProfileModel;
    private UserProfileInstance mUserProfileInstance;
    private List<AdditionalMedia> mMediaPaths = new ArrayList<>();
    private AdditionalMediaAdapter mAdditionalMediaAdapter;
    private boolean isSpeakerProfileFound;

    @Override
    public String getActivityName() {
        return "SpeakerCreateProfile";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_speaker_create_profile);

        //Utils.setTransparentTheme(this);
        sports = new Sports();
        init();
        mGenderType = Constants.GENDER_TYPE.MALE;
        setGenderType(mBinder.rlMale, mBinder.tvMale);
    }

    private void init() {
        mBinder.etAge.setOnClickListener(this);
        mBinder.imageCamera.setOnClickListener(this);
        mBinder.rlMale.setOnClickListener(this);
        mBinder.rlFemale.setOnClickListener(this);
        mBinder.buttonNext.setOnClickListener(this);
        mBinder.etSuburb.setOnClickListener(this);
        mBinder.etSyc.setOnClickListener(this);
        mBinder.etTeamsYouPlayed.setOnClickListener(this);
        mBinder.etTeamsYouPlayed.setFocusable(false);
        mCalendar = Calendar.getInstance();
        setUpHeader(mBinder.headerId, getString(R.string.create_profile), getString(R.string.we_need_to_know));

        mAdditionalMediaAdapter = new AdditionalMediaAdapter(this, this, mMediaPaths);
        mBinder.rvMediaListRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mBinder.rvMediaListRecyclerview.setAdapter(mAdditionalMediaAdapter);

        mBinder.etAboutMe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_about_me) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.performClick();
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mBinder.etAboutMe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.et_about_me) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mBinder.etAccolades.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.et_accolades) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        getSpeakerProfileStatus();
        //getCoachProfileStatus();
    }

    private void showDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(SpeakerCreateProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

//                            Calendar calendar = Calendar.getInstance();
//                            calendar.set(Calendar.YEAR, year);
//                            calendar.set(Calendar.MONTH, month);
//                            calendar.set(Calendar.DAY_OF_MONTH, day);

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormated = yyyymmddFormat.format(mCalendar.getTime());

                        try {
                            if (date != null && isValidDate(date)) {
                                mDOB = date;
                                mDobFormatted = dateFormated;
                                mBinder.etAge.setText(mDOB);
                            } else {
                                mDOB = "";
                                mDobFormatted = "";
                                mBinder.etAge.setText(mDOB);
                                showSnackbarFromTop(SpeakerCreateProfileActivity.this, getString(R.string.invalid_age));
                            }
                            int age = Utils.getAge(mCalendar);

                            if (age < 5) {
                                mDOB = "";
                                mDobFormatted = "";
                                mBinder.etAge.setText(mDOB);
                                showSnackbarFromTop(SpeakerCreateProfileActivity.this, getString(R.string.invalid_age));
                            }
                            LogUtils.LOGD(TAG, "Age:" + Utils.getAge(mCalendar));

                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "Age>" + e.getMessage());

                        }
                    }
                }, year, month, day);

        Calendar minCal = Calendar.getInstance();
        minCal.set(Calendar.YEAR, 1940);
        datePickerDialog.getDatePicker().setMinDate(minCal.getTimeInMillis());

        datePickerDialog.show();
    }

    private void setGenderType(RelativeLayout relSelected, TextView tvSelected) {
        mBinder.rlFemale.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.tvFemale.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        mBinder.rlMale.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.tvMale.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        relSelected.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
        tvSelected.setTextColor(ContextCompat.getColor(this, R.color.green_gender));
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.et_age:
                showDatePicker();
                break;

            case R.id.image_camera:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Permission is granted");
                        startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                    } else {
                        Log.d(TAG, "Permission is revoked");
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Log.d(TAG, "Permission is granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                }
//                if (ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},
//                            MY_PERMISSIONS_REQUEST_CAMERA);
//                } else {
//
//                }
                break;

            case R.id.rlMale:
                mGenderType = Constants.GENDER_TYPE.MALE;
                setGenderType(mBinder.rlMale, mBinder.tvMale);
                break;

            case R.id.rlFemale:
                mGenderType = Constants.GENDER_TYPE.FEMALE;
                setGenderType(mBinder.rlFemale, mBinder.tvFemale);
                break;

            case R.id.button_next:
                if(Utils.isNetworkAvailable()) {
                    if (isInputValid()) {
                        createProfile();
                    }
                } else {
                    showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.et_suburb:
                if(Utils.isNetworkAvailable()) {
                    launchUpdateLocation();
                } else {
                    showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.etSyc:
                if(Utils.isNetworkAvailable()) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.EXTRA_SPORTS_LIST, sports);
                    bundle.putBoolean(Constants.FROM_COACH_INFO, true);
                    Navigator.getInstance().navigateToActivityForResultWithData(SpeakerCreateProfileActivity.this,
                            SearchCoachActivityNext.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST);
                } else {
                    showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.et_teams_you_played:
                if(sports != null) {
                    if(sports.getName() != null && !TextUtils.isEmpty(sports.getName())) {
                        if(sports.getId() != 0) {
                            if(Utils.isNetworkAvailable()) {
                                Bundle b = new Bundle();
                                b.putInt(Constants.BundleKey.ID, sports.getId());
                                b.putParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST, (ArrayList<? extends Parcelable>) mTeamsList);
                                Navigator.getInstance().navigateToActivityForResultWithData(this,
                                        AddTeamActivity.class, b, Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS);
                            } else {
                                showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.no_internet));
                            }
                        } else {
                            showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.please_select_sport));
                        }
                    } else {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.please_select_sport));
                    }
                } else {
                    showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.please_select_sport));
                }
                break;
        }
    }

    public void showCropDialog(final Intent data) {
        mDialog = new Dialog(this, android.R.style.Theme_Light);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_image_crop);
        mCropImageView = (CropImageView) mDialog.findViewById(R.id.image_crop);
        mCropImageView.setAspectRatio(ASPECT_RATIO_SQUARE_X, ASPECT_RATIO_SQUARE_Y);

        Button btnCrop = (Button) mDialog.findViewById(R.id.button_crop);
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                // Bitmap cropped = mCropImageView.getCroppedImage(500, 200);
                Bitmap cropped = mCropImageView.getCroppedImage();
                if (cropped != null) {

                    mBinder.imageUser.setImageBitmap(cropped);
                    mCroppedImage = cropped;

                    Uri imageUri = Utils.getPickImageResultUri(data, SpeakerCreateProfileActivity.this);
                    mCropImageView.setImageUriAsync(imageUri);
                    //LogUtils.LOGD(TAG, " Image Cropped>");

                    Uri uri = Utils.getImageUri(SpeakerCreateProfileActivity.this, cropped);
                    String path = Utils.getRealPathFromURI(SpeakerCreateProfileActivity.this, uri);
                    LogUtils.LOGD("path>>", path);


                    /*
                    showProgressBar();
                    AwsUtil.beginUpload(path, Constants.AWS_FOLDER_PROFILE, AwsUtil.getTransferUtility
                                    (SpeakerCreateProfileActivity.this),
                            SpeakerCreateProfileActivity.this);
*/

                    uploadImage();
                } else {
                    mCroppedImage = null;
                }
            }
        });

        Button buttonCancel = (Button) mDialog.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void getCaptureUri(Intent data) {
        showCropDialog(data);
        Uri imageUri = Utils.getPickImageResultUri(data, this);

        // For API >= 23 we need to check specifically that we have permissions to read external storage,
        // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                Utils.isUriRequiresPermissions(imageUri, this)) {

            // request permissions and handle the result in onRequestPermissionsResult()
            requirePermissions = true;
            mCropImageUri = imageUri;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
            //LogUtils.LOGD(TAG, "Permission Granted>");
        }

        if (!requirePermissions) {
            mCropImageView.setImageUriAsync(imageUri);
            //LogUtils.LOGD(TAG, " Image Cropped>");
            String path = Utils.getRealPathFromURI(SpeakerCreateProfileActivity.this, imageUri);
            LogUtils.LOGD("path>>", path);
            //initS3Upload(path);
            /* showProgressBar();
            AwsUtil.beginUpload(path, Constants.AWS_FOLDER_PROFILE, AwsUtil.getTransferUtility
                            (this),
                    this);*/

            try {
                if (Utils.getBitmapFromUri(this, imageUri) == null) {
                    //LogUtils.LOGD(TAG, " null>");
                    if (mDialog != null && mDialog.isShowing()) {
                        showSnackbarFromTop(this, "Please select valid image");
                        mDialog.dismiss();
                    }
                }
            } catch (IOException e) {
                //LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());
                LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                getCaptureUri(data);
            } else if (requestCode == Constants.GET_VIDEO) {
                if (data.getData() != null) {
                    //create destination directory
                    File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/videos");
                    if (f.mkdirs() || f.isDirectory()) {
                        if(FileUtils.isVideoFileExists(SpeakerCreateProfileActivity.this, data.getData())) {
                            //compress and output new video specs
                            new VideoCompressAsyncTask(this).execute(data.getData().toString(), f.getPath());
                        } else {
                            showSnackbarFromTop(SpeakerCreateProfileActivity.this, getResources().getString(R.string.unable_to_fetch_video));
                        }
                    }
                }
            } else if (requestCode == REQUEST_CODE_LOCATION) {
                if (data != null) {
                    double mLatitude = data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0);
                    double mLongitude = data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0);
                    String mAddress = data.getStringExtra(Constants.EXTRA_ADDRESS);
                    mSuburb = new SuburbInfo();
                    mSuburb.setLng(mLongitude);
                    mSuburb.setLat(mLatitude);
                    mSuburb.setAddress(mAddress);

                    if (mAddress != null) {
                        mBinder.etSuburb.setText(mAddress);
                    }
                } else {
                    mSuburb = null;
                    mBinder.etSuburb.setText("");
                }
            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST) {
                if (data != null) {
                    sports = data.getParcelableExtra(Constants.EXTRA_SPORT);
                    mBinder.etSyc.setText(sports.getName());
                    mBinder.etTeamsYouPlayed.setText("");
                    if(mTeamsList != null && mTeamsList.size() > 0) {
                        mTeamsList.clear();
                    }
                }
            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_ADD_TEAMS) {
                if (data != null) {
                    mTeamsList = data.getParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST);
                    mBinder.etTeamsYouPlayed.setText(Utils.getCommaSepString(mTeamsList));
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE_VIDEO:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    openNativeVideoPicker();
                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            case MY_PERMISSIONS_REQUEST_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCropImageView.setImageUriAsync(mCropImageUri);
                } else {
                    //Toast.makeText(getApplicationContext(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void createProfile() {

        processToShowDialog();

        UpdateSpeakerProfileInfoRequest updateSpeakerRequest = new UpdateSpeakerProfileInfoRequest();
        updateSpeakerRequest.setName(mBinder.etName.getText().toString());
        updateSpeakerRequest.setDob(mDobFormatted);
        updateSpeakerRequest.setSuburb(mSuburb);
        updateSpeakerRequest.setGender(mGenderType);
        updateSpeakerRequest.setSportId(sports.getId());
        updateSpeakerRequest.setAboutMe(mBinder.etAboutMe.getText().toString());
        updateSpeakerRequest.setYoutubeLink("");
        updateSpeakerRequest.getTeamPlayedFor().addAll(Utils.getTeamIdList(mTeamsList));
        updateSpeakerRequest.setAccolades(mBinder.etAccolades.getText().toString());

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateGuestSpeakerProfileInfo(updateSpeakerRequest).enqueue(new BaseCallback<GetProfileResponse>(SpeakerCreateProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    Log.d(TAG, "success : updateGuestSpeakerProfileInfo");
                    mProfileModel = response.getResult();
                    if(mProfileModel != null && mProfileModel.getName() != null
                            && !TextUtils.isEmpty(mProfileModel.getName())) {
                        mUserProfileInstance = UserProfileInstance.getInstance();
                        mUserProfileInstance.setUserProfileModel(mProfileModel);
                        Navigator.getInstance().navigateToActivity(SpeakerCreateProfileActivity.this, AddSpeakingInfoActivity.class);
                        finish();
                    } else {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, "Not getting profile data properly.");
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SpeakerCreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void launchUpdateLocation() {
        Intent intent = new Intent(this, SearchCoachLocationActivity.class);
        intent.putExtra(Constants.EXTRA_TITLE, getString(R.string.select_location));
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    private void uploadImage() {
        showProgressBar();
        File file = ImageUtil.getFileFromBitmap(mCroppedImage, SpeakerCreateProfileActivity.this);

        RequestBody fbody = null;
        if (file != null) {
            fbody = RequestBody.create(MediaType.parse("image"), file);
        }
        RequestBody type = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE);

        AuthWebServices client = RequestController.getInstance().createService(AuthWebServices.class, true);
        client.uploadImage(type, fbody).enqueue(new BaseCallback<BaseResponse>(SpeakerCreateProfileActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    LogUtils.LOGD(TAG, "Image Upload success");
                    // showSnackbarFromTop(SpeakerCreateProfileActivity.this,response.getMessage());
                } else {
                    if(response != null && response.getMessage() != null
                            && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private boolean isInputValid() {
        boolean isValid = true;

        if (TextUtils.isEmpty(mBinder.etName.getText().toString().trim())) {
            isValid = false;
            mBinder.etName.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_name));

        } else if (TextUtils.isEmpty(mDobFormatted)) {
            isValid = false;
            mBinder.etAge.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_age));

        } else if (TextUtils.isEmpty(mBinder.etSuburb.getText().toString().trim())) {
            isValid = false;
            mBinder.etSuburb.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_suburb));

        } else if (StringUtils.isNullOrEmpty(mBinder.etSyc.getText().toString().trim())) {
            isValid = false;
            showSnackbarFromTop(this, getString(R.string.txt_err_sport));

        } else if (TextUtils.isEmpty(mBinder.etTeamsYouPlayed.getText().toString().trim())) {
            isValid = false;
            mBinder.etTeamsYouPlayed.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_teams));

        } else if (TextUtils.isEmpty(mBinder.etAboutMe.getText().toString().trim())) {
            isValid = false;
            mBinder.etAboutMe.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_about_me));

        } else if (TextUtils.isEmpty(mBinder.etAccolades.getText().toString().trim())) {
            isValid = false;
            mBinder.etAccolades.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_accolades));

        }

        return isValid;
    }

    private void getSpeakerProfileStatus() {
        Log.d(TAG, "viewProfile API - speaker called");
        //showToast("Get speaker profile status");
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.GUEST_SPEAKER);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(SpeakerCreateProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    mProfileModel = response.getResult();
                    if(mProfileModel != null
                            && mProfileModel.getName() != null
                            && !TextUtils.isEmpty(mProfileModel.getName())
                            && mProfileModel.getAccolades() != null
                            && !TextUtils.isEmpty(mProfileModel.getAccolades())) {
                        mUserProfileInstance = UserProfileInstance.getInstance();
                        mUserProfileInstance.setUserProfileModel(mProfileModel);
                        isSpeakerProfileFound = true;
                        setPreviousData();
                    } else {
                        isSpeakerProfileFound = false;
                        getCoachProfileStatus();
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SpeakerCreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getCoachProfileStatus() {
        Log.d(TAG, "viewProfile API - coach called");
        //showToast("Get coach profile status");
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(SpeakerCreateProfileActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    mProfileModel = response.getResult();
                    mUserProfileInstance = UserProfileInstance.getInstance();
                    mUserProfileInstance.setUserProfileModel(mProfileModel);
                    setPreviousData();
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SpeakerCreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setPreviousData() {
        if (mProfileModel != null) {

            if (mProfileModel.getSportDetail() != null && !StringUtils.isNullOrEmpty(mProfileModel.getSportDetail().getName())) {
                mBinder.etSyc.setText(mProfileModel.getSportDetail().getName());
                sports.setId(mProfileModel.getSportDetail().getId());
            }
            mBinder.etName.setText(mProfileModel.getName());

            mBinder.etAge.setText(Utils.getUIFormattedDate(mProfileModel.getDob()));
            mDobFormatted = mProfileModel.getDob();

            if (mProfileModel.getSuburb() != null && mProfileModel.getSuburb().getAddress() != null) {
                mSuburb = new SuburbInfo();
                mSuburb = mProfileModel.getSuburb();
                mBinder.etSuburb.setText(mProfileModel.getSuburb().getAddress());
            }

            String aboutMe = "";
            if(mProfileModel.getAboutMe() != null && !TextUtils.isEmpty(mProfileModel.getAboutMe())) {
                aboutMe = mProfileModel.getAboutMe();
            } else if(mProfileModel.getDescription() != null && !TextUtils.isEmpty(mProfileModel.getDescription())) {
                aboutMe = mProfileModel.getDescription();
            }

            // ***** Changed scenario of About Me set text code and teams list code as per client feedback **** //
            if(isSpeakerProfileFound) {
                mBinder.etAboutMe.setText(aboutMe);
                mTeamsList = mProfileModel.getTeamPlayedFor();
                if(mTeamsList != null && mTeamsList.size() > 0) {
                    mBinder.etTeamsYouPlayed.setText(Utils.getCommaSepString(mTeamsList));
                } else {
                    mTeamsList = new ArrayList<>();
                    mBinder.etTeamsYouPlayed.setText("");
                }
            } else {
                mBinder.etAboutMe.setText("");
                if(mProfileModel.getCoachType() == Constants.COACH_TYPE.STAR) {
                    mTeamsList = mProfileModel.getTeamPlayedFor();
                    mBinder.etTeamsYouPlayed.setText(Utils.getCommaSepString(mTeamsList));
                } else {
                    mTeamsList = new ArrayList<>();
                    mBinder.etTeamsYouPlayed.setText("");
                }
            }

//            mBinder.etUploadVideo.setText(mProfileModel.getYoutubeLink());

            //Additional Media
            mAdditionalMediaAdapter.setAdditionalMedias(mProfileModel.getMediaUrl());


            mBinder.etAccolades.setText(mProfileModel.getAccolades());
            if(mSuburb == null || TextUtils.isEmpty(String.valueOf(mSuburb.getLat()))
                    || TextUtils.isEmpty(String.valueOf(mSuburb.getLng()))
                    || TextUtils.isEmpty(mSuburb.getAddress())) {
                mSuburb = new SuburbInfo();
                mSuburb.setLat(mProfileModel.getSuburb().getLat());
                mSuburb.setLng(mProfileModel.getSuburb().getLng());
                mSuburb.setAddress(mProfileModel.getSuburb().getAddress());
            }

            if (mProfileModel.getProfileImage() != null && !TextUtils.isEmpty(mProfileModel.getProfileImage())) {
                Picasso.with(SpeakerCreateProfileActivity.this)
                        .load(mProfileModel.getProfileImage()).placeholder(R.drawable.ic_user_circle).error(R.drawable.ic_user_circle).into(mBinder.imageUser);
            }

            if(mProfileModel.getGender() == Constants.GENDER_TYPE.MALE) {
                mGenderType = Constants.GENDER_TYPE.MALE;
                setGenderType(mBinder.rlMale, mBinder.tvMale);
            } else {
                mGenderType = Constants.GENDER_TYPE.FEMALE;
                setGenderType(mBinder.rlFemale, mBinder.tvFemale);
            }
            if(mProfileModel.getSportDetail() != null && !TextUtils.isEmpty(mProfileModel.getSportDetail().getName())) {
                sports.setId(mProfileModel.getSportDetail().getId());
                sports.setName(mProfileModel.getSportDetail().getName());
                sports.setReferencecLogo(mProfileModel.getSportDetail().getIcon());
            }
        }

        mBinder.etName.requestFocus();
        if (mBinder.etName.getText() != null && !TextUtils.isEmpty(mBinder.etName.getText())) {
            mBinder.etName.setSelection(mBinder.etName.getText().length());
        }

    }

    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(pDateString);
        return new Date().after(date);
    }

    private final Target downloadImage = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (bitmap != null) {
                mBinder.imageUser.setImageBitmap(bitmap);
                mCroppedImage = bitmap;
                uploadImage();
            }

        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    public void success(String name, String path, int type) {
        if(path.endsWith(".png")||path.endsWith(".jpg")||path.endsWith(".jpeg")
                ||path.endsWith(".PNG") ||path.endsWith("JPG")||path.endsWith("JPEG")) {
            uploadImage(new File(path), type);
        } else {
            showSnackbarFromTop(this,"Please select a valid image.");
        }
//        mMediaPaths.add(path);
//        mAdditionalMediaAdapter.notifyDataSetChanged();
    }

    @Override
    public void fail(String message) {
        LogUtils.LOGD(">>>>>>", "" + message);
    }

    @Override
    public void onAddMediaSelected() {
        showAdditionalMediaDialog();
    }

    @Override
    public void onRemoveMediaSelected(int position) {
        removeDoc(mMediaPaths.get(position).getId(),position,false);
    }

    private void showAdditionalMediaDialog() {
        final CharSequence[] items = {"Photos", "Videos","Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Additional Media");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Photos")) {
                    dialog.dismiss();
                    showImagePicker();
                } else if (items[which].equals("Videos")) {
                    dialog.dismiss();
                    checkPermissionAndOpenVideoPicker();
                }
                else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void checkPermissionAndOpenVideoPicker() {
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requirePermissions = true;
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE_VIDEO);
        }

        if (!requirePermissions) {
            openNativeVideoPicker();
        }
    }

    public void showImagePicker() {
        ImagePickerUtils.add((this).getSupportFragmentManager(), this, 3);
    }

    private void openNativeVideoPicker() {
        Intent videoLibraryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        videoLibraryIntent.setType("video/*");
        videoLibraryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(videoLibraryIntent, Constants.GET_VIDEO);
    }

    private void showVideoPreview(Intent data) {
        if (data != null) {
            try {
                Uri uri = data.getData();
                if (uri != null) {
                    String videoPath = FileUtils.getRealVideoPathFromURI(this, uri.toString());
                    if (videoPath != null && videoPath.length() > 0) {
                        Log.d("mylog", "LINE 399 - AddTip - onActivityResult - "
                                + "Video uri : " + uri.getPath()
                                + "\n" + "Video path : " + videoPath);
                        final File sourceFile = new File(videoPath);
                        if (sourceFile.exists()) {
                            if (!FileUtils.isMaxSizeFile(sourceFile)) {
                                String videoThumbnailPath =
                                        FileUtils.createVideoThumbnail(videoPath, PreferenceUtil.getUserModel().getUserId());
                                if (!TextUtils.isEmpty(videoThumbnailPath)) {
                                    File videoThumbFile = new File(videoThumbnailPath);
                                    if (videoThumbFile.exists()) {
                                        Log.d("mylog", "LINE 408 - AddTip - Video thumbnail path : " + videoThumbnailPath);

//                                        videoFilePath = videoPath;
//                                        imageFilePath = videoThumbnailPath;

                                        uploadVideo(videoPath, videoThumbnailPath);
                                    }
                                }
                            } else {
                                showSnackbarFromTop(this,getResources().getString(R.string.max_file_size_limit_reached));
                            }
                        } else {
                            showSnackbarFromTop( this,getResources().getString(R.string.unable_to_fetch_video));
                        }
                    } else {
                        showSnackbarFromTop( this,getResources().getString(R.string.unable_to_fetch_video));
                    }
                } else {
                    com.sportsstars.util.LogUtils.LOGD("mylog", " uri is null");
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    private void showVideoPreview(String videoPath) {
        if (videoPath != null && videoPath.length() > 0) {
            Log.d("mylog", "LINE 399 - AddTip - onActivityResult - "
                    + "\n" + "Video path : " + videoPath);
            final File sourceFile = new File(videoPath);
            if (sourceFile.exists()) {
                if (!FileUtils.isMaxSizeFile(sourceFile)) {
                    String videoThumbnailPath =
                            FileUtils.createVideoThumbnail(videoPath, PreferenceUtil.getUserModel().getUserId());
                    if (!TextUtils.isEmpty(videoThumbnailPath)) {
                        File videoThumbFile = new File(videoThumbnailPath);
                        if (videoThumbFile.exists()) {
                            Log.d("mylog", "LINE 408 - AddTip - Video thumbnail path : " + videoThumbnailPath);
                            uploadVideo(videoPath, videoThumbnailPath);
                        }
                    }
                } else {
                    showSnackbarFromTop(this, getResources().getString(R.string.max_file_size_limit_reached));
                }
            } else {
                showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
            }
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.unable_to_fetch_video));
        }
    }

    private void uploadVideo(String videoPath, String videoThumb) {
        showProgressBar();
        File videoFile = null;
        if (!videoPath.contains("http")) {
            videoFile = new File(videoPath);
            if (videoFile == null || !videoFile.exists()) {
                showSnackbarFromTop(this,getString(R.string.error_msg_no_video_file));
                return;
            }
        }

        File imageFile = null;
        if (!videoThumb.contains("http")) {
            imageFile = new File(videoThumb);
            if (imageFile == null || !imageFile.exists()) {
                showSnackbarFromTop(this,getString(R.string.error_msg_no_image_file));
                return;
            }
        }

        MultipartBody.Part imagePart = null;
        if (imageFile != null) {
            RequestBody imageFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_IMAGE), imageFile);
            imagePart = MultipartBody.Part.createFormData("imageFile", imageFile.getName(), imageFileBody);
        }

        MultipartBody.Part videoPart = null;
        if (videoFile != null) {
            RequestBody videoFileBody = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_VIDEO), videoFile);
            videoPart = MultipartBody.Part.createFormData("videoFile", videoFile.getName(), videoFileBody);
        }

        RequestBody type = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE_ADDITIONAL_MEDIA_VIDEO);

        AuthWebServices client = RequestController.getInstance().createService(AuthWebServices.class, true);
        client.uploadAdditionalMediaVideo(type, imagePart, videoPart).enqueue(new BaseCallback<AdditionalMediaResponse>(this) {
            @Override
            public void onSuccess(AdditionalMediaResponse certificateResponse) {
                if (certificateResponse != null && certificateResponse.getStatus() == 1) {
                    if(certificateResponse.getResult() != null) {
                        mAdditionalMediaAdapter.setAdditionalMedia(certificateResponse.getResult());
//                    showSnackbarFromTop(AddCertificatesActivity.this, mPreviousDocId == 0 ? certificateResponse.getMessage() : getString(R.string.txt_upd_suc));
//                    addOrUpdateCertificate(type);
//                    if (mPreviousDocId != 0)
//                        removeDoc(mPreviousDocId, type, true);
                    }
                } else {
                    if(certificateResponse != null && certificateResponse.getMessage() != null
                            && !TextUtils.isEmpty(certificateResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this,certificateResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<AdditionalMediaResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                hideProgressBar();
            }
        });
    }

    private void uploadImage(final File file, final int tyNipe) {
//        String title = mCertificateList.get(type).getCertificateTitle();
//        if (StringUtils.isNullOrEmpty(title)) {
//            showSnackbarFromTop(this, getString(R.string.invalid_ttl));
//            return;
//        }
//        RequestBody certificateTtl = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), title);
        showProgressBar();
        RequestBody fBody = null;
        if (file != null) {
            fBody = RequestBody.create(MediaType.parse("image/*"), file);
        }
        RequestBody rbType = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE_ADDITIONAL_MEDIA_IMAGE);


        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.uploadAdditionalMedia(rbType, fBody).enqueue(new BaseCallback<AdditionalMediaResponse>(SpeakerCreateProfileActivity.this) {
            @Override
            public void onSuccess(AdditionalMediaResponse certificateResponse) {

                if (certificateResponse != null && certificateResponse.getStatus() == 1) {
                    if(certificateResponse.getResult() != null) {
                        mAdditionalMediaAdapter.setAdditionalMedia(certificateResponse.getResult());
//                    showSnackbarFromTop(AddCertificatesActivity.this, mPreviousDocId == 0 ? certificateResponse.getMessage() : getString(R.string.txt_upd_suc));
//                    addOrUpdateCertificate(type);
//                    if (mPreviousDocId != 0)
//                        removeDoc(mPreviousDocId, type, true);
                    }
                } else {
                    if(certificateResponse != null && certificateResponse.getMessage() != null
                            && !TextUtils.isEmpty(certificateResponse.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, certificateResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<AdditionalMediaResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                hideProgressBar();
            }
        });
    }

    private void removeDoc(int id, final int position, final boolean inBg) {
        if (!inBg)
            showProgressBar();

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.removeAdditionalMedia(id).enqueue(new BaseCallback<BaseResponse>(SpeakerCreateProfileActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (inBg) {
                        return;
                    }
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(SpeakerCreateProfileActivity.this, response.getMessage());
                    }
                    mMediaPaths.remove(position);
                    mAdditionalMediaAdapter.notifyDataSetChanged();
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SpeakerCreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context){
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar("Compressing Video");
        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {
                filePath = SiliCompressor.with(mContext).compressVideo(Uri.parse(paths[0]), paths[1]);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return  filePath;
        }

        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            Log.i("Silicompressor", "Path: "+compressedFilePath);
            hideProgressBar();
            showVideoPreview(compressedFilePath);
        }
    }

}
