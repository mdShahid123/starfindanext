package com.sportsstars.ui.speaker.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemEventSpeakerProfileBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.ui.speaker.vh.SpeakerProfileEventListViewHolder;

import java.util.ArrayList;

public class SpeakerProfileEventListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private ArrayList<EventViewHolderModel> mGlobalEventList;
    private Activity mActivity;
    private ListItemClickListener mListItemClickListener;

    public SpeakerProfileEventListAdapter(Activity activity, ArrayList<EventViewHolderModel> globalEventList,
                                          ListItemClickListener listItemClickListener) {
        mActivity = activity;
        mGlobalEventList = globalEventList;
        mListItemClickListener = listItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = new SpeakerProfileEventListViewHolder(
                (ItemEventSpeakerProfileBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_event_speaker_profile, parent, false), mActivity, mListItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(position);
        ((SpeakerProfileEventListViewHolder) holder).bindData(eventViewHolderModel, position);
    }

    @Override
    public int getItemCount() {
        return mGlobalEventList.size();
    }

}
