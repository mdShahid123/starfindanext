package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemTravelTypeViewHolderBinding;
import com.sportsstars.interfaces.TravelInfoListener;
import com.sportsstars.model.TravelViewHolderModel;
import com.sportsstars.util.Constants;

public class TravelTypeViewHolder extends RecyclerView.ViewHolder {

    private ItemTravelTypeViewHolderBinding mBinding;
    private Activity mActivity;
    private TravelInfoListener mTravelInfoListener;

    public TravelTypeViewHolder(ItemTravelTypeViewHolderBinding binding, Activity activity, TravelInfoListener travelInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mTravelInfoListener = travelInfoListener;
    }

    public void bindData(TravelViewHolderModel travelViewHolderModel, int position) {
        if(travelViewHolderModel != null) {
            if(travelViewHolderModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.UNSELECTED) {
                setInterStateType(mBinding.rlNo, mBinding.tvNo, true);
            } else if(travelViewHolderModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.NO) {
                setInterStateType(mBinding.rlNo, mBinding.tvNo, false);
            } else if(travelViewHolderModel.getInterStateTravel() == Constants.INTER_STATE_TRAVEL.YES) {
                setInterStateType(mBinding.rlYes, mBinding.tvYes, false);
            }
            if(travelViewHolderModel.getTravelType() == Constants.TYPE_OF_TRAVEL.UNSELECTED) {
                setTravelType(mBinding.rlEconomy, mBinding.tvEconomy, mBinding.tvEconomyInfo, true);
            } if(travelViewHolderModel.getTravelType() == Constants.TYPE_OF_TRAVEL.ECONOMY) {
                setTravelType(mBinding.rlEconomy, mBinding.tvEconomy, mBinding.tvEconomyInfo, false);
            } else if(travelViewHolderModel.getTravelType() == Constants.TYPE_OF_TRAVEL.VIP) {
                setTravelType(mBinding.rlVip, mBinding.tvVip, mBinding.tvVipInfo, false);
            }
            setClickListeners();
        }
    }

    private void setClickListeners() {
        mBinding.rlNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onInterStateTravelClickListener(Constants.INTER_STATE_TRAVEL.NO);
                }
            }
        });

        mBinding.rlYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onInterStateTravelClickListener(Constants.INTER_STATE_TRAVEL.YES);
                }
            }
        });

        mBinding.rlEconomy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onTravelTypeClickListener(Constants.TYPE_OF_TRAVEL.ECONOMY);
                }
            }
        });

        mBinding.rlVip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onTravelTypeClickListener(Constants.TYPE_OF_TRAVEL.VIP);
                }
            }
        });

        mBinding.rlEconomyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onTravelTypeInfoClickListener(Constants.TYPE_OF_TRAVEL.ECONOMY);
                }
            }
        });

        mBinding.rlVipInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTravelInfoListener != null) {
                    mTravelInfoListener.onTravelTypeInfoClickListener(Constants.TYPE_OF_TRAVEL.VIP);
                }
            }
        });
    }

    private void setInterStateType(RelativeLayout relSelected, TextView tvSelected, boolean isDefault) {
        mBinding.rlNo.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_grey));
        mBinding.tvNo.setTextColor(ContextCompat.getColor(mActivity, R.color.black_25));

        mBinding.rlYes.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_grey));
        mBinding.tvYes.setTextColor(ContextCompat.getColor(mActivity, R.color.black_25));

        if(!isDefault) {
            relSelected.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_border_green_bg_grey));
            tvSelected.setTextColor(ContextCompat.getColor(mActivity, R.color.green_gender));
        }
    }

    private void setTravelType(RelativeLayout relSelected, TextView tvSelected, TextView tvSelectedInfo, boolean isDefault) {
        mBinding.rlEconomy.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_grey));
        mBinding.tvEconomy.setTextColor(ContextCompat.getColor(mActivity, R.color.black_25));

        mBinding.rlVip.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_grey));
        mBinding.tvVip.setTextColor(ContextCompat.getColor(mActivity, R.color.black_25));

        mBinding.tvEconomyInfo.setTextColor(ContextCompat.getColor(mActivity,R.color.black_25));
        mBinding.tvEconomyInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);

        mBinding.tvVipInfo.setTextColor(ContextCompat.getColor(mActivity,R.color.black_25));
        mBinding.tvVipInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);

        if(!isDefault) {
            relSelected.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_border_green_bg_grey));
            tvSelected.setTextColor(ContextCompat.getColor(mActivity, R.color.green_gender));

            tvSelectedInfo.setTextColor(ContextCompat.getColor(mActivity,R.color.green_gender));
            tvSelectedInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
        }
    }

}
