package com.sportsstars.ui.speaker;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityEventBookingFormBinding;
import com.sportsstars.eventbus.BackNavigationEvent;
import com.sportsstars.model.Speaker;
import com.sportsstars.model.SpeakerFilterData;
import com.sportsstars.model.UserModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.speaker.CreateSpeakerBookingRequest;
import com.sportsstars.network.response.LearnerEventListResponse;
import com.sportsstars.network.response.speaker.EventSlot;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.LoginActivity;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.widget.AddDollarTextWatcher;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;

public class EventBookingFormActivity extends BaseActivity implements View.OnClickListener {

    private static final int MAX_BUDGET_LIMIT = 5000;
    private static final int REQUEST_CODE_LOCATION = 101;

    private ActivityEventBookingFormBinding mBinding;

    private ArrayList<Speaker> mSelectedSpeakerList;
    private ArrayList<Speaker> mRemainingSpeakerList;
    private ArrayList<Speaker> mAllSpeakerList = new ArrayList<>();
    private SpeakerFilterData filterData;

    private String mSelectedDate, mSelectedTime, mSessionDateTimeUtc, mDescription, mSuggestion;
    private boolean isMaxTwentyChecked;
    private int mNoOfAttendeesSelected;
    private int mDurationInterval;
    private String[] durationArray;

    private Calendar mCalendar;
    private String mDOB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_event_booking_form);

        getIntentData();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.LEARNER);
    }

    @Override
    public void onBackPressed() {
        boolean isLogin = PreferenceUtil.getIsLogin();
        if (isLogin) {
            UserModel userModel = PreferenceUtil.getUserModel();
            if(userModel != null) {
                // Navigate to clear stack
                if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                    Navigator.getInstance().navigateToActivityWithClearStack(EventBookingFormActivity.this, WelcomeActivity.class);
                } else {
                    Navigator.getInstance().navigateToActivityWithClearStack(EventBookingFormActivity.this, WelcomeActivity.class);
                }
                try {
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            showToast(getString(R.string.pls_login));
        }
        super.onBackPressed();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.SPEAKERS_LIST)) {
                mSelectedSpeakerList = bundle.getParcelableArrayList(Constants.BundleKey.SPEAKERS_LIST);
                if(mSelectedSpeakerList != null && mSelectedSpeakerList.size() > 0) {
                    Log.d("booking_log", "selected speaker count : " + mSelectedSpeakerList.size());
                }
            }
            if(getIntent().hasExtra(Constants.BundleKey.REMAINING_SPEAKERS_LIST)) {
                mRemainingSpeakerList = bundle.getParcelableArrayList(Constants.BundleKey.REMAINING_SPEAKERS_LIST);
                if(mRemainingSpeakerList != null && mRemainingSpeakerList.size() > 0) {
                    Log.d("booking_log", "remaining speaker count : " + mRemainingSpeakerList.size());
                    for(Speaker speaker : mRemainingSpeakerList) {
                        Log.d("booking_log", "Event booking screen -  speaker id : " + speaker.getUserId()
                                + " speaker name : " + speaker.getName());
                    }
                }
            }
            if(getIntent().hasExtra(Constants.BundleKey.SPEAKER_FILTER_DATA)) {
                filterData = bundle.getParcelable(Constants.BundleKey.SPEAKER_FILTER_DATA);
            }
        }
    }

    private void initViews() {

        //mBinding.svBookingForm.fullScroll(ScrollView.FOCUS_UP);
        mBinding.svBookingForm.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBinding.svBookingForm.fullScroll(ScrollView.FOCUS_UP);
            }
        }, 600);

        mBinding.buttonSend.setOnClickListener(this);
        mBinding.tvDateName.setOnClickListener(this);
        mBinding.tvTimeName.setOnClickListener(this);
        mBinding.ivDisclaimer.setOnClickListener(this);
        mBinding.ivDescriptionEdit.setOnClickListener(this);
        mBinding.ivSuggestionEdit.setOnClickListener(this);
        mBinding.etLocationName.setOnClickListener(this);

        mCalendar = Calendar.getInstance();

        if(isMaxTwentyChecked) {
            mBinding.ivDisclaimer.setImageResource(R.drawable.ic_check_selected);
        } else {
            mBinding.ivDisclaimer.setImageResource(R.drawable.ic_check_unselected);
        }

        if(filterData != null) {
            if(filterData.getSelectedEventViewHolder() != null
                && !TextUtils.isEmpty(filterData.getSelectedEventViewHolder().getName())) {
                mBinding.tvEventName.setText(filterData.getSelectedEventViewHolder().getName());
            }

            mBinding.tvBudgetName.setText(getResources().getString(R.string.max_budget_with_dollar,
                    (int) filterData.getMaxBudget()));
            mBinding.tvBudgetName.addTextChangedListener(new AddDollarTextWatcher(mBinding.tvBudgetName));

            filterData.setAddress("");
            filterData.setLatitude(0);
            filterData.setLongitude(0);
//            if(!TextUtils.isEmpty(filterData.getAddress())) {
//                mBinding.etLocationName.setText(filterData.getAddress());
//            }

            //set no. of attendees spinner
            ArrayAdapter<String> noOfAttendeesAdapter = new ArrayAdapter<String>(this,
                    R.layout.item_spinner_transparent,
                    getResources().getStringArray(R.array.no_of_attendees_array)) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        tv.setTextColor(ContextCompat.getColor(EventBookingFormActivity.this, R.color.hint_text_color));
                    } else {
                        tv.setTextColor(ContextCompat.getColor(EventBookingFormActivity.this, R.color.txt_clr_green));
                    }
                    return view;
                }
            };
            mBinding.spinnerAttendees.setAdapter(noOfAttendeesAdapter);
            noOfAttendeesAdapter.setDropDownViewResource(R.layout.item_booking_spinner);

            mBinding.spinnerAttendees.setSelection(-1);
            mBinding.spinnerAttendees.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (position == 0) {
                            return;
                        }
                        mBinding.ivSpinnerAttendees.setImageResource(R.drawable.ic_dropdown_selected);
                        mBinding.tvSpinnerLabelAttendees.setText(String.valueOf(parent.getItemAtPosition(position)));
                        mBinding.tvSpinnerLabelAttendees.setTextColor(ContextCompat.getColor(EventBookingFormActivity.this, R.color.selected_item_text_color));

                        if(position == 1) {
                            mNoOfAttendeesSelected = Constants.NUMBER_OF_ATTENDEES_TYPE.ATTENDEES_1_1O;
                        } else if(position == 2) {
                            mNoOfAttendeesSelected = Constants.NUMBER_OF_ATTENDEES_TYPE.ATTENDEES_10_2O;
                        } else if(position == 3) {
                            mNoOfAttendeesSelected = Constants.NUMBER_OF_ATTENDEES_TYPE.ATTENDEES_2O_50;
                        } else if(position == 4) {
                            mNoOfAttendeesSelected = Constants.NUMBER_OF_ATTENDEES_TYPE.ATTENDEES_5O_100;
                        } else if(position == 5) {
                            mNoOfAttendeesSelected = Constants.NUMBER_OF_ATTENDEES_TYPE.ATTENDEES_100_PLUS;
                        }

                        mBinding.spinnerAttendees.setSelection(-1);
                        ((TextView) view).setText(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            if(filterData.getSelectedEventViewHolder() != null
                    && filterData.getSelectedEventViewHolder().getEventSlot() != null
                    && filterData.getSelectedEventViewHolder().getEventSlot().size() > 0) {
                int arraySize = filterData.getSelectedEventViewHolder().getEventSlot().size()+1;
                durationArray = new String[arraySize];
                durationArray[0] = "SELECT DURATION INTERVAL";
                for(int i = 0; i < filterData.getSelectedEventViewHolder().getEventSlot().size(); i++) {
                    int arrayCount = i+1;
                    EventSlot eventSlot = filterData.getSelectedEventViewHolder().getEventSlot().get(i);
                    if(eventSlot != null && eventSlot.getId() != 0) {
                        durationArray[arrayCount] = String.valueOf(eventSlot.getSlotInterval());
                    }
                }

                //set duration spinner
                ArrayAdapter<String> durationAdapter = new ArrayAdapter<String>(this,
                        R.layout.item_spinner_transparent, durationArray) {
                    @Override
                    public boolean isEnabled(int position) {
                        return position != 0;
                    }

                    @Override
                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            tv.setTextColor(ContextCompat.getColor(EventBookingFormActivity.this, R.color.hint_text_color));
                        } else {
                            tv.setTextColor(ContextCompat.getColor(EventBookingFormActivity.this, R.color.txt_clr_green));
                        }
                        return view;
                    }
                };
                mBinding.spinnerDuration.setAdapter(durationAdapter);
                durationAdapter.setDropDownViewResource(R.layout.item_booking_spinner);

                mBinding.spinnerDuration.setSelection(-1);
                mBinding.spinnerDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            return;
                        }

                        mDurationInterval = Integer.parseInt(String.valueOf(parent.getItemAtPosition(position)));

                        mBinding.ivSpinner.setImageResource(R.drawable.ic_dropdown_selected);
                        mBinding.tvSpinnerLabel.setText(getResources().getString(R.string.selected_duration_interval,
                                String.valueOf(parent.getItemAtPosition(position))));
                        mBinding.tvSpinnerLabel.setTextColor(ContextCompat.getColor(EventBookingFormActivity.this,
                                R.color.selected_item_text_color));

                        mBinding.spinnerAttendees.setSelection(-1);
                        ((TextView) view).setText(null);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_send:
                if(isInputValid()) {
                    if(Utils.isNetworkAvailable()) {
                        boolean isLogin = PreferenceUtil.getIsLogin();
                        if (isLogin) {
                            UserModel userModel = PreferenceUtil.getUserModel();
                            if(userModel != null) {
                                //showBookingConfirmationDialog();
                                createSpeakerBooking();
                            } else {
                                showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.user_not_logged_in));
                            }
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, true);
                            Navigator.getInstance().navigateToActivityForResultWithData(EventBookingFormActivity.this,
                                    LoginActivity.class, bundle, Constants.REQUEST_CODE.REQUEST_CODE_LOGIN_STATUS);
                        }
                    } else {
                        showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.no_internet));
                    }
                }
                break;
            case R.id.tv_date_name:
                //showToast("Open date picker");
                showDatePicker();
                break;
            case R.id.tv_time_name:
                //showToast("Open time picker");
                setTime();
                break;
            case R.id.iv_disclaimer:
                if(isMaxTwentyChecked) {
                    isMaxTwentyChecked = false;
                    mBinding.ivDisclaimer.setImageResource(R.drawable.ic_check_unselected);
                } else {
                    isMaxTwentyChecked = true;
                    mBinding.ivDisclaimer.setImageResource(R.drawable.ic_check_selected);
                }
                break;
            case R.id.iv_description_edit:
                //showToast("Open date picker");
                showSoftKeyboard(mBinding.tvDescriptionName);
                break;
            case R.id.iv_suggestion_edit:
                //showToast("Open time picker");
                showSoftKeyboard(mBinding.tvSuggestionName);
                break;
            case R.id.et_location_name:
                if(Utils.isNetworkAvailable()) {
                    launchUpdateLocation();
                } else {
                    showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private void showBookingConfirmationDialog() {
        Alert.showEventBookingConfirmationDialog(EventBookingFormActivity.this,
                getResources().getString(R.string.title_event_confirmation_dialog),
                getResources().getString(R.string.message_event_confirmation_dialog),
                getResources().getString(R.string.text_event_confirmation_dialog_disclaimer),
                getResources().getString(R.string.text_event_confirmation_dialog_terms),
                getResources().getString(R.string.no_return),
                getResources().getString(R.string.yes_proceed),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        createSpeakerBooking();
                    }
                },
                new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {

                    }
                }
        );
    }

    private boolean isInputValid() {
        if(filterData == null) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.event_data_empty));
            return false;
        }
        if(filterData.getSelectedEventViewHolder() == null) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.event_data_empty));
            return false;
        }
        if(mDurationInterval == 0) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_duration));
            return false;
        }

        String maxBudgetStrWithDollar = mBinding.tvBudgetName.getText().toString().trim();
        if(TextUtils.isEmpty(maxBudgetStrWithDollar)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.budget_empty));
            return false;
        } else if(!TextUtils.isEmpty(maxBudgetStrWithDollar)
                && maxBudgetStrWithDollar.length() == 1
                && maxBudgetStrWithDollar.equals("$")) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.budget_empty));
            return false;
        }

        String maxBudgetStr = "";

        if(!TextUtils.isEmpty(maxBudgetStrWithDollar)
                && maxBudgetStrWithDollar.length() > 1
                &&  Character.toString(maxBudgetStrWithDollar.charAt(0)).equals("$")) {
            maxBudgetStr = maxBudgetStrWithDollar.substring(1, maxBudgetStrWithDollar.length());
        }

        if(TextUtils.isEmpty(maxBudgetStr)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.budget_empty));
            return false;
        }

        int maxBudget = Integer.parseInt(maxBudgetStr);
        if(maxBudget > MAX_BUDGET_LIMIT) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.budget_limit));
            return false;
        }

        filterData.setMaxBudget((double) maxBudget);

        if(filterData.getAddress() == null) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.address_not_found));
            return false;
        }
        if(TextUtils.isEmpty(filterData.getAddress())) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.address_not_found));
            return false;
        }
        if(filterData.getLatitude() == 0) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.address_not_found));
            return false;
        }
        if(filterData.getLongitude() == 0) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.address_not_found));
            return false;
        }
        if(mSelectedDate == null) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_valid_date));
            return false;
        }
        if(TextUtils.isEmpty(mSelectedDate)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_valid_date));
            return false;
        }

        if(mSelectedTime == null) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_valid_time));
            return false;
        }
        if(TextUtils.isEmpty(mSelectedTime)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_valid_time));
            return false;
        }

        mSelectedTime = mSelectedTime + ":00";
        String sessionDateAndTime = mSelectedDate + " " + mSelectedTime;
        mSessionDateTimeUtc = DateFormatUtils.convertLocalToUtc(sessionDateAndTime,
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);

        if(mSessionDateTimeUtc == null) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_valid_date_time));
            return false;
        }
        if(TextUtils.isEmpty(mSessionDateTimeUtc)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_valid_date_time));
            return false;
        }
        if(mNoOfAttendeesSelected == 0) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_select_no_of_attendees));
            return false;
        }

        mDescription = mBinding.tvDescriptionName.getText().toString();
        if(TextUtils.isEmpty(mDescription)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_enter_your_desc));
            return false;
        }

        mSuggestion = mBinding.tvSuggestionName.getText().toString();
        if(TextUtils.isEmpty(mSuggestion)) {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.please_enter_your_suggestion));
            return false;
        }

        return true;
    }

    private void createSpeakerBooking() {

        processToShowDialog();

        CreateSpeakerBookingRequest createSpeakerBookingRequest = new CreateSpeakerBookingRequest();
        createSpeakerBookingRequest.setEventId(filterData.getSelectedEventViewHolder().getId());
        createSpeakerBookingRequest.setDuration(mDurationInterval);
        createSpeakerBookingRequest.setBudget(filterData.getMaxBudget());
        createSpeakerBookingRequest.setSuburb(filterData.getAddress());
        createSpeakerBookingRequest.setLatitude(filterData.getLatitude());
        createSpeakerBookingRequest.setLongitude(filterData.getLongitude());
        createSpeakerBookingRequest.setSessionDateTime(mSessionDateTimeUtc);
        createSpeakerBookingRequest.setNoOfAttendees(mNoOfAttendeesSelected);
        createSpeakerBookingRequest.setDescription(mDescription);
        createSpeakerBookingRequest.setSuggestions(mSuggestion);
        createSpeakerBookingRequest.setMaxTwenty(isMaxTwentyChecked);

        ArrayList<Integer> selectedSpeakerIdList = new ArrayList<>();
        if(!isMaxTwentyChecked) {
            if(mSelectedSpeakerList != null && mSelectedSpeakerList.size() > 0) {
                for(Speaker speaker : mSelectedSpeakerList) {
                    if(speaker != null && speaker.getUserId() != 0) {
                        selectedSpeakerIdList.add(speaker.getUserId());
                    }
                }
            }
        } else {
            mAllSpeakerList.addAll(mSelectedSpeakerList);
            mAllSpeakerList.addAll(mRemainingSpeakerList);
            if(mAllSpeakerList != null && mAllSpeakerList.size() > 0) {
                for(Speaker speaker : mAllSpeakerList) {
                    if(speaker != null && speaker.getUserId() != 0) {
                        selectedSpeakerIdList.add(speaker.getUserId());
                    }
                }
            }
        }

        createSpeakerBookingRequest.setSpeakerId(selectedSpeakerIdList);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.createSpeakerBooking(createSpeakerBookingRequest).enqueue(new BaseCallback<BaseResponse>(this) {

                    @Override
                    public void onSuccess(BaseResponse response) {
                        if(response != null) {
                            if (response.getStatus() == 1) {
                                try {
                                    showBookingSentInfoDialog();
//                                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
//                                        showToast(response.getMessage());
//                                        //showSnackbarFromTop(EventBookingFormActivity.this, response.getMessage());
//                                    }
//                                    launchEventListScreen();
//                                    //getLearnerEventListing();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                        showSnackbarFromTop(EventBookingFormActivity.this, response.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                        try {
                            hideProgressBar();
                            if (baseResponse != null && baseResponse.getMessage() != null
                                    && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                showSnackbarFromTop(EventBookingFormActivity.this, baseResponse.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    private void launchEventListScreen() {
        if(Utils.isNetworkAvailable()) {
            EventBus.getDefault().post(new BackNavigationEvent(true));
            finish();
            Bundle bundleSpeaker = new Bundle();
            bundleSpeaker.putInt(Constants.BundleKey.LOAD_FRAGMENT_POSITION, 1);
            Navigator.getInstance().navigateToActivityWithData(EventBookingFormActivity.this,
                    LearnerHomeActivity.class, bundleSpeaker);
        } else {
            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void getLearnerEventListing() {

        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getLearnerEventList(1, 10).enqueue(new BaseCallback<LearnerEventListResponse>(this) {

            @Override
            public void onSuccess(LearnerEventListResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(EventBookingFormActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(EventBookingFormActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<LearnerEventListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(EventBookingFormActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(EventBookingFormActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

//                            Calendar calendar = Calendar.getInstance();
//                            calendar.set(Calendar.YEAR, year);
//                            calendar.set(Calendar.MONTH, month);
//                            calendar.set(Calendar.DAY_OF_MONTH, day);

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormatSelected = new SimpleDateFormat();
                        simpleDateFormatSelected.applyPattern(Utils.DATE_YYYY_MM_DD);
                        mSelectedDate = simpleDateFormatSelected.format(mCalendar.getTime());

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MMM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormated = yyyymmddFormat.format(mCalendar.getTime());

                        try {
                            if (date != null) {
                                mDOB = date;
                                mBinding.tvDateName.setText(mDOB);
                                mBinding.tvDateName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dropdown_selected,0);
                            } else {
                                mDOB = "";
                                mBinding.tvDateName.setText(mDOB);
                                mBinding.tvDateName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dropdown,0);
                                showSnackbarFromTop(EventBookingFormActivity.this, getString(R.string.invalid_date));
                            }
                        } catch (Exception e) {
                            LogUtils.LOGE("event_date", "Event date error : " + e.getMessage());
                        }
                    }
                }, year, month, day);

        Calendar minCal = Calendar.getInstance();
        minCal.set(Calendar.DAY_OF_MONTH, mCalendar.get(Calendar.DAY_OF_MONTH)+1);
        //minCal.set(Calendar.MONTH, mCalendar.get(Calendar.MONTH));
        //minCal.set(Calendar.YEAR, mCalendar.get(Calendar.YEAR));
        datePickerDialog.getDatePicker().setMinDate(minCal.getTimeInMillis());

        datePickerDialog.show();
    }

    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(pDateString);
        return new Date().after(date);
    }

    private void setTime() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String min;
                        String hour;
                        if(minute < 10) {
                            min = "0"+minute;
                        } else {
                            min = ""+minute;
                        }
                        if(hourOfDay < 10) {
                            hour = "0"+hourOfDay;
                        } else {
                            hour = ""+hourOfDay;
                        }
                        mSelectedTime = hour + ":" + min;
                        String readableTime = Utils.getReadableTimeUI(hourOfDay, min);
                        mBinding.tvTimeName.setText(readableTime);
                        mBinding.tvTimeName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dropdown_selected,0);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_LOGIN_STATUS) {
            if (resultCode == RESULT_OK) {
                if(Utils.isNetworkAvailable()) {
                    boolean isLogin = PreferenceUtil.getIsLogin();
                    if (isLogin) {
                        UserModel userModel = PreferenceUtil.getUserModel();
                        if(userModel != null) {
                            //showBookingConfirmationDialog();
                            createSpeakerBooking();
                        } else {
                            showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.user_not_logged_in));
                        }
                    } else {
                        showSnackBar(getString(R.string.pls_login));
                    }
                } else {
                    showSnackbarFromTop(EventBookingFormActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        } else if(requestCode == REQUEST_CODE_LOCATION) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    filterData.setLatitude(data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0));
                    filterData.setLongitude(data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0));
                    filterData.setAddress(data.getStringExtra(Constants.EXTRA_ADDRESS));

                    if (filterData.getAddress() != null) {
//                            Log.d("location_log", "SearchSpeaker - ActivityResult - mAddressString : " + mAddress
//                                    + " latitude : " + mLatitude + " longitude : " + mLongitude);
//                        mBinding.tvLocationName.setText(filterData.getAddress());

                        if(filterData.getAddress().equalsIgnoreCase("Unknown")) {
                            filterData.setLatitude(0);
                            filterData.setLongitude(0);
                            filterData.setAddress("");
                            mBinding.etLocationName.setText(filterData.getAddress());
                            return;
                        }

                        if(!TextUtils.isEmpty(filterData.getAddress())
                                && !filterData.getAddress().equalsIgnoreCase("Unknown")) {
                            mBinding.etLocationName.setText(filterData.getAddress());
                        }
                    } else {
                        filterData.setLatitude(0);
                        filterData.setLongitude(0);
                        filterData.setAddress("");
                        mBinding.etLocationName.setText(filterData.getAddress());
                    }
                }
            }
        }
    }

    private void showBookingSentInfoDialog() {
        String title = getResources().getString(R.string.booking_sent_dialog_title);
        String message = getResources().getString(R.string.booking_sent_dialog_message);
        Alert.showAlertDialog(EventBookingFormActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        launchEventListScreen();
                    }
                });
    }

    private void launchUpdateLocation() {
        Intent intent = new Intent(this, SearchCoachLocationActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    @Override
    public String getActivityName() {
        return "EventBookingForm";
    }

}
