package com.sportsstars.ui.speaker.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.SpeakerEventListItemBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.response.LearnerEvent;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SpeakerEventAdapter extends RecyclerView.Adapter<SpeakerEventAdapter.MyViewHolder> {

    private ArrayList<LearnerEvent> mEventList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ListItemClickListener mListItemClickListener;

    public SpeakerEventAdapter(Activity mActivity, ArrayList<LearnerEvent> mEventList,
                               ListItemClickListener mListItemClickListener) {
        this.mEventList = mEventList;
        this.mActivity = mActivity;
        this.mListItemClickListener = mListItemClickListener;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public SpeakerEventAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SpeakerEventListItemBinding mBinding = DataBindingUtil.inflate(mInflator, R.layout.speaker_event_list_item, parent, false);
        return new SpeakerEventAdapter.MyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(SpeakerEventAdapter.MyViewHolder holder, int position) {
        if (mEventList != null && mEventList.size() > 0) {
            holder.bindData(mEventList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        SpeakerEventListItemBinding speakerEventListItemBinding;

        public MyViewHolder(SpeakerEventListItemBinding speakerEventListItemBinding) {
            super(speakerEventListItemBinding.getRoot());
            this.speakerEventListItemBinding = speakerEventListItemBinding;
        }

        public SpeakerEventListItemBinding getBinding() {
            return speakerEventListItemBinding;
        }

        public void bindData(LearnerEvent learnerEvent, int position) {
            speakerEventListItemBinding.setLearnerEvent(learnerEvent);
            if(learnerEvent != null && !TextUtils.isEmpty(learnerEvent.getEventLogo())) {
                Picasso.with(mActivity)
                        .load(learnerEvent.getEventLogo())
                        .placeholder(R.drawable.ic_birthday)
                        .error(R.drawable.ic_birthday)
                        .into(speakerEventListItemBinding.ivEventLogo);
            }
            if(learnerEvent != null && !TextUtils.isEmpty(learnerEvent.getProfileImage())) {
                Picasso.with(mActivity)
                        .load(learnerEvent.getProfileImage())
                        .placeholder(R.drawable.ic_nav_profile_unselected)
                        .error(R.drawable.ic_nav_profile_unselected)
                        .into(speakerEventListItemBinding.ivProfilePic);
            }
            if(learnerEvent != null && !TextUtils.isEmpty(learnerEvent.getSessionStartTime())) {
                String utcDateTime = learnerEvent.getSessionStartTime();
                String localDateTime = DateFormatUtils.convertUtcToLocal(utcDateTime,
                        DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME);
                Log.d("mylog", "SpeakerEventAdapter - utcDateTime : " + utcDateTime
                        + " :: localDateTime : " + localDateTime);
                speakerEventListItemBinding.tvSessionDate.setText(Utils.getFormattedDateTime(localDateTime));
                speakerEventListItemBinding.tvSessionTime.setText(Utils.parseTimeForDDMM(localDateTime));
            }

            if(learnerEvent != null) {
                if(learnerEvent.getStatus() == Utils.STATUS_BOOKING.PAYMENT_PENDING
                        || learnerEvent.getStatus() == Utils.STATUS_BOOKING.AWAITING_RESPONSE) {
                    if(learnerEvent.getSpeakerStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.AWAITING) {
                        learnerEvent.setStatus(Utils.STATUS_BOOKING.AWAITING_RESPONSE);
                    } else if(learnerEvent.getSpeakerStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.ACCEPTED) {
                        learnerEvent.setStatus(Utils.STATUS_BOOKING.PAYMENT_PENDING);
                    } else if(learnerEvent.getSpeakerStatus() == Constants.SPEAKER_AVAILABILITY_STATUS.REJECTED) {
                        learnerEvent.setStatus(Utils.STATUS_BOOKING.CANCELLED);
                    }
                    String upcomingText = Utils.getCurrentAndUpcomingText(learnerEvent.getStatus());
                    speakerEventListItemBinding.tvUpcoming.setText(upcomingText);
                    speakerEventListItemBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(mActivity,
                            learnerEvent.getStatus()));
                } else {
                    String upcomingText = Utils.getCurrentAndUpcomingText(learnerEvent.getStatus());
                    speakerEventListItemBinding.tvUpcoming.setText(upcomingText);
                    speakerEventListItemBinding.tvUpcoming.setBackground(Utils.getCurrentAndUpcomingBackground(mActivity,
                            learnerEvent.getStatus()));
                }
            }

            speakerEventListItemBinding.rlSessionItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListItemClickListener.onListItemClicked(position);
                }
            });
        }

    }

}
