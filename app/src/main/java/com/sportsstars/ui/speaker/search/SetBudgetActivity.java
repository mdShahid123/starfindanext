package com.sportsstars.ui.speaker.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySetBudgetBinding;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.widget.AddDollarTextWatcher;

public class SetBudgetActivity extends BaseActivity {

    private ActivitySetBudgetBinding mBinding;
    private int minBudget, maxBudget;
    private static final int MAX_BUDGET_LIMIT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_set_budget);

        getIntentData();
        initViews();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.MIN_BUDGET)) {
                minBudget = bundle.getInt(Constants.BundleKey.MIN_BUDGET, 0);
            }
            if(getIntent().hasExtra(Constants.BundleKey.MAX_BUDGET)) {
                maxBudget = bundle.getInt(Constants.BundleKey.MAX_BUDGET, 0);
            }
        }
    }

    private void initViews() {

        //mBinding.etMinBudget.setText(String.valueOf(minBudget));
        //mBinding.etMaxBudget.setText(String.valueOf(maxBudget));

        mBinding.etMinBudget.setText(getResources().getString(R.string.min_budget_with_dollar, minBudget));
        mBinding.etMaxBudget.setText(getResources().getString(R.string.max_budget_with_dollar, maxBudget));

        mBinding.etMinBudget.setSelection(mBinding.etMinBudget.getText().toString().length());
        mBinding.etMaxBudget.setSelection(mBinding.etMaxBudget.getText().toString().length());

        mBinding.buttonSaveBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInputValid()) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.BundleKey.MIN_BUDGET, minBudget);
                    intent.putExtra(Constants.BundleKey.MAX_BUDGET, maxBudget);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        mBinding.etMinBudget.addTextChangedListener(new AddDollarTextWatcher(mBinding.etMinBudget));
        mBinding.etMaxBudget.addTextChangedListener(new AddDollarTextWatcher(mBinding.etMaxBudget));

//        mBinding.etMinBudget.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try {
//                    if(s.toString().length() == 1 && s.toString().equals("$")) {
//                        minBudget = 0;
//                        //mBinding.etMinBudget.setText("$");
//                        //mBinding.etMinBudget.setSelection(mBinding.etMinBudget.getText().length());
//                    } else if(s.toString().length() == 1 && !s.toString().equals("$")) {
//                        minBudget = Integer.parseInt(s.toString());
//                        mBinding.etMinBudget.setText(getResources().getString(R.string.min_budget_with_dollar, minBudget));
//                        //mBinding.etMinBudget.setSelection(mBinding.etMinBudget.getText().length());
//                    } else if(s.toString().length() > 1
//                            //&& !s.toString().substring(0,1).equals("$")
//                            && !Character.toString(s.toString().charAt(0)).equals("$")) {
//                        minBudget = Integer.parseInt(s.toString());
//                        //mBinding.etMinBudget.setText(getResources().getString(R.string.min_budget_with_dollar, minBudget));
//                    } else if(s.toString().length() > 1
//                            //&& s.toString().substring(0,1).equals("$")
//                            && Character.toString(s.toString().charAt(0)).equals("$")) {
//                        String integerStr = s.toString().substring(1, s.toString().length());
//                        minBudget = Integer.parseInt(integerStr);
//                        //mBinding.etMinBudget.setText(getResources().getString(R.string.min_budget_with_dollar, minBudget));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        mBinding.etMaxBudget.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
////                try {
////                    if(s.toString().length() == 1 && s.toString().equals("$")) {
////                        maxBudget = 0;
////                        mBinding.etMaxBudget.setText("$");
////                        mBinding.etMaxBudget.setSelection(mBinding.etMaxBudget.getText().length());
////                    } else if(s.toString().length() == 1 && !s.toString().equals("$")) {
////                        maxBudget = Integer.parseInt(s.toString());
////                        mBinding.etMaxBudget.setText(getResources().getString(R.string.max_budget_with_dollar, maxBudget));
////                        mBinding.etMaxBudget.setSelection(mBinding.etMaxBudget.getText().length());
////                    } else if(s.toString().length() > 1
////                            //&& !s.toString().substring(0,1).equals("$")
////                            && !Character.toString(s.toString().charAt(0)).equals("$")) {
////                        maxBudget = Integer.parseInt(s.toString());
////                        mBinding.etMaxBudget.setText(getResources().getString(R.string.max_budget_with_dollar, maxBudget));
////                    } else if(s.toString().length() > 1
////                            //&& s.toString().substring(0,1).equals("$")
////                            && Character.toString(s.toString().charAt(0)).equals("$")) {
////                        String integerStr = s.toString().substring(1, s.toString().length());
////                        maxBudget = Integer.parseInt(integerStr);
////                        mBinding.etMaxBudget.setText(getResources().getString(R.string.max_budget_with_dollar, maxBudget));
////                    }
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
//            }
//        });
    }

    private boolean isInputValid() {
        try {
            String minBudgetStrWithDollar = mBinding.etMinBudget.getText().toString().trim();
            String maxBudgetStrWithDollar = mBinding.etMaxBudget.getText().toString().trim();

            if(TextUtils.isEmpty(minBudgetStrWithDollar)) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.min_budget_empty));
                return false;
            } else if(TextUtils.isEmpty(maxBudgetStrWithDollar)) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.max_budget_empty));
                return false;
            } else if(!TextUtils.isEmpty(minBudgetStrWithDollar)
                    && minBudgetStrWithDollar.length() == 1
                    && minBudgetStrWithDollar.equals("$")) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.min_budget_empty));
                return false;
            } else if(!TextUtils.isEmpty(maxBudgetStrWithDollar)
                    && maxBudgetStrWithDollar.length() == 1
                    && maxBudgetStrWithDollar.equals("$")) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.max_budget_empty));
                return false;
            }

            String minBudgetStr = "";
            String maxBudgetStr = "";

            if(!TextUtils.isEmpty(minBudgetStrWithDollar)
                    && minBudgetStrWithDollar.length() > 1
                    &&  Character.toString(minBudgetStrWithDollar.charAt(0)).equals("$")) {
                minBudgetStr = minBudgetStrWithDollar.substring(1, minBudgetStrWithDollar.length());
            }

            if(!TextUtils.isEmpty(maxBudgetStrWithDollar)
                    && maxBudgetStrWithDollar.length() > 1
                    &&  Character.toString(maxBudgetStrWithDollar.charAt(0)).equals("$")) {
                maxBudgetStr = maxBudgetStrWithDollar.substring(1, maxBudgetStrWithDollar.length());
            }

            if(TextUtils.isEmpty(minBudgetStr)) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.min_budget_empty));
                return false;
            } else if(TextUtils.isEmpty(maxBudgetStr)) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.max_budget_empty));
                return false;
            }

            minBudget = Integer.parseInt(minBudgetStr);
            maxBudget = Integer.parseInt(maxBudgetStr);

            if(maxBudget == 0) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.max_budget_not_zero));
                return false;
            } else if(maxBudget <= minBudget) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.max_budget_not_less_than_min_budget));
                return false;
            } else if(minBudget >= MAX_BUDGET_LIMIT) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.min_budget_limit));
                return false;
            } else if(maxBudget > MAX_BUDGET_LIMIT) {
                showSnackbarFromTop(SetBudgetActivity.this, getResources().getString(R.string.max_budget_limit));
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String getActivityName() {
        return "SetBudget";
    }

}
