package com.sportsstars.ui.speaker.vh;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemTravelSlotViewHolderBinding;
import com.sportsstars.interfaces.TravelInfoListener;
import com.sportsstars.model.TravelViewHolderModel;
import com.sportsstars.network.response.speaker.StateCharges;

public class StateSlotViewHolder extends RecyclerView.ViewHolder {

    private ItemTravelSlotViewHolderBinding mBinding;
    private Activity mActivity;
    private TravelInfoListener mTravelInfoListener;

    public StateSlotViewHolder(ItemTravelSlotViewHolderBinding binding, Activity activity, TravelInfoListener travelInfoListener) {
        super(binding.getRoot());
        mBinding = binding;
        mActivity = activity;
        mTravelInfoListener = travelInfoListener;
    }

    public void bindData(TravelViewHolderModel travelViewHolderModel, int parentPosition, StateCharges stateCharges, int slotPosition) {
        if(stateCharges != null) {
            mBinding.tvSlotName.setText(stateCharges.getFeeText());

            if(stateCharges.getIsSuggested() == 1
                    && !TextUtils.isEmpty(String.valueOf(stateCharges.getSuggestedCharge()))
                    && stateCharges.getSuggestedCharge() != 0) {
                mBinding.tvSlotSuggested.setVisibility(View.VISIBLE);
                mBinding.tvSlotSuggested.setText("(suggested $"+stateCharges.getSuggestedCharge()+")");
            } else {
                mBinding.tvSlotSuggested.setVisibility(View.INVISIBLE);
            }

            if(stateCharges.getGuestSpeakerStates() != null) {
                if(!TextUtils.isEmpty(String.valueOf(stateCharges.getGuestSpeakerStates().getSpeakerCharge()))
                        && stateCharges.getGuestSpeakerStates().getSpeakerCharge() != 0) {
                    mBinding.tvSlotFee.setText("$"+String.valueOf(stateCharges.getGuestSpeakerStates().getSpeakerCharge()));
                } else {
                    if(stateCharges.getIsSuggested() == 1
                            && !TextUtils.isEmpty(String.valueOf(stateCharges.getSuggestedCharge()))
                            && stateCharges.getSuggestedCharge() != 0) {
                        mBinding.tvSlotFee.setText("");
                        mBinding.tvSlotFee.setHint("ENTER $"+stateCharges.getSuggestedCharge());
                    } else {
                        mBinding.tvSlotFee.setText("");
                        mBinding.tvSlotFee.setHint(mActivity.getResources().getString(R.string.enter_fee));
                    }
                }
            } else {
                if(stateCharges.getIsSuggested() == 1
                        && !TextUtils.isEmpty(String.valueOf(stateCharges.getSuggestedCharge()))
                        && stateCharges.getSuggestedCharge() != 0) {
                    mBinding.tvSlotFee.setText("");
                    mBinding.tvSlotFee.setHint("ENTER $"+stateCharges.getSuggestedCharge());
                } else {
                    mBinding.tvSlotFee.setText("");
                    mBinding.tvSlotFee.setHint(mActivity.getResources().getString(R.string.enter_fee));
                }
            }
            int gstValue = travelViewHolderModel.getGstValue();
            if(gstValue == 0) {
                gstValue = 10;
            }
            //String gstTaxText = "(inc. "+gstValue+"% GST)";
            String gstTaxText = "(Pls add GST to price)";
            mBinding.tvSlotTax.setText(gstTaxText);
            mBinding.tvSlotFee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mTravelInfoListener != null) {
                        mTravelInfoListener.onStateFeeClickListener(parentPosition, slotPosition);
                    }
                }
            });
        }
    }

}
