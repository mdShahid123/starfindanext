package com.sportsstars.ui.speaker;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddSpeakingInfoBinding;
import com.sportsstars.interfaces.EventInfoListener;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.speaker.UpdateSpeakerEventInfoRequest;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.speaker.EventCategory;
import com.sportsstars.network.response.speaker.EventList;
import com.sportsstars.network.response.speaker.EventListResponse;
import com.sportsstars.network.response.speaker.EventSlot;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.network.response.speaker.EventSubcategoryDetail;
import com.sportsstars.network.response.speaker.GuestSpeakerEvents;
import com.sportsstars.network.response.speaker.PostEventSubcategory;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import retrofit2.Call;

public class AddSpeakingInfoActivity extends BaseActivity implements EventInfoListener, Alert.OnFeeDialogButtonClickListener {

    private static final int REQUEST_CODE_SLOT_FEE = 3001;
    private static final int REQUEST_CODE_EVENT_SUBCATEGORIES = 4001;

    private ActivityAddSpeakingInfoBinding mBinding;

    private UserProfileModel mProfileModel;
    private UserProfileInstance mUserProfileInstance;
    private String mSpecialities = "";
    private ArrayList<EventViewHolderModel> mGlobalEventList = new ArrayList<>();
    private EventInfoAdapter mEventInfoAdapter;
    private int mParentPosition, mSlotPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_speaking_info);

        setUpHeader(mBinding.headerId, getString(R.string.add_speaking_info_title), getString(R.string.add_speaking_info_subtitle));

        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        //generateSpecialitiesObject();
        if(Utils.isNetworkAvailable()) {
            getEventList();
        } else {
            generateSpecialitiesObject();
            setEventList();
            logGlobalEventList();
            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.no_internet));
        }

        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable()) {
                    if(isInputValid()) {
                        Alert.showOkCancelDialog(AddSpeakingInfoActivity.this,
                                getResources().getString(R.string.speaking_info_alert_title),
                                getResources().getString(R.string.speaking_info_alert_msg),
                                getResources().getString(R.string.cancel),
                                getResources().getString(R.string.okay),
                                new Alert.OnOkayClickListener() {
                                    @Override
                                    public void onOkay() {
                                        updateGuestSpeakerEventInfo();
                                    }
                                },
                                new Alert.OnCancelClickListener() {
                                    @Override
                                    public void onCancel() {

                                    }
                                }
                        );
                        //updateGuestSpeakerEventInfo();
                    }
                    //Navigator.getInstance().navigateToActivity(AddSpeakingInfoActivity.this, AddTravelInfoActivity.class);
                } else {
                    showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });

    }

    private boolean isInputValid() {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
//            String specialities = mGlobalEventList.get(0).getSpecialities();
//            if(specialities == null) {
//                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_specialities));
//                return false;
//            } else if(TextUtils.isEmpty(specialities)) {
//                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_specialities));
//                return false;
//            }

            if(!isAnyEventEnabled()) {
                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_slot_price));
                return false;
            } else if(!isEnabledEventHasPriceData()) {
                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_price));
                return false;
            } else if(!isEnabledEventHasSpecialitiesData()) {
                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_specialities_for_event));
                return false;
            } else if(!isEventContainsFeeAndSubcategories()) {
                return false;
            }

//            if(!isEventHasData()) {
//                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_slot_price));
//                return false;
//            } else if(!isEventHasSpecialitiesData()) {
//                showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_add_subcategories));
//                return false;
//            }
        }
        return true;
    }

    private boolean isAnyEventEnabled() {
        boolean isEventEnabled = false;
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for (int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if (eventViewHolderModel != null && eventViewHolderModel.isChecked()) {
                    isEventEnabled = true;
                    break;
                }
            }
        }

        return isEventEnabled;
    }

    private boolean isEventHasData() {
        boolean isEventHasData = false;
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for(int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if(eventViewHolderModel != null) {
                    ArrayList<EventSlot> eventSlots = eventViewHolderModel.getEventSlot();
                    if(eventSlots != null && eventSlots.size() > 0) {
                        int eventSlotsSize = eventSlots.size();
                        int slotPosition = 0;
                        for(int j = 0; j < eventSlots.size(); j++) {
                            EventSlot eventSlot = eventSlots.get(j);
                            if(eventSlot != null) {
                                GuestSpeakerEvents guestSpeakerEvents = eventSlot.getGuestSpeakerEvents();
                                if(guestSpeakerEvents != null) {
                                    double slotPrice = guestSpeakerEvents.getSlotPrice();
                                    if(slotPrice == 0) {
                                        //isEventHasData = false;
                                        Log.d("speaker_event_log", "inside loop - "
                                                + " if slotPrice == 0 - slotPrice : " + slotPrice
                                                + " :: isEventHasData : " + isEventHasData
                                                + " :: slotPosition : " + slotPosition
                                                + " :: slotText : " + eventSlot.getSlotText()
                                        );
                                        //return isEventHasData;
                                        slotPosition++;
                                    } else if(slotPrice != 0 && slotPrice > 0) {
                                        slotPosition++;
                                        //isEventHasData = true;
                                        Log.d("speaker_event_log", "inside loop - "
                                                + " if slotPrice > 0 - slotPrice : " + slotPrice
                                                + " :: isEventHasData : " + isEventHasData
                                                + " :: slotPosition : " + slotPosition
                                                + " :: slotText : " + eventSlot.getSlotText()
                                        );
                                        //return isEventHasData;
                                    }
                                } else {
                                    //isEventHasData = false;
                                    //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                    //return isEventHasData;
                                }
                            }
                        }

//                        // This condition is used when we need any 1 slot in event slots must be entered with price.
//                        if(slotPosition > 0) {
//                            isEventHasData = true;
//                            Log.d("speaker_event_log", "outside loop - if slotPosition > 0 " +
//                                    "- isEventHasData : " + isEventHasData);
//                            return isEventHasData;
//                        }

                        // This condition is used when we need all slots in event slots must be entered with price.
                        // Commented below code as per requirement because at least 1 slot must be entered with price
                        // and other slots can be left empty.
                        if(slotPosition == eventSlotsSize) {
                            isEventHasData = true;
                            Log.d("speaker_log", "outside loop - slotPosition == eventSlotsSize "
                                    + " - isEventHasData : " + isEventHasData
                                    + " slotPosition : " + slotPosition
                                    + " eventSlotsSize : " + eventSlotsSize
                            );
                            return isEventHasData;
                        }

                    }
                }
            }
        }
        Log.d("speaker_event_log", "finally after parent loop - isEventHasData : " + isEventHasData);
        return isEventHasData;
    }

    private boolean isEnabledEventHasPriceData() {
        boolean isEventHasData = false;
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for(int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if(eventViewHolderModel != null && eventViewHolderModel.isChecked()) {
                    ArrayList<EventSlot> eventSlots = eventViewHolderModel.getEventSlot();
                    if(eventSlots != null && eventSlots.size() > 0) {
                        int eventSlotsSize = eventSlots.size();
                        int slotPosition = 0;
                        for(int j = 0; j < eventSlots.size(); j++) {
                            EventSlot eventSlot = eventSlots.get(j);
                            if(eventSlot != null) {
                                GuestSpeakerEvents guestSpeakerEvents = eventSlot.getGuestSpeakerEvents();
                                if(guestSpeakerEvents != null) {
                                    double slotPrice = guestSpeakerEvents.getSlotPrice();
                                    if(slotPrice == 0) {
                                        //isEventHasData = false;
                                        //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                        //return isEventHasData;
                                        slotPosition++;
                                    } else if(slotPrice != 0 && slotPrice > 0) {
                                        Log.d("check_slot_count", "outside loop - slotPrice : " + slotPrice
                                                + " for event : " + eventViewHolderModel.getName()
                                                + " for slot name : " + eventSlot.getSlotText()
                                        );
                                        slotPosition++;
                                        //isEventHasData = true;
                                        //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                        //return isEventHasData;
                                    }
                                } else {
                                    //isEventHasData = false;
                                    //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                    //return isEventHasData;
                                }
                            }
                        }

                        Log.d("check_slot_count", "outside loop - slotPosition : " + slotPosition
                                + " for event : " + eventViewHolderModel.getName());

//                        // This condition is used when we need any 1 slot in event slots must be entered with price.
//                        if(slotPosition > 0) {
//                            isEventHasData = true;
////                            Log.d("speaker_log_first", "outside loop - slotPosition > 0 " +
////                                    "- isEventHasData : " + isEventHasData + " for " + eventViewHolderModel.getName());
//                            return isEventHasData;
//                        }

                        // This condition is used when we need all slots in event slots must be entered with price.
                        // Commented below code as per requirement because at least 1 slot must be entered with price
                        // and other slots can be left empty.
                        if(slotPosition == eventSlotsSize) {
                            isEventHasData = true;
                            Log.d("speaker_log", "outside loop - slotPosition == eventSlotsSize "
                                    + " - isEventHasData : " + isEventHasData
                                    + " slotPosition : " + slotPosition
                                    + " eventSlotsSize : " + eventSlotsSize
                            );
                            return isEventHasData;
                        }

                    }
                }
            }
        }
//        Log.d("speaker_log_first", "after loop - isEventHasData : " + isEventHasData);
        return isEventHasData;
    }

    private boolean isEnabledEventHasSpecialitiesData() {
        boolean isEventHasSpecialitiesData = false;
        int selectedEventSubcategoriesCount = 0;
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for(int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if(eventViewHolderModel != null && eventViewHolderModel.isChecked()) {
                    if(eventViewHolderModel.getSelectedEventSubcategories() == null) {
                        //isEventHasSpecialitiesData = false;
                        //return isEventHasSpecialitiesData;
                    }
                    if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() == 0) {
                        //isEventHasSpecialitiesData = false;
                        //return isEventHasSpecialitiesData;
                    } else if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                        //isEventHasSpecialitiesData = true;
                        selectedEventSubcategoriesCount++;
                    }
                }
            }
        }

        if(selectedEventSubcategoriesCount > 0) {
            isEventHasSpecialitiesData = true;
        }
        Log.d("speaker_event_log", "finally after parent loop - isEventHasSpecialitiesData : "
                + isEventHasSpecialitiesData);
        return isEventHasSpecialitiesData;
    }

    private boolean isEventHasSpecialitiesData() {
        boolean isEventHasSpecialitiesData = false;
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for(int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if(eventViewHolderModel != null) {
                    if(eventViewHolderModel.isChecked()) {
                        int selectedEventSubcategoriesCount = 0;
                        if(eventViewHolderModel.getSelectedEventSubcategories() == null) {
                            isEventHasSpecialitiesData = false;
                            return isEventHasSpecialitiesData;
                        }
                        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() == 0) {
                            isEventHasSpecialitiesData = false;
                            return isEventHasSpecialitiesData;
                        } else if(eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                            isEventHasSpecialitiesData = true;
                        }
                    }
                }
            }
        }
        Log.d("speaker_event_log", "finally after parent loop - isEventHasSpecialitiesData : "
                + isEventHasSpecialitiesData);
        return isEventHasSpecialitiesData;
    }

    private boolean hasEventSpecialitiesData(EventViewHolderModel eventViewHolderModel) {
        boolean hasSpecialitiesData = false;
        if(eventViewHolderModel != null) {
            if(eventViewHolderModel.isChecked()) {
                int selectedEventSubcategoriesCount = 0;
                if(eventViewHolderModel.getSelectedEventSubcategories() == null) {
                    hasSpecialitiesData = false;
                    return hasSpecialitiesData;
                }
                if(eventViewHolderModel.getSelectedEventSubcategories() != null
                        && eventViewHolderModel.getSelectedEventSubcategories().size() == 0) {
                    hasSpecialitiesData = false;
                    return hasSpecialitiesData;
                } else if(eventViewHolderModel.getSelectedEventSubcategories() != null
                        && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                    hasSpecialitiesData = true;
                }
            }
        }
        Log.d("speaker_event_log", "hasEventSpecialitiesData : "
                + hasSpecialitiesData);
        return hasSpecialitiesData;
    }

    private int getEventDataSlotPosition() {
        int eventPosition = -1;
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for(int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if(eventViewHolderModel != null) {
                    ArrayList<EventSlot> eventSlots = eventViewHolderModel.getEventSlot();
                    if(eventSlots != null && eventSlots.size() > 0) {
                        int eventSlotsSize = eventSlots.size();
                        int slotPosition = 0;
                        for(int j = 0; j < eventSlots.size(); j++) {
                            EventSlot eventSlot = eventSlots.get(j);
                            if(eventSlot != null) {
                                GuestSpeakerEvents guestSpeakerEvents = eventSlot.getGuestSpeakerEvents();
                                if(guestSpeakerEvents != null) {
                                    double slotPrice = guestSpeakerEvents.getSlotPrice();
                                    if(slotPrice == 0) {
                                        //isEventHasData = false;
                                        //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                        //return isEventHasData;
                                        slotPosition++;
                                    } else if(slotPrice != 0 && slotPrice > 0) {
                                        slotPosition++;
                                        //isEventHasData = true;
                                        //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                        //return isEventHasData;
                                    }
                                } else {
                                    //isEventHasData = false;
                                    //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                    //return isEventHasData;
                                }
                            }
                        }

                        if(slotPosition == eventSlotsSize) {
                            eventPosition = i;
                            Log.d("speaker_log", "outside loop - slotPosition == eventSlotsSize " +
                                    "- eventPosition : " + eventPosition);
                            return eventPosition;
                        }
                    }
                }
            }
        }
        Log.d("speaker_log", "after loop - eventPosition : " + eventPosition);
        return eventPosition;
    }

    private boolean hasEventData(EventViewHolderModel eventViewHolderModel) {
        boolean isEventHasData = false;
        if(eventViewHolderModel != null) {
            ArrayList<EventSlot> eventSlots = eventViewHolderModel.getEventSlot();
            if(eventSlots != null && eventSlots.size() > 0) {
                int eventSlotsSize = eventSlots.size();
                int slotPosition = 0;
                for(int j = 0; j < eventSlots.size(); j++) {
                    EventSlot eventSlot = eventSlots.get(j);
                    if(eventSlot != null) {
                        GuestSpeakerEvents guestSpeakerEvents = eventSlot.getGuestSpeakerEvents();
                        if(guestSpeakerEvents != null) {
                            double slotPrice = guestSpeakerEvents.getSlotPrice();
                            if(slotPrice == 0) {
                                //isEventHasData = false;
                                //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                //return isEventHasData;
                                slotPosition++;
                            } else if(slotPrice != 0 && slotPrice > 0) {
                                slotPosition++;
                                //isEventHasData = true;
                                //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                                //return isEventHasData;
                            }
                        } else {
                            //isEventHasData = false;
                            //Log.d("speaker_log", "inside loop - isEventHasData : " + isEventHasData);
                            //return isEventHasData;
                        }
                    }
                }

//                // This condition is used when we need any 1 slot in event slots must be entered with price.
//                if(slotPosition > 0) {
//                    isEventHasData = true;
//                    Log.d("speaker_event_log", "outside loop - if slotPosition > 0 " +
//                            "- isEventHasData : " + isEventHasData);
//                    return isEventHasData;
//                }

                // This condition is used when we need all slots in event slots must be entered with price.
                // Commented below code as per requirement because at least 1 slot must be entered with price
                // and other slots can be left empty.
                if(slotPosition == eventSlotsSize) {
                    isEventHasData = true;
                    Log.d("speaker_log", "outside loop - slotPosition == eventSlotsSize "
                            + " - isEventHasData : " + isEventHasData
                            + " slotPosition : " + slotPosition
                            + " eventSlotsSize : " + eventSlotsSize
                    );
                    return isEventHasData;
                }

            }
        }
        Log.d("speaker_event_log", "finally after parent loop - isEventHasData : " + isEventHasData);
        return isEventHasData;
    }

    private void updateGuestSpeakerEventInfo() {

        UpdateSpeakerEventInfoRequest updateSpeakerEventInfoRequest = new UpdateSpeakerEventInfoRequest();
        //updateSpeakerEventInfoRequest.setSpecialities(mGlobalEventList.get(0).getSpecialities());
        ArrayList<GuestSpeakerEvents> guestSpeakerEventsList = getGuestSpeakerEventsList();
        ArrayList<PostEventSubcategory> postEventSubcategoriesList = getPostEventSubcategoriesList();
        int guestSpeakerEventsListSize = guestSpeakerEventsList.size();
        int eventSubcategoriesListSize = postEventSubcategoriesList.size();
//        Log.d("speaker_log", "updateEventInfo - guestSpeakerEventsList final size : " + guestSpeakerEventsListSize);
        if(guestSpeakerEventsListSize == 0) {
            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_price));
        } else if(eventSubcategoriesListSize == 0) {
            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_select_subcategories));
        } else {
            updateSpeakerEventInfoRequest.setGuestSpeakerEvents(guestSpeakerEventsList);
            updateSpeakerEventInfoRequest.setPostEventSubcategories(postEventSubcategoriesList);

            processToShowDialog();

            //String updateRequestJson =  new Gson().toJson(updateSpeakerEventInfoRequest);
            //Log.d("speaker_log", "updateEventInfo - updateRequestJson : " + updateRequestJson);

            AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
            webServices.updateGuestSpeakerEventInfo(updateSpeakerEventInfoRequest).enqueue(
                    new BaseCallback<GetProfileResponse>(AddSpeakingInfoActivity.this) {
                        @Override
                        public void onSuccess(GetProfileResponse response) {
                            if (response != null && response.getStatus() == 1) {
                                Log.d(TAG, "success : updateGuestSpeakerEventInfo");
                                mProfileModel = response.getResult();
                                if(mProfileModel != null && mProfileModel.getName() != null
                                        && !TextUtils.isEmpty(mProfileModel.getName())) {
                                    mUserProfileInstance = UserProfileInstance.getInstance();
                                    mUserProfileInstance.setUserProfileModel(mProfileModel);
                                    Navigator.getInstance().navigateToActivity(AddSpeakingInfoActivity.this, AddTravelInfoActivity.class);
                                    finish();
                                } else {
                                    showSnackbarFromTop(AddSpeakingInfoActivity.this, "Not getting profile data properly.");
                                }
                            } else {
                                try {
                                    if(response != null && response.getMessage() != null
                                            && !TextUtils.isEmpty(response.getMessage())) {
                                        showSnackbarFromTop(AddSpeakingInfoActivity.this, response.getMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                            try {
                                hideProgressBar();
                                if(baseResponse != null && baseResponse.getMessage() != null
                                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                                    showSnackbarFromTop(AddSpeakingInfoActivity.this, baseResponse.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }

    }

    private boolean isEventContainsFeeAndSubcategories() {
        boolean hasFeeAndSubcategories = false;
        boolean hasEventData = false;
        boolean hasSpecialitiesData = false;

        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for (int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if (eventViewHolderModel != null && eventViewHolderModel.isChecked()) {
                    hasEventData = hasEventData(eventViewHolderModel);
                    hasSpecialitiesData = hasEventSpecialitiesData(eventViewHolderModel);
                    if(!hasEventData && !hasSpecialitiesData) {
                        Log.d("event_log", "both are empty.");
                    } else if(!hasEventData && hasSpecialitiesData) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_price));
                        hasFeeAndSubcategories = false;
                        return hasFeeAndSubcategories;
                    } else if(!hasSpecialitiesData && hasEventData) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, getEmptySubcategoriesErrorMessage(eventViewHolderModel));
                        hasFeeAndSubcategories = false;
                        return hasFeeAndSubcategories;
                    } else if(hasEventData && hasSpecialitiesData) {
                        hasFeeAndSubcategories = true;
                    }
                }
            }
        }

        return hasFeeAndSubcategories;
    }

    private ArrayList<GuestSpeakerEvents> getGuestSpeakerEventsList() {
        boolean hasEventData = false;
        boolean hasSpecialitiesData = false;
        ArrayList<GuestSpeakerEvents> guestSpeakerEventsList = new ArrayList<>();
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for (int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if (eventViewHolderModel != null && eventViewHolderModel.isChecked()) {
                    hasEventData = hasEventData(eventViewHolderModel);
                    hasSpecialitiesData = hasEventSpecialitiesData(eventViewHolderModel);
                    if(!hasEventData && !hasSpecialitiesData) {
                        Log.d("event_log", "both are empty.");
                    } else if(!hasEventData && hasSpecialitiesData) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_price));
                        break;
                    } else if(!hasSpecialitiesData && hasEventData) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, getEmptySubcategoriesErrorMessage(eventViewHolderModel));
                        break;
                    } else if(hasEventData && hasSpecialitiesData) {
                        ArrayList<EventSlot> eventSlotsList = eventViewHolderModel.getEventSlot();
                        if(eventSlotsList != null && eventSlotsList.size() > 0) {
                            for(int j = 0; j < eventSlotsList.size(); j++) {
                                EventSlot eventSlot = eventSlotsList.get(j);
                                if(eventSlot != null) {
                                    GuestSpeakerEvents guestSpeakerEvents = eventSlot.getGuestSpeakerEvents();
                                    if(guestSpeakerEvents != null) {
                                        Log.d("speaker_event_log", "add getGuestSpeakerEvents data into list - "
                                                + " getId : " + guestSpeakerEvents.getId()
                                                + " getEventSlotId : " + guestSpeakerEvents.getEventSlotId()
                                                + " getSlotPrice : " + guestSpeakerEvents.getSlotPrice()
                                                + " getUserId : " + guestSpeakerEvents.getUserId()
                                        );
                                        //guestSpeakerEvents.setEventId(eventViewHolderModel.getId());
                                        //guestSpeakerEvents.setSelectedSubcategoryIds(getSelectedSubcategoryIds(eventViewHolderModel));
                                        guestSpeakerEventsList.add(guestSpeakerEvents);
                                    }
                                }
                            }
                        }
                    }

//                    if(hasEventData) {
//                        ArrayList<EventSlot> eventSlotsList = eventViewHolderModel.getEventSlot();
//                        if(eventSlotsList != null && eventSlotsList.size() > 0) {
//                            for(int j = 0; j < eventSlotsList.size(); j++) {
//                                EventSlot eventSlot = eventSlotsList.get(j);
//                                if(eventSlot != null) {
//                                    GuestSpeakerEvents guestSpeakerEvents = eventSlot.getGuestSpeakerEvents();
//                                    if(guestSpeakerEvents != null) {
//                                        Log.d("speaker_event_log", "add getGuestSpeakerEvents data into list - "
//                                                + " getId : " + guestSpeakerEvents.getId()
//                                                + " getEventSlotId : " + guestSpeakerEvents.getEventSlotId()
//                                                + " getSlotPrice : " + guestSpeakerEvents.getSlotPrice()
//                                                + " getUserId : " + guestSpeakerEvents.getUserId()
//                                        );
//                                        //guestSpeakerEvents.setEventId(eventViewHolderModel.getId());
//                                        //guestSpeakerEvents.setSelectedSubcategoryIds(getSelectedSubcategoryIds(eventViewHolderModel));
//                                        guestSpeakerEventsList.add(guestSpeakerEvents);
//                                    }
//                                }
//                            }
//                        }
//                    }

//                    else {
//                        break;
//                    }
                }
            }

//            if(!hasEventData) {
//                if(guestSpeakerEventsList != null && guestSpeakerEventsList.size() > 0) {
//                    guestSpeakerEventsList.clear();
//                }
//            }
        }
        return guestSpeakerEventsList;
    }

    private ArrayList<PostEventSubcategory> getPostEventSubcategoriesList() {
        boolean hasEventData = false;
        boolean hasSpecialitiesData = false;
        ArrayList<PostEventSubcategory> postEventSubcategoriesList = new ArrayList<>();
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            for (int i = 0; i < mGlobalEventList.size(); i++) {
                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
                if (eventViewHolderModel != null && eventViewHolderModel.isChecked()) {
//                    hasEventData = hasEventData(eventViewHolderModel);
//                    if(hasEventData) {
//                        PostEventSubcategory postEventSubcategory = new PostEventSubcategory();
//                        postEventSubcategory.setEventId(eventViewHolderModel.getId());
//                        postEventSubcategory.setSelectedSubcategoryIds(getSelectedSubcategoryIds(eventViewHolderModel));
//
//                        postEventSubcategoriesList.add(postEventSubcategory);
//                    }

                    hasEventData = hasEventData(eventViewHolderModel);
                    hasSpecialitiesData = hasEventSpecialitiesData(eventViewHolderModel);
                    if(!hasEventData && !hasSpecialitiesData) {
                        Log.d("event_log", "both are empty.");
                    } else if(!hasEventData && hasSpecialitiesData) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_price));
                        break;
                    } else if(!hasSpecialitiesData && hasEventData) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, getEmptySubcategoriesErrorMessage(eventViewHolderModel));
                        break;
                    } else if(hasEventData && hasSpecialitiesData) {
                        PostEventSubcategory postEventSubcategory = new PostEventSubcategory();
                        postEventSubcategory.setEventId(eventViewHolderModel.getId());
                        postEventSubcategory.setSelectedSubcategoryIds(getSelectedSubcategoryIds(eventViewHolderModel));

                        postEventSubcategoriesList.add(postEventSubcategory);
                    }
                }
            }
        }
        return postEventSubcategoriesList;
    }

    private ArrayList<Integer> getSelectedSubcategoryIds(EventViewHolderModel eventViewHolderModel) {
        ArrayList<Integer> subcategoryIdsList = new ArrayList<>();
        if(eventViewHolderModel != null && eventViewHolderModel.getSelectedEventSubcategories() != null
                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
            for(EventSubcategory eventSubcategory : eventViewHolderModel.getSelectedEventSubcategories()) {
                subcategoryIdsList.add(eventSubcategory.getEventSubcategoryId());
            }
        }
        return subcategoryIdsList;
    }

    private void generateSpecialitiesObject() {
//        if(mProfileModel != null && mProfileModel.getSpecialities() != null
//                && !TextUtils.isEmpty(mProfileModel.getSpecialities())) {
//            mSpecialities = mProfileModel.getSpecialities();
//        } else {
//            mSpecialities = "";
//        }
//        EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
//        eventViewHolderModel.setSpecialities(mSpecialities);
//        eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_SPECIALITIES);
//        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
//            mGlobalEventList.clear();
//            mGlobalEventList.add(eventViewHolderModel);
//            Log.d("speaker_log", "mGlobalEventList size after clear : " + mGlobalEventList.size());
//        } else {
//            mGlobalEventList = new ArrayList<>();
//            mGlobalEventList.add(eventViewHolderModel);
//            Log.d("speaker_log", "mGlobalEventList size after new creation : " + mGlobalEventList.size());
//        }
    }

    private void getEventList() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.getEventList().enqueue(new BaseCallback<EventListResponse>(AddSpeakingInfoActivity.this) {
            @Override
            public void onSuccess(EventListResponse response) {
                if(response != null) {
                    if(response.getStatus() == 1) {
                        if(response.getResult() != null && response.getResult().size() > 0) {
                            if(mProfileModel != null) {
                                if(mProfileModel.getGuestSpeakerEventList() != null
                                        && mProfileModel.getGuestSpeakerEventList().size() > 0) {
                                    mProfileModel.getGuestSpeakerEventList().clear();
                                }
                                mProfileModel.getGuestSpeakerEventList().addAll(response.getResult());
                            }
                            generateSpecialitiesObject();
                            generateEventList(response.getResult());
                            setEventList();
                            logGlobalEventList();
                        } else {
                            generateSpecialitiesObject();
                            setEventList();
                            logGlobalEventList();
                        }
                        //showToast("Getting event list successfully !!!");
                    } else {
                        try {
                            generateSpecialitiesObject();
                            setEventList();
                            logGlobalEventList();
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(AddSpeakingInfoActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<EventListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    generateSpecialitiesObject();
                    setEventList();
                    logGlobalEventList();
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddSpeakingInfoActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void generateEventList(ArrayList<EventList> localEventList) {
        if(localEventList != null && localEventList.size() > 0) {
            for(int i = 0; i < localEventList.size(); i++) {
                EventList eventList = localEventList.get(i);
                if(eventList != null) {
                    EventViewHolderModel eventViewHolderModel = new EventViewHolderModel();
                    eventViewHolderModel.setId(eventList.getId());
                    eventViewHolderModel.setName(eventList.getName());
                    eventViewHolderModel.setDescription(eventList.getDescription());
                    eventViewHolderModel.setEventLogo(eventList.getEventLogo());
                    eventViewHolderModel.setStatus(eventList.getStatus());
                    eventViewHolderModel.setGstValue(eventList.getGstValue());
                    eventViewHolderModel.setEventSlot(eventList.getEventSlot());
                    eventViewHolderModel.setViewType(Constants.VIEW_TYPE.VIEW_EVENT);
                    eventViewHolderModel.setChecked(isEventPriceAvailable(eventList));
                    eventViewHolderModel.setEventCategories(eventList.getEventCategories());

                    if(eventList.getSelectedEventSubcategories() != null
                            && eventList.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventList.getSelectedEventSubcategories()) {
                            eventSubcategory.setSelected(true);
                        }
                    }

                    eventViewHolderModel.setSelectedEventSubcategories(eventList.getSelectedEventSubcategories());

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
                        for(int z = 0; z < eventViewHolderModel.getEventCategories().getEventSubcategories().size(); z++) {
                            EventSubcategory eventSubcategory
                                    = eventViewHolderModel.getEventCategories().getEventSubcategories().get(z);
                            eventSubcategory.setEventSubcategoryId(eventSubcategory.getId());

                            EventSubcategoryDetail eventSubcategoryDetail = new EventSubcategoryDetail();
                            eventSubcategoryDetail.setId(eventSubcategory.getId());
                            eventSubcategoryDetail.setName(eventSubcategory.getName());

                            eventSubcategory.setEventSubcategoryDetail(eventSubcategoryDetail);
                        }
                    }

                    if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                        for(EventSubcategory eventSubcategory : eventViewHolderModel.getSelectedEventSubcategories()) {
                            eventSubcategory.setId(eventSubcategory.getEventSubcategoryId());
                            eventSubcategory.setSelected(true);
                        }
                    }

                    if(eventViewHolderModel.getEventCategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                            && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {

                        if (eventViewHolderModel.getSelectedEventSubcategories() != null
                                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {

                            for (EventSubcategory eventSubcategoryGlobal :
                                    eventViewHolderModel.getEventCategories().getEventSubcategories()) {
                                for (EventSubcategory eventSubcategorySelected :
                                        eventViewHolderModel.getSelectedEventSubcategories()) {
                                    if (eventSubcategoryGlobal.getEventSubcategoryId()
                                            == eventSubcategorySelected.getEventSubcategoryId()) {
                                        eventSubcategoryGlobal.setSelected(true);
                                    }
                                }
                            }

                        }

                    }

                    removeDuplicateEventSubcategories(eventViewHolderModel);

                    mGlobalEventList.add(eventViewHolderModel);
                }
            }
        }
    }

    private void removeDuplicateEventSubcategories(EventViewHolderModel eventViewHolderModel) {
        LinkedHashSet<EventSubcategory> duplicateRemovalHashSet = new LinkedHashSet<>();
        if(eventViewHolderModel.getSelectedEventSubcategories() != null
                && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
            duplicateRemovalHashSet.addAll(eventViewHolderModel.getSelectedEventSubcategories());
            if(duplicateRemovalHashSet.size() > 0) {
                Log.d("speaker_event_log", " duplicate removal set size : "
                        + duplicateRemovalHashSet.size()
                        + " for " + eventViewHolderModel.getName());
                eventViewHolderModel.getSelectedEventSubcategories().clear();
                eventViewHolderModel.getSelectedEventSubcategories().addAll(duplicateRemovalHashSet);
            }
        }
    }

    private boolean isEventPriceAvailable(EventList eventList) {
        boolean isChecked = false;
        int slotCount = 0;
        if(eventList.getEventSlot() != null && eventList.getEventSlot().size() > 0) {
            for(int j = 0; j < eventList.getEventSlot().size(); j++) {
                GuestSpeakerEvents guestSpeakerEvents = eventList.getEventSlot().get(j).getGuestSpeakerEvents();
                if(guestSpeakerEvents == null) {
                    isChecked = false;
                    //break;
                } else {
                    isChecked = true;
                    slotCount++;
                }
            }

            if(slotCount > 0) {
                isChecked = true;
            } else {
                isChecked = false;
            }
        }
        return isChecked;
    }

    private void setEventList() {
        mEventInfoAdapter = new EventInfoAdapter(AddSpeakingInfoActivity.this, mGlobalEventList,this);
        //mBinding.rvEventInfo.setHasFixedSize(true);
        mBinding.rvEventInfo.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvEventInfo.setAdapter(mEventInfoAdapter);
    }

    private void logGlobalEventList() {
//        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
//            for(int i = 0; i < mGlobalEventList.size(); i++) {
//                EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(i);
//                String eventLogStr = "Event list data - "
//                        + " position : " + i
//                        + " id : " + eventViewHolderModel.getId()
//                        + " name : " + eventViewHolderModel.getName()
//                        + " desc : " + eventViewHolderModel.getDescription()
//                        + " status : " + eventViewHolderModel.getStatus()
//                        + " gst value : " + eventViewHolderModel.getGstValue()
//                        //+ " event slot size : " + eventViewHolderModel.getEventSlot().size()
//                        + " view type : " + eventViewHolderModel.getViewType();
//                if(eventViewHolderModel.getViewType() == Constants.VIEW_TYPE.VIEW_EVENT) {
//                    eventLogStr = eventLogStr + " event slot size : " + eventViewHolderModel.getEventSlot().size();
//                }
//                Log.d("speaker_log", ""+eventLogStr);
//            }
//        }
    }

    @Override
    public String getActivityName() {
        return "AddSpeakingInfo";
    }

    @Override
    public void onSpecialitiesEntered(String specialitiesText) {
        //showToast("onSpecialitiesEntered - New Specialities : " + specialitiesText);
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(0);
            eventViewHolderModel.setSpecialities(specialitiesText);
            try {
                if(mEventInfoAdapter != null) {
                    //postAndNotifyAdapter(new Handler(), mBinding.rvEventInfo, mEventInfoAdapter);
                    mEventInfoAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onEventSwitchChangedListener(int position, boolean state) {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            Log.d("speaker_log", "onEventSwitchChangedListener - "
                    + " mGlobalEventList.size() : " + mGlobalEventList.size()
                    + " position : " + position);
            try {
                mGlobalEventList.get(position).setChecked(state);
                if(mEventInfoAdapter != null) {
                    //postAndNotifyAdapter(new Handler(), mBinding.rvEventInfo, mEventInfoAdapter);
                    mEventInfoAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onEventFeeClickListener(int parentPosition, int childPosition) {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            if(parentPosition >= mGlobalEventList.size()) {
                return;
            }
            mParentPosition = parentPosition;
            mSlotPosition = childPosition;
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(parentPosition);
            if(eventViewHolderModel != null) {
                if(eventViewHolderModel.getEventSlot() == null) {
                    return;
                }
                if(eventViewHolderModel.getEventSlot().size() == 0) {
                    return;
                }
                if(childPosition >= eventViewHolderModel.getEventSlot().size()) {
                    return;
                }
                EventSlot eventSlot = eventViewHolderModel.getEventSlot().get(childPosition);
                if(eventSlot != null) {
                    String slotText = eventSlot.getSlotText();
                    if(eventSlot.getGuestSpeakerEvents() == null) {
                        GuestSpeakerEvents guestSpeakerEvents = new GuestSpeakerEvents();
                        guestSpeakerEvents.setEventSlotId(eventSlot.getId());
                        guestSpeakerEvents.setId(0);
                        guestSpeakerEvents.setSlotPrice(0);
                        guestSpeakerEvents.setUserId(mProfileModel.getUserId());
                        eventSlot.setGuestSpeakerEvents(guestSpeakerEvents);
                        Log.d("speaker_log", "Inside onEventFeeClickListener - create new guestSpeakerEvents object");
                    }
                    String priceData = "";
                    if(eventSlot.getGuestSpeakerEvents().getSlotPrice() != 0) {
                        priceData = String.valueOf(eventSlot.getGuestSpeakerEvents().getSlotPrice());
                    } else {
                        priceData = "";
                    }
                    Alert.createSlotFeeEditAlert(AddSpeakingInfoActivity.this, eventViewHolderModel.getName(),
                            slotText, getResources().getString(R.string.enter_fee), priceData, getResources().getString(R.string.yes_small),
                            this, REQUEST_CODE_SLOT_FEE).show();
                }
            }
        }
    }

    @Override
    public void onEventCategoriesClickListener(int position) {
        //showToast("onEventCategoriesClickListener - position : " + position);
        try {
            if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
                if (position >= mGlobalEventList.size()) {
                    return;
                }
                EventCategory eventCategory = mGlobalEventList.get(position).getEventCategories();
                if (eventCategory != null) {
                    ArrayList<EventSubcategory> eventSubcategories = eventCategory.getEventSubcategories();
                    if (eventSubcategories != null && eventSubcategories.size() > 0) {
                        String eventCategoryName = "";
                        if(!TextUtils.isEmpty(eventCategory.getName())) {
                            eventCategoryName = eventCategory.getName();
                        }

                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
                        bundle.putInt(Constants.BundleKey.EVENT_POSITION, position);
                        bundle.putParcelableArrayList(Constants.BundleKey.EVENT_SUB_CATEGORIES, eventSubcategories);
                        bundle.putString(Constants.BundleKey.EVENT_CATEGORIES_NAME, eventCategoryName);
                        bundle.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
                        Navigator.getInstance().navigateToActivityForResultWithData(AddSpeakingInfoActivity.this,
                                SelectEventSubcategoriesActivity.class, bundle, REQUEST_CODE_EVENT_SUBCATEGORIES);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPositive(DialogInterface dialog, String price, int type) {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            if(mParentPosition >= mGlobalEventList.size()) {
                return;
            }
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(mParentPosition);
            if(eventViewHolderModel != null) {
                if(eventViewHolderModel.getEventSlot() == null) {
                    return;
                }
                if(eventViewHolderModel.getEventSlot().size() == 0) {
                    return;
                }
                if(mSlotPosition >= eventViewHolderModel.getEventSlot().size()) {
                    return;
                }
                EventSlot eventSlot = eventViewHolderModel.getEventSlot().get(mSlotPosition);
                if(eventSlot != null) {
                    if(eventSlot.getGuestSpeakerEvents() == null) {
                        GuestSpeakerEvents guestSpeakerEvents = new GuestSpeakerEvents();
                        guestSpeakerEvents.setEventSlotId(eventSlot.getId());
                        guestSpeakerEvents.setId(0);
                        guestSpeakerEvents.setSlotPrice(0);
                        guestSpeakerEvents.setUserId(mProfileModel.getUserId());
                        eventSlot.setGuestSpeakerEvents(guestSpeakerEvents);
                        Log.d("speaker_log", "Inside onPositive - create new guestSpeakerEvents object");
                    }

                    try {
                        if(price == null) {
                            eventSlot.getGuestSpeakerEvents().setSlotPrice(0);
                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_fee));
                            if(mEventInfoAdapter != null) {
                                mEventInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        }
                        if(TextUtils.isEmpty(price)) {
                            eventSlot.getGuestSpeakerEvents().setSlotPrice(0);
                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_fee));
                            if(mEventInfoAdapter != null) {
                                mEventInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        }
                        String priceWithDollarStr = price.trim();
                        if(!TextUtils.isEmpty(priceWithDollarStr) && priceWithDollarStr.length() == 1
                                && priceWithDollarStr.contains("$")) {
                            eventSlot.getGuestSpeakerEvents().setSlotPrice(0);
                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_event_fee));
                            if(mEventInfoAdapter != null) {
                                mEventInfoAdapter.notifyDataSetChanged();
                            }
                            return;
                        }
                        String priceWithoutDollarStr = priceWithDollarStr.trim();
                        if(!TextUtils.isEmpty(priceWithoutDollarStr) && priceWithoutDollarStr.length() > 1
                                && priceWithoutDollarStr.startsWith("$")) {
                            priceWithoutDollarStr = priceWithoutDollarStr.substring(1, priceWithoutDollarStr.length());
                        }
                        Double priceInDouble = Double.parseDouble(priceWithoutDollarStr);

//                        if(priceInDouble < 1) {
//                            eventSlot.getGuestSpeakerEvents().setSlotPrice(0);
//                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.event_fee_less_than_1));
//                            if(mEventInfoAdapter != null) {
//                                mEventInfoAdapter.notifyDataSetChanged();
//                            }
//                            return;
//                        } else

                            if(priceInDouble > 5000) {
                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.event_fee_greater_than_5000));
                            return;
                        } else {
                            eventSlot.getGuestSpeakerEvents().setSlotPrice(priceInDouble);
                            if(mEventInfoAdapter != null) {
                                mEventInfoAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onNegative(DialogInterface dialog, String price, int type) {
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            if(mParentPosition >= mGlobalEventList.size()) {
                return;
            }
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(mParentPosition);
            if(eventViewHolderModel != null) {
                if(eventViewHolderModel.getEventSlot() == null) {
                    return;
                }
                if(eventViewHolderModel.getEventSlot().size() == 0) {
                    return;
                }
                if(mSlotPosition >= eventViewHolderModel.getEventSlot().size()) {
                    return;
                }
//                EventSlot eventSlot = eventViewHolderModel.getEventSlot().get(mSlotPosition);
//                if(eventSlot != null) {
//                    try {
//                        if(price == null) {
//                            if(eventSlot.getGuestSpeakerEvents() != null) {
//                                eventSlot.getGuestSpeakerEvents().setSlotPrice(0);
//                            }
//                            return;
//                        }
//                        if(TextUtils.isEmpty(price)) {
//                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
//                            return;
//                        }
//                        String priceWithDollarStr = price.trim();
//                        if(!TextUtils.isEmpty(priceWithDollarStr) && priceWithDollarStr.length() == 1
//                                && priceWithDollarStr.contains("$")) {
//                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.please_enter_travel_fee));
//                            return;
//                        }
//                        String priceWithoutDollarStr = priceWithDollarStr.trim();
//                        if(!TextUtils.isEmpty(priceWithoutDollarStr) && priceWithoutDollarStr.length() > 1
//                                && priceWithoutDollarStr.startsWith("$")) {
//                            priceWithoutDollarStr = priceWithoutDollarStr.substring(1, priceWithoutDollarStr.length());
//                        }
//                        Double priceInDouble = Double.parseDouble(priceWithoutDollarStr);
//                        if(priceInDouble < 1) {
//                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.travel_fee_less_than_1));
//                            return;
//                        } else if(priceInDouble > 5000) {
//                            showSnackbarFromTop(AddSpeakingInfoActivity.this, getResources().getString(R.string.travel_fee_greater_than_5000));
//                            return;
//                        } else {
//                            eventSlot.getGuestSpeakerEvents().setSlotPrice(priceInDouble);
//                            if(mEventInfoAdapter != null) {
//                                mEventInfoAdapter.notifyDataSetChanged();
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
            }
        }
    }

    protected void postAndNotifyAdapter(final Handler handler, final RecyclerView recyclerView, final RecyclerView.Adapter adapter) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!recyclerView.isComputingLayout()) {
                    adapter.notifyDataSetChanged();
                } else {
                    postAndNotifyAdapter(handler, recyclerView, adapter);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_EVENT_SUBCATEGORIES) {
                if (data != null) {
                    updateEventListUi(data);
                }
            }
        }
    }

    private void updateEventListUi(Intent data) {
        int selectedEventPosition = data.getIntExtra(Constants.BundleKey.EVENT_POSITION, 0);
        //EventSubcategory eventSubcategory = data.getParcelableExtra(Constants.EXTRA_EVENT_SUB_CATEGORIES);
        ArrayList<EventSubcategory> eventSubcategoriesList = data.getParcelableArrayListExtra(Constants.EXTRA_EVENT_SUB_CATEGORIES_LIST);
        if(mGlobalEventList != null && mGlobalEventList.size() > 0) {
            EventViewHolderModel eventViewHolderModel = mGlobalEventList.get(selectedEventPosition);
            if(eventViewHolderModel != null) {
                if(eventViewHolderModel.getSelectedEventSubcategories() == null) {
                    ArrayList<EventSubcategory> subcategories = new ArrayList<>();
                    eventViewHolderModel.setSelectedEventSubcategories(subcategories);
                    //eventSubcategory.setSelected(true);
                    //eventViewHolderModel.getSelectedEventSubcategories().add(eventSubcategory);

                    // UPDATING SELECTED EVENT SUBCATEGORIES LIST WITH NEW DATA
                    for(int i = 0; i < eventSubcategoriesList.size(); i++) {
                        EventSubcategory eventSubcategory = eventSubcategoriesList.get(i);
                        if(eventSubcategory.isSelected()) {
                            eventViewHolderModel.getSelectedEventSubcategories().add(eventSubcategory);
                        }
                    }
                } else {
                    //eventSubcategory.setSelected(true);
                    //eventViewHolderModel.getSelectedEventSubcategories().add(eventSubcategory);

                    // CLEARING SELECTED EVENT SUBCATEGORIES LIST
                    if(eventViewHolderModel.getSelectedEventSubcategories() != null
                            && eventViewHolderModel.getSelectedEventSubcategories().size() > 0) {
                        eventViewHolderModel.getSelectedEventSubcategories().clear();
                    }

                    // UPDATING SELECTED EVENT SUBCATEGORIES LIST WITH NEW DATA
                    for(int i = 0; i < eventSubcategoriesList.size(); i++) {
                        EventSubcategory eventSubcategory = eventSubcategoriesList.get(i);
                        if(eventSubcategory.isSelected()) {
                            eventViewHolderModel.getSelectedEventSubcategories().add(eventSubcategory);
                        }
                    }
                }

                // UPDATING GLOBAL EVENT SUBCATEGORIES FROM GLOBAL LIST WITH NEW DATA
                EventCategory eventCategory = eventViewHolderModel.getEventCategories();
                ArrayList<EventSubcategory> eventSubcategoriesGlobal =  eventCategory.getEventSubcategories();
                for(int j = 0; j < eventSubcategoriesGlobal.size(); j++) {
                    EventSubcategory eventSubcategoryGlobal = eventSubcategoriesGlobal.get(j);
                    for(int k = 0; k < eventSubcategoriesList.size(); k++) {
                        EventSubcategory eventSubcategorySelected = eventSubcategoriesList.get(k);
                        if(eventSubcategoryGlobal.getEventSubcategoryId() == eventSubcategorySelected.getEventSubcategoryId()) {
                            if(eventSubcategorySelected.isSelected()) {
                                eventSubcategoryGlobal.setSelected(true);
                            } else {
                                eventSubcategoryGlobal.setSelected(false);
                            }
                        }
                    }
                }

                // FINALLY NOTIFY ADAPTER TO DISPLAY UPDATED LIST DATA ON UI
                if(mEventInfoAdapter != null) {
                    mEventInfoAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private String getEmptySubcategoriesErrorMessage(EventViewHolderModel eventViewHolderModel) {
        String errorMsg = getResources().getString(R.string.please_add_subcategories_generic);
        if(!TextUtils.isEmpty(eventViewHolderModel.getName())
                && eventViewHolderModel.getEventCategories() != null
                && !TextUtils.isEmpty(eventViewHolderModel.getEventCategories().getName())) {
            errorMsg = getResources().getString(R.string.please_add_subcategories)
                    + " " + eventViewHolderModel.getEventCategories().getName() + " for the event "
                    + eventViewHolderModel.getName() + ".";
        }
        return errorMsg;
    }

}
