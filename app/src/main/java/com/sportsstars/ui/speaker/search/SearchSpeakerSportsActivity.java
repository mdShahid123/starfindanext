package com.sportsstars.ui.speaker.search;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySearchSpeakerSportsBinding;
import com.sportsstars.databinding.ItemFlowSportsBinding;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.Sports;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SportsListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import java.util.ArrayList;

import retrofit2.Call;

public class SearchSpeakerSportsActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySearchSpeakerSportsBinding mBinding;
    private ArrayList<ItemFlowSportsBinding> mItemFlowBindingList;
    private ArrayList<Sports> mGlobalSportsList;
    private ArrayList<Sports> mSelectedSportsList = new ArrayList<>();
    private ArrayList<Sports> mSportsFilterList = new ArrayList<>();
    private boolean isNoSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_speaker_sports);

        getIntentData();
        initViews();
        setTextChangeListener();
//        if(Utils.isNetworkAvailable()) {
//            getSportsList();
//        } else {
//            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
//        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.EXTRA_SEARCH_SPORTS_LIST)) {
                mGlobalSportsList = bundle.getParcelableArrayList(Constants.EXTRA_SEARCH_SPORTS_LIST);
            }
        }
    }

    private void initViews() {
        mBinding.layoutTop.headerId.tvTitle.setText(R.string.select_sports_title);
        mBinding.tvSelect.setOnClickListener(this);

        mItemFlowBindingList = new ArrayList<>();
        mBinding.flowLayoutSports.removeAllViews();

        setSportsList();
    }

    private void getSportsList() {
        processToShowDialog();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);
        getSportsRequest.setPageSize(Constants.ALL_SPORTS_PAGE_SIZE);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>(SearchSpeakerSportsActivity.this) {
            @Override
            public void onSuccess(SportsListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        generateAllSportsObject();
                        mGlobalSportsList.addAll(response.getResult().getSportsList());
                        setSportsList();
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(SearchSpeakerSportsActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {
                try {
                    if(baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(SearchSpeakerSportsActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void generateAllSportsObject() {
        Sports sports = new Sports();
        sports.setId(0);
        sports.setName(getResources().getString(R.string.all_sports));
        sports.setSelected(false);
        sports.setReferencecLogo("");
        sports.setBackgroundImage("");
        sports.setDescription("");
        sports.setReferenceImageUrl("");
        sports.setSkillLevel(-1);

        if(mGlobalSportsList != null && mGlobalSportsList.size() > 0) {
            mGlobalSportsList.clear();
            mGlobalSportsList.add(sports);
            Log.d("speaker_log", "mSportsList size after clear : " + mGlobalSportsList.size());
        } else {
            mGlobalSportsList = new ArrayList<>();
            mGlobalSportsList.add(sports);
            Log.d("speaker_log", "mSportsList size after new creation : " + mGlobalSportsList.size());
        }
    }

    private void setSportsList() {
        if(mGlobalSportsList != null && mGlobalSportsList.size() > 0) {
            if(mSportsFilterList != null && mSportsFilterList.size() > 0) {
                mSportsFilterList.clear();
                mSportsFilterList.addAll(mGlobalSportsList);
            }
            for(Sports sports : mGlobalSportsList) {
                if(sports != null) {
                    if(sports.isSelected()) {
                        mSelectedSportsList.add(sports);
                    }
                    addTitleToLayout(sports);
                }
            }
        }
    }

    private void addTitleToLayout(final Sports sports) {

        if(sports != null) {

            String sportName = "";

            if(sports.getName() != null
                    && !TextUtils.isEmpty(sports.getName())) {
                sportName = sports.getName();
            }

            final ItemFlowSportsBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from
                    (mBinding.flowLayoutSports.getContext())
                    .inflate(R.layout.item_flow_sports, mBinding.flowLayoutSports, false));

            if(flowBinding != null) {

                flowBinding.flowChild.setText(sportName);
                flowBinding.flowChild.setTag(sports);
                if(sports.isSelected()) {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                } else {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                }

                mItemFlowBindingList.add(flowBinding);

                flowBinding.flowChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Sports sportsLocal = (Sports) flowBinding.flowChild.getTag();
                        if(isAllSportsObject(sportsLocal)) {
                            if(sportsLocal.isSelected()) {
                                flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                                flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                                sportsLocal.setSelected(false);
                            } else {
                                flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                                flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                                sportsLocal.setSelected(true);
                            }
                            checkForAllSportsSelection(sportsLocal);
                        } else {
                            if(sportsLocal.isSelected()) {
                                flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                                flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                                sportsLocal.setSelected(false);
                                mSelectedSportsList.remove(sportsLocal);
                                removeAllSportsSelection();
                            } else {
                                flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                                flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                                sportsLocal.setSelected(true);
                                mSelectedSportsList.add(sportsLocal);
                                addAllSportsSelection();
                            }

                            if(mGlobalSportsList != null && mGlobalSportsList.size() > 0) {
                                for(Sports sportsGlobal : mGlobalSportsList) {
                                    if(sportsGlobal.getId() == sportsLocal.getId()) {
                                        if(sportsLocal.isSelected()) {
                                            sportsLocal.setSelected(true);
                                            sportsGlobal.setSelected(true);
                                        } else {
                                            sportsLocal.setSelected(false);
                                            sportsGlobal.setSelected(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });

                mBinding.flowLayoutSports.addView(flowBinding.getRoot());

            }

        }

    }

    private void removeAllSportsSelection() {
        Sports allSports = mGlobalSportsList.get(0);
        if(isAllSportsObject(allSports)) {
            allSports.setSelected(false);
        }
        if(mItemFlowBindingList != null && mItemFlowBindingList.size() > 0) {
            for(ItemFlowSportsBinding flowBinding : mItemFlowBindingList) {
                Sports allSportsLocal = (Sports) flowBinding.flowChild.getTag();
                if(isAllSportsObject(allSportsLocal)) {
                    flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                    flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                    allSportsLocal.setSelected(false);
                }
            }
        }
    }

    private void addAllSportsSelection() {
        if(mSelectedSportsList != null && mSelectedSportsList.size() > 0) {
            if(mSelectedSportsList.size() == mGlobalSportsList.size()-1) {
                Sports allSports = mGlobalSportsList.get(0);
                if(isAllSportsObject(allSports)) {
                    allSports.setSelected(true);
                }
                if(mItemFlowBindingList != null && mItemFlowBindingList.size() > 0) {
                    for(ItemFlowSportsBinding flowBinding : mItemFlowBindingList) {
                        Sports allSportsLocal = (Sports) flowBinding.flowChild.getTag();
                        if(isAllSportsObject(allSportsLocal)) {
                            flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                            flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                            allSportsLocal.setSelected(true);
                        }
                    }
                }
            }
        }
    }

    private void checkForAllSportsSelection(Sports allSportsObj) {
        if(isAllSportsObject(allSportsObj)) {
            for(Sports sportsObj : mGlobalSportsList) {
                sportsObj.setSelected(allSportsObj.isSelected());
            }

            if(mSportsFilterList != null && mSportsFilterList.size() > 0) {
                mSportsFilterList.clear();
                mSportsFilterList.addAll(mGlobalSportsList);
            }

            if(mSelectedSportsList != null && mSelectedSportsList.size() > 0) {
                mSelectedSportsList.clear();
                for(Sports selectedObj : mGlobalSportsList) {
                    if(selectedObj.isSelected()) {
                        mSelectedSportsList.add(selectedObj);
                    }
                }
            }

            if(mItemFlowBindingList != null && mItemFlowBindingList.size() > 0) {
                for(ItemFlowSportsBinding flowBinding : mItemFlowBindingList) {
                    Sports sportsLocal = (Sports) flowBinding.flowChild.getTag();
                    if(allSportsObj.isSelected()) {
                        flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_green_8_2);
                        flowBinding.flowChild.setTextColor(getResources().getColor(R.color.white_color));
                        sportsLocal.setSelected(true);
                    } else {
                        flowBinding.flowChild.setBackgroundResource(R.drawable.shape_rounded_border_green_2);
                        flowBinding.flowChild.setTextColor(getResources().getColor(R.color.green_gender));
                        sportsLocal.setSelected(false);
                    }
                }
            }
        }
    }

    private boolean isAllSportsObject(Sports sports) {
        if(sports != null && sports.getId() == 0
                && sports.getName().equals(getResources().getString(R.string.all_sports))) {
            return true;
        } else {
            return false;
        }
    }

    private void setResultForSportsList(ArrayList<Sports> sportsList) {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(Constants.EXTRA_SEARCH_SPORTS_LIST, (ArrayList<? extends Parcelable>) sportsList);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void setTextChangeListener() {
        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //removeSubcategoriesSelection();
                String constraint = s.toString();
                if(constraint != null && constraint.length() > 0) {
                    constraint = constraint.toUpperCase();
                    ArrayList<Sports> filters = new ArrayList<>();
                    for(int i = 0; i < mGlobalSportsList.size(); i++) {
                        if(mGlobalSportsList.get(i).getName().toUpperCase().startsWith(constraint)) {
                            Sports sports = new Sports();
                            sports.setId(mGlobalSportsList.get(i).getId());
                            sports.setName(mGlobalSportsList.get(i).getName());
                            sports.setSelected(mGlobalSportsList.get(i).isSelected());
                            sports.setSkillLevel(mGlobalSportsList.get(i).getSkillLevel());
                            sports.setReferenceImageUrl(mGlobalSportsList.get(i).getReferenceImageUrl());
                            sports.setReferencecLogo(mGlobalSportsList.get(i).getReferencecLogo());
                            sports.setDescription(mGlobalSportsList.get(i).getDescription());
                            sports.setBackgroundImage(mGlobalSportsList.get(i).getBackgroundImage());

                            filters.add(sports);
                        }
                    }
                    if(mSportsFilterList != null) {
                        if(mSportsFilterList.size() > 0) {
                            mSportsFilterList.clear();
                        }
                        mSportsFilterList.addAll(filters);
                    }
                } else {
                    if(mSportsFilterList != null) {
                        if(mSportsFilterList.size() > 0) {
                            mSportsFilterList.clear();
                        }
                        mSportsFilterList.addAll(mGlobalSportsList);
                    }
                }
                if(mSportsFilterList != null && mSportsFilterList.size() > 0) {
                    mBinding.tvNoResults.setVisibility(View.GONE);
                    mBinding.flowLayoutSports.setVisibility(View.VISIBLE);
                    isNoSearchResult = false;
                } else {
                    mBinding.tvNoResults.setVisibility(View.VISIBLE);
                    mBinding.flowLayoutSports.setVisibility(View.GONE);
                    initViews();
                    isNoSearchResult = true;
                }

                populateFilterList(isNoSearchResult);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Method used to remove sports selection from the list.
     */
    private void removeSportsSelection() {
        if(mSportsFilterList != null && mSportsFilterList.size() > 0) {
            for(int i = 0; i < mSportsFilterList.size(); i++) {
                Sports sports = mSportsFilterList.get(i);
                sports.setSelected(false);
            }

            if(mGlobalSportsList != null && mGlobalSportsList.size() > 0) {
                for(int i = 0; i < mGlobalSportsList.size(); i++) {
                    Sports sports = mGlobalSportsList.get(i);
                    sports.setSelected(false);
                }
            }
        }

        initViews();
    }

    private void populateFilterList(boolean isNoSearchResult) {
        if(isNoSearchResult) {
            mItemFlowBindingList = new ArrayList<>();
            mBinding.flowLayoutSports.removeAllViews();

            if(mGlobalSportsList != null && mGlobalSportsList.size() > 0) {
                if(mSportsFilterList != null && mSportsFilterList.size() > 0) {
                    mSportsFilterList.clear();
                    mSportsFilterList.addAll(mGlobalSportsList);
                }
                for(Sports sports : mGlobalSportsList) {
                    if(sports != null) {
                        addTitleToLayout(sports);
                    }
                }
            }
        } else {
            mItemFlowBindingList = new ArrayList<>();
            mBinding.flowLayoutSports.removeAllViews();

            if(mSportsFilterList != null && mSportsFilterList.size() > 0) {
                for(Sports sports : mSportsFilterList) {
                    if(sports != null) {
                        addTitleToLayout(sports);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.tv_select) {
            int isSelectedCount = 0;
            for(int i = 0; i < mGlobalSportsList.size(); i++) {
                Sports sports = mGlobalSportsList.get(i);
                if(sports.isSelected()) {
                    isSelectedCount++;
                }
                Log.d("category_log", "Sports list - "
                        + " sports id : " + sports.getId()
                        + " sports name : " + sports.getName()
                        + " sports isSelected : " + sports.isSelected()
                        + " isSelected count : " + isSelectedCount
                );
            }

            if(isSelectedCount >= 1) {
                setResultForSportsList(mGlobalSportsList);
            } else {
                showSnackbarFromTop(SearchSpeakerSportsActivity.this,
                        getResources().getString(R.string.please_select_one_sport));
            }
        }
    }

    @Override
    public String getActivityName() {
        return "SearchSpeakerSports";
    }

}
