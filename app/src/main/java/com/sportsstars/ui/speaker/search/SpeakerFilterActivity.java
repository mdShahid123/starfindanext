package com.sportsstars.ui.speaker.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySpeakerFilterBinding;
import com.sportsstars.model.EventViewHolderModel;
import com.sportsstars.model.Speaker;
import com.sportsstars.model.SpeakerFilterData;
import com.sportsstars.model.Sports;
import com.sportsstars.model.Team;
import com.sportsstars.network.response.speaker.EventCategory;
import com.sportsstars.network.response.speaker.EventSubcategory;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.ui.speaker.AddSpeakingInfoActivity;
import com.sportsstars.ui.speaker.SelectEventSubcategoriesActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;
import com.sportsstars.widget.AddDollarTextWatcher;

import java.util.ArrayList;

public class SpeakerFilterActivity extends BaseActivity implements View.OnClickListener {

    private static final int MAX_BUDGET_LIMIT = 5000;

    private ActivitySpeakerFilterBinding mBinder;

    private SpeakerFilterData filterData;

    private boolean mMaleSelected;
    private boolean mFemaleSelected;

    private boolean isMaleChecked;
    private boolean isFemaleChecked;

    private boolean mYesSelected;
    private boolean mNoSelected;

    private boolean isYesChecked;
    private boolean isNoChecked;

    private int minBudgetInt;
    private int maxBudgetInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_speaker_filter);

        getIntentData();
        initViews();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.SPEAKER_FILTER_DATA)) {
                filterData = bundle.getParcelable(Constants.BundleKey.SPEAKER_FILTER_DATA);
                if(filterData != null
                        && filterData.getSelectedEventViewHolder() != null
                        && !TextUtils.isEmpty(filterData.getSelectedEventViewHolder().getName())) {
//                    Log.d("filter_data", "filter data - event view holder name : "
//                            + filterData.getSelectedEventViewHolder().getName());
//                    showToast("filter data - event view holder name : "
//                            + filterData.getSelectedEventViewHolder().getName());
                }
            }
        }
    }

    private void initViews() {
        mBinder.buttonApply.setOnClickListener(this);
        mBinder.buttonClear.setOnClickListener(this);
        mBinder.relMan.setOnClickListener(this);
        mBinder.relWoman.setOnClickListener(this);
        mBinder.relYes.setOnClickListener(this);
        mBinder.relNo.setOnClickListener(this);
        mBinder.tvEventsSelected.setOnClickListener(this);
        mBinder.tvSportsSelected.setOnClickListener(this);
        mBinder.tvTeamsSelected.setOnClickListener(this);
        mBinder.tvLocationSelected.setOnClickListener(this);
        mBinder.tvSpecialitiesSelected.setOnClickListener(this);

        mBinder.etBudgetMin.addTextChangedListener(new AddDollarTextWatcher(mBinder.etBudgetMin));
        mBinder.etBudgetMax.addTextChangedListener(new AddDollarTextWatcher(mBinder.etBudgetMax));

        setInitialGenderSelection();
        setInitialTravelSelection();
        setEventData();
        setSportsData();
        setTeamsData();
        setLocationData();
        setSpecialitiesData();
        setBudgetData();
    }

    private void setInitialGenderSelection() {
        isMaleChecked = false;
        isFemaleChecked = false;

        mMaleSelected = true;
        mFemaleSelected = true;

        if(filterData != null) {
            if(filterData.getGender() == Constants.GENDER_TYPE.MALE) {
                isMaleChecked = true;
                mMaleSelected = false;
            } else if(filterData.getGender() == Constants.GENDER_TYPE.FEMALE) {
                isFemaleChecked = true;
                mFemaleSelected = false;
            } else if(filterData.getGender() == Constants.GENDER_TYPE.NONE) {
                isMaleChecked = false;
                isFemaleChecked = false;

                mMaleSelected = true;
                mFemaleSelected = true;
            } else if(filterData.getGender() == Constants.GENDER_TYPE.BOTH_SELECTED) {
                isMaleChecked = true;
                isFemaleChecked = true;

                mMaleSelected = false;
                mFemaleSelected = false;
            }
        }

        setMaleSelected();
        setFemaleSelected();
    }

    private void setInitialTravelSelection() {
        isYesChecked = false;
        isNoChecked = false;

        mYesSelected = true;
        mNoSelected = true;

        if(filterData != null) {
            if(filterData.getTravelType() == Constants.INTER_STATE_TRAVEL.NO) {
                isNoChecked = true;
                mNoSelected = false;
            } else if(filterData.getTravelType() == Constants.INTER_STATE_TRAVEL.YES) {
                isYesChecked = true;
                mYesSelected = false;
            } else if(filterData.getTravelType() == Constants.INTER_STATE_TRAVEL.BOTH_UNSELECTED) {
                isYesChecked = false;
                isNoChecked = false;

                mYesSelected = true;
                mNoSelected = true;
            } else if(filterData.getTravelType() == Constants.INTER_STATE_TRAVEL.BOTH_SELECTED) {
                isYesChecked = true;
                isNoChecked = true;

                mYesSelected = false;
                mNoSelected = false;
            }
        }

        setYesSelected();
        setNoSelected();
    }

    private void setMaleSelected() {
        if (mMaleSelected) {
            mMaleSelected = false;
            mBinder.relMan.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
//            mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvMan.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isMaleChecked = false;
        } else {
            mMaleSelected = true;
            mBinder.relMan.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvMan.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isMaleChecked = true;
        }
    }

    private void setFemaleSelected() {
        if (mFemaleSelected) {
            mFemaleSelected = false;
            mBinder.relWoman.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
//            mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvWoman.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isFemaleChecked = false;
        } else {
            mFemaleSelected = true;
            mBinder.relWoman.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvWoman.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isFemaleChecked = true;
        }
    }

    private void setYesSelected() {
        if (mYesSelected) {
            mYesSelected = false;
            mBinder.relYes.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_grey));
//            mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvYes.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isYesChecked = false;
        } else {
            mYesSelected = true;
            mBinder.relYes.setBackground(ContextCompat.getDrawable(this, R.drawable
                    .shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvYes.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isYesChecked = true;
        }
    }

    private void setNoSelected() {
        if (mNoSelected) {
            mNoSelected = false;
            mBinder.relNo.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
//            mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_unselected);
            mBinder.tvNo.setTextColor(ContextCompat.getColor(this, R.color.black_25));

            isNoChecked = false;
        } else {
            mNoSelected = true;
            mBinder.relNo.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
//            mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_selected);
            mBinder.tvNo.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            isNoChecked = true;
        }
    }

    private void setEventData() {
        if(filterData != null) {
            if(filterData.getSelectedEventViewHolder() != null) {
                 if(!TextUtils.isEmpty(filterData.getSelectedEventViewHolder().getName())) {
                     mBinder.tvEventsSelected.setText(filterData.getSelectedEventViewHolder().getName());
                 }
            }
        }
    }

    private void setSportsData() {
        if(filterData != null) {
            if(filterData.isAllSportsSelected()) {
                mBinder.tvSportsSelected.setText(getResources().getString(R.string.all));
            } else {
                if (filterData.getSelectedSportsList() != null) {
                    if (filterData.getSelectedSportsList().size() > 0) {
                        if (filterData.getSelectedSportsList().size() == 1) {
                            mBinder.tvSportsSelected.setText(filterData.getSelectedSportsList().get(0).getName());
                        } else {
                            //mBinder.tvSportsSelected.setText(getResources().getString(R.string.selected_sports_count, filterData.getSelectedSportsList().size()));
                            mBinder.tvSportsSelected.setText(Utils.getCommaSepSportsString(filterData.getSelectedSportsList()));
                        }
                    } else {
                        mBinder.tvSportsSelected.setText(getResources().getString(R.string.all));
                    }
                } else {
                    mBinder.tvSportsSelected.setText(getResources().getString(R.string.all));
                }
            }
        }
    }

    private void setTeamsData() {
        if(filterData != null) {
            if (filterData.getSelectedTeamsList() != null) {
                if (filterData.getSelectedTeamsList().size() > 0) {
                    if (filterData.getSelectedTeamsList().size() == 1) {
                        mBinder.tvTeamsSelected.setText(filterData.getSelectedTeamsList().get(0).getName());
                    } else {
                        //mBinder.tvTeamsSelected.setText(getResources().getString(R.string.selected_teams_count, filterData.getSelectedTeamsList().size()));
                        mBinder.tvTeamsSelected.setText(Utils.getCommaSepString(filterData.getSelectedTeamsList()));
                    }
                } else {
                    mBinder.tvTeamsSelected.setText(getResources().getString(R.string.all));
                }
            } else {
                mBinder.tvTeamsSelected.setText(getResources().getString(R.string.all));
            }
        }
    }

    private void setLocationData() {
        if(filterData != null) {
            if(!TextUtils.isEmpty(filterData.getAddress())) {
                mBinder.tvLocationSelected.setText(filterData.getAddress());
            }
        }
    }

    private void setSpecialitiesData() {
        if(filterData != null) {
//            Log.d("speaker_log", "Speaker Filter - filterData.getSelectedEventSubcategories() size : "
//                    + filterData.getSelectedEventSubcategories().size());
            if(filterData.isAllSubcategoriesSelected()) {
                mBinder.tvSpecialitiesSelected.setText(getResources().getString(R.string.all));
            } else {
                if (filterData.getSelectedEventSubcategories() != null) {
                    if (filterData.getSelectedEventSubcategories().size() > 0) {
                        if (filterData.getSelectedEventSubcategories().size() == 1) {
                            mBinder.tvSpecialitiesSelected.setText(filterData.getSelectedEventSubcategories().get(0).getName());
                        } else {
                            //mBinder.tvSpecialitiesSelected.setText(getResources().getString(R.string.selected_specialities_count, filterData.getSelectedEventSubcategories().size()));
                            mBinder.tvSpecialitiesSelected.setText(Utils.getCommaSepSubcategoriesString(filterData.getSelectedEventSubcategories()));
                        }
                    } else {
                        mBinder.tvSpecialitiesSelected.setText(getResources().getString(R.string.all));
                    }
                } else {
                    mBinder.tvSpecialitiesSelected.setText(getResources().getString(R.string.all));
                }
            }
        }

        if(filterData != null && filterData.getSelectedEventViewHolder() != null
                && filterData.getSelectedEventViewHolder().getEventCategories() != null
                && !TextUtils.isEmpty(filterData.getSelectedEventViewHolder().getEventCategories().getName())) {
            mBinder.tvSpecialities.setText(filterData.getSelectedEventViewHolder().getEventCategories().getName());
        }

        if(filterData != null && filterData.getSelectedEventViewHolder() != null
                && filterData.getSelectedEventViewHolder().getEventCategories() != null
                && !TextUtils.isEmpty(filterData.getSelectedEventViewHolder().getEventCategories().getName())) {
            mBinder.tvSpecialitiesSelected.setHint("Search for " + filterData.getSelectedEventViewHolder().getEventCategories().getName());
        }
    }

    private void setBudgetData() {
        if(filterData != null) {
            minBudgetInt = (int) filterData.getMinBudget();
            maxBudgetInt = (int) filterData.getMaxBudget();
            mBinder.etBudgetMin.setText(getResources().getString(R.string.min_budget_with_dollar, minBudgetInt));
            mBinder.etBudgetMax.setText(getResources().getString(R.string.max_budget_with_dollar, maxBudgetInt));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_man:
                setMaleSelected();
                break;
            case R.id.rel_woman:
                setFemaleSelected();
                break;
            case R.id.tv_events_selected:
                launchSelectEventScreen();
                break;
            case R.id.tv_sports_selected:
                launchSelectSportsScreen();
                break;
            case R.id.tv_teams_selected:
                launchSelectTeamsScreen();
                break;
            case R.id.tv_location_selected:
                launchChangeLocationScreen();
                break;
            case R.id.tv_specialities_selected:
                launchSelectSpecialitiesScreen();
                break;
            case R.id.rel_yes:
                setYesSelected();
                break;
            case R.id.rel_no:
                setNoSelected();
                break;
            case R.id.button_apply:
                if(isInputValid()) {
                    applyFilter();
                }
                break;
            case R.id.button_clear:
                clearFilter();
                break;
        }
    }

    private void launchSelectEventScreen() {
        if(Utils.isNetworkAvailable()) {
            Bundle bundleSpeaker = new Bundle();
            bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
            bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
            bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, true);
            Navigator.getInstance().navigateToActivityForResultWithData(SpeakerFilterActivity.this,
                    SelectEventTypeActivity.class, bundleSpeaker, Constants.REQUEST_CODE.REQUEST_CODE_EVENT_TYPE);
        } else {
            showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void launchSelectSportsScreen() {
        if(Utils.isNetworkAvailable()) {
            if(filterData != null && filterData.getSelectedEventViewHolder() != null) {
                Bundle bundleSpeaker = new Bundle();
                bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
                bundleSpeaker.putParcelable(Constants.BundleKey.EVENT_VIEW_HOLDER, filterData.getSelectedEventViewHolder());
                bundleSpeaker.putInt(Constants.BundleKey.EVENT_ID, filterData.getSelectedEventViewHolder().getId());
                bundleSpeaker.putString(Constants.BundleKey.EVENT_NAME, filterData.getSelectedEventViewHolder().getName());
                bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, true);
//                bundleSpeaker.putParcelableArrayList(Constants.BundleKey.SPORTS_LIST, filterData.getSelectedSportsList());
//                bundleSpeaker.putBoolean(Constants.BundleKey.ALL_SPORTS_SELECTED, filterData.isAllSportsSelected());
                Navigator.getInstance().navigateToActivityForResultWithData(
                        SpeakerFilterActivity.this, SelectSpeakerSportsActivity.class, bundleSpeaker,
                        Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_SPEAKER_SPORTS);
            }
        } else {
            showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void launchSelectTeamsScreen() {
        if(Utils.isNetworkAvailable()) {
            if(filterData != null && filterData.getSelectedSportsList() != null) {
                Bundle bundleTeam = new Bundle();
                bundleTeam.putIntegerArrayList(Constants.BundleKey.SPORT_IDS_LIST, getSportIdsList());
                bundleTeam.putParcelableArrayList(Constants.BundleKey.PREVIOUSLY_SELECTED_TEAMS_LIST, filterData.getSelectedTeamsList());
                bundleTeam.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, true);
                Navigator.getInstance().navigateToActivityForResultWithData(
                        SpeakerFilterActivity.this,
                        SearchSpeakerTeamListActivity.class,
                        bundleTeam, Constants.REQUEST_CODE.REQUEST_CODE_TEAM_LIST);
            }
        } else {
            showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private ArrayList<Integer> getSportIdsList() {
        ArrayList<Integer> sportIdsList = new ArrayList<>();
        if(filterData != null && filterData.getSelectedSportsList() != null
                && filterData.getSelectedSportsList().size() > 0) {
            for(Sports sports : filterData.getSelectedSportsList()) {
                if(sports != null && sports.getId() != 0) {
                    sportIdsList.add(sports.getId());
                }
            }
        }
        return sportIdsList;
    }

    private void launchChangeLocationScreen() {
        if(Utils.isNetworkAvailable()) {
            Intent intent = new Intent(SpeakerFilterActivity.this, SearchCoachLocationActivity.class);
            startActivityForResult(intent, Constants.REQUEST_CODE.REQUEST_CODE_LOCATION);
        } else {
            showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void launchSelectSpecialitiesScreen() {
        if(Utils.isNetworkAvailable()) {
            try {
                if(filterData != null && filterData.getSelectedEventViewHolder() != null
                        && filterData.getSelectedEventViewHolder().getEventCategories() != null) {
                    EventCategory eventCategory = filterData.getSelectedEventViewHolder().getEventCategories();
                    if (eventCategory != null) {
                        ArrayList<EventSubcategory> eventSubcategories = eventCategory.getEventSubcategories();
                        if (eventSubcategories != null && eventSubcategories.size() > 0) {
                            String eventCategoryName = "";
                            if(!TextUtils.isEmpty(eventCategory.getName())) {
                                eventCategoryName = eventCategory.getName();
                            }
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
                            bundle.putInt(Constants.BundleKey.EVENT_POSITION, 0);
                            bundle.putParcelableArrayList(Constants.BundleKey.EVENT_SUB_CATEGORIES, eventSubcategories);
                            bundle.putString(Constants.BundleKey.EVENT_CATEGORIES_NAME, eventCategoryName);
                            bundle.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, true);
                            Navigator.getInstance().navigateToActivityForResultWithData(
                                    SpeakerFilterActivity.this,
                                    SelectEventSubcategoriesActivity.class, bundle,
                                    Constants.REQUEST_CODE.REQUEST_CODE_EVENT_SUBCATEGORIES);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private boolean isInputValid() {
        try {
            if(filterData == null) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.filter_data_is_empty));
                return false;
            } else if(filterData.getSelectedEventViewHolder() == null) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.event_data_is_empty));
                return false;
            } else if(filterData.getSelectedSportsList() == null) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.sports_list_data_is_empty));
                return false;
            } else if(filterData.getSelectedTeamsList() == null) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.team_list_data_is_empty));
                return false;
            } else if(filterData.getLatitude() == 0.0) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.address_is_empty));
                return false;
            } else if(filterData.getLongitude() == 0.0) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.address_is_empty));
                return false;
            } else if(filterData.getAddress() == null) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.address_is_empty));
                return false;
            } else if(filterData.getAddress() != null
                    && TextUtils.isEmpty(filterData.getAddress())) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.address_is_empty));
                return false;
            } else if(!TextUtils.isEmpty(filterData.getAddress())
                    && filterData.getAddress().equalsIgnoreCase("Unknown")) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.address_is_empty));
                return false;
            } else if(!TextUtils.isEmpty(filterData.getAddress())
                    && filterData.getAddress().equals(getResources().getString(R.string.search_by_location))) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.address_is_empty));
                return false;
            } else if(filterData.getSelectedEventSubcategories() == null) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.selected_specialities_list_is_empty));
                return false;
            }

            String minBudgetStrWithDollar = mBinder.etBudgetMin.getText().toString().trim();
            String maxBudgetStrWithDollar = mBinder.etBudgetMax.getText().toString().trim();

            if(TextUtils.isEmpty(minBudgetStrWithDollar)) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.min_budget_empty));
                return false;
            } else if(TextUtils.isEmpty(maxBudgetStrWithDollar)) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.max_budget_empty));
                return false;
            } else if(!TextUtils.isEmpty(minBudgetStrWithDollar)
                    && minBudgetStrWithDollar.length() == 1
                    && minBudgetStrWithDollar.equals("$")) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.min_budget_empty));
                return false;
            } else if(!TextUtils.isEmpty(maxBudgetStrWithDollar)
                    && maxBudgetStrWithDollar.length() == 1
                    && maxBudgetStrWithDollar.equals("$")) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.max_budget_empty));
                return false;
            }

            String minBudgetStr = "";
            String maxBudgetStr = "";

            if(!TextUtils.isEmpty(minBudgetStrWithDollar)
                    && minBudgetStrWithDollar.length() > 1
                    &&  Character.toString(minBudgetStrWithDollar.charAt(0)).equals("$")) {
                minBudgetStr = minBudgetStrWithDollar.substring(1, minBudgetStrWithDollar.length());
            }

            if(!TextUtils.isEmpty(maxBudgetStrWithDollar)
                    && maxBudgetStrWithDollar.length() > 1
                    &&  Character.toString(maxBudgetStrWithDollar.charAt(0)).equals("$")) {
                maxBudgetStr = maxBudgetStrWithDollar.substring(1, maxBudgetStrWithDollar.length());
            }

            if(TextUtils.isEmpty(minBudgetStr)) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.min_budget_empty));
                return false;
            } else if(TextUtils.isEmpty(maxBudgetStr)) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.max_budget_empty));
                return false;
            }

            minBudgetInt = Integer.parseInt(minBudgetStr);
            maxBudgetInt = Integer.parseInt(maxBudgetStr);

            if(maxBudgetInt == 0) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.max_budget_not_zero));
                return false;
            } else if(maxBudgetInt <= minBudgetInt) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.max_budget_not_less_than_min_budget));
                return false;
            } else if(minBudgetInt >= MAX_BUDGET_LIMIT) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.min_budget_limit));
                return false;
            } else if(maxBudgetInt > MAX_BUDGET_LIMIT) {
                showSnackbarFromTop(SpeakerFilterActivity.this, getResources().getString(R.string.max_budget_limit));
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setAllEventSubcategoriesList() {
        if(filterData != null && filterData.getSelectedEventViewHolder() != null
                && filterData.getSelectedEventViewHolder().getEventCategories() != null
                && filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories() != null
                && filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories().size() > 0) {
            if(filterData.getSelectedEventSubcategories() != null && filterData.getSelectedEventSubcategories().size() > 0) {
                filterData.getSelectedEventSubcategories().clear();
            }
            filterData.getSelectedEventSubcategories().addAll(filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories());
            filterData.setAllSubcategoriesSelected(true);
        }
    }

    private void applyFilter() {
        getSportData();
        setGenderStatus();
        setTravelTypeStatus();
        setMinMaxBudget();
        Intent intent = new Intent();
        intent.putExtra(Constants.BundleKey.SPEAKER_FILTER_DATA, filterData);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void getSportData() {
//        if(filterData != null && filterData.getSelectedSportsList() != null
//                && filterData.getSelectedSportsList().size() > 0) {
//            Sports mSport = filterData.getSelectedSportsList().get(0);
//            Log.d("sport_log", "speaker filter - setResult - "
//                    +" mSport id : " + mSport.getId() + " mSport name : " + mSport.getName());
//        }
    }

    private void clearFilter() {
        if(filterData != null) {
            filterData.setGender(Constants.GENDER_TYPE.NONE);
            setInitialGenderSelection();

            filterData.setTravelType(Constants.INTER_STATE_TRAVEL.UNSELECTED);
            setInitialTravelSelection();

            filterData.setAllSportsSelected(true);
            if(filterData.getSelectedSportsList() != null) {
                filterData.getSelectedSportsList().clear();
            }
            setSportsData();

            filterData.setAllTeamsSelected(true);
            if(filterData.getSelectedTeamsList() != null) {
                filterData.getSelectedTeamsList().clear();
            }
            setTeamsData();

            filterData.setAllSubcategoriesSelected(true);
            setAllEventSubcategoriesList();
            setSpecialitiesData();

            minBudgetInt = 0;
            maxBudgetInt = 5000;
            filterData.setMinBudget(minBudgetInt);
            filterData.setMaxBudget(maxBudgetInt);
            setBudgetData();
        }
    }

    private void setGenderStatus() {
        if(isMaleChecked && !isFemaleChecked) {
            filterData.setGender(Constants.GENDER_TYPE.MALE);
        } else if(!isMaleChecked && isFemaleChecked) {
            filterData.setGender(Constants.GENDER_TYPE.FEMALE);
        } else if(isMaleChecked && isFemaleChecked) {
            filterData.setGender(Constants.GENDER_TYPE.BOTH_SELECTED);
        } else if(!isMaleChecked && !isFemaleChecked) {
            filterData.setGender(Constants.GENDER_TYPE.NONE);
        }
    }

    private void setTravelTypeStatus() {
        if(isNoChecked && !isYesChecked) {
            filterData.setTravelType(Constants.INTER_STATE_TRAVEL.NO);
        } else if(!isNoChecked && isYesChecked) {
            filterData.setTravelType(Constants.INTER_STATE_TRAVEL.YES);
        } else if(isNoChecked && isYesChecked) {
            filterData.setTravelType(Constants.INTER_STATE_TRAVEL.BOTH_SELECTED);
        } else if(!isNoChecked && !isYesChecked) {
            filterData.setTravelType(Constants.INTER_STATE_TRAVEL.UNSELECTED);
        }
    }

    private void setMinMaxBudget() {
        filterData.setMinBudget(minBudgetInt);
        filterData.setMaxBudget(maxBudgetInt);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_EVENT_TYPE) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    EventViewHolderModel eventViewHolderModel = data.getParcelableExtra(Constants.BundleKey.EVENT_VIEW_HOLDER);
                    int eventId = data.getIntExtra(Constants.BundleKey.EVENT_ID, 0);
                    String eventName = data.getStringExtra(Constants.BundleKey.EVENT_NAME);

                    if(eventId == 0) {
                        showToast(getResources().getString(R.string.selected_event_is_invalid));
                        return;
                    }
                    if(TextUtils.isEmpty(eventName)) {
                        showToast(getResources().getString(R.string.selected_event_is_invalid));
                        return;
                    }
                    if(filterData != null) {
                        filterData.setSelectedEventViewHolder(eventViewHolderModel);
                        setEventData();
                        getEventSubcategoriesList(eventViewHolderModel);
                        setSpecialitiesData();
                    }
                }
            }
        } else if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SEARCH_SPEAKER_SPORTS) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    if(filterData != null) {
//                        ArrayList<Sports> newSportsList = data.getParcelableArrayListExtra(Constants.BundleKey.SPORTS_LIST);
//                        if(newSportsList != null && newSportsList.size() > 0) {
//                            Log.d("speaker_log", "new sports list size : " + newSportsList.size());
//                            for(Sports sports : newSportsList) {
//                                Log.d("speaker_log", "sports name : " + sports.getName());
//                            }
//                        }
                        if(filterData.getSelectedSportsList() != null && filterData.getSelectedSportsList().size() > 0) {
                            filterData.getSelectedSportsList().clear();
                        }
                        filterData.getSelectedSportsList().addAll(data.getParcelableArrayListExtra(Constants.BundleKey.SPORTS_LIST));
                        filterData.setAllSportsSelected(data.getBooleanExtra(Constants.BundleKey.ALL_SPORTS_SELECTED, false));
                        setSportsData();

                        if(filterData.getSelectedTeamsList() != null) {
                            filterData.getSelectedTeamsList().clear();
                        }
                        setTeamsData();
                    }
                }
            }
        } else if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_LOCATION) {
            if(resultCode == RESULT_OK) {
                if (data != null) {
                    filterData.setLatitude(data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0));
                    filterData.setLongitude(data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0));
                    filterData.setAddress(data.getStringExtra(Constants.EXTRA_ADDRESS));

                    if (filterData != null && filterData.getAddress() != null) {
//                        Log.d("location_log", "SearchSpeaker - ActivityResult - mAddressString : "
//                                + filterData.getAddress()
//                                + " latitude : " + filterData.getLatitude() + " longitude : " + filterData.getLongitude());
                        //mBinder.tvLocationSelected.setText(filterData.getAddress());
                    }

                    if(TextUtils.isEmpty(filterData.getAddress())) {
                        mBinder.tvLocationSelected.setText(getResources().getString(R.string.search_by_location));
                        return;
                    }

                    if (filterData != null && filterData.getAddress() != null) {
                        if(filterData.getAddress().equalsIgnoreCase("Unknown")) {
                            mBinder.tvLocationSelected.setText(getResources().getString(R.string.search_by_location));
                            return;
                        }

                        if(!TextUtils.isEmpty(filterData.getAddress())
                                && !filterData.getAddress().equalsIgnoreCase("Unknown")) {
                            mBinder.tvLocationSelected.setText(filterData.getAddress());
                        }
                    }
                }
            }
        } else if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_TEAM_LIST) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    if(filterData != null) {
                        if(filterData.getSelectedTeamsList() != null && filterData.getSelectedTeamsList().size() > 0) {
                            filterData.getSelectedTeamsList().clear();
                        }
                        ArrayList<Team> mSelectedTeamList = new ArrayList<>();
                        mSelectedTeamList.addAll(data.getParcelableArrayListExtra(Constants.BundleKey.SELECTED_TEAMS_LIST));
                        filterData.getSelectedTeamsList().addAll(mSelectedTeamList);
                        setTeamsData();
                    }
                }
            }
        } else if(requestCode == Constants.REQUEST_CODE.REQUEST_CODE_EVENT_SUBCATEGORIES) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    if(filterData != null) {
                        ArrayList<EventSubcategory> eventSubcategoriesList
                                = data.getParcelableArrayListExtra(Constants.EXTRA_EVENT_SUB_CATEGORIES_LIST);
                        if(eventSubcategoriesList != null) {
                            if(filterData.getSelectedEventViewHolder() != null
                                    && filterData.getSelectedEventViewHolder().getEventCategories() != null
                                    && filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories() != null
                                    && filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories().size() > 0) {
                                filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories().clear();
                                filterData.getSelectedEventViewHolder().getEventCategories().getEventSubcategories().addAll(eventSubcategoriesList);
                            }
                            ArrayList<EventSubcategory> selectedEventSubcategoriesList = new ArrayList<>();
                            if(eventSubcategoriesList.size() > 0) {
                                for(EventSubcategory eventSubcategory : eventSubcategoriesList) {
                                    if(eventSubcategory != null && eventSubcategory.isSelected()) {
                                        selectedEventSubcategoriesList.add(eventSubcategory);
                                    }
                                }
                            }

                            if(selectedEventSubcategoriesList != null) {
                                if(selectedEventSubcategoriesList.size() > 0) {
                                    if(selectedEventSubcategoriesList.size() == eventSubcategoriesList.size()) {
                                        filterData.setAllSubcategoriesSelected(true);
                                    } else {
                                        filterData.setAllSubcategoriesSelected(false);
                                    }
                                } else {
                                    filterData.setAllSubcategoriesSelected(true);
                                }

                                if(filterData.getSelectedEventSubcategories() != null
                                        && filterData.getSelectedEventSubcategories().size() > 0) {
                                    filterData.getSelectedEventSubcategories().clear();
                                }
//                                Log.d("speaker_log", "Speaker Filter - selectedEventSubcategoriesList size : " + selectedEventSubcategoriesList.size());
                                filterData.getSelectedEventSubcategories().addAll(selectedEventSubcategoriesList);
                                setSpecialitiesData();
                            }
                        }
                    }
                }
            }
        }
    }

    private ArrayList<EventSubcategory> getEventSubcategoriesList(EventViewHolderModel eventViewHolderModel) {
        if(eventViewHolderModel != null
                && eventViewHolderModel.getEventCategories() != null
                && eventViewHolderModel.getEventCategories().getEventSubcategories() != null
                && eventViewHolderModel.getEventCategories().getEventSubcategories().size() > 0) {
            if(filterData.getSelectedEventSubcategories() != null && filterData.getSelectedEventSubcategories().size() > 0) {
                filterData.getSelectedEventSubcategories().clear();
            }
            filterData.getSelectedEventSubcategories().addAll(eventViewHolderModel.getEventCategories().getEventSubcategories());
            filterData.setAllSubcategoriesSelected(true);
        }
        return filterData.getSelectedEventSubcategories();
    }

    @Override
    public String getActivityName() {
        return "SpeakerFilter";
    }

}
