package com.sportsstars.ui.common;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.appster.chatlib.ChatController;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.sportsstars.BuildConfig;
import com.sportsstars.R;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.CheckVersionResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.LoginActivity;
import com.sportsstars.ui.auth.ViewProfileActivity;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.search.FragmentSearchCoachByName;
import com.sportsstars.ui.search.FragmentSearchCoachByNameNext;
import com.sportsstars.ui.search.FragmentSearchCoachBySport;
import com.sportsstars.ui.search.FragmentSearchCoachBySportNext;
import com.sportsstars.ui.speaker.search.SearchSpeakerResultActivity;
import com.sportsstars.ui.speaker.search.SpeakerProfileActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LocationUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;

/**
 * Created by gautambisht on 11/11/16.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = LogUtils.makeLogTag(BaseActivity.class);
    protected static final int MY_PERMISSION_ACCESS_LOCATION = 101;
    protected boolean mAlive;
    private boolean mActive;
    public ProgressDialog mProgressDialog;
    private Toast mToast;
    protected static PermissionCallback permissionResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setup receiving firebase deep links
        setupFirebaseDeepLinksReceiver();
    }

    private void setupFirebaseDeepLinksReceiver() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            int deepLinkLength = deepLink.toString().length();
                            LogUtils.LOGD(TAG, "Deep link received: " + deepLink.toString());
                            LogUtils.LOGD(TAG, "Coach ID received: " + deepLink.toString().substring(deepLinkLength - 1, deepLinkLength));

                            int lastIndexOfForwardSlash = deepLink.toString().lastIndexOf("/");
                            int coachId = -1;
                            int userRole = -1;

                            String coachIdWithRole = deepLink.toString().substring(lastIndexOfForwardSlash + 1, deepLinkLength);
                            if(coachIdWithRole == null) {
                                return;
                            }
                            if(TextUtils.isEmpty(coachIdWithRole)) {
                                return;
                            }
                            String[] coachIdWithRoleArr = coachIdWithRole.split("_");
                            if(coachIdWithRoleArr == null) {
                                return;
                            }
                            if(coachIdWithRoleArr.length > 1) {
                                String coachIdStr = coachIdWithRoleArr[0];
                                String userRoleStr = coachIdWithRoleArr[1];
                                try {
                                    coachId = Integer.parseInt(coachIdStr);
                                } catch (Exception e) {
                                    LogUtils.LOGD(TAG, "Unable to parse Coach Id from deep link: " + e.getLocalizedMessage());
                                    openAppOnPlayStore();
                                    return;
                                }
                                LogUtils.LOGD(TAG, "Coach ID parsed: " + coachId);

                                try {
                                    userRole = Integer.parseInt(userRoleStr);
                                } catch (Exception e) {
                                    LogUtils.LOGD(TAG, "Unable to parse user role from deep link: " + e.getLocalizedMessage());
                                    openAppOnPlayStore();
                                    return;
                                }
                                LogUtils.LOGD(TAG, "User role parsed: " + userRole);

                                if(userRole == Constants.USER_ROLE.COACH) {
                                    // TODO: 20/04/18 Uncomment API call
                                    getCoachProfile(coachId);
                                } else if(userRole == Constants.USER_ROLE.GUEST_SPEAKER) {
                                    getSpeakerProfile(coachId);
                                }
                            }
                        } else {
                            LogUtils.LOGD(TAG, "Deep link NULL");
                            openAppOnPlayStore();
                        }

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        LogUtils.LOGD(TAG, "getDynamicLink:onFailure", e);
                        openAppOnPlayStore();
                    }
                });
    }

    private void openAppOnPlayStore() {
//        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//        try {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//        } catch (android.content.ActivityNotFoundException anfe) {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//        }
    }

    public static BaseFragment getFragment(Constants.FRAGMENTS fragmentId) {
        BaseFragment fragment = null;

        switch (fragmentId) {
            case SEARCH_BY_SPORT_FRAGMENT:
                fragment = FragmentSearchCoachBySport.newInstance();
                break;

            case SEARCH_BY_NAME_FRAGMENT:
                fragment = FragmentSearchCoachByName.newInstance();
                break;
            case SEARCH_BY_SPORT2_FRAGMENT:
                fragment = FragmentSearchCoachBySportNext.newInstance();
                break;

            case SEARCH_BY_NAME2_FRAGMENT:
                fragment = FragmentSearchCoachByNameNext.newInstance();
                break;
        }

        return fragment;
    }

    abstract public String getActivityName();

    public void showSnackBar(String message) {
        Alert.showSnackBar(findViewById(android.R.id.content), message);
    }

    public void showSnackBar(String message, String buttonText, View.OnClickListener listener) {
        Alert.showSnackBar(findViewById(android.R.id.content), message, buttonText, listener);
    }

    public boolean isActive() {
        return mActive;
    }

    public boolean isAlive() {
        return mAlive;
    }

    public void takePhoto() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        String AUTHORITY = "com.appster.dentamatch.provider";
        Uri photoUri = FileProvider.getUriForFile(this, AUTHORITY, file);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        startActivityForResult(cameraIntent, Constants.REQUEST_CODE.REQUEST_CODE_CAMERA);
    }

    public void getImageFromGallery() {
        Intent gIntent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        gIntent.setType("image/*");
        /*startActivityForResult(
                Intent.createChooser(gIntent, getString(R.string.title_select_file)),
                Constants.REQUEST_CODE.REQUEST_CODE_GALLERY);*/
    }


    @Override
    protected void onDestroy() {
        hideProgressBar();
        mAlive = false;
        Alert.dismissSnackBar();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mActive = true;
         getKeyHash();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mActive = true;
    }

    @Override
    protected void onStop() {
        mActive = false;
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!mAlive && Utils.isNetworkAvailable()) {
            getVersionCheck();
        }
        mAlive = true;
        //hideSoftKeyboard();
    }

    private void getVersionCheck() {
        //processToShowDialog();
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, false);
        authWebServices.checkVersion().enqueue(new BaseCallback<CheckVersionResponse>(this) {
            @Override
            public void onSuccess(CheckVersionResponse response) {
                try {
                    if (response != null && response.getStatus() == 1) {
                        int appVersionCode = 0;
                        try {
                            //int versionCode = BuildConfig.VERSION_CODE;
                            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            //String version = pInfo.versionName;
                            appVersionCode = pInfo.versionCode;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                            appVersionCode = BuildConfig.VERSION_CODE;
                        } catch (Exception e) {
                            e.printStackTrace();
                            appVersionCode = BuildConfig.VERSION_CODE;
                        }

                        if (appVersionCode == 0) {
                            //showToast("Not getting current version code at the moment. Please try again.");
                            return;
                        }
                        if (response.getResult() != null) {
                            int serverVersionCode = response.getResult().getAndroidVersion();
                            if(serverVersionCode == 0) {
                                return;
                            }
                            if (appVersionCode >= 1 && serverVersionCode >= 1 && appVersionCode < serverVersionCode) {
                                PreferenceUtil.setVersionMismatch(true);
                                Alert.showVersionMismatchDialog(BaseActivity.this,
                                        response.getResult().getUpdateInfo().getTitle(),
                                        response.getResult().getUpdateInfo().getDescriptionAndroid(),
                                        response.getResult().getUpdateInfo().getButtonRight(),
                                        response.getResult().getUpdateInfo().getButtonLeft(),
                                        response.getResult().isForceUpdate(),
                                        new Alert.OnOkayClickListener() {
                                            @Override
                                            public void onOkay() {
                                                try {
                                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    //finish();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Alert.OnCancelClickListener() {
                                            @Override
                                            public void onCancel() {
                                            }
                                        });
                            } else {
                                PreferenceUtil.setVersionMismatch(false);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Call<CheckVersionResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LocationUtils.REQUEST_CHECK_SETTINGS:
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(LocationUtils.TAG);
                fragment.onActivityResult(requestCode, resultCode, data);
                break;

            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != MY_PERMISSION_ACCESS_LOCATION) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        } else {

            if (grantResults.length == 0) {
                if (permissionResult != null) permissionResult.permissionsDenied();
                return;
            }

            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_DENIED) {
                    if (permissionResult != null) permissionResult.permissionsDenied();
                    return;
                }
            }

            if (permissionResult != null) {
                permissionResult.permissionsGranted();
            }
        }
    }

    public boolean hasPermission(String permission) {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, permission);
    }

    public interface PermissionCallback {

        void permissionsGranted();

        void permissionsDenied();
    }

    public void showProgressBar() {
        showProgressBar(null, null, null, 0);
    }

    public void showProgressBar(int delayTime) {
        showProgressBar(null, null, null, delayTime);
    }

    public void showProgressBar(String msg) {
        showProgressBar(null, msg, null, 0);
    }

    public void showProgressBar(String msg, int delayTime) {
        showProgressBar(null, msg, null, delayTime);
    }

    public void showProgressBar(final String title, final String msg, final View view, int delayTime) {
        if (mProgressDialog != null) {
            hideProgressBar();
        }
        if (isAlive()) {
            if (delayTime > 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(StringUtils.isNullOrEmpty(msg))
                        {
                            processToShowDialog();
                        }
                        else
                        {
                            processToShowDialogWithText(msg);
                        }

                    }
                }, delayTime);
            } else {
                if(StringUtils.isNullOrEmpty(msg))
                {
                    processToShowDialog();
                }
                else
                {
                    processToShowDialogWithText(msg);
                }
            }
        }
    }

    public void processToShowDialog() {
        try {
            mProgressDialog = ProgressDialog.show(this, null, null);
            if (mProgressDialog.getWindow() != null)
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setContentView(View.inflate(this, R.layout.progress_bar, null));

        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }
    }

    public void processToShowDialogWithText(String message) {
        try {
            mProgressDialog = ProgressDialog.show(this, message, message);
            if (mProgressDialog.getWindow() != null)
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            mProgressDialog.setCancelable(false);
            View rootView = View.inflate(this, R.layout.progress_bar, null);
            TextView tvMessage =  rootView.findViewById(R.id.tvMessage);
            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(message);
            mProgressDialog.setContentView(rootView);


        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }

    }

    public void hideProgressBar() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            mProgressDialog = null;

        } catch (Exception x) {
            LogUtils.LOGE(TAG, x.getMessage());
        }
    }


    //Show toast message and cancel if any previous toast is displaying.

    public void showToast(String message) {
        if (mToast != null) {
            mToast.cancel();
        }

        mToast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        mToast.show();
    }

    public void hideKeyboard() {
        try {
            hideKeyboard(getCurrentFocus());
        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }
    }

    public void hideKeyboard(View view) {
        try {
            if (view != null) {
                view.clearFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }
    }

    private Fragment pushFragment(Constants.FRAGMENTS fragmentId, Bundle args, int containerViewId, boolean addToBackStack, boolean shouldAdd, ANIMATION_TYPE animationType) {
        try {
            BaseFragment fragment = getFragment(fragmentId);
            if (fragment == null) return null;
            if (args != null)
                fragment.setArguments(args);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            switch (animationType) {
                case DEFAULT:
                case SLIDE:
                    ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
//                    ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    break;
                case FADE:
                    ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
                    break;
                case NONE:
                    break;
            }
            if (shouldAdd)
                ft.add(containerViewId, fragment, fragment.getFragmentName());
            else
                ft.replace(containerViewId, fragment, fragment.getFragmentName());
            if (addToBackStack)
                ft.addToBackStack(fragment.getFragmentName());
            if (shouldAdd)
                ft.commit();
            else
                ft.commitAllowingStateLoss();
            return fragment;
        } catch (Exception x) {
            LogUtils.LOGE(TAG, x.getMessage());
        }
        return null;
    }

    @SuppressWarnings("UnusedReturnValue")
    protected Fragment pushFragment(Constants.FRAGMENTS fragmentId, Bundle args, int containerViewId, boolean addToBackStack) {
        return pushFragment(fragmentId, args, containerViewId, addToBackStack, false, ANIMATION_TYPE.DEFAULT);
    }

    private Fragment pushFragment(Constants.FRAGMENTS fragmentId, Bundle args, int containerViewId, boolean addToBackStack, ANIMATION_TYPE animationType) {
        return pushFragment(fragmentId, args, containerViewId, addToBackStack, false, animationType);
    }

    public void pushFragment(BaseFragment fragment, Bundle args, int fragmentId) {
        try {
            if (fragment == null) {
                return;
            }

            if (args != null) {
                fragment.setArguments(args);
            }

            getSupportFragmentManager().executePendingTransactions();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            switch (animationType) {
//                case DEFAULT:
//                case SLIDE:
//                    ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                    break;
//
//                case FADE:
//                    ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
//                    break;
//
//                case NONE:
//                    break;
//            }

            if (fragment.isAdded()) {

            } else {
                //ft.replace(R.id.fragment_container, fragment);
                ft.replace(fragmentId, fragment);

                ft.commitAllowingStateLoss();
            }

        } catch (Exception x) {
            LogUtils.LOGE(TAG, x.getMessage());
        }
    }

    public void showSessionExpiredDialog() {
        Alert.showAlertDialog(BaseActivity.this,
                getString(R.string.session_expired_title),
                getString(R.string.session_expired_message),
                getString(R.string.txt_dlg_btn_ok),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        localLogOut();
                    }
                });
    }

    public void showAuthenticationDialog(String message) {
        Alert.showAlertDialog(BaseActivity.this,
                getString(R.string.auth_error_title),
                message,
                getString(R.string.txt_dlg_btn_ok),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        localLogOut();
                    }
                });
    }

    public void localLogOut() {


            try {
                ChatController.getInstance().closeConnection(this);
            } catch (Exception ex) {
                LogUtils.LOGE(">>>>>>", ex.toString());
            }
//            try {
//                Utils.clearDatabase();
//            } catch (Exception ex) {
//                LogUtils.LOGE(">>>>>>", ex.toString());
//            }

//            SportsStarsApp.destroyAllStaticVariables();
        // String fcmToken= PreferenceUtil.getFcmToken();
        PreferenceUtil.reset();
        // PreferenceUtil.setFcmToken(fcmToken);
        PreferenceUtil.setIsOnBoarding(true);
        PreferenceUtil.setIsLogin(false);
        PreferenceUtil.setCurrentUserRole(1);
        PreferenceUtil.setCoachLaunchFirstTime(false);
        PreferenceUtil.setVersionMismatch(false);
        PreferenceUtil.setCoachRatingShown(false);

        Utils.clearAllNotifications(this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Constants.EXTRA_IS_FORGOT_PASS, true);
        //bundle.putBoolean(Constants.EXTRA_IS_FORGOT_PASS, true);
        intent.putExtra(Constants.EXTRA_IS_DIRECTLY_FROM_WELCOME, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public enum ANIMATION_TYPE {
        SLIDE, FADE, DEFAULT, NONE
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            view.requestFocus();
            if(inputMethodManager != null) {
                inputMethodManager.showSoftInput(view, 0);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void launchImageViewer(View v, String imageUrl) {
        try {
            Intent intent = new Intent(this, ImageViewingActivity.class);
            intent.putExtra(Constants.EXTRA_PIC, imageUrl);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, v, "picImage");
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }
        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }
    }


    public void showSnackbarFromTop(Context ctx, String message) {
        TSnackbar snackbar = TSnackbar
                .make(getWindow().getDecorView().getRootView(), message, TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setIconLeft(R.drawable.ic_explanatory, 24); //Size in dp - 24 is great!
        //snackbar.setIconRight(R.drawable.ic_close, 12); //Resize to bigger dp
        snackbar.setIconPadding(25);
        snackbar.setMaxWidth(3000); //if you want fullsize on tablets
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.app_btn_green));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(ctx, R.color.get_started_color));
        textView.setGravity(Gravity.START);
        textView.setLineSpacing(1, 1.2f);
        textView.setMaxLines(6);

        Typeface face = Typeface.createFromAsset(getAssets(),
                getString(R.string.font_avenir_roman));
        textView.setTypeface(face);
        snackbarView.setPadding(5, 35, 10, 5);

        snackbar.show();
    }

    public void setUpHeader(View headerView, String ttlTxt, String subTtlTxt) {
        if (!StringUtils.isNullOrEmpty(ttlTxt))
            ((TextView) headerView.findViewById(R.id.tv_hdr_ttl)).setText(ttlTxt);
        if (!StringUtils.isNullOrEmpty(subTtlTxt)) {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.VISIBLE);
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setText(subTtlTxt);
        } else {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.GONE);
        }

        ((TextView) headerView.findViewById(R.id.tv_turn_off)).setVisibility(View.GONE);

        headerView.findViewById(R.id.iv_hdr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerView.findViewById(R.id.iv_switch).setVisibility(View.INVISIBLE);
    }

    public void setUpHeader(View headerView, String ttlTxt, Spannable subTtlTxt) {
        if (!StringUtils.isNullOrEmpty(ttlTxt))
            ((TextView) headerView.findViewById(R.id.tv_hdr_ttl)).setText(ttlTxt);
        if (!StringUtils.isNullOrEmpty(subTtlTxt.toString())) {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.VISIBLE);
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setText(subTtlTxt);
        } else {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.GONE);
        }

        ((TextView) headerView.findViewById(R.id.tv_turn_off)).setVisibility(View.GONE);

        headerView.findViewById(R.id.iv_hdr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerView.findViewById(R.id.iv_switch).setVisibility(View.INVISIBLE);
    }

    public String getHeaderTitle(View headerView) {
       return  ((TextView) headerView.findViewById(R.id.tv_hdr_ttl)).getText().toString();
    }


    public void setUpHeaderForChat(View headerView, String ttlTxt, String subTtlTxt, View.OnClickListener mListner) {
        if (!StringUtils.isNullOrEmpty(ttlTxt))
            ((TextView) headerView.findViewById(R.id.tv_hdr_ttl)).setText(ttlTxt);
        if (!StringUtils.isNullOrEmpty(subTtlTxt)) {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.VISIBLE);
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setText(subTtlTxt);
        } else {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.GONE);
        }

        headerView.findViewById(R.id.iv_hdr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView) headerView.findViewById(R.id.tv_turn_off)).setVisibility(View.GONE);
        ImageView ivSwitch = (ImageView) headerView.findViewById(R.id.iv_switch);
        ivSwitch.setImageResource(R.drawable.ic_archived_chats);
        ivSwitch.setVisibility(View.VISIBLE);
        ivSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListner.onClick(view);
            }
        });
    }

    public void setUpHeaderForCoachHome(View headerView, String ttlTxt, String subTtlTxt) {
        if (!StringUtils.isNullOrEmpty(ttlTxt))
            ((TextView) headerView.findViewById(R.id.tv_hdr_ttl)).setText(ttlTxt);
        if (!StringUtils.isNullOrEmpty(subTtlTxt)) {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.VISIBLE);
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setText(subTtlTxt);
        } else {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.GONE);
        }

        headerView.findViewById(R.id.iv_hdr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView) headerView.findViewById(R.id.tv_turn_off)).setVisibility(View.GONE);
        headerView.findViewById(R.id.iv_switch).setVisibility(View.VISIBLE);
        ((ImageView)headerView.findViewById(R.id.iv_switch)).setImageResource(R.drawable.ic_switch_persona);
        headerView.findViewById(R.id.iv_switch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Navigator.getInstance().navigateToActivity(BaseActivity.this, WelcomeActivity.class);
                //finish();
                if(BaseActivity.this != null) {
                    BaseActivity.this.finish();
                    Navigator.getInstance().navigateToActivityWithClearStack(BaseActivity.this, WelcomeActivity.class);
                }
            }
        });

    }

    public void setUpHeaderForCoachAvailability(View headerView, String ttlTxt, String subTtlTxt) {
        if (!StringUtils.isNullOrEmpty(ttlTxt))
            ((TextView) headerView.findViewById(R.id.tv_hdr_ttl)).setText(ttlTxt);
        if (!StringUtils.isNullOrEmpty(subTtlTxt)) {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.VISIBLE);
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setText(subTtlTxt);
        } else {
            ((TextView) headerView.findViewById(R.id.tv_hdr_sub)).setVisibility(View.GONE);
        }

        headerView.findViewById(R.id.iv_hdr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerView.findViewById(R.id.iv_switch).setVisibility(View.INVISIBLE);
        ((ImageView) headerView.findViewById(R.id.iv_switch)).setImageResource(R.drawable.ic_switch_persona);

        ((TextView) headerView.findViewById(R.id.tv_turn_off)).setVisibility(View.VISIBLE);
//        ((TextView) headerView.findViewById(R.id.tv_turn_off)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //Creating the instance of PopupMenu
//                PopupMenu popup = new PopupMenu(BaseActivity.this, headerView.findViewById(R.id.tv_turn_off));
//                //Inflating the Popup using xml file
//                popup.getMenuInflater().inflate(R.menu.popup_menu_calendar_off, popup.getMenu());
//
//                //registering popup with OnMenuItemClickListener
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//                        showToast("Coming Soon");
//                        //Toast.makeText(BaseActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
//                        return true;
//                    }
//                });
//
//                popup.show();//showing popup menu
//            }
//        });
    }


    private void getKeyHash() {

        try

        {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", "My key hash : " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
                LogUtils.LOGE("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

            LogUtils.LOGE(TAG, e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }
    }

    private void getCoachProfile(int id) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfileDetail(id).enqueue(new BaseCallback<GetProfileResponse>(this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        UserProfileInstance.getInstance().setUserProfileModel(response.getResult());
                        Navigator.getInstance().navigateToActivity(BaseActivity.this, ViewProfileActivity.class);
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(BaseActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(BaseActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getSpeakerProfile(int speakerId) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfileDetail(speakerId).enqueue(new BaseCallback<GetProfileResponse>(this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    Bundle bundleSpeakerProfile = new Bundle();
                    bundleSpeakerProfile.putInt(Constants.BundleKey.SPEAKER_ID, speakerId);
                    Navigator.getInstance().navigateToActivityWithData(BaseActivity.this,
                            SpeakerProfileActivity.class, bundleSpeakerProfile);
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(BaseActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(BaseActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        Bundle bundleSpeakerProfile = new Bundle();
//        bundleSpeakerProfile.putInt(Constants.BundleKey.SPEAKER_ID, speakerId);
//        Navigator.getInstance().navigateToActivityWithData(BaseActivity.this,
//                SpeakerProfileActivity.class, bundleSpeakerProfile);
    }

}
