package com.sportsstars.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sportsstars.R;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.onboardtutorial.OnBoardingActivity;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;


/*
 * Written by Appster on 14/04/16.
 */
public class SplashActivity extends BaseActivity implements Runnable {
    private static final int SPLASH_TIME = 500;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // manageFirebaseDeviceToken();
        //Utils.getHashkey(SplashActivity.this);
        handler.postDelayed(SplashActivity.this, SPLASH_TIME);

    }

    @Override
    public String getActivityName() {
        return SplashActivity.class.getSimpleName();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {

        if (!PreferenceUtil.isOnBoarding()) {
            Navigator.getInstance().navigateToActivity(SplashActivity.this, OnBoardingActivity.class);
            finish();
            return;
        }
        startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));
        finish();
    }

}
