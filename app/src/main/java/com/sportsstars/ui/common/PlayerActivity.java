package com.sportsstars.ui.common;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityPlayerBinding;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;

import java.io.IOException;

public class PlayerActivity extends BaseActivity {

    private static final String TAG = "PlayerLog";

    private ActivityPlayerBinding mBinding;
    private SimpleExoPlayer exoPlayer;
    private PlayerEventListener playerEventListener;
    private ProgressDialog progressDialog;

    private String videoURL;
    private long playbackPosition;
    private int currentWindow;
    private boolean playWhenReady = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreenWindow();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_player);

        initViews();
        initializePlayer();
    }

    private void initViews() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.VIDEO_URL)) {
                videoURL = bundle.getString(Constants.BundleKey.VIDEO_URL);
            }
        }

        progressDialog = new ProgressDialog(this);
        //progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        playerEventListener = new PlayerEventListener();
    }

    private void setFullScreenWindow() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initializePlayer() {
        try {
            RenderersFactory renderersFactory = new DefaultRenderersFactory(this,
                    null, DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF);

            if (exoPlayer == null) {
                exoPlayer = ExoPlayerFactory.newSimpleInstance(renderersFactory, new DefaultTrackSelector());
            }
            String userAgent = Util.getUserAgent(this, "ExoPlayer");

            Uri uri = Uri.parse(videoURL);

            MediaSource mediaSource = new ExtractorMediaSource(uri,
                    new DefaultDataSourceFactory(this, userAgent),
                    new DefaultExtractorsFactory(), new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    return false;
                }
            }), new ExtractorMediaSource.EventListener() {
                @Override
                public void onLoadError(IOException error) {
                    Log.d(TAG, "Error : " + error.getMessage());
                }
            });

            exoPlayer.addListener(playerEventListener);
            mBinding.exoPlayerView.setPlayer(exoPlayer);
            exoPlayer.prepare(mediaSource);
            exoPlayer.setPlayWhenReady(playWhenReady);
            exoPlayer.seekTo(currentWindow, playbackPosition);
            exoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        } catch (Exception e) {
            LogUtils.LOGE(TAG," exoplayer error " + e.toString());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideSystemUi();
        if ((Util.SDK_INT <= 23 || exoPlayer == null)) {
            initializePlayer();
        }
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        mBinding.exoPlayerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            playbackPosition = exoPlayer.getCurrentPosition();
            currentWindow = exoPlayer.getCurrentWindowIndex();
            playWhenReady = exoPlayer.getPlayWhenReady();
            exoPlayer.removeListener(playerEventListener);
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private class PlayerEventListener implements Player.EventListener {

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {

        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            String stateString;
            switch (playbackState) {
                case Player.STATE_IDLE:
                    stateString = "ExoPlayer.STATE_IDLE      -";
                    break;
                case Player.STATE_BUFFERING:
                    stateString = "ExoPlayer.STATE_BUFFERING -";
                    progressDialog.show();
                    break;
                case Player.STATE_READY:
                    stateString = "ExoPlayer.STATE_READY     -";
                    progressDialog.cancel();
                    break;
                case Player.STATE_ENDED:
                    progressDialog.cancel();
                    stateString = "ExoPlayer.STATE_ENDED     -";
                    break;
                default:
                    stateString = "UNKNOWN_STATE             -";
                    break;
            }
            LogUtils.LOGD(TAG, "changed state to " + stateString
                    + " playWhenReady: " + playWhenReady);
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            if(error != null && !TextUtils.isEmpty(error.getMessage())) {
                LogUtils.LOGD(TAG, "ExoPlaybackException is : " + error.getMessage());
            } else {
                LogUtils.LOGD(TAG, "ExoPlaybackException object is null");
            }
            progressDialog.cancel();
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
            LogUtils.LOGD(TAG, "onPositionDiscontinuity " + reason);
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            LogUtils.LOGD(TAG, "onPlaybackParametersChanged " + playbackParameters.toString());
        }

        @Override
        public void onSeekProcessed() {
            LogUtils.LOGD(TAG, "onSeekProcessed ");
        }

    }

    @Override
    public String getActivityName() {
        return PlayerActivity.class.getSimpleName();
    }

}
