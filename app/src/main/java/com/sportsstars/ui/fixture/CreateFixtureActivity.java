package com.sportsstars.ui.fixture;

import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityCreateFixtureBinding;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.CreateFixtureRequest;
import com.sportsstars.network.request.auth.UpdateFixtureRequest;
import com.sportsstars.network.response.auth.FixtureData;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;

import java.text.ParseException;
import java.util.Calendar;

import retrofit2.Call;

/**
 * Created by abhaykant on 30/04/18.
 */

public class CreateFixtureActivity extends BaseActivity implements OnDateSelectedListener,View.OnClickListener{
    private ActivityCreateFixtureBinding mBinding;
    private String mTodayDate;
    private String mSelectedDate;
    private String mSelectedTime;
    private int mTodayTime;
    private int mTodayMinute;
    private int mSportId=1;
    private UserProfileModel mSelectedCoachModel;

    // New Data required for Edit Fixture
    // 1. Existing fixture date
    // 2. Existing fixture time
    // 3. Existing fixture id
    // 4. Existing fixture sportId

    private boolean isEditModeEnabled;
    private FixtureData fixtureData;
    private int mFixtureId = -1;

    private String lastFixtureDate;
    private int selectedSportId;

    @Override
    public String getActivityName() {
        return CreateFixtureActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_fixture);

        init();
    }

    private void init() {

        int count = 1;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            count = bundle.getInt(Constants.EXTRA_COUNT);
            if(getIntent().hasExtra(Constants.BundleKey.FIXTURE_DATA)) {
                fixtureData = bundle.getParcelable(Constants.BundleKey.FIXTURE_DATA);
                isEditModeEnabled = true;
            }
//            else {
//                fixtureData = null;
//                isEditModeEnabled = false;
//            }
            if(getIntent().hasExtra(Constants.BundleKey.FIXTURE_SPORT_ID)) {
                selectedSportId = bundle.getInt(Constants.BundleKey.FIXTURE_SPORT_ID);
                isEditModeEnabled = true;
            }
//            else {
//                isEditModeEnabled = false;
//            }
        }

        mBinding.headerId.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));

        if(!isEditModeEnabled) {
            mSelectedCoachModel = UserProfileInstance.getInstance().getUserProfileModel();
            mSportId = mSelectedCoachModel.getSportDetail().getId();
            LogUtils.LOGD("mylog","sportId while creating : " + mSportId);
            mBinding.buttonSave.setText(getResources().getString(R.string.save));
        } else {
            if(fixtureData != null && selectedSportId == 0) {
                mSportId = fixtureData.getSportId();
                mFixtureId = fixtureData.getId();

                String dateTimeInUtc = fixtureData.getFixtureDateTime();
                String dateTimeInLocal = DateFormatUtils.convertUtcToLocal(dateTimeInUtc,
                        DateFormatUtils.SERVER_DATE_FORMAT_TIME,
                        DateFormatUtils.FIXTURE_DATE_FORMAT_TIME);

                String[] dateTimeInLocalArr = dateTimeInLocal.split(" ");
                String dateLocalStr = dateTimeInLocalArr[0];
                String timeLocalStr = dateTimeInLocalArr[1];

//                mSelectedDate = fixtureData.getPlayDate();
//                mSelectedTime = fixtureData.getPlayTime();

                mSelectedDate = dateLocalStr;
                mSelectedTime = timeLocalStr;

                //mSelectedDate = fixtureData.getPlayDate();
                //mSelectedTime = fixtureData.getPlayTime();
                LogUtils.LOGD("mylog","Editing -"
                        + " sport id : " + mSportId
                        + " fixture id : " + mFixtureId
                        + " selected date : " + mSelectedDate
                        + " selected time : " + mSelectedTime
                );
                mBinding.buttonSave.setText(getResources().getString(R.string.update));
            } else {
                mSportId = selectedSportId;
                mBinding.buttonSave.setText(getResources().getString(R.string.save));
            }
        }

//        Bundle bundle = getIntent().getExtras();
//        int count=1;
//        if (bundle != null) {
//            count = bundle.getInt(Constants.EXTRA_COUNT);
//        }
        mBinding.headerId.tvTitle.setText(getString(R.string.week)+" "+count+" "+getString(R.string.fixture));
        mTodayDate = Utils.getTodayDate();
        mTodayTime = Utils.getTodayTime();
        mTodayMinute = Utils.getTodayMinute();
        mBinding.calendarView.setOnDateChangedListener(this);
        mBinding.tvTimeVal.setOnClickListener(this);
        mBinding.buttonSave.setOnClickListener(this);

        if(!StringUtils.isNullOrEmpty(mTodayDate)) {
            String []dateArr = mTodayDate.split("-");
            mBinding.calendarView.state().edit()
                    .setMinimumDate(CalendarDay.from(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1])-1, Integer.parseInt(dateArr[2])))
                    .commit();
        }

        if(isEditModeEnabled) {
            if(fixtureData != null && selectedSportId == 0) {
                String dateTimeInUtc = fixtureData.getFixtureDateTime();
                String dateTimeInLocal = DateFormatUtils.convertUtcToLocal(dateTimeInUtc,
                        DateFormatUtils.SERVER_DATE_FORMAT_TIME,
                        DateFormatUtils.FIXTURE_DATE_FORMAT_TIME);

                String[] dateTimeInLocalArr = dateTimeInLocal.split(" ");
                String dateLocalStr = dateTimeInLocalArr[0];
                String timeLocalStr = dateTimeInLocalArr[1];

//                mSelectedDate = fixtureData.getPlayDate();
//                mSelectedTime = fixtureData.getPlayTime();

                mSelectedDate = dateLocalStr;
                mSelectedTime = timeLocalStr;

                String[] dateArr = dateLocalStr.split("-");
                CalendarDay calendarDay = CalendarDay.from(Integer.parseInt(dateArr[0]),
                        Integer.parseInt(dateArr[1])-1, Integer.parseInt(dateArr[2]));
                mBinding.calendarView.setCurrentDate(calendarDay);
                mBinding.calendarView.setDateSelected(calendarDay, true);

                String[] timeArr = timeLocalStr.split(":");
                String readableTime = Utils.getReadableTimeUI(Integer.parseInt(timeArr[0]), timeArr[1]);
                mBinding.tvTimeVal.setText(readableTime);
            } else {
                if(bundle != null) {
                    if(getIntent().hasExtra(Constants.BundleKey.LAST_FIXTURE_DATE)) {
                        lastFixtureDate = bundle.getString(Constants.BundleKey.LAST_FIXTURE_DATE);
                        if(!TextUtils.isEmpty(lastFixtureDate)) {
                            String[] dateArrEdit = lastFixtureDate.split("-");
                            CalendarDay calendarDayEdit = CalendarDay.from(Integer.parseInt(dateArrEdit[0]),
                                    Integer.parseInt(dateArrEdit[1])-1, Integer.parseInt(dateArrEdit[2]));
                            mBinding.calendarView.setCurrentDate(calendarDayEdit);
                            mBinding.calendarView.setDateSelected(calendarDayEdit, true);
                        }
                    }
                }
            }
        } else {
            if(bundle != null) {
                if(getIntent().hasExtra(Constants.BundleKey.LAST_FIXTURE_DATE)) {
                    lastFixtureDate = bundle.getString(Constants.BundleKey.LAST_FIXTURE_DATE);
                    if(!TextUtils.isEmpty(lastFixtureDate)) {
                        String[] dateArr = lastFixtureDate.split("-");
                        CalendarDay calendarDay = CalendarDay.from(Integer.parseInt(dateArr[0]),
                                Integer.parseInt(dateArr[1])-1, Integer.parseInt(dateArr[2]));
                        mBinding.calendarView.setCurrentDate(calendarDay);
                        mBinding.calendarView.setDateSelected(calendarDay, true);
                    }
                }
            }
        }

    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

         int month=date.getMonth()+1;
         int day=date.getDay();

        String monthStr,strDay;
         if(month<10){
             monthStr="0" +month;
         }else {
             monthStr=""+month;
         }

         if(day<10){
             strDay="0"+day;
         }else {
             strDay=""+day;
         }

        mSelectedDate = date.getYear()+"-"+monthStr+"-"+strDay;
    }

    private void setTime() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String min ;
                        if(minute<10){
                            min="0"+minute;
                        }else {
                            min =""+minute;
                        }
                        mSelectedTime = hourOfDay + ":" + min;

                        String readableTime = Utils.getReadableTimeUI(hourOfDay, min);
                        mBinding.tvTimeVal.setText(readableTime);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tv_time_val:
                setTime();
                break;

            case R.id.button_save:

                mTodayDate = Utils.getTodayDate();
                mTodayTime = Utils.getTodayTime();
                mTodayMinute = Utils.getTodayMinute();

                if(!StringUtils.isNullOrEmpty(mSelectedDate) && !StringUtils.isNullOrEmpty(mSelectedTime)) {
                    String mTimeArr[] = mSelectedTime.split(":");
                    try {
                        int dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
                        if(dateStatus == Constants.DATE_STATUS.GREATER) {
                            addOrUpdateFixture(isEditModeEnabled);
                        } else if ((dateStatus == Constants.DATE_STATUS.EQUAL
                                &&  Integer.parseInt(mTimeArr[0])-mTodayTime>4)) {
                            addOrUpdateFixture(isEditModeEnabled);
                        } else if ((dateStatus == Constants.DATE_STATUS.EQUAL
                                &&  Integer.parseInt(mTimeArr[0])-mTodayTime<=4)) {
                            if(Integer.parseInt(mTimeArr[0])-mTodayTime == 4) {
                                if(Integer.parseInt(mTimeArr[1])-mTodayMinute > 0) {
                                    addOrUpdateFixture(isEditModeEnabled);
                                } else {
                                    if(isEditModeEnabled) {
                                        showSnackbarFromTop(CreateFixtureActivity.this,
                                                getResources().getString(R.string.fixture_edit_time_error_msg));
                                    } else {
                                        showSnackbarFromTop(CreateFixtureActivity.this,
                                                getResources().getString(R.string.fixture_create_time_error_msg));
                                    }
                                }
                            } else if(Integer.parseInt(mTimeArr[0])-mTodayTime < 4) {
                                if(isEditModeEnabled) {
                                    showSnackbarFromTop(CreateFixtureActivity.this,
                                            getResources().getString(R.string.fixture_edit_time_error_msg));
                                } else {
                                    showSnackbarFromTop(CreateFixtureActivity.this,
                                            getResources().getString(R.string.fixture_create_time_error_msg));
                                }
                            }
                        } else {
                            showSnackbarFromTop(CreateFixtureActivity.this,getString(R.string.pls_select_valid_time));
                        }
                    }catch (ParseException e) {
                        LogUtils.LOGE("TAG",e.getMessage());
                    }
                } else {
                    showSnackbarFromTop(this, getString(R.string.pls_select_date_time));
                }
                break;
        }
    }

    private void addOrUpdateFixture(boolean isEditModeEnabled) {
        if(isEditModeEnabled) {
            if(fixtureData != null && selectedSportId == 0) {
                updateFixture(mSelectedDate, mSelectedTime, mFixtureId, mSportId);
            } else {
                createFixture(mSelectedDate, mSelectedTime, mSportId);
            }
        } else {
            createFixture(mSelectedDate, mSelectedTime, mSportId);
        }
    }

    private void createFixture(String fixtureDate, String fixtureTime, int sportId) {

        String newFixtureTime = "";
        String[] timeArr = fixtureTime.split(":");
        if(timeArr.length == 3) {
            newFixtureTime = fixtureTime;
        } else if(timeArr.length == 2) {
            newFixtureTime = fixtureTime + ":00";
        } else {
            newFixtureTime = "";
        }

        if(TextUtils.isEmpty(newFixtureTime)) {
            return;
        }

        Log.d("add_fixture_log", "create fixture - "
                +" fixture date : " + fixtureDate
                + " fixture time : " + fixtureTime
                + " newFixtureTime  : " + newFixtureTime);

        processToShowDialog();

        CreateFixtureRequest createFixtureRequest = new CreateFixtureRequest();
        createFixtureRequest.setFixtureDate(fixtureDate);
        createFixtureRequest.setFixtureTime(fixtureTime);
        createFixtureRequest.setSportId(sportId);
        createFixtureRequest.setFixtureDateTime(
                DateFormatUtils.convertLocalToUtc(fixtureDate + " " + newFixtureTime,
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME));

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.createFixture(createFixtureRequest).enqueue(new BaseCallback<BaseResponse>(CreateFixtureActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    try {
                        if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CreateFixtureActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CreateFixtureActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(CreateFixtureActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateFixture(String fixtureDate, String fixtureTime, int fixtureId, int sportId) {

        String newFixtureTime = "";
        String[] timeArr = fixtureTime.split(":");
        if(timeArr.length == 3) {
            newFixtureTime = fixtureTime;
        } else if(timeArr.length == 2) {
            newFixtureTime = fixtureTime + ":00";
        } else {
            newFixtureTime = "";
        }

        if(TextUtils.isEmpty(newFixtureTime)) {
            return;
        }

        processToShowDialog();

        UpdateFixtureRequest updateFixtureRequest = new UpdateFixtureRequest();
        updateFixtureRequest.setFixtureDate(fixtureDate);
        updateFixtureRequest.setFixtureTime(fixtureTime);
        updateFixtureRequest.setFixtureId(fixtureId);
        updateFixtureRequest.setSportId(sportId);
        updateFixtureRequest.setFixtureDateTime(
                DateFormatUtils.convertLocalToUtc(fixtureDate + " " + newFixtureTime,
                DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.SERVER_DATE_FORMAT_TIME));

        Log.d("add_fixture_log", "update fixture - "
                +" fixture date : " + fixtureDate
                + " fixture time : " + fixtureTime
                + " newFixtureTime  : " + newFixtureTime
                + " converted utc date and time : " + updateFixtureRequest.getFixtureDateTime());

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.updateFixture(updateFixtureRequest).enqueue(new BaseCallback<BaseResponse>(CreateFixtureActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(CreateFixtureActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finish();
                    } else {
                        try {
                            if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(CreateFixtureActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(CreateFixtureActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
