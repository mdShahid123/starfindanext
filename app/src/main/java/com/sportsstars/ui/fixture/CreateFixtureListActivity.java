package com.sportsstars.ui.fixture;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityFixtureListBinding;
import com.sportsstars.interfaces.FixtureItemClickListener;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.FixtureData;
import com.sportsstars.network.response.auth.FixtureList;
import com.sportsstars.network.response.auth.FixtureListResponse;
import com.sportsstars.network.response.auth.UpcomingSports;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.FixtureListAdapter;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import retrofit2.Call;

/**
 * Created by abhaykant on 30/04/18.
 */

public class CreateFixtureListActivity extends BaseActivity implements View.OnClickListener,
        Alert.OnOkayClickListener, FixtureItemClickListener, ListItemClickListener {

    private ActivityFixtureListBinding mBinding;
    private String mSportsName = "";
    private ArrayList<FixtureData> mFixtureList = new ArrayList<>();
    private String mCoachName = "";
    private int mSportsId = 0;
    private UserProfileModel mSelectedCoachModel;
    private boolean isEditModeEnabled;

    private ArrayList<UpcomingSports> upcomingSportsList = new ArrayList<>();
    private ArrayList<FixtureList> fixturesParentList = new ArrayList<>();
    private UpcomingSportsAdapter upcomingSportsAdapter;
    private int selectedSportId;
    private int lastSportSelectedPos = 0;

    private LinkedHashSet<UpcomingSports> upcomingSportsSet = new LinkedHashSet<>();

    @Override
    public String getActivityName() {
        return CreateFixtureListActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_fixture_list);
        Utils.setTransparentTheme(this);
        init();
    }

    private void init() {

        lastSportSelectedPos = 0;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isEditModeEnabled = bundle.getBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED);
        } else {
            isEditModeEnabled = false;
        }

        mSelectedCoachModel = UserProfileInstance.getInstance().getUserProfileModel();
        if (mSelectedCoachModel != null) {
            if (mSelectedCoachModel.getSportDetail() != null
                    && !TextUtils.isEmpty(mSelectedCoachModel.getSportDetail().getName())) {
                mSportsName = mSelectedCoachModel.getSportDetail().getName();
            } else {
                mSportsName = "";
            }

            if (!TextUtils.isEmpty(mSelectedCoachModel.getName())) {
                mCoachName = mSelectedCoachModel.getName();
            } else {
                mCoachName = "";
            }

            if (mSelectedCoachModel.getSportDetail() != null
                    && mSelectedCoachModel.getSportDetail().getId() != 0) {
                mSportsId = mSelectedCoachModel.getSportDetail().getId();
            } else {
                mSportsId = 0;
            }
        }

        if (isEditModeEnabled) {
            setUpHeader(mBinding.headerId, getString(R.string.edit_fixtures), getString(R.string.tell_us_when));
            mBinding.buttonSkipForNow.setVisibility(View.GONE);
            mBinding.buttonComplete.setVisibility(View.GONE);
            mBinding.tvAddYour.setText(getString(R.string.add_your) + " " + getString(R.string.fixture_below));
        } else {
            setUpHeader(mBinding.headerId, getString(R.string.create_fixtures), getString(R.string.tell_us_when));
            mBinding.buttonSkipForNow.setVisibility(View.VISIBLE);
            mBinding.buttonComplete.setVisibility(View.VISIBLE);
            mBinding.tvAddYour.setText(getString(R.string.add_your) + " " + mSportsName + " " + getString(R.string.fixture_below));
        }

        mBinding.tvAddAnother.setOnClickListener(this);
        mBinding.buttonComplete.setOnClickListener(this);
        mBinding.buttonSkipForNow.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isNetworkAvailable()) {
            getFixtureList();
        } else {
            showSnackbarFromTop(this, getResources().getString(R.string.no_internet));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_another:
                if (isEditModeEnabled) {
                    //showToast("Feature to Add New Fixture in Edit Mode is under discussion.");

                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.BundleKey.FIXTURE_SPORT_ID, selectedSportId);
                    if (mFixtureList != null && mFixtureList.size() > 0) {
                        bundle.putInt(Constants.EXTRA_COUNT, mFixtureList.size() + 1);
                        bundle.putString(Constants.BundleKey.LAST_FIXTURE_DATE, mFixtureList.get(mFixtureList.size() - 1).getPlayDate());
                    } else {
                        bundle.putInt(Constants.EXTRA_COUNT, 1);
                        //bundle.putString(Constants.BundleKey.LAST_FIXTURE_DATE, null);
                    }
                    Navigator.getInstance().navigateToActivityWithData(this, CreateFixtureActivity.class, bundle);

                } else {
                    Bundle bundle = new Bundle();
                    if (mFixtureList != null && mFixtureList.size() > 0) {
                        bundle.putInt(Constants.EXTRA_COUNT, mFixtureList.size() + 1);
                        bundle.putString(Constants.BundleKey.LAST_FIXTURE_DATE, mFixtureList.get(mFixtureList.size() - 1).getPlayDate());
                    } else {
                        bundle.putInt(Constants.EXTRA_COUNT, 1);
                        //bundle.putString(Constants.BundleKey.LAST_FIXTURE_DATE, null);
                    }
                    Navigator.getInstance().navigateToActivityWithData(this, CreateFixtureActivity.class, bundle);
                }
                break;

            case R.id.button_complete:

                if (isEditModeEnabled) {
                    finish();
                } else {
                    if (mFixtureList != null && mFixtureList.size() > 0) {
                        String msg = "You have successfully added your " + mSportsName + " fixture. \n" +
                                "\n" +
                                "You’ll now receive tips from " + mCoachName + " or the StarFinda Admin Team on the days you play.";

                        Alert.showAlertDialog(CreateFixtureListActivity.this, getString(R.string.success), msg, getString(R.string.okay), CreateFixtureListActivity.this);
                    } else {
                        showSnackbarFromTop(this, getString(R.string.pls_create_a_fixture));
                    }
                }

                break;

            case R.id.button_skip_for_now:
                if (!PreferenceUtil.isFromSessionDetails()) {
                    Navigator.getInstance().navigateToActivityWithClearStack(this, LearnerHomeActivity.class);
                } else {
                    finish();
                }
                break;
        }
    }


    private void getFixtureList() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.fixtureList().enqueue(new BaseCallback<FixtureListResponse>(CreateFixtureListActivity.this) {
            @Override
            public void onSuccess(FixtureListResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        refreshUI(response);
                    } else {
                        try {
                            if (response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(CreateFixtureListActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<FixtureListResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(CreateFixtureListActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setAdapter(ArrayList<FixtureData> fixturesData) {
        FixtureListAdapter mAdapter = new FixtureListAdapter(this, fixturesData, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mBinding.rvFixture.setLayoutManager(mLayoutManager);
        mBinding.rvFixture.setItemAnimator(new DefaultItemAnimator());
        mBinding.rvFixture.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void setUpcomingSportsAdapter(ArrayList<UpcomingSports> upcomingSportsList) {
        upcomingSportsAdapter = new UpcomingSportsAdapter(this, upcomingSportsList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvSports.setLayoutManager(mLayoutManager);
        mBinding.rvSports.setItemAnimator(new DefaultItemAnimator());
        mBinding.rvSports.setAdapter(upcomingSportsAdapter);
        upcomingSportsAdapter.notifyDataSetChanged();
    }

    private void showUpcomingSportsSection(boolean status) {
        if (status) {
            if (isEditModeEnabled) {
                mBinding.llSportsList.setVisibility(View.VISIBLE);
            } else {
                mBinding.llSportsList.setVisibility(View.GONE);
            }
        } else {
            mBinding.llSportsList.setVisibility(View.GONE);
        }
    }

    private int getSelectedSportPosition(int sportsId) {
        if (sportsId == 0) {
            return 0;
        }
        int position = 0;
        if (upcomingSportsList != null && upcomingSportsList.size() > 0) {
            for (int i = 0; i < upcomingSportsList.size(); i++) {
                if (upcomingSportsList.get(i).getId() == sportsId) {
                    position = i;
                    break;
                }
            }
        }

        return position;
    }

    private void refreshUI(FixtureListResponse response) {
        try {
//        if (response.getResult() != null && response.getResult().getUpcomingSports() != null
//                && response.getResult().getUpcomingSports().size() > 0) {
//            upcomingSportsList = response.getResult().getUpcomingSports();
//            showHideUpcomingSportsSection(true);
//            setUpcomingSportsAdapter(upcomingSportsList);
//        } else {
//            showHideUpcomingSportsSection(false);
//        }

            mFixtureList = new ArrayList<>();
            ArrayList<FixtureData> localFixtureDataList = new ArrayList<>();
            fixturesParentList = new ArrayList<>();
            upcomingSportsList = new ArrayList<>();

            // BELOW CODE IS USED TO DISPLAY SELECTED SPORT FIXTURES LIST IF UPCOMING SPORTS LIST IS NOT EMPTY

            if (response.getResult() != null && response.getResult().getFixtureList() != null
                    && response.getResult().getFixtureList().size() > 0) {
                fixturesParentList = response.getResult().getFixtureList();
            }

            if (fixturesParentList != null && fixturesParentList.size() > 0) {
                for (int i = 0; i < fixturesParentList.size(); i++) {
                    FixtureList fixtureList = fixturesParentList.get(i);
                    if (fixtureList != null) {
                        UpcomingSports upcomingSports = new UpcomingSports();
                        upcomingSports.setId(fixtureList.getSportId());
                        upcomingSports.setName(fixtureList.getSportName());
                        upcomingSports.setIcon(fixtureList.getSportIcon());
                        if (i == 0) {
                            upcomingSports.setSelected(true);
                        } else {
                            upcomingSports.setSelected(false);
                        }
                        upcomingSportsList.add(upcomingSports);
                    }
                }
            } else {
                mFixtureList.addAll(localFixtureDataList);
                setAdapter(mFixtureList);
            }

            if (response.getResult() != null
                    && response.getResult().getUpcomingSports() != null
                    && response.getResult().getUpcomingSports().size() > 0) {
                ArrayList<UpcomingSports> localUpcomingSportsList = new ArrayList<>();
                if (upcomingSportsList != null && upcomingSportsList.size() > 0) {
                    for (UpcomingSports upcomingSportsParentObj : upcomingSportsList) {
                        for (UpcomingSports upcomingSportsChildObj : response.getResult().getUpcomingSports()) {
                            if (upcomingSportsParentObj.getId() != upcomingSportsChildObj.getId()) {
                                localUpcomingSportsList.add(upcomingSportsChildObj);
                            }
                        }
                    }

                    upcomingSportsList.addAll(localUpcomingSportsList);
                    upcomingSportsSet.addAll(upcomingSportsList);
                    upcomingSportsList.clear();
                    upcomingSportsList.addAll(upcomingSportsSet);

                    if (upcomingSportsList != null && upcomingSportsList.size() > 0) {
                        showUpcomingSportsSection(true);
                        setUpcomingSportsAdapter(upcomingSportsList);
                        if(!isEditModeEnabled) {
                            lastSportSelectedPos = getSelectedSportPosition(mSportsId);
                        }
                        setSelectionAndPopulateList(lastSportSelectedPos);
                    } else {
                        showUpcomingSportsSection(false);
                    }
                } else {
                    upcomingSportsList = response.getResult().getUpcomingSports();
                    showUpcomingSportsSection(true);
                    setUpcomingSportsAdapter(upcomingSportsList);
                    if(!isEditModeEnabled) {
                        lastSportSelectedPos = getSelectedSportPosition(mSportsId);
                    }
                    setSelectionAndPopulateList(lastSportSelectedPos);
                }
            } else {
                if (upcomingSportsList != null && upcomingSportsList.size() > 0) {
                    showUpcomingSportsSection(true);
                    setUpcomingSportsAdapter(upcomingSportsList);
                    if(!isEditModeEnabled) {
                        lastSportSelectedPos = getSelectedSportPosition(mSportsId);
                    }
                    setSelectionAndPopulateList(lastSportSelectedPos);
                } else {
                    showUpcomingSportsSection(false);
                    mFixtureList.addAll(localFixtureDataList);
                    setAdapter(mFixtureList);
                }
            }

//        // BELOW CODE IS USED TO DISPLAY ALL FIXTURES IN THE LIST
//
//        if (response.getResult() != null && response.getResult().getFixtureList() != null
//                && response.getResult().getFixtureList().size() > 0) {
//            fixturesParentList = response.getResult().getFixtureList();
//            ArrayList<FixtureList> fixtureLists = response.getResult().getFixtureList();
//            if (fixtureLists != null && fixtureLists.size() > 0) {
//                for (FixtureList fixtureList : fixtureLists) {
//                    if (fixtureList != null && fixtureList.getSportFixtures() != null
//                            && fixtureList.getSportFixtures().size() > 0) {
//                        for (FixtureData fixtureData : fixtureList.getSportFixtures()) {
//                            fixtureData.setSportId(fixtureList.getSportId());
//                            fixtureData.setIcon(fixtureList.getSportIcon());
//                            fixtureData.setName(fixtureList.getSportName());
//                            fixtureData.setId(fixtureData.getId());
//                            fixtureData.setPlayDate(fixtureData.getPlayDate());
//                            fixtureData.setPlayTime(fixtureData.getPlayTime());
//                            fixtureData.setFixtureDateTime(fixtureData.getFixtureDateTime());
//                            localFixtureDataList.add(fixtureData);
//                        }
//                    }
//                }
//            }
//            mFixtureList.addAll(localFixtureDataList);
//            setAdapter(mFixtureList);
//        } else {
//            mFixtureList.addAll(localFixtureDataList);
//            setAdapter(mFixtureList);
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOkay() {
        if (!PreferenceUtil.isFromSessionDetails()) {
            Navigator.getInstance().navigateToActivityWithClearStack(this, LearnerHomeActivity.class);
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (isEditModeEnabled) {
            super.onBackPressed();
        }
        //super.onBackPressed();
    }

    @Override
    public void onFixtureItemClick(FixtureData fixtureData, int position) {
        if (isEditModeEnabled) {
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.BundleKey.FIXTURE_SPORT_ID, 0);
            if (mFixtureList != null && mFixtureList.size() > 0) {
                bundle.putInt(Constants.EXTRA_COUNT, position + 1);
            }
            bundle.putParcelable(Constants.BundleKey.FIXTURE_DATA, fixtureData);
            Navigator.getInstance().navigateToActivityWithData(this, CreateFixtureActivity.class, bundle);
        }
    }

    @Override
    public void onListItemClicked(int position) {
        setSelectionAndPopulateList(position);
    }

    private void setSelectionAndPopulateList(int position) {
        if (upcomingSportsList != null && upcomingSportsList.size() > 0) {
            lastSportSelectedPos = position;
            for (UpcomingSports upcomingSports : upcomingSportsList) {
                if (upcomingSports != null) {
                    upcomingSports.setSelected(false);
                }
            }
            UpcomingSports upcomingSports = upcomingSportsList.get(position);
            if (upcomingSports != null) {
                selectedSportId = upcomingSports.getId();
                upcomingSports.setSelected(true);
                upcomingSportsAdapter.notifyDataSetChanged();
                populateUpcomingSportFixturesList(upcomingSports);
            }
        }
    }

    private void populateUpcomingSportFixturesList(UpcomingSports upcomingSports) {
        if (fixturesParentList != null && fixturesParentList.size() > 0) {
            ArrayList<FixtureData> localFixtureDataList = new ArrayList<>();
            for (FixtureList fixtureList : fixturesParentList) {
                if (fixtureList != null && upcomingSports != null) {
                    if (fixtureList.getSportId() == upcomingSports.getId()
                            && fixtureList.getSportName().equals(upcomingSports.getName())) {
                        if (fixtureList.getSportFixtures() != null
                                && fixtureList.getSportFixtures().size() > 0) {
                            for (FixtureData fixtureData : fixtureList.getSportFixtures()) {
                                fixtureData.setSportId(fixtureList.getSportId());
                                fixtureData.setIcon(fixtureList.getSportIcon());
                                fixtureData.setName(fixtureList.getSportName());
                                fixtureData.setId(fixtureData.getId());
                                fixtureData.setPlayDate(fixtureData.getPlayDate());
                                fixtureData.setPlayTime(fixtureData.getPlayTime());

                                // CHANGES DONE HERE TO RESOLVE ISSUE COMING FROM BACKEND IN DATE TIME FORMAT
                                String correctedFixtureDateTime = correctFixtureDateTimeFormat(fixtureData);
                                fixtureData.setFixtureDateTime(correctedFixtureDateTime);

                                localFixtureDataList.add(fixtureData);
                            }
                        }
                    }
                }
            }
            if (mFixtureList != null) {
                mFixtureList.clear();
                mFixtureList.addAll(localFixtureDataList);
                setAdapter(mFixtureList);
            }
        }
    }

    private String correctFixtureDateTimeFormat(FixtureData fixtureData) {
        String[] dateTimeInUtcArr = fixtureData.getFixtureDateTime().split(" ");
        String dateUtcStr = dateTimeInUtcArr[0];
        String timeUtcStr = dateTimeInUtcArr[1];

        String newFixtureTime = "";
        String[] timeUtcArr = timeUtcStr.split(":");
        if(timeUtcArr.length == 3) {
            newFixtureTime = timeUtcStr;
        } else if(timeUtcArr.length == 2) {
            newFixtureTime = timeUtcStr + ":00";
        }

        return dateUtcStr + " " + newFixtureTime;
    }

}
