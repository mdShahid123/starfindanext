package com.sportsstars.ui.fixture;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemUpcomingSportsBinding;
import com.sportsstars.interfaces.ListItemClickListener;
import com.sportsstars.network.response.auth.UpcomingSports;

import java.util.ArrayList;

public class UpcomingSportsAdapter extends RecyclerView.Adapter<UpcomingSportsAdapter.MyViewHolder> {

    private ArrayList<UpcomingSports> mUpcomingSportsList;
    private final LayoutInflater mInflator;
    private final Activity mActivity;
    private ListItemClickListener mListItemClickListener;

    public UpcomingSportsAdapter(Activity mActivity, ArrayList<UpcomingSports> upcomingSportsList,
                                 ListItemClickListener mListItemClickListener) {
        this.mUpcomingSportsList = upcomingSportsList;
        this.mActivity = mActivity;
        this.mListItemClickListener = mListItemClickListener;
        mInflator = LayoutInflater.from(mActivity);
    }

    @Override
    public UpcomingSportsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemUpcomingSportsBinding mBinding = DataBindingUtil.inflate(mInflator, R.layout.item_upcoming_sports, parent, false);
        return new UpcomingSportsAdapter.MyViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(UpcomingSportsAdapter.MyViewHolder holder, int position) {
        if (mUpcomingSportsList != null && mUpcomingSportsList.size() > 0) {
            holder.bindData(mUpcomingSportsList.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return mUpcomingSportsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ItemUpcomingSportsBinding itemUpcomingSportsBinding;

        public MyViewHolder(ItemUpcomingSportsBinding itemUpcomingSportsBinding) {
            super(itemUpcomingSportsBinding.getRoot());
            this.itemUpcomingSportsBinding = itemUpcomingSportsBinding;
        }

        public ItemUpcomingSportsBinding getBinding() {
            return itemUpcomingSportsBinding;
        }

        public void bindData(UpcomingSports upcomingSports, int position) {
            if(upcomingSports != null) {
                if(!TextUtils.isEmpty(upcomingSports.getName())) {
                    itemUpcomingSportsBinding.flowChild.setText(upcomingSports.getName());
                }
                if(upcomingSports.isSelected()) {
                    itemUpcomingSportsBinding.flowChild.setTextColor(ContextCompat.getColor(mActivity, R.color.white_color));
                    itemUpcomingSportsBinding.flowChild.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_border_green_2_filled));
                } else {
                    itemUpcomingSportsBinding.flowChild.setTextColor(ContextCompat.getColor(mActivity, R.color.green_gender));
                    itemUpcomingSportsBinding.flowChild.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_rounded_border_green_2));
                }
                itemUpcomingSportsBinding.flowChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListItemClickListener.onListItemClicked(position);
                    }
                });
            }
        }

    }

}
