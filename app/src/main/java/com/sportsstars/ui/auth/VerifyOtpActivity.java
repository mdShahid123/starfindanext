package com.sportsstars.ui.auth;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityVerifyOtpBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetOtpRequest;
import com.sportsstars.network.request.auth.VerifyOtpRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.CoachCreateProfileActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.speaker.SpeakerCreateProfileActivity;
import com.sportsstars.ui.speaker.SpeakerHomeActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.widget.otpview.OtpListener;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;

public class VerifyOtpActivity extends BaseActivity implements View.OnClickListener {

    private ActivityVerifyOtpBinding mBinding;
    private String phoneNumber = "";
    private String otpNumber = "";
    private boolean isEditModeEnabled;
    private boolean isForLoginResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify_otp);

        Utils.setTransparentTheme(this);
        getIntentData();
        String otpSubtitle = getResources().getString(R.string.verify_otp_subtitle, phoneNumber);
        Spannable otpSubtitleSpannable = new SpannableString(otpSubtitle);
        otpSubtitleSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.app_btn_green)),
                otpSubtitle.indexOf("+"), otpSubtitle.indexOf(". Please"), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setUpHeader(mBinding.header, getResources().getString(R.string.verify_phone_title), otpSubtitleSpannable);
        initView();

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.PHONE_NUMBER)) {
                phoneNumber = bundle.getString(Constants.BundleKey.PHONE_NUMBER, "");
            }
            if (getIntent().hasExtra(Constants.BundleKey.IS_EDIT_MODE_ENABLED)) {
                isEditModeEnabled = bundle.getBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED);
            }
            if (getIntent().hasExtra(Constants.EXTRA_IS_LOGIN_RESULT)) {
                isForLoginResult = bundle.getBoolean(Constants.EXTRA_IS_LOGIN_RESULT);
            }
        }
    }

    private void initView() {
        mBinding.btnEditPhone.setOnClickListener(this);
        mBinding.btnResend.setOnClickListener(this);

        mBinding.etOtpNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() == 4) {
//                    if(Utils.isNetworkAvailable()) {
//                        if(isPhoneNumberInputValid() && isOtpNumberInputValid()) {
//                            verifyOtp();
//                        }
//                    } else {
//                        showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.no_internet));
//                    }
                }
            }
        });

        mBinding.otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                //Log.d("otp", "onOtpEntered => " + otp);
                //Toast.makeText(VerifyOtpActivity.this, "onOtpEntered => " + otp, Toast.LENGTH_LONG).show();
                if(Utils.isNetworkAvailable()) {
                    if(isPhoneNumberInputValid() && isOtpNumberInputValid(otp)) {
                        verifyOtp();
                    }
                } else {
                    showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEditPhone:
                finish();
                break;
            case R.id.btnResend:
                if(Utils.isNetworkAvailable()) {
                    if(isPhoneNumberInputValid()) {
                        resendOtp();
                    }
                } else {
                    showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    private boolean isOtpNumberInputValid() {
        String otpNumberStr = mBinding.etOtpNumber.getText().toString().trim();
        if(otpNumberStr == null) {
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.please_enter_otp_number));
            return false;
        } else if(TextUtils.isEmpty(otpNumberStr)) {
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.please_enter_otp_number));
            return false;
        } else if (otpNumberStr.length() < 4) {
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.otp_number_char_limit));
            return false;
        }

        return true;
    }

    private boolean isOtpNumberInputValid(String otpStr) {
        if(otpStr == null) {
            otpNumber = "";
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.please_enter_otp_number));
            return false;
        } else if(TextUtils.isEmpty(otpStr)) {
            otpNumber = "";
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.please_enter_otp_number));
            return false;
        } else if (otpStr.length() < 4) {
            otpNumber = otpStr;
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.otp_number_char_limit));
            return false;
        }

        otpNumber = otpStr;

        return true;
    }

    private boolean isPhoneNumberInputValid() {
        if(phoneNumber == null) {
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.please_enter_phone_number));
            return false;
        } else if(TextUtils.isEmpty(phoneNumber)) {
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.please_enter_phone_number));
            return false;
        } else if (phoneNumber.length() < 10 || phoneNumber.length() > 10) {
            showSnackbarFromTop(VerifyOtpActivity.this, getResources().getString(R.string.phone_number_char_limit));
            return false;
        }

        return true;
    }

    private void verifyOtp() {

        //String otpNumber = mBinding.etOtpNumber.getText().toString().trim();

        VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest();
        verifyOtpRequest.setCountryCode(getResources().getString(R.string.country_code_australia));
        verifyOtpRequest.setPhone(phoneNumber);
        verifyOtpRequest.setOtp(otpNumber);

        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.verifyOtp(verifyOtpRequest).enqueue(new BaseCallback<BaseResponse>(VerifyOtpActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                        }
                        EventBus.getDefault().post(new OtpVerifiedEvent(true));
                        if(isEditModeEnabled) {
//                            Navigator.getInstance().navigateToActivityWithClearStack(VerifyOtpActivity.this,
//                                        WelcomeActivity.class);

//                            if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
//                                Navigator.getInstance().navigateToActivity(VerifyOtpActivity.this,
//                                        LearnerHomeActivity.class);
//                            } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
//                                Navigator.getInstance().navigateToActivity(VerifyOtpActivity.this,
//                                        HomeActivity.class);
//                            } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
//                                Navigator.getInstance().navigateToActivity(VerifyOtpActivity.this,
//                                        SpeakerHomeActivity.class);
//                            }
                        } else {
                            if(isForLoginResult) {
                                //setResult(RESULT_OK);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, true);
                                Navigator.getInstance().navigateToActivityWithData(VerifyOtpActivity.this,
                                        com.sportsstars.ui.learner.CreateProfileActivity.class, bundle);
                            } else {
                                if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, false);
                                    Navigator.getInstance().navigateToActivityWithData(VerifyOtpActivity.this,
                                            com.sportsstars.ui.learner.CreateProfileActivity.class, bundle);
                                } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                                    Navigator.getInstance().navigateToActivity(VerifyOtpActivity.this,
                                            CoachCreateProfileActivity.class);
                                } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                                    Navigator.getInstance().navigateToActivity(VerifyOtpActivity.this,
                                            SpeakerCreateProfileActivity.class);
                                }
                            }
                        }
                        finish();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(VerifyOtpActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(baseResponse != null && baseResponse.getMessage() != null
                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                    showSnackbarFromTop(VerifyOtpActivity.this, baseResponse.getMessage());
                }
            }
        });
    }

    private void resendOtp() {
        GetOtpRequest getOtpRequest = new GetOtpRequest();
        getOtpRequest.setCountryCode(getResources().getString(R.string.country_code_australia));
        getOtpRequest.setPhone(phoneNumber);

        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getOtp(getOtpRequest).enqueue(new BaseCallback<BaseResponse>(VerifyOtpActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(VerifyOtpActivity.this, response.getMessage());
                        }
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(VerifyOtpActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(baseResponse != null && baseResponse.getMessage() != null
                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                    showSnackbarFromTop(VerifyOtpActivity.this, baseResponse.getMessage());
                }
            }
        });
    }

    @Override
    public String getActivityName() {
        return VerifyOtpActivity.class.getName();
    }

}
