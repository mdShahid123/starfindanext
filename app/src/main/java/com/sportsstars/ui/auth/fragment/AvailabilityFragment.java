package com.sportsstars.ui.auth.fragment;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentAvailabilityBinding;
import com.sportsstars.model.SelectedDates;
import com.sportsstars.model.SlotTime;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetAvailabilityRequest;
import com.sportsstars.network.response.auth.AvailabilityResult;
import com.sportsstars.network.response.auth.GetCoachAvailabilityResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.ViewProfileActivity;
import com.sportsstars.ui.auth.adapter.AvailabilityForLearnerAdapter;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;

/**
 * Created by atul on 11/04/18.
 * To inject activity reference.
 */

public class AvailabilityFragment extends BaseFragment implements View.OnClickListener {

    private static String TAG = LogUtils.makeLogTag(AvailabilityFragment.class);
    private FragmentAvailabilityBinding mBinding;
    private String[] mDays;
    private String mCurrentMonthStart, mCurrentDayStart;
    private String mCurrentMonthEnd, mCurrentDayEnd;
    private static final String DATE_FORMAT = Utils.DATE_YYYY_MM_DD;
    private Calendar mCalender;
    private int weekDaysCount = 0;
    private ArrayList<AvailabilityResult> mAvailabiltyResults;
    //private ArrayList<SlotTime>mCurrentSlotTimes;
    private String mSelectedDate;
    private ArrayList<SelectedDates> mSelectedWeekDatesList;
    private String mTodayDate;
    private int mTodayTime;
    private String mCurrentYearStart, mCurrentYearEnd;
    private int mCoachId;
    private int mCount = 0;
    private int mSlotTimeSelected = 0;
    private SlotTime mSlotSelected;

    private String mNextDayDate;
    private int mNextDayTime;


    public static AvailabilityFragment newInstance() {
        AvailabilityFragment availabilityFragment = new AvailabilityFragment();
        Bundle bundle = new Bundle();
        availabilityFragment.setArguments(bundle);
        return availabilityFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_availability, null, false);


        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            mCoachId = ((ViewProfileActivity) this.getActivity()).getCoachId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }

    private void init() {

        mCount = 0;
        weekDaysCount = 0;
        mCurrentMonthStart = "";
        mCurrentDayStart = "";
        mCurrentMonthEnd = "";
        mCurrentDayEnd = "";
        mCurrentYearStart = "";
        mCurrentYearEnd = "";
        mSelectedDate = "";
        mSelectedWeekDatesList = new ArrayList<>();

        mBinding.ivNext.setOnClickListener(this);
        mBinding.ivPrevious.setOnClickListener(this);
        mBinding.tvWeekRange.setOnClickListener(this);

        mBinding.itemMon.getRoot().setOnClickListener(this);
        mBinding.itemMon.getRoot().setOnClickListener(this);
        mBinding.itemTue.getRoot().setOnClickListener(this);
        mBinding.itemWed.getRoot().setOnClickListener(this);
        mBinding.itemThu.getRoot().setOnClickListener(this);
        mBinding.itemFri.getRoot().setOnClickListener(this);
        mBinding.itemSat.getRoot().setOnClickListener(this);
        mBinding.itemSun.getRoot().setOnClickListener(this);


        mBinding.itemTue.tvDay.setText(getString(R.string.tue));
        mBinding.itemWed.tvDay.setText(getString(R.string.wed));
        mBinding.itemThu.tvDay.setText(getString(R.string.thu));
        mBinding.itemFri.tvDay.setText(getString(R.string.fri));
        mBinding.itemSat.tvDay.setText(getString(R.string.sat));
        mBinding.itemSun.tvDay.setText(getString(R.string.sun));

        mTodayDate = Utils.getTodayDate();
        mTodayTime = Utils.getTodayTime();

        mNextDayDate = Utils.getNextDayDate();
        mNextDayTime = Utils.getNextDayTime();
        Log.d("date_log", "See Availability - mNextDayDate : " + mNextDayDate
                + " mNextDayTime : " + mNextDayTime);

        setDay(getWeekDay(Calendar.getInstance()), mNextDayDate);
        setWeekRangeUi();
        if (!StringUtils.isNullOrEmpty(mCurrentDayStart)) {
            String startDateFormatted = mCurrentYearStart + "-" + mCurrentMonthStart + "-" + mCurrentDayStart;
            String endDateFormatted = mCurrentYearEnd + "-" + mCurrentMonthEnd + "-" + mCurrentDayEnd;
            getAvailability(startDateFormatted, endDateFormatted);
        }
    }


    private void setDay(String[] days, String selectedDateFormated) {
        if (days == null) {
            return;
        }


        mBinding.itemMon.tvDate.setText(days[0]);
        mBinding.itemTue.tvDate.setText(days[1]);
        mBinding.itemWed.tvDate.setText(days[2]);
        mBinding.itemThu.tvDate.setText(days[3]);
        mBinding.itemFri.tvDate.setText(days[4]);
        mBinding.itemSat.tvDate.setText(days[5]);
        mBinding.itemSun.tvDate.setText(days[6]);

        int selectedDay = 0;
        if (!StringUtils.isNullOrEmpty(selectedDateFormated) && selectedDateFormated.contains("-")) {
            String selectedDateFormatedDup = selectedDateFormated;
            String[] selectedDateFormatedDupArr = selectedDateFormatedDup.split("-");
            selectedDay = Integer.parseInt(selectedDateFormatedDupArr[2]);
        }

        int pos = 0;
        if (selectedDay > 0) {
            for (int i = 0; i < days.length; i++) {
                if (selectedDay == Integer.parseInt(days[i])) {
                    pos = i;
                    break;
                }
            }
        }

        if (pos == 0) {
            setSelectedDateAvailability(mBinding.itemMon.getRoot(), mBinding.itemMon.tvDate, mBinding.itemMon.tvDay);
        } else if (pos == 1) {
            setSelectedDateAvailability(mBinding.itemTue.getRoot(), mBinding.itemTue.tvDate, mBinding.itemTue.tvDay);

        } else if (pos == 2) {
            setSelectedDateAvailability(mBinding.itemWed.getRoot(), mBinding.itemWed.tvDate, mBinding.itemWed.tvDay);

        } else if (pos == 3) {
            setSelectedDateAvailability(mBinding.itemThu.getRoot(), mBinding.itemThu.tvDate, mBinding.itemThu.tvDay);

        } else if (pos == 4) {
            setSelectedDateAvailability(mBinding.itemFri.getRoot(), mBinding.itemFri.tvDate, mBinding.itemFri.tvDay);

        } else if (pos == 5) {
            setSelectedDateAvailability(mBinding.itemSat.getRoot(), mBinding.itemSat.tvDate, mBinding.itemSat.tvDay);

        } else if (pos == 6) {
            setSelectedDateAvailability(mBinding.itemSun.getRoot(), mBinding.itemSun.tvDate, mBinding.itemSun.tvDay);

        }

        if (mSelectedWeekDatesList != null && mSelectedWeekDatesList.size() > 0) {
            mSelectedDate = mSelectedWeekDatesList.get(pos).getSelectedDate();
            LogUtils.LOGD(TAG, "selectedDate:::" + mSelectedDate);
        }

    }

    private void showSetAvailabilityDialog() {
        Alert.showAlertDialogWithCancel(
                getActivity(),
                getResources().getString(R.string.set_availability_alert_title),
                getResources().getString(R.string.set_availability_alert_msg),
                "NO",
                "YES", false, "",
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                }, new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_next:
                mSlotSelected = null;
                String dateFormated = mSelectedWeekDatesList.get(0).getSelectedDate();
                setDay(getWeekDayNext(), dateFormated);
                setWeekRangeUi();
                getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(), mSelectedWeekDatesList.get(6).getSelectedDate());

                if (mCount == 0) {
                    mCount++;
                }
                break;

            case R.id.iv_previous:
                mCount++;
                try {
                    int dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
                    if (dateStatus == Constants.DATE_STATUS.GREATER) {
                        mSlotSelected = null;
                        String days[] = getWeekDayPrev();

                        String dateFormatedPrev = "";
                        String validDate = getValidDate();
                        if (!TextUtils.isEmpty(validDate)) {
                            dateFormatedPrev = validDate;
                        } else {
                            mSelectedWeekDatesList.get(0).getSelectedDate();
                        }

                        //String dateFormatedPrev = mSelectedWeekDatesList.get(0).getSelectedDate();
                        setDay(days, dateFormatedPrev);
                        setWeekRangeUi();
                        getAvailability(mSelectedWeekDatesList.get(0).getSelectedDate(), mSelectedWeekDatesList.get(6).getSelectedDate());
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }

                break;

            case R.id.tv_week_range:
                mSlotSelected = null;
                showDatePicker();
                break;

            case R.id.item_mon:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(0).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemMon.getRoot(), mBinding.itemMon.tvDate, mBinding.itemMon.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
            case R.id.item_tue:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(1).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemTue.getRoot(), mBinding.itemTue.tvDate, mBinding.itemTue.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
            case R.id.item_wed:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(2).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemWed.getRoot(), mBinding.itemWed.tvDate, mBinding.itemWed.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
            case R.id.item_thu:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(3).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemThu.getRoot(), mBinding.itemThu.tvDate, mBinding.itemThu.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
            case R.id.item_fri:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(4).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemFri.getRoot(), mBinding.itemFri.tvDate, mBinding.itemFri.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
            case R.id.item_sat:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(5).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemSat.getRoot(), mBinding.itemSat.tvDate, mBinding.itemSat.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
            case R.id.item_sun:
                try {
                    mSelectedDate = mSelectedWeekDatesList.get(6).getSelectedDate();
                    if (Utils.compareDates(mTodayDate, mSelectedDate)) {
                        mSlotSelected = null;
                        setDefaultTimeSlots(mSelectedDate);
                        setSelectedDateAvailability(mBinding.itemSun.getRoot(), mBinding.itemSun.tvDate, mBinding.itemSun.tvDay);
                    } else {
                        mSelectedDate = mTodayDate;
                    }
                } catch (ParseException e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.getMessage());
                }
                break;
        }
    }

    private String getValidDate() {
        String mDay = "";
        mTodayDate = Utils.getTodayDate();
        if (mSelectedWeekDatesList != null && mSelectedWeekDatesList.size() > 0) {

            for (SelectedDates selectedDate : mSelectedWeekDatesList) {
                if (selectedDate.getSelectedDate().equalsIgnoreCase(mTodayDate)) {
                    mDay = mTodayDate;
                    break;
                }
            }
        }
        return mDay;
    }

    private void setSelectedDateAvailability(View view, TextView tvSelectedDay, TextView tvSelectedDate) {
        Typeface fontfaceAnr = Typeface.createFromAsset(getActivity().getAssets(),
                getActivity().getString(R.string.font_anr));
        Typeface fontfaceMedium = Typeface.createFromAsset(getActivity().getAssets(),
                getActivity().getString(R.string.font_avenir_medium));

        mBinding.itemMon.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemMon.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemMon.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemMon.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemMon.tvDay.setTypeface(fontfaceAnr);


        mBinding.itemTue.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemTue.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemTue.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemTue.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemTue.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemWed.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemWed.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemWed.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemWed.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemWed.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemThu.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemThu.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemThu.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemThu.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemThu.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemFri.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemFri.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemFri.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemFri.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemFri.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemSat.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemSat.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSat.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSat.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemSat.tvDay.setTypeface(fontfaceAnr);

        mBinding.itemSun.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.cal_cell_bg));
        mBinding.itemSun.tvDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSun.tvDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.cal_text_color));
        mBinding.itemSun.tvDate.setTypeface(fontfaceAnr);
        mBinding.itemSun.tvDay.setTypeface(fontfaceAnr);

        view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.app_btn_green));
        tvSelectedDay.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        tvSelectedDate.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

        tvSelectedDate.setTypeface(fontfaceMedium);
        tvSelectedDay.setTypeface(fontfaceMedium);

    }

    private void setWeekRangeUi() {
        try {
            String currentDayStartStr = mCurrentDayStart;
            int currentDayStartInt = Integer.parseInt(currentDayStartStr);
            String daySuffixStart = "th";
            daySuffixStart = Utils.getDateOfTheMonthSuffix(currentDayStartInt);
            daySuffixStart = daySuffixStart.toLowerCase();

            String currentDayEndStr = mCurrentDayEnd;
            int currentDayEndInt = Integer.parseInt(currentDayEndStr);
            String daySuffixEnd = "th";
            daySuffixEnd = Utils.getDateOfTheMonthSuffix(currentDayEndInt);
            daySuffixEnd = daySuffixEnd.toLowerCase();

            mBinding.tvWeekRange.setText(mCurrentDayStart + daySuffixStart + " " + Utils.getMonthInString(Integer.parseInt(mCurrentMonthStart)) + " - " + mCurrentDayEnd + daySuffixEnd + " " + Utils.getMonthInString(Integer.parseInt(mCurrentMonthEnd)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String[] getWeekDay(Calendar now) {
        mSelectedWeekDatesList = new ArrayList<>();
        // Calendar now = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        String[] days = new String[7];
        int delta;

        // check for samsung
        if (android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
            delta = -now.get(Calendar.DAY_OF_WEEK) + 2;
            weekDaysCount++;
        } else {
            delta = -now.get(Calendar.DAY_OF_WEEK) + 1;
        }


        now.add(Calendar.DAY_OF_MONTH, delta);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        for (int i = 0; i < 7; i++) {
            days[i] = format.format(now.getTime());
            Log.i(TAG, "getWeekDay>>" + now.getTime());
            String date = format.format(now.getTime());

            if (date != null) {
                String dateArr[] = date.split("-");
                if (i == 0) {
                    mCurrentMonthStart = dateArr[1];
                    mCurrentDayStart = dateArr[2];
                    mCurrentYearStart = dateArr[0];


                }
                if (i == 6) {
                    mCurrentMonthEnd = dateArr[1];
                    mCurrentDayEnd = dateArr[2];
                    mCurrentYearEnd = dateArr[0];


                }
                SelectedDates selectedDate = new SelectedDates();
                selectedDate.setSelectedDate(dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2]);
                selectedDate.setSelectedDays(dateArr[0]);
                mSelectedWeekDatesList.add(selectedDate);

                days[i] = dateArr[2];


                now.add(Calendar.DAY_OF_MONTH, 1);
            }

        }
        mCalender = now;

        return days;

    }


    public String[] getWeekDayPrev() {
        mSelectedWeekDatesList = new ArrayList<>();

        weekDaysCount--;
        //Calendar now1 = Calendar.getInstance();
        Calendar now1 = mCalender;
        Calendar now = (Calendar) now1.clone();

        // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

        String[] days = new String[7];
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 1;
        now.add(Calendar.WEEK_OF_YEAR, weekDaysCount);
        now.add(Calendar.DAY_OF_MONTH, delta);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        for (int i = 0; i < 7; i++) {
            String date = format.format(now.getTime());
            if (date != null) {
                String dateArr[] = date.split("-");
                if (i == 0) {
                    mCurrentMonthStart = dateArr[1];
                    mCurrentDayStart = dateArr[2];
                    mCurrentYearStart = dateArr[0];

                }
                if (i == 6) {
                    mCurrentMonthEnd = dateArr[1];
                    mCurrentDayEnd = dateArr[2];
                    mCurrentYearEnd = dateArr[0];

                }
                SelectedDates selectedDate = new SelectedDates();
                selectedDate.setSelectedDate(dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2]);
                selectedDate.setSelectedDays(dateArr[0]);
                mSelectedWeekDatesList.add(selectedDate);
                days[i] = dateArr[2];
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

        }

        return days;

    }


    public String[] getWeekDayNext() {
        mSelectedWeekDatesList = new ArrayList<>();
        if (mCount != 0) {
            weekDaysCount++;
        }
        //Calendar now1 = Calendar.getInstance();
        Calendar now1 = mCalender;

        Calendar now = (Calendar) now1.clone();

        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

        String[] days = new String[7];
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 1;
        now.add(Calendar.WEEK_OF_YEAR, weekDaysCount);
        now.add(Calendar.DAY_OF_MONTH, delta);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        for (int i = 0; i < 7; i++) {
            //days [i] = format.format(now.getTime());
            String date = format.format(now.getTime());
            if (date != null) {
                String dateArr[] = date.split("-");
                if (i == 0) {
                    mCurrentMonthStart = dateArr[1];
                    mCurrentDayStart = dateArr[2];
                    mCurrentYearStart = dateArr[0];

                }
                if (i == 6) {
                    mCurrentMonthEnd = dateArr[1];
                    mCurrentDayEnd = dateArr[2];
                    mCurrentYearEnd = dateArr[0];


                }
                SelectedDates selectedDate = new SelectedDates();
                selectedDate.setSelectedDate(dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2]);
                selectedDate.setSelectedDays(dateArr[0]);
                mSelectedWeekDatesList.add(selectedDate);

                days[i] = dateArr[2];
                now.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        return days;

    }


    private void showDatePicker() {
        final Calendar mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(getActivity(), new
                DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int
                            day) {

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_DD_MM_YYYY);
                        String date = simpleDateFormat.format(mCalendar.getTime());

                        SimpleDateFormat yyyymmddFormat = new SimpleDateFormat();
                        yyyymmddFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String dateFormated = yyyymmddFormat.format(mCalendar.getTime());

                        mCalender = mCalendar;
                        try {

                            if (date != null) {
                                mCount++;

                                weekDaysCount = -1;
                                setDay(getWeekDay(mCalendar), dateFormated);
                                setWeekRangeUi();
                            }


                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "Age>" + e.getMessage());

                        }

                    }
                }, year, month,
                day);

        //Calendar minCal = Calendar.getInstance();
        //  minCal.set(Calendar.DATE, minCal.getFirstDayOfWeek());

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        datePickerDialog.show();
    }

    private void getAvailability(final String startDate, String endDate) {
        processToShowDialog();

        GetAvailabilityRequest request = new GetAvailabilityRequest();
        request.setSlotDateStart(startDate);
        request.setSlotDateEnd(endDate);
        request.setCoachId(mCoachId);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getAvailability(request).enqueue(new BaseCallback<GetCoachAvailabilityResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(GetCoachAvailabilityResponse response) {
                hideProgressDialog();
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mAvailabiltyResults = response.getResult();
                        setDefaultTimeSlots(mSelectedDate);
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetCoachAvailabilityResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setDefaultTimeSlots(String date) {
        try {
            ArrayList<SlotTime> mDefaultSlotTimes = Utils.getDefaultCoachAvailabilityTimeSlot();
            final AvailabilityResult availabilitySlotTime = new AvailabilityResult();
            availabilitySlotTime.setSlotDate(date);

            if(mAvailabiltyResults != null && mAvailabiltyResults.size() > 0) {
                for (int i = 0; i < mAvailabiltyResults.size(); i++) {
                    ArrayList<SlotTime> slotTimeArrayList = mAvailabiltyResults.get(i).getSlotTime();
                    if (date.equalsIgnoreCase(mAvailabiltyResults.get(i).getSlotDate())) {

                        // Below code is used earlier for displaying available and unavailable slots.

//                        for (int j = 0; j < slotTimeArrayList.size(); j++) {
//                            for (int k = 0; k < mDefaultSlotTimes.size(); k++) {
//
//                                if (slotTimeArrayList.get(j).getSlotTime() == mDefaultSlotTimes.get(k).getSlotTime()) {
//                                    mDefaultSlotTimes.get(k).setId(slotTimeArrayList.get(j).getId());
//                                    mDefaultSlotTimes.get(k).setStatus(slotTimeArrayList.get(j).getStatus());
//                                    mDefaultSlotTimes.get(k).setSlotTime(slotTimeArrayList.get(j).getSlotTime());
//
//                                }
//                            }
//                        }


                        // Below is the new code used to show unavailable and available slots.

                        ArrayList<Integer> unavailableSlotPositionList = new ArrayList<>();

                        for (SlotTime localSlotTimeObj : mDefaultSlotTimes) {
                            boolean isExist = false;
                            for (SlotTime serverSlotTimeObj : slotTimeArrayList) {
                                if (localSlotTimeObj.getSlotTime() == serverSlotTimeObj.getSlotTime()) {
                                    isExist = true;
                                }
                                if(isExist) {
                                    localSlotTimeObj.setId(serverSlotTimeObj.getId());
                                    localSlotTimeObj.setStatus(serverSlotTimeObj.getStatus());
                                    localSlotTimeObj.setSlotTime(serverSlotTimeObj.getSlotTime());

                                    unavailableSlotPositionList.add(serverSlotTimeObj.getSlotTime());
                                    break;
                                } else {
//                                    if(localSlotTimeObj.getSlotTime() >= 0 && localSlotTimeObj.getSlotTime() <= 7) {
//                                        localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
//                                    } else if(localSlotTimeObj.getSlotTime() >= 8 && localSlotTimeObj.getSlotTime() <= 20) {
//                                        localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                                    } else if(localSlotTimeObj.getSlotTime() >= 21 && localSlotTimeObj.getSlotTime() <= 23) {
//                                        localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
//                                    }

                                    boolean serverExists = false;
                                    for (SlotTime serverSlotTimeObjLocal : slotTimeArrayList) {
                                        if(serverSlotTimeObjLocal.getStatus() == 0) {
                                            serverExists = true;
                                            break;
                                        }
                                    }

                                    if(serverExists) {
                                        localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
                                    } else {
                                        if(localSlotTimeObj.getSlotTime() >= 8 && localSlotTimeObj.getSlotTime() <= 20) {
                                            localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
                                        } else {
                                            localSlotTimeObj.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS);
                                        }
                                    }

                                }
                            }
                        }

//                        ArrayList<Integer> availableSlotPositionList = new ArrayList<>();
//                        for(int kl = 0; kl < 24; kl++) {
//                            if(!unavailableSlotPositionList.contains(kl)) {
//                                availableSlotPositionList.add(kl);
//                            }
//                        }
//
//                        for(int slotPosition : availableSlotPositionList) {
//                            //Log.d("slot_log", " availableSlotPositionList - available position : " + slotPosition);
//                            if(slotPosition >= 0 && slotPosition <= 7) {
//                                mDefaultSlotTimes.get(slotPosition).setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                            } else if(slotPosition >= 21 && slotPosition <= 23) {
//                                mDefaultSlotTimes.get(slotPosition).setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
//                            }
//                        }

                    }
                }
            }

            availabilitySlotTime.setSlotTime(mDefaultSlotTimes);
            //mCurrentSlotTimes=mDefaultSlotTimes;
            final AvailabilityForLearnerAdapter mAdapter = new AvailabilityForLearnerAdapter(getActivity(), availabilitySlotTime);
            mBinding.gridviewTime.setAdapter(mAdapter);

            mBinding.gridviewTime.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                    SlotTime selectedSlotTime = availabilitySlotTime.getSlotTime().get(pos);
                    mTodayTime = Utils.getTodayTime();
                    mTodayDate = Utils.getTodayDate();
                    try {
                        int dateStatus = Utils.compareDatesVal(mTodayDate, mSelectedDate);
                        if(dateStatus == Constants.DATE_STATUS.GREATER) {
                            long dateDiff = DateFormatUtils.getDateDifference(mTodayDate, mSelectedDate);
                            if(dateDiff > 1) {
                                notifyChanges(selectedSlotTime, pos, availabilitySlotTime, mAdapter);
                            } else {
                                if(selectedSlotTime.getSlotTime() <= mTodayTime) {
                                    showSnackbarFromTop(getString(R.string.slot_time_24_hour_limit));
                                } else {
                                    notifyChanges(selectedSlotTime, pos, availabilitySlotTime, mAdapter);
                                }
                            }
                        } else if ((dateStatus==Constants.DATE_STATUS.EQUAL
                                && mTodayTime < selectedSlotTime.getSlotTime())) {
                            //notifyAdapter(selectedSlotTime,mAdapter,availabilitySlotTime);
                            showSnackbarFromTop(getString(R.string.slot_time_24_hour_limit));
                        } else {
                            showSnackbarFromTop(getString(R.string.slot_passed));
                        }
                    } catch (ParseException e) {
                        LogUtils.LOGE(TAG,e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void notifyChanges(SlotTime selectedSlotTime, int pos, final AvailabilityResult availabilitySlotTime,
                               AvailabilityForLearnerAdapter mAdapter) {
        try {
            if (selectedSlotTime.getStatus() != Constants.TIME_SLOT_BOOK_STATUS.SLOT_BOOKED_STATUS &&
                    selectedSlotTime.getStatus() != Constants.TIME_SLOT_BOOK_STATUS.SLOT_UNAVAILABLE_STATUS) {
                if (selectedSlotTime.getStatus() == Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS ||
                        selectedSlotTime.getStatus() == Constants.TIME_SLOT_BOOK_STATUS.SLOT_SELECTED_STATUS) {

                    setOtherSlotStatus(pos, availabilitySlotTime.getSlotTime());
                    if (selectedSlotTime.getStatus() == Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS) {
                        selectedSlotTime.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_SELECTED_STATUS);
                        //mCurrentSlotTimes = availabilitySlotTime.getSlotTime();
                        mSlotSelected = selectedSlotTime;
                    } else {
                        // mCurrentSlotTimes=null;
                        selectedSlotTime.setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
                        mSlotSelected = null;

                    }
                    mSlotTimeSelected = selectedSlotTime.getSlotTime();
                    //mSlotSelected = selectedSlotTime;

                    showSlotSelectionInfoDialog();
                }
            }
            mAdapter.notifyDataSetChanged();
            //mCurrentSlotTimes = availabilitySlotTime.getSlotTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOtherSlotStatus(final int pos, final ArrayList<SlotTime> slotTimes) {
        for (int i = 0; i < slotTimes.size(); i++) {

            if (i != pos) {
                if (slotTimes.get(i).getStatus() == Constants.TIME_SLOT_BOOK_STATUS.SLOT_SELECTED_STATUS) {
                    slotTimes.get(i).setStatus(Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS);
                }
            }

        }
    }

    private void showSlotSelectionInfoDialog() {
        Alert.showInfoDialog(getActivity(),
                getResources().getString(R.string.slot_selection_alert_title),
                getResources().getString(R.string.slot_selection_alert_msg),
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                    }
                });
    }

    public SlotTime getSelectedSlot() {
        return mSlotSelected;
    }

    public String getSlotDate() {
        return mSelectedDate;
    }


}
