package com.sportsstars.ui.auth;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityCreateProfileBinding;
import com.sportsstars.model.SelectedSports;
import com.sportsstars.model.Sports;
import com.sportsstars.model.SuburbInfo;
import com.sportsstars.model.UserModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.CreateProfileRequest;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SportsListResponse;
import com.sportsstars.network.response.auth.UpdateProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.session.SelectLocationActivity;
import com.sportsstars.util.AwsUploadListner;
import com.sportsstars.util.AwsUtil;
import com.sportsstars.util.BitmapUtils;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;

/**
 * Created by abhaykant on 10/01/18.
 */

public class CreateProfileActivity extends BaseActivity implements View.OnClickListener, AwsUploadListner {
    private static final String TAG = LogUtils.makeLogTag(CreateProfileActivity.class);

    private ActivityCreateProfileBinding mBinder;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 101;
    private final int REQUEST_CODE = 1200;
    private CropImageView mCropImageView;
    private Dialog mDialog;
    private final int ASPECT_RATIO_SQUARE_X = 1;
    private final int ASPECT_RATIO_SQUARE_Y = 1;
    private Uri mCropImageUri;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1122;

    private int mGender;
    private int mSkillSet;
    private ArrayList<Sports> mSportsLists;
    private ArrayList<SelectedSports> mSelectedSports;
    private Calendar mCalendar;
    private String mDOB;
    private double mLatitude;
    private double mLongitude;
    private String mSuburb;
    private String mUrl = "";
    private SuburbInfo mSuburbInfo;
    private UserModel profileViewData;


    @Override
    public String getActivityName() {
        return CreateProfileActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_create_profile);

        init();
    }

    private void init() {

        mCalendar = Calendar.getInstance();

        mBinder.imageCamera.setOnClickListener(this);
        mBinder.relWoman.setOnClickListener(this);
        mBinder.relMan.setOnClickListener(this);
        mBinder.relBeginner.setOnClickListener(this);
        mBinder.relIntermediate.setOnClickListener(this);
        mBinder.relExpert.setOnClickListener(this);
        mBinder.etSports.setOnClickListener(this);
        mBinder.etAge.setOnClickListener(this);
        mBinder.etSuburb.setOnClickListener(this);
        mBinder.buttonSave.setOnClickListener(this);
        setDefaultGender();
        setDefaultSkill();
        mBinder.etName.addTextChangedListener(new MyTextWatcher(mBinder.etName));
        mBinder.etFavPlayer.addTextChangedListener(new MyTextWatcher(mBinder.etName));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            profileViewData = bundle.getParcelable(Constants.EXTRA_PROFILE);

            if (profileViewData != null) {
                setProfileData();
            }
        }

    }

    private void setDefaultGender() {
        mGender = 1;
        setGender(mBinder.relMan, mBinder.ivCheckMan, mBinder.tvMan);
    }

    private void setDefaultSkill() {
        mSkillSet = 1;
        setSkill(mBinder.relBeginner, mBinder.ivCheckBeginner, mBinder.tvBeginner);
    }

    private void setProfileData() {

        mBinder.etName.setText(profileViewData.getName());
        mDOB = profileViewData.getAge();
        if (profileViewData.getAge() != null) {
            String age = getAge(profileViewData.getAge()) + " "+getString(R.string.years);
            mBinder.etAge.setText(age);
        }
        mSuburbInfo = profileViewData.getSuburb();
        mBinder.etFavPlayer.setText(profileViewData.getFavPlayer());

        if (profileViewData.getGender() == 1) {
            setDefaultGender();
        } else {
            mGender = 2;
            setGender(mBinder.relWoman, mBinder.ivCheckWoman, mBinder.tvWoman);
        }
        if (profileViewData.getLeanerSport() != null && profileViewData.getLeanerSport().size() > 0) {

            if (profileViewData.getLeanerSport().get(0).getSkillLevel() == 1) {
                mSkillSet = 1;
                setSkill(mBinder.relBeginner, mBinder.ivCheckBeginner, mBinder.tvBeginner);
            } else if (profileViewData.getLeanerSport().get(0).getSkillLevel() == 2) {
                mSkillSet = 2;
                setSkill(mBinder.relIntermediate, mBinder.ivCheckIntermediate, mBinder.tvIntermediate);
            } else if (profileViewData.getLeanerSport().get(0).getSkillLevel() == 3) {
                mSkillSet = 3;
                setSkill(mBinder.relExpert, mBinder.ivCheckExpert, mBinder.tvExpert);
            }

            mSportsLists = new ArrayList<>();
            mSelectedSports = new ArrayList<>();

            if (profileViewData.getLeanerSport() != null && profileViewData.getLeanerSport().size() > 0) {

                getSportsList();
            }


        }

        mBinder.etSuburb.setText(profileViewData.getSuburb().getAddress());
        mLatitude = profileViewData.getSuburb().getLat();
        mLongitude = profileViewData.getSuburb().getLng();

        if (!TextUtils.isEmpty(profileViewData.getProfileImage())) {
            mUrl = profileViewData.getProfileImage();

            if (localImageExists(profileViewData.getProfileImage())) {
                loadFromLocal(profileViewData.getProfileImage());
            } else {
                showProgressBar();
                AwsUtil.beginDownload(profileViewData.getProfileImage(), AwsUtil.getTransferUtility(this)
                        , this, this);

            }
        }

        /*if (!TextUtils.isEmpty(profileViewData.getProfileImage())) {
            AwsUtil.beginDownload(profileViewData.getProfileImage(), AwsUtil.getTransferUtility(this)
                    , this, this);

            mUrl = profileViewData.getProfileImage();

        }*/

    }

    private int getAge(String date) {
        String[] parts = date.split("-");
        Calendar calendar = Calendar.getInstance();
        int age = 0;

        if (parts.length > 2) {
            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            int days = Integer.parseInt(parts[2]);

            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, days);

            try {
                age = Utils.getAge(calendar);
            } catch (Exception e) {
                LogUtils.LOGE(TAG, "getAge>" + e.getMessage());

            }
        }
        return age;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.image_camera:

                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Permission is granted");
                        startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                    } else {
                        Log.d(TAG, "Permission is revoked");
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Log.d(TAG, "Permission is granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                }
//                if (ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            MY_PERMISSIONS_REQUEST_CAMERA);
//                } else {
//                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
//                }



                   /* else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        Utils.askStoragePermission(MY_PERMISSIONS_REQUEST_WRITE_STORAGE, this);
                    } else {
                        startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);
                    }
*/
                break;

            case R.id.rel_woman:
                mGender = 2;

                setGender(mBinder.relWoman, mBinder.ivCheckWoman, mBinder.tvWoman);
                break;

            case R.id.rel_man:
                mGender = 1;
                setGender(mBinder.relMan, mBinder.ivCheckMan, mBinder.tvMan);

                break;

            case R.id.rel_beginner:
                mSkillSet = 1;
                setSkill(mBinder.relBeginner, mBinder.ivCheckBeginner, mBinder.tvBeginner);
                break;

            case R.id.rel_intermediate:
                mSkillSet = 2;

                setSkill(mBinder.relIntermediate, mBinder.ivCheckIntermediate, mBinder.tvIntermediate);

                break;

            case R.id.rel_expert:
                mSkillSet = 3;

                setSkill(mBinder.relExpert, mBinder.ivCheckExpert, mBinder.tvExpert);

                break;
            case R.id.et_sports:
                // Navigator.getInstance().navigateToActivity(this,SelectSportActivity.class);
                Intent intent = new Intent(CreateProfileActivity.this, SelectSportActivity.class);
                intent.putParcelableArrayListExtra(Constants.EXTRA_SPORTS_LIST, mSportsLists);
                startActivityForResult(intent, Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST);

                break;

            case R.id.et_age:
                showDatePicker();
                break;

            case R.id.et_suburb:
                // Navigator.getInstance().navigateToActivity(this,SelectSportActivity.class);
                Intent intentSuburb = new Intent(CreateProfileActivity.this, SelectLocationActivity.class);
                startActivityForResult(intentSuburb, Constants.REQUEST_CODE.REQUEST_CODE_SUBURB);

                break;

            case R.id.button_save:
                if (isInputValid()) {
                    createProfileApi();
                }
                break;


        }
    }

    private void setGender(RelativeLayout relSelected, ImageView ivSelected, TextView tvSelected) {

        mBinder.relWoman.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.ivCheckWoman.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvWoman.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        mBinder.relMan.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.ivCheckMan.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvMan.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        relSelected.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
        ivSelected.setImageResource(R.drawable.ic_check_selected);
        tvSelected.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

    }

    private void setSkill(RelativeLayout relSelected, ImageView ivSelected, TextView tvSelected) {

        mBinder.relBeginner.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.ivCheckBeginner.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvBeginner.setTextColor(ContextCompat.getColor(this, R.color.black_25));


        mBinder.relIntermediate.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.ivCheckIntermediate.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvIntermediate.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        mBinder.relExpert.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinder.ivCheckExpert.setImageResource(R.drawable.ic_check_unselected);
        mBinder.tvExpert.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        relSelected.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
        ivSelected.setImageResource(R.drawable.ic_check_selected);
        tvSelected.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

        if (mSelectedSports != null && mSelectedSports.size() > 0) {
            for (SelectedSports selectedSports : mSelectedSports) {
                selectedSports.setLevel(mSkillSet);
            }
        }

    }


    public void showCropDialog(final Intent data) {
        mDialog = new Dialog(this, android.R.style.Theme_Light);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_image_crop);
        mCropImageView = (CropImageView) mDialog.findViewById(R.id.image_crop);

        mCropImageView.setAspectRatio(ASPECT_RATIO_SQUARE_X, ASPECT_RATIO_SQUARE_Y);


        Button btnCrop = (Button) mDialog.findViewById(R.id.button_crop);
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                // Bitmap cropped = mCropImageView.getCroppedImage(500, 200);
                Bitmap cropped = mCropImageView.getCroppedImage();
                if (cropped != null) {

                    mBinder.imageUser.setImageBitmap(cropped);

                    Uri imageUri = Utils.getPickImageResultUri(data, CreateProfileActivity.this);
                    mCropImageView.setImageUriAsync(imageUri);
                    //LogUtils.LOGD(TAG, " Image Cropped>");

                    Uri uri = getImageUri(cropped);
                    String path = getRealPathFromURI(uri);
                    LogUtils.LOGD("path>>", path);

                    showProgressBar();
                    AwsUtil.beginUpload(path, Constants.AWS_FOLDER_PROFILE, AwsUtil.getTransferUtility
                                    (CreateProfileActivity.this),
                            CreateProfileActivity.this);

                }
            }
        });

        Button buttonCancel = (Button) mDialog.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, getString(R.string.app_name) + Calendar.getInstance(), null);
        return Uri.parse(path);
    }

    private void getCaptureUri(Intent data) {
        showCropDialog(data);
        Uri imageUri = Utils.getPickImageResultUri(data, this);

        // For API >= 23 we need to check specifically that we have permissions to read external storage,
        // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                Utils.isUriRequiresPermissions(imageUri, this)) {

            // request permissions and handle the result in onRequestPermissionsResult()
            requirePermissions = true;
            mCropImageUri = imageUri;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            //LogUtils.LOGD(TAG, "Permission Granted>");

        }

        if (!requirePermissions) {
            mCropImageView.setImageUriAsync(imageUri);
            //LogUtils.LOGD(TAG, " Image Cropped>");
            String path = getRealPathFromURI(imageUri);
            LogUtils.LOGD("path>>", path);
            //initS3Upload(path);
            /* showProgressBar();
            AwsUtil.beginUpload(path, Constants.AWS_FOLDER_PROFILE, AwsUtil.getTransferUtility
                            (this),
                    this);*/

            try {
                if (Utils.getBitmapFromUri(this, imageUri) == null) {
                    //LogUtils.LOGD(TAG, " null>");
                    if (mDialog != null && mDialog.isShowing()) {
                        showSnackbarFromTop(this, "Please select valid image");
                        mDialog.dismiss();
                    }
                }
            } catch (IOException e) {
                //LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());
                LogUtils.LOGE(TAG, "bmp err>" + e.getMessage());

            }


        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {

                getCaptureUri(data);

            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SPORT_LIST) {
                if (data != null) {
                    mSportsLists = data.getParcelableArrayListExtra(Constants.EXTRA_SPORTS_LIST);
                    mSelectedSports = new ArrayList<>();
                    String mSelectedSportsName = "";
                    if (mSportsLists != null) {

                        for (Sports sports : mSportsLists) {
                            if (sports.isSelected()) {
                                SelectedSports selectedSport = new SelectedSports();
                                selectedSport.setSportId(sports.getId());
                                selectedSport.setLevel(mSkillSet);
                                if (TextUtils.isEmpty(mSelectedSportsName)) {
                                    mSelectedSportsName = mSelectedSportsName + sports.getName();
                                } else {
                                    mSelectedSportsName = mSelectedSportsName + ", " + sports.getName();
                                }

                                mSelectedSports.add(selectedSport);

                            }
                        }

                        mBinder.etSports.setText(mSelectedSportsName);
                    }
                }
            } else if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_SUBURB) {
                if (data != null) {
                    mLatitude = data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0);
                    mLongitude = data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0);
                    mSuburb = data.getStringExtra(Constants.EXTRA_ADDRESS);
                    mSuburbInfo = new SuburbInfo();
                    mSuburbInfo.setAddress(mSuburb);
                    mSuburbInfo.setLat(mLatitude);
                    mSuburbInfo.setLng(mLongitude);

                    mBinder.etSuburb.setText(mSuburb);
                }

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            case MY_PERMISSIONS_REQUEST_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Log.d(TAG, "onRequestPermissionsResult storage permission granted");
                    startActivityForResult(Utils.getPickImageChooserIntent(this), REQUEST_CODE);

                } else {
                    // permission denied,
                    Toast.makeText(this, getString(R.string.permision_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCropImageView.setImageUriAsync(mCropImageUri);
                } else {
                    //Toast.makeText(getApplicationContext(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onUploaded(String url, boolean success) {

        if (!success) {
            hideProgressBar();
        }

        if (success) {
            mUrl = url;
            LogUtils.LOGD("url>>>", url);
            AwsUtil.beginDownload(url, AwsUtil.getTransferUtility(this)
                    , this, this);

        } else {
            mUrl = null;
            LogUtils.LOGD("url>>>", url);
        }

    }


    @Override
    public void onDownLoad(String path, boolean success) {
        LogUtils.LOGD("Downloadedurl>>>", path);

        hideProgressBar();

        if (success && path != null) {
            setImage(path);
        }
    }

    private void showDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        // Create a new instance of DatePickerDialog and return it
        datePickerDialog = new DatePickerDialog(CreateProfileActivity.this, new
                DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int
                            day) {

//                            Calendar calendar = Calendar.getInstance();
//                            calendar.set(Calendar.YEAR, year);
//                            calendar.set(Calendar.MONTH, month);
//                            calendar.set(Calendar.DAY_OF_MONTH, day);

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, day);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                        simpleDateFormat.applyPattern(Utils.DATE_YYYY_MM_DD);
                        String date = simpleDateFormat.format(mCalendar.getTime());


                        try {
                            int age = Utils.getAge(mCalendar);
                            if (age > 0) {
                                mDOB = date;
                                mBinder.etAge.setText(age + " " + getString(R.string.years));
                            } else {
                                showSnackbarFromTop(CreateProfileActivity.this, getString(R.string.invalid_age));
                            }
                            LogUtils.LOGD(TAG, "Age:" + Utils.getAge(mCalendar));

                        } catch (Exception e) {
                            LogUtils.LOGE(TAG, "Age>" + e.getMessage());

                        }

                    }
                }, year, month,
                day);

        Calendar minCal = Calendar.getInstance();
        minCal.set(Calendar.YEAR, 1940);
        datePickerDialog.getDatePicker().setMinDate(minCal.getTimeInMillis());

        datePickerDialog.show();
    }

    private void loadFromLocal(String fileName) {
        File mediaDir = new File(Environment.getExternalStoragePublicDirectory(Environment
                .DIRECTORY_PICTURES), getString(R.string.app_name));
        if (mediaDir.exists()) {
            String filepath = mediaDir.getPath() + File.separator + fileName;
            setImage(filepath);
        }
    }

    private boolean localImageExists(String fileName) {
        File mediaDir = new File(Environment.getExternalStoragePublicDirectory(Environment
                .DIRECTORY_PICTURES), getString(R.string.app_name));

        if (mediaDir.exists()) {
            File file = new File(mediaDir.getPath(), fileName);
            return file.exists();
        }

        return false;
    }

    public class MyTextWatcher implements TextWatcher {
        private EditText editText;

        public MyTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = editText.getText().toString();
            if (text.startsWith(" ")) {
                editText.setText(text.trim());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    private boolean isInputValid() {
        boolean isValid = true;

        /*if (mUrl==null || TextUtils.isEmpty(mUrl)) {
            isValid = false;
            showSnackbarFromTop(this,getString(R.string.image_empty));

        }*/
        if (TextUtils.isEmpty(mBinder.etName.getText().toString().trim())) {
            isValid = false;

            mBinder.etName.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_name));
            mBinder.lineName.setBackground(ContextCompat.getDrawable(this, R.color.divider_red_color));


        } else if (TextUtils.isEmpty(mDOB)) {
            isValid = false;
            mBinder.etAge.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_age));
            mBinder.lineAge.setBackground(ContextCompat.getDrawable(this, R.color.divider_red_color));
            mBinder.lineName.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));

        } else if (TextUtils.isEmpty(mBinder.etFavPlayer.getText().toString().trim())) {
            isValid = false;

            mBinder.etFavPlayer.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_fav_player));
            mBinder.lineFavPlayer.setBackground(ContextCompat.getDrawable(this, R.color.divider_red_color));

            mBinder.lineAge.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));
            mBinder.lineName.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));
        } else if (TextUtils.isEmpty(mBinder.etSuburb.getText().toString().trim())) {
            isValid = false;

            mBinder.etSuburb.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_suburb));
        } else if (TextUtils.isEmpty(mBinder.etSports.getText().toString().trim())) {
            isValid = false;

            mBinder.etSports.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_sports));
        }


        if (isValid) {
            mBinder.lineFavPlayer.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));
            mBinder.lineAge.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));
            mBinder.lineName.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));
        }
        return isValid;
    }


    private void createProfileApi() {
        processToShowDialog();

        CreateProfileRequest createProfileRequest = new CreateProfileRequest();
        createProfileRequest.setName(mBinder.etName.getText().toString());
        createProfileRequest.setAge(mDOB);
        createProfileRequest.setFavPalyer(mBinder.etFavPlayer.getText().toString());

        createProfileRequest.setGender(mGender);
        createProfileRequest.setSkillset(mSkillSet);
        createProfileRequest.setRole(1);
        createProfileRequest.setProfileImage(mUrl);
        createProfileRequest.setSports(mSelectedSports);
        createProfileRequest.setSuburb(mSuburbInfo);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateProfile(createProfileRequest).enqueue(new BaseCallback<UpdateProfileResponse>(CreateProfileActivity.this) {
            @Override
            public void onSuccess(UpdateProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showToast(response.getMessage());
                    }

                    UserModel userModel = PreferenceUtil.getUserModel();
                    userModel.setName(mBinder.etName.getText().toString());
                    userModel.setAge(mDOB);
                    userModel.setStatus(1);
                    PreferenceUtil.setUserModel(userModel);

                    if (profileViewData != null) {

                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                        // Navigator.getInstance().navigateToActivityWithClearStack(CreateProfileActivity.this, LearnerProfileViewActivity.class);
                    } else {
                        Navigator.getInstance().navigateToActivityWithClearStack(CreateProfileActivity.this, HomeActivity.class);
                    }
                    finish();
                } else {
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(CreateProfileActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<UpdateProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(CreateProfileActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void setImage(String imgPath) {
        try {
//            File f=new File(imgPath, "imgName.jpg");
            File f = new File(imgPath);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));

            if (b != null) {

                Bitmap bmp = BitmapUtils.scaleBitmapImage(this, imgPath, b.getWidth(), b.getHeight());


                mBinder.imageUser.setImageBitmap(bmp);
            }

        } catch (FileNotFoundException e) {
            LogUtils.LOGE(TAG, "setImage>" + e.getMessage());
        }

    }


    private void getSportsList() {
        processToShowDialog();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>
                (CreateProfileActivity.this) {
            @Override
            public void onSuccess(SportsListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        mSportsLists = response.getResult().getSportsList();
                        String mSelectedSportsName = "";

                        for (Sports learnerSport : profileViewData.getLeanerSport()) {
                           // Sports sport = new Sports();
                            SelectedSports selectedSports = new SelectedSports();
                           /* sport.setId(learnerSport.getId());
                            sport.setDescription(learnerSport.getDescription());
                            sport.setName(learnerSport.getName());
                            sport.setSelected(true);*/
                            learnerSport.setSelected(true);

                            selectedSports.setLevel(mSkillSet);
                            selectedSports.setSportId(learnerSport.getId());

                            if (TextUtils.isEmpty(mSelectedSportsName)) {
                                mSelectedSportsName = mSelectedSportsName + learnerSport.getName();
                            } else {
                                mSelectedSportsName = mSelectedSportsName + ", " + learnerSport.getName();
                            }

                            // mSportsLists.add(sport);
                            mSelectedSports.add(selectedSports);
                        }
                        mBinder.etSports.setText(mSelectedSportsName);

                        for (SelectedSports selectedSports : mSelectedSports) {
                            for (Sports sports : mSportsLists) {
                                if (selectedSports.getSportId() == sports.getId()) {
                                    sports.setSelected(true);
                                }

                            }
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }
}
