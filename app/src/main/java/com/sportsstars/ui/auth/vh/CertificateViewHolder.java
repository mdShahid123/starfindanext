package com.sportsstars.ui.auth.vh;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemCertificateBinding;
import com.sportsstars.model.Certificate;
import com.sportsstars.ui.auth.AddCertificatesActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class CertificateViewHolder extends RecyclerView.ViewHolder {

    private ItemCertificateBinding mBinding;

    public CertificateViewHolder(ItemCertificateBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindData(final Certificate certificate, final IUploadCallback uploadCallback, final int type) {
        mBinding.btnRemove.setVisibility(View.INVISIBLE);
        mBinding.setCertificate(certificate);
//        if(certificate.getCertificateType().equals(Constants.CERTIFICATE_TYPE.WORK_WITH_CHILDREN)) {
//            mBinding.tvCertificateType.setText(R.string.txt_cer_req);
//        } else {
//            mBinding.tvCertificateType.setText(R.string.txt_cer_opt);
//        }
        if(certificate.getCertificateTitle() != null && !TextUtils.isEmpty(certificate.getCertificateTitle())) {
              if(certificate.getCertificateTitle().equalsIgnoreCase(Constants.PUBLIC_LIABILITY_INSURANCE)) {
                  mBinding.tvCertificateType.setText(R.string.txt_cer_opt_public_insurance);
              } else if(certificate.getCertificateTitle().equalsIgnoreCase(Constants.WORK_WITH_CHILDREN_CERTIFICATE)) {
                  mBinding.tvCertificateType.setText(R.string.txt_cer_opt_work_with_children);
              } else {
                  mBinding.tvCertificateType.setText(R.string.txt_cer_opt);
              }
        }

        if(!TextUtils.isEmpty(certificate.getCertificateUrl())
                && certificate.getCertificateUrl().contains("http")) {
            mBinding.ivPdf.setImageResource(R.drawable.ic_jpg);
        } else {
            mBinding.ivPdf.setImageResource(Utils.getMimeType(certificate.getCertificateUrl()));
        }

        if (type < 4 && type > 1)
            mBinding.tvCertificateTtl.setEnabled(false);
        else {
            mBinding.tvCertificateTtl.setEnabled(true);
            mBinding.tvCertificateTtl.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        InputMethodManager imm = (InputMethodManager) itemView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(mBinding.tvCertificateTtl.getWindowToken(), 0);
                        mBinding.tvCertificateTtl.setFocusable(false);
                        mBinding.tvCertificateTtl.setFocusableInTouchMode(true);
                        Log.d("cert_log", "CertificateViewHolder - "
                                + " mBinding.tvCertificateTtl : " + mBinding.tvCertificateTtl.getText().toString()
                                + " editing completed. "
                                +  " certificate id : " + certificate.getId()
                        );
                        uploadCallback.onCertificateTitleChanged(certificate.getId(),
                                mBinding.tvCertificateTtl.getText().toString().trim());
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            mBinding.tvCertificateTtl.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String changedTitle = s.toString().toUpperCase();
                    String certificateTitle = certificate.getCertificateTitle().toUpperCase();
                    Log.d("cert_log", "ViewHolder - certificateId " + certificate.getId()
                            + " certificateTitle : " + certificateTitle
                            + " changedTitle : " + changedTitle);
                    AddCertificatesActivity.isCertTitleChanged = true;
                    AddCertificatesActivity.certId = certificate.getId();
                    AddCertificatesActivity.newCertTitle = changedTitle;
                }
            });
        }
        mBinding.btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadCallback.onUploadSelected(type, certificate.getId());
            }
        });
        mBinding.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadCallback.onDeleteSel(certificate.getId(), type);
            }
        });
    }

    public interface IUploadCallback {
        void onUploadSelected(int type, int id);

        void onDeleteSel(int id, int position);

        void onCertificateTitleChanged(int id, String title);
    }

}
