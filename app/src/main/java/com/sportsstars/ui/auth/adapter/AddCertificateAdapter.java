package com.sportsstars.ui.auth.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemAocLayoutBinding;
import com.sportsstars.databinding.ItemCertificateBinding;
import com.sportsstars.model.Certificate;
import com.sportsstars.ui.auth.vh.CertificateFooterViewHolder;
import com.sportsstars.ui.auth.vh.CertificateViewHolder;
import com.sportsstars.util.Constants;

import java.util.List;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AddCertificateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements CertificateFooterViewHolder.IAocCallback, CertificateViewHolder.IUploadCallback {

    private IUploadCallback iUploadCallback;
    private List<Certificate> mCertificateList;

    public AddCertificateAdapter(IUploadCallback uploadCallback, List<Certificate> certificateList) {
        iUploadCallback = uploadCallback;
        mCertificateList = certificateList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == Constants.CERTIFICATE.FOOTER) {
            viewHolder = new CertificateFooterViewHolder((ItemAocLayoutBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_aoc_layout, parent, false));
        } else
            viewHolder = new CertificateViewHolder((ItemCertificateBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_certificate, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CertificateViewHolder) {
            ((CertificateViewHolder) holder).bindData(mCertificateList.get(position), this, holder.getAdapterPosition());
        } else if (holder instanceof CertificateFooterViewHolder) {
            ((CertificateFooterViewHolder) holder).bindData(this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mCertificateList.get(position).getCertType();
    }

    @Override
    public int getItemCount() {
        return mCertificateList.size();
    }

    @Override
    public void onOptionSelected() {
        mCertificateList.add(mCertificateList.size() - 1, new Certificate().addNewCertificate(mCertificateList.size()-2));
        notifyItemInserted(mCertificateList.size() - 1);
    }

    public void setDocImg(int position, String filePath, int id) {
        Certificate certificate = mCertificateList.get(position);
        certificate.setId(id);
        certificate.setCertificateUrl(filePath);
        notifyItemChanged(position);
    }

    @Override
    public void onUploadSelected(int type, int id) {
        iUploadCallback.onUploadSelected(type, id);
    }

    @Override
    public void onDeleteSel(int id, int position) {
        iUploadCallback.onDelSelected(id, position);
    }

    @Override
    public void onCertificateTitleChanged(int id, String title) {
        iUploadCallback.onCertificateTitleChanged(id, title);
    }

    public interface IUploadCallback {
        void onUploadSelected(int type, int id);
        void onDelSelected(int id, int position);
        void onCertificateTitleChanged(int id, String title);
    }

}
