package com.sportsstars.ui.auth;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityForgotPassBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ForgotPasswordRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Utils;

import retrofit2.Call;

/**
 * Created by abhaykant on 10/01/18.
 */

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {
    private ActivityForgotPassBinding mBinder;

    @Override
    public String getActivityName() {
        return ForgotPasswordActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    private void initUI() {
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_forgot_pass);
        Utils.setTransparentTheme(this);

        mBinder.tvEmailInfo.setVisibility(View.INVISIBLE);
//        mBinder.headerId.tvTitle.setText("");

        mBinder.buttonContinue.setOnClickListener(this);
        //mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this,R.color.divider_red_color));
//        mBinder.headerId.ivBack.setOnClickListener(this);
    }

    private boolean isInputValid() {
        boolean isValid = true;

        if (TextUtils.isEmpty(mBinder.etEmail.getText().toString().trim())) {
            isValid = false;
            mBinder.etEmail.requestFocus();
            showSnackbarFromTop(this, getString(R.string.email_invalid));
        } else if (!Utils.EMAIL_ADDRESS.matcher(mBinder.etEmail.getText().toString().trim()).matches()) {
            isValid = false;
            mBinder.etEmail.requestFocus();
            showSnackbarFromTop(this, getString(R.string.email_invalid));
        } else {
            mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));
        }

        return isValid;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_continue:
                if (isInputValid()) {
                    forgotPassApi();
                }
                break;

            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    private void forgotPassApi() {
        processToShowDialog();

        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
        forgotPasswordRequest.setEmail(mBinder.etEmail.getText().toString().trim());

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.forgotPassword(forgotPasswordRequest).enqueue(new BaseCallback<BaseResponse>(ForgotPasswordActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    showSuccessDialog(getString(R.string.an_email_with_pass));
                    //mBinder.tvEmailInfo.setVisibility(View.VISIBLE);
                } else {
                    try {
                        if((response != null) && (response.getMessage() != null
                                && (!TextUtils.isEmpty(response.getMessage())))) {
                            showSnackbarFromTop(ForgotPasswordActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null) {
                        showSnackbarFromTop(ForgotPasswordActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void showSuccessDialog(final String msg) {
        setResult(RESULT_OK);
        finish();
    }
}
