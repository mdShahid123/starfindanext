package com.sportsstars.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddFacilityNameBinding;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;

public class AddFacilityNameActivity extends BaseActivity implements View.OnClickListener {

    private ActivityAddFacilityNameBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_facility_name);

        mBinding.header.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        mBinding.header.tvTitle.setText(getResources().getString(R.string.add_facility_name));

        mBinding.submitAddFacilityButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.submit_add_facility_button) {
            try {
                hideKeyboard();
            } catch (Exception e) {
                e.printStackTrace();
            }
            addFacilityName();
        }
    }

    private void addFacilityName() {
        String facilityName = mBinding.addFacilityEditText.getText().toString().trim();

        if(TextUtils.isEmpty(facilityName)) {
            showSnackbarFromTop(AddFacilityNameActivity.this, getResources().getString(R.string.add_facility_name_empty_msg));
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_FACILITY_NAME, facilityName);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public String getActivityName() {
        return AddFacilityNameActivity.class.getName();
    }

}
