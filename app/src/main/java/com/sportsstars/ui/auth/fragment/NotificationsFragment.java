package com.sportsstars.ui.auth.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.databinding.FragmentNotificationsBinding;
import com.sportsstars.interfaces.OnNotificationClickListener;
import com.sportsstars.model.ReceiveTipModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.notifications.NotificationData;
import com.sportsstars.network.response.notifications.NotificationsBaseResponse;
import com.sportsstars.network.response.notifications.Tip;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.NotificationsAdapter;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.learner.ReceiveTipActivity;
import com.sportsstars.ui.learner.events.LearnerEventDetailsActivity;
import com.sportsstars.ui.session.SessionCoachDetails;
import com.sportsstars.ui.session.SessionDetailsLearnerActivity;
import com.sportsstars.ui.speaker.SpeakerHomeActivity;
import com.sportsstars.ui.speaker.events.SpeakerEventDetailsActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

public class NotificationsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,OnNotificationClickListener{

    private FragmentNotificationsBinding mBinding;
    private LinearLayoutManager mLayoutManager;
    private int pageNum = 1;
    private NotificationsAdapter mAdapter;
    private ArrayList<NotificationData> mNotificationList;
    private boolean isPaginationNeeded;
    private boolean isFetching;
    private WeakReference<HomeActivity> mCoachHomeActivity;
    private WeakReference<SpeakerHomeActivity> mSpeakerHomeActivity;
    private WeakReference<LearnerHomeActivity> mLearnerHomeActivity;

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                mCoachHomeActivity = new WeakReference<>((HomeActivity) getActivity());
            } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                mSpeakerHomeActivity = new WeakReference<>((SpeakerHomeActivity) getActivity());
            } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                mLearnerHomeActivity = new WeakReference<>((LearnerHomeActivity) getActivity());
            }
        } catch (Exception e) {
            LogUtils.LOGE("Exception", e.getMessage());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }


    private void initViews() {
        /*
        Clear any previous instance associated with this view, this is done to handle single top
        instances notification activity.
         */
        mNotificationList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new NotificationsAdapter(mNotificationList, getActivity(), this);
        mBinding.rvNotificationOther.setLayoutManager(mLayoutManager);
        mBinding.rvNotificationOther.setAdapter(mAdapter);
        mBinding.rvNotificationOther.addItemDecoration(new DividerItemDecoration(getActivity(), RecyclerView.VERTICAL));
        mBinding.rvNotificationOther.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                checkIfItsLastItem();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getOthersNotification(getActivity(), false, false);
            }
        }, 250);

        mBinding.layOtherRefresh.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        pageNum = 1;
        getOthersNotification(getActivity(), false, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mNotificationList!=null && mNotificationList.size()>0)
        {
            for(NotificationData notificationData:mNotificationList)
            {
                notificationData.setIsRead(1);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    private void getOthersNotification(Context ct, final boolean isPagination, final boolean isRefreshing) {
        if (!isPagination && !isRefreshing) {
            showProgressBar();
        }

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.getNotificationList(PreferenceUtil.getCurrentUserRole(), pageNum).enqueue(new BaseCallback<NotificationsBaseResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(NotificationsBaseResponse response) {
                if (mBinding.layOtherRefresh.isRefreshing()) {
                    mBinding.layOtherRefresh.setRefreshing(false);
                }

                if (response != null && response.getStatus() == 1) {

                    if(isRefreshing) {
                        mNotificationList.clear();
                    }

                    if (!isPagination) {
                        mNotificationList.clear();
                    }

                    if (mBinding.layOtherRefresh.isRefreshing()) {
                        mBinding.layOtherRefresh.setRefreshing(false);
                    }

                    if (response.getResult() != null && response.getResult().getData() != null) {
                        mNotificationList.addAll(response.getResult().getData());
                        mAdapter.notifyDataSetChanged();
                    }

                    if (response.getResult() != null && response.getResult().getCurrentPage() < response.getResult().getLastPage()) {
                        isPaginationNeeded = true;
                    } else {
                        isPaginationNeeded = false;
                    }

                    if (mNotificationList.isEmpty()) {
                        mBinding.tvNoNotificationAvailable.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.tvNoNotificationAvailable.setVisibility(View.GONE);
                    }

                    if (mBinding.progressbarLoading.getVisibility() == View.VISIBLE) {
                        mBinding.progressbarLoading.setVisibility(View.GONE);
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<NotificationsBaseResponse> call, BaseResponse baseResponse) {
                try {
                     getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Checks of the adapter has reached its last position to enable pagination if needed.
     */
    private void checkIfItsLastItem() {
        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

        if (isPaginationNeeded) {
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                isPaginationNeeded = false;
                pageNum++;
                mBinding.progressbarLoading.setVisibility(View.VISIBLE);
                getOthersNotification(getActivity(), true, false);
            }
        }
    }

    @Override
    public void onNotificationClicked(NotificationData data) {
        if(Utils.isNetworkAvailable()) {
            if(data != null) {
                switch (data.getNotificationTypeId()) {
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CONFIRMED_FOR_COACH:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CANCELLED_FOR_COACH:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CONFIRMED_FOR_LEARNER:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_CANCELLED_FOR_LEARNER:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_RESCHEDULED_FOR_COACH:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_RESCHEDULED_FOR_LEARNER:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_COACH_24:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_COACH_2:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_LEARNER_24:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_BOOKING_REMINDER_FOR_LEARNER_2:
                        launchSessionDetailsScreen(data);
                        break;
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_COACH_PROFILE_APPROVED:
                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                            if(mCoachHomeActivity != null) {
                                HomeActivity.selectedTabPos = HomeActivity.SET_AVAILABILITY_FRAGMENT_POS;
                                mCoachHomeActivity.get().bottomBar.setCurrentItem(HomeActivity.SET_AVAILABILITY_FRAGMENT_POS);
                                pageNum = 1;
                            }
                        }
                        break;
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_PROFILE_APPROVED:
                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                            if(mSpeakerHomeActivity != null) {
                                SpeakerHomeActivity.selectedTabPos = SpeakerHomeActivity.Home_FRAGMENT_POS;
                                mSpeakerHomeActivity.get().bottomBar.setCurrentItem(SpeakerHomeActivity.Home_FRAGMENT_POS);
                                pageNum = 1;
                            }
                        }
                        break;
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_REQUEST:
//                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
//                            if(mSpeakerHomeActivity != null) {
//                                SpeakerHomeActivity.selectedTabPos = SpeakerHomeActivity.Home_FRAGMENT_POS;
//                                mSpeakerHomeActivity.get().bottomBar.setCurrentItem(SpeakerHomeActivity.Home_FRAGMENT_POS);
//                                pageNum = 1;
//                            }
//                        }
                        launchEventDetailsScreen(data);
                        break;
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_RECEIVE_TIP:
                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                            launchReceiveTipScreen(data);
                        }
                        break;

                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_ACCEPTED:
                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_DECLINED:
//                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
//                            if(mSpeakerHomeActivity != null) {
//                                SpeakerHomeActivity.selectedTabPos = SpeakerHomeActivity.Home_FRAGMENT_POS;
//                                mSpeakerHomeActivity.get().bottomBar.setCurrentItem(SpeakerHomeActivity.Home_FRAGMENT_POS);
//                                pageNum = 1;
//                            }
//                        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
//                            if(mLearnerHomeActivity != null) {
//                                LearnerHomeActivity.selectedTabPos = LearnerHomeActivity.MY_BOOKING;
//                                mLearnerHomeActivity.get().bottomBar.setCurrentItem(LearnerHomeActivity.MY_BOOKING);
//                                pageNum = 1;
//                            }
//                        }
                        launchEventDetailsScreen(data);
                        break;

                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_CANCELLED:
//                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
//                            if(mSpeakerHomeActivity != null) {
//                                SpeakerHomeActivity.selectedTabPos = SpeakerHomeActivity.Home_FRAGMENT_POS;
//                                mSpeakerHomeActivity.get().bottomBar.setCurrentItem(SpeakerHomeActivity.Home_FRAGMENT_POS);
//                                pageNum = 1;
//                            }
//                        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
//                            if(mLearnerHomeActivity != null) {
//                                LearnerHomeActivity.selectedTabPos = LearnerHomeActivity.MY_BOOKING;
//                                mLearnerHomeActivity.get().bottomBar.setCurrentItem(LearnerHomeActivity.MY_BOOKING);
//                                pageNum = 1;
//                            }
//                        }
                        launchCancelledEventDetailsScreen(data);
                        break;

                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_SPEAKER_BOOKING_PAYMENT:
//                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
//                            if(mSpeakerHomeActivity != null) {
//                                SpeakerHomeActivity.selectedTabPos = SpeakerHomeActivity.Home_FRAGMENT_POS;
//                                mSpeakerHomeActivity.get().bottomBar.setCurrentItem(SpeakerHomeActivity.Home_FRAGMENT_POS);
//                                pageNum = 1;
//                            }
//                        }
                        launchEventDetailsScreen(data);
                        break;
//                    case Constants.NOTIFICATION_TYPE.NOTIFICATION_TYPE_CHAT:
//                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
//                            if(mCoachHomeActivity != null) {
//                                HomeActivity.selectedTabPos = HomeActivity.TRACKS_FRAGMENT_POS;
//                                mCoachHomeActivity.get().bottomBar.setCurrentItem(HomeActivity.TRACKS_FRAGMENT_POS);
//                                pageNum = 1;
//                            }
//                        } else {
//                            if(mLearnerHomeActivity != null) {
//                                LearnerHomeActivity.selectedTabPos = LearnerHomeActivity.TRACKS_FRAGMENT_POS;
//                                mLearnerHomeActivity.get().bottomBar.setCurrentItem(LearnerHomeActivity.TRACKS_FRAGMENT_POS);
//                                pageNum = 1;
//                            }
//                        }
//                        break;
                }
            }
        } else {
            showSnackbarFromTop(getResources().getString(R.string.no_internet));
        }
    }

    private void launchSessionDetailsScreen(NotificationData data) {
        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
            Intent i = new Intent(getActivity(), SessionDetailsLearnerActivity.class);
            i.putExtra(getActivity().getString(R.string.booking_id_extra), data.getSessionId());
            getActivity().startActivity(i);
        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
            Intent i = new Intent(getActivity(), SessionCoachDetails.class);
            i.putExtra(getActivity().getString(R.string.booking_id_extra), data.getSessionId());
            getActivity().startActivity(i);
        }
    }

    private void launchEventDetailsScreen(NotificationData data) {
        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
            Intent i = new Intent(getActivity(), SpeakerEventDetailsActivity.class);
            i.putExtra(Constants.BundleKey.SESSION_ID_KEY, data.getGuestSpeakerSessionId());
            getActivity().startActivity(i);
        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
            Intent i = new Intent(getActivity(), LearnerEventDetailsActivity.class);
            i.putExtra(Constants.BundleKey.SESSION_ID_KEY, data.getGuestSpeakerSessionId());
            getActivity().startActivity(i);
        }
    }

    private void launchCancelledEventDetailsScreen(NotificationData data) {
        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
            Intent i = new Intent(getActivity(), SpeakerEventDetailsActivity.class);
            i.putExtra(Constants.BundleKey.SESSION_ID_KEY, data.getGuestSpeakerSessionId());
            getActivity().startActivity(i);
        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
            Intent i = new Intent(getActivity(), LearnerEventDetailsActivity.class);
            i.putExtra(Constants.BundleKey.SESSION_ID_KEY, data.getGuestSpeakerSessionId());
            getActivity().startActivity(i);
        }
    }

    private void launchReceiveTipScreen(NotificationData data) {
        Intent i = new Intent(getActivity(), ReceiveTipActivity.class);
        i.putExtra(Constants.BundleKey.NOTIFICATION_PAYLOAD_TIP_DETAILS, getReceiveTipDetails(data));
        if(data != null && data.getFromUser() != null && !TextUtils.isEmpty(data.getFromUser().getName())) {
            i.putExtra(Constants.BundleKey.COACH_NAME, data.getFromUser().getName());
        }
        startActivity(i);
    }

    private ReceiveTipModel getReceiveTipDetails(NotificationData data) {
        ReceiveTipModel receiveTipModel = null;
        try {
            if (data != null) {
                Tip tip = data.getTip();
                if (tip != null) {
                    receiveTipModel = new ReceiveTipModel();
                    receiveTipModel.setTipId(tip.getId());
                    if (data.getFromUser() != null
                            && !TextUtils.isEmpty(data.getFromUser().getName())) {
                        receiveTipModel.setCoachName(data.getFromUser().getName());
                    }
                    receiveTipModel.setTipType(tip.getType());
                    receiveTipModel.setTipTitle(tip.getTitle());
                    receiveTipModel.setTipMessage(tip.getDescriptionValue());
                    receiveTipModel.setTipImageUrl(tip.getImageUrl());
                    receiveTipModel.setTipVideoThumbnailUrl(tip.getImageUrl());
                    receiveTipModel.setTipVideoUrl(tip.getVideoUrl());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return receiveTipModel;
    }

//    @Override
//    public void onNotificationClicked(OthersData othersData) {
//        if(othersData.getNotificationType().getId()==Constants.NOTIFICATION_TYPE_TAGGED)
//        {
//            if(othersData.getPartyId()!=null)
//            {
//                if(othersData.getPartee().getIsHistory()==1)
//                {
//                    showSnackBarTop(getString(R.string.this_partee_is_over), mBinding.rvNotificationOther, true);
//                }
//                else
//                {
//                    Intent intent = new Intent(getActivity(), ChatActivity.class);
//                    intent.putExtra(Constants.EXTRA_PARTY_ID, othersData.getPartyId());
//                    startActivity(intent);
//                }
//            }
//        }
//        else
//        {
//            if(othersData.getPartyId()!=null)
//            {
//                Intent intent = new Intent(getActivity(), ParteeDetailsActivity.class);
//                intent.putExtra(Constants.EXTRA_PARTY_ID, othersData.getPartyId());
//                intent.putExtra(Constants.EXTRA_MATCH_ID, othersData.getMatchId());
//                intent.putExtra(Constants.EXTRA_IS_HISTORY, othersData.getPartee().getIsHistory());
//                startActivity(intent);
//            }
//        }
//
//    }
}
