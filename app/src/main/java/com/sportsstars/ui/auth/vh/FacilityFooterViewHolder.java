package com.sportsstars.ui.auth.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemAofLayoutBinding;
import com.sportsstars.model.Facility;

import java.util.List;

/**
 * Created by atul on 26/03/18.
 * To inject activity reference.
 */

public class FacilityFooterViewHolder extends RecyclerView.ViewHolder {

    private ItemAofLayoutBinding mBinding;

    public FacilityFooterViewHolder(ItemAofLayoutBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindData(final ISuburbCallback suburbCallback, List<Facility> facilities) {
        if (facilities.size() > 1) {
            Facility facility = facilities.get(facilities.size() - 1);
            facility.setFacilityName(mBinding.getRoot().getContext().getString(R.string.txt_footer_aaf));
            mBinding.tvAac.setText(facility.getFacilityName());
        } else {
            Facility facility = facilities.get(facilities.size() - 1);
            facility.setFacilityName(mBinding.getRoot().getContext().getString(R.string.txt_footer_aof));
            mBinding.tvAac.setText(facility.getFacilityName());
        }
        mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suburbCallback.onAddSuburb();
            }
        });
    }

    public interface ISuburbCallback {
        void onAddSuburb();
    }

}
