package com.sportsstars.ui.auth;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityVerifyPhoneBinding;
import com.sportsstars.eventbus.OtpVerifiedEvent;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetOtpRequest;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;

public class VerifyPhoneActivity extends BaseActivity implements View.OnClickListener {

    private ActivityVerifyPhoneBinding mBinding;
    private boolean isEditModeEnabled;
    private boolean isForLoginResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify_phone);

        if (EventBus.getDefault() != null) {
            EventBus.getDefault().register(this);
        }

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if (getIntent().hasExtra(Constants.BundleKey.IS_EDIT_MODE_ENABLED)) {
                isEditModeEnabled = bundle.getBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED);
            }
            if (getIntent().hasExtra(Constants.EXTRA_IS_LOGIN_RESULT)) {
                isForLoginResult = bundle.getBoolean(Constants.EXTRA_IS_LOGIN_RESULT);
            }
        }

        Utils.setTransparentTheme(this);
        if(!isEditModeEnabled) {
            setUpHeader(mBinding.header, getResources().getString(R.string.verify_phone_title),
                    getResources().getString(R.string.verify_phone_subtitle));
        } else {
            setUpHeader(mBinding.header, getResources().getString(R.string.verify_phone_title),
                    getResources().getString(R.string.verify_phone_subtitle));
        }

        initView();

    }

    private void initView() {
        mBinding.btnVerifyPhone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(Utils.isNetworkAvailable()) {
            if(isInputValid()) {
                verifyPhoneNumber();
            }
        } else {
            showSnackbarFromTop(VerifyPhoneActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private boolean isInputValid() {
        String phoneNumberStr = mBinding.etPhoneNumber.getText().toString().trim();
        if(phoneNumberStr == null) {
            showSnackbarFromTop(VerifyPhoneActivity.this, getResources().getString(R.string.please_enter_phone_number));
            return false;
        } else if(TextUtils.isEmpty(phoneNumberStr)) {
            showSnackbarFromTop(VerifyPhoneActivity.this, getResources().getString(R.string.please_enter_phone_number));
            return false;
        } else if (phoneNumberStr.length() < 10 || phoneNumberStr.length() > 10) {
            showSnackbarFromTop(VerifyPhoneActivity.this, getResources().getString(R.string.phone_number_char_limit));
            return false;
        }

        return true;
    }

    private void verifyPhoneNumber() {

        GetOtpRequest getOtpRequest = new GetOtpRequest();
        getOtpRequest.setCountryCode(getResources().getString(R.string.country_code_australia));
        //getOtpRequest.setCountryCode(getResources().getString(R.string.country_code_india));
        getOtpRequest.setPhone(mBinding.etPhoneNumber.getText().toString().trim());

        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getOtp(getOtpRequest).enqueue(new BaseCallback<BaseResponse>(VerifyPhoneActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showToast(response.getMessage());
                        }
                        goToVerifyOtpScreen();
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(VerifyPhoneActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if(baseResponse != null && baseResponse.getMessage() != null
                        && !TextUtils.isEmpty(baseResponse.getMessage())) {
                    showSnackbarFromTop(VerifyPhoneActivity.this, baseResponse.getMessage());
                }
            }
        });
    }

    private void goToVerifyOtpScreen() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString().trim();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BundleKey.PHONE_NUMBER, phoneNumber);
        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, isEditModeEnabled);
        bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, isForLoginResult);
        Navigator.getInstance().navigateToActivityWithData(VerifyPhoneActivity.this, VerifyOtpActivity.class, bundle);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault() != null) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onEvent(OtpVerifiedEvent otpVerifiedEvent) {
        if(otpVerifiedEvent.isOtpVerified()) {
            finish();
        }
    }

    @Override
    public String getActivityName() {
        return VerifyPhoneActivity.class.getName();
    }

}
