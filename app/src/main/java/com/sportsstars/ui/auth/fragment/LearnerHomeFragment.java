package com.sportsstars.ui.auth.fragment;

import android.arch.lifecycle.ViewModelProvider;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.MVVM.viewmodels.mysession.HomeActivityViewModel;
import com.sportsstars.R;
import com.sportsstars.databinding.LearnerHomeFragmentBinding;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.search.SearchCoachActivity;
import com.sportsstars.ui.search.SearchCoachSpeakerActivity;
import com.sportsstars.ui.speaker.search.SearchSpeakerTypeActivity;
import com.sportsstars.ui.speaker.search.SelectEventTypeActivity;
import com.sportsstars.ui.speaker.search.SelectSpeakerSportsActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;

import javax.inject.Inject;

/**
 * Created by abhaykant on 01/05/18.
 */

public class LearnerHomeFragment extends BaseFragment implements View.OnClickListener {
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private LearnerHomeFragmentBinding mBinding;
    HomeActivityViewModel homeActivityViewModel;
    String TAG = "LearnerHomeFragment";

    @Override
    public String getFragmentName() {
        return LearnerHomeFragment.class.getSimpleName();
    }

    public static LearnerHomeFragment newInstance() {
        LearnerHomeFragment learnerHomeFragment = new LearnerHomeFragment();
        Bundle bundle = new Bundle();
        learnerHomeFragment.setArguments(bundle);
        return learnerHomeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.learner_home_fragment, null, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        mBinding.relCoachMain.setOnClickListener(this);
        mBinding.relSpeakMain.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.rel_coach_main:
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, true);
                Navigator.getInstance().navigateToActivityWithData(getBaseActivity(), SearchCoachActivity.class, bundle);
                break;

            case R.id.rel_speak_main:
//                showSnackbarFromTop("Coming Soon");

                Bundle bundleSpeaker = new Bundle();
                bundleSpeaker.putBoolean(Constants.BundleKey.IS_COACH_SEARCH, false);
                bundleSpeaker.putInt(Constants.BundleKey.IS_PANEL, 0);
                bundleSpeaker.putBoolean(Constants.BundleKey.IS_FROM_SPEAKER_FILTER, false);
                //Navigator.getInstance().navigateToActivityWithData(getBaseActivity(), SearchSpeakerTypeActivity.class, bundleSpeaker);
                Navigator.getInstance().navigateToActivityWithData(getBaseActivity(), SelectEventTypeActivity.class, bundleSpeaker);
                //Navigator.getInstance().navigateToActivityWithData(getBaseActivity(), SelectSpeakerSportsActivity.class, bundleSpeaker);
                break;
        }
    }

}
