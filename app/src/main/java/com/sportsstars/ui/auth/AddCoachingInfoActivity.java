package com.sportsstars.ui.auth;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import com.edmodo.rangebar.RangeBar;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityCoachingInfoBinding;
import com.sportsstars.model.CoachInfo;
import com.sportsstars.model.SportDetail;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.auth.CoachCreateProfileStep1Response;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;

import java.util.Locale;

import retrofit2.Call;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AddCoachingInfoActivity extends BaseActivity implements View.OnClickListener, Alert.OnClickListener {

    private static final int MAX_RANGE_AGE = 100;
    private static final int MIN_RANGE_AGE = 8;
    private static final int TICK_COUNT_AGE = MAX_RANGE_AGE - MIN_RANGE_AGE + 1;

    private boolean isHandsOn;
    private CoachInfo mCoachInfo;
    private boolean isInspirational;
    private UserProfileModel mProfileModel;
    private ActivityCoachingInfoBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_coaching_info);
        setUpHeader();
    }

    private void setUpHeader() {
        setUpHeader(mBinding.header, getString(R.string.txt_ci_ttl), getString(R.string.txt_ci_sub));
        try {
            mCoachInfo = new CoachInfo();
            mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
            if(mProfileModel != null) {
                if(mProfileModel.getSportDetail() == null) {
                    mProfileModel.setSportDetail(new SportDetail());
                }
                copyData();
                mBinding.setCoachInf(mProfileModel);
                initView();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        mBinding.rangeBarAge.setTickCount(TICK_COUNT_AGE);
        mBinding.rangeBarAge.setTickHeight(0);
        mBinding.rangeBarAge.setBarWeight(10);
        mBinding.rangeBarAge.setConnectingLineColor(ContextCompat.getColor(this, R.color.green_gender));
        mBinding.rangeBarAge.setBarColor(ContextCompat.getColor(this, R.color.divider_color));
        mBinding.rangeBarAge.setThumbImageNormal(R.drawable.ic_oval_shadow);
        mBinding.rangeBarAge.setThumbImagePressed(R.drawable.ic_oval_shadow);

//        mBinding.rangeBarAge.setThumbIndices(0, TICK_COUNT_AGE - 1);

        int min = Integer.parseInt(mProfileModel.getAgeGroupFrom());
        int max = Integer.parseInt(mProfileModel.getAgeGroupTo());

        int left = min - MIN_RANGE_AGE;
        int right = max - MIN_RANGE_AGE - 1;

        mBinding.rangeBarAge.setThumbIndices(left, right);

        mProfileModel.setAgeGroupFrom(String.format(Locale.getDefault(), "%d YEARS", min));
        mProfileModel.setAgeGroupTo(String.format(Locale.getDefault(), "%d YEARS", max));
        setCallback();
    }

    private void setCallback() {
        mBinding.rangeBarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int min, int max) {
                if(min > max) {
                    int temp = min;
                    min = max;
                    max = temp;
                }

//                if (min < 0) min = 0;
//                if (max > (TICK_COUNT_AGE)) max = TICK_COUNT_AGE;

                if (min < 0) {
                    min = 0;
                    mBinding.rangeBarAge.setThumbIndices(min, max);
                }

                if (max > (TICK_COUNT_AGE - 1)) {
                    max = TICK_COUNT_AGE - 1;
                    mBinding.rangeBarAge.setThumbIndices(min, max);
                }

                int minAge = min + MIN_RANGE_AGE;
                int maxAge = max + MIN_RANGE_AGE;

                mBinding.tvAgeMin.setText(String.format(Locale.getDefault(), "%d YEARS", minAge));
                mBinding.tvAgeMax.setText(String.format(Locale.getDefault(), "%d YEARS", maxAge));

//                mBinding.tvAgeMin.setText(String.format(Locale.getDefault(), "%d YEARS", min < 1 ? 1 : min + 1));
//                mBinding.tvAgeMax.setText(String.format(Locale.getDefault(), "%d YEARS", max > 100 ? 100 : max + 1));
            }
        });
        mBinding.swSgp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    mBinding.etSgpGst.setText("");
                mBinding.sgParent.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        mBinding.swLgp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    mBinding.etLgpGst.setText("");
                mBinding.lgParent.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        mBinding.btnNext.setOnClickListener(this);
        mBinding.tvOooGst.setOnClickListener(this);
        mBinding.etSgpGst.setOnClickListener(this);
        mBinding.etLgpGst.setOnClickListener(this);
        mBinding.relHandsOn.setOnClickListener(this);
        mBinding.relInspirational.setOnClickListener(this);
        mBinding.tvHandsOnInfo.setOnClickListener(this);
        mBinding.tvInspirationalInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mBinding.btnNext) {
            String errorMsg = validate();
            if (errorMsg != null) {
                showSnackbarFromTop(this, errorMsg);
                return;
            }
            createProfile();
        } else if (view == mBinding.relHandsOn) {
            isHandsOn = !isHandsOn;
            handsOn();
        } else if (view == mBinding.relInspirational) {
            isInspirational = !isInspirational;
            inspirational();
        } else if (view == mBinding.tvOooGst) {
            Alert.createYesNoEditAlert(AddCoachingInfoActivity.this, getString(R.string.txt_oto_lbl), getString(R.string.txt_gst_hnt), getString(R.string.txt_gst_et_hnt), getOneOnOne(), getString(R.string.txt_ok), AddCoachingInfoActivity.this, Constants.SESSIONS.OOO).show();
        } else if (view == mBinding.etSgpGst) {
            Alert.createYesNoEditAlert(AddCoachingInfoActivity.this, getString(R.string.txt_sgp_lbl), getString(R.string.txt_gst_hnt), getString(R.string.txt_gst_et_hnt), getSmallOne(), getString(R.string.txt_ok), AddCoachingInfoActivity.this, Constants.SESSIONS.SGP).show();
        } else if (view == mBinding.etLgpGst) {
            Alert.createYesNoEditAlert(AddCoachingInfoActivity.this, getString(R.string.txt_lgp_lbl), getString(R.string.txt_gst_hnt), getString(R.string.txt_gst_et_hnt), getLargeOne(), getString(R.string.txt_ok), AddCoachingInfoActivity.this, Constants.SESSIONS.LGP).show();
        } else if (view == mBinding.tvHandsOnInfo) {
            showHandsOnInfoDialog();
//            if(isHandsOn) {
//                showHandsOnInfoDialog();
//            }
        } else if (view == mBinding.tvInspirationalInfo) {
            showInspirationalInfoDialog();
//            if(isInspirational) {
//                showInspirationalInfoDialog();
//            }
        }
    }

    private void showHandsOnInfoDialog() {
        String title = getResources().getString(R.string.txt_sst_opt1);
        String message = getResources().getString(R.string.hands_on_info);
        Alert.showInfoDialog(AddCoachingInfoActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void showInspirationalInfoDialog() {
        String title = getResources().getString(R.string.txt_sst_opt2);
        String message = getResources().getString(R.string.inspirational_info);
        Alert.showInfoDialog(AddCoachingInfoActivity.this,
                title,
                message,
                getResources().getString(R.string.okay),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {

                    }
                });
    }

    private void inspirational() {
        if (isInspirational) {
            mBinding.relInspirational.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
            mBinding.tvInspirational.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            mBinding.tvInspirationalInfo.setTextColor(ContextCompat.getColor(this,R.color.green_gender));
            mBinding.tvInspirationalInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
            return;
        }
        mBinding.relInspirational.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinding.tvInspirational.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        mBinding.tvInspirationalInfo.setTextColor(ContextCompat.getColor(this,R.color.black_25));
        mBinding.tvInspirationalInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);
    }

    private void handsOn() {
        if (isHandsOn) {
            mBinding.relHandsOn.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_border_green_bg_grey));
            mBinding.tvHandOn.setTextColor(ContextCompat.getColor(this, R.color.green_gender));

            mBinding.tvHandsOnInfo.setTextColor(ContextCompat.getColor(this,R.color.green_gender));
            mBinding.tvHandsOnInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_selected, 0, 0, 0);
            return;
        }
        mBinding.relHandsOn.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_rounded_grey));
        mBinding.tvHandOn.setTextColor(ContextCompat.getColor(this, R.color.black_25));

        mBinding.tvHandsOnInfo.setTextColor(ContextCompat.getColor(this,R.color.black_25));
        mBinding.tvHandsOnInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_unselected, 0, 0, 0);
    }

    @Override
    public void onPositive(DialogInterface dialog, String data, int type) {
        if(data!=null) {


            String price = data != null && (data.indexOf('$') != -1 && data.length() == 1) ? "" : data + getString(R.string.txt_ph);
            try {
                Double.parseDouble(data.substring(data.indexOf('$') + 1));
            } catch (Exception e) {
                data = "$";
                price = "";
            }
            switch (type) {
                case Constants.SESSIONS.OOO:
//                mBinding.tvOooGst.setText(price);
//                mBinding.swSgp.setChecked(false);
//                mBinding.swLgp.setChecked(false);

                    if (data.equalsIgnoreCase("$")) {
                        showSnackbarFromTop(this, getString(R.string.txt_err_fee));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) < 1) {
                        showSnackbarFromTop(this, getString(R.string.txt_ooo_err_fee_min));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) > 5000) {
                        showSnackbarFromTop(this, getString(R.string.txt_ooo_err_fee_max));
                        return;
                    } else if (StringUtils.isNullOrEmpty(data) || (data.indexOf('$') != -1 && data.length() == 1)) {
                        showSnackbarFromTop(this, getString(R.string.txt_err_fee));
                        //mBinding.etLgpGst.setText("");
                        return;
                    }
//                else if (!StringUtils.isNullOrEmpty(getSmallOne())) {
//                    double sgp = Double.parseDouble(getSmallOne());
//                    double ooo = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(sgp, ooo) <= 0) {
//                        showSnackbarFromTop(this, getString(R.string.txt_err_one_on_one_greater_than_small_group));
//                        return;
//                    }
//                    //mBinding.etLgpGst.setText(price);
//                }
//                else if (!StringUtils.isNullOrEmpty(getLargeOne())) {
//                    double lgp = Double.parseDouble(getLargeOne());
//                    double ooo = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(lgp, ooo) <= 0) {
//                        showSnackbarFromTop(this, getString(R.string.txt_err_one_on_one_greater_than_large_group));
//                        return;
//                    }
//                    //mBinding.etLgpGst.setText(price);
//                }

                    mBinding.tvOooGst.setText(price);
//                mBinding.swSgp.setChecked(false);
//                mBinding.swLgp.setChecked(false);
                    mBinding.swSgp.setChecked(StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice()) ? false : true);
                    mBinding.swLgp.setChecked(StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice()) ? false : true);
                    break;
                case Constants.SESSIONS.SGP:
                    if (data.equalsIgnoreCase("$")) {
                        showSnackbarFromTop(this, getString(R.string.txt_err_sgf));
                        return;
                    } else if (StringUtils.isNullOrEmpty(getOneOnOne())) {
                        mBinding.etSgpGst.setText("");
                        showSnackbarFromTop(this, getString(R.string.txt_err_fee));
                        return;
                    } else if (StringUtils.isNullOrEmpty(data) || (data.indexOf('$') != -1 && data.length() == 1)) {
                        mBinding.etSgpGst.setText("");
                        mBinding.swLgp.setChecked(false);
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) < 1) {
                        showSnackbarFromTop(this, getString(R.string.txt_sgp_err_fee_min));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) > 5000) {
                        showSnackbarFromTop(this, getString(R.string.txt_sgp_err_fee_max));
                        return;
                    } else {
//                    double sgp = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(sgp, Double.parseDouble(getOneOnOne())) <= 0) {
//                        showSnackbarFromTop(this, getString(R.string.txt_err_so_ph));
//                        return;
//                    } else if (!StringUtils.isNullOrEmpty(getLargeOne())) {
//                        double lgp = Double.parseDouble(getLargeOne());
//                        if (Double.compare(sgp, lgp) >= 0) {
//                            showSnackbarFromTop(this, getString(R.string.txt_err_sl_ph));
//                            return;
//                        }
//                    }
                        mBinding.etSgpGst.setText(price);
                    }
                    break;
                case Constants.SESSIONS.LGP:
                    if (data.equalsIgnoreCase("$")) {
                        showSnackbarFromTop(this, getString(R.string.txt_err_lgf));
                        return;
                    } else if (StringUtils.isNullOrEmpty(getOneOnOne())) {
                        mBinding.etLgpGst.setText("");
                        showSnackbarFromTop(this, getString(R.string.txt_err_fee));
                        return;
                    } else if (StringUtils.isNullOrEmpty(data) || (data.indexOf('$') != -1 && data.length() == 1)) {
                        mBinding.etLgpGst.setText("");
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) < 1) {
                        showSnackbarFromTop(this, getString(R.string.txt_lgp_err_fee_min));
                        return;
                    } else if (Double.parseDouble(data.substring(data.indexOf('$') + 1)) > 5000) {
                        showSnackbarFromTop(this, getString(R.string.txt_lgp_err_fee_max));
                        return;
                    } else if (StringUtils.isNullOrEmpty(getSmallOne())) {
//                    double ooo = Double.parseDouble(getOneOnOne());
//                    double lgp = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(lgp, ooo) <= 0) {
//                        showSnackbarFromTop(this, getString(R.string.txt_err_lo_ph));
//                        return;
//                    }
                        mBinding.etLgpGst.setText(price);
                    } else {
//                    double sgp = Double.parseDouble(getSmallOne());
//                    double lgp = Double.parseDouble(data.indexOf('$') != -1 ? data.substring(1) : data);
//                    if (Double.compare(lgp, sgp) <= 0) {
//                        showSnackbarFromTop(this, getString(R.string.txt_err_ls_ph));
//                        return;
//                    }
                        mBinding.etLgpGst.setText(price);
                    }

            }
            hideKeyboard();
        }
    }

    @Override
    public void onNegative(DialogInterface dialog) {

    }

    private String validate() {
        if (!isHandsOn && !isInspirational)
            return getString(R.string.txt_err_ssn);
        if (StringUtils.isNullOrEmpty(mProfileModel.getSpecialities()))
            return getString(R.string.txt_err_esp);
        if (StringUtils.isNullOrEmpty(mProfileModel.getOneOneBookingTypePrice()))
            return getString(R.string.txt_err_fee);
        if (mBinding.swSgp.isChecked() && StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice()))
            return getString(R.string.txt_err_sgf);
        if (mBinding.swLgp.isChecked() && StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice()))
            return getString(R.string.txt_err_lgf);
        return null;
    }

    private void createProfile() {
        processToShowDialog();
        mCoachInfo.setAgeGroupFrom(getAgeFrom());
        mProfileModel.setAgeGroupFrom(mCoachInfo.getAgeGroupFrom());
        mCoachInfo.setAgeGroupTo(getAgeTo());
        mProfileModel.setAgeGroupTo(mCoachInfo.getAgeGroupTo());
        mCoachInfo.setOneOneBookingTypePrice(getOneOnOne());
        mProfileModel.setOneOneBookingTypePrice(mCoachInfo.getOneOneBookingTypePrice());
        mCoachInfo.setSmallBookingTypePrice(getSmallOne());
        mProfileModel.setSmallBookingTypePrice(mCoachInfo.getSmallBookingTypePrice());
        mCoachInfo.setLargeBookingTypePrice(getLargeOne());
        mProfileModel.setLargeBookingTypePrice(mCoachInfo.getLargeBookingTypePrice());
        mCoachInfo.setSpecialities(mProfileModel.getSpecialities());
        mCoachInfo.setSessionType(isHandsOn && isInspirational ? Constants.SESSIONS.SESSION_BOTH : isHandsOn ? Constants.SESSIONS.SESSION_HANDS_ON : Constants.SESSIONS.SESSION_INSPIRATIONAL);
        mProfileModel.setSessionType(mCoachInfo.getSessionType());
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.coachCreateProfileStep2(mCoachInfo).enqueue(new BaseCallback<CoachCreateProfileStep1Response>(AddCoachingInfoActivity.this) {
            @Override
            public void onSuccess(CoachCreateProfileStep1Response response) {
                if (response != null && response.getStatus() == 1) {
                    //LogUtils.LOGD(TAG, "success: Name:" + response.getResult().getName());
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
                    Navigator.getInstance().navigateToActivityWithData(AddCoachingInfoActivity.this,
                            AddCertificatesActivity.class, bundle);
                } else {
                    if(response != null && response.getMessage() != null
                            && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(AddCoachingInfoActivity.this, response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<CoachCreateProfileStep1Response> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(TAG, "failed: Name:" + call.toString());
                hideProgressBar();
            }
        });
    }

    private String getOneOnOne() {
        if (!StringUtils.isNullOrEmpty(mProfileModel.getOneOneBookingTypePrice())) {
            if (mProfileModel.getOneOneBookingTypePrice().startsWith("$"))
                return mProfileModel.getOneOneBookingTypePrice().substring(1, mProfileModel.getOneOneBookingTypePrice().indexOf('/'));
            else
                return mProfileModel.getOneOneBookingTypePrice();
        }
        return "";
    }

    private String getSmallOne() {
        if (!StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice())) {
            if (mProfileModel.getSmallBookingTypePrice().startsWith("$"))
                return mProfileModel.getSmallBookingTypePrice().substring(1, mProfileModel.getSmallBookingTypePrice().indexOf('/'));
            else
                return mProfileModel.getSmallBookingTypePrice();
        }
        return "";
    }

    private String getLargeOne() {
        if (!StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice())) {
            if (mProfileModel.getLargeBookingTypePrice().startsWith("$"))
                return mProfileModel.getLargeBookingTypePrice().substring(1, mProfileModel.getLargeBookingTypePrice().indexOf('/'));
            else
                return mProfileModel.getLargeBookingTypePrice();
        }
        return "";
    }

    private String getAgeFrom() {
        if (mProfileModel.getAgeGroupFrom().contains(" ")) {
            return mProfileModel.getAgeGroupFrom().substring(0, mProfileModel.getAgeGroupFrom().indexOf(" "));
        }
        return "8";
    }

    private String getAgeTo() {
        if (mProfileModel.getAgeGroupTo().contains(" ")) {
            return mProfileModel.getAgeGroupTo().substring(0, mProfileModel.getAgeGroupTo().indexOf(" "));
        }
        return "100";
    }

    private void copyData() {
        if (mProfileModel != null) {
            if (mProfileModel.getAgeGroupFrom().contains(" "))
                mProfileModel.setAgeGroupFrom(getAgeFrom());
            if (mProfileModel.getAgeGroupTo().contains(" "))
                mProfileModel.setAgeGroupTo(getAgeTo());
            if (!StringUtils.isNullOrEmpty(mProfileModel.getOneOneBookingTypePrice()) && mProfileModel.getOneOneBookingTypePrice().indexOf('$') == -1)
                mProfileModel.setOneOneBookingTypePrice(getString(R.string.dollar) + mProfileModel.getOneOneBookingTypePrice() + getString(R.string.txt_ph));
            if (!StringUtils.isNullOrEmpty(mProfileModel.getSmallBookingTypePrice())) {
                mBinding.swSgp.setChecked(true);
                mBinding.sgParent.setVisibility(View.VISIBLE);
                if (mProfileModel.getSmallBookingTypePrice().indexOf('$') == -1)
                    mProfileModel.setSmallBookingTypePrice(getString(R.string.dollar) + mProfileModel.getSmallBookingTypePrice() + getString(R.string.txt_ph));
            }
            if (!StringUtils.isNullOrEmpty(mProfileModel.getLargeBookingTypePrice())) {
                mBinding.swLgp.setChecked(true);
                mBinding.lgParent.setVisibility(View.VISIBLE);
                if (mProfileModel.getLargeBookingTypePrice().indexOf('$') == -1)
                    mProfileModel.setLargeBookingTypePrice(getString(R.string.dollar) + mProfileModel.getLargeBookingTypePrice() + getString(R.string.txt_ph));
            }
            if (!StringUtils.isNullOrEmpty(mProfileModel.getSessionType())) {
                isHandsOn = mProfileModel.getSessionType().equals("1") || mProfileModel.getSessionType().equals("3");
                isInspirational = mProfileModel.getSessionType().equals("2") || mProfileModel.getSessionType().equals("3");
            }
            handsOn();
            inspirational();
        }
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }
}



