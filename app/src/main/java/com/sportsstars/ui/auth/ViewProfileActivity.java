package com.sportsstars.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;

import com.appster.chatlib.constants.AppConstants;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.sportsstars.R;
import com.sportsstars.chat.chat.ChatThreadActivity;
import com.sportsstars.databinding.ActivityViewProfileBinding;
import com.sportsstars.model.SlotTime;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.request.auth.ReportUserRequest;
import com.sportsstars.ui.auth.fragment.AboutFragment;
import com.sportsstars.ui.auth.fragment.AvailabilityFragment;
import com.sportsstars.ui.coach.ConfirmBookingActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.settings.ReportUserActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by atul on 09/04/18.
 * To inject activity reference.
 */

public class ViewProfileActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(ViewProfileActivity.class);
    private Fragment mFragment;
    private FragmentTransaction mTransaction;
    private UserProfileModel userProfileModel;
    private ActivityViewProfileBinding mBinding;

    private boolean isFromSessionDetails;
    private boolean isCoachSessionCompleted;
    private ReportUserRequest reportUserRequest;

    private boolean isAboutTabSelected = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_profile);
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                isFromSessionDetails = bundle.getBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, false);
                getDataFromSessionDetails(isFromSessionDetails);
                setViewVisibility(isFromSessionDetails);
            }

            PreferenceUtil.setIsFromSessionDetails(isFromSessionDetails);

            if(!isFromSessionDetails) {
                initBinding();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initBinding() {

        userProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if (userProfileModel != null) {
            mBinding.setCoachInfo(userProfileModel);
            setProfilePic();
            setFormattedPriceText();
            switchToAbout();
        }

        mBinding.mainPicBg.getLayoutParams().height = getRelativeHeaderHeightByPixel(isFromSessionDetails);

        mBinding.btnAbout.setOnClickListener(this);
        mBinding.btnAvailability.setOnClickListener(this);
        mBinding.btnShareMe.setOnClickListener(this);
        mBinding.btnBookMe.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.ivChat.setOnClickListener(this);
    }

    private void getDataFromSessionDetails(boolean isFromSessionDetails) {
        if(isFromSessionDetails) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                isCoachSessionCompleted = bundle.getBoolean(Constants.BundleKey.IS_COACH_SESSION_COMPLETED, false);
                reportUserRequest = bundle.getParcelable(Constants.BundleKey.REPORT_USER_DATA);
            }
        }
    }

    private void setViewVisibility(boolean isFromSessionDetails) {
        if(isFromSessionDetails) {
            mBinding.rlViewProfileHeader.setVisibility(View.VISIBLE);
            mBinding.ivBack.setVisibility(View.VISIBLE);
            mBinding.ivChat.setVisibility(View.VISIBLE);
            if(isCoachSessionCompleted) {
                mBinding.ivChat.setImageResource(R.drawable.ic_report_user);
            } else {
                mBinding.ivChat.setImageResource(R.drawable.ic_message);
            }
        } else {
            mBinding.rlViewProfileHeader.setVisibility(View.GONE);
            mBinding.ivBack.setVisibility(View.INVISIBLE);
            mBinding.ivChat.setVisibility(View.INVISIBLE);
        }
    }

//    private int getRelativeHeaderHeightByPixels(boolean isHeaderVisible) {
//        float newHeight = 0;
//        if(isHeaderVisible) {
//            newHeight = Utils.convertPixelsToDp(2100f, this);
//        } else {
//            newHeight = Utils.convertPixelsToDp(1850f, this);
//        }
//
//        Log.d("mylog", "LINE 130 - newHeight : " + newHeight);
//
//        return (int)newHeight;
//    }

    private int getRelativeHeaderHeightByPixel(boolean isHeaderVisible) {
        int newHeight = 0;
        Display display = getWindowManager().getDefaultDisplay();
        newHeight = display.getHeight();
        Log.d("mylog", "LINE 139 - oldHeight : " + newHeight);
        double originalValue = 0;
        if(isHeaderVisible) {
            originalValue = ((newHeight * 50d)/100);
        } else {
            originalValue = ((newHeight * 46d)/100);
        }
        newHeight = (int) Math.ceil(originalValue);
        Log.d("mylog", "LINE 147 - newHeight : " + newHeight);

        return newHeight;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAbout:
                isAboutTabSelected = true;
                switchToAbout();
                break;
            case R.id.btnAvailability:
                isAboutTabSelected = false;
                switchToAvailability();
                break;

//            case R.id.btnBookNow:
//                AvailabilityFragment fragment = (AvailabilityFragment) getSupportFragmentManager().findFragmentById(R.id.profileContainer);
//                String slotDate = fragment.getSlotDate();
//                SlotTime slot = fragment.getSelectedSlot();
//                if (slot != null) {
//                    Bundle b = new Bundle();
//                    b.putString(Constants.SELECTED_SLOT.SLOT_DATE, slotDate);
//                    b.putInt(Constants.SELECTED_SLOT.SLOT_TIME, slot.getSlotTime());
//                    b.putInt(Constants.SELECTED_SLOT.SLOT_ID, slot.getId());
//                    b.putString(Constants.BundleKey.SESSION,userProfileModel.getSessionType());
//                    b.putString(Constants.BundleKey.COACH_EMAIL, userProfileModel.getEmail());
//                    Navigator.getInstance().navigateToActivityWithData(this, ConfirmBookingActivity.class, b);
//                } else {
//                    showSnackbarFromTop(this, getString(R.string.pls_select_availability_slot));
//                }
//                break;

            case R.id.btnShareMe:
                shareCoachUrl();
                break;

            case R.id.btnBookMe:
                if(isAboutTabSelected) {
                    switchToAvailability();
                } else {
                    AvailabilityFragment fragment = (AvailabilityFragment) getSupportFragmentManager().findFragmentById(R.id.profileContainer);
                    String slotDate = fragment.getSlotDate();
                    SlotTime slot = fragment.getSelectedSlot();
                    if (slot != null) {
                        Bundle b = new Bundle();
                        b.putString(Constants.SELECTED_SLOT.SLOT_DATE, slotDate);
                        b.putInt(Constants.SELECTED_SLOT.SLOT_TIME, slot.getSlotTime());
                        b.putInt(Constants.SELECTED_SLOT.SLOT_ID, slot.getId());
                        b.putString(Constants.BundleKey.SESSION,userProfileModel.getSessionType());
                        b.putString(Constants.BundleKey.COACH_EMAIL, userProfileModel.getEmail());
                        Navigator.getInstance().navigateToActivityWithData(this, ConfirmBookingActivity.class, b);
                    } else {
                        showSnackbarFromTop(this, getString(R.string.pls_select_availability_slot));
                    }
                }
                break;

            case R.id.ivBack:
                finish();
                break;

            case R.id.ivChat:
                if(isCoachSessionCompleted) {
                    if(Utils.isNetworkAvailable()) {
                        launchReportUser(reportUserRequest);
                    } else {
                        showSnackbarFromTop(ViewProfileActivity.this, getResources().getString(R.string.no_internet));
                    }
                } else {
                    startChat();
                }
                break;
        }
    }

    private void launchReportUser(ReportUserRequest reportUserRequest) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKey.REPORT_USER_DATA, reportUserRequest);
        Navigator.getInstance().navigateToActivityWithData(ViewProfileActivity.this,
                ReportUserActivity.class, bundle);
    }

    private void startChat() {
        if(userProfileModel != null) {
            String userName = userProfileModel.getJid().split("@")[0];
            if (!TextUtils.isEmpty(userName)) {
                Intent intent = new Intent(this, ChatThreadActivity.class);
//                intent.putExtra(AppConstants.KEY_TO_USERNAME, userName);
                intent.putExtra(AppConstants.KEY_TO_USERNAME, userProfileModel.getName());
                intent.putExtra(AppConstants.KEY_TO_USER_PIC, userProfileModel.getProfileImage());
                intent.putExtra(AppConstants.KEY_TO_USER, userProfileModel.getJid());
                startActivity(intent);
            } else {
                showToast("Username of this profile not found to initiate chat.");
                //com.appster.chatlib.utils.Utils.showToast(this, "enter username");
            }
        }
    }

    private void shareCoachUrl() {

        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.DEEP_LINK
                        + userProfileModel.getUserId()
                        + "_" + Constants.USER_ROLE.COACH))
                .setDynamicLinkDomain(Constants.COACH_PROFILE_DEEP_LINKS.BASE_DOMAIN)
                // Open links with this app on Android
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder()
                                .setFallbackUrl(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.FALLBACK_URL))
                                .build())
                // Open links with this app on iOS
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder(Constants.COACH_PROFILE_DEEP_LINKS.IOS_PACKAGE_NAME)
                                .setFallbackUrl(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.FALLBACK_URL))
                                .setIpadFallbackUrl(Uri.parse(Constants.COACH_PROFILE_DEEP_LINKS.FALLBACK_URL))
                                .setAppStoreId(Constants.COACH_PROFILE_DEEP_LINKS.APP_STORE_ID)
                                .setIpadBundleId(Constants.COACH_PROFILE_DEEP_LINKS.IPAD_BUNDLE_ID)
                                .setMinimumVersion(Constants.COACH_PROFILE_DEEP_LINKS.IOS_MIN_VERSION)
                                .build())
                //.setNavigationInfoParameters(new DynamicLink.NavigationInfoParameters.Builder().setForcedRedirectEnabled(true).build())
                .buildDynamicLink();

        Uri dynamicLinkUri = dynamicLink.getUri();
        String finalDeepLinkUrl;
        try {
            finalDeepLinkUrl = URLDecoder.decode(dynamicLinkUri.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LogUtils.LOGD(TAG, "Failed during decoding URL");
            return;
        } catch (Exception e) {
            LogUtils.LOGD(TAG, "Failed during decoding URL");
            return;
        }

        String completeMessage = "";
        String shareMessage = "Hey! Checkout StarFinda app. \n";
        String playStoreUrl = " Android : https://play.google.com/store/apps/details?id=com.starfinda";
        String appStoreUrl = "\n  iOS : https://itunes.apple.com/us/app/starfinda/id1394386832?ls=1&mt=8";

        completeMessage = shareMessage + playStoreUrl + appStoreUrl;

        Intent shareCoachUrlIntent = new Intent(Intent.ACTION_SEND);
        shareCoachUrlIntent.setType("text/plain");
        shareCoachUrlIntent.putExtra(Intent.EXTRA_TEXT, finalDeepLinkUrl);

        //LogUtils.LOGD(TAG, "Deep Link shared: " + finalDeepLinkUrl);
        startActivity(Intent.createChooser(shareCoachUrlIntent, "Share via"));
    }

    private void switchToAbout() {
        isAboutTabSelected = true;
        mBinding.btnAvailability.setBackgroundColor(ContextCompat.getColor(this, R.color.white_color));
        mBinding.btnAbout.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));
        mTransaction = getSupportFragmentManager().beginTransaction();
        mFragment = AboutFragment.newInstance(userProfileModel);
        mTransaction.replace(R.id.profileContainer, mFragment);
        mTransaction.commit();
        mBinding.btnBookNow.setVisibility(View.GONE);
        mBinding.btnParent.setVisibility(View.VISIBLE);
    }

    private void switchToAvailability() {
        isAboutTabSelected = false;
        mBinding.btnAbout.setBackgroundColor(ContextCompat.getColor(this, R.color.white_color));
        mBinding.btnAvailability.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));
        mTransaction = getSupportFragmentManager().beginTransaction();
        mFragment = AvailabilityFragment.newInstance();
        mTransaction.replace(R.id.profileContainer, mFragment);
        mTransaction.commit();

//        mBinding.btnParent.setVisibility(View.GONE);
//        mBinding.btnBookNow.setVisibility(View.VISIBLE);
//        mBinding.btnBookNow.setOnClickListener(this);

        mBinding.btnBookNow.setVisibility(View.GONE);
        mBinding.btnParent.setVisibility(View.VISIBLE);
    }

    private void setProfilePic() {
        if (userProfileModel.getSportDetail() != null)
            Picasso.with(ViewProfileActivity.this)
                    .load(userProfileModel.getSportDetail().getIcon()).placeholder(R.drawable.ic_cricket_green).
                    error(R.drawable.ic_cricket_green).into(mBinding.ivSportIcon);
        if (StringUtils.isNullOrEmpty(userProfileModel.getProfileImage()))
            return;
        Picasso.with(ViewProfileActivity.this)
                .load(userProfileModel.getProfileImage()).placeholder(R.drawable.ic_user_circle).
                error(R.drawable.ic_user_circle).into(mBinding.ivProfilePic,
                new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        mBinding.ivBgHeader.setImageDrawable(mBinding.ivProfilePic.getDrawable());
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    private void setFormattedPriceText() {

        int smallTextSize = getResources().getDimensionPixelSize(R.dimen.text_size_10);
        int largeTextSize = getResources().getDimensionPixelSize(R.dimen.text_size_24);

        if (!StringUtils.isNullOrEmpty(userProfileModel.getOneOneBookingTypePrice())) {
            mBinding.tvOneOnOne.setText(Utils.getFormattedString(smallTextSize,
                    largeTextSize, userProfileModel.getOneOneBookingTypePrice()));
        }
        if (!StringUtils.isNullOrEmptyOrInt(userProfileModel.getSmallBookingTypePrice())) {
            mBinding.tvSbp.setText(Utils.getFormattedString(smallTextSize,
                    largeTextSize, userProfileModel.getSmallBookingTypePrice()));
        } else
            mBinding.tvSbp.setText(getString(R.string.txt_na));
        if (!StringUtils.isNullOrEmptyOrInt(userProfileModel.getLargeBookingTypePrice())) {
            mBinding.tvLbp.setText(Utils.getFormattedString(smallTextSize,
                    largeTextSize, userProfileModel.getLargeBookingTypePrice()));
        } else
            mBinding.tvLbp.setText(getString(R.string.txt_na));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isFromSessionDetails) {
            initBinding();
        }
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }

    public int getCoachId() {
        int coachId = 0;
        if(userProfileModel != null) {
            coachId = userProfileModel.getUserId();
        }
        return coachId;
    }

}
