package com.sportsstars.ui.auth.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemMediaLayoutBinding;
import com.sportsstars.model.AdditionalMedia;
import com.squareup.picasso.Picasso;

/**
 * Created by atul on 16/04/18.
 * To inject activity reference.
 */

public class MediaViewHolder extends RecyclerView.ViewHolder {

    private ItemMediaLayoutBinding binding;

    public MediaViewHolder(ItemMediaLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bindData(AdditionalMedia mediaLink,MediaClickCallback mediaClickCallback,int position) {
            Picasso.with(binding.ivMedia.getContext())
                    .load(mediaLink.getThumbImageUrl()).placeholder(R.drawable.bg_coach).
                    error(R.drawable.bg_coach).into(binding.ivMedia);
            if(mediaLink.getType()==7)
            {
                binding.ivPlayIcon.setVisibility(View.VISIBLE);
                binding.tvPlayMessage.setText(R.string.txt_play_video);
            }
            else
            {
                binding.ivPlayIcon.setVisibility(View.GONE);
                binding.tvPlayMessage.setText(R.string.txt_view_image);
            }

            binding.llPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaClickCallback.onMediaClicked(position);
                }
            });
    }

    public interface MediaClickCallback {
        void onMediaClicked(int position);
    }
}
