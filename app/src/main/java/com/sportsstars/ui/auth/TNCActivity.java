/*
 * Copyright © 2017 SeatXchange. All rights reserved.
 * Developed by Appster.
 *
 */

package com.sportsstars.ui.auth;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityTermsConditionsBinding;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Utils;

import java.util.ArrayList;


/**
 * Created by Appster on 29/03/17.
 * This is Terms & Condition Activity used to show Terms & Condition link in app using webview
 */

public class TNCActivity extends BaseActivity implements View.OnClickListener {

    private ActivityTermsConditionsBinding mBinder;
    //private boolean mIsPrivacyPolicy;

    private int selectedWebContent;
    private ArrayList<Integer> selectedSportIdsArray = new ArrayList<>();

    @Override
    public String getActivityName() {
        return TNCActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_terms_conditions);
        Utils.setTransparentTheme(this);
        mBinder.headerId.parent.setBackgroundColor(ContextCompat.getColor(this, R.color.black));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //mIsPrivacyPolicy = bundle.getBoolean(Constants.EXTRA_IS_PRIVACY_POLICY);
            selectedWebContent = bundle.getInt(Constants.EXTRA_WEB_CONTENT);
            if(getIntent().hasExtra(Constants.BundleKey.SPORTS_IDS)) {
                selectedSportIdsArray = bundle.getIntegerArrayList(Constants.BundleKey.SPORTS_IDS);
            }
        }
        setUpUi();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpUi() {

        mBinder.headerId.tvTitle.setText(getHeaderTitle(selectedWebContent));

//        if (mIsPrivacyPolicy) {
//            mBinder.headerId.tvTitle.setText(getString(R.string.privacy_policy_text));
//
//        } else {
//            mBinder.headerId.tvTitle.setText(getString(R.string.terms_conditions_text));
//        }

        WebSettings settings = mBinder.webview.getSettings();
        settings.setJavaScriptEnabled(true);
        mBinder.webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        mBinder.webview.loadUrl(getWebUrl(selectedWebContent));

//        if (mIsPrivacyPolicy) {
//            mBinder.webview.loadUrl(Constants.PRIVACY_POLICY_URL);
//
//        } else {
//
//            mBinder.webview.loadUrl(Constants.TNC_URL);
//        }
        mBinder.headerId.ivBack.setOnClickListener(this);
    }

    private String getHeaderTitle(int selectedWebContent) {
        String headerString = "";
        switch (selectedWebContent) {
            case Constants.SELECTED_WEB_CONTENT.TERMS_AND_CONDITIONS:
                headerString = getResources().getString(R.string.terms_conditions_text);
            break;
            case Constants.SELECTED_WEB_CONTENT.PRIVACY_POLICY:
                headerString =  getResources().getString(R.string.privacy_policy_text);
            break;
            case Constants.SELECTED_WEB_CONTENT.TRAINING_MANUAL:
                headerString =  getResources().getString(R.string.training_manual);
            break;
            case Constants.SELECTED_WEB_CONTENT.TRAINING_EQUIPMENT:
                headerString =  getResources().getString(R.string.training_equipment);
                break;
            default:
                headerString = "";
            break;
        }
        return headerString;
    }

    private String getWebUrl(int selectedWebContent) {
        String webUrlStr = "";
        switch (selectedWebContent) {
            case Constants.SELECTED_WEB_CONTENT.TERMS_AND_CONDITIONS:
                webUrlStr = Constants.TNC_URL;
            break;
            case Constants.SELECTED_WEB_CONTENT.PRIVACY_POLICY:
                webUrlStr = Constants.PRIVACY_POLICY_URL;
            break;
            case Constants.SELECTED_WEB_CONTENT.TRAINING_MANUAL:
                // Demo URL - http://54.252.198.116/api/v1/trainingManual?sportId[0]=1&sportId[1]=2
                if(selectedSportIdsArray != null && selectedSportIdsArray.size() > 0) {
                    webUrlStr = Constants.TRAINING_MANUAL_URL+generateExtendedUrl(selectedSportIdsArray);
                    Log.d("training_log", "complete web url : " + webUrlStr);
                }
            break;
            case Constants.SELECTED_WEB_CONTENT.TRAINING_EQUIPMENT:
                // Demo URL - http://54.252.198.116/api/v1/trainingEquipment?sportId[0]=1&sportId[1]=2
                if(selectedSportIdsArray != null && selectedSportIdsArray.size() > 0) {
                    webUrlStr = Constants.TRAINING_EQUIPMENT_URL+generateExtendedUrl(selectedSportIdsArray);
                    Log.d("training_log", "complete web url : " + webUrlStr);
                }
                break;
            default:
                webUrlStr = "";
            break;
        }
        return webUrlStr;
    }

    private String generateExtendedUrl(ArrayList<Integer> selectedSportIdsArray) {
        String extendedUrlStr = "?";
        if(selectedSportIdsArray != null && selectedSportIdsArray.size() > 0) {
            for(int i = 0 ; i < selectedSportIdsArray.size(); i++) {
                extendedUrlStr = extendedUrlStr + "sportId[" + i + "]=" + selectedSportIdsArray.get(i) + "&";
            }
        }
        extendedUrlStr = extendedUrlStr.substring(0, extendedUrlStr.length() - 1);
        Log.d("training_log", "extended url : " + extendedUrlStr);

        return extendedUrlStr;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

}
