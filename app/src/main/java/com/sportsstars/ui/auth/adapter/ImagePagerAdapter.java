package com.sportsstars.ui.auth.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemMediaLayoutBinding;
import com.sportsstars.model.AdditionalMedia;
import com.sportsstars.ui.auth.vh.MediaViewHolder;

import java.util.List;

/**
 * Created by atul on 16/04/18.
 * To inject activity reference.
 */

public class ImagePagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MediaViewHolder.MediaClickCallback {

    private final MediaClickCallBack mediaClickCallBack;
    private List<AdditionalMedia> mMediaLinks;

    public ImagePagerAdapter(List<AdditionalMedia> mediaLinks, MediaClickCallBack mediaClickCallBack) {
        mMediaLinks = mediaLinks;
        this.mediaClickCallBack = mediaClickCallBack;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MediaViewHolder((ItemMediaLayoutBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_media_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MediaViewHolder) {
            ((MediaViewHolder) holder).bindData(mMediaLinks.get(holder.getAdapterPosition()),this,position);
        }
    }

    @Override
    public int getItemCount() {
        return mMediaLinks.size();
    }

    @Override
    public void onMediaClicked(int position) {
        mediaClickCallBack.onMediaClicked(position);
    }

    public interface MediaClickCallBack {
        void onMediaClicked(int position);
    }

}
