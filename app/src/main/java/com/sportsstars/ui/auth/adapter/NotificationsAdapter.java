package com.sportsstars.ui.auth.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sportsstars.R;
import com.sportsstars.databinding.ItemNotificationsBinding;
import com.sportsstars.interfaces.OnNotificationClickListener;
import com.sportsstars.network.response.notifications.NotificationData;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyHolder> {

    private final OnNotificationClickListener onOtherNotificationClickListener;
    private ArrayList<NotificationData> mNotificationList;
    private Context context;
    private ItemNotificationsBinding mBinding;
    private ImageLoader imageLoader;
    private DisplayImageOptions op;

    public NotificationsAdapter(ArrayList<NotificationData> mNotificationList, Context context, OnNotificationClickListener onOtherNotificationClickListener) {
        this.mNotificationList = mNotificationList;
        this.onOtherNotificationClickListener = onOtherNotificationClickListener;
        this.context = context;

        op = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.user_avatar)
                .showImageForEmptyUri(R.drawable.user_avatar)
                .showImageOnFail(R.drawable.user_avatar)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new RoundedBitmapDisplayer(20))
                .build();
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public NotificationsAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_notifications, parent, false);

        return new NotificationsAdapter.MyHolder(mBinding.getRoot());
    }


    @Override
    public void onBindViewHolder(final NotificationsAdapter.MyHolder holder,int position) {

        String previousNotificationTime = null;
        String notificationTime = Utils.getNotificationTime(context,
                DateFormatUtils.getDateInLongFromUTC(mNotificationList.get(position).getCreatedAt()));
        if(position>0)
        {
            previousNotificationTime = Utils.getNotificationTime(context,
                    DateFormatUtils.getDateInLongFromUTC(mNotificationList.get(position-1).getCreatedAt()));
        }

        if (position == 0) {
            holder.tvDateHeader.setVisibility(View.VISIBLE);
            holder.viewSeparator.setVisibility(View.VISIBLE);
            if (notificationTime.equalsIgnoreCase(context.getString(R.string.yesterday)) ||
                    notificationTime.contains(context.getString(R.string.hrs)) ||
                            notificationTime.contains(context.getString(R.string.min)) ||
                                    notificationTime.contains(context.getString(R.string.sec)) ||
                                            notificationTime.equalsIgnoreCase(context.getString(R.string.just_now)))
            {
                holder.tvDateHeader.setText(context.getString(R.string.earlier));
            }
            else
            {
                holder.tvDateHeader.setText(notificationTime);
            }
        } else {
            if (previousNotificationTime != null) {
                if (previousNotificationTime.equalsIgnoreCase(notificationTime)) {
                    holder.tvDateHeader.setVisibility(View.GONE);
                    holder.viewSeparator.setVisibility(View.GONE);
                } else {
                    if (notificationTime.equalsIgnoreCase(context.getString(R.string.yesterday)) ||
                            notificationTime.contains(context.getString(R.string.hrs)) ||
                            notificationTime.contains(context.getString(R.string.min)) ||
                            notificationTime.contains(context.getString(R.string.sec)) ||
                            notificationTime.equalsIgnoreCase(context.getString(R.string.just_now))) {
                        if (previousNotificationTime.equalsIgnoreCase(context.getString(R.string.yesterday)) ||
                                previousNotificationTime.contains(context.getString(R.string.hrs)) ||
                                previousNotificationTime.contains(context.getString(R.string.min)) ||
                                previousNotificationTime.contains(context.getString(R.string.sec)) ||
                                previousNotificationTime.equalsIgnoreCase(context.getString(R.string.just_now))) {
                            holder.tvDateHeader.setVisibility(View.GONE);
                            holder.viewSeparator.setVisibility(View.GONE);
                        } else {
                            holder.tvDateHeader.setVisibility(View.VISIBLE);
                            holder.viewSeparator.setVisibility(View.VISIBLE);
                            holder.tvDateHeader.setText(context.getString(R.string.earlier));
                        }
                    } else {
                        holder.tvDateHeader.setVisibility(View.VISIBLE);
                        holder.viewSeparator.setVisibility(View.VISIBLE);
                        holder.tvDateHeader.setText(notificationTime);
                    }
                }
            }
        }

        holder.tvNotificationTime.setText(notificationTime);

        //Setting Unread/Read Color
        holder.llNotificationDataLayout.setBackgroundColor(ContextCompat.getColor(context, (mNotificationList.get(position).getIsRead() == 1) ? R.color.white_color : R.color.unread_notification_color));

        holder.tvNotificationText.setText(mNotificationList.get(position).getNotificationType().getDescription() + "");

//        if(mNotificationList != null && mNotificationList.size() > 0 && mNotificationList.get(position).getFromUser() != null) {
//            if (StringUtils.isNullOrEmpty(mNotificationList.get(position).getFromUser().getProfileImage())) {
//                holder.profile_pic.setImageResource(R.drawable.user_avatar);
//            } else {
//                Picasso.with(context)
//                        .load(mNotificationList.get(position).getFromUser().getProfileImage()).into(holder.profile_pic);
//            }
//        } else {
//            holder.profile_pic.setImageResource(R.drawable.user_avatar);
//        }

        holder.profile_pic.setImageResource(R.drawable.user_avatar);

        if(mNotificationList != null
                && mNotificationList.size() > 0
                && mNotificationList.get(position).getFromUser() != null) {
            if (!TextUtils.isEmpty(mNotificationList.get(position).getFromUser().getProfileImage())) {
                //Picasso.with(mActivity).invalidate(coach.getProfileImage());

//                Picasso.with(context)
//                        .load(mNotificationList.get(position).getFromUser().getProfileImage())
//                        .placeholder(R.drawable.user_avatar)
//                        .error(R.drawable.user_avatar)
//                        //.memoryPolicy(MemoryPolicy.NO_CACHE)
//                        //.networkPolicy(NetworkPolicy.NO_CACHE)
//                        .into((holder.profile_pic), new Callback() {
//                            @Override
//                            public void onSuccess() {
//
//                            }
//
//                            @Override
//                            public void onError() {
//                                holder.profile_pic.setImageResource(R.drawable.user_avatar);
//                            }
//                        });

                imageLoader.displayImage(mNotificationList.get(position).getFromUser().getProfileImage(), holder.profile_pic, op, new SimpleImageLoadingListener() {

                    @Override
                    public void onLoadingStarted(String url, View view) {
                        Log.d("image_log", "Coach list - onLoadingStarted - "
                                + " position : " + position
                                + " notification id : " + mNotificationList.get(position).getId()
                                + " notification from user name   : " + mNotificationList.get(position).getFromUser().getName()
                                + " notification image url : " + url
                        );
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        Log.d("image_log", "Coach list - onLoadingComplete - "
                                + " position : " + position
                                + " notification id : " + mNotificationList.get(position).getId()
                                + " notification from user name   : " + mNotificationList.get(position).getFromUser().getName()
                                + " notification image url : " + imageUri
                        );
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        super.onLoadingFailed(imageUri, view, failReason);
                        Log.d("image_log", "Coach list - onLoadingFailed - "
                                + " position : " + position
                                + " notification id : " + mNotificationList.get(position).getId()
                                + " notification from user name   : " + mNotificationList.get(position).getFromUser().getName()
                                + " notification image url : " + imageUri
                                + " failReason : " + failReason.getCause().toString()
                        );
                        holder.profile_pic.setImageResource(R.drawable.ic_user_circle);
                    }
                });

            } else {
                //Picasso.with(mActivity).invalidate(coach.getProfileImage());
                holder.profile_pic.setImageResource(R.drawable.user_avatar);
            }
        } else {
            holder.profile_pic.setImageResource(R.drawable.user_avatar);
        }

        holder.llNotificationDataLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mNotificationList != null
                        && mNotificationList.size() > 0
                        && mNotificationList.get(holder.getAdapterPosition()) != null) {
                    onOtherNotificationClickListener.onNotificationClicked(mNotificationList.get(holder.getAdapterPosition()));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        private View viewSeparator;
        TextView tvDateHeader, tvNotificationText, tvNotificationTime;
        CircleImageView profile_pic;
        LinearLayout llNotificationDataLayout;

        public MyHolder(View view) {
            super(view);
            tvDateHeader = mBinding.tvDateHeader;
            viewSeparator = mBinding.viewSeparator;
            tvNotificationText = mBinding.tvNotificationText;
            tvNotificationTime = mBinding.tvNotificationTime;
            profile_pic = mBinding.ivNotificationImage;
            llNotificationDataLayout = mBinding.llNotificationDataLayout;
        }
    }
}
