package com.sportsstars.ui.auth.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemFixureListBinding;
import com.sportsstars.interfaces.FixtureItemClickListener;
import com.sportsstars.network.response.auth.FixtureData;
import com.sportsstars.util.DateFormatUtils;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by abhaykant on 02/05/18.
 */


    public class FixtureListAdapter extends RecyclerView.Adapter<FixtureListAdapter.FixtureListViewHolder> {

        private static final String TAG = LogUtils.makeLogTag(FixtureListAdapter.class);

        private final LayoutInflater mInflator;
        private final Activity mActivity;
        private ArrayList<FixtureData> mFixtureList;
        private FixtureItemClickListener mFixtureItemClickListener;

        public void setSessionList(ArrayList<FixtureData> arrayList) {
            this.mFixtureList = arrayList;
            notifyDataSetChanged();
        }

        public FixtureListAdapter(Activity mActivity, ArrayList<FixtureData> sessionList,
                                  FixtureItemClickListener mFixtureItemClickListener) {
            this.mActivity = mActivity;
            this.mFixtureList = sessionList;
            this.mFixtureItemClickListener = mFixtureItemClickListener;
            mInflator = LayoutInflater.from(mActivity);
        }

        @Override
        public FixtureListAdapter.FixtureListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            ItemFixureListBinding searchListBinding = DataBindingUtil.inflate(mInflator, R.layout.item_fixure_list, parent, false);
            return new FixtureListAdapter.FixtureListViewHolder(searchListBinding);
        }

        @Override
        public void onBindViewHolder(final FixtureListAdapter.FixtureListViewHolder holder, final int position) {
            LogUtils.LOGD(TAG, "FixtureList Count>" + mFixtureList.size());

            if (mFixtureList != null && mFixtureList.size() > 0) {
                final FixtureData fixtureData = mFixtureList.get(position);
                int weekCount=position+1;
                holder.fixtureItemBinding.tvWeek.setText(mActivity.getString(R.string.week)+weekCount);

                String fixtureDateTimeLocal = DateFormatUtils.convertUtcToLocal(fixtureData.getFixtureDateTime(),
                        DateFormatUtils.SERVER_DATE_FORMAT_TIME, DateFormatUtils.FIXTURE_DATE_FORMAT_TIME);

                String dtStart = "";
                String time = "";
                String[] fixtureDateTimeArr = fixtureDateTimeLocal.split(" ");
                if(fixtureDateTimeArr != null && fixtureDateTimeArr.length > 1) {
                    dtStart = fixtureDateTimeArr[0];
                    time = fixtureDateTimeArr[1];
                    Log.d("fixture_log", "dtStart : " + dtStart
                        + " time : " + time);
                }

                //String dtStart = fixtureData.getPlayDate();
                SimpleDateFormat format = new SimpleDateFormat(DateFormatUtils.SERVER_DATE_FORMAT, Locale.getDefault());
                try {
                    Date date = format.parse(dtStart);
                   LogUtils.LOGD("fixture_log","date object : " + date);

                    String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
                    String day          = (String) DateFormat.format("dd",   date); // 20
                    //String monthString  = (String) DateFormat.format("MMM",  date); // Jun
                    String monthString  = (String) DateFormat.format("MMMM",  date); // June
                    String monthNumber  = (String) DateFormat.format("MM",   date); // 06
                    String year         = (String) DateFormat.format("yyyy", date); // 2013

                    holder.fixtureItemBinding.tvDate.setText(dayOfTheWeek+" "
                            +day+Utils.getDateOfTheMonthSuffix(Integer.parseInt(day)).toLowerCase()
                            + " "+monthString+" "+year);

                } catch (ParseException e) {
                   LogUtils.LOGE(TAG,e.getMessage());
                }

                //holder.fixtureItemBinding.tvDate.setText(fixtureData.getPlayDate());

                //String time=fixtureData.getPlayTime();
                int hours = 0;
                String min = "";
                if(!StringUtils.isNullOrEmpty(time) && time.contains(":")){
                    String [] timeArr = time.split(":");
                    hours = Integer.parseInt(timeArr[0]);
                    min = timeArr[1];
                }

                holder.fixtureItemBinding.tvTime.setText(Utils.getReadableTimeUI(hours,min));

                holder.fixtureItemBinding.rlRootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mFixtureItemClickListener != null && fixtureData != null) {
                            mFixtureItemClickListener.onFixtureItemClick(fixtureData, holder.getAdapterPosition());
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mFixtureList != null ? mFixtureList.size() : 0;
        }

        public void clearItems() {
            if (mFixtureList != null && mFixtureList.size() > 0) {
                mFixtureList.clear();
                notifyDataSetChanged();
            }
        }


        class FixtureListViewHolder extends RecyclerView.ViewHolder  {

            private ItemFixureListBinding fixtureItemBinding;

            private FixtureListViewHolder(ItemFixureListBinding itemFixureListBinding) {
                super(itemFixureListBinding.getRoot());
                this.fixtureItemBinding = itemFixureListBinding;
            }

            public ItemFixureListBinding getBinding() {
                return fixtureItemBinding;
            }

           /* @Override
            public void onClick(View view) {
                LogUtils.LOGD(TAG, "clicked");
                mClickListener.onItemClick(fixtureItemBinding, mSessionList.get(getAdapterPosition()), getAdapterPosition());
            }*/
        }

    }


