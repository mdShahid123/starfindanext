package com.sportsstars.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddFacilityBinding;
import com.sportsstars.model.Facility;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.response.FacilityResponse;
import com.sportsstars.network.response.auth.CoachProfileStatusResponse;
import com.sportsstars.network.response.auth.ProfileStatusResult;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.AddFacilityAdapter;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.payment.AddBankAccountActivity;
import com.sportsstars.ui.search.SearchCoachLocationActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DividerItemDecoration;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AddFacilityActivity extends BaseActivity implements AddFacilityAdapter.ISuburbCallback {

    private static final int REQUEST_CODE_LOCATION = 101;
    private static final int REQUEST_ADD_FACILITY_NAME = 131;

    private Facility mFacility;
    private List<Facility> mFacilities;
    private UserProfileModel mProfileModel;
    private AddFacilityAdapter mFacilityAdapter;
    private ActivityAddFacilityBinding mBinding;
    private ProfileStatusResult result;
    private boolean isFromSettings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_facility);

        getIntentData();
        setUpHeader();
    }

    private void setUpHeader() {
        Utils.setTransparentTheme(this);
        if(!isFromSettings) {
            setUpHeader(mBinding.header, getString(R.string.txt_af_ttl), getString(R.string.txt_af_sub));
            initView();
        } else {
            setUpHeader(mBinding.header, getString(R.string.edit_facilities_title), "");
            initViewInEditMode();
        }


    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SETTINGS)) {
                isFromSettings = bundle.getBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
            }
        }
    }

    private void initView() {
        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if(mProfileModel != null && mProfileModel.getCoachLocation() != null) {
            mFacilities =  mProfileModel.getCoachLocation();
        } else {
            mFacilities = new ArrayList<>();
        }
        mFacilityAdapter = new AddFacilityAdapter(this, mFacilities);
        mBinding.rvFacilities.setHasFixedSize(true);
        mBinding.rvFacilities.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvFacilities.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider_line));
        mBinding.rvFacilities.setAdapter(mFacilityAdapter);
        mBinding.tvAfSubLbl.setText(getResources().getString(R.string.txt_paf));
        mBinding.btnNext.setText(getResources().getString(R.string.confirm));
        setCallback();
    }

    private void initViewInEditMode() {
        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
        if(mProfileModel != null && mProfileModel.getCoachLocation() != null) {
            mFacilities =  mProfileModel.getCoachLocation();
        } else {
            mFacilities = new ArrayList<>();
        }
        mFacilityAdapter = new AddFacilityAdapter(this, mFacilities);
        mBinding.rvFacilities.setHasFixedSize(true);
        mBinding.rvFacilities.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvFacilities.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider_line));
        mBinding.rvFacilities.setAdapter(mFacilityAdapter);
        mBinding.tvAfSubLbl.setText(getResources().getString(R.string.edit_facilities_subtitle));
        mBinding.btnNext.setText(getResources().getString(R.string.save));
        setCallbackInEditMode();
    }

    private void setCallback() {
        mBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFacilities.size() == 1) {
                    showSnackbarFromTop(AddFacilityActivity.this, getString(R.string.txt_err_fac));
                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
                Navigator.getInstance().navigateToActivityWithData(AddFacilityActivity.this,
                        AddBankAccountActivity.class, bundle);

//                getCoachProfileStatus();

//                Navigator.getInstance().navigateToActivity(AddFacilityActivity.this, HomeActivity.class);
            }
        });
    }

    private void setCallbackInEditMode() {
        mBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFacilities.size() == 1) {
                    showSnackbarFromTop(AddFacilityActivity.this, getString(R.string.txt_err_fac));
                    return;
                }
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(isFromSettings) {
            if (mFacilities.size() == 1) {
                showSnackbarFromTop(AddFacilityActivity.this, getString(R.string.txt_err_fac));
                return;
            }
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void getCoachProfileStatus() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(AddFacilityActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    result = response.getResult();
                    if(result != null) {
                        if (result.getIsProfileCompleted() == 1 && result.getIsVerified() == 1) {
                            Navigator.getInstance().navigateToActivityWithClearStack(AddFacilityActivity.this, HomeActivity.class);
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result.getIsProfileCompleted() == 1 && result.getIsVerified() == 0) {
                            showToast(getResources().getString(R.string.coach_not_verified));
                            Navigator.getInstance().navigateToActivityWithClearStack(AddFacilityActivity.this, WelcomeActivity.class);
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result.getIsProfileCompleted() == 0 && result.getIsVerified() == 0) {
                            showToast(getResources().getString(R.string.coach_not_verified));
                            Navigator.getInstance().navigateToActivityWithClearStack(AddFacilityActivity.this, WelcomeActivity.class);
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddFacilityActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(AddFacilityActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onAddSuburb() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EXTRA_TITLE, mFacilities.get(mFacilities.size() - 1).getFacilityName());
        Navigator.getInstance().navigateToActivityForResultWithData(this, SearchCoachLocationActivity.class, bundle, REQUEST_CODE_LOCATION);
    }

    @Override
    public void onRemoveSuburb(int position) {
        if (position < 0)
            return;

        if(position == 0 && mFacilities != null && mFacilities.size() > 1 && mFacilities.size() < 3 ) {
            Alert.showOkCancelDialog(AddFacilityActivity.this,
                    getResources().getString(R.string.remove_facility_alert_title),
                    getResources().getString(R.string.remove_facility_alert_msg),
                    getResources().getString(R.string.cancel),
                    getResources().getString(R.string.okay),
                    new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            removeFacility(position);
                        }
                    },
                    new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {

                        }
                    }
            );
        } else {
            removeFacility(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_LOCATION:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        mFacility = new Facility();
                        mFacility.setFacilityAddress(data.getStringExtra(Constants.EXTRA_ADDRESS));
                        mFacility.setFacilityName(data.getStringExtra(Constants.EXTRA_ADDRESS));
                        mFacility.setFacilityLat(data.getDoubleExtra(Constants.EXTRA_LATITUDE, 0.0));
                        mFacility.setFacilityLong(data.getDoubleExtra(Constants.EXTRA_LONGITUDE, 0.0));
                        mFacility.setFacilityCity(data.getStringExtra(Constants.EXTRA_CITY_NAME));
                        mFacility.setFacilityZipcode(data.getStringExtra(Constants.EXTRA_POSTAL_CODE));


                        // Call for AddFacilityNameActivity from here startActivityForResultWithData

                        // onActivityResult for case REQUEST_CODE_ADD_FACILITY_NAME
                        // get facility name from data and set using mFacility.setFacilityName
                        // then call addFacility after that
                        launchAddFacilityNameScreen();


                        //addFacility();
                    }
                }
                break;
            case REQUEST_ADD_FACILITY_NAME:
                if(resultCode == RESULT_OK) {
                    if(data != null && mFacility != null) {
                        mFacility.setFacilityName(data.getStringExtra(Constants.EXTRA_FACILITY_NAME));
                        addFacility();
                    }
                }
                break;
        }
    }

    private void launchAddFacilityNameScreen() {
        Navigator.getInstance().navigateToActivityForResult(AddFacilityActivity.this, AddFacilityNameActivity.class, REQUEST_ADD_FACILITY_NAME);
    }

    private void addFacility() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.addFacilities(mFacility).enqueue(new BaseCallback<FacilityResponse>(AddFacilityActivity.this) {
            @Override
            public void onSuccess(FacilityResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mFacilities.add(mFacilities.size() - 1, response.getResult());
                        mFacilityAdapter.updateFacility();
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddFacilityActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<FacilityResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(TAG, "failed: Name:" + baseResponse.getMessage());
                hideProgressBar();
            }
        });
    }

    private void removeFacility(final int position) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        LogUtils.LOGI("Facility Size", mFacilities.size() + "");
        webServices.removeFacilities(mFacilities.get(position)).enqueue(new BaseCallback<BaseResponse>(AddFacilityActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    mFacilityAdapter.removeSuburb(position);
                } else {
                    if(response != null && response.getMessage() != null
                            && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(AddFacilityActivity.this, response.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGD(TAG, "failed: Name:" + baseResponse.getMessage());
                hideProgressBar();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }


}
