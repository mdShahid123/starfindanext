package com.sportsstars.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivitySelectSportsBinding;
import com.sportsstars.databinding.ItemFlowSportsBinding;
import com.sportsstars.model.Sports;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.GetSportsRequest;
import com.sportsstars.network.response.auth.SportsListResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;

import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by abhaykant on 11/01/18.
 */

public class SelectSportActivity extends BaseActivity implements View.OnClickListener {
    private ActivitySelectSportsBinding mBinder;
    private ArrayList<Sports> mSportsList;

    @Override
    public String getActivityName() {
        return SelectSportActivity.class.getSimpleName();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_select_sports);

        mSportsList = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mSportsList = bundle.getParcelableArrayList(Constants.EXTRA_SPORTS_LIST);
            if (mSportsList != null) {
                for (Sports items : mSportsList) {
                    addTitleToLayout(items);
                }
            }else {
                getSportsList();
            }
        }

        init();
    }

    private void init() {
        mBinder.buttonSave.setOnClickListener(this);
        mBinder.ivClose.setOnClickListener(this);

        mBinder.etSportsSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ArrayList<Sports> filteredList = new ArrayList<>();
                if (mSportsList != null && charSequence.length() > 0) {

                    for (Sports sports : mSportsList) {
                        if (sports.getName().toUpperCase().startsWith(charSequence.toString().toUpperCase())) {
                            filteredList.add(sports);
                            //mSportsList.remove(sports);
                        }
                    }
                }

                if (filteredList != null && filteredList.size() > 0) {
                    mBinder.flowLayoutSports.removeAllViews();

                    for (Sports mSelectedSports : filteredList) {
                        addTitleToLayout(mSelectedSports);
                    }
                } else {
                    if (charSequence.length() == 0) {
                        mBinder.flowLayoutSports.removeAllViews();
                        for (Sports items : mSportsList) {
                            addTitleToLayout(items);
                        }
                    } else {
                        mBinder.flowLayoutSports.removeAllViews();

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void addTitleToLayout(final Sports mSports) {
        String text = mSports.getName();
        int pos = 0;
        for (int i = 0; i < mSportsList.size(); i++) {
            if (mSports.getName().equals(mSportsList.get(i).getName())) {
                pos = i;
                break;
            }
        }


        final ItemFlowSportsBinding flowBinding = DataBindingUtil.bind(LayoutInflater.from(mBinder.flowLayoutSports.getContext())
                .inflate(R.layout.item_flow_sports, mBinder.flowLayoutSports, false));

        flowBinding.flowChild.setText(text);

        final Drawable imgMinus = getResources().getDrawable(R.drawable.ic_minus_green);
        final Drawable imgPlus = getResources().getDrawable(R.drawable.ic_plus_green);

        final int finalPos = pos;
        if (mSportsList.get(finalPos).isSelected()) {
            flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgMinus, null);

        } else {
            flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPlus, null);

        }

        flowBinding.flowChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSportsList.get(finalPos).isSelected()) {
                    mSportsList.get(finalPos).setSelected(false);
                    flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPlus, null);
                } else {
                    mSportsList.get(finalPos).setSelected(true);
                    flowBinding.flowChild.setCompoundDrawablesWithIntrinsicBounds(null, null, imgMinus, null);
                }
            }
        });

        mBinder.flowLayoutSports.addView(flowBinding.getRoot());

    }


    private void getSportsList() {
        processToShowDialog();

        GetSportsRequest getSportsRequest = new GetSportsRequest();
        getSportsRequest.setName("");
        getSportsRequest.setPage(1);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.sportsList(getSportsRequest).enqueue(new BaseCallback<SportsListResponse>
                (SelectSportActivity.this) {
            @Override
            public void onSuccess(SportsListResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().getSportsList() != null) {
                        mSportsList = response.getResult().getSportsList();

                        for (Sports items : mSportsList) {
                            addTitleToLayout(items);
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SportsListResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch ((view.getId())) {
            case R.id.button_save:
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra(Constants.EXTRA_SPORTS_LIST, mSportsList);
                setResult(RESULT_OK, intent);
                finish();//finishing activity

                break;

            case R.id.iv_close:
                finish();
                break;
        }
    }
}
