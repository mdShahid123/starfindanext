package com.sportsstars.ui.auth.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.chat.chat.ImagePreviewActivity;
import com.sportsstars.databinding.FragmentAboutBinding;
import com.sportsstars.interfaces.RecyclerPageChangeListener;
import com.sportsstars.model.Certificate;
import com.sportsstars.model.Facility;
import com.sportsstars.model.Team;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.ui.auth.adapter.ImageAdapter;
import com.sportsstars.ui.auth.adapter.LinePagerIndicatorDecoration;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.common.PlayerActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;

import java.util.List;

/**
 * Created by atul on 11/04/18.
 * To inject activity reference.
 */

public class AboutFragment extends BaseFragment implements ImageAdapter.MediaClickCallBack, RecyclerPageChangeListener {

    private UserProfileModel profileModel;
    private FragmentAboutBinding mBinding;

    public static AboutFragment newInstance(UserProfileModel profileModel) {
        AboutFragment aboutFragment = new AboutFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKey.DATA, profileModel);
        aboutFragment.setArguments(bundle);
        return aboutFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, null, false);
        initView();
        return mBinding.getRoot();
    }

    private void initView() {
        if (getArguments() != null) {
            profileModel = getArguments().getParcelable(Constants.BundleKey.DATA);
            mBinding.setCoachInfo(profileModel);
            setCerView();
            setTeamView();
            setLocView();
            setHonourView();
            setSessionView();
            setSpecialityView();
            setMediaView();
//            mBinding.youtubeParent.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(profileModel != null && !TextUtils.isEmpty(profileModel.getYoutubeLink())) {
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constants.BundleKey.DATA, profileModel.getYoutubeLink());
//                        Navigator.getInstance().navigateToActivityWithData(getActivity(), YouTubeMediaActivity.class, bundle);
//                    } else {
//                        showSnackbarFromTop(getResources().getString(R.string.error_youtube));
//                    }
//                }
//            });
            String age = getString(R.string.txt_frm) + profileModel.getAgeGroupFrom() + getString(R.string.txt_yr) + getString(R.string.txt_to) + profileModel.getAgeGroupTo() + getString(R.string.txt_yr);
            ((TextView) mBinding.blAges.findViewById(R.id.tvSpecList)).setText(age);
        }
    }

//    private void setYoutubeView() {
//        String youtubeUrl = Utils.extractYoutubeThumbnail(profileModel.getYoutubeLink());
//        if (!StringUtils.isNullOrEmpty(youtubeUrl))
//            Picasso.with(getActivity())
//                    .load(youtubeUrl).placeholder(R.drawable.media_place_holder).
//                    error(R.drawable.media_place_holder).into(mBinding.ivThumbnail);
//    }


    private void setCerView() {
        try {
            List<Certificate> list = profileModel.getCertificateList();
            if(list != null) {
                if(list.size() == 0) {
                    mBinding.tvAccreditationTitle.setVisibility(View.GONE);
                    return;
                }
                for (Certificate certificate : list) {
                    if(certificate.getCertificateTitle().contains(Constants.PUBLIC_LIABILITY_INSURANCE) || certificate.getCertificateTitle().contains(Constants.POLICE_CHECK)) {
                        View view = getLayoutInflater().inflate(R.layout.item_bullet_with_icon_tv, null);
                        ((ImageView) view.findViewById(R.id.ivBulletIcon)).setImageResource(R.drawable.ic_verified_coach);
                        ((TextView) view.findViewById(R.id.tvSpecList)).setText(certificate.getCertificateTitle());
                        mBinding.llCertificate.addView(view);
                    } else if(certificate.getCertificateTitle().contains(Constants.WORK_WITH_CHILDREN_CERTIFICATE) || certificate.getCertificateTitle().contains("Work With Children Certificate")) {
                        View view = getLayoutInflater().inflate(R.layout.item_bullet_with_icon_tv, null);
                        ((ImageView) view.findViewById(R.id.ivBulletIcon)).setImageResource(R.drawable.ic_children);
                        ((TextView) view.findViewById(R.id.tvSpecList)).setText(certificate.getCertificateTitle());
                        mBinding.llCertificate.addView(view);
                    } else {
                        View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
                        ((TextView) view.findViewById(R.id.tvSpecList)).setText(certificate.getCertificateTitle());
                        mBinding.llCertificate.addView(view);
                    }

//            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
//            ((TextView) view.findViewById(R.id.tvSpecList)).setText(StringUtils.toCamelCase(certificate.getCertificateTitle()));
//            mBinding.llCertificate.addView(view);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setHonourView() {
        if (!StringUtils.isNullOrEmpty(profileModel.getAccolades())) {
            String[] items = profileModel.getAccolades().split("\\s*,\\s*");
            drawView(items, mBinding.llHonour);
        }
    }

    private void setTeamView() {
        List<Team> list = profileModel.getTeamPlayedFor();
        for (Team team : list) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
            ((TextView) view.findViewById(R.id.tvSpecList)).setText(team.getName());
            mBinding.llTeams.addView(view);
        }
    }

    private void setLocView() {
        mBinding.llLocations.removeAllViews();
        List<Facility> list = profileModel.getCoachLocation();
        for (Facility facility : list) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_with_loc_icon_tv, null);
            ((TextView) view.findViewById(R.id.tvFacilityName)).setText(facility.getFacilityName());
            ((ImageView) view.findViewById(R.id.ivBulletIcon)).setImageResource(R.drawable.ic_location_pin_small);
            ((TextView) view.findViewById(R.id.tvFacilityAddress)).setText(facility.getFacilityAddress());
            mBinding.llLocations.addView(view);
        }
    }

    private void setSpecialityView() {
        if (!StringUtils.isNullOrEmpty(profileModel.getSpecialities())) {
            String[] items = profileModel.getSpecialities().split("\\s*,\\s*");
            drawView(items, mBinding.llEsp);
        }
    }

    private void setSessionView() {
        if (!StringUtils.isNullOrEmpty(profileModel.getSessionType())) {
            String[] items = profileModel.getSessionType().equals(Constants.SESSIONS.SESSION_HANDS_ON) ? new String[]{getString(R.string.txt_handsOn)} : profileModel.getSessionType().equals(Constants.SESSIONS.SESSION_INSPIRATIONAL) ? new String[]{getString(R.string.txt_inspirational)} : new String[]{getString(R.string.txt_handsOn), getString(R.string.txt_inspirational)};
            drawView(items, mBinding.llSession);
        }
    }

    private void drawView(String[] items, LinearLayout container) {
        for (String item : items) {
            View view = getLayoutInflater().inflate(R.layout.item_bullet_tv, null);
            ((TextView) view.findViewById(R.id.tvSpecList)).setText(item);
            container.addView(view);
        }
    }

    private void setMediaView() {

        if(profileModel.getMediaUrl().size() > 0) {
            mBinding.mediaContainer.setVisibility(View.VISIBLE);
            mBinding.tvPagesMedia.setVisibility(View.VISIBLE);
        } else {
            mBinding.mediaContainer.setVisibility(View.GONE);
            mBinding.tvPagesMedia.setVisibility(View.GONE);
        }

        ImageAdapter imageAdapter = new ImageAdapter(profileModel.getMediaUrl(),this);
        mBinding.mediaContainer.setHasFixedSize(true);
        mBinding.mediaContainer.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mBinding.mediaContainer.setAdapter(imageAdapter);

        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        mBinding.mediaContainer.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(mBinding.mediaContainer);

        mBinding.mediaContainer.addItemDecoration(new LinePagerIndicatorDecoration(this));

        if(profileModel.getMediaUrl().size() > 0) {
            mBinding.tvPagesMedia.setText(String.format("1/%d", profileModel.getMediaUrl().size()));
        }

    }

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }

    @Override
    public void onMediaClicked(int position) {
        if(profileModel.getMediaUrl().get(position).getType()==7)
        {
            Bundle bundlePlay = new Bundle();
            bundlePlay.putString(Constants.BundleKey.VIDEO_URL, profileModel.getMediaUrl().get(position).getImageUrl());
            Navigator.getInstance().navigateToActivityWithData(getBaseActivity(), PlayerActivity.class, bundlePlay);
        }
        else
        {
            Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
            intent.putExtra(ImagePreviewActivity.EXTRA_IMAGE_URL, profileModel.getMediaUrl().get(position).getImageUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onPageChanged(int position) {
        if(profileModel.getMediaUrl().size()>0)
        {
            mBinding.tvPagesMedia.setText(String.format("%d/%d", position + 1, profileModel.getMediaUrl().size()));
        }
    }
}
