package com.sportsstars.ui.auth;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.sportsstars.R;
import com.sportsstars.databinding.ActivityAddCertificateBinding;
import com.sportsstars.model.Certificate;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.UpdateCertificateRequest;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.response.auth.CertificateResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.adapter.AddCertificateAdapter;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.DividerItemDecoration;
import com.sportsstars.util.ImagePickerUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.StringUtils;
import com.sportsstars.util.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AddCertificatesActivity extends BaseActivity implements AddCertificateAdapter.IUploadCallback, ImagePickerUtils.OnImagePickerListener {

    private static final int UPLOAD_TYPE = 2;

    private int mPreviousDocId;
    private AddCertificateAdapter mAdapter;
    private UserProfileModel mProfileModel;
    private List<Certificate> mCertificateList;
    private ActivityAddCertificateBinding mBinding;
    private boolean isFromSettings;

    private ArrayList<Certificate> mCertificateTitleList;
    private int mCurrentTitlePosition = 0;

    public static int certId = 0;
    public static String newCertTitle = "";
    public static boolean isCertTitleChanged = false;

//    private SoftKeyboard softKeyboard;
//    private boolean isKeyboardOpen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_certificate);
        getIntentData();
        setUpHeader();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(getIntent().hasExtra(Constants.BundleKey.IS_FROM_SETTINGS)) {
                isFromSettings = bundle.getBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
            }
        }
    }

    private void setUpHeader() {
        if(!isFromSettings) {
            setUpHeader(mBinding.header, getString(R.string.txt_ac_ttl), getString(R.string.txt_ac_sub));
            mBinding.btnNext.setText(getResources().getString(R.string.next));
            if(Utils.isNetworkAvailable()) {
                updateCoachUserModel();
            } else {
                initView();
            }
        } else {
            setUpHeader(mBinding.header, getString(R.string.edit_certificate_title), "");
            mBinding.btnNext.setText(getResources().getString(R.string.save));
            initView();
        }
    }

    private void updateCoachUserModel() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileModel = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mProfileModel);
                        initView();
                    } else {
                        initView();
                    }
                } else {
                    initView();
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                initView();
                hideProgressBar();
            }
        });
    }

    private void updateCoachUserModelAndFinish() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileModel = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mProfileModel);
                        finish();
                    } else {
                        finish();
                    }
                } else {
                    finish();
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                finish();
            }
        });
    }

    private void updateCoachUserModelAndGoToAddFacility() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileModel = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mProfileModel);

                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
                        Navigator.getInstance().navigateToActivityWithData(AddCertificatesActivity.this, AddFacilityActivity.class, bundle);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
                        Navigator.getInstance().navigateToActivityWithData(AddCertificatesActivity.this, AddFacilityActivity.class, bundle);
                    }
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
                    Navigator.getInstance().navigateToActivityWithData(AddCertificatesActivity.this, AddFacilityActivity.class, bundle);
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                finish();
            }
        });
    }

    private void initView() {
        if(mCertificateList != null && mCertificateList.size() > 0) {
            mCertificateList.clear();
        }
        mCertificateList = new Certificate().getFixedCertificates();        // 5 objects including footer
        mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();    // 1 to many objects, but mostly 4 objects
        copyData();
        mAdapter = new AddCertificateAdapter(this, mCertificateList);
        mBinding.rvCertificates.setHasFixedSize(true);
        mBinding.rvCertificates.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvCertificates.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider));
        mBinding.rvCertificates.setAdapter(mAdapter);
        setCallback();
        //setKeyboardCallback();
    }

    private void setCallback() {
        mBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                // Commented older implementation
//                if(isFromSettings) {
//                    goBack();
//                } else {
//                    goToAddFacility();
//                }

                // Added new implementation to save all titles before going back or going to add facility screen
                saveAllCertificateTitleToServer();
            }
        });
    }

//    private void setKeyboardCallback() {
//        InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
//        softKeyboard = new SoftKeyboard(mBinding.parentRelLayout, im);
//
//        keyboardClose();
//
//        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {
//            @Override
//            public void onSoftKeyboardHide() {
//                Log.d("cert_log", " Keyboard close");
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        showToast("onSoftKeyboardHide called.");
//                        keyboardClose();
//                    }
//                });
//            }
//
//            @Override
//            public void onSoftKeyboardShow() {
//                Log.d("cert_log", " Keyboard open");
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        showToast("onSoftKeyboardShow called.");
//                        keyboardOpen();
//                    }
//                });
//            }
//        });
//    }

//    private void keyboardClose() {
//        isKeyboardOpen = false;
//        if(isCertTitleChanged) {
//            isCertTitleChanged = false;
//            certId = certId;
//            newCertTitle = newCertTitle;
//            Log.d("cert_log", "AddCertActivity - certId : " + certId + " newCertTitle : " + newCertTitle);
//
//            if(Utils.isNetworkAvailable()) {
//                if(certId == 0) {
//                    showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.upload_certificate_first));
//                    return;
//                }
//                if(newCertTitle == null || TextUtils.isEmpty(newCertTitle)) {
//                    showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.enter_title_certificate));
//                    return;
//                }
//                //updateCertificateTitleOnBack(certId, newCertTitle);
//            } else {
//                certId = 0;
//                newCertTitle = "";
//                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.no_internet));
//            }
//        }
//    }
//
//    private void keyboardOpen() {
//        isKeyboardOpen = true;
//        isCertTitleChanged = false;
//        certId = 0;
//        newCertTitle = "";
//    }

    private void saveAllCertificateTitleToServer() {
        try {
            if(Utils.isNetworkAvailable()) {
                if(mCertificateList != null && mCertificateList.size() > 0) {
                    mCertificateTitleList = new ArrayList<>();
                    for(int i = 0; i < mCertificateList.size(); i++) {
                        Certificate certificate = mCertificateList.get(i);
//                    if(certificate.getId() == 0) {
//                        showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.upload_certificate_first));
//                        return;
//                    }
//                    if(certificate.getCertificateTitle() == null || TextUtils.isEmpty(certificate.getCertificateTitle())) {
//                        showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.enter_title_certificate));
//                        return;
//                    }

                        if(hasTitleChanged(certificate) && certificate.getId() != 0) {
                            mCertificateTitleList.add(certificate);
                        }

                    }

                    if(mCertificateTitleList != null && mCertificateTitleList.size() > 0) {
                        int certId = mCertificateTitleList.get(0).getId();
                        String certTitle = mCertificateTitleList.get(0).getCertificateTitle();
                        Log.d("cert_log", "before updateCertificateTitleOnSave - certId : " + certId
                            + " certTitle : " + certTitle);
                        if(certId == 0) {
                            showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.upload_certificate_first));
                            return;
                        }
                        if(certTitle == null || TextUtils.isEmpty(certTitle)) {
                            showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.enter_title_certificate));
                            return;
                        }
                        updateCertificateTitleOnSave(certId, certTitle);
                    } else {
                        if(isFromSettings) {
                            goBack();
                        } else {
                            goToAddFacility();
                        }
                    }
                }

            } else {
                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.no_internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean hasTitleChanged(Certificate certificate) {
        boolean hasTitleChanged = false;
        Log.d("cert_log", "Certificate title : " + certificate.getCertificateTitle());
        if(certificate.getCertificateTitle().equalsIgnoreCase(Constants.PUBLIC_LIABILITY_INSURANCE)
                || certificate.getCertificateTitle().equalsIgnoreCase(Constants.WORK_WITH_CHILDREN_CERTIFICATE)
                || certificate.getCertificateTitle().equalsIgnoreCase(Constants.CERTIFICATE_1_ACCREDITATION)
                || certificate.getCertificateTitle().equalsIgnoreCase(Constants.CERTIFICATE_2_ACCREDITATION)
                || certificate.getCertificateTitle().equalsIgnoreCase(Constants.ADDITIONAL_CERTIFICATE)
                || certificate.getCertificateTitle().equalsIgnoreCase(Constants.ADD_ANOTHER_CERTIFICATE)) {
            hasTitleChanged = false;
        } else {
            hasTitleChanged = true;
        }
        return hasTitleChanged;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(isFromSettings) {
            goBack();
        } else {
            super.onBackPressed();
        }

//        if (isKeyboardOpen) {
//            isKeyboardOpen = false;
//            if (softKeyboard != null) {
//                softKeyboard.closeSoftKeyboard();
//            }
//            Utils.hideSoftKeyboard(AddCertificatesActivity.this);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    //releaseResourcesAndGoBack();
//                }
//            }, 500);
//        } else {
//            releaseResourcesAndGoBack();
//        }

//        if(isCertTitleChanged) {
//            isCertTitleChanged = false;
//            certId = certId;
//            newCertTitle = newCertTitle;
//            Log.d("cert_log", "AddCertActivity - certId : " + certId + " newCertTitle : " + newCertTitle);
//
//            if(Utils.isNetworkAvailable()) {
//                if(certId == 0) {
//                    showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.upload_certificate_first));
//                    return;
//                }
//                if(newCertTitle == null || TextUtils.isEmpty(newCertTitle)) {
//                    showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.enter_title_certificate));
//                    return;
//                }
//                //updateCertificateTitleOnBack(certId, newCertTitle);
//            } else {
//                certId = 0;
//                newCertTitle = "";
//                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.no_internet));
//            }
//        } else {
//            if(isFromSettings) {
//                goBack();
//            } else {
//                super.onBackPressed();
//            }
//        }

    }

//    private void releaseResourcesAndGoBack() {
//        if (softKeyboard != null) {
//            softKeyboard.unRegisterSoftKeyboardCallback();
//        }
//        if(isFromSettings) {
//            goBack();
//        } else {
//            super.onBackPressed();
//        }
//        //finish();
//    }

    private void goBack() {
//        for (Certificate certificate : mCertificateList) {
//            if (certificate.getCertType() == Constants.CERTIFICATE.MANDATORY && StringUtils.isNullOrEmpty(certificate.getCertificateUrl())) {
//                showSnackbarFromTop(AddCertificatesActivity.this, getString(R.string.txt_err_doc) + certificate.getCertificateTitle());
//                return;
//            }
//        }
        if(Utils.isNetworkAvailable()) {
            updateCoachUserModelAndFinish();
        } else {
            finish();
        }
    }

    private void goToAddFacility() {
//        for (Certificate certificate : mCertificateList) {
//            if (certificate.getCertType() == Constants.CERTIFICATE.MANDATORY && StringUtils.isNullOrEmpty(certificate.getCertificateUrl())) {
//                showSnackbarFromTop(AddCertificatesActivity.this, getString(R.string.txt_err_doc) + certificate.getCertificateTitle());
//                return;
//            }
//        }

        if(Utils.isNetworkAvailable()) {
            updateCoachUserModelAndGoToAddFacility();
        } else {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, false);
            Navigator.getInstance().navigateToActivityWithData(AddCertificatesActivity.this, AddFacilityActivity.class, bundle);
        }

    }

    @Override
    public void onUploadSelected(int type, int id) {
        mPreviousDocId = id;
        ImagePickerUtils.add((this).getSupportFragmentManager(), this, type);
    }

    @Override
    public void onDelSelected(int id, int position) {
        removeDoc(id, position, false);
    }

    @Override
    public void onCertificateTitleChanged(int id, String title) {
        if(Utils.isNetworkAvailable()) {
            if(id == 0) {
                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.upload_certificate_first));
                return;
            }
            if(title == null || TextUtils.isEmpty(title)) {
                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.enter_title_certificate));
                return;
            }
            updateCertificateTitle(id, title);
        } else {
            showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void updateCertificateTitle(int id, String title) {
        processToShowDialog();
        UpdateCertificateRequest updateCertificateRequest = new UpdateCertificateRequest();
        updateCertificateRequest.setId(id);
        updateCertificateRequest.setCertificateTitle(title);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateCertificateTitle(updateCertificateRequest).enqueue(new BaseCallback<BaseResponse>(AddCertificatesActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                hideProgressBar();
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(Utils.isNetworkAvailable()) {
                            updateCoachUserModel();
                        } else {
                            initView();
                        }
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            //showToast(response.getMessage());
                            showSnackbarFromTop(AddCertificatesActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void updateCertificateTitleOnBack(int id, String title) {
        //showToast("update certificate title - id : " + id + " title : " + title);
        processToShowDialog();
        UpdateCertificateRequest updateCertificateRequest = new UpdateCertificateRequest();
        updateCertificateRequest.setId(id);
        updateCertificateRequest.setCertificateTitle(title);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateCertificateTitle(updateCertificateRequest).enqueue(new BaseCallback<BaseResponse>(AddCertificatesActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                hideProgressBar();
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if(Utils.isNetworkAvailable()) {
                            updateCoachUserModel();
                        } else {
                            initView();
                        }
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            //showToast(response.getMessage());
                            showSnackbarFromTop(AddCertificatesActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void updateCertificateTitleOnSave(int id, String title) {
        //showToast("update certificate title - id : " + id + " title : " + title);
        processToShowDialog();
        UpdateCertificateRequest updateCertificateRequest = new UpdateCertificateRequest();
        updateCertificateRequest.setId(id);
        updateCertificateRequest.setCertificateTitle(title);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.updateCertificateTitle(updateCertificateRequest).enqueue(new BaseCallback<BaseResponse>(AddCertificatesActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                hideProgressBar();
                if (response != null) {
                    if (response.getStatus() == 1) {
                        mCurrentTitlePosition++;
                        if(mCurrentTitlePosition < mCertificateTitleList.size()) {
                            int certId = mCertificateTitleList.get(mCurrentTitlePosition).getId();
                            String certTitle = mCertificateTitleList.get(mCurrentTitlePosition).getCertificateTitle();
                            Log.d("cert_log", "before recurring updateCertificateTitleOnSave - certId : " + certId
                                    + " certTitle : " + certTitle);
                            if(certId == 0) {
                                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.upload_certificate_first));
                                return;
                            }
                            if(certTitle == null || TextUtils.isEmpty(certTitle)) {
                                showSnackbarFromTop(AddCertificatesActivity.this, getResources().getString(R.string.enter_title_certificate));
                                return;
                            }
                            updateCertificateTitleOnSave(certId, certTitle);
                        } else {
                            if(isFromSettings) {
                                goBack();
                            } else {
                                goToAddFacility();
                            }
                        }
                    } else {
                        if(!TextUtils.isEmpty(response.getMessage())) {
                            //showToast(response.getMessage());
                            showSnackbarFromTop(AddCertificatesActivity.this, response.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (softKeyboard != null) {
//            softKeyboard.closeSoftKeyboard();
//            softKeyboard.unRegisterSoftKeyboardCallback();
//        }
        try {
            //hideSoftKeyboard();
            //hideKeyboard();
            Utils.hideSoftKeyboard(AddCertificatesActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void success(String name, String path, int index) {
//        LogUtils.LOGI("AddCertificatesActivity", "PATH: " + path +"--- INDEX : "+index);
        uploadImage(new File(path), index);
    }

    @Override
    public void fail(String message) {

    }

    private void uploadImage(final File file, final int index) {
        String title = mCertificateList.get(index).getCertificateTitle();
        String type;
        if(index==0 || index ==1)
        {
            type = Constants.CERTIFICATE_TYPE.OTHER_CERTIFICATE;
        }
        else if(index==2)
        {
            type = Constants.CERTIFICATE_TYPE.POLICE_CHECK;
        }
        else if(index==3)
        {
            type = Constants.CERTIFICATE_TYPE.WORK_WITH_CHILDREN;
        }
        else
        {
            type = Constants.CERTIFICATE_TYPE.OTHER_CERTIFICATE;
        }

        if (StringUtils.isNullOrEmpty(title)) {
            showSnackbarFromTop(this, getString(R.string.invalid_ttl));
            return;
        }
        RequestBody certificateTtl = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), title);
        showProgressBar();
        RequestBody fBody = null;
        if (file != null) {
            fBody = RequestBody.create(MediaType.parse("image/*"), file);
        }
        RequestBody rbType = RequestBody.create(MediaType.parse(Constants.MULTIPART_FORM_DATA), "" + UPLOAD_TYPE);
        RequestBody certificateType = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), "" + (type));
        RequestBody certificateIndex = RequestBody.create(MediaType.parse(Constants.MEDIA_TYPE_TEXT), "" + (index));

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.uploadDoc(certificateIndex,rbType, fBody, certificateType, certificateTtl).enqueue(new BaseCallback<CertificateResponse>(AddCertificatesActivity.this) {
            @Override
            public void onSuccess(CertificateResponse certificateResponse) {
                if (certificateResponse != null && certificateResponse.getStatus() == 1) {
                    if(certificateResponse.getResult() != null) {
                        mAdapter.setDocImg(index, file.getPath(), certificateResponse.getResult().getId());
                        showSnackbarFromTop(AddCertificatesActivity.this, mPreviousDocId == 0 ? certificateResponse.getMessage() : getString(R.string.txt_upd_suc));
                        addOrUpdateCertificate(index);
                        if (mPreviousDocId != 0)
                            removeDoc(mPreviousDocId, index, true);
                    }

                } else {
                    if(certificateResponse != null && certificateResponse.getMessage() != null
                            && !TextUtils.isEmpty(certificateResponse.getMessage())) {
                        showSnackbarFromTop(AddCertificatesActivity.this, certificateResponse.getMessage());
                    }
                }
            }

            @Override
            public void onFail(Call<CertificateResponse> call, BaseResponse baseResponse) {
                //LogUtils.LOGE("Error", call.toString());
                hideProgressBar();
            }
        });
    }

    private void removeDoc(int id, final int position, final boolean inBg) {
        if (!inBg)
            showProgressBar();

        AuthWebServices client = RequestController.createService(AuthWebServices.class, true);
        client.removeCertificate(id).enqueue(new BaseCallback<BaseResponse>(AddCertificatesActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (inBg) {
                        mPreviousDocId = 0;
                        return;
                    }
                    if(response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                        showSnackbarFromTop(AddCertificatesActivity.this, response.getMessage());
                    }
                    removeDocItem(position);
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(AddCertificatesActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void copyData() {
        if (mProfileModel != null
                && mProfileModel.getCertificateList() != null
                && mProfileModel.getCertificateList().size() > 0) {
            List<Certificate> certificateListServer = mProfileModel.getCertificateList();
//            for(Certificate certificate : certificateListServer) {
//                if(certificate.getCertificateType().equals(Constants.CERTIFICATE_TYPE.WORK_WITH_CHILDREN)) {
//                    certificate.setCertType(Constants.CERTIFICATE.MANDATORY);
//                } else {
//                    certificate.setCertType(Constants.CERTIFICATE.OPTIONAL);
//                }
//            }

            Log.d("certLog", "mProfileModel.getCertificateList() size : "
                    + mProfileModel.getCertificateList().size()
                    + " mCertificateList size : " + mCertificateList.size());

            for (int i = 0; i < certificateListServer.size(); i++) {
                Certificate certificateServer = certificateListServer.get(i);

                // NEW IMPLEMENTATION

                int position = certificateServer.getCertificateIndex();
                int localListSize = 0;

                if(mCertificateList != null && mCertificateList.size() > 0)  {
                    localListSize = mCertificateList.size();
                }

                // localListSize = 5 , mostly.

                if(position < (localListSize-1)) {

                    String certificateTitle = "";
                    if(certificateServer.getCertificateTitle() != null
                            && !TextUtils.isEmpty(certificateServer.getCertificateTitle())
                            && certificateServer.getCertificateTitle().equalsIgnoreCase("POLICE CHECK")) {
                        certificateTitle = Constants.PUBLIC_LIABILITY_INSURANCE;
                    } else {
                        certificateTitle = certificateServer.getCertificateTitle();
                    }

                    Certificate certificate = mCertificateList.get(position);
                    certificate.setId(certificateServer.getId());
                    certificate.setCertType(certificateServer.getCertType());
                    certificate.setCertificateUrl(certificateServer.getCertificateUrl());
                    certificate.setCertificateTitle(certificateTitle);
                    certificate.setCertificateType(certificateServer.getCertificateType());
                    certificate.setCertificateIndex(certificateServer.getCertificateIndex());

                } else {

                    Certificate certificate = new Certificate().addNewCertificate(mCertificateList.size() - 2);
                    certificate.setId(certificateServer.getId());
                    certificate.setCertType(certificateServer.getCertType());
                    certificate.setCertificateUrl(certificateServer.getCertificateUrl());
                    certificate.setCertificateTitle(certificateServer.getCertificateTitle());

                    mCertificateList.add(mCertificateList.size() - 1, certificate);
                }


                // OLDER IMPLEMENTATION

//                int itemIndex = mCertificateList.indexOf(new Certificate(certificateListServer.get(i).getCertificateType()));
//                if (itemIndex != -1) {
//                    Certificate certificate = mCertificateList.get(itemIndex);
//                    certificate.setId(certificateListServer.get(i).getId());
//                    certificate.setCertType(certificateListServer.get(i).getCertType());
//                    certificate.setCertificateUrl(certificateListServer.get(i).getCertificateUrl());
//                    certificate.setCertificateTitle(certificateListServer.get(i).getCertificateTitle());
//                    certificate.setCertificateType(certificateListServer.get(i).getCertificateType());
//                    certificate.setCertificateIndex(certificateListServer.get(i).getCertificateIndex());
//                    Log.d("certlog", "LINE 266 - AddCertAct - copyData - when  itemIndex != -1 - "
//                            + " index : " + i
//                            + " itemIndex : " + itemIndex
//                            + " cert id : " + certificate.getId()
//                            + " cert certType : " + certificate.getCertType()
//                            + " cert certUrl : " + certificate.getCertificateUrl()
//                            + " cert certTitle : " + certificate.getCertificateTitle()
//                            + " cert certType : " + certificate.getCertificateType()
//                            + " cert certIndex : " + certificate.getCertificateIndex()
//                    );
//                    continue;
//                } else {
//                    Log.d("certlog", "LINE 273 - AddCertAct - copyData - when  itemIndex == -1 - "
//                            + " index : " + i
//                            + " itemIndex : " + itemIndex);
//                }
//
//                Certificate certificate = new Certificate().addNewCertificate(mCertificateList.size() - 2);
//                certificate.setId(certificateListServer.get(i).getId());
//                certificate.setCertType(certificateListServer.get(i).getCertType());
//                certificate.setCertificateUrl(certificateListServer.get(i).getCertificateUrl());
//                certificate.setCertificateTitle(certificateListServer.get(i).getCertificateTitle());
//
//                Log.d("certlog", "LINE 284 - AddCertAct - copyData - when  itemIndex == -1 - "
//                        + " index : " + i
//                        + " itemIndex : " + itemIndex
//                        + " cert id : " + certificate.getId()
//                        + " cert certType : " + certificate.getCertType()
//                        + " cert certUrl : " + certificate.getCertificateUrl()
//                        + " cert certTitle : " + certificate.getCertificateTitle()
//                        + " cert certType : " + certificate.getCertificateType()
//                        + " cert certIndex : " + certificate.getCertificateIndex()
//                );
//
//                mCertificateList.add(mCertificateList.size() - 1, certificate);

            }
        }
    }

    private void addOrUpdateCertificate(int type) {
        List<Certificate> certificateList = mProfileModel.getCertificateList();
        int itemIndex = certificateList.indexOf(new Certificate(mCertificateList.get(type).getCertificateType()));
        if (itemIndex != -1) {
            Certificate certificate = certificateList.get(itemIndex);
            certificate.setId(mCertificateList.get(type).getId());
            certificate.setCertType(mCertificateList.get(type).getCertType());
            certificate.setCertificateUrl(mCertificateList.get(type).getCertificateUrl());
            certificate.setCertificateTitle(mCertificateList.get(type).getCertificateTitle());
            return;
        }
        Certificate certificate = new Certificate();
        certificate.setId(mCertificateList.get(type).getId());
        certificate.setCertType(mCertificateList.get(type).getCertType());
        certificate.setCertificateType(mCertificateList.get(type).getCertificateType());
        certificate.setCertificateUrl(mCertificateList.get(type).getCertificateUrl());
        certificate.setCertificateTitle(mCertificateList.get(type).getCertificateTitle());
        mProfileModel.getCertificateList().add(certificate);
    }

    private void removeDocItem(int pos) {
        List<Certificate> certificateList = mProfileModel.getCertificateList();
        int itemIndex = certificateList.indexOf(new Certificate(mCertificateList.get(pos).getCertificateType()));
        if (itemIndex != -1) {
            certificateList.remove(itemIndex);
            if (pos <= 3) {
                mAdapter.setDocImg(pos, "", 0);
                return;
            }
            mCertificateList.remove(pos);
            mAdapter.notifyItemRemoved(pos);
        }
    }

    @Override
    public String getActivityName() {
        return getClass().getSimpleName();
    }

}
