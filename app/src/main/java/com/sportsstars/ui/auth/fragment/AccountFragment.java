package com.sportsstars.ui.auth.fragment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentAccountBinding;
import com.sportsstars.model.LearnerProfile;
import com.sportsstars.model.ProfileLearner;
import com.sportsstars.model.UserProfileInstance;
import com.sportsstars.model.UserProfileModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.ViewUserRequest;
import com.sportsstars.network.response.auth.FixtureListResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.auth.AddCertificatesActivity;
import com.sportsstars.ui.auth.AddFacilityActivity;
import com.sportsstars.ui.auth.TNCActivity;
import com.sportsstars.ui.auth.WelcomeActivity;
import com.sportsstars.ui.coach.CoachViewProfileActivity;
import com.sportsstars.ui.coach.ManagePaymentActivity;
import com.sportsstars.ui.common.BaseFragment;
import com.sportsstars.ui.fixture.CreateFixtureListActivity;
import com.sportsstars.ui.learner.ViewProfileActivity;
import com.sportsstars.ui.payment.ManageCardListActivity;
import com.sportsstars.ui.settings.ChangePasswordActivity;
import com.sportsstars.ui.settings.SettingsActivity;
import com.sportsstars.ui.speaker.SpeakerViewProfileActivity;
import com.sportsstars.util.Constants;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by atul on 19/04/18.
 * To inject activity reference.
 */

public class AccountFragment extends BaseFragment implements View.OnClickListener {

    private Activity mActivity;
    private ProfileLearner mProfileLearner;
    private UserProfileModel mProfileCoach;
    private FragmentAccountBinding mBinding;
    private FixtureListResponse fixtureListResponse = null;
    private boolean isFromCertificate = false;

    public static AccountFragment newInstance() {
        AccountFragment accountFragment = new AccountFragment();
        Bundle bundle = new Bundle();
        accountFragment.setArguments(bundle);
        return accountFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false);
        initView();
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
    }

    private void initView() {
        isFromCertificate = false;
        mBinding.signUpTextView.setOnClickListener(this);
        mBinding.viewCertificatesTextView.setOnClickListener(this);
        mBinding.viewFacilitiesTextView.setOnClickListener(this);
        mBinding.trainingManualTextView.setOnClickListener(this);
        mBinding.trainingEquipmentTextView.setOnClickListener(this);
        mBinding.settingsTextView.setOnClickListener(this);
        mBinding.showProfile.setOnClickListener(this);
        updateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFromCertificate) {
            isFromCertificate = false;
            updateCoachUserModel();
        }
    }

    private void updateCoachUserModel() {
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileCoach = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mProfileCoach);
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateUI() {
        if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
            mBinding.signUpTextView.setText(getResources().getString(R.string.switch_as_learner_speaker));
            mBinding.trainingEquipmentTextView.setVisibility(View.VISIBLE);
            getCoachProfileStatus();
        } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
            mBinding.signUpTextView.setText(getResources().getString(R.string.switch_as_coach_speaker));
            mBinding.viewCertificatesTextView.setText(getResources().getString(R.string.view_fixtures));
            mBinding.viewFacilitiesTextView.setText(getResources().getString(R.string.change_password));
            mBinding.trainingManualTextView.setText(getResources().getString(R.string.manage_payment));
            mBinding.trainingEquipmentTextView.setVisibility(View.GONE);
            getLearnerProfile();
        } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
            mBinding.signUpTextView.setText(getResources().getString(R.string.switch_as_coach_learner));
            mBinding.viewCertificatesTextView.setText(getResources().getString(R.string.view_fixtures));
            mBinding.viewFacilitiesTextView.setText(getResources().getString(R.string.change_password));
            mBinding.trainingManualTextView.setText(getResources().getString(R.string.manage_payment));
            mBinding.trainingEquipmentTextView.setVisibility(View.GONE);
            mBinding.viewCertificatesTextView.setVisibility(View.GONE);
            getGuestSpeakerProfileStatus();
        }
    }

    private void getCoachProfileStatus() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.COACH);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileCoach = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mProfileCoach);
                        setUpView(response.getResult().getName(), response.getResult().getProfileImage(), response.getResult().getEmail());
                    }
                } else {
                    try {
                         if(response != null && response.getMessage() != null
                                 && !TextUtils.isEmpty(response.getMessage())) {
                             showSnackbarFromTop(response.getMessage());
                         }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getLearnerProfile() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.LEARNER);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewLearnerProfile(request).enqueue(new BaseCallback<LearnerProfile>(getBaseActivity()) {
            @Override
            public void onSuccess(LearnerProfile response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileLearner = response.getResult();
                        setUpView(response.getResult().getName(), response.getResult().getProfileImage(), response.getResult().getEmail());
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<LearnerProfile> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getGuestSpeakerProfileStatus() {
        processToShowDialog();
        ViewUserRequest request = new ViewUserRequest();
        request.setUserType(Constants.USER_ROLE.GUEST_SPEAKER);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfile(request).enqueue(new BaseCallback<GetProfileResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if(response.getResult() != null) {
                        mProfileCoach = response.getResult();
                        UserProfileInstance.getInstance().setUserProfileModel(mProfileCoach);
                        setUpView(response.getResult().getName(), response.getResult().getProfileImage(), response.getResult().getEmail());
                    }
                } else {
                    try {
                        if(response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setUpView(String name, String profileImage, String emailId) {
        mBinding.tvPrfName.setText(name);
        mBinding.tvPrfEmail.setText(emailId);
        if (profileImage != null && !TextUtils.isEmpty(profileImage)) {
            Picasso.with(getContext())
                    .load(profileImage)
                    .placeholder(R.drawable.ic_user_circle)
                    .error(R.drawable.ic_user_circle)
                    .into(mBinding.imageUser);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.showProfile:
                if(Utils.isNetworkAvailable()) {
                    if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                        //showSnackBar("Functionality coming Soon");
                        Navigator.getInstance().navigateToActivity(getActivity(), CoachViewProfileActivity.class);
                        return;
                    }

                    if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.BundleKey.DATA, mProfileLearner);
                        bundle.putBoolean(Constants.BundleKey.IS_FROM_SESSION_DETAILS, false);
                        Navigator.getInstance().navigateForResultWithBundle(getActivity(),
                                this, com.sportsstars.ui.learner.ViewProfileActivity.class, 202, bundle);
                        return;
                    }

                    if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                        Navigator.getInstance().navigateToActivity(getActivity(), SpeakerViewProfileActivity.class);
                        return;
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.sign_up_text_view:
                if(getActivity() != null) {
                    getActivity().finish();
                    Navigator.getInstance().navigateToActivityWithClearStack(getActivity(), WelcomeActivity.class);
                }
                break;

            case R.id.view_certificates_text_view:
                if(Utils.isNetworkAvailable()) {
                    if (PreferenceUtil.isLearnerRole()) {
                        //showSnackBar("View Fixtures - Feature Coming Soon");
                        if(Utils.isNetworkAvailable()) {
                            getFixturesList();
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    } else if (PreferenceUtil.isCoachRole()) {
                        //showSnackBar("View Certificates - Feature Coming Soon");
                        if(Utils.isNetworkAvailable()) {
                            isFromCertificate = true;
                            Bundle bundleCert = new Bundle();
                            bundleCert.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, true);
                            Navigator.getInstance().navigateToActivityWithData(getActivity(),
                                    AddCertificatesActivity.class, bundleCert);
                        } else {
                            isFromCertificate = false;
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.view_facilities_text_view:
                if(Utils.isNetworkAvailable()) {
                    if (PreferenceUtil.isLearnerRole()) {
                        Navigator.getInstance().navigateToActivity(getActivity(), ChangePasswordActivity.class);
                    } else if (PreferenceUtil.isCoachRole()) {
                        //showSnackBar("View Facilities - Feature Coming Soon");
                        Bundle bundleFacility = new Bundle();
                        bundleFacility.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, true);
                        Navigator.getInstance().navigateToActivityWithData(getActivity(), AddFacilityActivity.class, bundleFacility);
                    } else if (PreferenceUtil.isGuestSpeakerRole()) {
                        Navigator.getInstance().navigateToActivity(getActivity(), ChangePasswordActivity.class);
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.training_manual_text_view:
                if(Utils.isNetworkAvailable()) {
                    if (PreferenceUtil.isLearnerRole()) {
                        if(Utils.isNetworkAvailable()) {
                            Bundle bundleCard = new Bundle();
                            bundleCard.putBoolean(Constants.BundleKey.IS_FROM_SETTINGS, true);
                            Navigator.getInstance().navigateToActivityWithData(getActivity(), ManageCardListActivity.class, bundleCard);
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    } else if (PreferenceUtil.isCoachRole()) {
                        if(Utils.isNetworkAvailable()) {
                            UserProfileModel mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
                            if(mProfileModel != null && mProfileModel.getSportDetail() != null
                                    && mProfileModel.getSportDetail().getId() != 0) {
                                ArrayList<Integer> sportIdsArray = new ArrayList<>();
                                sportIdsArray.add(mProfileModel.getSportDetail().getId());
                                if(sportIdsArray != null && sportIdsArray.size() > 0) {
                                    Bundle bundledTraining = new Bundle();
                                    bundledTraining.putInt(Constants.EXTRA_WEB_CONTENT, Constants.SELECTED_WEB_CONTENT.TRAINING_MANUAL);
                                    bundledTraining.putIntegerArrayList(Constants.BundleKey.SPORTS_IDS, sportIdsArray);
                                    Navigator.getInstance().navigateToActivityWithData(getActivity(), TNCActivity.class, bundledTraining);
                                } else {
                                    showSnackbarFromTop(getResources().getString(R.string.sports_details_not_found));
                                }
                            } else {
                                showSnackbarFromTop(getResources().getString(R.string.sports_details_not_found));
                            }
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    } else if (PreferenceUtil.isGuestSpeakerRole()) {
                        if(Utils.isNetworkAvailable()) {
                            Navigator.getInstance().navigateToActivity(getActivity(), ManagePaymentActivity.class);
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.training_equipment_text_view:
                if(Utils.isNetworkAvailable()) {
                    if(PreferenceUtil.isCoachRole()) {
                        if(Utils.isNetworkAvailable()) {
                            UserProfileModel mProfileModel = UserProfileInstance.getInstance().getUserProfileModel();
                            if(mProfileModel != null && mProfileModel.getSportDetail() != null
                                    && mProfileModel.getSportDetail().getId() != 0) {
                                ArrayList<Integer> sportIdsArray = new ArrayList<>();
                                sportIdsArray.add(mProfileModel.getSportDetail().getId());
                                if(sportIdsArray != null && sportIdsArray.size() > 0) {
                                    Bundle bundledTrainingEquipment = new Bundle();
                                    bundledTrainingEquipment.putInt(Constants.EXTRA_WEB_CONTENT, Constants.SELECTED_WEB_CONTENT.TRAINING_EQUIPMENT);
                                    bundledTrainingEquipment.putIntegerArrayList(Constants.BundleKey.SPORTS_IDS, sportIdsArray);
                                    Navigator.getInstance().navigateToActivityWithData(getActivity(), TNCActivity.class, bundledTrainingEquipment);
                                } else {
                                    showSnackbarFromTop(getResources().getString(R.string.sports_details_not_found));
                                }
                            } else {
                                showSnackbarFromTop(getResources().getString(R.string.sports_details_not_found));
                            }
                        } else {
                            showSnackbarFromTop(getResources().getString(R.string.no_internet));
                        }
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.settings_text_view:
                if(Utils.isNetworkAvailable()) {
                    try {
                        Bundle b = new Bundle();

                        int toggleNotificationStatus = 1;

                        if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                            toggleNotificationStatus = mProfileCoach.getToggleNotification();
                        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                            toggleNotificationStatus = mProfileCoach.getToggleNotification();
                        } else if(PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                            toggleNotificationStatus = mProfileLearner.getToggleNotification();
                        }

                        b.putInt(Constants.TOGGLE_NOTIFICATIONS.KEY_NOTIFICATIONS, toggleNotificationStatus);

                        Intent settingsIntent = new Intent(getActivity(), SettingsActivity.class);
                        settingsIntent.putExtras(b);
                        startActivityForResult(settingsIntent, Constants.REQUEST_CODE.REQUEST_CODE_NOTIFS_TOGGLE, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showSnackbarFromTop(getResources().getString(R.string.no_internet));
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 202:
                    mProfileLearner = data.getParcelableExtra(Constants.BundleKey.DATA);
                    setUpView(mProfileLearner.getName(), mProfileLearner.getProfileImage(), mProfileLearner.getEmail());
                    break;

                case Constants.REQUEST_CODE.REQUEST_CODE_NOTIFS_TOGGLE:
                    if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.COACH) {
                        getCoachProfileStatus();
                    } else if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.GUEST_SPEAKER) {
                        getGuestSpeakerProfileStatus();
                    } else {
                        getLearnerProfile();
                    }
                    break;
            }
        }
    }

    private void getFixturesList() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class,true);
        webServices.fixtureList().enqueue(new BaseCallback<FixtureListResponse>(getBaseActivity()) {
            @Override
            public void onSuccess(FixtureListResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if (response.getResult() != null) {
                            if (fixtureListResponse != null) {
                                fixtureListResponse = null;
                            }
                            fixtureListResponse = response;
                            if(isFixtureOrSessionAvailable()) {
                                Bundle bundleFixture = new Bundle();
                                bundleFixture.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, true);
                                Navigator.getInstance().navigateToActivityForResultWithData(getActivity(),
                                        CreateFixtureListActivity.class, bundleFixture, 303);
                            } else {
                                showSnackbarFromTop(getResources().getString(R.string.no_upcoming_session_or_fixture));
                            }
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<FixtureListResponse> call, BaseResponse baseResponse) {
                if(fixtureListResponse != null) {
                    fixtureListResponse = null;
                }
                try {
                    getBaseActivity().hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean isFixtureOrSessionAvailable() {
        boolean isAvailable = true;
        ViewProfileActivity.isFixtureOrSessionAvailable = true;
        if(fixtureListResponse != null && fixtureListResponse.getResult() != null) {
            if((fixtureListResponse.getResult().getFixtureList() == null
                    || fixtureListResponse.getResult().getFixtureList().size() == 0)
                    && (fixtureListResponse.getResult().getUpcomingSports() == null
                    || fixtureListResponse.getResult().getUpcomingSports().size() == 0)) {
                ViewProfileActivity.isFixtureOrSessionAvailable = false;
                isAvailable = false;
            } else {
                ViewProfileActivity.isFixtureOrSessionAvailable = true;
                isAvailable = true;
            }
        }

        return isAvailable;
    }

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }

}
