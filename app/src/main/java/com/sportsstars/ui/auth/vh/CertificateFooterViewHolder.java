package com.sportsstars.ui.auth.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sportsstars.databinding.ItemAocLayoutBinding;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class CertificateFooterViewHolder extends RecyclerView.ViewHolder {

    private ItemAocLayoutBinding mBinding;

    public CertificateFooterViewHolder(ItemAocLayoutBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindData(final IAocCallback listener){
        mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOptionSelected();
            }
        });
    }

    public interface IAocCallback {
        void onOptionSelected();
    }

}
