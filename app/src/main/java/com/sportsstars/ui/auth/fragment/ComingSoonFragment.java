package com.sportsstars.ui.auth.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.FragmentComingSoonBinding;
import com.sportsstars.ui.common.BaseFragment;

public class ComingSoonFragment extends BaseFragment {

    private FragmentComingSoonBinding fragmentComingSoonBinding;

    @Override
    public String getFragmentName() {
        return getClass().getSimpleName();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentComingSoonBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coming_soon, container, false);
        return fragmentComingSoonBinding.getRoot();
    }
}
