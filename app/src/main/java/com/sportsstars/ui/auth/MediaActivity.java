package com.sportsstars.ui.auth;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import com.sportsstars.R;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.PlayServicesUtils;

/**
 * Created by atul on 17/04/18.
 * To inject activity reference.
 */

public class MediaActivity extends AppCompatActivity{

    VideoView videoView;
    String videoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_layout);
        videoView = findViewById(R.id.videoView);
        new MediaAsyncTask().execute();
    }

    private class MediaAsyncTask extends AsyncTask<Void, Void, Void>
    {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MediaActivity.this, "", "Loading Video wait...", true);
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {
                String url = "http://www.youtube.com/watch?v=OtLa7wDpuOU";
                videoUrl = PlayServicesUtils.getUrlVideoRTSP(url);
                LogUtils.LOGE("Video url for playing=========>>>>>", videoUrl);
            }
            catch (Exception e)
            {
                LogUtils.LOGE("Login Soap Calling in Exception", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            progressDialog.dismiss();

            videoView.setVideoURI(Uri.parse(videoUrl));
            MediaController mc = new MediaController(MediaActivity.this);
            videoView.setMediaController(mc);
            videoView.requestFocus();
            videoView.start();
            mc.show();
        }

    }

}
