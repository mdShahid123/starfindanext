package com.sportsstars.ui.auth.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sportsstars.R;
import com.sportsstars.model.SlotTime;
import com.sportsstars.network.response.auth.AvailabilityResult;
import com.sportsstars.util.Constants;

/**
 * Created by abhaykant on 16/04/18.
 */

public class AvailabilityForLearnerAdapter extends BaseAdapter{

    private AvailabilityResult AvailabilityResult ;
    private Activity mActivity;

    public AvailabilityForLearnerAdapter(Activity activity, AvailabilityResult availabilityResult) {
        this.AvailabilityResult = availabilityResult;
        mActivity=activity;
    }


    public int getCount() {
        return AvailabilityResult.getSlotTime().size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            view = inflater.inflate(R.layout.item_availability_time, null);
        }
        SlotTime slotTime = AvailabilityResult.getSlotTime().get(position);
        TextView tvSportName = view.findViewById(R.id.tv_availability_time);

        if(slotTime.getSlotTime()>=12) {
            int slotTimePm= slotTime.getSlotTime()-12;
            if(slotTimePm==0){
                tvSportName.setText("" + 12 + ":00 pm");
            }else {
                tvSportName.setText("" + slotTimePm + ":00 pm");
            }
        }else {
            if(slotTime.getSlotTime()==0) {
                tvSportName.setText("" + 12 + ":00 am");
            }else {
                tvSportName.setText("" + slotTime.getSlotTime() + ":00 am");

            }
        }

        if(slotTime.getStatus()== Constants.TIME_SLOT_BOOK_STATUS.SLOT_AVAILABLE_STATUS){
            tvSportName.setBackgroundColor(ContextCompat.getColor(mActivity,R.color.cal_cell_bg));
        }else if(slotTime.getStatus()== Constants.TIME_SLOT_BOOK_STATUS.SLOT_BOOKED_STATUS){
            tvSportName.setText(mActivity.getString(R.string.booked));
            tvSportName.setBackgroundColor(ContextCompat.getColor(mActivity,R.color.un_available_color));
        }//new status 2
        else if(slotTime.getStatus()== Constants.TIME_SLOT_BOOK_STATUS.SLOT_SELECTED_STATUS){
            tvSportName.setBackgroundColor(ContextCompat.getColor(mActivity,R.color.app_btn_green));
        }
        else {
            tvSportName.setText(mActivity.getString(R.string.unavailable));
            tvSportName.setBackgroundColor(ContextCompat.getColor(mActivity,R.color.un_available_color));

        }

        return view;
    }
}

