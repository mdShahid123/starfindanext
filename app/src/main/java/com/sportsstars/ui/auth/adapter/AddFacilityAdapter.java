package com.sportsstars.ui.auth.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sportsstars.R;
import com.sportsstars.databinding.ItemAofLayoutBinding;
import com.sportsstars.databinding.ItemFacilityLayoutBinding;
import com.sportsstars.model.Facility;
import com.sportsstars.ui.auth.vh.FacilityFooterViewHolder;
import com.sportsstars.ui.auth.vh.FacilityViewHolder;
import com.sportsstars.util.Constants;
import com.sportsstars.util.StringUtils;

import java.util.List;

/**
 * Created by atul on 23/03/18.
 * To inject activity reference.
 */

public class AddFacilityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FacilityFooterViewHolder.ISuburbCallback {

    private List<Facility> mFacilities;
    private ISuburbCallback mISuburbCallback;


    public AddFacilityAdapter(ISuburbCallback suburbCallback, List<Facility> facilities) {
        mFacilities = facilities;
        mISuburbCallback = suburbCallback;
        addFooter();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == Constants.FACILITIES.FOOTER) {
            viewHolder = new FacilityFooterViewHolder((ItemAofLayoutBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_aof_layout, parent, false));
        } else {
            viewHolder = new FacilityViewHolder((ItemFacilityLayoutBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_facility_layout, parent, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FacilityFooterViewHolder) {
            ((FacilityFooterViewHolder) holder).bindData(this, mFacilities);
        } else if (holder instanceof FacilityViewHolder) {
            ItemFacilityLayoutBinding layoutBinding = ((FacilityViewHolder) holder).bindData(mFacilities.get(position));
            layoutBinding.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mISuburbCallback.onRemoveSuburb(holder.getAdapterPosition());
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mFacilities.size() == 0 || StringUtils.isNullOrEmpty(mFacilities.get(position).getFacilityAddress()))
            return Constants.FACILITIES.FOOTER;
        return Constants.FACILITIES.ITEM;
    }

    @Override
    public int getItemCount() {
        return mFacilities.size();
    }

    @Override
    public void onAddSuburb() {
        mISuburbCallback.onAddSuburb();
    }

    public void updateFacility() {
        notifyItemInserted(mFacilities.size());
    }

    public void removeSuburb(int position) {
        mFacilities.remove(position);
        notifyItemRemoved(position);
        notifyItemChanged(mFacilities.size() - 1);
    }

    private void addFooter() {
        if (mFacilities.size() == 0 || !StringUtils.isNullOrEmpty(mFacilities.get(mFacilities.size() - 1).getFacilityAddress())) {
            mFacilities.add(new Facility());
        }
    }

    public interface ISuburbCallback {
        void onAddSuburb();

        void onRemoveSuburb(int position);
    }

}
