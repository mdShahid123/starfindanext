package com.sportsstars.ui.auth.vh;

import android.support.v7.widget.RecyclerView;

import com.sportsstars.databinding.ItemFacilityLayoutBinding;
import com.sportsstars.model.Facility;

/**
 * Created by atul on 28/03/18.
 * To inject activity reference.
 */

public class FacilityViewHolder extends RecyclerView.ViewHolder{

    private ItemFacilityLayoutBinding mLayoutBinding;

    public FacilityViewHolder(ItemFacilityLayoutBinding layoutBinding){
        super(layoutBinding.getRoot());
        mLayoutBinding = layoutBinding;
    }

    public ItemFacilityLayoutBinding bindData(Facility facility){
        mLayoutBinding.setFacility(facility);
        return mLayoutBinding;
    }

}
