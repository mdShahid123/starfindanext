package com.sportsstars.ui.auth;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.sportsstars.MVVM.ui.learner.LearnerHomeActivity;
import com.sportsstars.R;
import com.sportsstars.SportsStarsApp;
import com.sportsstars.databinding.ActivityUserTypesBinding;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.PostRatingRequest;
import com.sportsstars.network.request.auth.UpdateDeviceTokenRequest;
import com.sportsstars.network.request.auth.UserRoleRequest;
import com.sportsstars.network.response.CalendarSyncBaseResponse;
import com.sportsstars.network.response.PendingRating;
import com.sportsstars.network.response.PendingRatingResponse;
import com.sportsstars.network.response.auth.CoachProfileStatusResponse;
import com.sportsstars.network.response.auth.GetProfileResponse;
import com.sportsstars.network.response.auth.ProfileStatusResult;
import com.sportsstars.network.response.auth.UserRoleResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.coach.CoachCreateProfileActivity;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.ui.common.HomeActivity;
import com.sportsstars.ui.search.SearchCoachSpeakerActivity;
import com.sportsstars.ui.speaker.SpeakerCreateProfileActivity;
import com.sportsstars.ui.speaker.SpeakerHomeActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.calendarsync.CalendarSyncHelper;

import java.util.ArrayList;

import retrofit2.Call;

public class WelcomeActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(WelcomeActivity.class);
    private ActivityUserTypesBinding mBinder;
    private int mUserRole = Constants.USER_ROLE.LEARNER;
    private boolean isLogin;
    private boolean isFromTutorial;
    private ProfileStatusResult result;
    private ArrayList<PendingRating> pendingRatingList = new ArrayList<>();
    private int currentRatingPosition;
    private int selectedRating;

    private ProgressDialog calendarProgressDialog;

    @Override
    public String getActivityName() {
        return WelcomeActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_user_types);

        Utils.setTransparentTheme(this);
        mBinder.headerId.tvTitle.setText(getString(R.string.welcome));

        isLogin = PreferenceUtil.getIsLogin();
        mBinder.setIsLogin(false);
        if (isLogin) {
            mBinder.layoutAlreadyAcc.setVisibility(View.VISIBLE);
            mBinder.headerId.tvTitle.setText(getString(R.string.welcome_back));
            mBinder.tvYouCanSwitch.setVisibility(View.VISIBLE);
            mBinder.tvAlreadyHaveAcc.setVisibility(View.GONE);
            mBinder.tvSignIn.setVisibility(View.GONE);

            mBinder.tvLearnerLoginInfo.setVisibility(View.VISIBLE);
            mBinder.tvSetupCoach.setVisibility(View.VISIBLE);
            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFromTutorial = bundle.getBoolean(Constants.EXTRA_FROM_TUTORIAL);
        }
        if (isFromTutorial) {
            mBinder.headerId.ivBack.setVisibility(View.VISIBLE);
        } else {
            mBinder.headerId.ivBack.setVisibility(View.GONE);
        }

        init();

        if (isLogin) {

            if (Utils.isNetworkAvailable()) {
                updateDeviceToken();
            }

            try {
//                showDummyRatingDialog();
                if (PreferenceUtil.getCurrentUserRole() == Constants.USER_ROLE.LEARNER) {
                    if (Utils.isNetworkAvailable()) {
                        getPendingRatings();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (ContextCompat.checkSelfPermission(WelcomeActivity.this, Manifest.permission.WRITE_CALENDAR)
                    != PackageManager.PERMISSION_GRANTED) {
                askCalendarPermission();
            } else {
                if (ContextCompat.checkSelfPermission(WelcomeActivity.this, Manifest.permission.READ_CALENDAR)
                        != PackageManager.PERMISSION_GRANTED) {
                    askReadCalendarPermission();
                } else {
                    if (Utils.isNetworkAvailable()) {
                        getBookingEventListToCalendar();
                    }
                }
            }

        } else {
            Alert.showRegisterUserDialog(WelcomeActivity.this,
                    getResources().getString(R.string.register_user_title),
                    getResources().getString(R.string.register_user_alert_msg),
                    getResources().getString(R.string.cancel),
                    getResources().getString(R.string.sign_up),
                    new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            Bundle bundleRelCoach = new Bundle();
                            bundleRelCoach.putBoolean(Constants.EXTRA_IS_DIRECTLY_FROM_WELCOME, true);
                            Navigator.getInstance().navigateToActivityWithData(WelcomeActivity.this, LoginActivity.class, bundleRelCoach);
                            finish();
                        }
                    },
                    new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {

                        }
                    }
            );
        }

    }

    private void getPendingRatings() {

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getPendingRating(Constants.USER_ROLE.LEARNER).enqueue(new BaseCallback<PendingRatingResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(PendingRatingResponse response) {
                if (response != null && response.getStatus() == 1) {
                    if (pendingRatingList != null && pendingRatingList.size() > 0) {
                        pendingRatingList.clear();
                    }
                    currentRatingPosition = 0;
                    selectedRating = 0;
                    if (response.getResult() != null && response.getResult().size() > 0) {
                        if(pendingRatingList!=null) {
                            pendingRatingList.addAll(response.getResult());
                        }
                        managePendingRatingAlertDialog();
                    }
                }
            }

            @Override
            public void onFail(Call<PendingRatingResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
//                if (baseResponse != null && baseResponse.getMessage() != null)
//                    showSnackbarFromTop(CreateFixtureListActivity.this, baseResponse.getMessage());
            }
        });
    }

    private void managePendingRatingAlertDialog() {
        if (pendingRatingList != null && pendingRatingList.size() > 0) {
            if (currentRatingPosition < pendingRatingList.size()) {
                PendingRating pendingRating = pendingRatingList.get(currentRatingPosition);
                if (pendingRating != null) {
                    hideProgressBarForCalendar();
                    showPendingRatingAlertDialog(pendingRating);
                } else {
                    hideProgressBarForCalendar();
                    showNextRatingAlertDialog();
                }
            }
        }
    }

//    // This method should be removed before release, using this for testing only.
//    private void showDummyRatingDialog() {
//        PendingRating pendingRating = new PendingRating();
//        pendingRating.setName("Dummy Coach");
//        pendingRating.setProfileImage("");
//        pendingRating.setSessionId(0);
//        pendingRating.setUserId(0);
//        showPendingRatingAlertDialog(pendingRating);
//    }

    private void showPendingRatingAlertDialog(PendingRating pendingRating) {
        if (pendingRating != null) {
            selectedRating = 0;
            String coachName = "Your coach";
            String coachProfileImageUrl = "";
            if (!TextUtils.isEmpty(pendingRating.getName())) {
                coachName = pendingRating.getName();
            }
            if (!TextUtils.isEmpty(pendingRating.getProfileImage())) {
                coachProfileImageUrl = pendingRating.getProfileImage();
            }
            String ratingMessage = getResources().getString(R.string.please_rate_your_coach, coachName);
            Alert.showRatingAlertDialog(
                    WelcomeActivity.this,
                    coachProfileImageUrl,
                    getResources().getString(R.string.rate_your_session),
                    ratingMessage,
                    new Alert.OnRatingBarChangeListener() {
                        @Override
                        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                            if (fromUser) {
                                //Toast.makeText(WelcomeActivity.this, "Your selected rating is : " + rating, Toast.LENGTH_LONG).show();
                                float newRating = (float) Math.ceil(rating);
                                ratingBar.setRating(newRating);
                                selectedRating = (int) newRating;
                            }
                        }
                    },
                    new Alert.OnOkayClickListener() {
                        @Override
                        public void onOkay() {
                            if (Utils.isNetworkAvailable()) {
                                if (selectedRating < 1) {
                                    showToast(getResources().getString(R.string.please_rate_your_session));
                                    showNextRatingAlertDialog();
                                    //showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.please_rate_your_session));
                                } else {
                                    // Call here to check if coach exists or not.
                                    getCoachProfile(pendingRating, selectedRating, false);

                                    //postRating(pendingRating, selectedRating, false);
                                }
                            } else {
                                showNextRatingAlertDialog();
                                showToast(getResources().getString(R.string.no_internet));
                                //showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.no_internet));
                            }
                        }
                    },
                    new Alert.OnCancelClickListener() {
                        @Override
                        public void onCancel() {
                            if (Utils.isNetworkAvailable()) {
                                // Call here to check if coach exists or not.
                                getCoachProfile(pendingRating, 0, true);

                                //postRating(pendingRating, 0, true);
                            } else {
                                showNextRatingAlertDialog();
                            }
                        }
                    }
            );
        }
    }

    private void showNextRatingAlertDialog() {
        currentRatingPosition++;
        managePendingRatingAlertDialog();
    }

    private void postRating(PendingRating pendingRating, int rating, boolean isSkip) {

        PostRatingRequest postRatingRequest = new PostRatingRequest();
        postRatingRequest.setToUserId(pendingRating.getUserId());
        postRatingRequest.setSessionId(pendingRating.getSessionId());
        postRatingRequest.setSkip(isSkip);
        postRatingRequest.setRating(rating);

        if (postRatingRequest.getToUserId() == 0 || postRatingRequest.getSessionId() == 0) {
            return;
        }

        if (!postRatingRequest.isSkip() && postRatingRequest.getRating() < 1) {
            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.please_rate_your_session));
            return;
        }

        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.postRating(postRatingRequest).enqueue(new BaseCallback<BaseResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
//                        if(!TextUtils.isEmpty(response.getMessage())) {
//                            showToast(response.getMessage());
//                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
//                        }
                        showNextRatingAlertDialog();
                    } else {
//                        if(!TextUtils.isEmpty(response.getMessage())) {
//                            showToast(response.getMessage());
//                            //showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
//                        }
                        showNextRatingAlertDialog();
                    }
                }
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void updateDeviceToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        String fcmToken = PreferenceUtil.getFCMToken();
        //LogUtils.LOGD("FCM instance token ", token);
        //LogUtils.LOGD("FCM token", token);
        if (TextUtils.isEmpty(token) && TextUtils.isEmpty(fcmToken)) {
        } else if (!TextUtils.isEmpty(token)) {
            PreferenceUtil.setFCMToken(token);
        }

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, false);
        webServices.updateDeviceToken(new UpdateDeviceTokenRequest(PreferenceUtil.getFCMToken())).enqueue(new BaseCallback<BaseResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(BaseResponse response) {
            }

            @Override
            public void onFail(Call<BaseResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });
    }

    private void init() {
        mBinder.headerId.ivBack.setOnClickListener(this);
        mBinder.relLearnerMain.setOnClickListener(this);
        mBinder.tvSignIn.setOnClickListener(this);
        mBinder.relCoachMain.setOnClickListener(this);
        mBinder.relSpeakMain.setOnClickListener(this);

        if (isLogin) {
            getCoachProfileStatus();
        }

        SportsStarsApp.destroyAllStaticVariables();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_sign_in:
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.no_internet));
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.EXTRA_IS_FORGOT_PASS, true);
                bundle.putBoolean(Constants.EXTRA_IS_DIRECTLY_FROM_WELCOME, true);
                Navigator.getInstance().navigateToActivityWithData(WelcomeActivity.this, LoginActivity.class, bundle);
                finish();
                break;

            case R.id.rel_learner_main:
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.no_internet));
                    return;
                }
                mUserRole = Constants.USER_ROLE.LEARNER;
                PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.LEARNER);
                clearPendingNotifications(mUserRole);
                if (isLogin) {
                    getLearnerStatus();
                } else {
                    Navigator.getInstance().navigateToActivity(WelcomeActivity.this, SearchCoachSpeakerActivity.class);
                }
                break;

            case R.id.rel_coach_main:
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.no_internet));
                    return;
                }
                mUserRole = Constants.USER_ROLE.COACH;
                PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.COACH);
                clearPendingNotifications(mUserRole);
                if (isLogin) {
                    getCoachStatus();
//                    if(result != null) {
//                        if(result.getIsProfileCompleted() == 0
//                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
//                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, CoachCreateProfileActivity.class);
//                        } else if (result.getIsProfileCompleted() == 1
//                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
//                            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_not_verified));
//                        }  else if (result.getIsProfileCompleted() == 1
//                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
//                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, HomeActivity.class);
//                        }
//                    }
                } else {
                    Bundle bundleRelCoach = new Bundle();
                    bundleRelCoach.putBoolean(Constants.EXTRA_IS_DIRECTLY_FROM_WELCOME, true);
                    Navigator.getInstance().navigateToActivityWithData(WelcomeActivity.this, LoginActivity.class, bundleRelCoach);
                    finish();
                }
                break;

            case R.id.rel_speak_main:
                if (!Utils.isNetworkAvailable()) {
                    showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.no_internet));
                    return;
                }
                mUserRole = Constants.USER_ROLE.GUEST_SPEAKER;
                PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.GUEST_SPEAKER);
                clearPendingNotifications(mUserRole);
                if (isLogin) {
                    getGuestSpeakerStatus();
//                    if(result != null) {
//                        if(result.getIsProfileCompleted() == 0
//                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
//                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, CoachCreateProfileActivity.class);
//                        } else if (result.getIsProfileCompleted() == 1
//                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
//                            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_not_verified));
//                        }  else if (result.getIsProfileCompleted() == 1
//                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
//                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, HomeActivity.class);
//                        }
//                    }
                } else {
                    Bundle bundleRelSpeaker = new Bundle();
                    bundleRelSpeaker.putBoolean(Constants.EXTRA_IS_DIRECTLY_FROM_WELCOME, true);
                    Navigator.getInstance().navigateToActivityWithData(WelcomeActivity.this, LoginActivity.class, bundleRelSpeaker);
                    finish();
                }
                break;

        }
    }

    private void clearPendingNotifications(int mUserRole) {
        Utils.clearAllNotifications(WelcomeActivity.this);
    }

    private void userRoleApi() {
        processToShowDialog();

        UserRoleRequest userRoleRequest = new UserRoleRequest();
        userRoleRequest.setUserLoggedInType(mUserRole);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.userRole(userRoleRequest).enqueue(new BaseCallback<UserRoleResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(UserRoleResponse response) {

                if (response.getStatus() == 1) {
                    /*UserModel userModel = PreferenceUtil.getUserModel();
                    userModel.setUserLoggedInType(mUserRole);
                    userModel.setStatus(1);
                    PreferenceUtil.setUserModel(userModel);*/

                    if (mUserRole == Constants.USER_ROLE.LEARNER) {
                        Navigator.getInstance().navigateToActivity(WelcomeActivity.this, LearnerHomeActivity.class);
                        //Navigator.getInstance().navigateToActivity(WelcomeActivity.this, com.sportsstars.ui.learner.ViewProfileActivity.class);
                    } else if (mUserRole == Constants.USER_ROLE.COACH) {
                        Navigator.getInstance().navigateToActivity(WelcomeActivity.this, HomeActivity.class);
                    } else {
                        Navigator.getInstance().navigateToActivity(WelcomeActivity.this, SpeakerHomeActivity.class);
                    }

                } else {
                    showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFail(Call<UserRoleResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
                if (baseResponse != null && baseResponse.getMessage() != null) {
                    showSnackbarFromTop(WelcomeActivity.this, baseResponse.getMessage());

                }
            }
        });
    }


    private void getCoachProfileStatus() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    result = response.getResult();
                    setStatusText(result);
                } else {
                    try {
                        if (response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(WelcomeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void launchPhoneVerificationScreen(boolean isEditModeEnabled) {
        // go to phone number screen
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, isEditModeEnabled);
        bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, false);
        Navigator.getInstance().navigateToActivityWithData(WelcomeActivity.this,
                VerifyPhoneActivity.class, bundle);
    }

    private void getLearnerStatus() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    result = response.getResult();
                    if (result != null) {
                        setStatusText(result);
                        if(result.getIsMobileVerified() == 0
                                && result.getIsLearnerProfileCompleted() == 0) {
                            launchPhoneVerificationScreen(false);
//                            if(result.getIsLearnerProfileCompleted() == 1) {
//                                showVerifyPhoneDialog();
//                            } else {
//                                launchPhoneVerificationScreen(false);
//                            }
                        } else if (result.getIsLearnerProfileCompleted() == 1) {
                            userRoleApi();
                        } else {
                            //Log.d("learnerlog", "else case - getIsLearnerProfileCompleted : " + result.getIsLearnerProfileCompleted());
                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, com.sportsstars.ui.learner.CreateProfileActivity.class);
                        }
                    }
                } else {
                    try {
                        if (response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(WelcomeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getCoachStatus() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    result = response.getResult();
                    if (result != null) {
                        if(result.getIsMobileVerified() == 0
                                && result.getIsProfileCompleted() == 0) {
                            launchPhoneVerificationScreen(false);
//                            if(result.getIsProfileCompleted() == 1
//                                    && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
//                                showVerifyPhoneDialog();
//                            } else {
//                                launchPhoneVerificationScreen(false);
//                            }
                        } else if (result.getIsProfileCompleted() == 0
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, CoachCreateProfileActivity.class);
                        } else if (result.getIsProfileCompleted() == 1
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_not_verified));
                        } else if (result.getIsProfileCompleted() == 1
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
                            if (Utils.isNetworkAvailable()) {
                                userRoleApi();
                            } else {
                                Navigator.getInstance().navigateToActivity(WelcomeActivity.this, HomeActivity.class);
                            }
                        } else if (result.getIsProfileCompleted() == 1
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_REJECTED) {
                            //showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_rejected_by_admin));
                            Alert.showInfoDialog(WelcomeActivity.this,
                                    getResources().getString(R.string.coach_rejected_by_admin_alert_title),
                                    getResources().getString(R.string.coach_rejected_by_admin),
                                    getResources().getString(R.string.okay),
                                    new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {
                                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, CoachCreateProfileActivity.class);
                                        }
                                    });
                        } else if (result.getIsProfileCompleted() == 1
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_RESUBMITTED) {
                            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_resubmitted_for_approval_by_admin));
                        } else if (result.getIsProfileCompleted() == 0
                                && result.getIsVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, CoachCreateProfileActivity.class);
                        }

                        setStatusText(result);
                    }
                } else {
                    try {
                        if (response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(WelcomeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getGuestSpeakerStatus() {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    result = response.getResult();
                    if (result != null) {
                        if(result.getIsMobileVerified() == 0
                                && result.getIsGuestProfileCompleted() == 0) {
                            launchPhoneVerificationScreen(false);
//                            if(result.getIsGuestProfileCompleted() == 1
//                                    && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
//                                showVerifyPhoneDialog();
//                            } else {
//                                launchPhoneVerificationScreen(false);
//                            }
                        } else if (result.getIsGuestProfileCompleted() == 0
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, SpeakerCreateProfileActivity.class);
                        } else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.NOT_VERIFIED) {
                            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_not_verified));
                        } else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
                            if (Utils.isNetworkAvailable()) {
                                userRoleApi();
                            } else {
                                Navigator.getInstance().navigateToActivity(WelcomeActivity.this, SpeakerHomeActivity.class);
                            }
                        } else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_REJECTED) {
                            //showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_rejected_by_admin));
                            Alert.showInfoDialog(WelcomeActivity.this,
                                    getResources().getString(R.string.coach_rejected_by_admin_alert_title),
                                    getResources().getString(R.string.speaker_rejected_by_admin),
                                    getResources().getString(R.string.okay),
                                    new Alert.OnOkayClickListener() {
                                        @Override
                                        public void onOkay() {
                                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, SpeakerCreateProfileActivity.class);
                                        }
                                    });
                        } else if (result.getIsGuestProfileCompleted() == 1
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_RESUBMITTED) {
                            showSnackbarFromTop(WelcomeActivity.this, getResources().getString(R.string.coach_resubmitted_for_approval_by_admin));
                        } else if (result.getIsGuestProfileCompleted() == 0
                                && result.getIsSpeakerVerified() == Constants.USER_VERIFIED_STATUS.IS_VERIFIED) {
                            Navigator.getInstance().navigateToActivity(WelcomeActivity.this, SpeakerCreateProfileActivity.class);
                        }

                        setStatusText(result);
                    }
                } else {
                    try {
                        if (response != null && response.getMessage() != null
                                && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(WelcomeActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(WelcomeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setStatusText(ProfileStatusResult result) {
        if (result == null) {
            return;
        }
        // Conditions check for Coach
        if (result.getIsProfileCompleted() == 1 && result.getIsVerified() == 1) {
            mBinder.tvSetupCoach.setVisibility(View.VISIBLE);
            mBinder.tvSetupCoach.setText(getString(R.string.check_ur_session));
        } else if (result.getIsProfileCompleted() == 1 && result.getIsVerified() == 0) {
            mBinder.tvSetupCoach.setVisibility(View.VISIBLE);
            mBinder.tvSetupCoach.setText(getString(R.string.check_ur_session));
        } else if (result.getIsProfileCompleted() == 0 && result.getIsVerified() == 0) {
            mBinder.tvSetupCoach.setVisibility(View.VISIBLE);
            mBinder.tvSetupCoach.setText(getString(R.string.set_up_coaches));
        } else {
            mBinder.tvSetupCoach.setVisibility(View.VISIBLE);
            mBinder.tvSetupCoach.setText(getString(R.string.set_up_coaches));
        }

        // Conditions check for learner
        if (result.getIsLearnerProfileCompleted() == 1) {
            mBinder.tvLearnerLoginInfo.setVisibility(View.VISIBLE);
            mBinder.tvLearnerLoginInfo.setText(getString(R.string.search_book_and_coaches));
        } else if (result.getIsLearnerProfileCompleted() == 0) {
            mBinder.tvLearnerLoginInfo.setVisibility(View.VISIBLE);
            mBinder.tvLearnerLoginInfo.setText(getString(R.string.setup_leaner));
        }

        // Conditions check for Guest Speaker
        if (result.getIsGuestProfileCompleted() == 1 && result.getIsSpeakerVerified() == 1) {
            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
            mBinder.tvSetupGuestSpeaker.setText(getString(R.string.check_ur_bookings));
        } else if (result.getIsGuestProfileCompleted() == 1 && result.getIsSpeakerVerified() == 0) {
            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
            mBinder.tvSetupGuestSpeaker.setText(getString(R.string.check_ur_bookings));
        } else if (result.getIsGuestProfileCompleted() == 0 && result.getIsSpeakerVerified() == 0) {
            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
            mBinder.tvSetupGuestSpeaker.setText(getString(R.string.setup_guest));
        } else {
            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
            mBinder.tvSetupGuestSpeaker.setText(getString(R.string.setup_guest));
        }

//        if (result.getIsGuestProfileCompleted() == 1) {
//            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
//            mBinder.tvSetupGuestSpeaker.setText(getString(R.string.setup_guest));
//        } else if (result.getIsGuestProfileCompleted() == 0) {
//            mBinder.tvSetupGuestSpeaker.setVisibility(View.VISIBLE);
//            mBinder.tvSetupGuestSpeaker.setText(getString(R.string.setup_guest));
//        }

    }

    private void getCoachProfile(PendingRating pendingRating, int selectedRating, boolean isSkip) {
        processToShowDialog();
        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.viewProfileDetail(pendingRating.getUserId()).enqueue(new BaseCallback<GetProfileResponse>(WelcomeActivity.this) {
            @Override
            public void onSuccess(GetProfileResponse response) {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        postRating(pendingRating, selectedRating, isSkip);
                    } else {
                        try {
                            showNextRatingAlertDialog();
                            if (response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                                showToast(response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<GetProfileResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    showNextRatingAlertDialog();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(WelcomeActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showVerifyPhoneDialog() {
        Alert.showOkCancelDialog(WelcomeActivity.this,
                getResources().getString(R.string.verify_phone_alert_title),
                getResources().getString(R.string.verify_phone_alert_msg),
                getResources().getString(R.string.skip),
                getResources().getString(R.string.verify),
                new Alert.OnOkayClickListener() {
                    @Override
                    public void onOkay() {
                        showToast("okay button clicked - launch verification screen");
                        //launchPhoneVerificationScreen(true);
                    }
                },
                new Alert.OnCancelClickListener() {
                    @Override
                    public void onCancel() {
                        showToast("cancel button clicked - set user role API to go to home screen");
                        //userRoleApi();
                    }
                }
        );
    }

    // PERMISSION RELATED TASKS //

    public static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 112;
    public static final int MY_PERMISSIONS_REQUEST_READ_CALENDAR = 113;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(WelcomeActivity.this, Manifest.permission.READ_CALENDAR)
                            != PackageManager.PERMISSION_GRANTED) {
                        askReadCalendarPermission();
                    } else {
                        if (Utils.isNetworkAvailable()) {
                            getBookingEventListToCalendar();
                        }
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_not_granted_for_calendar), Toast.LENGTH_SHORT).show();
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_CALENDAR:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Utils.isNetworkAvailable()) {
                        getBookingEventListToCalendar();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_not_granted_for_calendar), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void askCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CALENDAR)) {
            showCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
        }
    }

    private void askReadCalendarPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CALENDAR)) {
            showReadCalendarPermissionExplanation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CALENDAR},
                    MY_PERMISSIONS_REQUEST_READ_CALENDAR);
        }
    }

    private void showCalendarPermissionExplanation() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(WelcomeActivity.this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void showReadCalendarPermissionExplanation() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.calendar_permission_title));
        builder.setMessage(getResources().getString(R.string.calendar_read_permission_message));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(WelcomeActivity.this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        MY_PERMISSIONS_REQUEST_READ_CALENDAR);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void getBookingEventListToCalendar() {
        AuthWebServices authWebServices = RequestController.createService(AuthWebServices.class, true);
        authWebServices.getCalendarSyncData().enqueue(new BaseCallback<CalendarSyncBaseResponse>(this) {
            @Override
            public void onSuccess(CalendarSyncBaseResponse response) {
                hideProgressBar();
                if (response != null && response.getStatus() == 1) {
                    if (response.getResult() != null && response.getResult().size() > 0) {
                        try {
                            // Putting dummy loader to disable user interaction for 6 secs
                            // because there is no callback method implemented for device Calendar events manipulation
                            showProgressBarForCalendar();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressBarForCalendar();
                                }
                            }, 6000);

                            // Deleting existing calendar events first, updated events will be added from server.
                            CalendarSyncHelper.getInstance().deleteAllCalendarEvents(WelcomeActivity.this, response.getResult());

                            // Updating the list after all deletion task is performed in 3 secs.
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    CalendarSyncHelper.getInstance().updateCalendarEventList(WelcomeActivity.this, response.getResult());
                                }
                            }, 3000);
                            //CalendarSyncHelper.getInstance().updateCalendarEventList(WelcomeActivity.this, response.getResult());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<CalendarSyncBaseResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showProgressBarForCalendar() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            calendarProgressDialog = ProgressDialog.show(this, null, null);
            if (calendarProgressDialog.getWindow() != null) {
                calendarProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
            calendarProgressDialog.setCancelable(false);
            calendarProgressDialog.setContentView(View.inflate(this, R.layout.progress_bar, null));
        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.getMessage());
        }
    }

    public void hideProgressBarForCalendar() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            if (calendarProgressDialog != null && calendarProgressDialog.isShowing()) {
                calendarProgressDialog.dismiss();
            }

            calendarProgressDialog = null;

        } catch (Exception x) {
            LogUtils.LOGE(TAG, x.getMessage());
        }
    }

}
