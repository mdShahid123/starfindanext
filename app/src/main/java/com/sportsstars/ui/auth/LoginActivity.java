package com.sportsstars.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.FacebookSdkNotInitializedException;
import com.facebook.login.LoginManager;
import com.sportsstars.R;
import com.sportsstars.databinding.ActivityLoginBinding;
import com.sportsstars.model.DeviceInfo;
import com.sportsstars.model.UserModel;
import com.sportsstars.network.BaseCallback;
import com.sportsstars.network.BaseResponse;
import com.sportsstars.network.RequestController;
import com.sportsstars.network.request.auth.SignUpRequest;
import com.sportsstars.network.response.auth.CoachProfileStatusResponse;
import com.sportsstars.network.response.auth.ProfileStatusResult;
import com.sportsstars.network.response.auth.SignInResponse;
import com.sportsstars.network.response.auth.SignUpResponse;
import com.sportsstars.network.retrofit.AuthWebServices;
import com.sportsstars.ui.common.BaseActivity;
import com.sportsstars.util.Alert;
import com.sportsstars.util.Constants;
import com.sportsstars.util.LogUtils;
import com.sportsstars.util.Navigator;
import com.sportsstars.util.PreferenceUtil;
import com.sportsstars.util.Utils;
import com.sportsstars.util.socialhelper.SocialAuthError;
import com.sportsstars.util.socialhelper.SocialAuthListener;
import com.sportsstars.util.socialhelper.SocialGoogleHelper;
import com.sportsstars.util.socialhelper.SocialMediaHelper;
import com.sportsstars.util.socialhelper.SocialProfile;
import com.sportsstars.util.socialhelper.SocialType;

import retrofit2.Call;

/**
 *
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, SocialAuthListener, Alert.OnOkayClickListener {

    private static final String TAG = LogUtils.makeLogTag(LoginActivity.class);
    private ActivityLoginBinding mBinder;
    private static boolean mIsSignIn, isForLoginResult;
    private boolean mIsShowPassword = true;
    private SocialMediaHelper socialMediaHelper;

    private String mEnteredEmail;
    private String mEnteredPassword;
    private boolean isDirectlyFromWelcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Utils.setTransparentTheme(this);

        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_login);

        init();

        hideKeyboard(mBinder.etEmail);
        hideKeyboard(mBinder.etPassword);

        //signUpUI();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isForLoginResult = bundle.getBoolean(Constants.EXTRA_IS_LOGIN_RESULT);
            boolean fromForgotPass = bundle.getBoolean(Constants.EXTRA_IS_FORGOT_PASS);
            isDirectlyFromWelcome = bundle.getBoolean(Constants.EXTRA_IS_DIRECTLY_FROM_WELCOME);
            if (fromForgotPass) {
                mIsSignIn = true;
                signInUI();
            } else {
                signUpUI();
            }
        } else {
            signUpUI();
        }
    }

    private void init() {

        //initialise entered email and password to be empty strings
        mEnteredEmail = "";
        mEnteredPassword = "";

        socialMediaHelper = new SocialMediaHelper(this, this);

        mBinder.dividerUsing.tvSeparatorText.setText(getString(R.string.using));
        mBinder.btnSignInLabel.setOnClickListener(this);
        mBinder.btnSignUpLabel.setOnClickListener(this);
        mBinder.tvForgotPass.setOnClickListener(this);
        mBinder.imgPassword.setOnClickListener(this);
        mBinder.buttonSignIn.setOnClickListener(this);
        mBinder.tvPrivacyPolicy.setOnClickListener(this);
        mBinder.tvTermsCondtions.setOnClickListener(this);
        mBinder.btnGPlus.setOnClickListener(this);
        mBinder.btnFb.setOnClickListener(this);

        mBinder.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEnteredEmail = s.toString();
            }
        });

        mBinder.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    mBinder.imgPassword.setVisibility(View.VISIBLE);
                } else {
                    mBinder.imgPassword.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                mEnteredPassword = s.toString();
            }
        });

        //hide password initially
        Utils.showPassword(LoginActivity.this, mBinder.etPassword, mIsShowPassword, mBinder.imgPassword);

        mBinder.layoutBanner.ivHdrBack.setVisibility(View.VISIBLE);
        mBinder.layoutBanner.ivHdrBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack();
    }

    private void goBack() {
        if(isDirectlyFromWelcome) {
            Navigator.getInstance().navigateToActivityWithClearStack(LoginActivity.this, WelcomeActivity.class);
        }
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_terms_condtions:
                Bundle bundledData = new Bundle();
                bundledData.putInt(Constants.EXTRA_WEB_CONTENT, Constants.SELECTED_WEB_CONTENT.TERMS_AND_CONDITIONS);
                Navigator.getInstance().navigateToActivityWithData(this, TNCActivity.class, bundledData);

                break;

            case R.id.tv_privacy_policy:
                Bundle bundledPrivacyPolicy = new Bundle();
                bundledPrivacyPolicy.putInt(Constants.EXTRA_WEB_CONTENT, Constants.SELECTED_WEB_CONTENT.PRIVACY_POLICY);
                Navigator.getInstance().navigateToActivityWithData(this, TNCActivity.class, bundledPrivacyPolicy);

                break;

            case R.id.btn_sign_in_label:
                if (!mIsSignIn)
                    signInUI();
                break;

            case R.id.btn_sign_up_label:
                if (mIsSignIn)
                    signUpUI();
                break;

            case R.id.tv_forgot_pass:
                //
                // Navigator.getInstance().navigateToActivity(this, ForgotPasswordActivity.class);
                Navigator.getInstance().navigateToActivityForResult(this, ForgotPasswordActivity.class, Constants.REQUEST_CODE.REQUEST_CODE_FORGOT_PASS);
                break;


            case R.id.button_sign_in:
                if (mIsSignIn) {
                    if (isInputValid()) {
                        signInApi();
                    }
                } else {
                    if (isInputValid()) {
                        signUpApi(Constants.SIGN_UP_TYPE.EMAIL, mBinder.etEmail.getText().toString(), null);
                    }
                }

                break;

            case R.id.img_password:
                mIsShowPassword = !mIsShowPassword;
                Utils.showPassword(LoginActivity.this, mBinder.etPassword, mIsShowPassword, mBinder.imgPassword);
                break;

            case R.id.btn_gPlus:
                SocialGoogleHelper.RESULT_CODE_GOOGLE_LOGIN = -2;
                socialMediaHelper.setSocialType(SocialType.GOOGLE);
                socialMediaHelper.initProcess();
                break;

            case R.id.btn_fb:
                try {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                } catch (FacebookSdkNotInitializedException e) {
                    if (e.getMessage() != null)
                        LogUtils.LOGE(TAG, e.getMessage());
                }
                //PreferenceUtil.clearDatabase(this);
                SocialGoogleHelper.RESULT_CODE_GOOGLE_LOGIN = -2;
                socialMediaHelper.setSocialType(SocialType.FB);
                socialMediaHelper.initProcess();
                break;
        }

    }

    private void signInUI() {
        mBinder.layoutBanner.tvTitle.setText(getString(R.string.welcome_back));
        mBinder.layoutBanner.tvInfo.setText(getString(R.string.pls_enter_your_details));

        mBinder.buttonSignIn.setText(getString(R.string.sign_in));

        mBinder.btnSignInLabel.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));
        mBinder.btnSignUpLabel.setBackgroundColor(ContextCompat.getColor(this, R.color.white_color));

        mBinder.tvForgotPass.setVisibility(View.VISIBLE);

        mBinder.etPassword.setHint(getString(R.string.password));
        //mEnteredPassword = "";
        mBinder.etPassword.setText(mEnteredPassword);
        mBinder.etPassword.setSelection(mEnteredPassword.length());

        if (mEnteredEmail.isEmpty())
            mBinder.etEmail.requestFocus();

        mBinder.etEmail.setText(mEnteredEmail);
        mBinder.etEmail.setSelection(mEnteredEmail.length());

        mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));
        mBinder.linePassword.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));

        mIsSignIn = true;

        mBinder.tvBySignUp.setVisibility(View.INVISIBLE);
        mBinder.layoutTermsConditions.setVisibility(View.INVISIBLE);

        Utils.showPassword(LoginActivity.this, mBinder.etPassword, mIsShowPassword, mBinder.imgPassword);
    }

    private void signUpUI() {
        mBinder.layoutBanner.tvTitle.setText(getString(R.string.create_acc));
        mBinder.layoutBanner.tvInfo.setText(getString(R.string.to_continue_you));

        mBinder.buttonSignIn.setText(getString(R.string.create_acc));

        mBinder.btnSignInLabel.setBackgroundColor(ContextCompat.getColor(this, R.color.white_color));
        mBinder.btnSignUpLabel.setBackgroundColor(ContextCompat.getColor(this, R.color.app_btn_green));

        mBinder.tvForgotPass.setVisibility(View.INVISIBLE);

        mBinder.etPassword.setHint(getString(R.string.create_password));
        //mEnteredPassword = "";
        mBinder.etPassword.setText(mEnteredPassword);
        mBinder.etPassword.setSelection(mEnteredPassword.length());

        if (mEnteredEmail.isEmpty())
            mBinder.etEmail.requestFocus();

        mBinder.etEmail.setText(mEnteredEmail);
        mBinder.etEmail.setSelection(mEnteredEmail.length());

        mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));
        mBinder.linePassword.setBackground(ContextCompat.getDrawable(this, R.color.divider_color));

        mIsSignIn = false;

        mBinder.tvBySignUp.setVisibility(View.VISIBLE);
        mBinder.layoutTermsConditions.setVisibility(View.VISIBLE);

        Utils.showPassword(LoginActivity.this, mBinder.etPassword, mIsShowPassword, mBinder.imgPassword);
    }

    @Override
    public String getActivityName() {
        return LoginActivity.class.getSimpleName();
    }

    private boolean isInputValid() {
        boolean isValid = true;

        if (TextUtils.isEmpty(mBinder.etEmail.getText().toString().trim())) {
            isValid = false;

            mBinder.etEmail.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_email));
            //mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_red_color));


        } else if (!Utils.EMAIL_ADDRESS.matcher(mBinder.etEmail.getText().toString().trim()).matches()) {
            isValid = false;
            mBinder.etEmail.requestFocus();
            showSnackbarFromTop(this, getString(R.string.email_invalid));
            //  mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_red_color));

        } else if (TextUtils.isEmpty(mBinder.etPassword.getText().toString().trim())) {
            isValid = false;
            mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));

            mBinder.etPassword.requestFocus();
            showSnackbarFromTop(this, getString(R.string.empty_password));
        } else if (mBinder.etPassword.getText().toString().trim().length() < getResources().getInteger(R.integer.password_min_length)) {
            isValid = false;
            mBinder.lineEmail.setBackground(ContextCompat.getDrawable(this, R.color.divider_green_color));
            mBinder.etPassword.requestFocus();
            showSnackbarFromTop(this, getString(R.string.invalid_password));
        }
        return isValid;
    }

    private void signInApi() {
        processToShowDialog();

        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail(mBinder.etEmail.getText().toString());
        signUpRequest.setPassword(mBinder.etPassword.getText().toString());

        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setDeviceToken(PreferenceUtil.getFCMToken());
        deviceInfo.setDeviceType(Constants.DEVICE_TYPE);
        signUpRequest.setDeviceInfo(deviceInfo);

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.signIn(signUpRequest).enqueue(new BaseCallback<SignInResponse>(LoginActivity.this) {
            @Override
            public void onSuccess(SignInResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        PreferenceUtil.setUserToken(response.getResult().getAccessToken());
                        PreferenceUtil.setUserModel(response.getResult());
                        PreferenceUtil.setIsLogin(true);
                        int currentUserRole = (int) response.getResult().getUserLoggedInType();
                        PreferenceUtil.setCurrentUserRole(currentUserRole);
                        Log.d("mylog", "accessToken : " + response.getResult().getAccessToken()
                                + " user logged in type : " + PreferenceUtil.getCurrentUserRole());

                        if(isForLoginResult) {
                            getProfileStatus();
                        } else {
                            startActivity(new Intent(LoginActivity.this, WelcomeActivity.class));
                            finish();
                        }

                    } else {
                        try {
                            if(response != null && response.getMessage() != null
                                    && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(LoginActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SignInResponse> call, BaseResponse baseResponse) {
                try {
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(LoginActivity.this, baseResponse.getMessage());
                    }
                    hideProgressBar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void signUpApi(final int signupType, String email, String gPlusIdToken) {

        Log.d("fb_token", "fb token : " + gPlusIdToken);

        processToShowDialog();

        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail(mBinder.etEmail.getText().toString());
        signUpRequest.setPassword(mBinder.etPassword.getText().toString());
        signUpRequest.setSignUpType(signupType);
        DeviceInfo deviceInfo = new DeviceInfo();
        //deviceInfo.setDeviceId("gdfdgfd55fdddd");
        deviceInfo.setDeviceToken(PreferenceUtil.getFCMToken());
        deviceInfo.setDeviceType(Constants.DEVICE_TYPE);
        signUpRequest.setDeviceInfo(deviceInfo);


        if (signupType == Constants.SIGN_UP_TYPE.GOOGLE_PLUS) {
            signUpRequest.setEmail(email);
            signUpRequest.setGoogleToken(gPlusIdToken);
        }
        if (signupType == Constants.SIGN_UP_TYPE.FACEBOOK) {
            signUpRequest.setEmail(email);
            signUpRequest.setFacebookToken(gPlusIdToken);
        }

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class);
        webServices.signUp(signUpRequest).enqueue(new BaseCallback<SignUpResponse>(LoginActivity.this) {
            @Override
            public void onSuccess(SignUpResponse response) {
                if(response != null) {
                    if (response.getStatus() == 1) {
                        PreferenceUtil.setUserToken(response.getResult().getAccessToken());
                        if (signupType == Constants.SIGN_UP_TYPE.EMAIL) {
                            Alert.showAlertDialog(LoginActivity.this, getString(R.string.success), response.getMessage(), getString(R.string.ok), LoginActivity.this);
                            mBinder.etEmail.requestFocus();
                            return;
                        }
                        //showSignUpSuccessDialog(response.getMessage());
                        PreferenceUtil.setIsLogin(true);
                        PreferenceUtil.setCurrentUserRole(Constants.USER_ROLE.LEARNER);

                        UserModel userModel = new UserModel();
                        userModel.setAccessToken(response.getResult().getAccessToken());
                        userModel.setEmail(response.getResult().getEmail());
                        userModel.setId(response.getResult().getId());
                        PreferenceUtil.setUserModel(userModel);

                        if(isForLoginResult) {
                            getProfileStatus();
                        } else {
                            Navigator.getInstance().navigateToActivity(LoginActivity.this, WelcomeActivity.class);
                            finish();
                        }
                    } else {
                        try {
                            if(response != null && response.getMessage() != null
                                    && !TextUtils.isEmpty(response.getMessage())) {
                                showSnackbarFromTop(LoginActivity.this, response.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFail(Call<SignUpResponse> call, BaseResponse baseResponse) {
                hideProgressBar();
            }
        });

    }

    private void getProfileStatus() {
        processToShowDialog();

        AuthWebServices webServices = RequestController.createService(AuthWebServices.class, true);
        webServices.getCoachProfileStatus().enqueue(new BaseCallback<CoachProfileStatusResponse>(LoginActivity.this) {
            @Override
            public void onSuccess(CoachProfileStatusResponse response) {
                if (response != null && response.getStatus() == 1) {
                    ProfileStatusResult result = response.getResult();
                    if(result.getIsMobileVerified() == 0) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, true);
                        bundle.putBoolean(Constants.BundleKey.IS_EDIT_MODE_ENABLED, false);
                        Navigator.getInstance().navigateToActivityWithData(LoginActivity.this,
                                VerifyPhoneActivity.class, bundle);
                    } else if (result.getIsLearnerProfileCompleted() == 1) {
                        setResult(RESULT_OK);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.EXTRA_IS_LOGIN_RESULT, true);
                        Navigator.getInstance().navigateToActivityWithData(LoginActivity.this,
                                com.sportsstars.ui.learner.CreateProfileActivity.class, bundle);
                    }
                    finish();
                } else {
                    try {
                        if(response != null && response.getMessage() != null && !TextUtils.isEmpty(response.getMessage())) {
                            showSnackbarFromTop(LoginActivity.this, response.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFail(Call<CoachProfileStatusResponse> call, BaseResponse baseResponse) {
                try {
                    hideProgressBar();
                    if (baseResponse != null && baseResponse.getMessage() != null
                            && !TextUtils.isEmpty(baseResponse.getMessage())) {
                        showSnackbarFromTop(LoginActivity.this, baseResponse.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /* private void showSignUpSuccessDialog(String msg) {
         AlertDialog.Builder builder = new AlertDialog.Builder(this);
         //builder.re(Window.FEATURE_NO_TITLE);


         builder*//*.setTitle(getString(R.string.success))*//*
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Navigator.getInstance().navigateToActivity(LoginActivity.this, WelcomeActivity.class);
                    }
                }).setCancelable(false)

                //.setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
*/
    @Override
    public void onExecute(SocialType provider, Object o) {
        /*if (!New.haveneNetworkConnection(this)) {
            showSnackBar(getString(R.string.internet_error));
            return;
        }*/

        SocialProfile profile = (SocialProfile) o;
        LogUtils.LOGD(TAG, provider.name() + "  " + profile.getProviderId());
        String email = profile.getEmail();
        String id = profile.getProviderId();
        String accessToken = profile.getAccessToken();

        String name = provider.name();
        LogUtils.LOGD(TAG, "email:" + email + "  G+ accessToken::" + accessToken);
        LogUtils.LOGD(TAG, "IdToken:" + profile.getIdToken());

        String src = provider.name();
        if (src != null && src.equalsIgnoreCase("GOOGLE")) {
            signUpApi(Constants.SIGN_UP_TYPE.GOOGLE_PLUS, email, profile.getIdToken());
        } else {
            LogUtils.LOGD(TAG, "fbAcceessToken : " + profile.getAccessToken());
            signUpApi(Constants.SIGN_UP_TYPE.FACEBOOK, email, profile.getAccessToken());
        }

    }


    @Override
    public void onError(SocialAuthError e) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE.REQUEST_CODE_FORGOT_PASS) {
            if (resultCode == RESULT_OK) {
                showSnackbarFromTop(this, getString(R.string.an_email_with_pass));
                return;
            }
        }

        if (socialMediaHelper != null) {
            if(requestCode == SocialGoogleHelper.RC_SIGN_IN) {
                SocialGoogleHelper.RESULT_CODE_GOOGLE_LOGIN = resultCode;
            }
            socialMediaHelper.onActivityResult(requestCode, resultCode, data);
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == 64206)
                return;
            setResult(RESULT_OK);
        }

    }

    @Override
    public void onOkay() {

        mIsSignIn = true;
        signInUI();
    }
}



