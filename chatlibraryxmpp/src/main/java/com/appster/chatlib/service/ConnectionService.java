package com.appster.chatlib.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.constants.AppConstants;
import com.appster.chatlib.task.ConnectionTask;
import com.appster.chatlib.utils.LogUtils;
import com.appster.chatlib.utils.PrefUtils;
import com.appster.chatlib.utils.Utils;


public class ConnectionService extends Service {

    public final static String STOP_CONNECTION_SERVICE = "stopConnectionService";
    public final static String DISCONNECT_CONNECTION_SERVICE = "disconnectConnectionService";
    IBinder mBinder = new LocalBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        ChatController.getInstance().onServiceCreated();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.LOGD("TAG", "in onStartCommand");
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(STOP_CONNECTION_SERVICE) || intent.getAction().equals(DISCONNECT_CONNECTION_SERVICE)) {
                if (ChatController.connection != null) {
                    try {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    ChatController.connection.disconnect();
                                } catch (Exception ex) {
                                    LogUtils.LOGE("ConnectionService", ex.toString());
                                }
                                Utils.destroyConnectionObject();
                                System.gc();

                            }
                        }).start();

                        if (intent.getAction().equals(STOP_CONNECTION_SERVICE)) {
                            LogUtils.LOGD("TAG", "ConnectionService disconnected.");
                            stopSelf();
                        } else {
                            LogUtils.LOGD("TAG", "ConnectionService stopped.");
                        }
                    } catch (Exception e) {
                        LogUtils.LOGE(">>>", e.toString());

                    }
                }

                return START_NOT_STICKY;
            }
        }


        String userName;
        String password;
        if (intent == null) {
            userName = Utils.getUserNameFromJabberId(PrefUtils.getUsername());
            password = PrefUtils.getPassword();
        } else {
            userName = intent.getStringExtra(AppConstants.USERNAME);
            password = intent.getStringExtra(AppConstants.PASSWORD);
        }
        if (ChatController.getInstance().canReconnect()) {
            ChatController.getInstance().onServiceStarted();
            validatedToInitService(userName, password);
        }

        return START_NOT_STICKY;
    }

    private void validatedToInitService(String userName, String password) {
        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password)) {
            return;
        }

        if (ChatController.connectionTask != null) {
            ChatController.connectionTask.cancel(true);
        }ChatController.connectionTask = new ConnectionTask(userName, password);
        ChatController.connectionTask.execute();

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        LogUtils.LOGD("ConnectionService", "Connection services destroyed");
        ChatController.getInstance().onServiceDestroyed();
        super.onDestroy();
    }

    public class LocalBinder extends Binder {
        public ConnectionService getServiceInstance() {
            return ConnectionService.this;
        }
    }

    public static Intent createIntent(Context context) {
        return new Intent(context, ConnectionService.class);
    }

}