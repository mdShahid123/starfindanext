/**
 * Copyright (c) 2013, Redsolution LTD. All rights reserved.
 * <p>
 * This file is part of Xabber project; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License, Version 3.
 * <p>
 * Xabber is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License,
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */
package com.appster.chatlib.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.appster.chatlib.connection.NetworkManager;
import com.appster.chatlib.utils.LogUtils;

/**
 * Receiver for network events.
 */
public class ConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction()))
            return;
        NetworkInfo networkInfo = intent
                .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        if (networkInfo == null) {
            LogUtils.LOGE("ConnReceiver", "NO INFO");
            return;
        }
        NetworkManager.getInstance().onNetworkChange(networkInfo);
    }

}