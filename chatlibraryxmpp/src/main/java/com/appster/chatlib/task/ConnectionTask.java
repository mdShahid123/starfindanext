package com.appster.chatlib.task;

import android.os.AsyncTask;

import com.appster.chatlib.connection.Connection;
import com.appster.chatlib.utils.LogUtils;
import com.appster.chatlib.utils.PrefUtils;

// Create connection in background
public class ConnectionTask extends AsyncTask<Void, Void, Void> {
    String username, password;

    public ConnectionTask(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {

            Connection.createConnection(username, password,
                    PrefUtils.getHost(), PrefUtils.getPort(), PrefUtils.getServiceName());
        } catch (Exception ex) {
            LogUtils.LOGE("XMPPConTask", ex.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}