package com.appster.chatlib.servicemanagers;

import android.content.Context;
import android.text.TextUtils;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.connection.ConnectionManager;
import com.appster.chatlib.constants.AppConstants;
import com.appster.chatlib.messagecomposer.MessageData;
import com.appster.chatlib.messagecomposer.MessageExtension;
import com.appster.chatlib.utils.IdConvertorUtils;
import com.appster.chatlib.utils.LogUtils;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.util.StringUtils;

/* SUCManager stands for :  Single User Chat Manager

 */
public class SUCManager {

    private static final String TAG = "SUCManager";


    private Chat chat;
    private String toUserJid;

    public SUCManager(String toUserJid) {
        this.toUserJid = toUserJid;
        chat = chatWith(toUserJid);
    }

    protected Chat chatWith(String to_user) {

        LogUtils.LOGD("SATRT CHAT", "IMCHATMANAGER" + to_user);
        if (ChatController.chatmanager == null) {
            if (ChatController.connection != null)
                ChatController.chatmanager = ChatManager.getInstanceFor(ChatController.connection);
        }
        if (ChatController.chatmanager == null) return null;
        try {
            LogUtils.LOGD(SUCManager.class.getName() + ":chatWith","INITIATED");
            return ChatController.chatmanager.chatWith(IdConvertorUtils.convertTextToEntityBareJid(to_user));

        } catch (Exception e) {
            LogUtils.LOGE(SUCManager.class.getName() + ":chatWith", e.getMessage());

        }
        return null;
    }

    private String isMessageDataValid(MessageData messageData) {
        String msg = "";
        if (messageData != null) {
            if (TextUtils.isEmpty(messageData.getToJid())) {
                msg = "toJid cannot be empty";
            } else if (messageData.getMediaType() == null) {
                msg = "mediaType is mandatory";
            } else if (messageData.getMediaType() == MessageData.MediaType.text) {
                if (TextUtils.isEmpty(messageData.getMsgText())) {
//                    msg = "Body cannot be empty";
                }

            } else if (messageData.getMediaType() == MessageData.MediaType.image) {
                if (TextUtils.isEmpty(messageData.getImageUrl()) || TextUtils.isEmpty(messageData.getImageUrlThumb())) {
                    msg = "media message must contain media --> imageUrl and imageUrlThumb are must)";
                }
            }
        } else {
            msg = "cannot send null data as message";
        }
        return msg;
    }


    public MessageData sendMessage(Context context, MessageData messageData) throws Exception {
        String validateMsg = isMessageDataValid(messageData);
        String exceptionMsg = SUCManager.class.getName() + " >>> sendMessage() : Cannot send message, \n" + validateMsg;
        if (!TextUtils.isEmpty(validateMsg)) {
            throw new Exception(exceptionMsg);
        }
        if (chat == null) {
            chat = chatWith(messageData.getToJid());
        }

        Message message = new Message();
        message.setType(Message.Type.chat);
        messageData.setChatType(message.getType());

        MessageExtension msgExt = null;
        if (messageData.getMsgExt() != null) {
            msgExt = messageData.getMsgExt();
        } else {
            msgExt = new MessageExtension();
            messageData.setMsgExt(msgExt);
        }


        if (!TextUtils.isEmpty(messageData.getStanzaId()))
            message.setStanzaId(messageData.getStanzaId());

        messageData.setStanzaId(message.getStanzaId());

        if (messageData.getMediaType() != null) {
            msgExt.addElement(MessageExtension.ELEMENT_MEDIA_TYPE, messageData.getMediaType().toString());
            if (messageData.getMediaType() == MessageData.MediaType.text) {
                msgExt.addElement(MessageExtension.ELEMENT_URL, "")
                        .addElement(MessageExtension.ELEMENT_URL_THUMB, "");
                message.setBody(messageData.getMsgText());
            } else if (messageData.getMediaType() == MessageData.MediaType.image) {
                msgExt.addElement(MessageExtension.ELEMENT_URL, messageData.getImageUrl())
                        .addElement(MessageExtension.ELEMENT_URL_THUMB, messageData.getImageUrlThumb());
                message.setBody(messageData.getMsgText());
            } else if (messageData.getMediaType() == MessageData.MediaType.video) {
                msgExt.addElement(MessageExtension.ELEMENT_URL, messageData.getImageUrl())
                        .addElement(MessageExtension.ELEMENT_URL_THUMB, messageData.getImageUrlThumb());
                message.setBody(messageData.getMsgText());
            } else if (messageData.getMediaType() == MessageData.MediaType.doc) {
                msgExt.addElement(MessageExtension.ELEMENT_URL, messageData.getImageUrl())
                .addElement(MessageExtension.ELEMENT_URL_LOCAL, messageData.getImageUrlLocal())
                        .addElement(MessageExtension.ELEMENT_FILE_NAME, messageData.getFileName())
                        .addElement(MessageExtension.ELEMENT_FILE_TYPE, messageData.getFileType());
                message.setBody(messageData.getMsgText());
            }
        }

        //If auto receipt is enable then we add auto receipt tag in message
        if (DeliveryManager.getInstance().isReceiptEnable()) {
            DeliveryManager.getInstance().addDeliverReceiptRequest(messageData);
        }

        msgExt.addElement(MessageExtension.ELEMENT_KEY_MESSAGE_DATAT_YPE, MessageExtension.ELEMENT_CHAT_NORMAL);
        message.addExtension(msgExt.build());
        boolean success = true;
        try {
            chat.send(message);
        } catch (SmackException.NotConnectedException e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
            ConnectionManager.restartConnection();
        } catch (Exception e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
        }

        if (!ChatController.getInstance().isAuthenticated()) {
            success = false;
        }

        if (!success) {
            message.addSubject(MessageExtension.ELEMENT_MSG_STATUS, MessageData.MsgStatus.unsent.getValue() + "");
            messageData.setMsgStatus(MessageData.MsgStatus.unsent.getValue());

        } else {
            message.addSubject(MessageExtension.ELEMENT_MSG_STATUS, MessageData.MsgStatus.sent.getValue() + "");
            messageData.setMsgStatus(MessageData.MsgStatus.sent.getValue());
        }
        return messageData;

    }


    public MessageData sendAcknowledgementDelivered(Context context, MessageData messageData) throws Exception {
        String validateMsg = isMessageDataValid(messageData);
        String exceptionMsg = SUCManager.class.getName() + " >>> sendMessage() : Cannot send message, \n" + validateMsg;
        if (!TextUtils.isEmpty(validateMsg)) {
            throw new Exception(exceptionMsg);
        }
        if (chat == null) {
            chat = chatWith(messageData.getToJid());
        }

        Message message = new Message();
        message.setType(Message.Type.chat);
        messageData.setChatType(message.getType());

        MessageExtension msgExt = null;
        if (messageData.getMsgExt() != null) {
            msgExt = messageData.getMsgExt();
        } else {
            msgExt = new MessageExtension();
        }

        msgExt.addElement( MessageExtension.ELEMENT_DELIVERED,"");
        msgExt.addElement(MessageExtension.ELEMENT_STANZA_ID, messageData.getStanzaId());
        msgExt.addElement(AppConstants.MESSAGE_ID, messageData.getStanzaId());

//        msgExt.addElement(AppConstants.MESSAGE_ID, messageData.getMsgExt().getElementValue(AppConstants.MESSAGE_ID));

//        <message type="chat"
//        to="user437starfinda@ec2-13-211-46-213.ap-southeast-2.compute.amazonaws.com"
//        from="user436starfinda@ec2-13-211-46-213.ap-southeast-2.compute.amazonaws.com/user436starfinda@ec2-13-211-46-213.ap-southeast-2.compute.amazonaws.com">
//        <data xmlns="data:jabber">
//        <delivered/>
//        <messageId>t72D9-39</messageId>
//        </data>
//        </message>
        messageData.setStanzaId(message.getStanzaId());
//        message.setBody(messageData.getMsgText());
LogUtils.LOGD(">>>>>>>>","STANZA ID : "+message.getStanzaId());
        message.addExtension(msgExt.build());
        boolean success = true;
        try {
            chat.send(message);
        } catch (SmackException.NotConnectedException e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
            ConnectionManager.restartConnection();
        } catch (Exception e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
        }

        if (!ChatController.getInstance().isAuthenticated()) {
            success = false;
        }

        if (!success) {
            message.addSubject(MessageExtension.ELEMENT_MSG_STATUS, MessageData.MsgStatus.unsent.getValue() + "");
            messageData.setMsgStatus(MessageData.MsgStatus.unsent.getValue());

        } else {
            message.addSubject(MessageExtension.ELEMENT_MSG_STATUS, MessageData.MsgStatus.sent.getValue() + "");
            messageData.setMsgStatus(MessageData.MsgStatus.sent.getValue());
        }
        return messageData;

    }


    public MessageData sendAcknowledgementRead(Context context, MessageData messageData) throws Exception {

        String validateMsg = isMessageDataValid(messageData);
        String exceptionMsg = SUCManager.class.getName() + " >>> sendMessage() : Cannot send message, \n" + validateMsg;
        if (!TextUtils.isEmpty(validateMsg)) {
            throw new Exception(exceptionMsg);
        }
        if (chat == null) {
            chat = chatWith(messageData.getToJid());
        }

        Message message = new Message();
        message.setType(Message.Type.chat);
        messageData.setChatType(message.getType());

        MessageExtension msgExt = null;
        if (messageData.getMsgExt() != null) {
            msgExt = messageData.getMsgExt();
        } else {
            msgExt = new MessageExtension();
        }
        msgExt.addElement(AppConstants.MESSAGE_ID, messageData.getStanzaId());
        msgExt.addElement(MessageExtension.ELEMENT_DISPLAYED, "");
        msgExt.addElement(MessageExtension.ELEMENT_STANZA_ID, messageData.getStanzaId());

        messageData.setStanzaId(message.getStanzaId());
//        message.setBody(messageData.getMsgText());

        message.addExtension(msgExt.build());
        boolean success = true;
        try {
            chat.send(message);
        } catch (SmackException.NotConnectedException e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
            ConnectionManager.restartConnection();
        } catch (Exception e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
        }

        if (!ChatController.getInstance().isAuthenticated()) {
            success = false;
        }

        if (!success) {
            message.addSubject(MessageExtension.ELEMENT_MSG_STATUS, MessageData.MsgStatus.unsent.getValue() + "");
            messageData.setMsgStatus(MessageData.MsgStatus.unsent.getValue());

        } else {
            message.addSubject(MessageExtension.ELEMENT_MSG_STATUS, MessageData.MsgStatus.sent.getValue() + "");
            messageData.setMsgStatus(MessageData.MsgStatus.sent.getValue());
        }
        return messageData;

    }


    public boolean informMessageRead(String toJid,String fromJid, String msgId, String msgBody) {

        if (StringUtils.isNullOrEmpty(fromJid))
            return false;

        boolean success = true;

        if (chat == null) {
            chat = chatWith(fromJid);
        }

        Message message = new Message();
        message.setType(Message.Type.chat);

        MessageExtension msgExt = null;
            msgExt = new MessageExtension();
        msgExt.addElement(AppConstants.MESSAGE_ID, msgId);
        msgExt.addElement(MessageExtension.ELEMENT_MEDIA_TYPE, "text");
        msgExt.addElement(MessageExtension.ELEMENT_DISPLAYED, "");
        msgExt.addElement(MessageExtension.ELEMENT_STANZA_ID, msgId);

//        message.setBody(msgBody);

        message.addExtension(msgExt.build());

        try {
            chat.send(message);
        } catch (SmackException.NotConnectedException e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
            ConnectionManager.restartConnection();
        } catch (Exception e) {
            success = false;
            LogUtils.LOGE(TAG, e.toString());
        }

        if (!ChatController.getInstance().isAuthenticated()) {
            success = false;
        }

        return success;

    }

}