package com.appster.chatlib.servicemanagers;

import android.content.Context;
import android.text.TextUtils;

import com.appster.chatlib.messagecomposer.MessageExtension;
import com.appster.chatlib.utils.LogUtils;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.chatstates.ChatState;

/**
 * Created by neeraj on 12/07/17.
 */

/**
 * TSManager stands for : Typing Status Manager
 */
public class TSManager {

    private Context context;
    private String toUserJid;
    private String myJid;
    private MessageExtension messageExtension;
    SUCManager sucManager;


    public TSManager(Context context, String toUserJid) {
        this.context = context;
        this.toUserJid = toUserJid;
        sucManager = new SUCManager(toUserJid);

    }

    public TSManager(Context context, String toUserJid, String myJid) {
        this(context, toUserJid);
        this.myJid = myJid;

    }

    public TSManager(Context context, String toUserJid, MessageExtension msgExtension) {
        this(context, toUserJid, "");
        messageExtension = msgExtension;
        addBasicData(messageExtension, null);

    }

    public TSManager(Context context, String toUserJid, String myJid, MessageExtension msgExtension) {
        this(context, toUserJid, myJid);
        messageExtension = msgExtension;
        addBasicData(messageExtension, null);

    }

    private void addBasicData(MessageExtension messageExtension, ChatState chatState) {
        if (messageExtension != null) {
            messageExtension.addElement(MessageExtension.ELEMENT_KEY_MESSAGE_DATAT_YPE, MessageExtension.ELEMENT_TYPING_MESSAGE);
            messageExtension.addElement(MessageExtension.ELEMENT_TYPING_STATUS, chatState == null ? ChatState.composing.toString() : chatState.toString());
            messageExtension.addElement(MessageExtension.ELEMENT_TYPING_STATUS_TO_JID, toUserJid);

            if (!TextUtils.isEmpty(myJid)) {
                messageExtension.addElement(MessageExtension.ELEMENT_TYPING_STATUS_COMPOSER_JID, myJid);
            }
        }
    }

    public void sendComposingStatus() {

        Chat chat = sucManager.chatWith(toUserJid);
        Message message = new Message();
        MessageExtension msgExt = null;
        if (messageExtension == null) {
            msgExt = new MessageExtension();
            addBasicData(msgExt, null);
        } else {
            msgExt = messageExtension;
        }
        message.addExtension(msgExt.build());
        try {
            chat.send(message);
        } catch (SmackException.NotConnectedException e) {
            LogUtils.LOGE(SUCManager.class.getName(), e.getMessage());
        } catch (Exception ex) {
            LogUtils.LOGE(SUCManager.class.getName(), ex.getMessage());
        }

    }


    public void stopComposingStatus() {

        Chat chat = sucManager.chatWith(toUserJid);
        Message message = new Message();
        MessageExtension msgExt = null;
        if (messageExtension == null) {
            msgExt = new MessageExtension();
            addBasicData(msgExt, ChatState.paused);
        } else {
            msgExt = messageExtension;
        }
        message.addExtension(msgExt.build());
        try {
            chat.send(message);
        } catch (SmackException.NotConnectedException e) {
            LogUtils.LOGE(SUCManager.class.getName(), e.getMessage());
        } catch (Exception ex) {
            LogUtils.LOGE(SUCManager.class.getName(), ex.getMessage());
        }
    }

}
