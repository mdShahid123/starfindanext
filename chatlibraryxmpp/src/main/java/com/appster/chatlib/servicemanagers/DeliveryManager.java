package com.appster.chatlib.servicemanagers;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.messagecomposer.MessageData;
import com.appster.chatlib.messagecomposer.MessageExtension;

/**
 * Created by neeraj on 17/07/17.
 */

public class DeliveryManager {


    private static DeliveryManager deliveryManager;
    private boolean autoDeliveryReceipt = false;

    //Enable --> We request to receiver to send delivery report
    //Disable -->
    private boolean isReceiptEnable = false;


    public static DeliveryManager getInstance() {
        if (deliveryManager == null) {
            deliveryManager = new DeliveryManager();
        }
        return deliveryManager;
    }

    public boolean isReceiptEnable() {
        return isReceiptEnable;
    }

    public void setReceiptEnable(boolean receiptEnable) {
        isReceiptEnable = receiptEnable;
    }

    public boolean isAutoDeliveryReceipt() {
        return autoDeliveryReceipt;
    }

    public void setAutoDeliveryReceipt(boolean autoDeliveryReceipt) {
        this.autoDeliveryReceipt = autoDeliveryReceipt;
    }

    //Add auto receipt tag in message data extension
    public void addDeliverReceiptRequest(MessageData messageData) {
        if (messageData != null && messageData.getMsgExt() != null) {
            messageData.getMsgExt().addElement(MessageExtension.ELEMENT_RECEIPT_REQUEST, MessageExtension.ELEMENT_RECEIPT_REQUEST);
        }

    }

    /**
     * This method is used to send delivery report automatically to sender of message
     *
     * @param messageData
     * @throws Exception
     */
    public void sendAutoDeliveryReport(MessageData messageData) throws Exception {
        SUCManager sucManager = new SUCManager(messageData.getToJid());
        sucManager.sendAcknowledgementDelivered(ChatController.getAppContext(),messageData);
    }

    public void sendAutoReadReport(MessageData messageData) throws Exception {
        SUCManager sucManager = new SUCManager(messageData.getToJid());
        sucManager.sendAcknowledgementRead(ChatController.getAppContext(),messageData);
    }

    public boolean sendReadReport(String toJid,String fromJid, String msgId, String msgBody) throws Exception {
        SUCManager sucManager = new SUCManager(fromJid);
        return sucManager.informMessageRead(toJid,fromJid,msgId,msgBody);
    }


}
