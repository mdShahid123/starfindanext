package com.appster.chatlib.utils;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

/**
 * Created by neeraj on 04/07/17.
 */

public class IdConvertorUtils {

    //This method is used to convert string ID into EntityJid object, Smack 4.2.0 does not allow to pass string JIDs
    public static EntityJid convertTextToEntityJid(String jidString) {
        try {
            return JidCreate.entityBareFrom(jidString);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static EntityBareJid convertTextToEntityBareJid(String jidString) {
        try {
            return JidCreate.entityBareFrom(jidString);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertJidToStringBareJid(Jid jid) {
        if (jid != null) {
            return jid.asBareJid().toString();
        }
        return null;

    }

    public static String convertJidToStringJid(Jid jid) {
        if (jid != null) {
            return jid.toString();
        }
        return null;

    }
}
