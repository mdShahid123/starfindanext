package com.appster.chatlib.utils;

import com.orhanobut.hawk.Hawk;

/**
 * Created by neeraj on 03/07/17.
 */

public class PrefUtils {

    public static final String KEY_USERNAME = "USERNAME";
    public static final String KEY_JID = "JID";
    public static final String KEY_PASSWORD = "PASSWORD";

    public static final String KEY_HOST = "HOST";

    public static final String KEY_PORT = "PORT";

    public static final String KEY_SERVICE_NAME = "SERVICE_NAME";

    public static final String KEY_SETUP_DONE = "SETUP_DONE";


    public static void setUserName(String userName) {
        Hawk.put(KEY_USERNAME, userName);
    }

    public static String getUsername() {
        return Hawk.get(KEY_USERNAME, "");
    }

    public static void setJId(String jid) {
        Hawk.put(KEY_JID, jid);
    }

    public static String getJid() {
        return Hawk.get(KEY_JID, "");
    }

    public static void setPassword(String password) {
        Hawk.put(KEY_PASSWORD, password);
    }

    public static String getPassword() {
        return Hawk.get(KEY_PASSWORD, "");
    }

    public static void setHost(String host) {
        Hawk.put(KEY_HOST, host);
    }

    public static String getHost() {
        return Hawk.get(KEY_HOST, "");
    }

    public static void setPort(int port) {
        Hawk.put(KEY_PORT, port);
    }

    public static int getPort() {
        return Hawk.get(KEY_PORT, 5222);
    }


    public static void setServiceName(String serviceName) {
        Hawk.put(KEY_SERVICE_NAME, serviceName);
    }

    public static String getServiceName() {
        return Hawk.get(KEY_SERVICE_NAME, "localhost");
    }

    public static void setSetupDone(boolean isSetupDone) {
        Hawk.put(KEY_SETUP_DONE, isSetupDone);
    }

    public static boolean isSetupDone() {
        return Hawk.get(KEY_SETUP_DONE, false);
    }
}
