package com.appster.chatlib.utils;

import android.content.Context;
import android.widget.Toast;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.connection.MyXmppConnectionBuilder;
import com.appster.chatlib.constants.AppConstants;

import org.jivesoftware.smack.util.StringUtils;

/**
 * Created by neeraj on 03/07/17.
 */

public class Utils {

    //Get user name from jabber id
    public static String getUserNameFromJabberId(String jabberId) {
        if (!StringUtils.isNullOrEmpty(jabberId)) {
            if (jabberId.contains("@")) {
                return jabberId.split("@")[0];
            }
            return jabberId;
        }
        return jabberId;
    }

    public static void destroyConnectionObject() {
        ChatController.connection = null;
        ChatController.destroyAllStaticVariables();
        MyXmppConnectionBuilder.getInstance().destroyConnectionObject();

    }

    public static boolean isPerfectJabberId(String jabberId) {
        if (jabberId != null && jabberId.length() > 0) {
            if (jabberId.contains(AppConstants.jabberIdSuffix))
                return true;
        }
        return false;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }




}
