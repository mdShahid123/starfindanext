package com.appster.chatlib.utils;

import android.os.Handler;

import com.appster.chatlib.listener.OnCloseListener;
import com.appster.chatlib.listener.OnInitializedListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by neeraj on 17/07/17.
 */

public class TaskRunnerManager implements OnCloseListener, OnInitializedListener {
    private final String TAG = "TaskRunnerManager";

    /**
     * Thread to execute tasks in background..
     */
    private  ExecutorService backgroundExecutor;
    /**
     * Handler to execute runnable in UI thread.
     */
    private  Handler handler;

    private static TaskRunnerManager taskRunnerManager;

    private static boolean isInitialized = false;

    static {
        taskRunnerManager = new TaskRunnerManager();
    }

    private TaskRunnerManager() {

    }

    public static TaskRunnerManager getInstance() {
        return taskRunnerManager;
    }


    /**
     * Submits request to be executed in background.
     */
    public void runInBackground(final Runnable runnable) {
        backgroundExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } catch (Exception e) {
                    LogUtils.LOGE(TAG, e.toString());
                }
            }
        });
    }

    /**
     * Submits request to be executed in UI thread.
     */
    public void runOnUiThread(final Runnable runnable) {
        handler.post(runnable);
    }

    /**
     * Submits request to be executed in UI thread.
     */
    public void runOnUiThreadDelay(final Runnable runnable, long delayMillis) {
        handler.postDelayed(runnable, delayMillis);
    }

    @Override
    public void onClose() {
        isInitialized = false;
        if(backgroundExecutor!=null && !backgroundExecutor.isShutdown()){
            backgroundExecutor.shutdown();
        }
    }

    @Override
    public void onInitialized() {
        if (!isInitialized) {
            handler = new Handler();
            backgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
                @Override
                public Thread newThread(Runnable runnable) {
                    Thread thread = new Thread(runnable, "Background executor service");
                    thread.setPriority(Thread.MIN_PRIORITY);
                    thread.setDaemon(true);
                    return thread;
                }
            });
            isInitialized = true;
        }
    }
}
