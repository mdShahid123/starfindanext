package com.appster.chatlib.utils;

/**
 * Created by nishant on 13/12/16.
 */

import android.util.Log;

import com.appster.chatlib.BuildConfig;

import org.jivesoftware.smack.util.StringUtils;


public class LogUtils {
    private static final String LOG_PREFIX = "your app name";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    private LogUtils() {
    }

    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }

        return LOG_PREFIX + str;
    }

    /**
     * Don't use this when obfuscating class names!
     */
    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }

    public static void LOGD(final String tag, String message) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG || Log.isLoggable("STARFINDA", Log.DEBUG)) {
            if (!StringUtils.isNullOrEmpty(message))
                Log.d("STARFINDA", message);
        }
    }

    public static void LOGD(final String tag, String message, Throwable cause) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG || Log.isLoggable("STARFINDA", Log.DEBUG)) {
            if (!StringUtils.isNullOrEmpty(message))
                Log.d("STARFINDA", message, cause);
        }
    }

    public static void LOGV(final String tag, String message) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG && Log.isLoggable("STARFINDA", Log.VERBOSE)) {
            Log.v("STARFINDA", message);
        }

    }

    public static void LOGV(final String tag, String message, Throwable cause) {
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (BuildConfig.DEBUG && Log.isLoggable("STARFINDA", Log.VERBOSE)) {
            Log.v("STARFINDA", message, cause);
        }
    }

    public static void LOGI(final String tag, String message) {
        if (!StringUtils.isNullOrEmpty(message))
            Log.i("STARFINDA", message);
    }

    public static void LOGI(final String tag, String message, Throwable cause) {
        if (!StringUtils.isNullOrEmpty(message))
            Log.i("STARFINDA", message, cause);
    }

    public static void LOGW(final String tag, String message) {
        if (!StringUtils.isNullOrEmpty(message))
            Log.w("STARFINDA", message);
    }

    public static void LOGW(final String tag, String message, Throwable cause) {
        if (!StringUtils.isNullOrEmpty(message))
            Log.w("STARFINDA", message, cause);
    }

    public static void LOGE(final String tag, String message) {
        if (!StringUtils.isNullOrEmpty(message)) {
            Log.e("STARFINDA", message);
            /*if(!BuildConfig.DEBUG)
                Crashlytics.log(Log.ERROR, tag, message);*/
        }
    }

    public static void LOGE(final String tag, String message, Throwable cause) {
        if (cause != null && !StringUtils.isNullOrEmpty(message)) {
            Log.e("STARFINDA", message, cause);
            /*if(!BuildConfig.DEBUG) {
                Crashlytics.log(Log.ERROR, tag, message);
                Crashlytics.logException(cause);
            }*/
        }
    }

    public static void printStackTrace(Throwable e) {
        e.printStackTrace();
    }
}