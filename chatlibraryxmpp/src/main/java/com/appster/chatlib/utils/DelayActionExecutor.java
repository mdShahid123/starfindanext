package com.appster.chatlib.utils;

import android.os.Handler;
import android.os.Looper;

//This class is used to perform delay action or post Delay
public class DelayActionExecutor {

    private Handler handler;
    private long DEFAULT_WAIT_TIME = 5000;

    //Start to wait and trigger action afte given waitTime
    public void startOnUI(final OnExecution onExecution, long waitTime) {
        handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (onExecution != null) {
                    onExecution.onExecute();
                }
            }
        }, waitTime);
    }

    //Start to wait and trigger action afte default wait time
    public void startOnUI(final OnExecution onExecution) {
        handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (onExecution != null) {
                    onExecution.onExecute();
                }
            }
        }, DEFAULT_WAIT_TIME);
    }


    public interface OnExecution {
        void onExecute();
    }

}
