package com.appster.chatlib.listener;

import com.appster.chatlib.messagecomposer.MessageData;

/**
 * Created by neeraj on 18/07/17.
 */

public interface MessageListener {

    void onMessageReceived(MessageData messageData);
}
