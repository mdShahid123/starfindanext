package com.appster.chatlib.listener;

import android.text.TextUtils;

import com.appster.chatlib.messagecomposer.MessageConvertor;
import com.appster.chatlib.messagecomposer.MessageData;
import com.appster.chatlib.messagecomposer.MessageExtension;
import com.appster.chatlib.servicemanagers.DeliveryManager;
import com.appster.chatlib.utils.LogUtils;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.util.StringUtils;


/**
 * Created by neeraj on 13/07/17.
 */

public class IncomingMessageListener implements StanzaListener {

    MessageListener messageListener;
    private static IncomingMessageListener incomingMessageListener;

    static {
        incomingMessageListener = new IncomingMessageListener();

//        XMPPConnectionRegistry.addConnectionCreationListener(new ConnectionCreationListener() {
//            @Override
//            public void connectionCreated(final XMPPConnection connection) {
//                ChatController.getInstance().setAllMessageListeners();
//            }
//        });
    }

    private IncomingMessageListener() {
    }

    public static IncomingMessageListener getInstance() {
        if (incomingMessageListener == null) {
            incomingMessageListener = new IncomingMessageListener();
        }
        return incomingMessageListener;
    }


    @Override
    public void processStanza(Stanza packet) throws SmackException.NotConnectedException, InterruptedException {
        Message message = (Message) packet;

        LogUtils.LOGD("IML", "Message received >>>" + message.toString());
        MessageData messageData = MessageConvertor.smackMessagetoMessageData(message);

        if (messageListener != null) {
            messageListener.onMessageReceived(messageData);
        }


//        if (isReceiptRequested(messageData) && DeliveryManager.getInstance().isAutoDeliveryReceipt()) {
            try {
                if (isChatMessage(messageData)) {

//                    if (ChatController.isChatScreenActive && Univer.currentChatjabberID.equals(toJid)) {
//                        element.setValue(AppConstants.ELEMENT_DISPLAYED, "");
//                        new RealmController().updateMsgToReadStatusSent(realm, toJid, msgId, fromJid);
//
//                    } else {
//                        element.setValue(AppConstants.ELEMENT_DELIVERED, "");
//                        if (!isPreviouseMsg)
//                            new RealmController().updateUnreadStatus(realm, toJid);
//                    }
//                    MessageData deliverMsgData = new MessageData(messageData.getFromJid(), "",MessageData.MessageType.sender);
//                    deliverMsgData.setStanzaId(messageData.getStanzaId());
//                    DeliveryManager.getInstance().sendAutoDeliveryReport(deliverMsgData);

                }
                else if (isGroupMessage(messageData)) {

                    String messageCreatorJid = messageData.getCustomValue(MessageExtension.ELEMENT_MSG_CREATOR_JID);
                    String groupJid = messageData.getCustomValue(MessageExtension.ELEMENT_GROUP_JID);
                    MessageData deliverMsgData = new MessageData(messageCreatorJid, "", MessageData.MessageType.sender);
                    deliverMsgData.getMsgExt().addElement(MessageExtension.ELEMENT_GROUP_JID, groupJid);
                    deliverMsgData.setStanzaId(messageData.getStanzaId());
                    DeliveryManager.getInstance().sendAutoDeliveryReport(messageData);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static boolean isTypingStatus(MessageData messageData) {
        return isContainDataType(messageData, MessageExtension.ELEMENT_TYPING_MESSAGE);
    }

    public static boolean isErrorMessage(MessageData messageData) {
        return messageData.getChatType() == Message.Type.error;
    }

    public static boolean isGroupMessage(MessageData messageData) {
        return isContainDataType(messageData, MessageExtension.ELEMENT_GROUP_CHAT_NORMAL);
    }

    public static boolean isChatMessage(MessageData messageData) {
        return isContainDataType(messageData, MessageExtension.ELEMENT_CHAT_NORMAL);
    }

    public static boolean isDeliveryReport(MessageData messageData) {
        return isContainElement(messageData, MessageExtension.ELEMENT_DELIVERED);
    }

    public static boolean isReadReport(MessageData messageData) {
        return isContainElement(messageData, MessageExtension.ELEMENT_DISPLAYED);
    }

    public static boolean isReceiptRequested(MessageData messageData){
        return isContainElement(messageData,MessageExtension.ELEMENT_RECEIPT_REQUEST);
    }

//    public static boolean isContainDataType(MessageData messageData, String elementValue) {
//
//        if (messageData == null || TextUtils.isEmpty(elementValue) || messageData.getMsgExt() == null || messageData.getMsgExt().getElementValue(MessageExtension.ELEMENT_MEDIA_TYPE) == null) {
//            return false;
//        } else if (messageData.getMsgExt().getElementValue(MessageExtension.ELEMENT_KEY_MESSAGE_DATAT_YPE)
//                .equals(elementValue)) {
//            return true;
//        }
//        return false;
//    }

    public static boolean isContainDataType(MessageData messageData, String elementValue) {

        if (messageData == null || TextUtils.isEmpty(elementValue) || StringUtils.isNullOrEmpty(messageData.getMsgText())|| messageData.getMsgExt() == null || messageData.getChatType() == null) {
            return false;
        } else if (messageData.getChatType().toString()
                .equals(elementValue)) {
            return true;
        }
        return false;
    }

    public static boolean isContainElement(MessageData messageData, String element) {

        if (messageData == null || TextUtils.isEmpty(element) || messageData.getMsgExt() == null) {
            return false;
        } else if (messageData.getMsgExt().getElementValue(element)!=null) {
            return true;
        }
        return false;
    }

    public void setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

}
