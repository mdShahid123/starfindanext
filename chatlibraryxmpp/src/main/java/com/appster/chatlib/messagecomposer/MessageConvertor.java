package com.appster.chatlib.messagecomposer;

import com.appster.chatlib.utils.IdConvertorUtils;
import com.appster.chatlib.utils.LogUtils;
import com.appster.chatlib.utils.PrefUtils;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.StandardExtensionElement;

import java.util.List;

/**
 * Created by neeraj on 14/07/17.
 */

public class MessageConvertor {

    public static MessageData smackMessagetoMessageData(Message smackMessage) {
        MessageData messageData = null;
        if (smackMessage != null) {

            MessageData.MessageType messageType = isMsgSentByMe(smackMessage)? MessageData.MessageType.sender: MessageData.MessageType.receiver;
            messageData = new MessageData(IdConvertorUtils.convertJidToStringBareJid(smackMessage.getTo()), smackMessage.getBody(),messageType);
            messageData.setChatType(smackMessage.getType());
            messageData.setFromJid(IdConvertorUtils.convertJidToStringBareJid(smackMessage.getFrom()));
            messageData.setStanzaId(smackMessage.getStanzaId());

            StandardExtensionElement stdExtElement = smackMessage.getExtension(MessageExtension.ELEMENT_DATA, MessageExtension.ELEMENT_DATA_NAME_SPACE);
            MessageExtension msgExt = convertStandardExtToMessageExt(stdExtElement);
            messageData.setMsgExt(msgExt);

        }
        return messageData;

    }


    public static MessageExtension convertStandardExtToMessageExt(StandardExtensionElement standardExtElement) {
        MessageExtension msgExt = null;
        if (standardExtElement != null) {
            msgExt = new MessageExtension();
            List<StandardExtensionElement> listElements = standardExtElement.getElements();
            for (StandardExtensionElement element : listElements
                    ) {
                String elementName = element.getElementName();
                String elementValue;
                if(elementName.equalsIgnoreCase("delivered") ||elementName.equalsIgnoreCase("displayed"))
                {
                    elementValue = "1";
                }
                else
                {
                    elementValue = element.getText() + "";
                }

                msgExt.addElement(elementName, elementValue);
            }
        }
        return msgExt;

    }


    //Check weather this message is created by me
    public static boolean isMsgSentByMe(Message message) {
        try {
            String fromJid = IdConvertorUtils.convertJidToStringBareJid(message.getFrom());
            String toJid = IdConvertorUtils.convertJidToStringBareJid(message.getTo());
            String myJid = MessageExtractor.getJabberIdWithoutResource(PrefUtils.getJid());
            if (myJid != null && myJid.equalsIgnoreCase(fromJid)) {
                return true;
            }
        } catch (Exception ex) {
            LogUtils.LOGE(">>>>>>", ex.toString());
        }
        return false;
    }




}
