package com.appster.chatlib.messagecomposer;

import android.text.TextUtils;

import org.jivesoftware.smack.packet.StandardExtensionElement;

/**
 * Created by neeraj on 11/07/17.
 */

public class MessageExtension {


    //--------------------------------------------------   Constant KEYS  ------------------------------------------------------------


    public static final String ELEMENT_DATA_NAME_SPACE = "data:jabber";
    public static final String ELEMENT_DATA = "data";


    //-------------------------------  Delivery Status element -------------------------

    public static final String ELEMENT_KEY_MESSAGE_DATAT_YPE = "message_data_type";
    public static final String ELEMENT_RECEIPT_REQUEST = "receipt_request";
    public static final String ELEMENT_CHAT_NORMAL = "chat";
    public static final String ELEMENT_GROUP_CHAT_NORMAL = "groupchat_normal";



    public static final String ELEMENT_TO_JID = "to_jid";
    public static final String ELEMENT_FROM_JID = "from_jid";
    public static final String ELEMENT_MEDIA_TYPE = "mediaType";
    public static final String ELEMENT_URL = "url";
    public static final String ELEMENT_URL_THUMB = "thumbUrl";
    public static final String ELEMENT_URL_LOCAL = "UrlLocal";
    public static final String ELEMENT_FILE_NAME = "fileName";
    public static final String ELEMENT_FILE_TYPE = "fileType";
    public static final String IMAGE_TYPE_FULL = "/mediaFiles/";
    public static final String IMAGE_TYPE_THUMB = "/mediaFiles-thumb/";
    public static final String ELEMENT_MEDIA_TYPE_IMAGE = "image";

//    public static final String ELEMENT_DELIVERED = "msg_delivered";
public static final String ELEMENT_DELIVERED = "delivered";
    public static final String ELEMENT_STANZA_ID = "stanzaId";
//    public static final String ELEMENT_READ = "msg_read";
public static final String ELEMENT_DISPLAYED = "displayed";
    public static final String ELEMENT_MSG_CREATOR_JID = "msgCreatorJid";
    public static final String ELEMENT_GROUP_JID = "group_jid";

    public static final String ELEMENT_DATA_NAME_DELAY = "urn:xmpp:delay";
    public static final String ELEMENT_DELAY = "delay";

    public static final String ELEMENT_MSG_STATUS = "msg_status";


    //-------------------------------  Typing status element -------------------------

    public static final String ELEMENT_TYPING_MESSAGE = "TYPING_MESSAGE";
    public static final String ELEMENT_TYPING_STATUS = "TYPING_STATUS";
    public static final String ELEMENT_TYPING_STATUS_TO_JID = "TYPING_STATUS_TO_JID";
    public static final String ELEMENT_TYPING_STATUS_COMPOSER_JID = "TYPING_STATUS_COMPOSER_JID";


    private StandardExtensionElement.Builder builder;

    public MessageExtension() {
        builder = StandardExtensionElement.builder(ELEMENT_DATA, ELEMENT_DATA_NAME_SPACE);
    }

    public MessageExtension(String name, String nameSpace) {
        builder = StandardExtensionElement.builder(name, nameSpace);

    }

    public MessageExtension addElement(String tagName, String value) {
//        builder.addAttribute(tagName, value == null ? "" : value);
        if (!TextUtils.isEmpty(tagName)) {
            builder.addElement(tagName, value == null ? "" : value);
        }
        return this;

    }

    public String getElementValue(String tagName) {
        String value = null;
        StandardExtensionElement stdExtElement = builder.build().getFirstElement(tagName);
        if (stdExtElement != null) {
            value = stdExtElement.getText();
        }
        return value;
    }


    public StandardExtensionElement build() {
        return builder.build();
    }


}
