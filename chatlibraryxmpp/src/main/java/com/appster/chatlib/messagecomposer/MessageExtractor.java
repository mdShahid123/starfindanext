package com.appster.chatlib.messagecomposer;

/**
 * Created by neeraj on 20/07/17.
 */

public class MessageExtractor {



    //Check resource is attached with jabber id
    public static String getJabberIdWithoutResource(String jabberIdwithResource) {
        if (jabberIdwithResource != null && jabberIdwithResource.length() > 0 && jabberIdwithResource.contains("/")) {
            return jabberIdwithResource.split("/")[0];
        }
        return jabberIdwithResource;
    }


}
