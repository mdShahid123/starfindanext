package com.appster.chatlib.messagecomposer;

/**
 * Copyright © 2017 Appster LLP. All rights reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Created by Neeraj Kumar Kirola
 */

import org.jivesoftware.smack.packet.Message;

/**
 * Created by neeraj on 10/07/17.
 */

//This model class is used to pass chat message data to SUCManager to send as message.
//It is mandatory to create object of this class
public class MessageData {

    MessageExtension msgExt;


    //toJid is mandatory field to be entered. Otherwise an exception will thrown with a message
    public String toJid;


    //fromJid if for Received message only
    public String fromJid;
    public String msgText, stanzaId;
    public Message.Type chatType = Message.Type.chat;
    public String fileName, fileType;

    //Default message type is text
    public MediaType mediaType = MediaType.text;
    public MessageType messageType = MessageType.sender;


    //These fields are mandatory if mediaType is image type. Otherwise an exception will thrown with a message
    public String imageUrl, imageUrlThumb, imageUrlLocal;

    public int msgStatus;

    private MessageData(String toJid, String msg) {
        this.toJid = toJid;
        this.msgText = msg;

    }

    public MessageData(String toJid, String msg, MessageType messageType) {
        this(toJid,msg);
        this.mediaType = MediaType.text;
        this.messageType = messageType;

    }

    public MessageData(String toJid, String msg, MessageType messageType, String imageUrl, String imageUrlThumb) {
        this(toJid, msg, messageType);
        this.mediaType = MediaType.image;
        this.imageUrl = imageUrl;
        this.imageUrlThumb = imageUrlThumb;
    }

    public MessageData(String toJid, String msg, MessageType messageType, String imageUrl, String imageUrlThumb, String imageUrlLocal) {
        this(toJid, msg, messageType, imageUrl, imageUrlThumb);
        this.imageUrlLocal = imageUrlLocal;
    }

    // Type of message : send by my, received by me, dataheader display in center horizontally, headre msg display in center horizontally
    public enum MessageType {
        sender,
        receiver,
        dateHeader,
        headerMsg
    }

    //MediaType is used to identiry : This message contains text only or image itself.
    public enum MediaType {
        text,
        image,
        video,
        doc
    }

    public enum FileType {
        doc,
        html,
        pdf,
        ppt,
        txt,
        xls,
        xml
    }


    public void addCustomValues(String tagName, String value) {
        if (msgExt == null) {
            msgExt = new MessageExtension();
        }
        msgExt.addElement(tagName, value);
    }

    //tagName or element name
    public String getCustomValue(String tagName) {
        String value = null;
        if (msgExt != null) {
            value = msgExt.getElementValue(tagName);
        }
        return value;
    }

    /*It shows current status of message
                                          - Sender messsage Status -
    unsent : This message is created by not sent or created and saved locally but not sent.
    sent : This message is sent by me, but not delivered.
    delivered : This message is delivered to another user. We have received acknowledge that this message is received.
    read : This message is read by receiver.

                                          - Receiver messsage Status -
     unread : This message is reveived but not read by receiver.
     read_sent :  Read acknowledge of this message has been sent.

    */
    public enum MsgStatus {
        unsent(0), sent(1),
        delivered(2), unread(3), read(4), read_sent(5);
        private final int status;

        MsgStatus(final int statusValue) {
            status = statusValue;
        }

        public int getValue() {
            return status;
        }
    }

    //It Define role of group memeber in group
    public enum Role {
        admins, members
    }


    public MessageExtension getMsgExt() {
        return msgExt;
    }

    public void setMsgExt(MessageExtension msgExt) {
        this.msgExt = msgExt;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getToJid() {
        return toJid;
    }

    public void setToJid(String toJid) {
        this.toJid = toJid;
    }

    public String getStanzaId() {
        return stanzaId;
    }

    public void setStanzaId(String stanzaId) {
        this.stanzaId = stanzaId;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrlThumb() {
        return imageUrlThumb;
    }

    public void setImageUrlThumb(String imageUrlThumb) {
        this.imageUrlThumb = imageUrlThumb;
    }

    public String getImageUrlLocal() {
        return imageUrlLocal;
    }

    public void setImageUrlLocal(String imageUrlLocal) {
        this.imageUrlLocal = imageUrlLocal;
    }

    public String getFromJid() {
        return fromJid;
    }

    public void setFromJid(String fromJid) {
        this.fromJid = fromJid;
    }

    public Message.Type getChatType() {
        return chatType;
    }

    public void setChatType(Message.Type chatType) {
        this.chatType = chatType;
    }

    public int getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(int msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
