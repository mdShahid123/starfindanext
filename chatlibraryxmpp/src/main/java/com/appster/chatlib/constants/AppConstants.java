package com.appster.chatlib.constants;

import com.appster.chatlib.utils.PrefUtils;

/**
 * Created by neeraj on 03/07/17.
 */

public class AppConstants {


    public static final String KEY_TO_USER_PIC = "TO_USERPIC";
    public static final String KEY_IS_CHAT_INACTIVE = "IS_CHAT_INACTIVE";
    public static String jabberIdSuffix = "@" + PrefUtils.getServiceName();

    public static final String USERNAME = "user_name";
    public static final String PASSWORD = "password";

    public static final String MESSAGE_ID = "messageId";

    public static final String KEY_TO_USER = "TO_USER";
    public static final String KEY_TO_USERNAME = "TO_USERNAME";
    public static final String CHAT_MSG_BROADCAST_IDENTIFIER = "com.appster.chatlib.ChatMessage";

    public static final String CHAT_MSG_RECEIVED_BROADCAST_IDENTIFIER = "chat.appster.com.xmppchat.msg_received_broadcast_identifier";

    public enum UserType {
        sender,
        receiver,
        dateHeader,
        headerMsg,
    }

    public enum MsgStatus {
        unsent(0), sent(1), delivered(2), unread(3), read(4), read_sent(5);
        private final int status;

        MsgStatus(final int statusValue) {
            status = statusValue;
        }

        public int getValue() {
            return status;
        }
    }

}
