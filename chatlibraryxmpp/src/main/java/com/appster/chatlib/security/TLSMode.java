package com.appster.chatlib.security;

/**
 * Created by neeraj on 13/07/17.
 */

import org.jivesoftware.smack.ConnectionConfiguration;

/**
 * Supported security mode.
 *
 *
 */
public enum TLSMode {

    /**
     * Security via TLS encryption is used whenever it's available.
     */
    enabled,

    /**
     * Security via TLS encryption is required in order to connect.
     */
    required,

    /**
     * Security via old SSL based encryption is enabled. If the server does not
     * handle legacy-SSL, the connection to the server will fail.
     */
    legacy;

    ConnectionConfiguration.SecurityMode getSecurityMode() {
        if (this == enabled)
            return ConnectionConfiguration.SecurityMode.ifpossible;
        else if (this == required)
            return ConnectionConfiguration.SecurityMode.required;
        else if (this == legacy)
            return ConnectionConfiguration.SecurityMode.disabled;
        else
            throw new IllegalStateException();
    }

}
