/**
 * Copyright (c) 2013, Redsolution LTD. All rights reserved.
 * <p>
 * This file is part of Xabber project; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License, Version 3.
 * <p>
 * Xabber is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License,
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */
package com.appster.chatlib.connection;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.listener.OnCloseListener;
import com.appster.chatlib.listener.OnInitializedListener;
import com.appster.chatlib.receiver.ConnectivityReceiver;
import com.appster.chatlib.utils.LogUtils;


/**
 * Manage network connectivity.
 *
 * @author alexander.ivanov
 */
public class NetworkManager implements OnCloseListener, OnInitializedListener {

    private final ConnectivityReceiver connectivityReceiver;

    private final ConnectivityManager connectivityManager;

    /**
     * Type of last active network.
     */
    private Integer type;

    /**
     * Whether last active network was suspended.
     */
    private boolean suspended;

    private final WifiLock wifiLock;

    private final WakeLock wakeLock;

    /**
     * Current network state.
     */
    private NetworkState state;

    private static final NetworkManager instance;
    private static boolean isInitialized = false;

    static {
        instance = new NetworkManager(ChatController.getAppContext());

    }

    public static NetworkManager getInstance() {
        return instance;
    }

    private NetworkManager(Context application) {
        connectivityReceiver = new ConnectivityReceiver();
        connectivityManager = (ConnectivityManager) application
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo active = connectivityManager.getActiveNetworkInfo();
        type = getType(active);
        suspended = isSuspended(active);

        wifiLock = ((WifiManager) application.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE)).createWifiLock(
                WifiManager.WIFI_MODE_FULL, "Wifi Lock");
        wifiLock.setReferenceCounted(false);
        wakeLock = ((PowerManager) application
                .getSystemService(Context.POWER_SERVICE)).newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "Wake Lock");
        wakeLock.setReferenceCounted(false);
        state = NetworkState.available;
    }

    /**
     * @param networkInfo
     * @return Type of network. <code>null</code> if network is
     * <code>null</code> or it is not connected and is not suspended.
     */
    private Integer getType(NetworkInfo networkInfo) {
        if (networkInfo != null
                && (networkInfo.getState() == State.CONNECTED || networkInfo
                .getState() == State.SUSPENDED))
            return networkInfo.getType();
        return null;
    }

    /**
     * @param networkInfo
     * @return <code>true</code> if network is not <code>null</code> and is
     * suspended.
     */
    private boolean isSuspended(NetworkInfo networkInfo) {
        return networkInfo != null && networkInfo.getState() == State.SUSPENDED;
    }

    public NetworkState getState() {
        return state;
    }

    @Override
    public void onInitialized() {
        if (!isInitialized) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(ConnectivityManager.ACTION_BACKGROUND_DATA_SETTING_CHANGED);
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            ChatController.getAppContext()
                    .registerReceiver(connectivityReceiver, filter);
            onWakeLockSettingsChanged();
            onWifiLockSettingsChanged();
            isInitialized = true;
        }
    }

    @Override
    public void onClose() {
        try {
            isInitialized = false;
            if (connectivityReceiver != null) {
                ChatController.getAppContext().unregisterReceiver(connectivityReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onNetworkChange(NetworkInfo networkInfo) {
        try {
            NetworkInfo active = connectivityManager.getActiveNetworkInfo();
            LogUtils.LOGI("NetworkMan", "Network: " + networkInfo + ", active: " + active);
            Integer type;
            boolean suspended;
            if (active == null && this.type != null
                    && this.type == networkInfo.getType()) {
                type = getType(networkInfo);
                suspended = isSuspended(networkInfo);
            } else {
                type = getType(active);
                suspended = isSuspended(active);
            }
            if (this.type == type) {
                if (this.suspended == suspended)
                    LogUtils.LOGI("NetworkMan", "State does not changed.");
                else if (suspended)
                    onSuspend();
                else
                    onResume();
            } else {
                if (suspended) {
                    type = null;
                    suspended = false;
                }
                if (type == null)
                    onUnavailable();
                else
                    onAvailable(type);
            }
            this.type = type;
            this.suspended = suspended;
        } catch (SecurityException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * New network is available. Start connection.
     */
    private void onAvailable(int type) {
        state = NetworkState.available;
        LogUtils.LOGI("NetworkMan", "Available");
        ConnectionManager.restartConnection();
//        if (type == ConnectivityManager.TYPE_WIFI)
//            ConnectionManager.getInstance().forceReconnect();
//        else
//            ConnectionManager.getInstance().updateConnections(false);
    }

    /**
     * Network is temporary unavailable.
     */
    private void onSuspend() {
        state = NetworkState.suspended;
        LogUtils.LOGI("NetworkMan", "Suspend");
        ChatController.getInstance().disconnectConnection();
        // TODO: ConnectionManager.getInstance().pauseKeepAlive();
    }

    /**
     * Network becomes available after suspend.
     */
    private void onResume() {
        state = NetworkState.available;
        LogUtils.LOGI("NetworkMan", "Resume");
        ConnectionManager.restartConnection();
        // TODO: ConnectionManager.getInstance().forceKeepAlive();
    }

    /**
     * Network is not available. Stop connections.
     */
    private void onUnavailable() {
        state = NetworkState.unavailable;
        LogUtils.LOGI("NetworkMan", "Unavailable");
        ChatController.getInstance().disconnectConnection();
//        ConnectionManager.getInstance().updateConnections(false);
    }

    public void onWifiLockSettingsChanged() {
//        if (SettingsManager.connectionWifiLock())
//            wifiLock.acquire();
//        else
//            wifiLock.release();
    }

    public void onWakeLockSettingsChanged() {
//        if (SettingsManager.connectionWakeLock())
//            wakeLock.acquire();
//        else
//            wakeLock.release();
    }

}
