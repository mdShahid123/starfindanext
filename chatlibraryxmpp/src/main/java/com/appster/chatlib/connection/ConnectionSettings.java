package com.appster.chatlib.connection;

import com.appster.chatlib.security.TLSMode;

/**
 * Created by neeraj on 14/07/17.
 */

public class ConnectionSettings {


    /**
     * User part of jid.
     */
    private final String userName;

    /**
     * Server part of jid.
     */
    private final String serverName;

    /**
     * Resource part of jid.
     */
    private final String resource;

    /**
     * Use custom connection host and port.
     */
    private boolean custom;

    /**
     * Host for connection.
     */
    private String host;

    /**
     * Port for connection.
     */
    private int port;

    /**
     * Password.
     */
    private String password;

    /**
     * Whether SASL Authentication Enabled.
     */
    private boolean saslEnabled;

    /**
     * TLS mode.
     */
    private TLSMode tlsMode;

    public ConnectionSettings(String resource, boolean custom, String host, int port, String password, boolean saslEnabled, TLSMode tlsMode, String userName, String serverName) {
        this.resource = resource;
        this.custom = custom;
        this.host = host;
        this.port = port;
        this.password = password;
        this.saslEnabled = saslEnabled;
        this.tlsMode = tlsMode;
        this.userName = userName;
        this.serverName = serverName;
    }


    public String getUserName() {
        return userName;
    }

    public String getServerName() {
        return serverName;
    }

    public String getResource() {
        return resource;
    }

    public boolean isCustom() {
        return custom;
    }

    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSaslEnabled() {
        return saslEnabled;
    }

    public void setSaslEnabled(boolean saslEnabled) {
        this.saslEnabled = saslEnabled;
    }

    public TLSMode getTlsMode() {
        return tlsMode;
    }

    public void setTlsMode(TLSMode tlsMode) {
        this.tlsMode = tlsMode;
    }

    public void updateConnectionSettings(boolean custom, String host, int port, String password, boolean saslEnabled, TLSMode tlsMode) {
        this.custom = custom;
        this.host = host;
        this.port = port;
        this.password = password;
        this.saslEnabled = saslEnabled;
        this.tlsMode = tlsMode;
    }

}
