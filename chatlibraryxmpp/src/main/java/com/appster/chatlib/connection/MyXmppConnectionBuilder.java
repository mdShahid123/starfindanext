package com.appster.chatlib.connection;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.utils.LogUtils;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.util.ArrayList;


public class MyXmppConnectionBuilder {

    private static MyXmppConnectionBuilder myXmppConnectionBuilder;
    private ArrayList<ConnectionListener> connectionListenerList;

    private XMPPTCPConnection connection;
    private ConnectionStatus connectionStatus = ConnectionStatus.DISCONNECTED;
    private final ConnectionListener connectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection connection) {
            LogUtils.LOGD("xcstatus", "connected");
            ChatController.connection = (AbstractXMPPConnection) connection;


        }

        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {
            LogUtils.LOGD("xcstatus", "authenticated");
            connectionStatus = ConnectionStatus.CONNECTED;
            Connection.connectedSuccessfully();
        }

        @Override
        public void connectionClosed() {
            LogUtils.LOGD("xcstatus", "connectionClosed");
            connectionStatus = ConnectionStatus.DISCONNECTED;

//            ChatController.getInstance().disconnectConnection();
//            Intent disconnectIntent = new Intent(AppConstants.CONNECTION_DISCONNECTED);
//            ChatController.getAppContext().sendBroadcast(disconnectIntent);

        }

        @Override
        public void connectionClosedOnError(Exception e) {
            LogUtils.LOGD("xcstatus", "connectionClosedOnError:" + e.getMessage());
            connectionStatus = ConnectionStatus.DISCONNECTED;
//            ChatController.getInstance().disconnectConnection();
//            Intent disconnectIntent = new Intent(AppConstants.CONNECTION_DISCONNECTED);
//            ChatController.getAppContext().sendBroadcast(disconnectIntent);
        }

        @Override
        public void reconnectionSuccessful() {
            LogUtils.LOGD("xcstatus", "reconnectionSuccessful");
            connectionStatus = ConnectionStatus.CONNECTED;
        }

        @Override
        public void reconnectingIn(int seconds) {
            LogUtils.LOGD("xcstatus", "reconnectingIn -> " + seconds);
            connectionStatus = ConnectionStatus.CONNECTING;

        }

        @Override
        public void reconnectionFailed(Exception e) {
            LogUtils.LOGD("xcstatus", "reconnectionFailed  " + e.getMessage());
//            ChatController.getInstance().disconnectConnection();
            connectionStatus = ConnectionStatus.DISCONNECTED;

        }
    };

    public MyXmppConnectionBuilder() {
        connectionListenerList = new ArrayList<>();
    }

    public static MyXmppConnectionBuilder getInstance() {
        if (myXmppConnectionBuilder == null) {
            myXmppConnectionBuilder = new MyXmppConnectionBuilder();
        }
        return myXmppConnectionBuilder;
    }

    public XMPPTCPConnection getConnectionXmppConnection(XMPPTCPConnectionConfiguration.Builder configBuilder) {
        if (connection == null) {
            connection = new XMPPTCPConnection(configBuilder.build());
            addConnectionListener(connectionListener);
        }
        return connection;
    }

    public XMPPTCPConnection getConnection() {
        return connection;
    }


    public void addConnectionListener(ConnectionListener connectionListener) {
        connectionListenerList.add(connectionListener);
        if (connection != null) {
            for (ConnectionListener cl : connectionListenerList
                    ) {
                connection.addConnectionListener(cl);
            }

        }
    }

    public void removeConnectionListeners() {
        try {
            if (connection != null && connectionListenerList != null && connectionListenerList.size() > 0) {
                for (ConnectionListener cl : connectionListenerList) {
                    connection.removeConnectionListener(cl);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void destroyConnectionObject() {
        try {
            if (connection != null) {
                removeConnectionListeners();
                connection = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ConnectionStatus getConnectionStatus() {
        return connectionStatus;
    }

    public static enum ConnectionStatus {CONNECTED, DISCONNECTED, CONNECTING}
}
