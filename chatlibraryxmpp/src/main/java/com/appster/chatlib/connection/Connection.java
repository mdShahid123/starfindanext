package com.appster.chatlib.connection;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.utils.LogUtils;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;


public class Connection {
    public static PingManager pingManager;
    public static PingFailedListener pingFailedListener;
    public static int PING_INTERVAL_SYNC = 10; // in seconds don't reduce ping interval otherwise history sync will slow or hang.
    public static int PING_INTERVAL = 15;
    private static ConnectionConfiguration.SecurityMode securityMode = ConnectionConfiguration.SecurityMode.ifpossible;


    /**
     * Setup connection here
     *
     * @param user
     * @param password
     * @param hostname
     * @param port
     * @param servicename
     */
    public static void createConnection(final String user, final String password, String hostname, int port, String servicename) {

        final XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        configBuilder.setUsernameAndPassword(user, password);
        configBuilder.setHost(hostname);
        configBuilder.setPort(port);
        configBuilder.setSendPresence(true);

        configBuilder.setSecurityMode(securityMode);
        configBuilder.setDebuggerEnabled(true);

        try {
            DomainBareJid serviceName = JidCreate.domainBareFrom(servicename);
            configBuilder.setXmppDomain(serviceName);

            InetAddress addr = InetAddress.getByName(hostname);
            configBuilder.setHostAddress(addr);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        configBuilder.setConnectTimeout(60000);
        try {
            configBuilder.setResource(getUniqueResource());
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }


//        XMPPTCPConnection.setUseStreamManagementDefault(true);
//        XMPPTCPConnection.setUseStreamManagementResumptionDefault(true);

//        SASLAuthentication.unBlacklistSASLMechanism(SASLPlainMechanism.NAME);
//        SASLAuthentication.unBlacklistSASLMechanism(SASLPlainMechanism.CRAMMD5);
//        SASLAuthentication.unBlacklistSASLMechanism(SASLPlainMechanism.DIGESTMD5);
        SASLAuthentication.unBlacklistSASLMechanism("SCRAM-SHA-1");

//        SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
//        SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
//        SASLAuthentication.blacklistSASLMechanism(SASLPlainMechanism.NAME);
//        SASLAuthentication.blacklistSASLMechanism(SASLPlainMechanism.CRAMMD5);

        try {
            TLSUtils.acceptAllCertificates(configBuilder);
            TLSUtils.disableHostnameVerificationForTlsCertificates(configBuilder);
        } catch (NoSuchAlgorithmException e) {
            LogUtils.LOGE(">>>", e.toString());
        } catch (KeyManagementException e) {
            LogUtils.LOGE(">>>", e.toString());
        }

        ChatController.connection = MyXmppConnectionBuilder.getInstance().getConnectionXmppConnection(configBuilder);

//        if (NetworkMonitor.isNetworkAvailable(context, false)) {
        connectAndLogin();
//        }
        enableAutomaticReconnect();

    }


    public static void createConnection(XMPPTCPConnectionConfiguration.Builder configBuilder) {
//        ProviderManager.addIQProvider("vCard", "vcard-temp", new VCardProvider());
//        ProviderManager.addExtensionProvider("result", "urn:xmpp:mam:0", new PacketExtProvider(mContext));
//        ProviderManager.addExtensionProvider("fin", "urn:xmpp:mam:0", new PacketExtProvider(mContext));


        XMPPTCPConnection.setUseStreamManagementDefault(true);
        XMPPTCPConnection.setUseStreamManagementResumptionDefault(true);

//        SASLAuthentication.unBlacklistSASLMechanism(SASLPlainMechanism.NAME);
//        SASLAuthentication.unBlacklistSASLMechanism(SASLPlainMechanism.CRAMMD5);
//        SASLAuthentication.unBlacklistSASLMechanism(SASLPlainMechanism.DIGESTMD5);
//        SASLAuthentication.unBlacklistSASLMechanism("SCRAM-SHA-1");

//        SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
//        SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
//        SASLAuthentication.blacklistSASLMechanism(SASLPlainMechanism.NAME);

//        try {
//            TLSUtils.acceptAllCertificates(configBuilder);
//            TLSUtils.disableHostnameVerificationForTlsCertificicates(configBuilder);
//        } catch (NoSuchAlgorithmException e) {
//            LogUtils.LOGE(">>>", e.toString());
//        } catch (KeyManagementException e) {
//            LogUtils.LOGE(">>>", e.toString());
//        }

        ChatController.connection = MyXmppConnectionBuilder.getInstance().getConnectionXmppConnection(configBuilder);

//        if (NetworkMonitor.isNetworkAvailable(context, false)) {
        connectAndLogin();
//        }
        enableAutomaticReconnect();

    }

    public static void setSecurityMode(ConnectionConfiguration.SecurityMode securityMode) {
        Connection.securityMode = securityMode;
    }

    public static void connectedSuccessfully() {
//        Intent authenticatedIntent = new Intent(AppConstants.CONNECTION_AUTHENTICATED);
//        mContext.sendBroadcast(authenticatedIntent);


//        pingFailedListener = new PingFailedListener() {
//            @Override
//            public void pingFailed() {
//                LogUtils.LOGD("pingmanager", "ping failed...");
//            }
//        };

//        // Don't remove or comment this code, this is helpful to complete history sync XEP313
        pingManager = PingManager.getInstanceFor(ChatController.connection);
//
//        pingManager.registerPingFailedListener(pingFailedListener);
//
        ChatManager.getInstanceFor(ChatController.connection);
//        MUCManager.multiUserChatManager = MultiUserChatManager.getInstanceFor(ChatController.connection);
//        MUCManager.setInviteListener(MUCManager.multiUserChatManager, mContext);
//
//
//        if (PreferenceUtil.getIsOldUserMessageRequired()) {
//            pingManager.setPingInterval(PING_INTERVAL_SYNC);
//            LogUtils.LOGE("xcstatus", "required fetching old messages");
//            Intent allGroup = new Intent(ChatSyncFullActivity.BROAD_OLD_MESSAGES);
//            allGroup.putExtra(AppConstants.EXTRA_CHAT_SYNC_TYPE_GROUP, true);
//            LocalBroadcastManager.getInstance(mContext).sendBroadcast(allGroup);
//        } else {
//            pingManager.setPingInterval(PING_INTERVAL);
//            fetchReadStatus();
//        }

        ChatController.getInstance().setAllMessageListeners();

    }

    public static void enableAutomaticReconnect() {
//        ConnectionManager.getInstance().enable();
        ReconnectionManager.getInstanceFor(ChatController.connection).enableAutomaticReconnection();
    }

    public static void disableAutomaticReconnect() {
//        ConnectionManager.stop();
        ReconnectionManager.getInstanceFor(ChatController.connection).disableAutomaticReconnection();
    }


    public static void fetchReadStatus() {
//        ChatController.getInstance().setUniversalChatListener();
//
//        Presence p = new Presence(Presence.Type.available, "I am available.", 42, Presence.Mode.available);
//        try {
//            ChatController.connection.sendStanza(p);
//        } catch (SmackException.NotConnectedException e) {
//            LogUtils.LOGE(Connection.class.getName(), e.getMessage());
//        } catch (Exception ex) {
//            LogUtils.LOGE(Connection.class.getName(), ex.getMessage());
//        }
//
//        rejoinAllGroups();
////            sendReadStatus();
//
//
//        UserData user = PreferenceUtil.getUserProfile();
//        if (user != null) {
//            ChatUser myProfile = new ChatUser();
//            myProfile.setProfilePic(user.getProfilePicture());
//            myProfile.setMobile(user.getMobile());
//            myProfile.setName(user.getName());
//            new VCardProfileDetails().uploadVCardProfile(myProfile);
//        }
    }

    public static void rejoinAllGroups() {
//        if (!GroupRejoinTask.isWorking) {
//            new GroupRejoinTask().execute();
//        } else {
//            LogUtils.LOGD("Connection", "rejoin already working...");
//        }

    }


//    public static void sendReadStatus() {
//        new SendReadStatusTask(null, null).execute();
//    }

    public static void sendAllUnsentMessages() {
//        new SendAllUnsentMsgTask().execute();
    }


    private static void connectAndLogin() {
        try {
            ChatController.connection.setPacketReplyTimeout(20000);
            if (!ChatController.connection.isConnected()) {

                ChatController.connection.connect();
                ChatController.connection.login();

            } else if (!ChatController.connection.isAuthenticated()) {
                ChatController.connection.login();
            }
        } catch (Exception e) {
            LogUtils.LOGE("connectAndLogin:" + Connection.class.getName(), e.getMessage());
        }
    }

    public static String getUniqueResource() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

}
