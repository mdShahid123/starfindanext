package com.appster.chatlib.connection;

import com.appster.chatlib.ChatController;
import com.appster.chatlib.utils.DelayActionExecutor;
import com.appster.chatlib.utils.LogUtils;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.ping.PingManager;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class ConnectionManager {

    private final static String TAG = "ConnectionManager";

    private static ConnectionManager myAlarmPingManager;
    private static boolean isEnabled;
    private static ScheduledExecutorService scheduler;
    private static long INTERVAL = 15L;
    private long START_AFTER = 15L;
    private int attempt;


    private ConnectionManager() {
        if (scheduler == null || scheduler.isShutdown()) {
            scheduler =
                    Executors.newSingleThreadScheduledExecutor();
        }
    }

    public static ConnectionManager getInstance() {

        if (myAlarmPingManager == null) {
            myAlarmPingManager = new ConnectionManager();
        }
        return myAlarmPingManager;
    }

    public static void stop() {
        try {
            scheduler.shutdownNow();
            myAlarmPingManager = null;
            isEnabled = false;
            LogUtils.LOGD(TAG, "alarm manager stopped.");
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, ex.toString());
        }

    }

    public void enable() {

        if (isEnabled) return;
        isEnabled = true;
        LogUtils.LOGD(TAG, "start ping alarm manager");
        try {
            scheduler.scheduleAtFixedRate
                    (new Runnable() {
                        public void run() {
                            try {
                                checkNconnect();
                            } catch (Exception ex) {
                                LogUtils.LOGE(TAG, ex.toString());
                            }
                        }
                    }, START_AFTER, INTERVAL, TimeUnit.SECONDS);
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, ex.toString());
        }
    }

    public static void setInterval(long interval) {
        INTERVAL = interval > 15L ? interval : INTERVAL;
    }

    public static void restartConnection() {
        ChatController.getInstance().disconnectConnection();

        new DelayActionExecutor().startOnUI(new DelayActionExecutor.OnExecution() {
            @Override
            public void onExecute() {
                try {
                    ChatController.getInstance().startConnection();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void checkNconnect() {
        attempt++;
        LogUtils.LOGD(TAG, "check connection attempt ->"+ attempt);
        if (ChatController.connection != null) {
            try {
                if (!PingManager.getInstanceFor(ChatController.connection).pingMyServer()) {
                    if (!ChatController.connection.isAuthenticated())
                        restartConnection();
                    LogUtils.LOGD(TAG, "ping failed from alarm manager");
                } else {
                    try {
                        if (attempt > 4) {
                            attempt = 0;
//                            Connection.rejoinAllGroups();
                        }
                    } catch (Exception ex) {
                        LogUtils.LOGE(TAG, ex.toString());
                    }
                }
            } catch (SmackException.NotConnectedException e) {
                LogUtils.LOGE(TAG, e.toString());
                restartConnection();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LogUtils.LOGE(TAG, e.toString());
            }

        } else {
            try {
                ChatController.getInstance().startConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (attempt > 5) {
            attempt = 0;
        }
    }

}
