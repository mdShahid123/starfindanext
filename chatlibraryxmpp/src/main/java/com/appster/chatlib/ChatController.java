package com.appster.chatlib;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.appster.chatlib.connection.Connection;
import com.appster.chatlib.connection.MyXmppConnectionBuilder;
import com.appster.chatlib.connection.NetworkManager;
import com.appster.chatlib.constants.AppConstants;
import com.appster.chatlib.listener.IncomingMessageListener;
import com.appster.chatlib.listener.MessageListener;
import com.appster.chatlib.service.ConnectionService;
import com.appster.chatlib.task.ConnectionTask;
import com.appster.chatlib.utils.LogUtils;
import com.appster.chatlib.utils.PrefUtils;
import com.appster.chatlib.utils.TaskRunnerManager;
import com.orhanobut.hawk.Hawk;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.filter.StanzaTypeFilter;

/**
 * This class is used to controll most of the functionalities of this library.
 * It provide initial intraction with app.
 */

public class ChatController {

    private String TAG = "ChatController";
    public static boolean isDubug = true;
    public static AbstractXMPPConnection connection;
    public static ChatManager chatmanager;
    public static boolean isChatScreenActive = false;
    private static Context mAppContext;
    private static ChatController chatController;
    public static ConnectionTask connectionTask;

    static {
        chatController = new ChatController();
    }

    private ChatController() {

    }

    public static ChatController getInstance() {
        if (chatController == null) {
            chatController = new ChatController();
        }
        return chatController;
    }

    public static void init(Context context) {
        mAppContext = context;
        Hawk.init(context).build();
    }

    public static void destroyAllStaticVariables() {
        chatmanager = null;
        connectionTask = null;
        chatmanager = null;
        connection = null;
        isChatScreenActive = false;
        LogUtils.LOGD("STATIC", "Destroy all static variables.");

    }


    /**
     * It gets minimum / mandatory fields to start connection service to createConnection with chat server.
     *
     * @param userName
     * @param jid
     * @param password
     * @param host
     * @param port
     * @param serviceName
     */
    public void setUpConnectionData(String userName, String jid, String password, String host, int port, String serviceName) {

        try {
            PrefUtils.setUserName(userName);
            PrefUtils.setJId(jid);
            PrefUtils.setPassword(password);
            PrefUtils.setHost(host);
            PrefUtils.setPort(port);
            PrefUtils.setServiceName(serviceName);
            PrefUtils.setSetupDone(true);
        } catch (Exception e) {
            LogUtils.LOGE(TAG, e.toString());
        }


    }

    public static Context getAppContext() {
        if (mAppContext == null) {
            LogUtils.LOGE("ChatController", "Please call ChatController.init() method first which is inside: \n" +
                    ChatController.class.getName());
        }
        return mAppContext;
    }


    /**
     * Check if all mandatory fields are provided by user
     *
     * @return exception message
     */
    private String canStartConnection() {

        //TODO : Check all mandatory fields before start connection service.
        // TODO If there may be more fields which are mandatory to start connection put & check them here

        String msg = "";
        if (!PrefUtils.isSetupDone()) {
            msg = mAppContext.getString(R.string.please_call_initsetup);
        } else if (TextUtils.isEmpty(PrefUtils.getHost())) {
            msg = mAppContext.getString(R.string.host_cant_left_blank);
        } else if (TextUtils.isEmpty(PrefUtils.getJid())) {
            msg = mAppContext.getString(R.string.jid_cant_left_blank);
        } else if (TextUtils.isEmpty(PrefUtils.getUsername())) {
            msg = mAppContext.getString(R.string.username_cant_left_blank);
        } else if (TextUtils.isEmpty(PrefUtils.getPassword())) {
            msg = mAppContext.getString(R.string.password_cant_left_blank);
        } else if (TextUtils.isEmpty(PrefUtils.getServiceName())) {
            msg = mAppContext.getString(R.string.servicename_cant_left_blank);
        }
        return msg;
    }


    /**
     * This method is used to start connection service.
     * will throw an exception if one of the mandatory field is empty
     * See 'canStartConnection()' method for mandatory fields.
     *
     * @throws Exception
     */
    public void startConnection() throws Exception {
        String isConnectionMsg = canStartConnection();
        if (!TextUtils.isEmpty(isConnectionMsg)) {
            isConnectionMsg = mAppContext.getString(R.string.cannot_setup_connection_profile_null) + "\n" + isConnectionMsg;
            LogUtils.LOGE(TAG, isConnectionMsg);
            throw new Exception(isConnectionMsg);
        }
        String userName = PrefUtils.getJid().split("@")[0];
        String pass = PrefUtils.getPassword();
        Intent mIntent = ConnectionService.createIntent(mAppContext);
        mIntent.putExtra(AppConstants.USERNAME, userName);
        mIntent.putExtra(AppConstants.PASSWORD, pass);
        mAppContext.startService(mIntent);
    }

    /**
     * Disconnect existing connection and also disbale auto reconnect service.
     *
     * @param context
     */
    public void closeConnection(Context context) {
        Connection.disableAutomaticReconnect();
        Intent mIntent = ConnectionService.createIntent(mAppContext);
        mIntent.setAction(ConnectionService.STOP_CONNECTION_SERVICE);
        context.startService(mIntent);
    }

    /**
     * Chekc if connection already persist
     *
     * @return
     */
    public boolean canReconnect() {
        boolean canConnect = false;
        String isConnectionMsg = canStartConnection();
        if (!TextUtils.isEmpty(isConnectionMsg)) {
            LogUtils.LOGE(TAG, "cannot  reconnect -->" + isConnectionMsg);
            return false;
        }
        if (ChatController.connection == null) {
        }
        try {
            if (!ChatController.connection.isAuthenticated())
//                canConnect = true;
            return !Connection.pingManager.pingMyServer();
        } catch (Exception e) {
            LogUtils.LOGE(ChatController.class.getName() + ":readyToInitService:", e.getMessage());
            canConnect = true;
        }
        LogUtils.LOGD(TAG, "CanReconnect-->" + (canConnect ? "" : "Cannot reconnect , Already connected."));
        return canConnect;
    }

    /**
     * Disconnect existing connection and dont disbale auto reconnect service, also dont terminate connection service.
     *
     * @param
     */
    public void disconnectConnection() {
        Intent mIntent = ConnectionService.createIntent(mAppContext);
        mIntent.setAction(ConnectionService.DISCONNECT_CONNECTION_SERVICE);
        mAppContext.startService(mIntent);
    }

    /**
     * To get status of connection when connection state changes.
     */
    public void setConnectionListener(ConnectionListener connectionListener) {

        MyXmppConnectionBuilder.getInstance().addConnectionListener(connectionListener);

    }

    /**
     * Check is user connected with chat server.
     *
     * @return
     */
    public boolean isConnected() {
        if (connection != null && connection.isConnected()) {
            return true;
        }
        return false;
    }

    /**
     * Check is user authenticated
     *
     * @return
     */
    public boolean isAuthenticated() {
        if (connection != null && connection.isAuthenticated()) {
            return true;
        }
        return false;
    }

    public void onServiceCreated() {

    }

    /**
     * Called when connection service started.
     */
    public void onServiceStarted() {
        onInitialized();
    }

    /**
     * Called when connection service destroyed.
     */
    public void onServiceDestroyed() {
        onClose();
    }


    /**
     * Initialize managers
     */
    private void onInitialized() {
        NetworkManager.getInstance().onInitialized();
        TaskRunnerManager.getInstance().onInitialized();
    }


    /**
     * close managers
     */
    private void onClose() {
        NetworkManager.getInstance().onClose();
        TaskRunnerManager.getInstance().onClose();
    }

    public void setAllMessageListeners() {
        setIncomingMessageListener();
    }

    protected void removeAllMessageListenrs() {
        removeIncomingMessageListener();
    }


    protected void setIncomingMessageListener() {
        if (connection != null) {
            connection.addAsyncStanzaListener(IncomingMessageListener.getInstance(), StanzaTypeFilter.MESSAGE);
        }
    }

    protected void removeIncomingMessageListener() {
        if (connection != null)
            connection.removeAsyncStanzaListener(IncomingMessageListener.getInstance());
    }

    public void setMessageListener(MessageListener messageListener) {
        IncomingMessageListener.getInstance().setMessageListener(messageListener);
    }

}
